
build\\robotflesh.elf:     file format elf32-littlearm

Sections:
Idx Name              Size      VMA       LMA       File off  Algn  Flags
  0 .isr_vector       00000188  08000000  08000000  00010000  2**0  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .text             0000cec4  08000190  08000190  00010190  2**4  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .rodata           00000970  0800d058  0800d058  0001d058  2**3  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .init_array       00000004  0800d9c8  0800d9c8  0001d9c8  2**2  CONTENTS, ALLOC, LOAD, DATA
  4 .fini_array       00000004  0800d9cc  0800d9cc  0001d9cc  2**2  CONTENTS, ALLOC, LOAD, DATA
  5 .data             00000250  20000000  0800d9d0  00020000  2**2  CONTENTS, ALLOC, LOAD, DATA
  6 .ccmram           00000000  10000000  10000000  00020250  2**0  CONTENTS
  7 .bss              000190f8  20000250  20000250  00020250  2**3  ALLOC
  8 ._user_heap_stack 00000600  20019348  20019348  00020250  2**0  ALLOC
  9 .ARM.attributes   00000030  00000000  00000000  00020250  2**0  CONTENTS, READONLY
 10 .debug_info       00021612  00000000  00000000  00020280  2**0  CONTENTS, READONLY, DEBUGGING
 11 .debug_abbrev     00004d08  00000000  00000000  00041892  2**0  CONTENTS, READONLY, DEBUGGING
 12 .debug_aranges    00001138  00000000  00000000  0004659a  2**0  CONTENTS, READONLY, DEBUGGING
 13 .debug_ranges     00000f98  00000000  00000000  000476d2  2**0  CONTENTS, READONLY, DEBUGGING
 14 .debug_macro      00020d51  00000000  00000000  0004866a  2**0  CONTENTS, READONLY, DEBUGGING
 15 .debug_line       0000e247  00000000  00000000  000693bb  2**0  CONTENTS, READONLY, DEBUGGING
 16 .debug_str        000aa9d6  00000000  00000000  00077602  2**0  CONTENTS, READONLY, DEBUGGING
 17 .comment          00000075  00000000  00000000  00121fd8  2**0  CONTENTS, READONLY
 18 .debug_frame      000050d4  00000000  00000000  00122050  2**2  CONTENTS, READONLY, DEBUGGING

Disassembly of section .text:

08000190 <__do_global_dtors_aux>:
 8000190:	b510      	push	{r4, lr}
 8000192:	4c05      	ldr	r4, [pc, #20]	; (80001a8 <__do_global_dtors_aux+0x18>)
 8000194:	7823      	ldrb	r3, [r4, #0]
 8000196:	b933      	cbnz	r3, 80001a6 <__do_global_dtors_aux+0x16>
 8000198:	4b04      	ldr	r3, [pc, #16]	; (80001ac <__do_global_dtors_aux+0x1c>)
 800019a:	b113      	cbz	r3, 80001a2 <__do_global_dtors_aux+0x12>
 800019c:	4804      	ldr	r0, [pc, #16]	; (80001b0 <__do_global_dtors_aux+0x20>)
 800019e:	f3af 8000 	nop.w
 80001a2:	2301      	movs	r3, #1
 80001a4:	7023      	strb	r3, [r4, #0]
 80001a6:	bd10      	pop	{r4, pc}
 80001a8:	20000250 	.word	0x20000250
 80001ac:	00000000 	.word	0x00000000
 80001b0:	0800d03c 	.word	0x0800d03c

080001b4 <frame_dummy>:
 80001b4:	b508      	push	{r3, lr}
 80001b6:	4b03      	ldr	r3, [pc, #12]	; (80001c4 <frame_dummy+0x10>)
 80001b8:	b11b      	cbz	r3, 80001c2 <frame_dummy+0xe>
 80001ba:	4903      	ldr	r1, [pc, #12]	; (80001c8 <frame_dummy+0x14>)
 80001bc:	4803      	ldr	r0, [pc, #12]	; (80001cc <frame_dummy+0x18>)
 80001be:	f3af 8000 	nop.w
 80001c2:	bd08      	pop	{r3, pc}
 80001c4:	00000000 	.word	0x00000000
 80001c8:	20000254 	.word	0x20000254
 80001cc:	0800d03c 	.word	0x0800d03c

080001d0 <__aeabi_drsub>:
 80001d0:	f081 4100 	eor.w	r1, r1, #2147483648	; 0x80000000
 80001d4:	e002      	b.n	80001dc <__adddf3>
 80001d6:	bf00      	nop

080001d8 <__aeabi_dsub>:
 80001d8:	f083 4300 	eor.w	r3, r3, #2147483648	; 0x80000000

080001dc <__adddf3>:
 80001dc:	b530      	push	{r4, r5, lr}
 80001de:	ea4f 0441 	mov.w	r4, r1, lsl #1
 80001e2:	ea4f 0543 	mov.w	r5, r3, lsl #1
 80001e6:	ea94 0f05 	teq	r4, r5
 80001ea:	bf08      	it	eq
 80001ec:	ea90 0f02 	teqeq	r0, r2
 80001f0:	bf1f      	itttt	ne
 80001f2:	ea54 0c00 	orrsne.w	ip, r4, r0
 80001f6:	ea55 0c02 	orrsne.w	ip, r5, r2
 80001fa:	ea7f 5c64 	mvnsne.w	ip, r4, asr #21
 80001fe:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8000202:	f000 80e2 	beq.w	80003ca <__adddf3+0x1ee>
 8000206:	ea4f 5454 	mov.w	r4, r4, lsr #21
 800020a:	ebd4 5555 	rsbs	r5, r4, r5, lsr #21
 800020e:	bfb8      	it	lt
 8000210:	426d      	neglt	r5, r5
 8000212:	dd0c      	ble.n	800022e <__adddf3+0x52>
 8000214:	442c      	add	r4, r5
 8000216:	ea80 0202 	eor.w	r2, r0, r2
 800021a:	ea81 0303 	eor.w	r3, r1, r3
 800021e:	ea82 0000 	eor.w	r0, r2, r0
 8000222:	ea83 0101 	eor.w	r1, r3, r1
 8000226:	ea80 0202 	eor.w	r2, r0, r2
 800022a:	ea81 0303 	eor.w	r3, r1, r3
 800022e:	2d36      	cmp	r5, #54	; 0x36
 8000230:	bf88      	it	hi
 8000232:	bd30      	pophi	{r4, r5, pc}
 8000234:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 8000238:	ea4f 3101 	mov.w	r1, r1, lsl #12
 800023c:	f44f 1c80 	mov.w	ip, #1048576	; 0x100000
 8000240:	ea4c 3111 	orr.w	r1, ip, r1, lsr #12
 8000244:	d002      	beq.n	800024c <__adddf3+0x70>
 8000246:	4240      	negs	r0, r0
 8000248:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 800024c:	f013 4f00 	tst.w	r3, #2147483648	; 0x80000000
 8000250:	ea4f 3303 	mov.w	r3, r3, lsl #12
 8000254:	ea4c 3313 	orr.w	r3, ip, r3, lsr #12
 8000258:	d002      	beq.n	8000260 <__adddf3+0x84>
 800025a:	4252      	negs	r2, r2
 800025c:	eb63 0343 	sbc.w	r3, r3, r3, lsl #1
 8000260:	ea94 0f05 	teq	r4, r5
 8000264:	f000 80a7 	beq.w	80003b6 <__adddf3+0x1da>
 8000268:	f1a4 0401 	sub.w	r4, r4, #1
 800026c:	f1d5 0e20 	rsbs	lr, r5, #32
 8000270:	db0d      	blt.n	800028e <__adddf3+0xb2>
 8000272:	fa02 fc0e 	lsl.w	ip, r2, lr
 8000276:	fa22 f205 	lsr.w	r2, r2, r5
 800027a:	1880      	adds	r0, r0, r2
 800027c:	f141 0100 	adc.w	r1, r1, #0
 8000280:	fa03 f20e 	lsl.w	r2, r3, lr
 8000284:	1880      	adds	r0, r0, r2
 8000286:	fa43 f305 	asr.w	r3, r3, r5
 800028a:	4159      	adcs	r1, r3
 800028c:	e00e      	b.n	80002ac <__adddf3+0xd0>
 800028e:	f1a5 0520 	sub.w	r5, r5, #32
 8000292:	f10e 0e20 	add.w	lr, lr, #32
 8000296:	2a01      	cmp	r2, #1
 8000298:	fa03 fc0e 	lsl.w	ip, r3, lr
 800029c:	bf28      	it	cs
 800029e:	f04c 0c02 	orrcs.w	ip, ip, #2
 80002a2:	fa43 f305 	asr.w	r3, r3, r5
 80002a6:	18c0      	adds	r0, r0, r3
 80002a8:	eb51 71e3 	adcs.w	r1, r1, r3, asr #31
 80002ac:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 80002b0:	d507      	bpl.n	80002c2 <__adddf3+0xe6>
 80002b2:	f04f 0e00 	mov.w	lr, #0
 80002b6:	f1dc 0c00 	rsbs	ip, ip, #0
 80002ba:	eb7e 0000 	sbcs.w	r0, lr, r0
 80002be:	eb6e 0101 	sbc.w	r1, lr, r1
 80002c2:	f5b1 1f80 	cmp.w	r1, #1048576	; 0x100000
 80002c6:	d31b      	bcc.n	8000300 <__adddf3+0x124>
 80002c8:	f5b1 1f00 	cmp.w	r1, #2097152	; 0x200000
 80002cc:	d30c      	bcc.n	80002e8 <__adddf3+0x10c>
 80002ce:	0849      	lsrs	r1, r1, #1
 80002d0:	ea5f 0030 	movs.w	r0, r0, rrx
 80002d4:	ea4f 0c3c 	mov.w	ip, ip, rrx
 80002d8:	f104 0401 	add.w	r4, r4, #1
 80002dc:	ea4f 5244 	mov.w	r2, r4, lsl #21
 80002e0:	f512 0f80 	cmn.w	r2, #4194304	; 0x400000
 80002e4:	f080 809a 	bcs.w	800041c <__adddf3+0x240>
 80002e8:	f1bc 4f00 	cmp.w	ip, #2147483648	; 0x80000000
 80002ec:	bf08      	it	eq
 80002ee:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 80002f2:	f150 0000 	adcs.w	r0, r0, #0
 80002f6:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 80002fa:	ea41 0105 	orr.w	r1, r1, r5
 80002fe:	bd30      	pop	{r4, r5, pc}
 8000300:	ea5f 0c4c 	movs.w	ip, ip, lsl #1
 8000304:	4140      	adcs	r0, r0
 8000306:	eb41 0101 	adc.w	r1, r1, r1
 800030a:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 800030e:	f1a4 0401 	sub.w	r4, r4, #1
 8000312:	d1e9      	bne.n	80002e8 <__adddf3+0x10c>
 8000314:	f091 0f00 	teq	r1, #0
 8000318:	bf04      	itt	eq
 800031a:	4601      	moveq	r1, r0
 800031c:	2000      	moveq	r0, #0
 800031e:	fab1 f381 	clz	r3, r1
 8000322:	bf08      	it	eq
 8000324:	3320      	addeq	r3, #32
 8000326:	f1a3 030b 	sub.w	r3, r3, #11
 800032a:	f1b3 0220 	subs.w	r2, r3, #32
 800032e:	da0c      	bge.n	800034a <__adddf3+0x16e>
 8000330:	320c      	adds	r2, #12
 8000332:	dd08      	ble.n	8000346 <__adddf3+0x16a>
 8000334:	f102 0c14 	add.w	ip, r2, #20
 8000338:	f1c2 020c 	rsb	r2, r2, #12
 800033c:	fa01 f00c 	lsl.w	r0, r1, ip
 8000340:	fa21 f102 	lsr.w	r1, r1, r2
 8000344:	e00c      	b.n	8000360 <__adddf3+0x184>
 8000346:	f102 0214 	add.w	r2, r2, #20
 800034a:	bfd8      	it	le
 800034c:	f1c2 0c20 	rsble	ip, r2, #32
 8000350:	fa01 f102 	lsl.w	r1, r1, r2
 8000354:	fa20 fc0c 	lsr.w	ip, r0, ip
 8000358:	bfdc      	itt	le
 800035a:	ea41 010c 	orrle.w	r1, r1, ip
 800035e:	4090      	lslle	r0, r2
 8000360:	1ae4      	subs	r4, r4, r3
 8000362:	bfa2      	ittt	ge
 8000364:	eb01 5104 	addge.w	r1, r1, r4, lsl #20
 8000368:	4329      	orrge	r1, r5
 800036a:	bd30      	popge	{r4, r5, pc}
 800036c:	ea6f 0404 	mvn.w	r4, r4
 8000370:	3c1f      	subs	r4, #31
 8000372:	da1c      	bge.n	80003ae <__adddf3+0x1d2>
 8000374:	340c      	adds	r4, #12
 8000376:	dc0e      	bgt.n	8000396 <__adddf3+0x1ba>
 8000378:	f104 0414 	add.w	r4, r4, #20
 800037c:	f1c4 0220 	rsb	r2, r4, #32
 8000380:	fa20 f004 	lsr.w	r0, r0, r4
 8000384:	fa01 f302 	lsl.w	r3, r1, r2
 8000388:	ea40 0003 	orr.w	r0, r0, r3
 800038c:	fa21 f304 	lsr.w	r3, r1, r4
 8000390:	ea45 0103 	orr.w	r1, r5, r3
 8000394:	bd30      	pop	{r4, r5, pc}
 8000396:	f1c4 040c 	rsb	r4, r4, #12
 800039a:	f1c4 0220 	rsb	r2, r4, #32
 800039e:	fa20 f002 	lsr.w	r0, r0, r2
 80003a2:	fa01 f304 	lsl.w	r3, r1, r4
 80003a6:	ea40 0003 	orr.w	r0, r0, r3
 80003aa:	4629      	mov	r1, r5
 80003ac:	bd30      	pop	{r4, r5, pc}
 80003ae:	fa21 f004 	lsr.w	r0, r1, r4
 80003b2:	4629      	mov	r1, r5
 80003b4:	bd30      	pop	{r4, r5, pc}
 80003b6:	f094 0f00 	teq	r4, #0
 80003ba:	f483 1380 	eor.w	r3, r3, #1048576	; 0x100000
 80003be:	bf06      	itte	eq
 80003c0:	f481 1180 	eoreq.w	r1, r1, #1048576	; 0x100000
 80003c4:	3401      	addeq	r4, #1
 80003c6:	3d01      	subne	r5, #1
 80003c8:	e74e      	b.n	8000268 <__adddf3+0x8c>
 80003ca:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 80003ce:	bf18      	it	ne
 80003d0:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 80003d4:	d029      	beq.n	800042a <__adddf3+0x24e>
 80003d6:	ea94 0f05 	teq	r4, r5
 80003da:	bf08      	it	eq
 80003dc:	ea90 0f02 	teqeq	r0, r2
 80003e0:	d005      	beq.n	80003ee <__adddf3+0x212>
 80003e2:	ea54 0c00 	orrs.w	ip, r4, r0
 80003e6:	bf04      	itt	eq
 80003e8:	4619      	moveq	r1, r3
 80003ea:	4610      	moveq	r0, r2
 80003ec:	bd30      	pop	{r4, r5, pc}
 80003ee:	ea91 0f03 	teq	r1, r3
 80003f2:	bf1e      	ittt	ne
 80003f4:	2100      	movne	r1, #0
 80003f6:	2000      	movne	r0, #0
 80003f8:	bd30      	popne	{r4, r5, pc}
 80003fa:	ea5f 5c54 	movs.w	ip, r4, lsr #21
 80003fe:	d105      	bne.n	800040c <__adddf3+0x230>
 8000400:	0040      	lsls	r0, r0, #1
 8000402:	4149      	adcs	r1, r1
 8000404:	bf28      	it	cs
 8000406:	f041 4100 	orrcs.w	r1, r1, #2147483648	; 0x80000000
 800040a:	bd30      	pop	{r4, r5, pc}
 800040c:	f514 0480 	adds.w	r4, r4, #4194304	; 0x400000
 8000410:	bf3c      	itt	cc
 8000412:	f501 1180 	addcc.w	r1, r1, #1048576	; 0x100000
 8000416:	bd30      	popcc	{r4, r5, pc}
 8000418:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 800041c:	f045 41fe 	orr.w	r1, r5, #2130706432	; 0x7f000000
 8000420:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8000424:	f04f 0000 	mov.w	r0, #0
 8000428:	bd30      	pop	{r4, r5, pc}
 800042a:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 800042e:	bf1a      	itte	ne
 8000430:	4619      	movne	r1, r3
 8000432:	4610      	movne	r0, r2
 8000434:	ea7f 5c65 	mvnseq.w	ip, r5, asr #21
 8000438:	bf1c      	itt	ne
 800043a:	460b      	movne	r3, r1
 800043c:	4602      	movne	r2, r0
 800043e:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 8000442:	bf06      	itte	eq
 8000444:	ea52 3503 	orrseq.w	r5, r2, r3, lsl #12
 8000448:	ea91 0f03 	teqeq	r1, r3
 800044c:	f441 2100 	orrne.w	r1, r1, #524288	; 0x80000
 8000450:	bd30      	pop	{r4, r5, pc}
 8000452:	bf00      	nop

08000454 <__aeabi_ui2d>:
 8000454:	f090 0f00 	teq	r0, #0
 8000458:	bf04      	itt	eq
 800045a:	2100      	moveq	r1, #0
 800045c:	4770      	bxeq	lr
 800045e:	b530      	push	{r4, r5, lr}
 8000460:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8000464:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8000468:	f04f 0500 	mov.w	r5, #0
 800046c:	f04f 0100 	mov.w	r1, #0
 8000470:	e750      	b.n	8000314 <__adddf3+0x138>
 8000472:	bf00      	nop

08000474 <__aeabi_i2d>:
 8000474:	f090 0f00 	teq	r0, #0
 8000478:	bf04      	itt	eq
 800047a:	2100      	moveq	r1, #0
 800047c:	4770      	bxeq	lr
 800047e:	b530      	push	{r4, r5, lr}
 8000480:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8000484:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8000488:	f010 4500 	ands.w	r5, r0, #2147483648	; 0x80000000
 800048c:	bf48      	it	mi
 800048e:	4240      	negmi	r0, r0
 8000490:	f04f 0100 	mov.w	r1, #0
 8000494:	e73e      	b.n	8000314 <__adddf3+0x138>
 8000496:	bf00      	nop

08000498 <__aeabi_f2d>:
 8000498:	0042      	lsls	r2, r0, #1
 800049a:	ea4f 01e2 	mov.w	r1, r2, asr #3
 800049e:	ea4f 0131 	mov.w	r1, r1, rrx
 80004a2:	ea4f 7002 	mov.w	r0, r2, lsl #28
 80004a6:	bf1f      	itttt	ne
 80004a8:	f012 437f 	andsne.w	r3, r2, #4278190080	; 0xff000000
 80004ac:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 80004b0:	f081 5160 	eorne.w	r1, r1, #939524096	; 0x38000000
 80004b4:	4770      	bxne	lr
 80004b6:	f032 427f 	bics.w	r2, r2, #4278190080	; 0xff000000
 80004ba:	bf08      	it	eq
 80004bc:	4770      	bxeq	lr
 80004be:	f093 4f7f 	teq	r3, #4278190080	; 0xff000000
 80004c2:	bf04      	itt	eq
 80004c4:	f441 2100 	orreq.w	r1, r1, #524288	; 0x80000
 80004c8:	4770      	bxeq	lr
 80004ca:	b530      	push	{r4, r5, lr}
 80004cc:	f44f 7460 	mov.w	r4, #896	; 0x380
 80004d0:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 80004d4:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 80004d8:	e71c      	b.n	8000314 <__adddf3+0x138>
 80004da:	bf00      	nop

080004dc <__aeabi_ul2d>:
 80004dc:	ea50 0201 	orrs.w	r2, r0, r1
 80004e0:	bf08      	it	eq
 80004e2:	4770      	bxeq	lr
 80004e4:	b530      	push	{r4, r5, lr}
 80004e6:	f04f 0500 	mov.w	r5, #0
 80004ea:	e00a      	b.n	8000502 <__aeabi_l2d+0x16>

080004ec <__aeabi_l2d>:
 80004ec:	ea50 0201 	orrs.w	r2, r0, r1
 80004f0:	bf08      	it	eq
 80004f2:	4770      	bxeq	lr
 80004f4:	b530      	push	{r4, r5, lr}
 80004f6:	f011 4500 	ands.w	r5, r1, #2147483648	; 0x80000000
 80004fa:	d502      	bpl.n	8000502 <__aeabi_l2d+0x16>
 80004fc:	4240      	negs	r0, r0
 80004fe:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 8000502:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8000506:	f104 0432 	add.w	r4, r4, #50	; 0x32
 800050a:	ea5f 5c91 	movs.w	ip, r1, lsr #22
 800050e:	f43f aed8 	beq.w	80002c2 <__adddf3+0xe6>
 8000512:	f04f 0203 	mov.w	r2, #3
 8000516:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 800051a:	bf18      	it	ne
 800051c:	3203      	addne	r2, #3
 800051e:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 8000522:	bf18      	it	ne
 8000524:	3203      	addne	r2, #3
 8000526:	eb02 02dc 	add.w	r2, r2, ip, lsr #3
 800052a:	f1c2 0320 	rsb	r3, r2, #32
 800052e:	fa00 fc03 	lsl.w	ip, r0, r3
 8000532:	fa20 f002 	lsr.w	r0, r0, r2
 8000536:	fa01 fe03 	lsl.w	lr, r1, r3
 800053a:	ea40 000e 	orr.w	r0, r0, lr
 800053e:	fa21 f102 	lsr.w	r1, r1, r2
 8000542:	4414      	add	r4, r2
 8000544:	e6bd      	b.n	80002c2 <__adddf3+0xe6>
 8000546:	bf00      	nop

08000548 <__aeabi_dmul>:
 8000548:	b570      	push	{r4, r5, r6, lr}
 800054a:	f04f 0cff 	mov.w	ip, #255	; 0xff
 800054e:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 8000552:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 8000556:	bf1d      	ittte	ne
 8000558:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 800055c:	ea94 0f0c 	teqne	r4, ip
 8000560:	ea95 0f0c 	teqne	r5, ip
 8000564:	f000 f8de 	bleq	8000724 <__aeabi_dmul+0x1dc>
 8000568:	442c      	add	r4, r5
 800056a:	ea81 0603 	eor.w	r6, r1, r3
 800056e:	ea21 514c 	bic.w	r1, r1, ip, lsl #21
 8000572:	ea23 534c 	bic.w	r3, r3, ip, lsl #21
 8000576:	ea50 3501 	orrs.w	r5, r0, r1, lsl #12
 800057a:	bf18      	it	ne
 800057c:	ea52 3503 	orrsne.w	r5, r2, r3, lsl #12
 8000580:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8000584:	f443 1380 	orr.w	r3, r3, #1048576	; 0x100000
 8000588:	d038      	beq.n	80005fc <__aeabi_dmul+0xb4>
 800058a:	fba0 ce02 	umull	ip, lr, r0, r2
 800058e:	f04f 0500 	mov.w	r5, #0
 8000592:	fbe1 e502 	umlal	lr, r5, r1, r2
 8000596:	f006 4200 	and.w	r2, r6, #2147483648	; 0x80000000
 800059a:	fbe0 e503 	umlal	lr, r5, r0, r3
 800059e:	f04f 0600 	mov.w	r6, #0
 80005a2:	fbe1 5603 	umlal	r5, r6, r1, r3
 80005a6:	f09c 0f00 	teq	ip, #0
 80005aa:	bf18      	it	ne
 80005ac:	f04e 0e01 	orrne.w	lr, lr, #1
 80005b0:	f1a4 04ff 	sub.w	r4, r4, #255	; 0xff
 80005b4:	f5b6 7f00 	cmp.w	r6, #512	; 0x200
 80005b8:	f564 7440 	sbc.w	r4, r4, #768	; 0x300
 80005bc:	d204      	bcs.n	80005c8 <__aeabi_dmul+0x80>
 80005be:	ea5f 0e4e 	movs.w	lr, lr, lsl #1
 80005c2:	416d      	adcs	r5, r5
 80005c4:	eb46 0606 	adc.w	r6, r6, r6
 80005c8:	ea42 21c6 	orr.w	r1, r2, r6, lsl #11
 80005cc:	ea41 5155 	orr.w	r1, r1, r5, lsr #21
 80005d0:	ea4f 20c5 	mov.w	r0, r5, lsl #11
 80005d4:	ea40 505e 	orr.w	r0, r0, lr, lsr #21
 80005d8:	ea4f 2ece 	mov.w	lr, lr, lsl #11
 80005dc:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 80005e0:	bf88      	it	hi
 80005e2:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 80005e6:	d81e      	bhi.n	8000626 <__aeabi_dmul+0xde>
 80005e8:	f1be 4f00 	cmp.w	lr, #2147483648	; 0x80000000
 80005ec:	bf08      	it	eq
 80005ee:	ea5f 0e50 	movseq.w	lr, r0, lsr #1
 80005f2:	f150 0000 	adcs.w	r0, r0, #0
 80005f6:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 80005fa:	bd70      	pop	{r4, r5, r6, pc}
 80005fc:	f006 4600 	and.w	r6, r6, #2147483648	; 0x80000000
 8000600:	ea46 0101 	orr.w	r1, r6, r1
 8000604:	ea40 0002 	orr.w	r0, r0, r2
 8000608:	ea81 0103 	eor.w	r1, r1, r3
 800060c:	ebb4 045c 	subs.w	r4, r4, ip, lsr #1
 8000610:	bfc2      	ittt	gt
 8000612:	ebd4 050c 	rsbsgt	r5, r4, ip
 8000616:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 800061a:	bd70      	popgt	{r4, r5, r6, pc}
 800061c:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8000620:	f04f 0e00 	mov.w	lr, #0
 8000624:	3c01      	subs	r4, #1
 8000626:	f300 80ab 	bgt.w	8000780 <__aeabi_dmul+0x238>
 800062a:	f114 0f36 	cmn.w	r4, #54	; 0x36
 800062e:	bfde      	ittt	le
 8000630:	2000      	movle	r0, #0
 8000632:	f001 4100 	andle.w	r1, r1, #2147483648	; 0x80000000
 8000636:	bd70      	pople	{r4, r5, r6, pc}
 8000638:	f1c4 0400 	rsb	r4, r4, #0
 800063c:	3c20      	subs	r4, #32
 800063e:	da35      	bge.n	80006ac <__aeabi_dmul+0x164>
 8000640:	340c      	adds	r4, #12
 8000642:	dc1b      	bgt.n	800067c <__aeabi_dmul+0x134>
 8000644:	f104 0414 	add.w	r4, r4, #20
 8000648:	f1c4 0520 	rsb	r5, r4, #32
 800064c:	fa00 f305 	lsl.w	r3, r0, r5
 8000650:	fa20 f004 	lsr.w	r0, r0, r4
 8000654:	fa01 f205 	lsl.w	r2, r1, r5
 8000658:	ea40 0002 	orr.w	r0, r0, r2
 800065c:	f001 4200 	and.w	r2, r1, #2147483648	; 0x80000000
 8000660:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 8000664:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 8000668:	fa21 f604 	lsr.w	r6, r1, r4
 800066c:	eb42 0106 	adc.w	r1, r2, r6
 8000670:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8000674:	bf08      	it	eq
 8000676:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 800067a:	bd70      	pop	{r4, r5, r6, pc}
 800067c:	f1c4 040c 	rsb	r4, r4, #12
 8000680:	f1c4 0520 	rsb	r5, r4, #32
 8000684:	fa00 f304 	lsl.w	r3, r0, r4
 8000688:	fa20 f005 	lsr.w	r0, r0, r5
 800068c:	fa01 f204 	lsl.w	r2, r1, r4
 8000690:	ea40 0002 	orr.w	r0, r0, r2
 8000694:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8000698:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 800069c:	f141 0100 	adc.w	r1, r1, #0
 80006a0:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80006a4:	bf08      	it	eq
 80006a6:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80006aa:	bd70      	pop	{r4, r5, r6, pc}
 80006ac:	f1c4 0520 	rsb	r5, r4, #32
 80006b0:	fa00 f205 	lsl.w	r2, r0, r5
 80006b4:	ea4e 0e02 	orr.w	lr, lr, r2
 80006b8:	fa20 f304 	lsr.w	r3, r0, r4
 80006bc:	fa01 f205 	lsl.w	r2, r1, r5
 80006c0:	ea43 0302 	orr.w	r3, r3, r2
 80006c4:	fa21 f004 	lsr.w	r0, r1, r4
 80006c8:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80006cc:	fa21 f204 	lsr.w	r2, r1, r4
 80006d0:	ea20 0002 	bic.w	r0, r0, r2
 80006d4:	eb00 70d3 	add.w	r0, r0, r3, lsr #31
 80006d8:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80006dc:	bf08      	it	eq
 80006de:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80006e2:	bd70      	pop	{r4, r5, r6, pc}
 80006e4:	f094 0f00 	teq	r4, #0
 80006e8:	d10f      	bne.n	800070a <__aeabi_dmul+0x1c2>
 80006ea:	f001 4600 	and.w	r6, r1, #2147483648	; 0x80000000
 80006ee:	0040      	lsls	r0, r0, #1
 80006f0:	eb41 0101 	adc.w	r1, r1, r1
 80006f4:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80006f8:	bf08      	it	eq
 80006fa:	3c01      	subeq	r4, #1
 80006fc:	d0f7      	beq.n	80006ee <__aeabi_dmul+0x1a6>
 80006fe:	ea41 0106 	orr.w	r1, r1, r6
 8000702:	f095 0f00 	teq	r5, #0
 8000706:	bf18      	it	ne
 8000708:	4770      	bxne	lr
 800070a:	f003 4600 	and.w	r6, r3, #2147483648	; 0x80000000
 800070e:	0052      	lsls	r2, r2, #1
 8000710:	eb43 0303 	adc.w	r3, r3, r3
 8000714:	f413 1f80 	tst.w	r3, #1048576	; 0x100000
 8000718:	bf08      	it	eq
 800071a:	3d01      	subeq	r5, #1
 800071c:	d0f7      	beq.n	800070e <__aeabi_dmul+0x1c6>
 800071e:	ea43 0306 	orr.w	r3, r3, r6
 8000722:	4770      	bx	lr
 8000724:	ea94 0f0c 	teq	r4, ip
 8000728:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 800072c:	bf18      	it	ne
 800072e:	ea95 0f0c 	teqne	r5, ip
 8000732:	d00c      	beq.n	800074e <__aeabi_dmul+0x206>
 8000734:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8000738:	bf18      	it	ne
 800073a:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 800073e:	d1d1      	bne.n	80006e4 <__aeabi_dmul+0x19c>
 8000740:	ea81 0103 	eor.w	r1, r1, r3
 8000744:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8000748:	f04f 0000 	mov.w	r0, #0
 800074c:	bd70      	pop	{r4, r5, r6, pc}
 800074e:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8000752:	bf06      	itte	eq
 8000754:	4610      	moveq	r0, r2
 8000756:	4619      	moveq	r1, r3
 8000758:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 800075c:	d019      	beq.n	8000792 <__aeabi_dmul+0x24a>
 800075e:	ea94 0f0c 	teq	r4, ip
 8000762:	d102      	bne.n	800076a <__aeabi_dmul+0x222>
 8000764:	ea50 3601 	orrs.w	r6, r0, r1, lsl #12
 8000768:	d113      	bne.n	8000792 <__aeabi_dmul+0x24a>
 800076a:	ea95 0f0c 	teq	r5, ip
 800076e:	d105      	bne.n	800077c <__aeabi_dmul+0x234>
 8000770:	ea52 3603 	orrs.w	r6, r2, r3, lsl #12
 8000774:	bf1c      	itt	ne
 8000776:	4610      	movne	r0, r2
 8000778:	4619      	movne	r1, r3
 800077a:	d10a      	bne.n	8000792 <__aeabi_dmul+0x24a>
 800077c:	ea81 0103 	eor.w	r1, r1, r3
 8000780:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8000784:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 8000788:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 800078c:	f04f 0000 	mov.w	r0, #0
 8000790:	bd70      	pop	{r4, r5, r6, pc}
 8000792:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 8000796:	f441 0178 	orr.w	r1, r1, #16252928	; 0xf80000
 800079a:	bd70      	pop	{r4, r5, r6, pc}

0800079c <__aeabi_ddiv>:
 800079c:	b570      	push	{r4, r5, r6, lr}
 800079e:	f04f 0cff 	mov.w	ip, #255	; 0xff
 80007a2:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 80007a6:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 80007aa:	bf1d      	ittte	ne
 80007ac:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 80007b0:	ea94 0f0c 	teqne	r4, ip
 80007b4:	ea95 0f0c 	teqne	r5, ip
 80007b8:	f000 f8a7 	bleq	800090a <__aeabi_ddiv+0x16e>
 80007bc:	eba4 0405 	sub.w	r4, r4, r5
 80007c0:	ea81 0e03 	eor.w	lr, r1, r3
 80007c4:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 80007c8:	ea4f 3101 	mov.w	r1, r1, lsl #12
 80007cc:	f000 8088 	beq.w	80008e0 <__aeabi_ddiv+0x144>
 80007d0:	ea4f 3303 	mov.w	r3, r3, lsl #12
 80007d4:	f04f 5580 	mov.w	r5, #268435456	; 0x10000000
 80007d8:	ea45 1313 	orr.w	r3, r5, r3, lsr #4
 80007dc:	ea43 6312 	orr.w	r3, r3, r2, lsr #24
 80007e0:	ea4f 2202 	mov.w	r2, r2, lsl #8
 80007e4:	ea45 1511 	orr.w	r5, r5, r1, lsr #4
 80007e8:	ea45 6510 	orr.w	r5, r5, r0, lsr #24
 80007ec:	ea4f 2600 	mov.w	r6, r0, lsl #8
 80007f0:	f00e 4100 	and.w	r1, lr, #2147483648	; 0x80000000
 80007f4:	429d      	cmp	r5, r3
 80007f6:	bf08      	it	eq
 80007f8:	4296      	cmpeq	r6, r2
 80007fa:	f144 04fd 	adc.w	r4, r4, #253	; 0xfd
 80007fe:	f504 7440 	add.w	r4, r4, #768	; 0x300
 8000802:	d202      	bcs.n	800080a <__aeabi_ddiv+0x6e>
 8000804:	085b      	lsrs	r3, r3, #1
 8000806:	ea4f 0232 	mov.w	r2, r2, rrx
 800080a:	1ab6      	subs	r6, r6, r2
 800080c:	eb65 0503 	sbc.w	r5, r5, r3
 8000810:	085b      	lsrs	r3, r3, #1
 8000812:	ea4f 0232 	mov.w	r2, r2, rrx
 8000816:	f44f 1080 	mov.w	r0, #1048576	; 0x100000
 800081a:	f44f 2c00 	mov.w	ip, #524288	; 0x80000
 800081e:	ebb6 0e02 	subs.w	lr, r6, r2
 8000822:	eb75 0e03 	sbcs.w	lr, r5, r3
 8000826:	bf22      	ittt	cs
 8000828:	1ab6      	subcs	r6, r6, r2
 800082a:	4675      	movcs	r5, lr
 800082c:	ea40 000c 	orrcs.w	r0, r0, ip
 8000830:	085b      	lsrs	r3, r3, #1
 8000832:	ea4f 0232 	mov.w	r2, r2, rrx
 8000836:	ebb6 0e02 	subs.w	lr, r6, r2
 800083a:	eb75 0e03 	sbcs.w	lr, r5, r3
 800083e:	bf22      	ittt	cs
 8000840:	1ab6      	subcs	r6, r6, r2
 8000842:	4675      	movcs	r5, lr
 8000844:	ea40 005c 	orrcs.w	r0, r0, ip, lsr #1
 8000848:	085b      	lsrs	r3, r3, #1
 800084a:	ea4f 0232 	mov.w	r2, r2, rrx
 800084e:	ebb6 0e02 	subs.w	lr, r6, r2
 8000852:	eb75 0e03 	sbcs.w	lr, r5, r3
 8000856:	bf22      	ittt	cs
 8000858:	1ab6      	subcs	r6, r6, r2
 800085a:	4675      	movcs	r5, lr
 800085c:	ea40 009c 	orrcs.w	r0, r0, ip, lsr #2
 8000860:	085b      	lsrs	r3, r3, #1
 8000862:	ea4f 0232 	mov.w	r2, r2, rrx
 8000866:	ebb6 0e02 	subs.w	lr, r6, r2
 800086a:	eb75 0e03 	sbcs.w	lr, r5, r3
 800086e:	bf22      	ittt	cs
 8000870:	1ab6      	subcs	r6, r6, r2
 8000872:	4675      	movcs	r5, lr
 8000874:	ea40 00dc 	orrcs.w	r0, r0, ip, lsr #3
 8000878:	ea55 0e06 	orrs.w	lr, r5, r6
 800087c:	d018      	beq.n	80008b0 <__aeabi_ddiv+0x114>
 800087e:	ea4f 1505 	mov.w	r5, r5, lsl #4
 8000882:	ea45 7516 	orr.w	r5, r5, r6, lsr #28
 8000886:	ea4f 1606 	mov.w	r6, r6, lsl #4
 800088a:	ea4f 03c3 	mov.w	r3, r3, lsl #3
 800088e:	ea43 7352 	orr.w	r3, r3, r2, lsr #29
 8000892:	ea4f 02c2 	mov.w	r2, r2, lsl #3
 8000896:	ea5f 1c1c 	movs.w	ip, ip, lsr #4
 800089a:	d1c0      	bne.n	800081e <__aeabi_ddiv+0x82>
 800089c:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80008a0:	d10b      	bne.n	80008ba <__aeabi_ddiv+0x11e>
 80008a2:	ea41 0100 	orr.w	r1, r1, r0
 80008a6:	f04f 0000 	mov.w	r0, #0
 80008aa:	f04f 4c00 	mov.w	ip, #2147483648	; 0x80000000
 80008ae:	e7b6      	b.n	800081e <__aeabi_ddiv+0x82>
 80008b0:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80008b4:	bf04      	itt	eq
 80008b6:	4301      	orreq	r1, r0
 80008b8:	2000      	moveq	r0, #0
 80008ba:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 80008be:	bf88      	it	hi
 80008c0:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 80008c4:	f63f aeaf 	bhi.w	8000626 <__aeabi_dmul+0xde>
 80008c8:	ebb5 0c03 	subs.w	ip, r5, r3
 80008cc:	bf04      	itt	eq
 80008ce:	ebb6 0c02 	subseq.w	ip, r6, r2
 80008d2:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 80008d6:	f150 0000 	adcs.w	r0, r0, #0
 80008da:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 80008de:	bd70      	pop	{r4, r5, r6, pc}
 80008e0:	f00e 4e00 	and.w	lr, lr, #2147483648	; 0x80000000
 80008e4:	ea4e 3111 	orr.w	r1, lr, r1, lsr #12
 80008e8:	eb14 045c 	adds.w	r4, r4, ip, lsr #1
 80008ec:	bfc2      	ittt	gt
 80008ee:	ebd4 050c 	rsbsgt	r5, r4, ip
 80008f2:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 80008f6:	bd70      	popgt	{r4, r5, r6, pc}
 80008f8:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 80008fc:	f04f 0e00 	mov.w	lr, #0
 8000900:	3c01      	subs	r4, #1
 8000902:	e690      	b.n	8000626 <__aeabi_dmul+0xde>
 8000904:	ea45 0e06 	orr.w	lr, r5, r6
 8000908:	e68d      	b.n	8000626 <__aeabi_dmul+0xde>
 800090a:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 800090e:	ea94 0f0c 	teq	r4, ip
 8000912:	bf08      	it	eq
 8000914:	ea95 0f0c 	teqeq	r5, ip
 8000918:	f43f af3b 	beq.w	8000792 <__aeabi_dmul+0x24a>
 800091c:	ea94 0f0c 	teq	r4, ip
 8000920:	d10a      	bne.n	8000938 <__aeabi_ddiv+0x19c>
 8000922:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 8000926:	f47f af34 	bne.w	8000792 <__aeabi_dmul+0x24a>
 800092a:	ea95 0f0c 	teq	r5, ip
 800092e:	f47f af25 	bne.w	800077c <__aeabi_dmul+0x234>
 8000932:	4610      	mov	r0, r2
 8000934:	4619      	mov	r1, r3
 8000936:	e72c      	b.n	8000792 <__aeabi_dmul+0x24a>
 8000938:	ea95 0f0c 	teq	r5, ip
 800093c:	d106      	bne.n	800094c <__aeabi_ddiv+0x1b0>
 800093e:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 8000942:	f43f aefd 	beq.w	8000740 <__aeabi_dmul+0x1f8>
 8000946:	4610      	mov	r0, r2
 8000948:	4619      	mov	r1, r3
 800094a:	e722      	b.n	8000792 <__aeabi_dmul+0x24a>
 800094c:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8000950:	bf18      	it	ne
 8000952:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 8000956:	f47f aec5 	bne.w	80006e4 <__aeabi_dmul+0x19c>
 800095a:	ea50 0441 	orrs.w	r4, r0, r1, lsl #1
 800095e:	f47f af0d 	bne.w	800077c <__aeabi_dmul+0x234>
 8000962:	ea52 0543 	orrs.w	r5, r2, r3, lsl #1
 8000966:	f47f aeeb 	bne.w	8000740 <__aeabi_dmul+0x1f8>
 800096a:	e712      	b.n	8000792 <__aeabi_dmul+0x24a>

0800096c <strlen>:
 800096c:	4603      	mov	r3, r0
 800096e:	f813 2b01 	ldrb.w	r2, [r3], #1
 8000972:	2a00      	cmp	r2, #0
 8000974:	d1fb      	bne.n	800096e <strlen+0x2>
 8000976:	1a18      	subs	r0, r3, r0
 8000978:	3801      	subs	r0, #1
 800097a:	4770      	bx	lr

0800097c <__gedf2>:
 800097c:	f04f 3cff 	mov.w	ip, #4294967295
 8000980:	e006      	b.n	8000990 <__cmpdf2+0x4>
 8000982:	bf00      	nop

08000984 <__ledf2>:
 8000984:	f04f 0c01 	mov.w	ip, #1
 8000988:	e002      	b.n	8000990 <__cmpdf2+0x4>
 800098a:	bf00      	nop

0800098c <__cmpdf2>:
 800098c:	f04f 0c01 	mov.w	ip, #1
 8000990:	f84d cd04 	str.w	ip, [sp, #-4]!
 8000994:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 8000998:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 800099c:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 80009a0:	bf18      	it	ne
 80009a2:	ea7f 5c6c 	mvnsne.w	ip, ip, asr #21
 80009a6:	d01b      	beq.n	80009e0 <__cmpdf2+0x54>
 80009a8:	b001      	add	sp, #4
 80009aa:	ea50 0c41 	orrs.w	ip, r0, r1, lsl #1
 80009ae:	bf0c      	ite	eq
 80009b0:	ea52 0c43 	orrseq.w	ip, r2, r3, lsl #1
 80009b4:	ea91 0f03 	teqne	r1, r3
 80009b8:	bf02      	ittt	eq
 80009ba:	ea90 0f02 	teqeq	r0, r2
 80009be:	2000      	moveq	r0, #0
 80009c0:	4770      	bxeq	lr
 80009c2:	f110 0f00 	cmn.w	r0, #0
 80009c6:	ea91 0f03 	teq	r1, r3
 80009ca:	bf58      	it	pl
 80009cc:	4299      	cmppl	r1, r3
 80009ce:	bf08      	it	eq
 80009d0:	4290      	cmpeq	r0, r2
 80009d2:	bf2c      	ite	cs
 80009d4:	17d8      	asrcs	r0, r3, #31
 80009d6:	ea6f 70e3 	mvncc.w	r0, r3, asr #31
 80009da:	f040 0001 	orr.w	r0, r0, #1
 80009de:	4770      	bx	lr
 80009e0:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 80009e4:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80009e8:	d102      	bne.n	80009f0 <__cmpdf2+0x64>
 80009ea:	ea50 3c01 	orrs.w	ip, r0, r1, lsl #12
 80009ee:	d107      	bne.n	8000a00 <__cmpdf2+0x74>
 80009f0:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 80009f4:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80009f8:	d1d6      	bne.n	80009a8 <__cmpdf2+0x1c>
 80009fa:	ea52 3c03 	orrs.w	ip, r2, r3, lsl #12
 80009fe:	d0d3      	beq.n	80009a8 <__cmpdf2+0x1c>
 8000a00:	f85d 0b04 	ldr.w	r0, [sp], #4
 8000a04:	4770      	bx	lr
 8000a06:	bf00      	nop

08000a08 <__aeabi_cdrcmple>:
 8000a08:	4684      	mov	ip, r0
 8000a0a:	4610      	mov	r0, r2
 8000a0c:	4662      	mov	r2, ip
 8000a0e:	468c      	mov	ip, r1
 8000a10:	4619      	mov	r1, r3
 8000a12:	4663      	mov	r3, ip
 8000a14:	e000      	b.n	8000a18 <__aeabi_cdcmpeq>
 8000a16:	bf00      	nop

08000a18 <__aeabi_cdcmpeq>:
 8000a18:	b501      	push	{r0, lr}
 8000a1a:	f7ff ffb7 	bl	800098c <__cmpdf2>
 8000a1e:	2800      	cmp	r0, #0
 8000a20:	bf48      	it	mi
 8000a22:	f110 0f00 	cmnmi.w	r0, #0
 8000a26:	bd01      	pop	{r0, pc}

08000a28 <__aeabi_dcmpeq>:
 8000a28:	f84d ed08 	str.w	lr, [sp, #-8]!
 8000a2c:	f7ff fff4 	bl	8000a18 <__aeabi_cdcmpeq>
 8000a30:	bf0c      	ite	eq
 8000a32:	2001      	moveq	r0, #1
 8000a34:	2000      	movne	r0, #0
 8000a36:	f85d fb08 	ldr.w	pc, [sp], #8
 8000a3a:	bf00      	nop

08000a3c <__aeabi_dcmplt>:
 8000a3c:	f84d ed08 	str.w	lr, [sp, #-8]!
 8000a40:	f7ff ffea 	bl	8000a18 <__aeabi_cdcmpeq>
 8000a44:	bf34      	ite	cc
 8000a46:	2001      	movcc	r0, #1
 8000a48:	2000      	movcs	r0, #0
 8000a4a:	f85d fb08 	ldr.w	pc, [sp], #8
 8000a4e:	bf00      	nop

08000a50 <__aeabi_dcmple>:
 8000a50:	f84d ed08 	str.w	lr, [sp, #-8]!
 8000a54:	f7ff ffe0 	bl	8000a18 <__aeabi_cdcmpeq>
 8000a58:	bf94      	ite	ls
 8000a5a:	2001      	movls	r0, #1
 8000a5c:	2000      	movhi	r0, #0
 8000a5e:	f85d fb08 	ldr.w	pc, [sp], #8
 8000a62:	bf00      	nop

08000a64 <__aeabi_dcmpge>:
 8000a64:	f84d ed08 	str.w	lr, [sp, #-8]!
 8000a68:	f7ff ffce 	bl	8000a08 <__aeabi_cdrcmple>
 8000a6c:	bf94      	ite	ls
 8000a6e:	2001      	movls	r0, #1
 8000a70:	2000      	movhi	r0, #0
 8000a72:	f85d fb08 	ldr.w	pc, [sp], #8
 8000a76:	bf00      	nop

08000a78 <__aeabi_dcmpgt>:
 8000a78:	f84d ed08 	str.w	lr, [sp, #-8]!
 8000a7c:	f7ff ffc4 	bl	8000a08 <__aeabi_cdrcmple>
 8000a80:	bf34      	ite	cc
 8000a82:	2001      	movcc	r0, #1
 8000a84:	2000      	movcs	r0, #0
 8000a86:	f85d fb08 	ldr.w	pc, [sp], #8
 8000a8a:	bf00      	nop

08000a8c <__aeabi_dcmpun>:
 8000a8c:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 8000a90:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 8000a94:	d102      	bne.n	8000a9c <__aeabi_dcmpun+0x10>
 8000a96:	ea50 3c01 	orrs.w	ip, r0, r1, lsl #12
 8000a9a:	d10a      	bne.n	8000ab2 <__aeabi_dcmpun+0x26>
 8000a9c:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 8000aa0:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 8000aa4:	d102      	bne.n	8000aac <__aeabi_dcmpun+0x20>
 8000aa6:	ea52 3c03 	orrs.w	ip, r2, r3, lsl #12
 8000aaa:	d102      	bne.n	8000ab2 <__aeabi_dcmpun+0x26>
 8000aac:	f04f 0000 	mov.w	r0, #0
 8000ab0:	4770      	bx	lr
 8000ab2:	f04f 0001 	mov.w	r0, #1
 8000ab6:	4770      	bx	lr

08000ab8 <__aeabi_d2iz>:
 8000ab8:	ea4f 0241 	mov.w	r2, r1, lsl #1
 8000abc:	f512 1200 	adds.w	r2, r2, #2097152	; 0x200000
 8000ac0:	d215      	bcs.n	8000aee <__aeabi_d2iz+0x36>
 8000ac2:	d511      	bpl.n	8000ae8 <__aeabi_d2iz+0x30>
 8000ac4:	f46f 7378 	mvn.w	r3, #992	; 0x3e0
 8000ac8:	ebb3 5262 	subs.w	r2, r3, r2, asr #21
 8000acc:	d912      	bls.n	8000af4 <__aeabi_d2iz+0x3c>
 8000ace:	ea4f 23c1 	mov.w	r3, r1, lsl #11
 8000ad2:	f043 4300 	orr.w	r3, r3, #2147483648	; 0x80000000
 8000ad6:	ea43 5350 	orr.w	r3, r3, r0, lsr #21
 8000ada:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 8000ade:	fa23 f002 	lsr.w	r0, r3, r2
 8000ae2:	bf18      	it	ne
 8000ae4:	4240      	negne	r0, r0
 8000ae6:	4770      	bx	lr
 8000ae8:	f04f 0000 	mov.w	r0, #0
 8000aec:	4770      	bx	lr
 8000aee:	ea50 3001 	orrs.w	r0, r0, r1, lsl #12
 8000af2:	d105      	bne.n	8000b00 <__aeabi_d2iz+0x48>
 8000af4:	f011 4000 	ands.w	r0, r1, #2147483648	; 0x80000000
 8000af8:	bf08      	it	eq
 8000afa:	f06f 4000 	mvneq.w	r0, #2147483648	; 0x80000000
 8000afe:	4770      	bx	lr
 8000b00:	f04f 0000 	mov.w	r0, #0
 8000b04:	4770      	bx	lr
 8000b06:	bf00      	nop

08000b08 <Reset_Handler>:
 8000b08:	f8df d034 	ldr.w	sp, [pc, #52]	; 8000b40 <LoopFillZerobss+0x14>
 8000b0c:	2100      	movs	r1, #0
 8000b0e:	e003      	b.n	8000b18 <LoopCopyDataInit>

08000b10 <CopyDataInit>:
 8000b10:	4b0c      	ldr	r3, [pc, #48]	; (8000b44 <LoopFillZerobss+0x18>)
 8000b12:	585b      	ldr	r3, [r3, r1]
 8000b14:	5043      	str	r3, [r0, r1]
 8000b16:	3104      	adds	r1, #4

08000b18 <LoopCopyDataInit>:
 8000b18:	480b      	ldr	r0, [pc, #44]	; (8000b48 <LoopFillZerobss+0x1c>)
 8000b1a:	4b0c      	ldr	r3, [pc, #48]	; (8000b4c <LoopFillZerobss+0x20>)
 8000b1c:	1842      	adds	r2, r0, r1
 8000b1e:	429a      	cmp	r2, r3
 8000b20:	d3f6      	bcc.n	8000b10 <CopyDataInit>
 8000b22:	4a0b      	ldr	r2, [pc, #44]	; (8000b50 <LoopFillZerobss+0x24>)
 8000b24:	e002      	b.n	8000b2c <LoopFillZerobss>

08000b26 <FillZerobss>:
 8000b26:	2300      	movs	r3, #0
 8000b28:	f842 3b04 	str.w	r3, [r2], #4

08000b2c <LoopFillZerobss>:
 8000b2c:	4b09      	ldr	r3, [pc, #36]	; (8000b54 <LoopFillZerobss+0x28>)
 8000b2e:	429a      	cmp	r2, r3
 8000b30:	d3f9      	bcc.n	8000b26 <FillZerobss>
 8000b32:	f009 fa3f 	bl	8009fb4 <SystemInit>
 8000b36:	f009 febd 	bl	800a8b4 <__libc_init_array>
 8000b3a:	f000 f9f5 	bl	8000f28 <main>
 8000b3e:	4770      	bx	lr
 8000b40:	20020000 	.word	0x20020000
 8000b44:	0800d9d0 	.word	0x0800d9d0
 8000b48:	20000000 	.word	0x20000000
 8000b4c:	20000250 	.word	0x20000250
 8000b50:	20000250 	.word	0x20000250
 8000b54:	20019348 	.word	0x20019348

08000b58 <ADC_IRQHandler>:
 8000b58:	e7fe      	b.n	8000b58 <ADC_IRQHandler>
	...

08000b5c <NVIC_SetPriorityGrouping>:
 8000b5c:	b480      	push	{r7}
 8000b5e:	b085      	sub	sp, #20
 8000b60:	af00      	add	r7, sp, #0
 8000b62:	6078      	str	r0, [r7, #4]
 8000b64:	687b      	ldr	r3, [r7, #4]
 8000b66:	f003 0307 	and.w	r3, r3, #7
 8000b6a:	60fb      	str	r3, [r7, #12]
 8000b6c:	4b0c      	ldr	r3, [pc, #48]	; (8000ba0 <NVIC_SetPriorityGrouping+0x44>)
 8000b6e:	68db      	ldr	r3, [r3, #12]
 8000b70:	60bb      	str	r3, [r7, #8]
 8000b72:	68ba      	ldr	r2, [r7, #8]
 8000b74:	f64f 03ff 	movw	r3, #63743	; 0xf8ff
 8000b78:	4013      	ands	r3, r2
 8000b7a:	60bb      	str	r3, [r7, #8]
 8000b7c:	68fb      	ldr	r3, [r7, #12]
 8000b7e:	021a      	lsls	r2, r3, #8
 8000b80:	68bb      	ldr	r3, [r7, #8]
 8000b82:	4313      	orrs	r3, r2
 8000b84:	f043 63bf 	orr.w	r3, r3, #100139008	; 0x5f80000
 8000b88:	f443 3300 	orr.w	r3, r3, #131072	; 0x20000
 8000b8c:	60bb      	str	r3, [r7, #8]
 8000b8e:	4a04      	ldr	r2, [pc, #16]	; (8000ba0 <NVIC_SetPriorityGrouping+0x44>)
 8000b90:	68bb      	ldr	r3, [r7, #8]
 8000b92:	60d3      	str	r3, [r2, #12]
 8000b94:	bf00      	nop
 8000b96:	3714      	adds	r7, #20
 8000b98:	46bd      	mov	sp, r7
 8000b9a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000b9e:	4770      	bx	lr
 8000ba0:	e000ed00 	.word	0xe000ed00

08000ba4 <NVIC_SetPriority>:
 8000ba4:	b480      	push	{r7}
 8000ba6:	b083      	sub	sp, #12
 8000ba8:	af00      	add	r7, sp, #0
 8000baa:	4603      	mov	r3, r0
 8000bac:	6039      	str	r1, [r7, #0]
 8000bae:	71fb      	strb	r3, [r7, #7]
 8000bb0:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8000bb4:	2b00      	cmp	r3, #0
 8000bb6:	da0b      	bge.n	8000bd0 <NVIC_SetPriority+0x2c>
 8000bb8:	683b      	ldr	r3, [r7, #0]
 8000bba:	b2da      	uxtb	r2, r3
 8000bbc:	490c      	ldr	r1, [pc, #48]	; (8000bf0 <NVIC_SetPriority+0x4c>)
 8000bbe:	79fb      	ldrb	r3, [r7, #7]
 8000bc0:	f003 030f 	and.w	r3, r3, #15
 8000bc4:	3b04      	subs	r3, #4
 8000bc6:	0112      	lsls	r2, r2, #4
 8000bc8:	b2d2      	uxtb	r2, r2
 8000bca:	440b      	add	r3, r1
 8000bcc:	761a      	strb	r2, [r3, #24]
 8000bce:	e009      	b.n	8000be4 <NVIC_SetPriority+0x40>
 8000bd0:	683b      	ldr	r3, [r7, #0]
 8000bd2:	b2da      	uxtb	r2, r3
 8000bd4:	4907      	ldr	r1, [pc, #28]	; (8000bf4 <NVIC_SetPriority+0x50>)
 8000bd6:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8000bda:	0112      	lsls	r2, r2, #4
 8000bdc:	b2d2      	uxtb	r2, r2
 8000bde:	440b      	add	r3, r1
 8000be0:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8000be4:	bf00      	nop
 8000be6:	370c      	adds	r7, #12
 8000be8:	46bd      	mov	sp, r7
 8000bea:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000bee:	4770      	bx	lr
 8000bf0:	e000ed00 	.word	0xe000ed00
 8000bf4:	e000e100 	.word	0xe000e100

08000bf8 <SysTick_Config>:
 8000bf8:	b580      	push	{r7, lr}
 8000bfa:	b082      	sub	sp, #8
 8000bfc:	af00      	add	r7, sp, #0
 8000bfe:	6078      	str	r0, [r7, #4]
 8000c00:	687b      	ldr	r3, [r7, #4]
 8000c02:	3b01      	subs	r3, #1
 8000c04:	f1b3 7f80 	cmp.w	r3, #16777216	; 0x1000000
 8000c08:	d301      	bcc.n	8000c0e <SysTick_Config+0x16>
 8000c0a:	2301      	movs	r3, #1
 8000c0c:	e00f      	b.n	8000c2e <SysTick_Config+0x36>
 8000c0e:	4a0a      	ldr	r2, [pc, #40]	; (8000c38 <SysTick_Config+0x40>)
 8000c10:	687b      	ldr	r3, [r7, #4]
 8000c12:	3b01      	subs	r3, #1
 8000c14:	6053      	str	r3, [r2, #4]
 8000c16:	210f      	movs	r1, #15
 8000c18:	f04f 30ff 	mov.w	r0, #4294967295
 8000c1c:	f7ff ffc2 	bl	8000ba4 <NVIC_SetPriority>
 8000c20:	4b05      	ldr	r3, [pc, #20]	; (8000c38 <SysTick_Config+0x40>)
 8000c22:	2200      	movs	r2, #0
 8000c24:	609a      	str	r2, [r3, #8]
 8000c26:	4b04      	ldr	r3, [pc, #16]	; (8000c38 <SysTick_Config+0x40>)
 8000c28:	2207      	movs	r2, #7
 8000c2a:	601a      	str	r2, [r3, #0]
 8000c2c:	2300      	movs	r3, #0
 8000c2e:	4618      	mov	r0, r3
 8000c30:	3708      	adds	r7, #8
 8000c32:	46bd      	mov	sp, r7
 8000c34:	bd80      	pop	{r7, pc}
 8000c36:	bf00      	nop
 8000c38:	e000e010 	.word	0xe000e010

08000c3c <LL_RCC_HSE_Enable>:
 8000c3c:	b480      	push	{r7}
 8000c3e:	af00      	add	r7, sp, #0
 8000c40:	4b05      	ldr	r3, [pc, #20]	; (8000c58 <LL_RCC_HSE_Enable+0x1c>)
 8000c42:	681b      	ldr	r3, [r3, #0]
 8000c44:	4a04      	ldr	r2, [pc, #16]	; (8000c58 <LL_RCC_HSE_Enable+0x1c>)
 8000c46:	f443 3380 	orr.w	r3, r3, #65536	; 0x10000
 8000c4a:	6013      	str	r3, [r2, #0]
 8000c4c:	bf00      	nop
 8000c4e:	46bd      	mov	sp, r7
 8000c50:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000c54:	4770      	bx	lr
 8000c56:	bf00      	nop
 8000c58:	40023800 	.word	0x40023800

08000c5c <LL_RCC_HSE_IsReady>:
 8000c5c:	b480      	push	{r7}
 8000c5e:	af00      	add	r7, sp, #0
 8000c60:	4b07      	ldr	r3, [pc, #28]	; (8000c80 <LL_RCC_HSE_IsReady+0x24>)
 8000c62:	681b      	ldr	r3, [r3, #0]
 8000c64:	f403 3300 	and.w	r3, r3, #131072	; 0x20000
 8000c68:	f5b3 3f00 	cmp.w	r3, #131072	; 0x20000
 8000c6c:	bf0c      	ite	eq
 8000c6e:	2301      	moveq	r3, #1
 8000c70:	2300      	movne	r3, #0
 8000c72:	b2db      	uxtb	r3, r3
 8000c74:	4618      	mov	r0, r3
 8000c76:	46bd      	mov	sp, r7
 8000c78:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000c7c:	4770      	bx	lr
 8000c7e:	bf00      	nop
 8000c80:	40023800 	.word	0x40023800

08000c84 <LL_RCC_SetSysClkSource>:
 8000c84:	b480      	push	{r7}
 8000c86:	b083      	sub	sp, #12
 8000c88:	af00      	add	r7, sp, #0
 8000c8a:	6078      	str	r0, [r7, #4]
 8000c8c:	4b06      	ldr	r3, [pc, #24]	; (8000ca8 <LL_RCC_SetSysClkSource+0x24>)
 8000c8e:	689b      	ldr	r3, [r3, #8]
 8000c90:	f023 0203 	bic.w	r2, r3, #3
 8000c94:	4904      	ldr	r1, [pc, #16]	; (8000ca8 <LL_RCC_SetSysClkSource+0x24>)
 8000c96:	687b      	ldr	r3, [r7, #4]
 8000c98:	4313      	orrs	r3, r2
 8000c9a:	608b      	str	r3, [r1, #8]
 8000c9c:	bf00      	nop
 8000c9e:	370c      	adds	r7, #12
 8000ca0:	46bd      	mov	sp, r7
 8000ca2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000ca6:	4770      	bx	lr
 8000ca8:	40023800 	.word	0x40023800

08000cac <LL_RCC_GetSysClkSource>:
 8000cac:	b480      	push	{r7}
 8000cae:	af00      	add	r7, sp, #0
 8000cb0:	4b04      	ldr	r3, [pc, #16]	; (8000cc4 <LL_RCC_GetSysClkSource+0x18>)
 8000cb2:	689b      	ldr	r3, [r3, #8]
 8000cb4:	f003 030c 	and.w	r3, r3, #12
 8000cb8:	4618      	mov	r0, r3
 8000cba:	46bd      	mov	sp, r7
 8000cbc:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000cc0:	4770      	bx	lr
 8000cc2:	bf00      	nop
 8000cc4:	40023800 	.word	0x40023800

08000cc8 <LL_RCC_SetAHBPrescaler>:
 8000cc8:	b480      	push	{r7}
 8000cca:	b083      	sub	sp, #12
 8000ccc:	af00      	add	r7, sp, #0
 8000cce:	6078      	str	r0, [r7, #4]
 8000cd0:	4b06      	ldr	r3, [pc, #24]	; (8000cec <LL_RCC_SetAHBPrescaler+0x24>)
 8000cd2:	689b      	ldr	r3, [r3, #8]
 8000cd4:	f023 02f0 	bic.w	r2, r3, #240	; 0xf0
 8000cd8:	4904      	ldr	r1, [pc, #16]	; (8000cec <LL_RCC_SetAHBPrescaler+0x24>)
 8000cda:	687b      	ldr	r3, [r7, #4]
 8000cdc:	4313      	orrs	r3, r2
 8000cde:	608b      	str	r3, [r1, #8]
 8000ce0:	bf00      	nop
 8000ce2:	370c      	adds	r7, #12
 8000ce4:	46bd      	mov	sp, r7
 8000ce6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000cea:	4770      	bx	lr
 8000cec:	40023800 	.word	0x40023800

08000cf0 <LL_RCC_SetAPB1Prescaler>:
 8000cf0:	b480      	push	{r7}
 8000cf2:	b083      	sub	sp, #12
 8000cf4:	af00      	add	r7, sp, #0
 8000cf6:	6078      	str	r0, [r7, #4]
 8000cf8:	4b06      	ldr	r3, [pc, #24]	; (8000d14 <LL_RCC_SetAPB1Prescaler+0x24>)
 8000cfa:	689b      	ldr	r3, [r3, #8]
 8000cfc:	f423 52e0 	bic.w	r2, r3, #7168	; 0x1c00
 8000d00:	4904      	ldr	r1, [pc, #16]	; (8000d14 <LL_RCC_SetAPB1Prescaler+0x24>)
 8000d02:	687b      	ldr	r3, [r7, #4]
 8000d04:	4313      	orrs	r3, r2
 8000d06:	608b      	str	r3, [r1, #8]
 8000d08:	bf00      	nop
 8000d0a:	370c      	adds	r7, #12
 8000d0c:	46bd      	mov	sp, r7
 8000d0e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000d12:	4770      	bx	lr
 8000d14:	40023800 	.word	0x40023800

08000d18 <LL_RCC_SetAPB2Prescaler>:
 8000d18:	b480      	push	{r7}
 8000d1a:	b083      	sub	sp, #12
 8000d1c:	af00      	add	r7, sp, #0
 8000d1e:	6078      	str	r0, [r7, #4]
 8000d20:	4b06      	ldr	r3, [pc, #24]	; (8000d3c <LL_RCC_SetAPB2Prescaler+0x24>)
 8000d22:	689b      	ldr	r3, [r3, #8]
 8000d24:	f423 4260 	bic.w	r2, r3, #57344	; 0xe000
 8000d28:	4904      	ldr	r1, [pc, #16]	; (8000d3c <LL_RCC_SetAPB2Prescaler+0x24>)
 8000d2a:	687b      	ldr	r3, [r7, #4]
 8000d2c:	4313      	orrs	r3, r2
 8000d2e:	608b      	str	r3, [r1, #8]
 8000d30:	bf00      	nop
 8000d32:	370c      	adds	r7, #12
 8000d34:	46bd      	mov	sp, r7
 8000d36:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000d3a:	4770      	bx	lr
 8000d3c:	40023800 	.word	0x40023800

08000d40 <LL_RCC_PLL_Enable>:
 8000d40:	b480      	push	{r7}
 8000d42:	af00      	add	r7, sp, #0
 8000d44:	4b05      	ldr	r3, [pc, #20]	; (8000d5c <LL_RCC_PLL_Enable+0x1c>)
 8000d46:	681b      	ldr	r3, [r3, #0]
 8000d48:	4a04      	ldr	r2, [pc, #16]	; (8000d5c <LL_RCC_PLL_Enable+0x1c>)
 8000d4a:	f043 7380 	orr.w	r3, r3, #16777216	; 0x1000000
 8000d4e:	6013      	str	r3, [r2, #0]
 8000d50:	bf00      	nop
 8000d52:	46bd      	mov	sp, r7
 8000d54:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000d58:	4770      	bx	lr
 8000d5a:	bf00      	nop
 8000d5c:	40023800 	.word	0x40023800

08000d60 <LL_RCC_PLL_IsReady>:
 8000d60:	b480      	push	{r7}
 8000d62:	af00      	add	r7, sp, #0
 8000d64:	4b07      	ldr	r3, [pc, #28]	; (8000d84 <LL_RCC_PLL_IsReady+0x24>)
 8000d66:	681b      	ldr	r3, [r3, #0]
 8000d68:	f003 7300 	and.w	r3, r3, #33554432	; 0x2000000
 8000d6c:	f1b3 7f00 	cmp.w	r3, #33554432	; 0x2000000
 8000d70:	bf0c      	ite	eq
 8000d72:	2301      	moveq	r3, #1
 8000d74:	2300      	movne	r3, #0
 8000d76:	b2db      	uxtb	r3, r3
 8000d78:	4618      	mov	r0, r3
 8000d7a:	46bd      	mov	sp, r7
 8000d7c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000d80:	4770      	bx	lr
 8000d82:	bf00      	nop
 8000d84:	40023800 	.word	0x40023800

08000d88 <LL_RCC_PLL_ConfigDomain_SYS>:
 8000d88:	b480      	push	{r7}
 8000d8a:	b085      	sub	sp, #20
 8000d8c:	af00      	add	r7, sp, #0
 8000d8e:	60f8      	str	r0, [r7, #12]
 8000d90:	60b9      	str	r1, [r7, #8]
 8000d92:	607a      	str	r2, [r7, #4]
 8000d94:	603b      	str	r3, [r7, #0]
 8000d96:	4b0d      	ldr	r3, [pc, #52]	; (8000dcc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8000d98:	685a      	ldr	r2, [r3, #4]
 8000d9a:	4b0d      	ldr	r3, [pc, #52]	; (8000dd0 <LL_RCC_PLL_ConfigDomain_SYS+0x48>)
 8000d9c:	4013      	ands	r3, r2
 8000d9e:	68f9      	ldr	r1, [r7, #12]
 8000da0:	68ba      	ldr	r2, [r7, #8]
 8000da2:	4311      	orrs	r1, r2
 8000da4:	687a      	ldr	r2, [r7, #4]
 8000da6:	0192      	lsls	r2, r2, #6
 8000da8:	430a      	orrs	r2, r1
 8000daa:	4908      	ldr	r1, [pc, #32]	; (8000dcc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8000dac:	4313      	orrs	r3, r2
 8000dae:	604b      	str	r3, [r1, #4]
 8000db0:	4b06      	ldr	r3, [pc, #24]	; (8000dcc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8000db2:	685b      	ldr	r3, [r3, #4]
 8000db4:	f423 3240 	bic.w	r2, r3, #196608	; 0x30000
 8000db8:	4904      	ldr	r1, [pc, #16]	; (8000dcc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8000dba:	683b      	ldr	r3, [r7, #0]
 8000dbc:	4313      	orrs	r3, r2
 8000dbe:	604b      	str	r3, [r1, #4]
 8000dc0:	bf00      	nop
 8000dc2:	3714      	adds	r7, #20
 8000dc4:	46bd      	mov	sp, r7
 8000dc6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000dca:	4770      	bx	lr
 8000dcc:	40023800 	.word	0x40023800
 8000dd0:	ffbf8000 	.word	0xffbf8000

08000dd4 <LL_FLASH_SetLatency>:
 8000dd4:	b480      	push	{r7}
 8000dd6:	b083      	sub	sp, #12
 8000dd8:	af00      	add	r7, sp, #0
 8000dda:	6078      	str	r0, [r7, #4]
 8000ddc:	4b06      	ldr	r3, [pc, #24]	; (8000df8 <LL_FLASH_SetLatency+0x24>)
 8000dde:	681b      	ldr	r3, [r3, #0]
 8000de0:	f023 020f 	bic.w	r2, r3, #15
 8000de4:	4904      	ldr	r1, [pc, #16]	; (8000df8 <LL_FLASH_SetLatency+0x24>)
 8000de6:	687b      	ldr	r3, [r7, #4]
 8000de8:	4313      	orrs	r3, r2
 8000dea:	600b      	str	r3, [r1, #0]
 8000dec:	bf00      	nop
 8000dee:	370c      	adds	r7, #12
 8000df0:	46bd      	mov	sp, r7
 8000df2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000df6:	4770      	bx	lr
 8000df8:	40023c00 	.word	0x40023c00

08000dfc <LL_AHB1_GRP1_EnableClock>:
 8000dfc:	b480      	push	{r7}
 8000dfe:	b085      	sub	sp, #20
 8000e00:	af00      	add	r7, sp, #0
 8000e02:	6078      	str	r0, [r7, #4]
 8000e04:	4b08      	ldr	r3, [pc, #32]	; (8000e28 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8000e06:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8000e08:	4907      	ldr	r1, [pc, #28]	; (8000e28 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8000e0a:	687b      	ldr	r3, [r7, #4]
 8000e0c:	4313      	orrs	r3, r2
 8000e0e:	630b      	str	r3, [r1, #48]	; 0x30
 8000e10:	4b05      	ldr	r3, [pc, #20]	; (8000e28 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8000e12:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8000e14:	687b      	ldr	r3, [r7, #4]
 8000e16:	4013      	ands	r3, r2
 8000e18:	60fb      	str	r3, [r7, #12]
 8000e1a:	68fb      	ldr	r3, [r7, #12]
 8000e1c:	bf00      	nop
 8000e1e:	3714      	adds	r7, #20
 8000e20:	46bd      	mov	sp, r7
 8000e22:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000e26:	4770      	bx	lr
 8000e28:	40023800 	.word	0x40023800

08000e2c <LL_GPIO_SetPinMode>:
 8000e2c:	b480      	push	{r7}
 8000e2e:	b089      	sub	sp, #36	; 0x24
 8000e30:	af00      	add	r7, sp, #0
 8000e32:	60f8      	str	r0, [r7, #12]
 8000e34:	60b9      	str	r1, [r7, #8]
 8000e36:	607a      	str	r2, [r7, #4]
 8000e38:	68fb      	ldr	r3, [r7, #12]
 8000e3a:	681a      	ldr	r2, [r3, #0]
 8000e3c:	68bb      	ldr	r3, [r7, #8]
 8000e3e:	617b      	str	r3, [r7, #20]
 8000e40:	697b      	ldr	r3, [r7, #20]
 8000e42:	fa93 f3a3 	rbit	r3, r3
 8000e46:	613b      	str	r3, [r7, #16]
 8000e48:	693b      	ldr	r3, [r7, #16]
 8000e4a:	fab3 f383 	clz	r3, r3
 8000e4e:	005b      	lsls	r3, r3, #1
 8000e50:	2103      	movs	r1, #3
 8000e52:	fa01 f303 	lsl.w	r3, r1, r3
 8000e56:	43db      	mvns	r3, r3
 8000e58:	401a      	ands	r2, r3
 8000e5a:	68bb      	ldr	r3, [r7, #8]
 8000e5c:	61fb      	str	r3, [r7, #28]
 8000e5e:	69fb      	ldr	r3, [r7, #28]
 8000e60:	fa93 f3a3 	rbit	r3, r3
 8000e64:	61bb      	str	r3, [r7, #24]
 8000e66:	69bb      	ldr	r3, [r7, #24]
 8000e68:	fab3 f383 	clz	r3, r3
 8000e6c:	005b      	lsls	r3, r3, #1
 8000e6e:	6879      	ldr	r1, [r7, #4]
 8000e70:	fa01 f303 	lsl.w	r3, r1, r3
 8000e74:	431a      	orrs	r2, r3
 8000e76:	68fb      	ldr	r3, [r7, #12]
 8000e78:	601a      	str	r2, [r3, #0]
 8000e7a:	bf00      	nop
 8000e7c:	3724      	adds	r7, #36	; 0x24
 8000e7e:	46bd      	mov	sp, r7
 8000e80:	f85d 7b04 	ldr.w	r7, [sp], #4
 8000e84:	4770      	bx	lr
	...

08000e88 <rcc_config>:
 8000e88:	b580      	push	{r7, lr}
 8000e8a:	af00      	add	r7, sp, #0
 8000e8c:	f7ff fed6 	bl	8000c3c <LL_RCC_HSE_Enable>
 8000e90:	bf00      	nop
 8000e92:	f7ff fee3 	bl	8000c5c <LL_RCC_HSE_IsReady>
 8000e96:	4603      	mov	r3, r0
 8000e98:	2b01      	cmp	r3, #1
 8000e9a:	d1fa      	bne.n	8000e92 <rcc_config+0xa>
 8000e9c:	2005      	movs	r0, #5
 8000e9e:	f7ff ff99 	bl	8000dd4 <LL_FLASH_SetLatency>
 8000ea2:	2300      	movs	r3, #0
 8000ea4:	f44f 72a8 	mov.w	r2, #336	; 0x150
 8000ea8:	2108      	movs	r1, #8
 8000eaa:	f44f 0080 	mov.w	r0, #4194304	; 0x400000
 8000eae:	f7ff ff6b 	bl	8000d88 <LL_RCC_PLL_ConfigDomain_SYS>
 8000eb2:	f7ff ff45 	bl	8000d40 <LL_RCC_PLL_Enable>
 8000eb6:	bf00      	nop
 8000eb8:	f7ff ff52 	bl	8000d60 <LL_RCC_PLL_IsReady>
 8000ebc:	4603      	mov	r3, r0
 8000ebe:	2b01      	cmp	r3, #1
 8000ec0:	d1fa      	bne.n	8000eb8 <rcc_config+0x30>
 8000ec2:	2000      	movs	r0, #0
 8000ec4:	f7ff ff00 	bl	8000cc8 <LL_RCC_SetAHBPrescaler>
 8000ec8:	2002      	movs	r0, #2
 8000eca:	f7ff fedb 	bl	8000c84 <LL_RCC_SetSysClkSource>
 8000ece:	bf00      	nop
 8000ed0:	f7ff feec 	bl	8000cac <LL_RCC_GetSysClkSource>
 8000ed4:	4603      	mov	r3, r0
 8000ed6:	2b08      	cmp	r3, #8
 8000ed8:	d1fa      	bne.n	8000ed0 <rcc_config+0x48>
 8000eda:	f44f 50a0 	mov.w	r0, #5120	; 0x1400
 8000ede:	f7ff ff07 	bl	8000cf0 <LL_RCC_SetAPB1Prescaler>
 8000ee2:	f44f 4000 	mov.w	r0, #32768	; 0x8000
 8000ee6:	f7ff ff17 	bl	8000d18 <LL_RCC_SetAPB2Prescaler>
 8000eea:	4804      	ldr	r0, [pc, #16]	; (8000efc <rcc_config+0x74>)
 8000eec:	f7ff fe84 	bl	8000bf8 <SysTick_Config>
 8000ef0:	4b03      	ldr	r3, [pc, #12]	; (8000f00 <rcc_config+0x78>)
 8000ef2:	4a04      	ldr	r2, [pc, #16]	; (8000f04 <rcc_config+0x7c>)
 8000ef4:	601a      	str	r2, [r3, #0]
 8000ef6:	bf00      	nop
 8000ef8:	bd80      	pop	{r7, pc}
 8000efa:	bf00      	nop
 8000efc:	00029040 	.word	0x00029040
 8000f00:	2000007c 	.word	0x2000007c
 8000f04:	0a037a00 	.word	0x0a037a00

08000f08 <gpio_config>:
 8000f08:	b580      	push	{r7, lr}
 8000f0a:	af00      	add	r7, sp, #0
 8000f0c:	2008      	movs	r0, #8
 8000f0e:	f7ff ff75 	bl	8000dfc <LL_AHB1_GRP1_EnableClock>
 8000f12:	2201      	movs	r2, #1
 8000f14:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8000f18:	4802      	ldr	r0, [pc, #8]	; (8000f24 <gpio_config+0x1c>)
 8000f1a:	f7ff ff87 	bl	8000e2c <LL_GPIO_SetPinMode>
 8000f1e:	bf00      	nop
 8000f20:	bd80      	pop	{r7, pc}
 8000f22:	bf00      	nop
 8000f24:	40020c00 	.word	0x40020c00

08000f28 <main>:
 8000f28:	b580      	push	{r7, lr}
 8000f2a:	b084      	sub	sp, #16
 8000f2c:	af04      	add	r7, sp, #16
 8000f2e:	f7ff ffab 	bl	8000e88 <rcc_config>
 8000f32:	f7ff ffe9 	bl	8000f08 <gpio_config>
 8000f36:	f004 f9bb 	bl	80052b0 <init_sort_mech>
 8000f3a:	2000      	movs	r0, #0
 8000f3c:	f7ff fe0e 	bl	8000b5c <NVIC_SetPriorityGrouping>
 8000f40:	4b1c      	ldr	r3, [pc, #112]	; (8000fb4 <main+0x8c>)
 8000f42:	9302      	str	r3, [sp, #8]
 8000f44:	4b1c      	ldr	r3, [pc, #112]	; (8000fb8 <main+0x90>)
 8000f46:	9301      	str	r3, [sp, #4]
 8000f48:	2303      	movs	r3, #3
 8000f4a:	9300      	str	r3, [sp, #0]
 8000f4c:	2300      	movs	r3, #0
 8000f4e:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8000f52:	491a      	ldr	r1, [pc, #104]	; (8000fbc <main+0x94>)
 8000f54:	481a      	ldr	r0, [pc, #104]	; (8000fc0 <main+0x98>)
 8000f56:	f006 fee7 	bl	8007d28 <xTaskCreateStatic>
 8000f5a:	4b1a      	ldr	r3, [pc, #104]	; (8000fc4 <main+0x9c>)
 8000f5c:	9302      	str	r3, [sp, #8]
 8000f5e:	4b1a      	ldr	r3, [pc, #104]	; (8000fc8 <main+0xa0>)
 8000f60:	9301      	str	r3, [sp, #4]
 8000f62:	2302      	movs	r3, #2
 8000f64:	9300      	str	r3, [sp, #0]
 8000f66:	2300      	movs	r3, #0
 8000f68:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8000f6c:	4917      	ldr	r1, [pc, #92]	; (8000fcc <main+0xa4>)
 8000f6e:	4818      	ldr	r0, [pc, #96]	; (8000fd0 <main+0xa8>)
 8000f70:	f006 feda 	bl	8007d28 <xTaskCreateStatic>
 8000f74:	4b17      	ldr	r3, [pc, #92]	; (8000fd4 <main+0xac>)
 8000f76:	9302      	str	r3, [sp, #8]
 8000f78:	4b17      	ldr	r3, [pc, #92]	; (8000fd8 <main+0xb0>)
 8000f7a:	9301      	str	r3, [sp, #4]
 8000f7c:	2302      	movs	r3, #2
 8000f7e:	9300      	str	r3, [sp, #0]
 8000f80:	2300      	movs	r3, #0
 8000f82:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8000f86:	4915      	ldr	r1, [pc, #84]	; (8000fdc <main+0xb4>)
 8000f88:	4815      	ldr	r0, [pc, #84]	; (8000fe0 <main+0xb8>)
 8000f8a:	f006 fecd 	bl	8007d28 <xTaskCreateStatic>
 8000f8e:	4b15      	ldr	r3, [pc, #84]	; (8000fe4 <main+0xbc>)
 8000f90:	9302      	str	r3, [sp, #8]
 8000f92:	4b15      	ldr	r3, [pc, #84]	; (8000fe8 <main+0xc0>)
 8000f94:	9301      	str	r3, [sp, #4]
 8000f96:	2301      	movs	r3, #1
 8000f98:	9300      	str	r3, [sp, #0]
 8000f9a:	2300      	movs	r3, #0
 8000f9c:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8000fa0:	4912      	ldr	r1, [pc, #72]	; (8000fec <main+0xc4>)
 8000fa2:	4813      	ldr	r0, [pc, #76]	; (8000ff0 <main+0xc8>)
 8000fa4:	f006 fec0 	bl	8007d28 <xTaskCreateStatic>
 8000fa8:	f007 f890 	bl	80080cc <vTaskStartScheduler>
 8000fac:	2300      	movs	r3, #0
 8000fae:	4618      	mov	r0, r3
 8000fb0:	46bd      	mov	sp, r7
 8000fb2:	bd80      	pop	{r7, pc}
 8000fb4:	20013a20 	.word	0x20013a20
 8000fb8:	200167f0 	.word	0x200167f0
 8000fbc:	0800d058 	.word	0x0800d058
 8000fc0:	08006d3d 	.word	0x08006d3d
 8000fc4:	20018800 	.word	0x20018800
 8000fc8:	20014ef0 	.word	0x20014ef0
 8000fcc:	0800d064 	.word	0x0800d064
 8000fd0:	08003579 	.word	0x08003579
 8000fd4:	20015ef0 	.word	0x20015ef0
 8000fd8:	200177fc 	.word	0x200177fc
 8000fdc:	0800d070 	.word	0x0800d070
 8000fe0:	08004615 	.word	0x08004615
 8000fe4:	20016370 	.word	0x20016370
 8000fe8:	20013ea0 	.word	0x20013ea0
 8000fec:	0800d07c 	.word	0x0800d07c
 8000ff0:	08001d79 	.word	0x08001d79

08000ff4 <LL_RCC_HSE_Disable>:
 8000ff4:	b480      	push	{r7}
 8000ff6:	af00      	add	r7, sp, #0
 8000ff8:	4b05      	ldr	r3, [pc, #20]	; (8001010 <LL_RCC_HSE_Disable+0x1c>)
 8000ffa:	681b      	ldr	r3, [r3, #0]
 8000ffc:	4a04      	ldr	r2, [pc, #16]	; (8001010 <LL_RCC_HSE_Disable+0x1c>)
 8000ffe:	f423 3380 	bic.w	r3, r3, #65536	; 0x10000
 8001002:	6013      	str	r3, [r2, #0]
 8001004:	bf00      	nop
 8001006:	46bd      	mov	sp, r7
 8001008:	f85d 7b04 	ldr.w	r7, [sp], #4
 800100c:	4770      	bx	lr
 800100e:	bf00      	nop
 8001010:	40023800 	.word	0x40023800

08001014 <LL_RCC_HSI_Enable>:
 8001014:	b480      	push	{r7}
 8001016:	af00      	add	r7, sp, #0
 8001018:	4b05      	ldr	r3, [pc, #20]	; (8001030 <LL_RCC_HSI_Enable+0x1c>)
 800101a:	681b      	ldr	r3, [r3, #0]
 800101c:	4a04      	ldr	r2, [pc, #16]	; (8001030 <LL_RCC_HSI_Enable+0x1c>)
 800101e:	f043 0301 	orr.w	r3, r3, #1
 8001022:	6013      	str	r3, [r2, #0]
 8001024:	bf00      	nop
 8001026:	46bd      	mov	sp, r7
 8001028:	f85d 7b04 	ldr.w	r7, [sp], #4
 800102c:	4770      	bx	lr
 800102e:	bf00      	nop
 8001030:	40023800 	.word	0x40023800

08001034 <LL_RCC_HSI_IsReady>:
 8001034:	b480      	push	{r7}
 8001036:	af00      	add	r7, sp, #0
 8001038:	4b06      	ldr	r3, [pc, #24]	; (8001054 <LL_RCC_HSI_IsReady+0x20>)
 800103a:	681b      	ldr	r3, [r3, #0]
 800103c:	f003 0302 	and.w	r3, r3, #2
 8001040:	2b02      	cmp	r3, #2
 8001042:	bf0c      	ite	eq
 8001044:	2301      	moveq	r3, #1
 8001046:	2300      	movne	r3, #0
 8001048:	b2db      	uxtb	r3, r3
 800104a:	4618      	mov	r0, r3
 800104c:	46bd      	mov	sp, r7
 800104e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001052:	4770      	bx	lr
 8001054:	40023800 	.word	0x40023800

08001058 <LL_RCC_SetAHBPrescaler>:
 8001058:	b480      	push	{r7}
 800105a:	b083      	sub	sp, #12
 800105c:	af00      	add	r7, sp, #0
 800105e:	6078      	str	r0, [r7, #4]
 8001060:	4b06      	ldr	r3, [pc, #24]	; (800107c <LL_RCC_SetAHBPrescaler+0x24>)
 8001062:	689b      	ldr	r3, [r3, #8]
 8001064:	f023 02f0 	bic.w	r2, r3, #240	; 0xf0
 8001068:	4904      	ldr	r1, [pc, #16]	; (800107c <LL_RCC_SetAHBPrescaler+0x24>)
 800106a:	687b      	ldr	r3, [r7, #4]
 800106c:	4313      	orrs	r3, r2
 800106e:	608b      	str	r3, [r1, #8]
 8001070:	bf00      	nop
 8001072:	370c      	adds	r7, #12
 8001074:	46bd      	mov	sp, r7
 8001076:	f85d 7b04 	ldr.w	r7, [sp], #4
 800107a:	4770      	bx	lr
 800107c:	40023800 	.word	0x40023800

08001080 <LL_RCC_SetAPB1Prescaler>:
 8001080:	b480      	push	{r7}
 8001082:	b083      	sub	sp, #12
 8001084:	af00      	add	r7, sp, #0
 8001086:	6078      	str	r0, [r7, #4]
 8001088:	4b06      	ldr	r3, [pc, #24]	; (80010a4 <LL_RCC_SetAPB1Prescaler+0x24>)
 800108a:	689b      	ldr	r3, [r3, #8]
 800108c:	f423 52e0 	bic.w	r2, r3, #7168	; 0x1c00
 8001090:	4904      	ldr	r1, [pc, #16]	; (80010a4 <LL_RCC_SetAPB1Prescaler+0x24>)
 8001092:	687b      	ldr	r3, [r7, #4]
 8001094:	4313      	orrs	r3, r2
 8001096:	608b      	str	r3, [r1, #8]
 8001098:	bf00      	nop
 800109a:	370c      	adds	r7, #12
 800109c:	46bd      	mov	sp, r7
 800109e:	f85d 7b04 	ldr.w	r7, [sp], #4
 80010a2:	4770      	bx	lr
 80010a4:	40023800 	.word	0x40023800

080010a8 <LL_RCC_SetAPB2Prescaler>:
 80010a8:	b480      	push	{r7}
 80010aa:	b083      	sub	sp, #12
 80010ac:	af00      	add	r7, sp, #0
 80010ae:	6078      	str	r0, [r7, #4]
 80010b0:	4b06      	ldr	r3, [pc, #24]	; (80010cc <LL_RCC_SetAPB2Prescaler+0x24>)
 80010b2:	689b      	ldr	r3, [r3, #8]
 80010b4:	f423 4260 	bic.w	r2, r3, #57344	; 0xe000
 80010b8:	4904      	ldr	r1, [pc, #16]	; (80010cc <LL_RCC_SetAPB2Prescaler+0x24>)
 80010ba:	687b      	ldr	r3, [r7, #4]
 80010bc:	4313      	orrs	r3, r2
 80010be:	608b      	str	r3, [r1, #8]
 80010c0:	bf00      	nop
 80010c2:	370c      	adds	r7, #12
 80010c4:	46bd      	mov	sp, r7
 80010c6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80010ca:	4770      	bx	lr
 80010cc:	40023800 	.word	0x40023800

080010d0 <LL_RCC_PLL_Disable>:
 80010d0:	b480      	push	{r7}
 80010d2:	af00      	add	r7, sp, #0
 80010d4:	4b05      	ldr	r3, [pc, #20]	; (80010ec <LL_RCC_PLL_Disable+0x1c>)
 80010d6:	681b      	ldr	r3, [r3, #0]
 80010d8:	4a04      	ldr	r2, [pc, #16]	; (80010ec <LL_RCC_PLL_Disable+0x1c>)
 80010da:	f023 7380 	bic.w	r3, r3, #16777216	; 0x1000000
 80010de:	6013      	str	r3, [r2, #0]
 80010e0:	bf00      	nop
 80010e2:	46bd      	mov	sp, r7
 80010e4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80010e8:	4770      	bx	lr
 80010ea:	bf00      	nop
 80010ec:	40023800 	.word	0x40023800

080010f0 <DeInit>:
 80010f0:	b580      	push	{r7, lr}
 80010f2:	af00      	add	r7, sp, #0
 80010f4:	f7ff ffec 	bl	80010d0 <LL_RCC_PLL_Disable>
 80010f8:	f7ff ff7c 	bl	8000ff4 <LL_RCC_HSE_Disable>
 80010fc:	f7ff ff8a 	bl	8001014 <LL_RCC_HSI_Enable>
 8001100:	bf00      	nop
 8001102:	f7ff ff97 	bl	8001034 <LL_RCC_HSI_IsReady>
 8001106:	4603      	mov	r3, r0
 8001108:	2b01      	cmp	r3, #1
 800110a:	d1fa      	bne.n	8001102 <DeInit+0x12>
 800110c:	2000      	movs	r0, #0
 800110e:	f7ff ffa3 	bl	8001058 <LL_RCC_SetAHBPrescaler>
 8001112:	2000      	movs	r0, #0
 8001114:	f7ff ffb4 	bl	8001080 <LL_RCC_SetAPB1Prescaler>
 8001118:	2000      	movs	r0, #0
 800111a:	f7ff ffc5 	bl	80010a8 <LL_RCC_SetAPB2Prescaler>
 800111e:	4b07      	ldr	r3, [pc, #28]	; (800113c <DeInit+0x4c>)
 8001120:	2200      	movs	r2, #0
 8001122:	601a      	str	r2, [r3, #0]
 8001124:	4b05      	ldr	r3, [pc, #20]	; (800113c <DeInit+0x4c>)
 8001126:	2200      	movs	r2, #0
 8001128:	605a      	str	r2, [r3, #4]
 800112a:	4b04      	ldr	r3, [pc, #16]	; (800113c <DeInit+0x4c>)
 800112c:	2200      	movs	r2, #0
 800112e:	609a      	str	r2, [r3, #8]
 8001130:	b672      	cpsid	i
 8001132:	4b03      	ldr	r3, [pc, #12]	; (8001140 <DeInit+0x50>)
 8001134:	2201      	movs	r2, #1
 8001136:	601a      	str	r2, [r3, #0]
 8001138:	bf00      	nop
 800113a:	bd80      	pop	{r7, pc}
 800113c:	e000e010 	.word	0xe000e010
 8001140:	40013800 	.word	0x40013800

08001144 <init_bootloader>:
 8001144:	b580      	push	{r7, lr}
 8001146:	b084      	sub	sp, #16
 8001148:	af00      	add	r7, sp, #0
 800114a:	4b0a      	ldr	r3, [pc, #40]	; (8001174 <init_bootloader+0x30>)
 800114c:	607b      	str	r3, [r7, #4]
 800114e:	f7ff ffcf 	bl	80010f0 <DeInit>
 8001152:	687b      	ldr	r3, [r7, #4]
 8001154:	3304      	adds	r3, #4
 8001156:	681b      	ldr	r3, [r3, #0]
 8001158:	60fb      	str	r3, [r7, #12]
 800115a:	687b      	ldr	r3, [r7, #4]
 800115c:	681b      	ldr	r3, [r3, #0]
 800115e:	60bb      	str	r3, [r7, #8]
 8001160:	68bb      	ldr	r3, [r7, #8]
 8001162:	f383 8808 	msr	MSP, r3
 8001166:	68fb      	ldr	r3, [r7, #12]
 8001168:	4798      	blx	r3
 800116a:	bf00      	nop
 800116c:	3710      	adds	r7, #16
 800116e:	46bd      	mov	sp, r7
 8001170:	bd80      	pop	{r7, pc}
 8001172:	bf00      	nop
 8001174:	1fff0000 	.word	0x1fff0000

08001178 <cmd_bootloader>:
 8001178:	b580      	push	{r7, lr}
 800117a:	b082      	sub	sp, #8
 800117c:	af00      	add	r7, sp, #0
 800117e:	6078      	str	r0, [r7, #4]
 8001180:	f7ff ffe0 	bl	8001144 <init_bootloader>
 8001184:	2203      	movs	r2, #3
 8001186:	4904      	ldr	r1, [pc, #16]	; (8001198 <cmd_bootloader+0x20>)
 8001188:	6878      	ldr	r0, [r7, #4]
 800118a:	f009 fbbf 	bl	800a90c <memcpy>
 800118e:	2303      	movs	r3, #3
 8001190:	4618      	mov	r0, r3
 8001192:	3708      	adds	r7, #8
 8001194:	46bd      	mov	sp, r7
 8001196:	bd80      	pop	{r7, pc}
 8001198:	0800d08c 	.word	0x0800d08c

0800119c <_malloc_r>:
 800119c:	b580      	push	{r7, lr}
 800119e:	b082      	sub	sp, #8
 80011a0:	af00      	add	r7, sp, #0
 80011a2:	6078      	str	r0, [r7, #4]
 80011a4:	6039      	str	r1, [r7, #0]
 80011a6:	6838      	ldr	r0, [r7, #0]
 80011a8:	f008 fd2a 	bl	8009c00 <pvPortMalloc>
 80011ac:	4603      	mov	r3, r0
 80011ae:	4618      	mov	r0, r3
 80011b0:	3708      	adds	r7, #8
 80011b2:	46bd      	mov	sp, r7
 80011b4:	bd80      	pop	{r7, pc}

080011b6 <_free_r>:
 80011b6:	b580      	push	{r7, lr}
 80011b8:	b082      	sub	sp, #8
 80011ba:	af00      	add	r7, sp, #0
 80011bc:	6078      	str	r0, [r7, #4]
 80011be:	6039      	str	r1, [r7, #0]
 80011c0:	6838      	ldr	r0, [r7, #0]
 80011c2:	f008 fde3 	bl	8009d8c <vPortFree>
 80011c6:	bf00      	nop
 80011c8:	3708      	adds	r7, #8
 80011ca:	46bd      	mov	sp, r7
 80011cc:	bd80      	pop	{r7, pc}
	...

080011d0 <NVIC_EnableIRQ>:
 80011d0:	b480      	push	{r7}
 80011d2:	b083      	sub	sp, #12
 80011d4:	af00      	add	r7, sp, #0
 80011d6:	4603      	mov	r3, r0
 80011d8:	71fb      	strb	r3, [r7, #7]
 80011da:	79fb      	ldrb	r3, [r7, #7]
 80011dc:	f003 021f 	and.w	r2, r3, #31
 80011e0:	4907      	ldr	r1, [pc, #28]	; (8001200 <NVIC_EnableIRQ+0x30>)
 80011e2:	f997 3007 	ldrsb.w	r3, [r7, #7]
 80011e6:	095b      	lsrs	r3, r3, #5
 80011e8:	2001      	movs	r0, #1
 80011ea:	fa00 f202 	lsl.w	r2, r0, r2
 80011ee:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 80011f2:	bf00      	nop
 80011f4:	370c      	adds	r7, #12
 80011f6:	46bd      	mov	sp, r7
 80011f8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80011fc:	4770      	bx	lr
 80011fe:	bf00      	nop
 8001200:	e000e100 	.word	0xe000e100

08001204 <NVIC_SetPriority>:
 8001204:	b480      	push	{r7}
 8001206:	b083      	sub	sp, #12
 8001208:	af00      	add	r7, sp, #0
 800120a:	4603      	mov	r3, r0
 800120c:	6039      	str	r1, [r7, #0]
 800120e:	71fb      	strb	r3, [r7, #7]
 8001210:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8001214:	2b00      	cmp	r3, #0
 8001216:	da0b      	bge.n	8001230 <NVIC_SetPriority+0x2c>
 8001218:	683b      	ldr	r3, [r7, #0]
 800121a:	b2da      	uxtb	r2, r3
 800121c:	490c      	ldr	r1, [pc, #48]	; (8001250 <NVIC_SetPriority+0x4c>)
 800121e:	79fb      	ldrb	r3, [r7, #7]
 8001220:	f003 030f 	and.w	r3, r3, #15
 8001224:	3b04      	subs	r3, #4
 8001226:	0112      	lsls	r2, r2, #4
 8001228:	b2d2      	uxtb	r2, r2
 800122a:	440b      	add	r3, r1
 800122c:	761a      	strb	r2, [r3, #24]
 800122e:	e009      	b.n	8001244 <NVIC_SetPriority+0x40>
 8001230:	683b      	ldr	r3, [r7, #0]
 8001232:	b2da      	uxtb	r2, r3
 8001234:	4907      	ldr	r1, [pc, #28]	; (8001254 <NVIC_SetPriority+0x50>)
 8001236:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800123a:	0112      	lsls	r2, r2, #4
 800123c:	b2d2      	uxtb	r2, r2
 800123e:	440b      	add	r3, r1
 8001240:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8001244:	bf00      	nop
 8001246:	370c      	adds	r7, #12
 8001248:	46bd      	mov	sp, r7
 800124a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800124e:	4770      	bx	lr
 8001250:	e000ed00 	.word	0xe000ed00
 8001254:	e000e100 	.word	0xe000e100

08001258 <LL_USART_IsActiveFlag_TC>:
 8001258:	b480      	push	{r7}
 800125a:	b083      	sub	sp, #12
 800125c:	af00      	add	r7, sp, #0
 800125e:	6078      	str	r0, [r7, #4]
 8001260:	687b      	ldr	r3, [r7, #4]
 8001262:	681b      	ldr	r3, [r3, #0]
 8001264:	f003 0340 	and.w	r3, r3, #64	; 0x40
 8001268:	2b40      	cmp	r3, #64	; 0x40
 800126a:	bf0c      	ite	eq
 800126c:	2301      	moveq	r3, #1
 800126e:	2300      	movne	r3, #0
 8001270:	b2db      	uxtb	r3, r3
 8001272:	4618      	mov	r0, r3
 8001274:	370c      	adds	r7, #12
 8001276:	46bd      	mov	sp, r7
 8001278:	f85d 7b04 	ldr.w	r7, [sp], #4
 800127c:	4770      	bx	lr

0800127e <LL_USART_IsActiveFlag_TXE>:
 800127e:	b480      	push	{r7}
 8001280:	b083      	sub	sp, #12
 8001282:	af00      	add	r7, sp, #0
 8001284:	6078      	str	r0, [r7, #4]
 8001286:	687b      	ldr	r3, [r7, #4]
 8001288:	681b      	ldr	r3, [r3, #0]
 800128a:	f003 0380 	and.w	r3, r3, #128	; 0x80
 800128e:	2b80      	cmp	r3, #128	; 0x80
 8001290:	bf0c      	ite	eq
 8001292:	2301      	moveq	r3, #1
 8001294:	2300      	movne	r3, #0
 8001296:	b2db      	uxtb	r3, r3
 8001298:	4618      	mov	r0, r3
 800129a:	370c      	adds	r7, #12
 800129c:	46bd      	mov	sp, r7
 800129e:	f85d 7b04 	ldr.w	r7, [sp], #4
 80012a2:	4770      	bx	lr

080012a4 <LL_USART_ClearFlag_TC>:
 80012a4:	b480      	push	{r7}
 80012a6:	b083      	sub	sp, #12
 80012a8:	af00      	add	r7, sp, #0
 80012aa:	6078      	str	r0, [r7, #4]
 80012ac:	687b      	ldr	r3, [r7, #4]
 80012ae:	f06f 0240 	mvn.w	r2, #64	; 0x40
 80012b2:	601a      	str	r2, [r3, #0]
 80012b4:	bf00      	nop
 80012b6:	370c      	adds	r7, #12
 80012b8:	46bd      	mov	sp, r7
 80012ba:	f85d 7b04 	ldr.w	r7, [sp], #4
 80012be:	4770      	bx	lr

080012c0 <LL_USART_TransmitData8>:
 80012c0:	b480      	push	{r7}
 80012c2:	b083      	sub	sp, #12
 80012c4:	af00      	add	r7, sp, #0
 80012c6:	6078      	str	r0, [r7, #4]
 80012c8:	460b      	mov	r3, r1
 80012ca:	70fb      	strb	r3, [r7, #3]
 80012cc:	78fa      	ldrb	r2, [r7, #3]
 80012ce:	687b      	ldr	r3, [r7, #4]
 80012d0:	605a      	str	r2, [r3, #4]
 80012d2:	bf00      	nop
 80012d4:	370c      	adds	r7, #12
 80012d6:	46bd      	mov	sp, r7
 80012d8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80012dc:	4770      	bx	lr

080012de <LL_GPIO_SetPinMode>:
 80012de:	b480      	push	{r7}
 80012e0:	b089      	sub	sp, #36	; 0x24
 80012e2:	af00      	add	r7, sp, #0
 80012e4:	60f8      	str	r0, [r7, #12]
 80012e6:	60b9      	str	r1, [r7, #8]
 80012e8:	607a      	str	r2, [r7, #4]
 80012ea:	68fb      	ldr	r3, [r7, #12]
 80012ec:	681a      	ldr	r2, [r3, #0]
 80012ee:	68bb      	ldr	r3, [r7, #8]
 80012f0:	617b      	str	r3, [r7, #20]
 80012f2:	697b      	ldr	r3, [r7, #20]
 80012f4:	fa93 f3a3 	rbit	r3, r3
 80012f8:	613b      	str	r3, [r7, #16]
 80012fa:	693b      	ldr	r3, [r7, #16]
 80012fc:	fab3 f383 	clz	r3, r3
 8001300:	005b      	lsls	r3, r3, #1
 8001302:	2103      	movs	r1, #3
 8001304:	fa01 f303 	lsl.w	r3, r1, r3
 8001308:	43db      	mvns	r3, r3
 800130a:	401a      	ands	r2, r3
 800130c:	68bb      	ldr	r3, [r7, #8]
 800130e:	61fb      	str	r3, [r7, #28]
 8001310:	69fb      	ldr	r3, [r7, #28]
 8001312:	fa93 f3a3 	rbit	r3, r3
 8001316:	61bb      	str	r3, [r7, #24]
 8001318:	69bb      	ldr	r3, [r7, #24]
 800131a:	fab3 f383 	clz	r3, r3
 800131e:	005b      	lsls	r3, r3, #1
 8001320:	6879      	ldr	r1, [r7, #4]
 8001322:	fa01 f303 	lsl.w	r3, r1, r3
 8001326:	431a      	orrs	r2, r3
 8001328:	68fb      	ldr	r3, [r7, #12]
 800132a:	601a      	str	r2, [r3, #0]
 800132c:	bf00      	nop
 800132e:	3724      	adds	r7, #36	; 0x24
 8001330:	46bd      	mov	sp, r7
 8001332:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001336:	4770      	bx	lr

08001338 <LL_GPIO_SetPinPull>:
 8001338:	b480      	push	{r7}
 800133a:	b089      	sub	sp, #36	; 0x24
 800133c:	af00      	add	r7, sp, #0
 800133e:	60f8      	str	r0, [r7, #12]
 8001340:	60b9      	str	r1, [r7, #8]
 8001342:	607a      	str	r2, [r7, #4]
 8001344:	68fb      	ldr	r3, [r7, #12]
 8001346:	68da      	ldr	r2, [r3, #12]
 8001348:	68bb      	ldr	r3, [r7, #8]
 800134a:	617b      	str	r3, [r7, #20]
 800134c:	697b      	ldr	r3, [r7, #20]
 800134e:	fa93 f3a3 	rbit	r3, r3
 8001352:	613b      	str	r3, [r7, #16]
 8001354:	693b      	ldr	r3, [r7, #16]
 8001356:	fab3 f383 	clz	r3, r3
 800135a:	005b      	lsls	r3, r3, #1
 800135c:	2103      	movs	r1, #3
 800135e:	fa01 f303 	lsl.w	r3, r1, r3
 8001362:	43db      	mvns	r3, r3
 8001364:	401a      	ands	r2, r3
 8001366:	68bb      	ldr	r3, [r7, #8]
 8001368:	61fb      	str	r3, [r7, #28]
 800136a:	69fb      	ldr	r3, [r7, #28]
 800136c:	fa93 f3a3 	rbit	r3, r3
 8001370:	61bb      	str	r3, [r7, #24]
 8001372:	69bb      	ldr	r3, [r7, #24]
 8001374:	fab3 f383 	clz	r3, r3
 8001378:	005b      	lsls	r3, r3, #1
 800137a:	6879      	ldr	r1, [r7, #4]
 800137c:	fa01 f303 	lsl.w	r3, r1, r3
 8001380:	431a      	orrs	r2, r3
 8001382:	68fb      	ldr	r3, [r7, #12]
 8001384:	60da      	str	r2, [r3, #12]
 8001386:	bf00      	nop
 8001388:	3724      	adds	r7, #36	; 0x24
 800138a:	46bd      	mov	sp, r7
 800138c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001390:	4770      	bx	lr

08001392 <LL_GPIO_SetAFPin_8_15>:
 8001392:	b480      	push	{r7}
 8001394:	b089      	sub	sp, #36	; 0x24
 8001396:	af00      	add	r7, sp, #0
 8001398:	60f8      	str	r0, [r7, #12]
 800139a:	60b9      	str	r1, [r7, #8]
 800139c:	607a      	str	r2, [r7, #4]
 800139e:	68fb      	ldr	r3, [r7, #12]
 80013a0:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 80013a2:	68bb      	ldr	r3, [r7, #8]
 80013a4:	0a1b      	lsrs	r3, r3, #8
 80013a6:	617b      	str	r3, [r7, #20]
 80013a8:	697b      	ldr	r3, [r7, #20]
 80013aa:	fa93 f3a3 	rbit	r3, r3
 80013ae:	613b      	str	r3, [r7, #16]
 80013b0:	693b      	ldr	r3, [r7, #16]
 80013b2:	fab3 f383 	clz	r3, r3
 80013b6:	009b      	lsls	r3, r3, #2
 80013b8:	210f      	movs	r1, #15
 80013ba:	fa01 f303 	lsl.w	r3, r1, r3
 80013be:	43db      	mvns	r3, r3
 80013c0:	401a      	ands	r2, r3
 80013c2:	68bb      	ldr	r3, [r7, #8]
 80013c4:	0a1b      	lsrs	r3, r3, #8
 80013c6:	61fb      	str	r3, [r7, #28]
 80013c8:	69fb      	ldr	r3, [r7, #28]
 80013ca:	fa93 f3a3 	rbit	r3, r3
 80013ce:	61bb      	str	r3, [r7, #24]
 80013d0:	69bb      	ldr	r3, [r7, #24]
 80013d2:	fab3 f383 	clz	r3, r3
 80013d6:	009b      	lsls	r3, r3, #2
 80013d8:	6879      	ldr	r1, [r7, #4]
 80013da:	fa01 f303 	lsl.w	r3, r1, r3
 80013de:	431a      	orrs	r2, r3
 80013e0:	68fb      	ldr	r3, [r7, #12]
 80013e2:	625a      	str	r2, [r3, #36]	; 0x24
 80013e4:	bf00      	nop
 80013e6:	3724      	adds	r7, #36	; 0x24
 80013e8:	46bd      	mov	sp, r7
 80013ea:	f85d 7b04 	ldr.w	r7, [sp], #4
 80013ee:	4770      	bx	lr

080013f0 <LL_GPIO_IsInputPinSet>:
 80013f0:	b480      	push	{r7}
 80013f2:	b083      	sub	sp, #12
 80013f4:	af00      	add	r7, sp, #0
 80013f6:	6078      	str	r0, [r7, #4]
 80013f8:	6039      	str	r1, [r7, #0]
 80013fa:	687b      	ldr	r3, [r7, #4]
 80013fc:	691a      	ldr	r2, [r3, #16]
 80013fe:	683b      	ldr	r3, [r7, #0]
 8001400:	4013      	ands	r3, r2
 8001402:	683a      	ldr	r2, [r7, #0]
 8001404:	429a      	cmp	r2, r3
 8001406:	bf0c      	ite	eq
 8001408:	2301      	moveq	r3, #1
 800140a:	2300      	movne	r3, #0
 800140c:	b2db      	uxtb	r3, r3
 800140e:	4618      	mov	r0, r3
 8001410:	370c      	adds	r7, #12
 8001412:	46bd      	mov	sp, r7
 8001414:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001418:	4770      	bx	lr

0800141a <LL_GPIO_SetOutputPin>:
 800141a:	b480      	push	{r7}
 800141c:	b083      	sub	sp, #12
 800141e:	af00      	add	r7, sp, #0
 8001420:	6078      	str	r0, [r7, #4]
 8001422:	6039      	str	r1, [r7, #0]
 8001424:	687b      	ldr	r3, [r7, #4]
 8001426:	683a      	ldr	r2, [r7, #0]
 8001428:	619a      	str	r2, [r3, #24]
 800142a:	bf00      	nop
 800142c:	370c      	adds	r7, #12
 800142e:	46bd      	mov	sp, r7
 8001430:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001434:	4770      	bx	lr

08001436 <LL_GPIO_ResetOutputPin>:
 8001436:	b480      	push	{r7}
 8001438:	b083      	sub	sp, #12
 800143a:	af00      	add	r7, sp, #0
 800143c:	6078      	str	r0, [r7, #4]
 800143e:	6039      	str	r1, [r7, #0]
 8001440:	683b      	ldr	r3, [r7, #0]
 8001442:	041a      	lsls	r2, r3, #16
 8001444:	687b      	ldr	r3, [r7, #4]
 8001446:	619a      	str	r2, [r3, #24]
 8001448:	bf00      	nop
 800144a:	370c      	adds	r7, #12
 800144c:	46bd      	mov	sp, r7
 800144e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001452:	4770      	bx	lr

08001454 <LL_AHB1_GRP1_EnableClock>:
 8001454:	b480      	push	{r7}
 8001456:	b085      	sub	sp, #20
 8001458:	af00      	add	r7, sp, #0
 800145a:	6078      	str	r0, [r7, #4]
 800145c:	4b08      	ldr	r3, [pc, #32]	; (8001480 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800145e:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8001460:	4907      	ldr	r1, [pc, #28]	; (8001480 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8001462:	687b      	ldr	r3, [r7, #4]
 8001464:	4313      	orrs	r3, r2
 8001466:	630b      	str	r3, [r1, #48]	; 0x30
 8001468:	4b05      	ldr	r3, [pc, #20]	; (8001480 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800146a:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800146c:	687b      	ldr	r3, [r7, #4]
 800146e:	4013      	ands	r3, r2
 8001470:	60fb      	str	r3, [r7, #12]
 8001472:	68fb      	ldr	r3, [r7, #12]
 8001474:	bf00      	nop
 8001476:	3714      	adds	r7, #20
 8001478:	46bd      	mov	sp, r7
 800147a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800147e:	4770      	bx	lr
 8001480:	40023800 	.word	0x40023800

08001484 <LL_APB1_GRP1_EnableClock>:
 8001484:	b480      	push	{r7}
 8001486:	b085      	sub	sp, #20
 8001488:	af00      	add	r7, sp, #0
 800148a:	6078      	str	r0, [r7, #4]
 800148c:	4b08      	ldr	r3, [pc, #32]	; (80014b0 <LL_APB1_GRP1_EnableClock+0x2c>)
 800148e:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8001490:	4907      	ldr	r1, [pc, #28]	; (80014b0 <LL_APB1_GRP1_EnableClock+0x2c>)
 8001492:	687b      	ldr	r3, [r7, #4]
 8001494:	4313      	orrs	r3, r2
 8001496:	640b      	str	r3, [r1, #64]	; 0x40
 8001498:	4b05      	ldr	r3, [pc, #20]	; (80014b0 <LL_APB1_GRP1_EnableClock+0x2c>)
 800149a:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 800149c:	687b      	ldr	r3, [r7, #4]
 800149e:	4013      	ands	r3, r2
 80014a0:	60fb      	str	r3, [r7, #12]
 80014a2:	68fb      	ldr	r3, [r7, #12]
 80014a4:	bf00      	nop
 80014a6:	3714      	adds	r7, #20
 80014a8:	46bd      	mov	sp, r7
 80014aa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80014ae:	4770      	bx	lr
 80014b0:	40023800 	.word	0x40023800

080014b4 <LL_APB2_GRP1_EnableClock>:
 80014b4:	b480      	push	{r7}
 80014b6:	b085      	sub	sp, #20
 80014b8:	af00      	add	r7, sp, #0
 80014ba:	6078      	str	r0, [r7, #4]
 80014bc:	4b08      	ldr	r3, [pc, #32]	; (80014e0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80014be:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80014c0:	4907      	ldr	r1, [pc, #28]	; (80014e0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80014c2:	687b      	ldr	r3, [r7, #4]
 80014c4:	4313      	orrs	r3, r2
 80014c6:	644b      	str	r3, [r1, #68]	; 0x44
 80014c8:	4b05      	ldr	r3, [pc, #20]	; (80014e0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80014ca:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80014cc:	687b      	ldr	r3, [r7, #4]
 80014ce:	4013      	ands	r3, r2
 80014d0:	60fb      	str	r3, [r7, #12]
 80014d2:	68fb      	ldr	r3, [r7, #12]
 80014d4:	bf00      	nop
 80014d6:	3714      	adds	r7, #20
 80014d8:	46bd      	mov	sp, r7
 80014da:	f85d 7b04 	ldr.w	r7, [sp], #4
 80014de:	4770      	bx	lr
 80014e0:	40023800 	.word	0x40023800

080014e4 <LL_TIM_EnableCounter>:
 80014e4:	b480      	push	{r7}
 80014e6:	b083      	sub	sp, #12
 80014e8:	af00      	add	r7, sp, #0
 80014ea:	6078      	str	r0, [r7, #4]
 80014ec:	687b      	ldr	r3, [r7, #4]
 80014ee:	681b      	ldr	r3, [r3, #0]
 80014f0:	f043 0201 	orr.w	r2, r3, #1
 80014f4:	687b      	ldr	r3, [r7, #4]
 80014f6:	601a      	str	r2, [r3, #0]
 80014f8:	bf00      	nop
 80014fa:	370c      	adds	r7, #12
 80014fc:	46bd      	mov	sp, r7
 80014fe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001502:	4770      	bx	lr

08001504 <LL_TIM_DisableCounter>:
 8001504:	b480      	push	{r7}
 8001506:	b083      	sub	sp, #12
 8001508:	af00      	add	r7, sp, #0
 800150a:	6078      	str	r0, [r7, #4]
 800150c:	687b      	ldr	r3, [r7, #4]
 800150e:	681b      	ldr	r3, [r3, #0]
 8001510:	f023 0201 	bic.w	r2, r3, #1
 8001514:	687b      	ldr	r3, [r7, #4]
 8001516:	601a      	str	r2, [r3, #0]
 8001518:	bf00      	nop
 800151a:	370c      	adds	r7, #12
 800151c:	46bd      	mov	sp, r7
 800151e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001522:	4770      	bx	lr

08001524 <LL_TIM_SetCounterMode>:
 8001524:	b480      	push	{r7}
 8001526:	b083      	sub	sp, #12
 8001528:	af00      	add	r7, sp, #0
 800152a:	6078      	str	r0, [r7, #4]
 800152c:	6039      	str	r1, [r7, #0]
 800152e:	687b      	ldr	r3, [r7, #4]
 8001530:	681b      	ldr	r3, [r3, #0]
 8001532:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8001536:	683b      	ldr	r3, [r7, #0]
 8001538:	431a      	orrs	r2, r3
 800153a:	687b      	ldr	r3, [r7, #4]
 800153c:	601a      	str	r2, [r3, #0]
 800153e:	bf00      	nop
 8001540:	370c      	adds	r7, #12
 8001542:	46bd      	mov	sp, r7
 8001544:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001548:	4770      	bx	lr

0800154a <LL_TIM_EnableARRPreload>:
 800154a:	b480      	push	{r7}
 800154c:	b083      	sub	sp, #12
 800154e:	af00      	add	r7, sp, #0
 8001550:	6078      	str	r0, [r7, #4]
 8001552:	687b      	ldr	r3, [r7, #4]
 8001554:	681b      	ldr	r3, [r3, #0]
 8001556:	f043 0280 	orr.w	r2, r3, #128	; 0x80
 800155a:	687b      	ldr	r3, [r7, #4]
 800155c:	601a      	str	r2, [r3, #0]
 800155e:	bf00      	nop
 8001560:	370c      	adds	r7, #12
 8001562:	46bd      	mov	sp, r7
 8001564:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001568:	4770      	bx	lr

0800156a <LL_TIM_SetPrescaler>:
 800156a:	b480      	push	{r7}
 800156c:	b083      	sub	sp, #12
 800156e:	af00      	add	r7, sp, #0
 8001570:	6078      	str	r0, [r7, #4]
 8001572:	6039      	str	r1, [r7, #0]
 8001574:	687b      	ldr	r3, [r7, #4]
 8001576:	683a      	ldr	r2, [r7, #0]
 8001578:	629a      	str	r2, [r3, #40]	; 0x28
 800157a:	bf00      	nop
 800157c:	370c      	adds	r7, #12
 800157e:	46bd      	mov	sp, r7
 8001580:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001584:	4770      	bx	lr

08001586 <LL_TIM_SetAutoReload>:
 8001586:	b480      	push	{r7}
 8001588:	b083      	sub	sp, #12
 800158a:	af00      	add	r7, sp, #0
 800158c:	6078      	str	r0, [r7, #4]
 800158e:	6039      	str	r1, [r7, #0]
 8001590:	687b      	ldr	r3, [r7, #4]
 8001592:	683a      	ldr	r2, [r7, #0]
 8001594:	62da      	str	r2, [r3, #44]	; 0x2c
 8001596:	bf00      	nop
 8001598:	370c      	adds	r7, #12
 800159a:	46bd      	mov	sp, r7
 800159c:	f85d 7b04 	ldr.w	r7, [sp], #4
 80015a0:	4770      	bx	lr

080015a2 <LL_TIM_CC_EnableChannel>:
 80015a2:	b480      	push	{r7}
 80015a4:	b083      	sub	sp, #12
 80015a6:	af00      	add	r7, sp, #0
 80015a8:	6078      	str	r0, [r7, #4]
 80015aa:	6039      	str	r1, [r7, #0]
 80015ac:	687b      	ldr	r3, [r7, #4]
 80015ae:	6a1a      	ldr	r2, [r3, #32]
 80015b0:	683b      	ldr	r3, [r7, #0]
 80015b2:	431a      	orrs	r2, r3
 80015b4:	687b      	ldr	r3, [r7, #4]
 80015b6:	621a      	str	r2, [r3, #32]
 80015b8:	bf00      	nop
 80015ba:	370c      	adds	r7, #12
 80015bc:	46bd      	mov	sp, r7
 80015be:	f85d 7b04 	ldr.w	r7, [sp], #4
 80015c2:	4770      	bx	lr

080015c4 <LL_TIM_OC_SetMode>:
 80015c4:	b4b0      	push	{r4, r5, r7}
 80015c6:	b085      	sub	sp, #20
 80015c8:	af00      	add	r7, sp, #0
 80015ca:	60f8      	str	r0, [r7, #12]
 80015cc:	60b9      	str	r1, [r7, #8]
 80015ce:	607a      	str	r2, [r7, #4]
 80015d0:	68bb      	ldr	r3, [r7, #8]
 80015d2:	2b01      	cmp	r3, #1
 80015d4:	d01c      	beq.n	8001610 <LL_TIM_OC_SetMode+0x4c>
 80015d6:	68bb      	ldr	r3, [r7, #8]
 80015d8:	2b04      	cmp	r3, #4
 80015da:	d017      	beq.n	800160c <LL_TIM_OC_SetMode+0x48>
 80015dc:	68bb      	ldr	r3, [r7, #8]
 80015de:	2b10      	cmp	r3, #16
 80015e0:	d012      	beq.n	8001608 <LL_TIM_OC_SetMode+0x44>
 80015e2:	68bb      	ldr	r3, [r7, #8]
 80015e4:	2b40      	cmp	r3, #64	; 0x40
 80015e6:	d00d      	beq.n	8001604 <LL_TIM_OC_SetMode+0x40>
 80015e8:	68bb      	ldr	r3, [r7, #8]
 80015ea:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 80015ee:	d007      	beq.n	8001600 <LL_TIM_OC_SetMode+0x3c>
 80015f0:	68bb      	ldr	r3, [r7, #8]
 80015f2:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 80015f6:	d101      	bne.n	80015fc <LL_TIM_OC_SetMode+0x38>
 80015f8:	2305      	movs	r3, #5
 80015fa:	e00a      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 80015fc:	2306      	movs	r3, #6
 80015fe:	e008      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 8001600:	2304      	movs	r3, #4
 8001602:	e006      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 8001604:	2303      	movs	r3, #3
 8001606:	e004      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 8001608:	2302      	movs	r3, #2
 800160a:	e002      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 800160c:	2301      	movs	r3, #1
 800160e:	e000      	b.n	8001612 <LL_TIM_OC_SetMode+0x4e>
 8001610:	2300      	movs	r3, #0
 8001612:	461d      	mov	r5, r3
 8001614:	68fb      	ldr	r3, [r7, #12]
 8001616:	3318      	adds	r3, #24
 8001618:	461a      	mov	r2, r3
 800161a:	4629      	mov	r1, r5
 800161c:	4b0c      	ldr	r3, [pc, #48]	; (8001650 <LL_TIM_OC_SetMode+0x8c>)
 800161e:	5c5b      	ldrb	r3, [r3, r1]
 8001620:	4413      	add	r3, r2
 8001622:	461c      	mov	r4, r3
 8001624:	6822      	ldr	r2, [r4, #0]
 8001626:	4629      	mov	r1, r5
 8001628:	4b0a      	ldr	r3, [pc, #40]	; (8001654 <LL_TIM_OC_SetMode+0x90>)
 800162a:	5c5b      	ldrb	r3, [r3, r1]
 800162c:	4619      	mov	r1, r3
 800162e:	2373      	movs	r3, #115	; 0x73
 8001630:	408b      	lsls	r3, r1
 8001632:	43db      	mvns	r3, r3
 8001634:	401a      	ands	r2, r3
 8001636:	4629      	mov	r1, r5
 8001638:	4b06      	ldr	r3, [pc, #24]	; (8001654 <LL_TIM_OC_SetMode+0x90>)
 800163a:	5c5b      	ldrb	r3, [r3, r1]
 800163c:	4619      	mov	r1, r3
 800163e:	687b      	ldr	r3, [r7, #4]
 8001640:	408b      	lsls	r3, r1
 8001642:	4313      	orrs	r3, r2
 8001644:	6023      	str	r3, [r4, #0]
 8001646:	bf00      	nop
 8001648:	3714      	adds	r7, #20
 800164a:	46bd      	mov	sp, r7
 800164c:	bcb0      	pop	{r4, r5, r7}
 800164e:	4770      	bx	lr
 8001650:	0800d118 	.word	0x0800d118
 8001654:	0800d120 	.word	0x0800d120

08001658 <LL_TIM_OC_EnableFast>:
 8001658:	b4b0      	push	{r4, r5, r7}
 800165a:	b083      	sub	sp, #12
 800165c:	af00      	add	r7, sp, #0
 800165e:	6078      	str	r0, [r7, #4]
 8001660:	6039      	str	r1, [r7, #0]
 8001662:	683b      	ldr	r3, [r7, #0]
 8001664:	2b01      	cmp	r3, #1
 8001666:	d01c      	beq.n	80016a2 <LL_TIM_OC_EnableFast+0x4a>
 8001668:	683b      	ldr	r3, [r7, #0]
 800166a:	2b04      	cmp	r3, #4
 800166c:	d017      	beq.n	800169e <LL_TIM_OC_EnableFast+0x46>
 800166e:	683b      	ldr	r3, [r7, #0]
 8001670:	2b10      	cmp	r3, #16
 8001672:	d012      	beq.n	800169a <LL_TIM_OC_EnableFast+0x42>
 8001674:	683b      	ldr	r3, [r7, #0]
 8001676:	2b40      	cmp	r3, #64	; 0x40
 8001678:	d00d      	beq.n	8001696 <LL_TIM_OC_EnableFast+0x3e>
 800167a:	683b      	ldr	r3, [r7, #0]
 800167c:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8001680:	d007      	beq.n	8001692 <LL_TIM_OC_EnableFast+0x3a>
 8001682:	683b      	ldr	r3, [r7, #0]
 8001684:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8001688:	d101      	bne.n	800168e <LL_TIM_OC_EnableFast+0x36>
 800168a:	2305      	movs	r3, #5
 800168c:	e00a      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 800168e:	2306      	movs	r3, #6
 8001690:	e008      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 8001692:	2304      	movs	r3, #4
 8001694:	e006      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 8001696:	2303      	movs	r3, #3
 8001698:	e004      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 800169a:	2302      	movs	r3, #2
 800169c:	e002      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 800169e:	2301      	movs	r3, #1
 80016a0:	e000      	b.n	80016a4 <LL_TIM_OC_EnableFast+0x4c>
 80016a2:	2300      	movs	r3, #0
 80016a4:	461d      	mov	r5, r3
 80016a6:	687b      	ldr	r3, [r7, #4]
 80016a8:	3318      	adds	r3, #24
 80016aa:	461a      	mov	r2, r3
 80016ac:	4629      	mov	r1, r5
 80016ae:	4b09      	ldr	r3, [pc, #36]	; (80016d4 <LL_TIM_OC_EnableFast+0x7c>)
 80016b0:	5c5b      	ldrb	r3, [r3, r1]
 80016b2:	4413      	add	r3, r2
 80016b4:	461c      	mov	r4, r3
 80016b6:	6822      	ldr	r2, [r4, #0]
 80016b8:	4629      	mov	r1, r5
 80016ba:	4b07      	ldr	r3, [pc, #28]	; (80016d8 <LL_TIM_OC_EnableFast+0x80>)
 80016bc:	5c5b      	ldrb	r3, [r3, r1]
 80016be:	4619      	mov	r1, r3
 80016c0:	2304      	movs	r3, #4
 80016c2:	408b      	lsls	r3, r1
 80016c4:	4313      	orrs	r3, r2
 80016c6:	6023      	str	r3, [r4, #0]
 80016c8:	bf00      	nop
 80016ca:	370c      	adds	r7, #12
 80016cc:	46bd      	mov	sp, r7
 80016ce:	bcb0      	pop	{r4, r5, r7}
 80016d0:	4770      	bx	lr
 80016d2:	bf00      	nop
 80016d4:	0800d118 	.word	0x0800d118
 80016d8:	0800d120 	.word	0x0800d120

080016dc <LL_TIM_OC_EnablePreload>:
 80016dc:	b4b0      	push	{r4, r5, r7}
 80016de:	b083      	sub	sp, #12
 80016e0:	af00      	add	r7, sp, #0
 80016e2:	6078      	str	r0, [r7, #4]
 80016e4:	6039      	str	r1, [r7, #0]
 80016e6:	683b      	ldr	r3, [r7, #0]
 80016e8:	2b01      	cmp	r3, #1
 80016ea:	d01c      	beq.n	8001726 <LL_TIM_OC_EnablePreload+0x4a>
 80016ec:	683b      	ldr	r3, [r7, #0]
 80016ee:	2b04      	cmp	r3, #4
 80016f0:	d017      	beq.n	8001722 <LL_TIM_OC_EnablePreload+0x46>
 80016f2:	683b      	ldr	r3, [r7, #0]
 80016f4:	2b10      	cmp	r3, #16
 80016f6:	d012      	beq.n	800171e <LL_TIM_OC_EnablePreload+0x42>
 80016f8:	683b      	ldr	r3, [r7, #0]
 80016fa:	2b40      	cmp	r3, #64	; 0x40
 80016fc:	d00d      	beq.n	800171a <LL_TIM_OC_EnablePreload+0x3e>
 80016fe:	683b      	ldr	r3, [r7, #0]
 8001700:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8001704:	d007      	beq.n	8001716 <LL_TIM_OC_EnablePreload+0x3a>
 8001706:	683b      	ldr	r3, [r7, #0]
 8001708:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 800170c:	d101      	bne.n	8001712 <LL_TIM_OC_EnablePreload+0x36>
 800170e:	2305      	movs	r3, #5
 8001710:	e00a      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 8001712:	2306      	movs	r3, #6
 8001714:	e008      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 8001716:	2304      	movs	r3, #4
 8001718:	e006      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 800171a:	2303      	movs	r3, #3
 800171c:	e004      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 800171e:	2302      	movs	r3, #2
 8001720:	e002      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 8001722:	2301      	movs	r3, #1
 8001724:	e000      	b.n	8001728 <LL_TIM_OC_EnablePreload+0x4c>
 8001726:	2300      	movs	r3, #0
 8001728:	461d      	mov	r5, r3
 800172a:	687b      	ldr	r3, [r7, #4]
 800172c:	3318      	adds	r3, #24
 800172e:	461a      	mov	r2, r3
 8001730:	4629      	mov	r1, r5
 8001732:	4b09      	ldr	r3, [pc, #36]	; (8001758 <LL_TIM_OC_EnablePreload+0x7c>)
 8001734:	5c5b      	ldrb	r3, [r3, r1]
 8001736:	4413      	add	r3, r2
 8001738:	461c      	mov	r4, r3
 800173a:	6822      	ldr	r2, [r4, #0]
 800173c:	4629      	mov	r1, r5
 800173e:	4b07      	ldr	r3, [pc, #28]	; (800175c <LL_TIM_OC_EnablePreload+0x80>)
 8001740:	5c5b      	ldrb	r3, [r3, r1]
 8001742:	4619      	mov	r1, r3
 8001744:	2308      	movs	r3, #8
 8001746:	408b      	lsls	r3, r1
 8001748:	4313      	orrs	r3, r2
 800174a:	6023      	str	r3, [r4, #0]
 800174c:	bf00      	nop
 800174e:	370c      	adds	r7, #12
 8001750:	46bd      	mov	sp, r7
 8001752:	bcb0      	pop	{r4, r5, r7}
 8001754:	4770      	bx	lr
 8001756:	bf00      	nop
 8001758:	0800d118 	.word	0x0800d118
 800175c:	0800d120 	.word	0x0800d120

08001760 <LL_TIM_OC_SetCompareCH1>:
 8001760:	b480      	push	{r7}
 8001762:	b083      	sub	sp, #12
 8001764:	af00      	add	r7, sp, #0
 8001766:	6078      	str	r0, [r7, #4]
 8001768:	6039      	str	r1, [r7, #0]
 800176a:	687b      	ldr	r3, [r7, #4]
 800176c:	683a      	ldr	r2, [r7, #0]
 800176e:	635a      	str	r2, [r3, #52]	; 0x34
 8001770:	bf00      	nop
 8001772:	370c      	adds	r7, #12
 8001774:	46bd      	mov	sp, r7
 8001776:	f85d 7b04 	ldr.w	r7, [sp], #4
 800177a:	4770      	bx	lr

0800177c <LL_TIM_ClearFlag_UPDATE>:
 800177c:	b480      	push	{r7}
 800177e:	b083      	sub	sp, #12
 8001780:	af00      	add	r7, sp, #0
 8001782:	6078      	str	r0, [r7, #4]
 8001784:	687b      	ldr	r3, [r7, #4]
 8001786:	f06f 0201 	mvn.w	r2, #1
 800178a:	611a      	str	r2, [r3, #16]
 800178c:	bf00      	nop
 800178e:	370c      	adds	r7, #12
 8001790:	46bd      	mov	sp, r7
 8001792:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001796:	4770      	bx	lr

08001798 <LL_TIM_IsActiveFlag_UPDATE>:
 8001798:	b480      	push	{r7}
 800179a:	b083      	sub	sp, #12
 800179c:	af00      	add	r7, sp, #0
 800179e:	6078      	str	r0, [r7, #4]
 80017a0:	687b      	ldr	r3, [r7, #4]
 80017a2:	691b      	ldr	r3, [r3, #16]
 80017a4:	f003 0301 	and.w	r3, r3, #1
 80017a8:	2b01      	cmp	r3, #1
 80017aa:	bf0c      	ite	eq
 80017ac:	2301      	moveq	r3, #1
 80017ae:	2300      	movne	r3, #0
 80017b0:	b2db      	uxtb	r3, r3
 80017b2:	4618      	mov	r0, r3
 80017b4:	370c      	adds	r7, #12
 80017b6:	46bd      	mov	sp, r7
 80017b8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80017bc:	4770      	bx	lr

080017be <LL_TIM_EnableIT_UPDATE>:
 80017be:	b480      	push	{r7}
 80017c0:	b083      	sub	sp, #12
 80017c2:	af00      	add	r7, sp, #0
 80017c4:	6078      	str	r0, [r7, #4]
 80017c6:	687b      	ldr	r3, [r7, #4]
 80017c8:	68db      	ldr	r3, [r3, #12]
 80017ca:	f043 0201 	orr.w	r2, r3, #1
 80017ce:	687b      	ldr	r3, [r7, #4]
 80017d0:	60da      	str	r2, [r3, #12]
 80017d2:	bf00      	nop
 80017d4:	370c      	adds	r7, #12
 80017d6:	46bd      	mov	sp, r7
 80017d8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80017dc:	4770      	bx	lr

080017de <LL_TIM_GenerateEvent_UPDATE>:
 80017de:	b480      	push	{r7}
 80017e0:	b083      	sub	sp, #12
 80017e2:	af00      	add	r7, sp, #0
 80017e4:	6078      	str	r0, [r7, #4]
 80017e6:	687b      	ldr	r3, [r7, #4]
 80017e8:	695b      	ldr	r3, [r3, #20]
 80017ea:	f043 0201 	orr.w	r2, r3, #1
 80017ee:	687b      	ldr	r3, [r7, #4]
 80017f0:	615a      	str	r2, [r3, #20]
 80017f2:	bf00      	nop
 80017f4:	370c      	adds	r7, #12
 80017f6:	46bd      	mov	sp, r7
 80017f8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80017fc:	4770      	bx	lr
	...

08001800 <stm_driver_send_msg>:
 8001800:	b580      	push	{r7, lr}
 8001802:	b084      	sub	sp, #16
 8001804:	af00      	add	r7, sp, #0
 8001806:	6078      	str	r0, [r7, #4]
 8001808:	6039      	str	r1, [r7, #0]
 800180a:	2300      	movs	r3, #0
 800180c:	60fb      	str	r3, [r7, #12]
 800180e:	4813      	ldr	r0, [pc, #76]	; (800185c <stm_driver_send_msg+0x5c>)
 8001810:	f7ff fd48 	bl	80012a4 <LL_USART_ClearFlag_TC>
 8001814:	e011      	b.n	800183a <stm_driver_send_msg+0x3a>
 8001816:	bf00      	nop
 8001818:	4810      	ldr	r0, [pc, #64]	; (800185c <stm_driver_send_msg+0x5c>)
 800181a:	f7ff fd30 	bl	800127e <LL_USART_IsActiveFlag_TXE>
 800181e:	4603      	mov	r3, r0
 8001820:	2b00      	cmp	r3, #0
 8001822:	d0f9      	beq.n	8001818 <stm_driver_send_msg+0x18>
 8001824:	68fb      	ldr	r3, [r7, #12]
 8001826:	1c5a      	adds	r2, r3, #1
 8001828:	60fa      	str	r2, [r7, #12]
 800182a:	461a      	mov	r2, r3
 800182c:	687b      	ldr	r3, [r7, #4]
 800182e:	4413      	add	r3, r2
 8001830:	781b      	ldrb	r3, [r3, #0]
 8001832:	4619      	mov	r1, r3
 8001834:	4809      	ldr	r0, [pc, #36]	; (800185c <stm_driver_send_msg+0x5c>)
 8001836:	f7ff fd43 	bl	80012c0 <LL_USART_TransmitData8>
 800183a:	683b      	ldr	r3, [r7, #0]
 800183c:	1e5a      	subs	r2, r3, #1
 800183e:	603a      	str	r2, [r7, #0]
 8001840:	2b00      	cmp	r3, #0
 8001842:	d1e8      	bne.n	8001816 <stm_driver_send_msg+0x16>
 8001844:	bf00      	nop
 8001846:	4805      	ldr	r0, [pc, #20]	; (800185c <stm_driver_send_msg+0x5c>)
 8001848:	f7ff fd06 	bl	8001258 <LL_USART_IsActiveFlag_TC>
 800184c:	4603      	mov	r3, r0
 800184e:	2b00      	cmp	r3, #0
 8001850:	d0f9      	beq.n	8001846 <stm_driver_send_msg+0x46>
 8001852:	bf00      	nop
 8001854:	3710      	adds	r7, #16
 8001856:	46bd      	mov	sp, r7
 8001858:	bd80      	pop	{r7, pc}
 800185a:	bf00      	nop
 800185c:	40011000 	.word	0x40011000

08001860 <manip_dyn_stop>:
 8001860:	b580      	push	{r7, lr}
 8001862:	b086      	sub	sp, #24
 8001864:	af00      	add	r7, sp, #0
 8001866:	1d3b      	adds	r3, r7, #4
 8001868:	2200      	movs	r2, #0
 800186a:	601a      	str	r2, [r3, #0]
 800186c:	605a      	str	r2, [r3, #4]
 800186e:	609a      	str	r2, [r3, #8]
 8001870:	60da      	str	r2, [r3, #12]
 8001872:	821a      	strh	r2, [r3, #16]
 8001874:	2302      	movs	r3, #2
 8001876:	713b      	strb	r3, [r7, #4]
 8001878:	1d3b      	adds	r3, r7, #4
 800187a:	2112      	movs	r1, #18
 800187c:	4618      	mov	r0, r3
 800187e:	f7ff ffbf 	bl	8001800 <stm_driver_send_msg>
 8001882:	bf00      	nop
 8001884:	3718      	adds	r7, #24
 8001886:	46bd      	mov	sp, r7
 8001888:	bd80      	pop	{r7, pc}
	...

0800188c <manip_hw_config>:
 800188c:	b580      	push	{r7, lr}
 800188e:	af00      	add	r7, sp, #0
 8001890:	2001      	movs	r0, #1
 8001892:	f7ff fddf 	bl	8001454 <LL_AHB1_GRP1_EnableClock>
 8001896:	2201      	movs	r2, #1
 8001898:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800189c:	484f      	ldr	r0, [pc, #316]	; (80019dc <manip_hw_config+0x150>)
 800189e:	f7ff fd1e 	bl	80012de <LL_GPIO_SetPinMode>
 80018a2:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 80018a6:	484d      	ldr	r0, [pc, #308]	; (80019dc <manip_hw_config+0x150>)
 80018a8:	f7ff fdc5 	bl	8001436 <LL_GPIO_ResetOutputPin>
 80018ac:	2002      	movs	r0, #2
 80018ae:	f7ff fdd1 	bl	8001454 <LL_AHB1_GRP1_EnableClock>
 80018b2:	2202      	movs	r2, #2
 80018b4:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80018b8:	4849      	ldr	r0, [pc, #292]	; (80019e0 <manip_hw_config+0x154>)
 80018ba:	f7ff fd10 	bl	80012de <LL_GPIO_SetPinMode>
 80018be:	2209      	movs	r2, #9
 80018c0:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80018c4:	4846      	ldr	r0, [pc, #280]	; (80019e0 <manip_hw_config+0x154>)
 80018c6:	f7ff fd64 	bl	8001392 <LL_GPIO_SetAFPin_8_15>
 80018ca:	2040      	movs	r0, #64	; 0x40
 80018cc:	f7ff fdda 	bl	8001484 <LL_APB1_GRP1_EnableClock>
 80018d0:	2100      	movs	r1, #0
 80018d2:	4844      	ldr	r0, [pc, #272]	; (80019e4 <manip_hw_config+0x158>)
 80018d4:	f7ff fe26 	bl	8001524 <LL_TIM_SetCounterMode>
 80018d8:	f240 3147 	movw	r1, #839	; 0x347
 80018dc:	4841      	ldr	r0, [pc, #260]	; (80019e4 <manip_hw_config+0x158>)
 80018de:	f7ff fe44 	bl	800156a <LL_TIM_SetPrescaler>
 80018e2:	f240 71cf 	movw	r1, #1999	; 0x7cf
 80018e6:	483f      	ldr	r0, [pc, #252]	; (80019e4 <manip_hw_config+0x158>)
 80018e8:	f7ff fe4d 	bl	8001586 <LL_TIM_SetAutoReload>
 80018ec:	2101      	movs	r1, #1
 80018ee:	483d      	ldr	r0, [pc, #244]	; (80019e4 <manip_hw_config+0x158>)
 80018f0:	f7ff fe57 	bl	80015a2 <LL_TIM_CC_EnableChannel>
 80018f4:	2101      	movs	r1, #1
 80018f6:	483b      	ldr	r0, [pc, #236]	; (80019e4 <manip_hw_config+0x158>)
 80018f8:	f7ff feae 	bl	8001658 <LL_TIM_OC_EnableFast>
 80018fc:	2101      	movs	r1, #1
 80018fe:	4839      	ldr	r0, [pc, #228]	; (80019e4 <manip_hw_config+0x158>)
 8001900:	f7ff feec 	bl	80016dc <LL_TIM_OC_EnablePreload>
 8001904:	4837      	ldr	r0, [pc, #220]	; (80019e4 <manip_hw_config+0x158>)
 8001906:	f7ff fe20 	bl	800154a <LL_TIM_EnableARRPreload>
 800190a:	2260      	movs	r2, #96	; 0x60
 800190c:	2101      	movs	r1, #1
 800190e:	4835      	ldr	r0, [pc, #212]	; (80019e4 <manip_hw_config+0x158>)
 8001910:	f7ff fe58 	bl	80015c4 <LL_TIM_OC_SetMode>
 8001914:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
 8001918:	4832      	ldr	r0, [pc, #200]	; (80019e4 <manip_hw_config+0x158>)
 800191a:	f7ff ff21 	bl	8001760 <LL_TIM_OC_SetCompareCH1>
 800191e:	4831      	ldr	r0, [pc, #196]	; (80019e4 <manip_hw_config+0x158>)
 8001920:	f7ff ff5d 	bl	80017de <LL_TIM_GenerateEvent_UPDATE>
 8001924:	f44f 3000 	mov.w	r0, #131072	; 0x20000
 8001928:	f7ff fdc4 	bl	80014b4 <LL_APB2_GRP1_EnableClock>
 800192c:	f240 31e7 	movw	r1, #999	; 0x3e7
 8001930:	482d      	ldr	r0, [pc, #180]	; (80019e8 <manip_hw_config+0x15c>)
 8001932:	f7ff fe28 	bl	8001586 <LL_TIM_SetAutoReload>
 8001936:	21a7      	movs	r1, #167	; 0xa7
 8001938:	482b      	ldr	r0, [pc, #172]	; (80019e8 <manip_hw_config+0x15c>)
 800193a:	f7ff fe16 	bl	800156a <LL_TIM_SetPrescaler>
 800193e:	2100      	movs	r1, #0
 8001940:	4829      	ldr	r0, [pc, #164]	; (80019e8 <manip_hw_config+0x15c>)
 8001942:	f7ff fdef 	bl	8001524 <LL_TIM_SetCounterMode>
 8001946:	4828      	ldr	r0, [pc, #160]	; (80019e8 <manip_hw_config+0x15c>)
 8001948:	f7ff ff39 	bl	80017be <LL_TIM_EnableIT_UPDATE>
 800194c:	2107      	movs	r1, #7
 800194e:	2019      	movs	r0, #25
 8001950:	f7ff fc58 	bl	8001204 <NVIC_SetPriority>
 8001954:	2019      	movs	r0, #25
 8001956:	f7ff fc3b 	bl	80011d0 <NVIC_EnableIRQ>
 800195a:	2010      	movs	r0, #16
 800195c:	f7ff fd7a 	bl	8001454 <LL_AHB1_GRP1_EnableClock>
 8001960:	2200      	movs	r2, #0
 8001962:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8001966:	4821      	ldr	r0, [pc, #132]	; (80019ec <manip_hw_config+0x160>)
 8001968:	f7ff fcb9 	bl	80012de <LL_GPIO_SetPinMode>
 800196c:	2200      	movs	r2, #0
 800196e:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8001972:	481e      	ldr	r0, [pc, #120]	; (80019ec <manip_hw_config+0x160>)
 8001974:	f7ff fce0 	bl	8001338 <LL_GPIO_SetPinPull>
 8001978:	2200      	movs	r2, #0
 800197a:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800197e:	481b      	ldr	r0, [pc, #108]	; (80019ec <manip_hw_config+0x160>)
 8001980:	f7ff fcad 	bl	80012de <LL_GPIO_SetPinMode>
 8001984:	2200      	movs	r2, #0
 8001986:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800198a:	4818      	ldr	r0, [pc, #96]	; (80019ec <manip_hw_config+0x160>)
 800198c:	f7ff fcd4 	bl	8001338 <LL_GPIO_SetPinPull>
 8001990:	2200      	movs	r2, #0
 8001992:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8001996:	4815      	ldr	r0, [pc, #84]	; (80019ec <manip_hw_config+0x160>)
 8001998:	f7ff fca1 	bl	80012de <LL_GPIO_SetPinMode>
 800199c:	2200      	movs	r2, #0
 800199e:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 80019a2:	4812      	ldr	r0, [pc, #72]	; (80019ec <manip_hw_config+0x160>)
 80019a4:	f7ff fcc8 	bl	8001338 <LL_GPIO_SetPinPull>
 80019a8:	2200      	movs	r2, #0
 80019aa:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80019ae:	480f      	ldr	r0, [pc, #60]	; (80019ec <manip_hw_config+0x160>)
 80019b0:	f7ff fc95 	bl	80012de <LL_GPIO_SetPinMode>
 80019b4:	2200      	movs	r2, #0
 80019b6:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80019ba:	480c      	ldr	r0, [pc, #48]	; (80019ec <manip_hw_config+0x160>)
 80019bc:	f7ff fcbc 	bl	8001338 <LL_GPIO_SetPinPull>
 80019c0:	2200      	movs	r2, #0
 80019c2:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80019c6:	4809      	ldr	r0, [pc, #36]	; (80019ec <manip_hw_config+0x160>)
 80019c8:	f7ff fc89 	bl	80012de <LL_GPIO_SetPinMode>
 80019cc:	2200      	movs	r2, #0
 80019ce:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80019d2:	4806      	ldr	r0, [pc, #24]	; (80019ec <manip_hw_config+0x160>)
 80019d4:	f7ff fcb0 	bl	8001338 <LL_GPIO_SetPinPull>
 80019d8:	bf00      	nop
 80019da:	bd80      	pop	{r7, pc}
 80019dc:	40020000 	.word	0x40020000
 80019e0:	40020400 	.word	0x40020400
 80019e4:	40001800 	.word	0x40001800
 80019e8:	40014400 	.word	0x40014400
 80019ec:	40021000 	.word	0x40021000

080019f0 <stick_enable_torque>:
 80019f0:	b580      	push	{r7, lr}
 80019f2:	af00      	add	r7, sp, #0
 80019f4:	4802      	ldr	r0, [pc, #8]	; (8001a00 <stick_enable_torque+0x10>)
 80019f6:	f7ff fd75 	bl	80014e4 <LL_TIM_EnableCounter>
 80019fa:	bf00      	nop
 80019fc:	bd80      	pop	{r7, pc}
 80019fe:	bf00      	nop
 8001a00:	40001800 	.word	0x40001800

08001a04 <stick_disable_torque>:
 8001a04:	b580      	push	{r7, lr}
 8001a06:	af00      	add	r7, sp, #0
 8001a08:	4802      	ldr	r0, [pc, #8]	; (8001a14 <stick_disable_torque+0x10>)
 8001a0a:	f7ff fd7b 	bl	8001504 <LL_TIM_DisableCounter>
 8001a0e:	bf00      	nop
 8001a10:	bd80      	pop	{r7, pc}
 8001a12:	bf00      	nop
 8001a14:	40001800 	.word	0x40001800

08001a18 <stick_set_angle>:
 8001a18:	b580      	push	{r7, lr}
 8001a1a:	b082      	sub	sp, #8
 8001a1c:	af00      	add	r7, sp, #0
 8001a1e:	4603      	mov	r3, r0
 8001a20:	71fb      	strb	r3, [r7, #7]
 8001a22:	79fb      	ldrb	r3, [r7, #7]
 8001a24:	4619      	mov	r1, r3
 8001a26:	4803      	ldr	r0, [pc, #12]	; (8001a34 <stick_set_angle+0x1c>)
 8001a28:	f7ff fe9a 	bl	8001760 <LL_TIM_OC_SetCompareCH1>
 8001a2c:	bf00      	nop
 8001a2e:	3708      	adds	r7, #8
 8001a30:	46bd      	mov	sp, r7
 8001a32:	bd80      	pop	{r7, pc}
 8001a34:	40001800 	.word	0x40001800

08001a38 <dyn_update>:
 8001a38:	b480      	push	{r7}
 8001a3a:	b083      	sub	sp, #12
 8001a3c:	af00      	add	r7, sp, #0
 8001a3e:	2300      	movs	r3, #0
 8001a40:	607b      	str	r3, [r7, #4]
 8001a42:	4b38      	ldr	r3, [pc, #224]	; (8001b24 <dyn_update+0xec>)
 8001a44:	681b      	ldr	r3, [r3, #0]
 8001a46:	3330      	adds	r3, #48	; 0x30
 8001a48:	603b      	str	r3, [r7, #0]
 8001a4a:	4b36      	ldr	r3, [pc, #216]	; (8001b24 <dyn_update+0xec>)
 8001a4c:	681b      	ldr	r3, [r3, #0]
 8001a4e:	789b      	ldrb	r3, [r3, #2]
 8001a50:	607b      	str	r3, [r7, #4]
 8001a52:	e05a      	b.n	8001b0a <dyn_update+0xd2>
 8001a54:	4b33      	ldr	r3, [pc, #204]	; (8001b24 <dyn_update+0xec>)
 8001a56:	681b      	ldr	r3, [r3, #0]
 8001a58:	6899      	ldr	r1, [r3, #8]
 8001a5a:	687a      	ldr	r2, [r7, #4]
 8001a5c:	4613      	mov	r3, r2
 8001a5e:	00db      	lsls	r3, r3, #3
 8001a60:	4413      	add	r3, r2
 8001a62:	009b      	lsls	r3, r3, #2
 8001a64:	461a      	mov	r2, r3
 8001a66:	683b      	ldr	r3, [r7, #0]
 8001a68:	4413      	add	r3, r2
 8001a6a:	68db      	ldr	r3, [r3, #12]
 8001a6c:	1ac9      	subs	r1, r1, r3
 8001a6e:	687a      	ldr	r2, [r7, #4]
 8001a70:	4613      	mov	r3, r2
 8001a72:	00db      	lsls	r3, r3, #3
 8001a74:	4413      	add	r3, r2
 8001a76:	009b      	lsls	r3, r3, #2
 8001a78:	461a      	mov	r2, r3
 8001a7a:	683b      	ldr	r3, [r7, #0]
 8001a7c:	4413      	add	r3, r2
 8001a7e:	689b      	ldr	r3, [r3, #8]
 8001a80:	4299      	cmp	r1, r3
 8001a82:	d33f      	bcc.n	8001b04 <dyn_update+0xcc>
 8001a84:	687a      	ldr	r2, [r7, #4]
 8001a86:	4613      	mov	r3, r2
 8001a88:	00db      	lsls	r3, r3, #3
 8001a8a:	4413      	add	r3, r2
 8001a8c:	009b      	lsls	r3, r3, #2
 8001a8e:	461a      	mov	r2, r3
 8001a90:	683b      	ldr	r3, [r7, #0]
 8001a92:	4413      	add	r3, r2
 8001a94:	7c1b      	ldrb	r3, [r3, #16]
 8001a96:	2b00      	cmp	r3, #0
 8001a98:	d034      	beq.n	8001b04 <dyn_update+0xcc>
 8001a9a:	687a      	ldr	r2, [r7, #4]
 8001a9c:	4613      	mov	r3, r2
 8001a9e:	00db      	lsls	r3, r3, #3
 8001aa0:	4413      	add	r3, r2
 8001aa2:	009b      	lsls	r3, r3, #2
 8001aa4:	461a      	mov	r2, r3
 8001aa6:	683b      	ldr	r3, [r7, #0]
 8001aa8:	1899      	adds	r1, r3, r2
 8001aaa:	687a      	ldr	r2, [r7, #4]
 8001aac:	4613      	mov	r3, r2
 8001aae:	00db      	lsls	r3, r3, #3
 8001ab0:	4413      	add	r3, r2
 8001ab2:	009b      	lsls	r3, r3, #2
 8001ab4:	461a      	mov	r2, r3
 8001ab6:	683b      	ldr	r3, [r7, #0]
 8001ab8:	4413      	add	r3, r2
 8001aba:	888a      	ldrh	r2, [r1, #4]
 8001abc:	805a      	strh	r2, [r3, #2]
 8001abe:	687a      	ldr	r2, [r7, #4]
 8001ac0:	4613      	mov	r3, r2
 8001ac2:	00db      	lsls	r3, r3, #3
 8001ac4:	4413      	add	r3, r2
 8001ac6:	009b      	lsls	r3, r3, #2
 8001ac8:	461a      	mov	r2, r3
 8001aca:	683b      	ldr	r3, [r7, #0]
 8001acc:	4413      	add	r3, r2
 8001ace:	2201      	movs	r2, #1
 8001ad0:	745a      	strb	r2, [r3, #17]
 8001ad2:	4b14      	ldr	r3, [pc, #80]	; (8001b24 <dyn_update+0xec>)
 8001ad4:	681b      	ldr	r3, [r3, #0]
 8001ad6:	78db      	ldrb	r3, [r3, #3]
 8001ad8:	b259      	sxtb	r1, r3
 8001ada:	687a      	ldr	r2, [r7, #4]
 8001adc:	4613      	mov	r3, r2
 8001ade:	00db      	lsls	r3, r3, #3
 8001ae0:	4413      	add	r3, r2
 8001ae2:	009b      	lsls	r3, r3, #2
 8001ae4:	461a      	mov	r2, r3
 8001ae6:	683b      	ldr	r3, [r7, #0]
 8001ae8:	4413      	add	r3, r2
 8001aea:	781b      	ldrb	r3, [r3, #0]
 8001aec:	461a      	mov	r2, r3
 8001aee:	2301      	movs	r3, #1
 8001af0:	4093      	lsls	r3, r2
 8001af2:	b25b      	sxtb	r3, r3
 8001af4:	43db      	mvns	r3, r3
 8001af6:	b25b      	sxtb	r3, r3
 8001af8:	400b      	ands	r3, r1
 8001afa:	b25a      	sxtb	r2, r3
 8001afc:	4b09      	ldr	r3, [pc, #36]	; (8001b24 <dyn_update+0xec>)
 8001afe:	681b      	ldr	r3, [r3, #0]
 8001b00:	b2d2      	uxtb	r2, r2
 8001b02:	70da      	strb	r2, [r3, #3]
 8001b04:	687b      	ldr	r3, [r7, #4]
 8001b06:	3301      	adds	r3, #1
 8001b08:	607b      	str	r3, [r7, #4]
 8001b0a:	4b06      	ldr	r3, [pc, #24]	; (8001b24 <dyn_update+0xec>)
 8001b0c:	681b      	ldr	r3, [r3, #0]
 8001b0e:	785b      	ldrb	r3, [r3, #1]
 8001b10:	461a      	mov	r2, r3
 8001b12:	687b      	ldr	r3, [r7, #4]
 8001b14:	4293      	cmp	r3, r2
 8001b16:	dd9d      	ble.n	8001a54 <dyn_update+0x1c>
 8001b18:	bf00      	nop
 8001b1a:	370c      	adds	r7, #12
 8001b1c:	46bd      	mov	sp, r7
 8001b1e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001b22:	4770      	bx	lr
 8001b24:	2000026c 	.word	0x2000026c

08001b28 <cmd_update>:
 8001b28:	b480      	push	{r7}
 8001b2a:	b083      	sub	sp, #12
 8001b2c:	af00      	add	r7, sp, #0
 8001b2e:	2300      	movs	r3, #0
 8001b30:	607b      	str	r3, [r7, #4]
 8001b32:	4b16      	ldr	r3, [pc, #88]	; (8001b8c <cmd_update+0x64>)
 8001b34:	681b      	ldr	r3, [r3, #0]
 8001b36:	3330      	adds	r3, #48	; 0x30
 8001b38:	603b      	str	r3, [r7, #0]
 8001b3a:	4b14      	ldr	r3, [pc, #80]	; (8001b8c <cmd_update+0x64>)
 8001b3c:	681b      	ldr	r3, [r3, #0]
 8001b3e:	789b      	ldrb	r3, [r3, #2]
 8001b40:	607b      	str	r3, [r7, #4]
 8001b42:	e013      	b.n	8001b6c <cmd_update+0x44>
 8001b44:	687a      	ldr	r2, [r7, #4]
 8001b46:	4613      	mov	r3, r2
 8001b48:	00db      	lsls	r3, r3, #3
 8001b4a:	4413      	add	r3, r2
 8001b4c:	009b      	lsls	r3, r3, #2
 8001b4e:	461a      	mov	r2, r3
 8001b50:	683b      	ldr	r3, [r7, #0]
 8001b52:	4413      	add	r3, r2
 8001b54:	7c5b      	ldrb	r3, [r3, #17]
 8001b56:	2b01      	cmp	r3, #1
 8001b58:	d110      	bne.n	8001b7c <cmd_update+0x54>
 8001b5a:	4b0c      	ldr	r3, [pc, #48]	; (8001b8c <cmd_update+0x64>)
 8001b5c:	681b      	ldr	r3, [r3, #0]
 8001b5e:	789a      	ldrb	r2, [r3, #2]
 8001b60:	3201      	adds	r2, #1
 8001b62:	b2d2      	uxtb	r2, r2
 8001b64:	709a      	strb	r2, [r3, #2]
 8001b66:	687b      	ldr	r3, [r7, #4]
 8001b68:	3301      	adds	r3, #1
 8001b6a:	607b      	str	r3, [r7, #4]
 8001b6c:	4b07      	ldr	r3, [pc, #28]	; (8001b8c <cmd_update+0x64>)
 8001b6e:	681b      	ldr	r3, [r3, #0]
 8001b70:	785b      	ldrb	r3, [r3, #1]
 8001b72:	461a      	mov	r2, r3
 8001b74:	687b      	ldr	r3, [r7, #4]
 8001b76:	4293      	cmp	r3, r2
 8001b78:	dde4      	ble.n	8001b44 <cmd_update+0x1c>
 8001b7a:	e000      	b.n	8001b7e <cmd_update+0x56>
 8001b7c:	bf00      	nop
 8001b7e:	bf00      	nop
 8001b80:	370c      	adds	r7, #12
 8001b82:	46bd      	mov	sp, r7
 8001b84:	f85d 7b04 	ldr.w	r7, [sp], #4
 8001b88:	4770      	bx	lr
 8001b8a:	bf00      	nop
 8001b8c:	2000026c 	.word	0x2000026c

08001b90 <dyn_time_slice_operator>:
 8001b90:	b580      	push	{r7, lr}
 8001b92:	b084      	sub	sp, #16
 8001b94:	af00      	add	r7, sp, #0
 8001b96:	6078      	str	r0, [r7, #4]
 8001b98:	687b      	ldr	r3, [r7, #4]
 8001b9a:	3330      	adds	r3, #48	; 0x30
 8001b9c:	60fb      	str	r3, [r7, #12]
 8001b9e:	f7ff ff4b 	bl	8001a38 <dyn_update>
 8001ba2:	687b      	ldr	r3, [r7, #4]
 8001ba4:	785b      	ldrb	r3, [r3, #1]
 8001ba6:	461a      	mov	r2, r3
 8001ba8:	4613      	mov	r3, r2
 8001baa:	00db      	lsls	r3, r3, #3
 8001bac:	4413      	add	r3, r2
 8001bae:	009b      	lsls	r3, r3, #2
 8001bb0:	461a      	mov	r2, r3
 8001bb2:	68fb      	ldr	r3, [r7, #12]
 8001bb4:	4413      	add	r3, r2
 8001bb6:	7c1b      	ldrb	r3, [r3, #16]
 8001bb8:	2b00      	cmp	r3, #0
 8001bba:	d163      	bne.n	8001c84 <dyn_time_slice_operator+0xf4>
 8001bbc:	687b      	ldr	r3, [r7, #4]
 8001bbe:	78db      	ldrb	r3, [r3, #3]
 8001bc0:	4619      	mov	r1, r3
 8001bc2:	687b      	ldr	r3, [r7, #4]
 8001bc4:	785b      	ldrb	r3, [r3, #1]
 8001bc6:	461a      	mov	r2, r3
 8001bc8:	4613      	mov	r3, r2
 8001bca:	00db      	lsls	r3, r3, #3
 8001bcc:	4413      	add	r3, r2
 8001bce:	009b      	lsls	r3, r3, #2
 8001bd0:	461a      	mov	r2, r3
 8001bd2:	68fb      	ldr	r3, [r7, #12]
 8001bd4:	4413      	add	r3, r2
 8001bd6:	781b      	ldrb	r3, [r3, #0]
 8001bd8:	fa41 f303 	asr.w	r3, r1, r3
 8001bdc:	f003 0301 	and.w	r3, r3, #1
 8001be0:	2b00      	cmp	r3, #0
 8001be2:	d14f      	bne.n	8001c84 <dyn_time_slice_operator+0xf4>
 8001be4:	687b      	ldr	r3, [r7, #4]
 8001be6:	785b      	ldrb	r3, [r3, #1]
 8001be8:	461a      	mov	r2, r3
 8001bea:	4613      	mov	r3, r2
 8001bec:	00db      	lsls	r3, r3, #3
 8001bee:	4413      	add	r3, r2
 8001bf0:	009b      	lsls	r3, r3, #2
 8001bf2:	461a      	mov	r2, r3
 8001bf4:	68fb      	ldr	r3, [r7, #12]
 8001bf6:	4413      	add	r3, r2
 8001bf8:	2201      	movs	r2, #1
 8001bfa:	741a      	strb	r2, [r3, #16]
 8001bfc:	687b      	ldr	r3, [r7, #4]
 8001bfe:	785b      	ldrb	r3, [r3, #1]
 8001c00:	461a      	mov	r2, r3
 8001c02:	4613      	mov	r3, r2
 8001c04:	00db      	lsls	r3, r3, #3
 8001c06:	4413      	add	r3, r2
 8001c08:	009b      	lsls	r3, r3, #2
 8001c0a:	461a      	mov	r2, r3
 8001c0c:	68fb      	ldr	r3, [r7, #12]
 8001c0e:	4413      	add	r3, r2
 8001c10:	3312      	adds	r3, #18
 8001c12:	2112      	movs	r1, #18
 8001c14:	4618      	mov	r0, r3
 8001c16:	f7ff fdf3 	bl	8001800 <stm_driver_send_msg>
 8001c1a:	687b      	ldr	r3, [r7, #4]
 8001c1c:	785b      	ldrb	r3, [r3, #1]
 8001c1e:	461a      	mov	r2, r3
 8001c20:	4613      	mov	r3, r2
 8001c22:	00db      	lsls	r3, r3, #3
 8001c24:	4413      	add	r3, r2
 8001c26:	009b      	lsls	r3, r3, #2
 8001c28:	461a      	mov	r2, r3
 8001c2a:	68fb      	ldr	r3, [r7, #12]
 8001c2c:	4413      	add	r3, r2
 8001c2e:	687a      	ldr	r2, [r7, #4]
 8001c30:	6892      	ldr	r2, [r2, #8]
 8001c32:	60da      	str	r2, [r3, #12]
 8001c34:	687b      	ldr	r3, [r7, #4]
 8001c36:	78db      	ldrb	r3, [r3, #3]
 8001c38:	b25a      	sxtb	r2, r3
 8001c3a:	687b      	ldr	r3, [r7, #4]
 8001c3c:	785b      	ldrb	r3, [r3, #1]
 8001c3e:	4619      	mov	r1, r3
 8001c40:	460b      	mov	r3, r1
 8001c42:	00db      	lsls	r3, r3, #3
 8001c44:	440b      	add	r3, r1
 8001c46:	009b      	lsls	r3, r3, #2
 8001c48:	4619      	mov	r1, r3
 8001c4a:	68fb      	ldr	r3, [r7, #12]
 8001c4c:	440b      	add	r3, r1
 8001c4e:	781b      	ldrb	r3, [r3, #0]
 8001c50:	4619      	mov	r1, r3
 8001c52:	2301      	movs	r3, #1
 8001c54:	408b      	lsls	r3, r1
 8001c56:	b25b      	sxtb	r3, r3
 8001c58:	4313      	orrs	r3, r2
 8001c5a:	b25b      	sxtb	r3, r3
 8001c5c:	b2da      	uxtb	r2, r3
 8001c5e:	687b      	ldr	r3, [r7, #4]
 8001c60:	70da      	strb	r2, [r3, #3]
 8001c62:	687b      	ldr	r3, [r7, #4]
 8001c64:	785b      	ldrb	r3, [r3, #1]
 8001c66:	461a      	mov	r2, r3
 8001c68:	687b      	ldr	r3, [r7, #4]
 8001c6a:	781b      	ldrb	r3, [r3, #0]
 8001c6c:	3b01      	subs	r3, #1
 8001c6e:	429a      	cmp	r2, r3
 8001c70:	d008      	beq.n	8001c84 <dyn_time_slice_operator+0xf4>
 8001c72:	687b      	ldr	r3, [r7, #4]
 8001c74:	785b      	ldrb	r3, [r3, #1]
 8001c76:	3301      	adds	r3, #1
 8001c78:	b2da      	uxtb	r2, r3
 8001c7a:	687b      	ldr	r3, [r7, #4]
 8001c7c:	705a      	strb	r2, [r3, #1]
 8001c7e:	2096      	movs	r0, #150	; 0x96
 8001c80:	f006 f9f0 	bl	8008064 <vTaskDelay>
 8001c84:	f7ff ff50 	bl	8001b28 <cmd_update>
 8001c88:	bf00      	nop
 8001c8a:	3710      	adds	r7, #16
 8001c8c:	46bd      	mov	sp, r7
 8001c8e:	bd80      	pop	{r7, pc}

08001c90 <dyn_set_init_pos>:
 8001c90:	b580      	push	{r7, lr}
 8001c92:	b086      	sub	sp, #24
 8001c94:	af00      	add	r7, sp, #0
 8001c96:	2300      	movs	r3, #0
 8001c98:	75fb      	strb	r3, [r7, #23]
 8001c9a:	1d3b      	adds	r3, r7, #4
 8001c9c:	2213      	movs	r2, #19
 8001c9e:	2100      	movs	r1, #0
 8001ca0:	4618      	mov	r0, r3
 8001ca2:	f008 fe41 	bl	800a928 <memset>
 8001ca6:	2302      	movs	r3, #2
 8001ca8:	713b      	strb	r3, [r7, #4]
 8001caa:	2300      	movs	r3, #0
 8001cac:	75fb      	strb	r3, [r7, #23]
 8001cae:	e024      	b.n	8001cfa <dyn_set_init_pos+0x6a>
 8001cb0:	4b18      	ldr	r3, [pc, #96]	; (8001d14 <dyn_set_init_pos+0x84>)
 8001cb2:	681a      	ldr	r2, [r3, #0]
 8001cb4:	7dfb      	ldrb	r3, [r7, #23]
 8001cb6:	3304      	adds	r3, #4
 8001cb8:	005b      	lsls	r3, r3, #1
 8001cba:	4413      	add	r3, r2
 8001cbc:	889b      	ldrh	r3, [r3, #4]
 8001cbe:	0a1b      	lsrs	r3, r3, #8
 8001cc0:	b29a      	uxth	r2, r3
 8001cc2:	7dfb      	ldrb	r3, [r7, #23]
 8001cc4:	005b      	lsls	r3, r3, #1
 8001cc6:	3301      	adds	r3, #1
 8001cc8:	b2d2      	uxtb	r2, r2
 8001cca:	f107 0118 	add.w	r1, r7, #24
 8001cce:	440b      	add	r3, r1
 8001cd0:	f803 2c14 	strb.w	r2, [r3, #-20]
 8001cd4:	4b0f      	ldr	r3, [pc, #60]	; (8001d14 <dyn_set_init_pos+0x84>)
 8001cd6:	681a      	ldr	r2, [r3, #0]
 8001cd8:	7dfb      	ldrb	r3, [r7, #23]
 8001cda:	3304      	adds	r3, #4
 8001cdc:	005b      	lsls	r3, r3, #1
 8001cde:	4413      	add	r3, r2
 8001ce0:	889a      	ldrh	r2, [r3, #4]
 8001ce2:	7dfb      	ldrb	r3, [r7, #23]
 8001ce4:	3301      	adds	r3, #1
 8001ce6:	005b      	lsls	r3, r3, #1
 8001ce8:	b2d2      	uxtb	r2, r2
 8001cea:	f107 0118 	add.w	r1, r7, #24
 8001cee:	440b      	add	r3, r1
 8001cf0:	f803 2c14 	strb.w	r2, [r3, #-20]
 8001cf4:	7dfb      	ldrb	r3, [r7, #23]
 8001cf6:	3301      	adds	r3, #1
 8001cf8:	75fb      	strb	r3, [r7, #23]
 8001cfa:	7dfb      	ldrb	r3, [r7, #23]
 8001cfc:	2b08      	cmp	r3, #8
 8001cfe:	d9d7      	bls.n	8001cb0 <dyn_set_init_pos+0x20>
 8001d00:	1d3b      	adds	r3, r7, #4
 8001d02:	2113      	movs	r1, #19
 8001d04:	4618      	mov	r0, r3
 8001d06:	f7ff fd7b 	bl	8001800 <stm_driver_send_msg>
 8001d0a:	bf00      	nop
 8001d0c:	3718      	adds	r7, #24
 8001d0e:	46bd      	mov	sp, r7
 8001d10:	bd80      	pop	{r7, pc}
 8001d12:	bf00      	nop
 8001d14:	2000026c 	.word	0x2000026c

08001d18 <manipulators_block>:
 8001d18:	b580      	push	{r7, lr}
 8001d1a:	af00      	add	r7, sp, #0
 8001d1c:	f7ff fda0 	bl	8001860 <manip_dyn_stop>
 8001d20:	4b0e      	ldr	r3, [pc, #56]	; (8001d5c <manipulators_block+0x44>)
 8001d22:	681b      	ldr	r3, [r3, #0]
 8001d24:	791a      	ldrb	r2, [r3, #4]
 8001d26:	4b0d      	ldr	r3, [pc, #52]	; (8001d5c <manipulators_block+0x44>)
 8001d28:	681b      	ldr	r3, [r3, #0]
 8001d2a:	f042 0204 	orr.w	r2, r2, #4
 8001d2e:	b2d2      	uxtb	r2, r2
 8001d30:	711a      	strb	r2, [r3, #4]
 8001d32:	f003 ff23 	bl	8005b7c <step_stop_motors>
 8001d36:	4b09      	ldr	r3, [pc, #36]	; (8001d5c <manipulators_block+0x44>)
 8001d38:	681b      	ldr	r3, [r3, #0]
 8001d3a:	791a      	ldrb	r2, [r3, #4]
 8001d3c:	4b07      	ldr	r3, [pc, #28]	; (8001d5c <manipulators_block+0x44>)
 8001d3e:	681b      	ldr	r3, [r3, #0]
 8001d40:	f042 0208 	orr.w	r2, r2, #8
 8001d44:	b2d2      	uxtb	r2, r2
 8001d46:	711a      	strb	r2, [r3, #4]
 8001d48:	f7ff fe5c 	bl	8001a04 <stick_disable_torque>
 8001d4c:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 8001d50:	4803      	ldr	r0, [pc, #12]	; (8001d60 <manipulators_block+0x48>)
 8001d52:	f7ff fb62 	bl	800141a <LL_GPIO_SetOutputPin>
 8001d56:	bf00      	nop
 8001d58:	bd80      	pop	{r7, pc}
 8001d5a:	bf00      	nop
 8001d5c:	2000026c 	.word	0x2000026c
 8001d60:	40020000 	.word	0x40020000

08001d64 <manipulators_rise_flag>:
 8001d64:	b580      	push	{r7, lr}
 8001d66:	af00      	add	r7, sp, #0
 8001d68:	2001      	movs	r0, #1
 8001d6a:	f7ff fe55 	bl	8001a18 <stick_set_angle>
 8001d6e:	f7ff fe3f 	bl	80019f0 <stick_enable_torque>
 8001d72:	bf00      	nop
 8001d74:	bd80      	pop	{r7, pc}
	...

08001d78 <manipulators_manager>:
 8001d78:	b5b0      	push	{r4, r5, r7, lr}
 8001d7a:	f5ad 7d32 	sub.w	sp, sp, #712	; 0x2c8
 8001d7e:	af00      	add	r7, sp, #0
 8001d80:	1d3b      	adds	r3, r7, #4
 8001d82:	6018      	str	r0, [r3, #0]
 8001d84:	f006 fe2e 	bl	80089e4 <xTaskGetCurrentTaskHandle>
 8001d88:	4602      	mov	r2, r0
 8001d8a:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001d8e:	f8c3 2284 	str.w	r2, [r3, #644]	; 0x284
 8001d92:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001d96:	2200      	movs	r2, #0
 8001d98:	70da      	strb	r2, [r3, #3]
 8001d9a:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001d9e:	2200      	movs	r2, #0
 8001da0:	711a      	strb	r2, [r3, #4]
 8001da2:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001da6:	2200      	movs	r2, #0
 8001da8:	609a      	str	r2, [r3, #8]
 8001daa:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001dae:	2200      	movs	r2, #0
 8001db0:	701a      	strb	r2, [r3, #0]
 8001db2:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001db6:	2200      	movs	r2, #0
 8001db8:	705a      	strb	r2, [r3, #1]
 8001dba:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001dbe:	2200      	movs	r2, #0
 8001dc0:	709a      	strb	r2, [r3, #2]
 8001dc2:	f107 032c 	add.w	r3, r7, #44	; 0x2c
 8001dc6:	4aa3      	ldr	r2, [pc, #652]	; (8002054 <manipulators_manager+0x2dc>)
 8001dc8:	e892 0003 	ldmia.w	r2, {r0, r1}
 8001dcc:	e883 0003 	stmia.w	r3, {r0, r1}
 8001dd0:	f107 0320 	add.w	r3, r7, #32
 8001dd4:	4aa0      	ldr	r2, [pc, #640]	; (8002058 <manipulators_manager+0x2e0>)
 8001dd6:	ca07      	ldmia	r2, {r0, r1, r2}
 8001dd8:	c303      	stmia	r3!, {r0, r1}
 8001dda:	801a      	strh	r2, [r3, #0]
 8001ddc:	f107 030c 	add.w	r3, r7, #12
 8001de0:	4a9e      	ldr	r2, [pc, #632]	; (800205c <manipulators_manager+0x2e4>)
 8001de2:	461c      	mov	r4, r3
 8001de4:	4615      	mov	r5, r2
 8001de6:	cd0f      	ldmia	r5!, {r0, r1, r2, r3}
 8001de8:	c40f      	stmia	r4!, {r0, r1, r2, r3}
 8001dea:	682b      	ldr	r3, [r5, #0]
 8001dec:	8023      	strh	r3, [r4, #0]
 8001dee:	2300      	movs	r3, #0
 8001df0:	f887 32c7 	strb.w	r3, [r7, #711]	; 0x2c7
 8001df4:	e022      	b.n	8001e3c <manipulators_manager+0xc4>
 8001df6:	f897 12c7 	ldrb.w	r1, [r7, #711]	; 0x2c7
 8001dfa:	f897 32c7 	ldrb.w	r3, [r7, #711]	; 0x2c7
 8001dfe:	f107 020c 	add.w	r2, r7, #12
 8001e02:	f832 1011 	ldrh.w	r1, [r2, r1, lsl #1]
 8001e06:	f107 0234 	add.w	r2, r7, #52	; 0x34
 8001e0a:	330c      	adds	r3, #12
 8001e0c:	005b      	lsls	r3, r3, #1
 8001e0e:	4413      	add	r3, r2
 8001e10:	460a      	mov	r2, r1
 8001e12:	80da      	strh	r2, [r3, #6]
 8001e14:	f897 12c7 	ldrb.w	r1, [r7, #711]	; 0x2c7
 8001e18:	f897 32c7 	ldrb.w	r3, [r7, #711]	; 0x2c7
 8001e1c:	f107 0220 	add.w	r2, r7, #32
 8001e20:	f832 1011 	ldrh.w	r1, [r2, r1, lsl #1]
 8001e24:	f107 0234 	add.w	r2, r7, #52	; 0x34
 8001e28:	3304      	adds	r3, #4
 8001e2a:	005b      	lsls	r3, r3, #1
 8001e2c:	4413      	add	r3, r2
 8001e2e:	460a      	mov	r2, r1
 8001e30:	809a      	strh	r2, [r3, #4]
 8001e32:	f897 32c7 	ldrb.w	r3, [r7, #711]	; 0x2c7
 8001e36:	3301      	adds	r3, #1
 8001e38:	f887 32c7 	strb.w	r3, [r7, #711]	; 0x2c7
 8001e3c:	f897 32c7 	ldrb.w	r3, [r7, #711]	; 0x2c7
 8001e40:	2b04      	cmp	r3, #4
 8001e42:	d9d8      	bls.n	8001df6 <manipulators_manager+0x7e>
 8001e44:	2300      	movs	r3, #0
 8001e46:	f887 32c6 	strb.w	r3, [r7, #710]	; 0x2c6
 8001e4a:	e025      	b.n	8001e98 <manipulators_manager+0x120>
 8001e4c:	f897 32c6 	ldrb.w	r3, [r7, #710]	; 0x2c6
 8001e50:	1d59      	adds	r1, r3, #5
 8001e52:	f897 32c6 	ldrb.w	r3, [r7, #710]	; 0x2c6
 8001e56:	3305      	adds	r3, #5
 8001e58:	f107 020c 	add.w	r2, r7, #12
 8001e5c:	f832 1011 	ldrh.w	r1, [r2, r1, lsl #1]
 8001e60:	f107 0234 	add.w	r2, r7, #52	; 0x34
 8001e64:	330c      	adds	r3, #12
 8001e66:	005b      	lsls	r3, r3, #1
 8001e68:	4413      	add	r3, r2
 8001e6a:	460a      	mov	r2, r1
 8001e6c:	80da      	strh	r2, [r3, #6]
 8001e6e:	f897 12c6 	ldrb.w	r1, [r7, #710]	; 0x2c6
 8001e72:	f897 32c6 	ldrb.w	r3, [r7, #710]	; 0x2c6
 8001e76:	3305      	adds	r3, #5
 8001e78:	f107 022c 	add.w	r2, r7, #44	; 0x2c
 8001e7c:	f832 1011 	ldrh.w	r1, [r2, r1, lsl #1]
 8001e80:	f107 0234 	add.w	r2, r7, #52	; 0x34
 8001e84:	3304      	adds	r3, #4
 8001e86:	005b      	lsls	r3, r3, #1
 8001e88:	4413      	add	r3, r2
 8001e8a:	460a      	mov	r2, r1
 8001e8c:	809a      	strh	r2, [r3, #4]
 8001e8e:	f897 32c6 	ldrb.w	r3, [r7, #710]	; 0x2c6
 8001e92:	3301      	adds	r3, #1
 8001e94:	f887 32c6 	strb.w	r3, [r7, #710]	; 0x2c6
 8001e98:	f897 32c6 	ldrb.w	r3, [r7, #710]	; 0x2c6
 8001e9c:	2b03      	cmp	r3, #3
 8001e9e:	d9d5      	bls.n	8001e4c <manipulators_manager+0xd4>
 8001ea0:	2300      	movs	r3, #0
 8001ea2:	f887 32c5 	strb.w	r3, [r7, #709]	; 0x2c5
 8001ea6:	e050      	b.n	8001f4a <manipulators_manager+0x1d2>
 8001ea8:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001eac:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001eb0:	4613      	mov	r3, r2
 8001eb2:	00db      	lsls	r3, r3, #3
 8001eb4:	4413      	add	r3, r2
 8001eb6:	009b      	lsls	r3, r3, #2
 8001eb8:	3340      	adds	r3, #64	; 0x40
 8001eba:	440b      	add	r3, r1
 8001ebc:	3302      	adds	r3, #2
 8001ebe:	2212      	movs	r2, #18
 8001ec0:	2100      	movs	r1, #0
 8001ec2:	4618      	mov	r0, r3
 8001ec4:	f008 fd30 	bl	800a928 <memset>
 8001ec8:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001ecc:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001ed0:	4613      	mov	r3, r2
 8001ed2:	00db      	lsls	r3, r3, #3
 8001ed4:	4413      	add	r3, r2
 8001ed6:	009b      	lsls	r3, r3, #2
 8001ed8:	440b      	add	r3, r1
 8001eda:	3334      	adds	r3, #52	; 0x34
 8001edc:	2200      	movs	r2, #0
 8001ede:	801a      	strh	r2, [r3, #0]
 8001ee0:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001ee4:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001ee8:	4613      	mov	r3, r2
 8001eea:	00db      	lsls	r3, r3, #3
 8001eec:	4413      	add	r3, r2
 8001eee:	009b      	lsls	r3, r3, #2
 8001ef0:	440b      	add	r3, r1
 8001ef2:	3332      	adds	r3, #50	; 0x32
 8001ef4:	2200      	movs	r2, #0
 8001ef6:	801a      	strh	r2, [r3, #0]
 8001ef8:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001efc:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001f00:	4613      	mov	r3, r2
 8001f02:	00db      	lsls	r3, r3, #3
 8001f04:	4413      	add	r3, r2
 8001f06:	009b      	lsls	r3, r3, #2
 8001f08:	440b      	add	r3, r1
 8001f0a:	333c      	adds	r3, #60	; 0x3c
 8001f0c:	2200      	movs	r2, #0
 8001f0e:	601a      	str	r2, [r3, #0]
 8001f10:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001f14:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001f18:	4613      	mov	r3, r2
 8001f1a:	00db      	lsls	r3, r3, #3
 8001f1c:	4413      	add	r3, r2
 8001f1e:	009b      	lsls	r3, r3, #2
 8001f20:	440b      	add	r3, r1
 8001f22:	3340      	adds	r3, #64	; 0x40
 8001f24:	2200      	movs	r2, #0
 8001f26:	701a      	strb	r2, [r3, #0]
 8001f28:	f897 22c5 	ldrb.w	r2, [r7, #709]	; 0x2c5
 8001f2c:	f107 0134 	add.w	r1, r7, #52	; 0x34
 8001f30:	4613      	mov	r3, r2
 8001f32:	00db      	lsls	r3, r3, #3
 8001f34:	4413      	add	r3, r2
 8001f36:	009b      	lsls	r3, r3, #2
 8001f38:	440b      	add	r3, r1
 8001f3a:	3341      	adds	r3, #65	; 0x41
 8001f3c:	2200      	movs	r2, #0
 8001f3e:	701a      	strb	r2, [r3, #0]
 8001f40:	f897 32c5 	ldrb.w	r3, [r7, #709]	; 0x2c5
 8001f44:	3301      	adds	r3, #1
 8001f46:	f887 32c5 	strb.w	r3, [r7, #709]	; 0x2c5
 8001f4a:	f897 32c5 	ldrb.w	r3, [r7, #709]	; 0x2c5
 8001f4e:	2b0f      	cmp	r3, #15
 8001f50:	d9aa      	bls.n	8001ea8 <manipulators_manager+0x130>
 8001f52:	4a43      	ldr	r2, [pc, #268]	; (8002060 <manipulators_manager+0x2e8>)
 8001f54:	f107 0334 	add.w	r3, r7, #52	; 0x34
 8001f58:	6013      	str	r3, [r2, #0]
 8001f5a:	f7ff fc97 	bl	800188c <manip_hw_config>
 8001f5e:	203c      	movs	r0, #60	; 0x3c
 8001f60:	f006 f880 	bl	8008064 <vTaskDelay>
 8001f64:	f003 fd84 	bl	8005a70 <step_init>
 8001f68:	2300      	movs	r3, #0
 8001f6a:	f8c7 32c0 	str.w	r3, [r7, #704]	; 0x2c0
 8001f6e:	2300      	movs	r3, #0
 8001f70:	f8c7 32bc 	str.w	r3, [r7, #700]	; 0x2bc
 8001f74:	2000      	movs	r0, #0
 8001f76:	f003 fe1d 	bl	8005bb4 <step_start_calibration>
 8001f7a:	f7ff fe89 	bl	8001c90 <dyn_set_init_pos>
 8001f7e:	e007      	b.n	8001f90 <manipulators_manager+0x218>
 8001f80:	4b38      	ldr	r3, [pc, #224]	; (8002064 <manipulators_manager+0x2ec>)
 8001f82:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8001f86:	601a      	str	r2, [r3, #0]
 8001f88:	f3bf 8f4f 	dsb	sy
 8001f8c:	f3bf 8f6f 	isb	sy
 8001f90:	2000      	movs	r0, #0
 8001f92:	f003 fe81 	bl	8005c98 <step_is_running>
 8001f96:	4603      	mov	r3, r0
 8001f98:	2b00      	cmp	r3, #0
 8001f9a:	d1f1      	bne.n	8001f80 <manipulators_manager+0x208>
 8001f9c:	2000      	movs	r0, #0
 8001f9e:	f003 fefd 	bl	8005d9c <step_get_current_step>
 8001fa2:	f8c7 02bc 	str.w	r0, [r7, #700]	; 0x2bc
 8001fa6:	f8d7 32bc 	ldr.w	r3, [r7, #700]	; 0x2bc
 8001faa:	f203 536a 	addw	r3, r3, #1386	; 0x56a
 8001fae:	f8c7 32c0 	str.w	r3, [r7, #704]	; 0x2c0
 8001fb2:	f8d7 12c0 	ldr.w	r1, [r7, #704]	; 0x2c0
 8001fb6:	2000      	movs	r0, #0
 8001fb8:	f003 feac 	bl	8005d14 <step_set_step_goal>
 8001fbc:	203c      	movs	r0, #60	; 0x3c
 8001fbe:	f006 f851 	bl	8008064 <vTaskDelay>
 8001fc2:	f04f 31ff 	mov.w	r1, #4294967295
 8001fc6:	2001      	movs	r0, #1
 8001fc8:	f006 feea 	bl	8008da0 <ulTaskNotifyTake>
 8001fcc:	4826      	ldr	r0, [pc, #152]	; (8002068 <manipulators_manager+0x2f0>)
 8001fce:	f7ff fa89 	bl	80014e4 <LL_TIM_EnableCounter>
 8001fd2:	4b23      	ldr	r3, [pc, #140]	; (8002060 <manipulators_manager+0x2e8>)
 8001fd4:	681b      	ldr	r3, [r3, #0]
 8001fd6:	2200      	movs	r2, #0
 8001fd8:	705a      	strb	r2, [r3, #1]
 8001fda:	4b21      	ldr	r3, [pc, #132]	; (8002060 <manipulators_manager+0x2e8>)
 8001fdc:	681b      	ldr	r3, [r3, #0]
 8001fde:	2200      	movs	r2, #0
 8001fe0:	709a      	strb	r2, [r3, #2]
 8001fe2:	e00c      	b.n	8001ffe <manipulators_manager+0x286>
 8001fe4:	4b1e      	ldr	r3, [pc, #120]	; (8002060 <manipulators_manager+0x2e8>)
 8001fe6:	681b      	ldr	r3, [r3, #0]
 8001fe8:	4618      	mov	r0, r3
 8001fea:	f7ff fdd1 	bl	8001b90 <dyn_time_slice_operator>
 8001fee:	4b1d      	ldr	r3, [pc, #116]	; (8002064 <manipulators_manager+0x2ec>)
 8001ff0:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8001ff4:	601a      	str	r2, [r3, #0]
 8001ff6:	f3bf 8f4f 	dsb	sy
 8001ffa:	f3bf 8f6f 	isb	sy
 8001ffe:	4b18      	ldr	r3, [pc, #96]	; (8002060 <manipulators_manager+0x2e8>)
 8002000:	681b      	ldr	r3, [r3, #0]
 8002002:	789a      	ldrb	r2, [r3, #2]
 8002004:	4b16      	ldr	r3, [pc, #88]	; (8002060 <manipulators_manager+0x2e8>)
 8002006:	681b      	ldr	r3, [r3, #0]
 8002008:	781b      	ldrb	r3, [r3, #0]
 800200a:	429a      	cmp	r2, r3
 800200c:	d3ea      	bcc.n	8001fe4 <manipulators_manager+0x26c>
 800200e:	4b14      	ldr	r3, [pc, #80]	; (8002060 <manipulators_manager+0x2e8>)
 8002010:	681b      	ldr	r3, [r3, #0]
 8002012:	2200      	movs	r2, #0
 8002014:	701a      	strb	r2, [r3, #0]
 8002016:	4b12      	ldr	r3, [pc, #72]	; (8002060 <manipulators_manager+0x2e8>)
 8002018:	681b      	ldr	r3, [r3, #0]
 800201a:	2200      	movs	r2, #0
 800201c:	705a      	strb	r2, [r3, #1]
 800201e:	4b10      	ldr	r3, [pc, #64]	; (8002060 <manipulators_manager+0x2e8>)
 8002020:	681b      	ldr	r3, [r3, #0]
 8002022:	2200      	movs	r2, #0
 8002024:	709a      	strb	r2, [r3, #2]
 8002026:	4b0e      	ldr	r3, [pc, #56]	; (8002060 <manipulators_manager+0x2e8>)
 8002028:	681b      	ldr	r3, [r3, #0]
 800202a:	78da      	ldrb	r2, [r3, #3]
 800202c:	4b0c      	ldr	r3, [pc, #48]	; (8002060 <manipulators_manager+0x2e8>)
 800202e:	681b      	ldr	r3, [r3, #0]
 8002030:	f022 0201 	bic.w	r2, r2, #1
 8002034:	b2d2      	uxtb	r2, r2
 8002036:	70da      	strb	r2, [r3, #3]
 8002038:	480b      	ldr	r0, [pc, #44]	; (8002068 <manipulators_manager+0x2f0>)
 800203a:	f7ff fa63 	bl	8001504 <LL_TIM_DisableCounter>
 800203e:	4b08      	ldr	r3, [pc, #32]	; (8002060 <manipulators_manager+0x2e8>)
 8002040:	681b      	ldr	r3, [r3, #0]
 8002042:	791a      	ldrb	r2, [r3, #4]
 8002044:	4b06      	ldr	r3, [pc, #24]	; (8002060 <manipulators_manager+0x2e8>)
 8002046:	681b      	ldr	r3, [r3, #0]
 8002048:	f022 0201 	bic.w	r2, r2, #1
 800204c:	b2d2      	uxtb	r2, r2
 800204e:	711a      	strb	r2, [r3, #4]
 8002050:	e7b7      	b.n	8001fc2 <manipulators_manager+0x24a>
 8002052:	bf00      	nop
 8002054:	0800d090 	.word	0x0800d090
 8002058:	0800d098 	.word	0x0800d098
 800205c:	0800d0a4 	.word	0x0800d0a4
 8002060:	2000026c 	.word	0x2000026c
 8002064:	e000ed04 	.word	0xe000ed04
 8002068:	40014400 	.word	0x40014400

0800206c <cmd_hold_status>:
 800206c:	b580      	push	{r7, lr}
 800206e:	b084      	sub	sp, #16
 8002070:	af00      	add	r7, sp, #0
 8002072:	6078      	str	r0, [r7, #4]
 8002074:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8002078:	4823      	ldr	r0, [pc, #140]	; (8002108 <cmd_hold_status+0x9c>)
 800207a:	f7ff f9b9 	bl	80013f0 <LL_GPIO_IsInputPinSet>
 800207e:	4603      	mov	r3, r0
 8002080:	2b00      	cmp	r3, #0
 8002082:	bf0c      	ite	eq
 8002084:	2301      	moveq	r3, #1
 8002086:	2300      	movne	r3, #0
 8002088:	b2db      	uxtb	r3, r3
 800208a:	723b      	strb	r3, [r7, #8]
 800208c:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 8002090:	481d      	ldr	r0, [pc, #116]	; (8002108 <cmd_hold_status+0x9c>)
 8002092:	f7ff f9ad 	bl	80013f0 <LL_GPIO_IsInputPinSet>
 8002096:	4603      	mov	r3, r0
 8002098:	2b00      	cmp	r3, #0
 800209a:	bf0c      	ite	eq
 800209c:	2301      	moveq	r3, #1
 800209e:	2300      	movne	r3, #0
 80020a0:	b2db      	uxtb	r3, r3
 80020a2:	727b      	strb	r3, [r7, #9]
 80020a4:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 80020a8:	4817      	ldr	r0, [pc, #92]	; (8002108 <cmd_hold_status+0x9c>)
 80020aa:	f7ff f9a1 	bl	80013f0 <LL_GPIO_IsInputPinSet>
 80020ae:	4603      	mov	r3, r0
 80020b0:	2b00      	cmp	r3, #0
 80020b2:	bf0c      	ite	eq
 80020b4:	2301      	moveq	r3, #1
 80020b6:	2300      	movne	r3, #0
 80020b8:	b2db      	uxtb	r3, r3
 80020ba:	72bb      	strb	r3, [r7, #10]
 80020bc:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80020c0:	4811      	ldr	r0, [pc, #68]	; (8002108 <cmd_hold_status+0x9c>)
 80020c2:	f7ff f995 	bl	80013f0 <LL_GPIO_IsInputPinSet>
 80020c6:	4603      	mov	r3, r0
 80020c8:	2b00      	cmp	r3, #0
 80020ca:	bf0c      	ite	eq
 80020cc:	2301      	moveq	r3, #1
 80020ce:	2300      	movne	r3, #0
 80020d0:	b2db      	uxtb	r3, r3
 80020d2:	72fb      	strb	r3, [r7, #11]
 80020d4:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80020d8:	480b      	ldr	r0, [pc, #44]	; (8002108 <cmd_hold_status+0x9c>)
 80020da:	f7ff f989 	bl	80013f0 <LL_GPIO_IsInputPinSet>
 80020de:	4603      	mov	r3, r0
 80020e0:	2b00      	cmp	r3, #0
 80020e2:	bf0c      	ite	eq
 80020e4:	2301      	moveq	r3, #1
 80020e6:	2300      	movne	r3, #0
 80020e8:	b2db      	uxtb	r3, r3
 80020ea:	733b      	strb	r3, [r7, #12]
 80020ec:	687b      	ldr	r3, [r7, #4]
 80020ee:	4618      	mov	r0, r3
 80020f0:	f107 0308 	add.w	r3, r7, #8
 80020f4:	2205      	movs	r2, #5
 80020f6:	4619      	mov	r1, r3
 80020f8:	f008 fc08 	bl	800a90c <memcpy>
 80020fc:	2305      	movs	r3, #5
 80020fe:	4618      	mov	r0, r3
 8002100:	3710      	adds	r7, #16
 8002102:	46bd      	mov	sp, r7
 8002104:	bd80      	pop	{r7, pc}
 8002106:	bf00      	nop
 8002108:	40021000 	.word	0x40021000

0800210c <cmd_step_calibrate>:
 800210c:	b580      	push	{r7, lr}
 800210e:	b082      	sub	sp, #8
 8002110:	af00      	add	r7, sp, #0
 8002112:	6078      	str	r0, [r7, #4]
 8002114:	4b0d      	ldr	r3, [pc, #52]	; (800214c <cmd_step_calibrate+0x40>)
 8002116:	681b      	ldr	r3, [r3, #0]
 8002118:	791b      	ldrb	r3, [r3, #4]
 800211a:	f003 0308 	and.w	r3, r3, #8
 800211e:	2b00      	cmp	r3, #0
 8002120:	d109      	bne.n	8002136 <cmd_step_calibrate+0x2a>
 8002122:	2000      	movs	r0, #0
 8002124:	f003 fd46 	bl	8005bb4 <step_start_calibration>
 8002128:	2203      	movs	r2, #3
 800212a:	4909      	ldr	r1, [pc, #36]	; (8002150 <cmd_step_calibrate+0x44>)
 800212c:	6878      	ldr	r0, [r7, #4]
 800212e:	f008 fbed 	bl	800a90c <memcpy>
 8002132:	2303      	movs	r3, #3
 8002134:	e006      	b.n	8002144 <cmd_step_calibrate+0x38>
 8002136:	bf00      	nop
 8002138:	2203      	movs	r2, #3
 800213a:	4906      	ldr	r1, [pc, #24]	; (8002154 <cmd_step_calibrate+0x48>)
 800213c:	6878      	ldr	r0, [r7, #4]
 800213e:	f008 fbe5 	bl	800a90c <memcpy>
 8002142:	2303      	movs	r3, #3
 8002144:	4618      	mov	r0, r3
 8002146:	3708      	adds	r7, #8
 8002148:	46bd      	mov	sp, r7
 800214a:	bd80      	pop	{r7, pc}
 800214c:	2000026c 	.word	0x2000026c
 8002150:	0800d0b8 	.word	0x0800d0b8
 8002154:	0800d0bc 	.word	0x0800d0bc

08002158 <cmd_step_set_step>:
 8002158:	b580      	push	{r7, lr}
 800215a:	b084      	sub	sp, #16
 800215c:	af00      	add	r7, sp, #0
 800215e:	6078      	str	r0, [r7, #4]
 8002160:	687b      	ldr	r3, [r7, #4]
 8002162:	60fb      	str	r3, [r7, #12]
 8002164:	2000      	movs	r0, #0
 8002166:	f003 fd79 	bl	8005c5c <step_is_calibrated>
 800216a:	4603      	mov	r3, r0
 800216c:	2b00      	cmp	r3, #0
 800216e:	d016      	beq.n	800219e <cmd_step_set_step+0x46>
 8002170:	4b11      	ldr	r3, [pc, #68]	; (80021b8 <cmd_step_set_step+0x60>)
 8002172:	681b      	ldr	r3, [r3, #0]
 8002174:	791b      	ldrb	r3, [r3, #4]
 8002176:	f003 0308 	and.w	r3, r3, #8
 800217a:	2b00      	cmp	r3, #0
 800217c:	d10f      	bne.n	800219e <cmd_step_set_step+0x46>
 800217e:	68fb      	ldr	r3, [r7, #12]
 8002180:	681b      	ldr	r3, [r3, #0]
 8002182:	4619      	mov	r1, r3
 8002184:	2000      	movs	r0, #0
 8002186:	f003 fdc5 	bl	8005d14 <step_set_step_goal>
 800218a:	4603      	mov	r3, r0
 800218c:	2b00      	cmp	r3, #0
 800218e:	d108      	bne.n	80021a2 <cmd_step_set_step+0x4a>
 8002190:	2203      	movs	r2, #3
 8002192:	490a      	ldr	r1, [pc, #40]	; (80021bc <cmd_step_set_step+0x64>)
 8002194:	6878      	ldr	r0, [r7, #4]
 8002196:	f008 fbb9 	bl	800a90c <memcpy>
 800219a:	2303      	movs	r3, #3
 800219c:	e008      	b.n	80021b0 <cmd_step_set_step+0x58>
 800219e:	bf00      	nop
 80021a0:	e000      	b.n	80021a4 <cmd_step_set_step+0x4c>
 80021a2:	bf00      	nop
 80021a4:	2203      	movs	r2, #3
 80021a6:	4906      	ldr	r1, [pc, #24]	; (80021c0 <cmd_step_set_step+0x68>)
 80021a8:	6878      	ldr	r0, [r7, #4]
 80021aa:	f008 fbaf 	bl	800a90c <memcpy>
 80021ae:	2303      	movs	r3, #3
 80021b0:	4618      	mov	r0, r3
 80021b2:	3710      	adds	r7, #16
 80021b4:	46bd      	mov	sp, r7
 80021b6:	bd80      	pop	{r7, pc}
 80021b8:	2000026c 	.word	0x2000026c
 80021bc:	0800d0b8 	.word	0x0800d0b8
 80021c0:	0800d0bc 	.word	0x0800d0bc

080021c4 <cmd_step_is_running>:
 80021c4:	b580      	push	{r7, lr}
 80021c6:	b082      	sub	sp, #8
 80021c8:	af00      	add	r7, sp, #0
 80021ca:	6078      	str	r0, [r7, #4]
 80021cc:	2000      	movs	r0, #0
 80021ce:	f003 fd63 	bl	8005c98 <step_is_running>
 80021d2:	4603      	mov	r3, r0
 80021d4:	2b00      	cmp	r3, #0
 80021d6:	d106      	bne.n	80021e6 <cmd_step_is_running+0x22>
 80021d8:	2203      	movs	r2, #3
 80021da:	4908      	ldr	r1, [pc, #32]	; (80021fc <cmd_step_is_running+0x38>)
 80021dc:	6878      	ldr	r0, [r7, #4]
 80021de:	f008 fb95 	bl	800a90c <memcpy>
 80021e2:	2303      	movs	r3, #3
 80021e4:	e006      	b.n	80021f4 <cmd_step_is_running+0x30>
 80021e6:	bf00      	nop
 80021e8:	2203      	movs	r2, #3
 80021ea:	4905      	ldr	r1, [pc, #20]	; (8002200 <cmd_step_is_running+0x3c>)
 80021ec:	6878      	ldr	r0, [r7, #4]
 80021ee:	f008 fb8d 	bl	800a90c <memcpy>
 80021f2:	2303      	movs	r3, #3
 80021f4:	4618      	mov	r0, r3
 80021f6:	3708      	adds	r7, #8
 80021f8:	46bd      	mov	sp, r7
 80021fa:	bd80      	pop	{r7, pc}
 80021fc:	0800d0b8 	.word	0x0800d0b8
 8002200:	0800d0bc 	.word	0x0800d0bc

08002204 <cmd_paws_open>:
 8002204:	b580      	push	{r7, lr}
 8002206:	b08a      	sub	sp, #40	; 0x28
 8002208:	af00      	add	r7, sp, #0
 800220a:	6078      	str	r0, [r7, #4]
 800220c:	4a45      	ldr	r2, [pc, #276]	; (8002324 <cmd_paws_open+0x120>)
 800220e:	f107 031c 	add.w	r3, r7, #28
 8002212:	ca07      	ldmia	r2, {r0, r1, r2}
 8002214:	c303      	stmia	r3!, {r0, r1}
 8002216:	801a      	strh	r2, [r3, #0]
 8002218:	687b      	ldr	r3, [r7, #4]
 800221a:	781b      	ldrb	r3, [r3, #0]
 800221c:	f887 3026 	strb.w	r3, [r7, #38]	; 0x26
 8002220:	f107 0308 	add.w	r3, r7, #8
 8002224:	2213      	movs	r2, #19
 8002226:	2100      	movs	r1, #0
 8002228:	4618      	mov	r0, r3
 800222a:	f008 fb7d 	bl	800a928 <memset>
 800222e:	2304      	movs	r3, #4
 8002230:	723b      	strb	r3, [r7, #8]
 8002232:	2300      	movs	r3, #0
 8002234:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 8002238:	e045      	b.n	80022c6 <cmd_paws_open+0xc2>
 800223a:	f897 2026 	ldrb.w	r2, [r7, #38]	; 0x26
 800223e:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002242:	fa42 f303 	asr.w	r3, r2, r3
 8002246:	f003 0301 	and.w	r3, r3, #1
 800224a:	2b00      	cmp	r3, #0
 800224c:	d010      	beq.n	8002270 <cmd_paws_open+0x6c>
 800224e:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002252:	4a35      	ldr	r2, [pc, #212]	; (8002328 <cmd_paws_open+0x124>)
 8002254:	6812      	ldr	r2, [r2, #0]
 8002256:	f897 1027 	ldrb.w	r1, [r7, #39]	; 0x27
 800225a:	005b      	lsls	r3, r3, #1
 800225c:	f107 0028 	add.w	r0, r7, #40	; 0x28
 8002260:	4403      	add	r3, r0
 8002262:	f833 0c0c 	ldrh.w	r0, [r3, #-12]
 8002266:	1d0b      	adds	r3, r1, #4
 8002268:	005b      	lsls	r3, r3, #1
 800226a:	4413      	add	r3, r2
 800226c:	4602      	mov	r2, r0
 800226e:	809a      	strh	r2, [r3, #4]
 8002270:	4b2d      	ldr	r3, [pc, #180]	; (8002328 <cmd_paws_open+0x124>)
 8002272:	681a      	ldr	r2, [r3, #0]
 8002274:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002278:	3304      	adds	r3, #4
 800227a:	005b      	lsls	r3, r3, #1
 800227c:	4413      	add	r3, r2
 800227e:	889b      	ldrh	r3, [r3, #4]
 8002280:	0a1b      	lsrs	r3, r3, #8
 8002282:	b29a      	uxth	r2, r3
 8002284:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002288:	005b      	lsls	r3, r3, #1
 800228a:	3301      	adds	r3, #1
 800228c:	b2d2      	uxtb	r2, r2
 800228e:	f107 0128 	add.w	r1, r7, #40	; 0x28
 8002292:	440b      	add	r3, r1
 8002294:	f803 2c20 	strb.w	r2, [r3, #-32]
 8002298:	4b23      	ldr	r3, [pc, #140]	; (8002328 <cmd_paws_open+0x124>)
 800229a:	681a      	ldr	r2, [r3, #0]
 800229c:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80022a0:	3304      	adds	r3, #4
 80022a2:	005b      	lsls	r3, r3, #1
 80022a4:	4413      	add	r3, r2
 80022a6:	889a      	ldrh	r2, [r3, #4]
 80022a8:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80022ac:	3301      	adds	r3, #1
 80022ae:	005b      	lsls	r3, r3, #1
 80022b0:	b2d2      	uxtb	r2, r2
 80022b2:	f107 0128 	add.w	r1, r7, #40	; 0x28
 80022b6:	440b      	add	r3, r1
 80022b8:	f803 2c20 	strb.w	r2, [r3, #-32]
 80022bc:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80022c0:	3301      	adds	r3, #1
 80022c2:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 80022c6:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80022ca:	2b04      	cmp	r3, #4
 80022cc:	d9b5      	bls.n	800223a <cmd_paws_open+0x36>
 80022ce:	f107 0308 	add.w	r3, r7, #8
 80022d2:	2113      	movs	r1, #19
 80022d4:	4618      	mov	r0, r3
 80022d6:	f7ff fa93 	bl	8001800 <stm_driver_send_msg>
 80022da:	4b13      	ldr	r3, [pc, #76]	; (8002328 <cmd_paws_open+0x124>)
 80022dc:	681b      	ldr	r3, [r3, #0]
 80022de:	78da      	ldrb	r2, [r3, #3]
 80022e0:	4b11      	ldr	r3, [pc, #68]	; (8002328 <cmd_paws_open+0x124>)
 80022e2:	681b      	ldr	r3, [r3, #0]
 80022e4:	f042 0201 	orr.w	r2, r2, #1
 80022e8:	b2d2      	uxtb	r2, r2
 80022ea:	70da      	strb	r2, [r3, #3]
 80022ec:	4b0e      	ldr	r3, [pc, #56]	; (8002328 <cmd_paws_open+0x124>)
 80022ee:	681b      	ldr	r3, [r3, #0]
 80022f0:	791a      	ldrb	r2, [r3, #4]
 80022f2:	4b0d      	ldr	r3, [pc, #52]	; (8002328 <cmd_paws_open+0x124>)
 80022f4:	681b      	ldr	r3, [r3, #0]
 80022f6:	f042 0201 	orr.w	r2, r2, #1
 80022fa:	b2d2      	uxtb	r2, r2
 80022fc:	711a      	strb	r2, [r3, #4]
 80022fe:	4b0a      	ldr	r3, [pc, #40]	; (8002328 <cmd_paws_open+0x124>)
 8002300:	681b      	ldr	r3, [r3, #0]
 8002302:	f8d3 0284 	ldr.w	r0, [r3, #644]	; 0x284
 8002306:	2300      	movs	r3, #0
 8002308:	2202      	movs	r2, #2
 800230a:	2100      	movs	r1, #0
 800230c:	f006 fd94 	bl	8008e38 <xTaskGenericNotify>
 8002310:	2203      	movs	r2, #3
 8002312:	4906      	ldr	r1, [pc, #24]	; (800232c <cmd_paws_open+0x128>)
 8002314:	6878      	ldr	r0, [r7, #4]
 8002316:	f008 faf9 	bl	800a90c <memcpy>
 800231a:	2303      	movs	r3, #3
 800231c:	4618      	mov	r0, r3
 800231e:	3728      	adds	r7, #40	; 0x28
 8002320:	46bd      	mov	sp, r7
 8002322:	bd80      	pop	{r7, pc}
 8002324:	0800d098 	.word	0x0800d098
 8002328:	2000026c 	.word	0x2000026c
 800232c:	0800d0b8 	.word	0x0800d0b8

08002330 <cmd_paws_close>:
 8002330:	b580      	push	{r7, lr}
 8002332:	b08a      	sub	sp, #40	; 0x28
 8002334:	af00      	add	r7, sp, #0
 8002336:	6078      	str	r0, [r7, #4]
 8002338:	4a45      	ldr	r2, [pc, #276]	; (8002450 <cmd_paws_close+0x120>)
 800233a:	f107 031c 	add.w	r3, r7, #28
 800233e:	ca07      	ldmia	r2, {r0, r1, r2}
 8002340:	c303      	stmia	r3!, {r0, r1}
 8002342:	801a      	strh	r2, [r3, #0]
 8002344:	687b      	ldr	r3, [r7, #4]
 8002346:	781b      	ldrb	r3, [r3, #0]
 8002348:	f887 3026 	strb.w	r3, [r7, #38]	; 0x26
 800234c:	f107 0308 	add.w	r3, r7, #8
 8002350:	2213      	movs	r2, #19
 8002352:	2100      	movs	r1, #0
 8002354:	4618      	mov	r0, r3
 8002356:	f008 fae7 	bl	800a928 <memset>
 800235a:	2304      	movs	r3, #4
 800235c:	723b      	strb	r3, [r7, #8]
 800235e:	2300      	movs	r3, #0
 8002360:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 8002364:	e045      	b.n	80023f2 <cmd_paws_close+0xc2>
 8002366:	f897 2026 	ldrb.w	r2, [r7, #38]	; 0x26
 800236a:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800236e:	fa42 f303 	asr.w	r3, r2, r3
 8002372:	f003 0301 	and.w	r3, r3, #1
 8002376:	2b00      	cmp	r3, #0
 8002378:	d010      	beq.n	800239c <cmd_paws_close+0x6c>
 800237a:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800237e:	4a35      	ldr	r2, [pc, #212]	; (8002454 <cmd_paws_close+0x124>)
 8002380:	6812      	ldr	r2, [r2, #0]
 8002382:	f897 1027 	ldrb.w	r1, [r7, #39]	; 0x27
 8002386:	005b      	lsls	r3, r3, #1
 8002388:	f107 0028 	add.w	r0, r7, #40	; 0x28
 800238c:	4403      	add	r3, r0
 800238e:	f833 0c0c 	ldrh.w	r0, [r3, #-12]
 8002392:	1d0b      	adds	r3, r1, #4
 8002394:	005b      	lsls	r3, r3, #1
 8002396:	4413      	add	r3, r2
 8002398:	4602      	mov	r2, r0
 800239a:	809a      	strh	r2, [r3, #4]
 800239c:	4b2d      	ldr	r3, [pc, #180]	; (8002454 <cmd_paws_close+0x124>)
 800239e:	681a      	ldr	r2, [r3, #0]
 80023a0:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023a4:	3304      	adds	r3, #4
 80023a6:	005b      	lsls	r3, r3, #1
 80023a8:	4413      	add	r3, r2
 80023aa:	889b      	ldrh	r3, [r3, #4]
 80023ac:	0a1b      	lsrs	r3, r3, #8
 80023ae:	b29a      	uxth	r2, r3
 80023b0:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023b4:	005b      	lsls	r3, r3, #1
 80023b6:	3301      	adds	r3, #1
 80023b8:	b2d2      	uxtb	r2, r2
 80023ba:	f107 0128 	add.w	r1, r7, #40	; 0x28
 80023be:	440b      	add	r3, r1
 80023c0:	f803 2c20 	strb.w	r2, [r3, #-32]
 80023c4:	4b23      	ldr	r3, [pc, #140]	; (8002454 <cmd_paws_close+0x124>)
 80023c6:	681a      	ldr	r2, [r3, #0]
 80023c8:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023cc:	3304      	adds	r3, #4
 80023ce:	005b      	lsls	r3, r3, #1
 80023d0:	4413      	add	r3, r2
 80023d2:	889a      	ldrh	r2, [r3, #4]
 80023d4:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023d8:	3301      	adds	r3, #1
 80023da:	005b      	lsls	r3, r3, #1
 80023dc:	b2d2      	uxtb	r2, r2
 80023de:	f107 0128 	add.w	r1, r7, #40	; 0x28
 80023e2:	440b      	add	r3, r1
 80023e4:	f803 2c20 	strb.w	r2, [r3, #-32]
 80023e8:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023ec:	3301      	adds	r3, #1
 80023ee:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 80023f2:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80023f6:	2b04      	cmp	r3, #4
 80023f8:	d9b5      	bls.n	8002366 <cmd_paws_close+0x36>
 80023fa:	f107 0308 	add.w	r3, r7, #8
 80023fe:	2113      	movs	r1, #19
 8002400:	4618      	mov	r0, r3
 8002402:	f7ff f9fd 	bl	8001800 <stm_driver_send_msg>
 8002406:	4b13      	ldr	r3, [pc, #76]	; (8002454 <cmd_paws_close+0x124>)
 8002408:	681b      	ldr	r3, [r3, #0]
 800240a:	78da      	ldrb	r2, [r3, #3]
 800240c:	4b11      	ldr	r3, [pc, #68]	; (8002454 <cmd_paws_close+0x124>)
 800240e:	681b      	ldr	r3, [r3, #0]
 8002410:	f042 0201 	orr.w	r2, r2, #1
 8002414:	b2d2      	uxtb	r2, r2
 8002416:	70da      	strb	r2, [r3, #3]
 8002418:	4b0e      	ldr	r3, [pc, #56]	; (8002454 <cmd_paws_close+0x124>)
 800241a:	681b      	ldr	r3, [r3, #0]
 800241c:	791a      	ldrb	r2, [r3, #4]
 800241e:	4b0d      	ldr	r3, [pc, #52]	; (8002454 <cmd_paws_close+0x124>)
 8002420:	681b      	ldr	r3, [r3, #0]
 8002422:	f042 0201 	orr.w	r2, r2, #1
 8002426:	b2d2      	uxtb	r2, r2
 8002428:	711a      	strb	r2, [r3, #4]
 800242a:	4b0a      	ldr	r3, [pc, #40]	; (8002454 <cmd_paws_close+0x124>)
 800242c:	681b      	ldr	r3, [r3, #0]
 800242e:	f8d3 0284 	ldr.w	r0, [r3, #644]	; 0x284
 8002432:	2300      	movs	r3, #0
 8002434:	2202      	movs	r2, #2
 8002436:	2100      	movs	r1, #0
 8002438:	f006 fcfe 	bl	8008e38 <xTaskGenericNotify>
 800243c:	2203      	movs	r2, #3
 800243e:	4906      	ldr	r1, [pc, #24]	; (8002458 <cmd_paws_close+0x128>)
 8002440:	6878      	ldr	r0, [r7, #4]
 8002442:	f008 fa63 	bl	800a90c <memcpy>
 8002446:	2303      	movs	r3, #3
 8002448:	4618      	mov	r0, r3
 800244a:	3728      	adds	r7, #40	; 0x28
 800244c:	46bd      	mov	sp, r7
 800244e:	bd80      	pop	{r7, pc}
 8002450:	0800d0c0 	.word	0x0800d0c0
 8002454:	2000026c 	.word	0x2000026c
 8002458:	0800d0b8 	.word	0x0800d0b8

0800245c <cmd_door_close>:
 800245c:	b580      	push	{r7, lr}
 800245e:	b08a      	sub	sp, #40	; 0x28
 8002460:	af00      	add	r7, sp, #0
 8002462:	6078      	str	r0, [r7, #4]
 8002464:	687b      	ldr	r3, [r7, #4]
 8002466:	781b      	ldrb	r3, [r3, #0]
 8002468:	f887 3026 	strb.w	r3, [r7, #38]	; 0x26
 800246c:	4a46      	ldr	r2, [pc, #280]	; (8002588 <cmd_door_close+0x12c>)
 800246e:	f107 031c 	add.w	r3, r7, #28
 8002472:	e892 0003 	ldmia.w	r2, {r0, r1}
 8002476:	e883 0003 	stmia.w	r3, {r0, r1}
 800247a:	f107 0308 	add.w	r3, r7, #8
 800247e:	2213      	movs	r2, #19
 8002480:	2100      	movs	r1, #0
 8002482:	4618      	mov	r0, r3
 8002484:	f008 fa50 	bl	800a928 <memset>
 8002488:	2303      	movs	r3, #3
 800248a:	723b      	strb	r3, [r7, #8]
 800248c:	2300      	movs	r3, #0
 800248e:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 8002492:	e049      	b.n	8002528 <cmd_door_close+0xcc>
 8002494:	f897 2026 	ldrb.w	r2, [r7, #38]	; 0x26
 8002498:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800249c:	fa42 f303 	asr.w	r3, r2, r3
 80024a0:	f003 0301 	and.w	r3, r3, #1
 80024a4:	2b00      	cmp	r3, #0
 80024a6:	d011      	beq.n	80024cc <cmd_door_close+0x70>
 80024a8:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80024ac:	4a37      	ldr	r2, [pc, #220]	; (800258c <cmd_door_close+0x130>)
 80024ae:	6812      	ldr	r2, [r2, #0]
 80024b0:	f897 1027 	ldrb.w	r1, [r7, #39]	; 0x27
 80024b4:	3105      	adds	r1, #5
 80024b6:	005b      	lsls	r3, r3, #1
 80024b8:	f107 0028 	add.w	r0, r7, #40	; 0x28
 80024bc:	4403      	add	r3, r0
 80024be:	f833 0c0c 	ldrh.w	r0, [r3, #-12]
 80024c2:	1d0b      	adds	r3, r1, #4
 80024c4:	005b      	lsls	r3, r3, #1
 80024c6:	4413      	add	r3, r2
 80024c8:	4602      	mov	r2, r0
 80024ca:	809a      	strh	r2, [r3, #4]
 80024cc:	4b2f      	ldr	r3, [pc, #188]	; (800258c <cmd_door_close+0x130>)
 80024ce:	681a      	ldr	r2, [r3, #0]
 80024d0:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80024d4:	3305      	adds	r3, #5
 80024d6:	3304      	adds	r3, #4
 80024d8:	005b      	lsls	r3, r3, #1
 80024da:	4413      	add	r3, r2
 80024dc:	889b      	ldrh	r3, [r3, #4]
 80024de:	0a1b      	lsrs	r3, r3, #8
 80024e0:	b29a      	uxth	r2, r3
 80024e2:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80024e6:	3305      	adds	r3, #5
 80024e8:	005b      	lsls	r3, r3, #1
 80024ea:	3301      	adds	r3, #1
 80024ec:	b2d2      	uxtb	r2, r2
 80024ee:	f107 0128 	add.w	r1, r7, #40	; 0x28
 80024f2:	440b      	add	r3, r1
 80024f4:	f803 2c20 	strb.w	r2, [r3, #-32]
 80024f8:	4b24      	ldr	r3, [pc, #144]	; (800258c <cmd_door_close+0x130>)
 80024fa:	681a      	ldr	r2, [r3, #0]
 80024fc:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002500:	3305      	adds	r3, #5
 8002502:	3304      	adds	r3, #4
 8002504:	005b      	lsls	r3, r3, #1
 8002506:	4413      	add	r3, r2
 8002508:	889a      	ldrh	r2, [r3, #4]
 800250a:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800250e:	3306      	adds	r3, #6
 8002510:	005b      	lsls	r3, r3, #1
 8002512:	b2d2      	uxtb	r2, r2
 8002514:	f107 0128 	add.w	r1, r7, #40	; 0x28
 8002518:	440b      	add	r3, r1
 800251a:	f803 2c20 	strb.w	r2, [r3, #-32]
 800251e:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002522:	3301      	adds	r3, #1
 8002524:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 8002528:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800252c:	2b03      	cmp	r3, #3
 800252e:	d9b1      	bls.n	8002494 <cmd_door_close+0x38>
 8002530:	f107 0308 	add.w	r3, r7, #8
 8002534:	2113      	movs	r1, #19
 8002536:	4618      	mov	r0, r3
 8002538:	f7ff f962 	bl	8001800 <stm_driver_send_msg>
 800253c:	4b13      	ldr	r3, [pc, #76]	; (800258c <cmd_door_close+0x130>)
 800253e:	681b      	ldr	r3, [r3, #0]
 8002540:	78da      	ldrb	r2, [r3, #3]
 8002542:	4b12      	ldr	r3, [pc, #72]	; (800258c <cmd_door_close+0x130>)
 8002544:	681b      	ldr	r3, [r3, #0]
 8002546:	f042 0201 	orr.w	r2, r2, #1
 800254a:	b2d2      	uxtb	r2, r2
 800254c:	70da      	strb	r2, [r3, #3]
 800254e:	4b0f      	ldr	r3, [pc, #60]	; (800258c <cmd_door_close+0x130>)
 8002550:	681b      	ldr	r3, [r3, #0]
 8002552:	791a      	ldrb	r2, [r3, #4]
 8002554:	4b0d      	ldr	r3, [pc, #52]	; (800258c <cmd_door_close+0x130>)
 8002556:	681b      	ldr	r3, [r3, #0]
 8002558:	f042 0201 	orr.w	r2, r2, #1
 800255c:	b2d2      	uxtb	r2, r2
 800255e:	711a      	strb	r2, [r3, #4]
 8002560:	4b0a      	ldr	r3, [pc, #40]	; (800258c <cmd_door_close+0x130>)
 8002562:	681b      	ldr	r3, [r3, #0]
 8002564:	f8d3 0284 	ldr.w	r0, [r3, #644]	; 0x284
 8002568:	2300      	movs	r3, #0
 800256a:	2202      	movs	r2, #2
 800256c:	2100      	movs	r1, #0
 800256e:	f006 fc63 	bl	8008e38 <xTaskGenericNotify>
 8002572:	2203      	movs	r2, #3
 8002574:	4906      	ldr	r1, [pc, #24]	; (8002590 <cmd_door_close+0x134>)
 8002576:	6878      	ldr	r0, [r7, #4]
 8002578:	f008 f9c8 	bl	800a90c <memcpy>
 800257c:	2303      	movs	r3, #3
 800257e:	4618      	mov	r0, r3
 8002580:	3728      	adds	r7, #40	; 0x28
 8002582:	46bd      	mov	sp, r7
 8002584:	bd80      	pop	{r7, pc}
 8002586:	bf00      	nop
 8002588:	0800d0cc 	.word	0x0800d0cc
 800258c:	2000026c 	.word	0x2000026c
 8002590:	0800d0b8 	.word	0x0800d0b8

08002594 <cmd_door_open>:
 8002594:	b580      	push	{r7, lr}
 8002596:	b08a      	sub	sp, #40	; 0x28
 8002598:	af00      	add	r7, sp, #0
 800259a:	6078      	str	r0, [r7, #4]
 800259c:	687b      	ldr	r3, [r7, #4]
 800259e:	781b      	ldrb	r3, [r3, #0]
 80025a0:	f887 3026 	strb.w	r3, [r7, #38]	; 0x26
 80025a4:	4a46      	ldr	r2, [pc, #280]	; (80026c0 <cmd_door_open+0x12c>)
 80025a6:	f107 031c 	add.w	r3, r7, #28
 80025aa:	e892 0003 	ldmia.w	r2, {r0, r1}
 80025ae:	e883 0003 	stmia.w	r3, {r0, r1}
 80025b2:	f107 0308 	add.w	r3, r7, #8
 80025b6:	2213      	movs	r2, #19
 80025b8:	2100      	movs	r1, #0
 80025ba:	4618      	mov	r0, r3
 80025bc:	f008 f9b4 	bl	800a928 <memset>
 80025c0:	2303      	movs	r3, #3
 80025c2:	723b      	strb	r3, [r7, #8]
 80025c4:	2300      	movs	r3, #0
 80025c6:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 80025ca:	e049      	b.n	8002660 <cmd_door_open+0xcc>
 80025cc:	f897 2026 	ldrb.w	r2, [r7, #38]	; 0x26
 80025d0:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80025d4:	fa42 f303 	asr.w	r3, r2, r3
 80025d8:	f003 0301 	and.w	r3, r3, #1
 80025dc:	2b00      	cmp	r3, #0
 80025de:	d011      	beq.n	8002604 <cmd_door_open+0x70>
 80025e0:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 80025e4:	4a37      	ldr	r2, [pc, #220]	; (80026c4 <cmd_door_open+0x130>)
 80025e6:	6812      	ldr	r2, [r2, #0]
 80025e8:	f897 1027 	ldrb.w	r1, [r7, #39]	; 0x27
 80025ec:	3105      	adds	r1, #5
 80025ee:	005b      	lsls	r3, r3, #1
 80025f0:	f107 0028 	add.w	r0, r7, #40	; 0x28
 80025f4:	4403      	add	r3, r0
 80025f6:	f833 0c0c 	ldrh.w	r0, [r3, #-12]
 80025fa:	1d0b      	adds	r3, r1, #4
 80025fc:	005b      	lsls	r3, r3, #1
 80025fe:	4413      	add	r3, r2
 8002600:	4602      	mov	r2, r0
 8002602:	809a      	strh	r2, [r3, #4]
 8002604:	4b2f      	ldr	r3, [pc, #188]	; (80026c4 <cmd_door_open+0x130>)
 8002606:	681a      	ldr	r2, [r3, #0]
 8002608:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800260c:	3305      	adds	r3, #5
 800260e:	3304      	adds	r3, #4
 8002610:	005b      	lsls	r3, r3, #1
 8002612:	4413      	add	r3, r2
 8002614:	889b      	ldrh	r3, [r3, #4]
 8002616:	0a1b      	lsrs	r3, r3, #8
 8002618:	b29a      	uxth	r2, r3
 800261a:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800261e:	3305      	adds	r3, #5
 8002620:	005b      	lsls	r3, r3, #1
 8002622:	3301      	adds	r3, #1
 8002624:	b2d2      	uxtb	r2, r2
 8002626:	f107 0128 	add.w	r1, r7, #40	; 0x28
 800262a:	440b      	add	r3, r1
 800262c:	f803 2c20 	strb.w	r2, [r3, #-32]
 8002630:	4b24      	ldr	r3, [pc, #144]	; (80026c4 <cmd_door_open+0x130>)
 8002632:	681a      	ldr	r2, [r3, #0]
 8002634:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002638:	3305      	adds	r3, #5
 800263a:	3304      	adds	r3, #4
 800263c:	005b      	lsls	r3, r3, #1
 800263e:	4413      	add	r3, r2
 8002640:	889a      	ldrh	r2, [r3, #4]
 8002642:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002646:	3306      	adds	r3, #6
 8002648:	005b      	lsls	r3, r3, #1
 800264a:	b2d2      	uxtb	r2, r2
 800264c:	f107 0128 	add.w	r1, r7, #40	; 0x28
 8002650:	440b      	add	r3, r1
 8002652:	f803 2c20 	strb.w	r2, [r3, #-32]
 8002656:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 800265a:	3301      	adds	r3, #1
 800265c:	f887 3027 	strb.w	r3, [r7, #39]	; 0x27
 8002660:	f897 3027 	ldrb.w	r3, [r7, #39]	; 0x27
 8002664:	2b03      	cmp	r3, #3
 8002666:	d9b1      	bls.n	80025cc <cmd_door_open+0x38>
 8002668:	f107 0308 	add.w	r3, r7, #8
 800266c:	2113      	movs	r1, #19
 800266e:	4618      	mov	r0, r3
 8002670:	f7ff f8c6 	bl	8001800 <stm_driver_send_msg>
 8002674:	4b13      	ldr	r3, [pc, #76]	; (80026c4 <cmd_door_open+0x130>)
 8002676:	681b      	ldr	r3, [r3, #0]
 8002678:	78da      	ldrb	r2, [r3, #3]
 800267a:	4b12      	ldr	r3, [pc, #72]	; (80026c4 <cmd_door_open+0x130>)
 800267c:	681b      	ldr	r3, [r3, #0]
 800267e:	f042 0201 	orr.w	r2, r2, #1
 8002682:	b2d2      	uxtb	r2, r2
 8002684:	70da      	strb	r2, [r3, #3]
 8002686:	4b0f      	ldr	r3, [pc, #60]	; (80026c4 <cmd_door_open+0x130>)
 8002688:	681b      	ldr	r3, [r3, #0]
 800268a:	791a      	ldrb	r2, [r3, #4]
 800268c:	4b0d      	ldr	r3, [pc, #52]	; (80026c4 <cmd_door_open+0x130>)
 800268e:	681b      	ldr	r3, [r3, #0]
 8002690:	f042 0201 	orr.w	r2, r2, #1
 8002694:	b2d2      	uxtb	r2, r2
 8002696:	711a      	strb	r2, [r3, #4]
 8002698:	4b0a      	ldr	r3, [pc, #40]	; (80026c4 <cmd_door_open+0x130>)
 800269a:	681b      	ldr	r3, [r3, #0]
 800269c:	f8d3 0284 	ldr.w	r0, [r3, #644]	; 0x284
 80026a0:	2300      	movs	r3, #0
 80026a2:	2202      	movs	r2, #2
 80026a4:	2100      	movs	r1, #0
 80026a6:	f006 fbc7 	bl	8008e38 <xTaskGenericNotify>
 80026aa:	2203      	movs	r2, #3
 80026ac:	4906      	ldr	r1, [pc, #24]	; (80026c8 <cmd_door_open+0x134>)
 80026ae:	6878      	ldr	r0, [r7, #4]
 80026b0:	f008 f92c 	bl	800a90c <memcpy>
 80026b4:	2303      	movs	r3, #3
 80026b6:	4618      	mov	r0, r3
 80026b8:	3728      	adds	r7, #40	; 0x28
 80026ba:	46bd      	mov	sp, r7
 80026bc:	bd80      	pop	{r7, pc}
 80026be:	bf00      	nop
 80026c0:	0800d090 	.word	0x0800d090
 80026c4:	2000026c 	.word	0x2000026c
 80026c8:	0800d0b8 	.word	0x0800d0b8

080026cc <TIM1_UP_TIM10_IRQHandler>:
 80026cc:	b580      	push	{r7, lr}
 80026ce:	b082      	sub	sp, #8
 80026d0:	af00      	add	r7, sp, #0
 80026d2:	2300      	movs	r3, #0
 80026d4:	607b      	str	r3, [r7, #4]
 80026d6:	4814      	ldr	r0, [pc, #80]	; (8002728 <TIM1_UP_TIM10_IRQHandler+0x5c>)
 80026d8:	f7ff f85e 	bl	8001798 <LL_TIM_IsActiveFlag_UPDATE>
 80026dc:	4603      	mov	r3, r0
 80026de:	2b00      	cmp	r3, #0
 80026e0:	d012      	beq.n	8002708 <TIM1_UP_TIM10_IRQHandler+0x3c>
 80026e2:	4811      	ldr	r0, [pc, #68]	; (8002728 <TIM1_UP_TIM10_IRQHandler+0x5c>)
 80026e4:	f7ff f84a 	bl	800177c <LL_TIM_ClearFlag_UPDATE>
 80026e8:	4b10      	ldr	r3, [pc, #64]	; (800272c <TIM1_UP_TIM10_IRQHandler+0x60>)
 80026ea:	681b      	ldr	r3, [r3, #0]
 80026ec:	689a      	ldr	r2, [r3, #8]
 80026ee:	4b0f      	ldr	r3, [pc, #60]	; (800272c <TIM1_UP_TIM10_IRQHandler+0x60>)
 80026f0:	681b      	ldr	r3, [r3, #0]
 80026f2:	3201      	adds	r2, #1
 80026f4:	609a      	str	r2, [r3, #8]
 80026f6:	4b0d      	ldr	r3, [pc, #52]	; (800272c <TIM1_UP_TIM10_IRQHandler+0x60>)
 80026f8:	681b      	ldr	r3, [r3, #0]
 80026fa:	f8d3 3284 	ldr.w	r3, [r3, #644]	; 0x284
 80026fe:	1d3a      	adds	r2, r7, #4
 8002700:	4611      	mov	r1, r2
 8002702:	4618      	mov	r0, r3
 8002704:	f006 fc54 	bl	8008fb0 <vTaskNotifyGiveFromISR>
 8002708:	687b      	ldr	r3, [r7, #4]
 800270a:	2b00      	cmp	r3, #0
 800270c:	d007      	beq.n	800271e <TIM1_UP_TIM10_IRQHandler+0x52>
 800270e:	4b08      	ldr	r3, [pc, #32]	; (8002730 <TIM1_UP_TIM10_IRQHandler+0x64>)
 8002710:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8002714:	601a      	str	r2, [r3, #0]
 8002716:	f3bf 8f4f 	dsb	sy
 800271a:	f3bf 8f6f 	isb	sy
 800271e:	bf00      	nop
 8002720:	3708      	adds	r7, #8
 8002722:	46bd      	mov	sp, r7
 8002724:	bd80      	pop	{r7, pc}
 8002726:	bf00      	nop
 8002728:	40014400 	.word	0x40014400
 800272c:	2000026c 	.word	0x2000026c
 8002730:	e000ed04 	.word	0xe000ed04

08002734 <NVIC_EnableIRQ>:
 8002734:	b480      	push	{r7}
 8002736:	b083      	sub	sp, #12
 8002738:	af00      	add	r7, sp, #0
 800273a:	4603      	mov	r3, r0
 800273c:	71fb      	strb	r3, [r7, #7]
 800273e:	79fb      	ldrb	r3, [r7, #7]
 8002740:	f003 021f 	and.w	r2, r3, #31
 8002744:	4907      	ldr	r1, [pc, #28]	; (8002764 <NVIC_EnableIRQ+0x30>)
 8002746:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800274a:	095b      	lsrs	r3, r3, #5
 800274c:	2001      	movs	r0, #1
 800274e:	fa00 f202 	lsl.w	r2, r0, r2
 8002752:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8002756:	bf00      	nop
 8002758:	370c      	adds	r7, #12
 800275a:	46bd      	mov	sp, r7
 800275c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002760:	4770      	bx	lr
 8002762:	bf00      	nop
 8002764:	e000e100 	.word	0xe000e100

08002768 <NVIC_SetPriority>:
 8002768:	b480      	push	{r7}
 800276a:	b083      	sub	sp, #12
 800276c:	af00      	add	r7, sp, #0
 800276e:	4603      	mov	r3, r0
 8002770:	6039      	str	r1, [r7, #0]
 8002772:	71fb      	strb	r3, [r7, #7]
 8002774:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002778:	2b00      	cmp	r3, #0
 800277a:	da0b      	bge.n	8002794 <NVIC_SetPriority+0x2c>
 800277c:	683b      	ldr	r3, [r7, #0]
 800277e:	b2da      	uxtb	r2, r3
 8002780:	490c      	ldr	r1, [pc, #48]	; (80027b4 <NVIC_SetPriority+0x4c>)
 8002782:	79fb      	ldrb	r3, [r7, #7]
 8002784:	f003 030f 	and.w	r3, r3, #15
 8002788:	3b04      	subs	r3, #4
 800278a:	0112      	lsls	r2, r2, #4
 800278c:	b2d2      	uxtb	r2, r2
 800278e:	440b      	add	r3, r1
 8002790:	761a      	strb	r2, [r3, #24]
 8002792:	e009      	b.n	80027a8 <NVIC_SetPriority+0x40>
 8002794:	683b      	ldr	r3, [r7, #0]
 8002796:	b2da      	uxtb	r2, r3
 8002798:	4907      	ldr	r1, [pc, #28]	; (80027b8 <NVIC_SetPriority+0x50>)
 800279a:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800279e:	0112      	lsls	r2, r2, #4
 80027a0:	b2d2      	uxtb	r2, r2
 80027a2:	440b      	add	r3, r1
 80027a4:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 80027a8:	bf00      	nop
 80027aa:	370c      	adds	r7, #12
 80027ac:	46bd      	mov	sp, r7
 80027ae:	f85d 7b04 	ldr.w	r7, [sp], #4
 80027b2:	4770      	bx	lr
 80027b4:	e000ed00 	.word	0xe000ed00
 80027b8:	e000e100 	.word	0xe000e100

080027bc <LL_GPIO_SetPinMode>:
 80027bc:	b480      	push	{r7}
 80027be:	b089      	sub	sp, #36	; 0x24
 80027c0:	af00      	add	r7, sp, #0
 80027c2:	60f8      	str	r0, [r7, #12]
 80027c4:	60b9      	str	r1, [r7, #8]
 80027c6:	607a      	str	r2, [r7, #4]
 80027c8:	68fb      	ldr	r3, [r7, #12]
 80027ca:	681a      	ldr	r2, [r3, #0]
 80027cc:	68bb      	ldr	r3, [r7, #8]
 80027ce:	617b      	str	r3, [r7, #20]
 80027d0:	697b      	ldr	r3, [r7, #20]
 80027d2:	fa93 f3a3 	rbit	r3, r3
 80027d6:	613b      	str	r3, [r7, #16]
 80027d8:	693b      	ldr	r3, [r7, #16]
 80027da:	fab3 f383 	clz	r3, r3
 80027de:	005b      	lsls	r3, r3, #1
 80027e0:	2103      	movs	r1, #3
 80027e2:	fa01 f303 	lsl.w	r3, r1, r3
 80027e6:	43db      	mvns	r3, r3
 80027e8:	401a      	ands	r2, r3
 80027ea:	68bb      	ldr	r3, [r7, #8]
 80027ec:	61fb      	str	r3, [r7, #28]
 80027ee:	69fb      	ldr	r3, [r7, #28]
 80027f0:	fa93 f3a3 	rbit	r3, r3
 80027f4:	61bb      	str	r3, [r7, #24]
 80027f6:	69bb      	ldr	r3, [r7, #24]
 80027f8:	fab3 f383 	clz	r3, r3
 80027fc:	005b      	lsls	r3, r3, #1
 80027fe:	6879      	ldr	r1, [r7, #4]
 8002800:	fa01 f303 	lsl.w	r3, r1, r3
 8002804:	431a      	orrs	r2, r3
 8002806:	68fb      	ldr	r3, [r7, #12]
 8002808:	601a      	str	r2, [r3, #0]
 800280a:	bf00      	nop
 800280c:	3724      	adds	r7, #36	; 0x24
 800280e:	46bd      	mov	sp, r7
 8002810:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002814:	4770      	bx	lr

08002816 <LL_GPIO_SetPinOutputType>:
 8002816:	b480      	push	{r7}
 8002818:	b085      	sub	sp, #20
 800281a:	af00      	add	r7, sp, #0
 800281c:	60f8      	str	r0, [r7, #12]
 800281e:	60b9      	str	r1, [r7, #8]
 8002820:	607a      	str	r2, [r7, #4]
 8002822:	68fb      	ldr	r3, [r7, #12]
 8002824:	685a      	ldr	r2, [r3, #4]
 8002826:	68bb      	ldr	r3, [r7, #8]
 8002828:	43db      	mvns	r3, r3
 800282a:	401a      	ands	r2, r3
 800282c:	68bb      	ldr	r3, [r7, #8]
 800282e:	6879      	ldr	r1, [r7, #4]
 8002830:	fb01 f303 	mul.w	r3, r1, r3
 8002834:	431a      	orrs	r2, r3
 8002836:	68fb      	ldr	r3, [r7, #12]
 8002838:	605a      	str	r2, [r3, #4]
 800283a:	bf00      	nop
 800283c:	3714      	adds	r7, #20
 800283e:	46bd      	mov	sp, r7
 8002840:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002844:	4770      	bx	lr

08002846 <LL_GPIO_SetPinPull>:
 8002846:	b480      	push	{r7}
 8002848:	b089      	sub	sp, #36	; 0x24
 800284a:	af00      	add	r7, sp, #0
 800284c:	60f8      	str	r0, [r7, #12]
 800284e:	60b9      	str	r1, [r7, #8]
 8002850:	607a      	str	r2, [r7, #4]
 8002852:	68fb      	ldr	r3, [r7, #12]
 8002854:	68da      	ldr	r2, [r3, #12]
 8002856:	68bb      	ldr	r3, [r7, #8]
 8002858:	617b      	str	r3, [r7, #20]
 800285a:	697b      	ldr	r3, [r7, #20]
 800285c:	fa93 f3a3 	rbit	r3, r3
 8002860:	613b      	str	r3, [r7, #16]
 8002862:	693b      	ldr	r3, [r7, #16]
 8002864:	fab3 f383 	clz	r3, r3
 8002868:	005b      	lsls	r3, r3, #1
 800286a:	2103      	movs	r1, #3
 800286c:	fa01 f303 	lsl.w	r3, r1, r3
 8002870:	43db      	mvns	r3, r3
 8002872:	401a      	ands	r2, r3
 8002874:	68bb      	ldr	r3, [r7, #8]
 8002876:	61fb      	str	r3, [r7, #28]
 8002878:	69fb      	ldr	r3, [r7, #28]
 800287a:	fa93 f3a3 	rbit	r3, r3
 800287e:	61bb      	str	r3, [r7, #24]
 8002880:	69bb      	ldr	r3, [r7, #24]
 8002882:	fab3 f383 	clz	r3, r3
 8002886:	005b      	lsls	r3, r3, #1
 8002888:	6879      	ldr	r1, [r7, #4]
 800288a:	fa01 f303 	lsl.w	r3, r1, r3
 800288e:	431a      	orrs	r2, r3
 8002890:	68fb      	ldr	r3, [r7, #12]
 8002892:	60da      	str	r2, [r3, #12]
 8002894:	bf00      	nop
 8002896:	3724      	adds	r7, #36	; 0x24
 8002898:	46bd      	mov	sp, r7
 800289a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800289e:	4770      	bx	lr

080028a0 <LL_GPIO_SetAFPin_8_15>:
 80028a0:	b480      	push	{r7}
 80028a2:	b089      	sub	sp, #36	; 0x24
 80028a4:	af00      	add	r7, sp, #0
 80028a6:	60f8      	str	r0, [r7, #12]
 80028a8:	60b9      	str	r1, [r7, #8]
 80028aa:	607a      	str	r2, [r7, #4]
 80028ac:	68fb      	ldr	r3, [r7, #12]
 80028ae:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 80028b0:	68bb      	ldr	r3, [r7, #8]
 80028b2:	0a1b      	lsrs	r3, r3, #8
 80028b4:	617b      	str	r3, [r7, #20]
 80028b6:	697b      	ldr	r3, [r7, #20]
 80028b8:	fa93 f3a3 	rbit	r3, r3
 80028bc:	613b      	str	r3, [r7, #16]
 80028be:	693b      	ldr	r3, [r7, #16]
 80028c0:	fab3 f383 	clz	r3, r3
 80028c4:	009b      	lsls	r3, r3, #2
 80028c6:	210f      	movs	r1, #15
 80028c8:	fa01 f303 	lsl.w	r3, r1, r3
 80028cc:	43db      	mvns	r3, r3
 80028ce:	401a      	ands	r2, r3
 80028d0:	68bb      	ldr	r3, [r7, #8]
 80028d2:	0a1b      	lsrs	r3, r3, #8
 80028d4:	61fb      	str	r3, [r7, #28]
 80028d6:	69fb      	ldr	r3, [r7, #28]
 80028d8:	fa93 f3a3 	rbit	r3, r3
 80028dc:	61bb      	str	r3, [r7, #24]
 80028de:	69bb      	ldr	r3, [r7, #24]
 80028e0:	fab3 f383 	clz	r3, r3
 80028e4:	009b      	lsls	r3, r3, #2
 80028e6:	6879      	ldr	r1, [r7, #4]
 80028e8:	fa01 f303 	lsl.w	r3, r1, r3
 80028ec:	431a      	orrs	r2, r3
 80028ee:	68fb      	ldr	r3, [r7, #12]
 80028f0:	625a      	str	r2, [r3, #36]	; 0x24
 80028f2:	bf00      	nop
 80028f4:	3724      	adds	r7, #36	; 0x24
 80028f6:	46bd      	mov	sp, r7
 80028f8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80028fc:	4770      	bx	lr

080028fe <LL_GPIO_IsInputPinSet>:
 80028fe:	b480      	push	{r7}
 8002900:	b083      	sub	sp, #12
 8002902:	af00      	add	r7, sp, #0
 8002904:	6078      	str	r0, [r7, #4]
 8002906:	6039      	str	r1, [r7, #0]
 8002908:	687b      	ldr	r3, [r7, #4]
 800290a:	691a      	ldr	r2, [r3, #16]
 800290c:	683b      	ldr	r3, [r7, #0]
 800290e:	4013      	ands	r3, r2
 8002910:	683a      	ldr	r2, [r7, #0]
 8002912:	429a      	cmp	r2, r3
 8002914:	bf0c      	ite	eq
 8002916:	2301      	moveq	r3, #1
 8002918:	2300      	movne	r3, #0
 800291a:	b2db      	uxtb	r3, r3
 800291c:	4618      	mov	r0, r3
 800291e:	370c      	adds	r7, #12
 8002920:	46bd      	mov	sp, r7
 8002922:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002926:	4770      	bx	lr

08002928 <LL_GPIO_SetOutputPin>:
 8002928:	b480      	push	{r7}
 800292a:	b083      	sub	sp, #12
 800292c:	af00      	add	r7, sp, #0
 800292e:	6078      	str	r0, [r7, #4]
 8002930:	6039      	str	r1, [r7, #0]
 8002932:	687b      	ldr	r3, [r7, #4]
 8002934:	683a      	ldr	r2, [r7, #0]
 8002936:	619a      	str	r2, [r3, #24]
 8002938:	bf00      	nop
 800293a:	370c      	adds	r7, #12
 800293c:	46bd      	mov	sp, r7
 800293e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002942:	4770      	bx	lr

08002944 <LL_GPIO_ResetOutputPin>:
 8002944:	b480      	push	{r7}
 8002946:	b083      	sub	sp, #12
 8002948:	af00      	add	r7, sp, #0
 800294a:	6078      	str	r0, [r7, #4]
 800294c:	6039      	str	r1, [r7, #0]
 800294e:	683b      	ldr	r3, [r7, #0]
 8002950:	041a      	lsls	r2, r3, #16
 8002952:	687b      	ldr	r3, [r7, #4]
 8002954:	619a      	str	r2, [r3, #24]
 8002956:	bf00      	nop
 8002958:	370c      	adds	r7, #12
 800295a:	46bd      	mov	sp, r7
 800295c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002960:	4770      	bx	lr
	...

08002964 <LL_AHB1_GRP1_EnableClock>:
 8002964:	b480      	push	{r7}
 8002966:	b085      	sub	sp, #20
 8002968:	af00      	add	r7, sp, #0
 800296a:	6078      	str	r0, [r7, #4]
 800296c:	4b08      	ldr	r3, [pc, #32]	; (8002990 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800296e:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8002970:	4907      	ldr	r1, [pc, #28]	; (8002990 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002972:	687b      	ldr	r3, [r7, #4]
 8002974:	4313      	orrs	r3, r2
 8002976:	630b      	str	r3, [r1, #48]	; 0x30
 8002978:	4b05      	ldr	r3, [pc, #20]	; (8002990 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800297a:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800297c:	687b      	ldr	r3, [r7, #4]
 800297e:	4013      	ands	r3, r2
 8002980:	60fb      	str	r3, [r7, #12]
 8002982:	68fb      	ldr	r3, [r7, #12]
 8002984:	bf00      	nop
 8002986:	3714      	adds	r7, #20
 8002988:	46bd      	mov	sp, r7
 800298a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800298e:	4770      	bx	lr
 8002990:	40023800 	.word	0x40023800

08002994 <LL_APB1_GRP1_EnableClock>:
 8002994:	b480      	push	{r7}
 8002996:	b085      	sub	sp, #20
 8002998:	af00      	add	r7, sp, #0
 800299a:	6078      	str	r0, [r7, #4]
 800299c:	4b08      	ldr	r3, [pc, #32]	; (80029c0 <LL_APB1_GRP1_EnableClock+0x2c>)
 800299e:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 80029a0:	4907      	ldr	r1, [pc, #28]	; (80029c0 <LL_APB1_GRP1_EnableClock+0x2c>)
 80029a2:	687b      	ldr	r3, [r7, #4]
 80029a4:	4313      	orrs	r3, r2
 80029a6:	640b      	str	r3, [r1, #64]	; 0x40
 80029a8:	4b05      	ldr	r3, [pc, #20]	; (80029c0 <LL_APB1_GRP1_EnableClock+0x2c>)
 80029aa:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 80029ac:	687b      	ldr	r3, [r7, #4]
 80029ae:	4013      	ands	r3, r2
 80029b0:	60fb      	str	r3, [r7, #12]
 80029b2:	68fb      	ldr	r3, [r7, #12]
 80029b4:	bf00      	nop
 80029b6:	3714      	adds	r7, #20
 80029b8:	46bd      	mov	sp, r7
 80029ba:	f85d 7b04 	ldr.w	r7, [sp], #4
 80029be:	4770      	bx	lr
 80029c0:	40023800 	.word	0x40023800

080029c4 <LL_APB2_GRP1_EnableClock>:
 80029c4:	b480      	push	{r7}
 80029c6:	b085      	sub	sp, #20
 80029c8:	af00      	add	r7, sp, #0
 80029ca:	6078      	str	r0, [r7, #4]
 80029cc:	4b08      	ldr	r3, [pc, #32]	; (80029f0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80029ce:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80029d0:	4907      	ldr	r1, [pc, #28]	; (80029f0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80029d2:	687b      	ldr	r3, [r7, #4]
 80029d4:	4313      	orrs	r3, r2
 80029d6:	644b      	str	r3, [r1, #68]	; 0x44
 80029d8:	4b05      	ldr	r3, [pc, #20]	; (80029f0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80029da:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80029dc:	687b      	ldr	r3, [r7, #4]
 80029de:	4013      	ands	r3, r2
 80029e0:	60fb      	str	r3, [r7, #12]
 80029e2:	68fb      	ldr	r3, [r7, #12]
 80029e4:	bf00      	nop
 80029e6:	3714      	adds	r7, #20
 80029e8:	46bd      	mov	sp, r7
 80029ea:	f85d 7b04 	ldr.w	r7, [sp], #4
 80029ee:	4770      	bx	lr
 80029f0:	40023800 	.word	0x40023800

080029f4 <LL_TIM_EnableCounter>:
 80029f4:	b480      	push	{r7}
 80029f6:	b083      	sub	sp, #12
 80029f8:	af00      	add	r7, sp, #0
 80029fa:	6078      	str	r0, [r7, #4]
 80029fc:	687b      	ldr	r3, [r7, #4]
 80029fe:	681b      	ldr	r3, [r3, #0]
 8002a00:	f043 0201 	orr.w	r2, r3, #1
 8002a04:	687b      	ldr	r3, [r7, #4]
 8002a06:	601a      	str	r2, [r3, #0]
 8002a08:	bf00      	nop
 8002a0a:	370c      	adds	r7, #12
 8002a0c:	46bd      	mov	sp, r7
 8002a0e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a12:	4770      	bx	lr

08002a14 <LL_TIM_IsEnabledCounter>:
 8002a14:	b480      	push	{r7}
 8002a16:	b083      	sub	sp, #12
 8002a18:	af00      	add	r7, sp, #0
 8002a1a:	6078      	str	r0, [r7, #4]
 8002a1c:	687b      	ldr	r3, [r7, #4]
 8002a1e:	681b      	ldr	r3, [r3, #0]
 8002a20:	f003 0301 	and.w	r3, r3, #1
 8002a24:	2b01      	cmp	r3, #1
 8002a26:	bf0c      	ite	eq
 8002a28:	2301      	moveq	r3, #1
 8002a2a:	2300      	movne	r3, #0
 8002a2c:	b2db      	uxtb	r3, r3
 8002a2e:	4618      	mov	r0, r3
 8002a30:	370c      	adds	r7, #12
 8002a32:	46bd      	mov	sp, r7
 8002a34:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a38:	4770      	bx	lr

08002a3a <LL_TIM_EnableUpdateEvent>:
 8002a3a:	b480      	push	{r7}
 8002a3c:	b083      	sub	sp, #12
 8002a3e:	af00      	add	r7, sp, #0
 8002a40:	6078      	str	r0, [r7, #4]
 8002a42:	687b      	ldr	r3, [r7, #4]
 8002a44:	681b      	ldr	r3, [r3, #0]
 8002a46:	f023 0202 	bic.w	r2, r3, #2
 8002a4a:	687b      	ldr	r3, [r7, #4]
 8002a4c:	601a      	str	r2, [r3, #0]
 8002a4e:	bf00      	nop
 8002a50:	370c      	adds	r7, #12
 8002a52:	46bd      	mov	sp, r7
 8002a54:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a58:	4770      	bx	lr

08002a5a <LL_TIM_SetUpdateSource>:
 8002a5a:	b480      	push	{r7}
 8002a5c:	b083      	sub	sp, #12
 8002a5e:	af00      	add	r7, sp, #0
 8002a60:	6078      	str	r0, [r7, #4]
 8002a62:	6039      	str	r1, [r7, #0]
 8002a64:	687b      	ldr	r3, [r7, #4]
 8002a66:	681b      	ldr	r3, [r3, #0]
 8002a68:	f023 0204 	bic.w	r2, r3, #4
 8002a6c:	683b      	ldr	r3, [r7, #0]
 8002a6e:	431a      	orrs	r2, r3
 8002a70:	687b      	ldr	r3, [r7, #4]
 8002a72:	601a      	str	r2, [r3, #0]
 8002a74:	bf00      	nop
 8002a76:	370c      	adds	r7, #12
 8002a78:	46bd      	mov	sp, r7
 8002a7a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a7e:	4770      	bx	lr

08002a80 <LL_TIM_SetCounterMode>:
 8002a80:	b480      	push	{r7}
 8002a82:	b083      	sub	sp, #12
 8002a84:	af00      	add	r7, sp, #0
 8002a86:	6078      	str	r0, [r7, #4]
 8002a88:	6039      	str	r1, [r7, #0]
 8002a8a:	687b      	ldr	r3, [r7, #4]
 8002a8c:	681b      	ldr	r3, [r3, #0]
 8002a8e:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8002a92:	683b      	ldr	r3, [r7, #0]
 8002a94:	431a      	orrs	r2, r3
 8002a96:	687b      	ldr	r3, [r7, #4]
 8002a98:	601a      	str	r2, [r3, #0]
 8002a9a:	bf00      	nop
 8002a9c:	370c      	adds	r7, #12
 8002a9e:	46bd      	mov	sp, r7
 8002aa0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002aa4:	4770      	bx	lr

08002aa6 <LL_TIM_EnableARRPreload>:
 8002aa6:	b480      	push	{r7}
 8002aa8:	b083      	sub	sp, #12
 8002aaa:	af00      	add	r7, sp, #0
 8002aac:	6078      	str	r0, [r7, #4]
 8002aae:	687b      	ldr	r3, [r7, #4]
 8002ab0:	681b      	ldr	r3, [r3, #0]
 8002ab2:	f043 0280 	orr.w	r2, r3, #128	; 0x80
 8002ab6:	687b      	ldr	r3, [r7, #4]
 8002ab8:	601a      	str	r2, [r3, #0]
 8002aba:	bf00      	nop
 8002abc:	370c      	adds	r7, #12
 8002abe:	46bd      	mov	sp, r7
 8002ac0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002ac4:	4770      	bx	lr

08002ac6 <LL_TIM_SetClockDivision>:
 8002ac6:	b480      	push	{r7}
 8002ac8:	b083      	sub	sp, #12
 8002aca:	af00      	add	r7, sp, #0
 8002acc:	6078      	str	r0, [r7, #4]
 8002ace:	6039      	str	r1, [r7, #0]
 8002ad0:	687b      	ldr	r3, [r7, #4]
 8002ad2:	681b      	ldr	r3, [r3, #0]
 8002ad4:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8002ad8:	683b      	ldr	r3, [r7, #0]
 8002ada:	431a      	orrs	r2, r3
 8002adc:	687b      	ldr	r3, [r7, #4]
 8002ade:	601a      	str	r2, [r3, #0]
 8002ae0:	bf00      	nop
 8002ae2:	370c      	adds	r7, #12
 8002ae4:	46bd      	mov	sp, r7
 8002ae6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002aea:	4770      	bx	lr

08002aec <LL_TIM_SetPrescaler>:
 8002aec:	b480      	push	{r7}
 8002aee:	b083      	sub	sp, #12
 8002af0:	af00      	add	r7, sp, #0
 8002af2:	6078      	str	r0, [r7, #4]
 8002af4:	6039      	str	r1, [r7, #0]
 8002af6:	687b      	ldr	r3, [r7, #4]
 8002af8:	683a      	ldr	r2, [r7, #0]
 8002afa:	629a      	str	r2, [r3, #40]	; 0x28
 8002afc:	bf00      	nop
 8002afe:	370c      	adds	r7, #12
 8002b00:	46bd      	mov	sp, r7
 8002b02:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002b06:	4770      	bx	lr

08002b08 <LL_TIM_SetAutoReload>:
 8002b08:	b480      	push	{r7}
 8002b0a:	b083      	sub	sp, #12
 8002b0c:	af00      	add	r7, sp, #0
 8002b0e:	6078      	str	r0, [r7, #4]
 8002b10:	6039      	str	r1, [r7, #0]
 8002b12:	687b      	ldr	r3, [r7, #4]
 8002b14:	683a      	ldr	r2, [r7, #0]
 8002b16:	62da      	str	r2, [r3, #44]	; 0x2c
 8002b18:	bf00      	nop
 8002b1a:	370c      	adds	r7, #12
 8002b1c:	46bd      	mov	sp, r7
 8002b1e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002b22:	4770      	bx	lr

08002b24 <LL_TIM_CC_EnableChannel>:
 8002b24:	b480      	push	{r7}
 8002b26:	b083      	sub	sp, #12
 8002b28:	af00      	add	r7, sp, #0
 8002b2a:	6078      	str	r0, [r7, #4]
 8002b2c:	6039      	str	r1, [r7, #0]
 8002b2e:	687b      	ldr	r3, [r7, #4]
 8002b30:	6a1a      	ldr	r2, [r3, #32]
 8002b32:	683b      	ldr	r3, [r7, #0]
 8002b34:	431a      	orrs	r2, r3
 8002b36:	687b      	ldr	r3, [r7, #4]
 8002b38:	621a      	str	r2, [r3, #32]
 8002b3a:	bf00      	nop
 8002b3c:	370c      	adds	r7, #12
 8002b3e:	46bd      	mov	sp, r7
 8002b40:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002b44:	4770      	bx	lr
	...

08002b48 <LL_TIM_OC_SetMode>:
 8002b48:	b4b0      	push	{r4, r5, r7}
 8002b4a:	b085      	sub	sp, #20
 8002b4c:	af00      	add	r7, sp, #0
 8002b4e:	60f8      	str	r0, [r7, #12]
 8002b50:	60b9      	str	r1, [r7, #8]
 8002b52:	607a      	str	r2, [r7, #4]
 8002b54:	68bb      	ldr	r3, [r7, #8]
 8002b56:	2b01      	cmp	r3, #1
 8002b58:	d01c      	beq.n	8002b94 <LL_TIM_OC_SetMode+0x4c>
 8002b5a:	68bb      	ldr	r3, [r7, #8]
 8002b5c:	2b04      	cmp	r3, #4
 8002b5e:	d017      	beq.n	8002b90 <LL_TIM_OC_SetMode+0x48>
 8002b60:	68bb      	ldr	r3, [r7, #8]
 8002b62:	2b10      	cmp	r3, #16
 8002b64:	d012      	beq.n	8002b8c <LL_TIM_OC_SetMode+0x44>
 8002b66:	68bb      	ldr	r3, [r7, #8]
 8002b68:	2b40      	cmp	r3, #64	; 0x40
 8002b6a:	d00d      	beq.n	8002b88 <LL_TIM_OC_SetMode+0x40>
 8002b6c:	68bb      	ldr	r3, [r7, #8]
 8002b6e:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8002b72:	d007      	beq.n	8002b84 <LL_TIM_OC_SetMode+0x3c>
 8002b74:	68bb      	ldr	r3, [r7, #8]
 8002b76:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8002b7a:	d101      	bne.n	8002b80 <LL_TIM_OC_SetMode+0x38>
 8002b7c:	2305      	movs	r3, #5
 8002b7e:	e00a      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b80:	2306      	movs	r3, #6
 8002b82:	e008      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b84:	2304      	movs	r3, #4
 8002b86:	e006      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b88:	2303      	movs	r3, #3
 8002b8a:	e004      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b8c:	2302      	movs	r3, #2
 8002b8e:	e002      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b90:	2301      	movs	r3, #1
 8002b92:	e000      	b.n	8002b96 <LL_TIM_OC_SetMode+0x4e>
 8002b94:	2300      	movs	r3, #0
 8002b96:	461d      	mov	r5, r3
 8002b98:	68fb      	ldr	r3, [r7, #12]
 8002b9a:	3318      	adds	r3, #24
 8002b9c:	461a      	mov	r2, r3
 8002b9e:	4629      	mov	r1, r5
 8002ba0:	4b0c      	ldr	r3, [pc, #48]	; (8002bd4 <LL_TIM_OC_SetMode+0x8c>)
 8002ba2:	5c5b      	ldrb	r3, [r3, r1]
 8002ba4:	4413      	add	r3, r2
 8002ba6:	461c      	mov	r4, r3
 8002ba8:	6822      	ldr	r2, [r4, #0]
 8002baa:	4629      	mov	r1, r5
 8002bac:	4b0a      	ldr	r3, [pc, #40]	; (8002bd8 <LL_TIM_OC_SetMode+0x90>)
 8002bae:	5c5b      	ldrb	r3, [r3, r1]
 8002bb0:	4619      	mov	r1, r3
 8002bb2:	2373      	movs	r3, #115	; 0x73
 8002bb4:	408b      	lsls	r3, r1
 8002bb6:	43db      	mvns	r3, r3
 8002bb8:	401a      	ands	r2, r3
 8002bba:	4629      	mov	r1, r5
 8002bbc:	4b06      	ldr	r3, [pc, #24]	; (8002bd8 <LL_TIM_OC_SetMode+0x90>)
 8002bbe:	5c5b      	ldrb	r3, [r3, r1]
 8002bc0:	4619      	mov	r1, r3
 8002bc2:	687b      	ldr	r3, [r7, #4]
 8002bc4:	408b      	lsls	r3, r1
 8002bc6:	4313      	orrs	r3, r2
 8002bc8:	6023      	str	r3, [r4, #0]
 8002bca:	bf00      	nop
 8002bcc:	3714      	adds	r7, #20
 8002bce:	46bd      	mov	sp, r7
 8002bd0:	bcb0      	pop	{r4, r5, r7}
 8002bd2:	4770      	bx	lr
 8002bd4:	0800d128 	.word	0x0800d128
 8002bd8:	0800d130 	.word	0x0800d130

08002bdc <LL_TIM_OC_EnableFast>:
 8002bdc:	b4b0      	push	{r4, r5, r7}
 8002bde:	b083      	sub	sp, #12
 8002be0:	af00      	add	r7, sp, #0
 8002be2:	6078      	str	r0, [r7, #4]
 8002be4:	6039      	str	r1, [r7, #0]
 8002be6:	683b      	ldr	r3, [r7, #0]
 8002be8:	2b01      	cmp	r3, #1
 8002bea:	d01c      	beq.n	8002c26 <LL_TIM_OC_EnableFast+0x4a>
 8002bec:	683b      	ldr	r3, [r7, #0]
 8002bee:	2b04      	cmp	r3, #4
 8002bf0:	d017      	beq.n	8002c22 <LL_TIM_OC_EnableFast+0x46>
 8002bf2:	683b      	ldr	r3, [r7, #0]
 8002bf4:	2b10      	cmp	r3, #16
 8002bf6:	d012      	beq.n	8002c1e <LL_TIM_OC_EnableFast+0x42>
 8002bf8:	683b      	ldr	r3, [r7, #0]
 8002bfa:	2b40      	cmp	r3, #64	; 0x40
 8002bfc:	d00d      	beq.n	8002c1a <LL_TIM_OC_EnableFast+0x3e>
 8002bfe:	683b      	ldr	r3, [r7, #0]
 8002c00:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8002c04:	d007      	beq.n	8002c16 <LL_TIM_OC_EnableFast+0x3a>
 8002c06:	683b      	ldr	r3, [r7, #0]
 8002c08:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8002c0c:	d101      	bne.n	8002c12 <LL_TIM_OC_EnableFast+0x36>
 8002c0e:	2305      	movs	r3, #5
 8002c10:	e00a      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c12:	2306      	movs	r3, #6
 8002c14:	e008      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c16:	2304      	movs	r3, #4
 8002c18:	e006      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c1a:	2303      	movs	r3, #3
 8002c1c:	e004      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c1e:	2302      	movs	r3, #2
 8002c20:	e002      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c22:	2301      	movs	r3, #1
 8002c24:	e000      	b.n	8002c28 <LL_TIM_OC_EnableFast+0x4c>
 8002c26:	2300      	movs	r3, #0
 8002c28:	461d      	mov	r5, r3
 8002c2a:	687b      	ldr	r3, [r7, #4]
 8002c2c:	3318      	adds	r3, #24
 8002c2e:	461a      	mov	r2, r3
 8002c30:	4629      	mov	r1, r5
 8002c32:	4b09      	ldr	r3, [pc, #36]	; (8002c58 <LL_TIM_OC_EnableFast+0x7c>)
 8002c34:	5c5b      	ldrb	r3, [r3, r1]
 8002c36:	4413      	add	r3, r2
 8002c38:	461c      	mov	r4, r3
 8002c3a:	6822      	ldr	r2, [r4, #0]
 8002c3c:	4629      	mov	r1, r5
 8002c3e:	4b07      	ldr	r3, [pc, #28]	; (8002c5c <LL_TIM_OC_EnableFast+0x80>)
 8002c40:	5c5b      	ldrb	r3, [r3, r1]
 8002c42:	4619      	mov	r1, r3
 8002c44:	2304      	movs	r3, #4
 8002c46:	408b      	lsls	r3, r1
 8002c48:	4313      	orrs	r3, r2
 8002c4a:	6023      	str	r3, [r4, #0]
 8002c4c:	bf00      	nop
 8002c4e:	370c      	adds	r7, #12
 8002c50:	46bd      	mov	sp, r7
 8002c52:	bcb0      	pop	{r4, r5, r7}
 8002c54:	4770      	bx	lr
 8002c56:	bf00      	nop
 8002c58:	0800d128 	.word	0x0800d128
 8002c5c:	0800d130 	.word	0x0800d130

08002c60 <LL_TIM_OC_EnablePreload>:
 8002c60:	b4b0      	push	{r4, r5, r7}
 8002c62:	b083      	sub	sp, #12
 8002c64:	af00      	add	r7, sp, #0
 8002c66:	6078      	str	r0, [r7, #4]
 8002c68:	6039      	str	r1, [r7, #0]
 8002c6a:	683b      	ldr	r3, [r7, #0]
 8002c6c:	2b01      	cmp	r3, #1
 8002c6e:	d01c      	beq.n	8002caa <LL_TIM_OC_EnablePreload+0x4a>
 8002c70:	683b      	ldr	r3, [r7, #0]
 8002c72:	2b04      	cmp	r3, #4
 8002c74:	d017      	beq.n	8002ca6 <LL_TIM_OC_EnablePreload+0x46>
 8002c76:	683b      	ldr	r3, [r7, #0]
 8002c78:	2b10      	cmp	r3, #16
 8002c7a:	d012      	beq.n	8002ca2 <LL_TIM_OC_EnablePreload+0x42>
 8002c7c:	683b      	ldr	r3, [r7, #0]
 8002c7e:	2b40      	cmp	r3, #64	; 0x40
 8002c80:	d00d      	beq.n	8002c9e <LL_TIM_OC_EnablePreload+0x3e>
 8002c82:	683b      	ldr	r3, [r7, #0]
 8002c84:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8002c88:	d007      	beq.n	8002c9a <LL_TIM_OC_EnablePreload+0x3a>
 8002c8a:	683b      	ldr	r3, [r7, #0]
 8002c8c:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8002c90:	d101      	bne.n	8002c96 <LL_TIM_OC_EnablePreload+0x36>
 8002c92:	2305      	movs	r3, #5
 8002c94:	e00a      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002c96:	2306      	movs	r3, #6
 8002c98:	e008      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002c9a:	2304      	movs	r3, #4
 8002c9c:	e006      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002c9e:	2303      	movs	r3, #3
 8002ca0:	e004      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002ca2:	2302      	movs	r3, #2
 8002ca4:	e002      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002ca6:	2301      	movs	r3, #1
 8002ca8:	e000      	b.n	8002cac <LL_TIM_OC_EnablePreload+0x4c>
 8002caa:	2300      	movs	r3, #0
 8002cac:	461d      	mov	r5, r3
 8002cae:	687b      	ldr	r3, [r7, #4]
 8002cb0:	3318      	adds	r3, #24
 8002cb2:	461a      	mov	r2, r3
 8002cb4:	4629      	mov	r1, r5
 8002cb6:	4b09      	ldr	r3, [pc, #36]	; (8002cdc <LL_TIM_OC_EnablePreload+0x7c>)
 8002cb8:	5c5b      	ldrb	r3, [r3, r1]
 8002cba:	4413      	add	r3, r2
 8002cbc:	461c      	mov	r4, r3
 8002cbe:	6822      	ldr	r2, [r4, #0]
 8002cc0:	4629      	mov	r1, r5
 8002cc2:	4b07      	ldr	r3, [pc, #28]	; (8002ce0 <LL_TIM_OC_EnablePreload+0x80>)
 8002cc4:	5c5b      	ldrb	r3, [r3, r1]
 8002cc6:	4619      	mov	r1, r3
 8002cc8:	2308      	movs	r3, #8
 8002cca:	408b      	lsls	r3, r1
 8002ccc:	4313      	orrs	r3, r2
 8002cce:	6023      	str	r3, [r4, #0]
 8002cd0:	bf00      	nop
 8002cd2:	370c      	adds	r7, #12
 8002cd4:	46bd      	mov	sp, r7
 8002cd6:	bcb0      	pop	{r4, r5, r7}
 8002cd8:	4770      	bx	lr
 8002cda:	bf00      	nop
 8002cdc:	0800d128 	.word	0x0800d128
 8002ce0:	0800d130 	.word	0x0800d130

08002ce4 <LL_TIM_OC_SetCompareCH1>:
 8002ce4:	b480      	push	{r7}
 8002ce6:	b083      	sub	sp, #12
 8002ce8:	af00      	add	r7, sp, #0
 8002cea:	6078      	str	r0, [r7, #4]
 8002cec:	6039      	str	r1, [r7, #0]
 8002cee:	687b      	ldr	r3, [r7, #4]
 8002cf0:	683a      	ldr	r2, [r7, #0]
 8002cf2:	635a      	str	r2, [r3, #52]	; 0x34
 8002cf4:	bf00      	nop
 8002cf6:	370c      	adds	r7, #12
 8002cf8:	46bd      	mov	sp, r7
 8002cfa:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002cfe:	4770      	bx	lr

08002d00 <LL_TIM_OC_SetCompareCH2>:
 8002d00:	b480      	push	{r7}
 8002d02:	b083      	sub	sp, #12
 8002d04:	af00      	add	r7, sp, #0
 8002d06:	6078      	str	r0, [r7, #4]
 8002d08:	6039      	str	r1, [r7, #0]
 8002d0a:	687b      	ldr	r3, [r7, #4]
 8002d0c:	683a      	ldr	r2, [r7, #0]
 8002d0e:	639a      	str	r2, [r3, #56]	; 0x38
 8002d10:	bf00      	nop
 8002d12:	370c      	adds	r7, #12
 8002d14:	46bd      	mov	sp, r7
 8002d16:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d1a:	4770      	bx	lr

08002d1c <LL_TIM_OC_SetCompareCH3>:
 8002d1c:	b480      	push	{r7}
 8002d1e:	b083      	sub	sp, #12
 8002d20:	af00      	add	r7, sp, #0
 8002d22:	6078      	str	r0, [r7, #4]
 8002d24:	6039      	str	r1, [r7, #0]
 8002d26:	687b      	ldr	r3, [r7, #4]
 8002d28:	683a      	ldr	r2, [r7, #0]
 8002d2a:	63da      	str	r2, [r3, #60]	; 0x3c
 8002d2c:	bf00      	nop
 8002d2e:	370c      	adds	r7, #12
 8002d30:	46bd      	mov	sp, r7
 8002d32:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d36:	4770      	bx	lr

08002d38 <LL_TIM_OC_SetCompareCH4>:
 8002d38:	b480      	push	{r7}
 8002d3a:	b083      	sub	sp, #12
 8002d3c:	af00      	add	r7, sp, #0
 8002d3e:	6078      	str	r0, [r7, #4]
 8002d40:	6039      	str	r1, [r7, #0]
 8002d42:	687b      	ldr	r3, [r7, #4]
 8002d44:	683a      	ldr	r2, [r7, #0]
 8002d46:	641a      	str	r2, [r3, #64]	; 0x40
 8002d48:	bf00      	nop
 8002d4a:	370c      	adds	r7, #12
 8002d4c:	46bd      	mov	sp, r7
 8002d4e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d52:	4770      	bx	lr

08002d54 <LL_TIM_ClearFlag_UPDATE>:
 8002d54:	b480      	push	{r7}
 8002d56:	b083      	sub	sp, #12
 8002d58:	af00      	add	r7, sp, #0
 8002d5a:	6078      	str	r0, [r7, #4]
 8002d5c:	687b      	ldr	r3, [r7, #4]
 8002d5e:	f06f 0201 	mvn.w	r2, #1
 8002d62:	611a      	str	r2, [r3, #16]
 8002d64:	bf00      	nop
 8002d66:	370c      	adds	r7, #12
 8002d68:	46bd      	mov	sp, r7
 8002d6a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d6e:	4770      	bx	lr

08002d70 <LL_TIM_IsActiveFlag_UPDATE>:
 8002d70:	b480      	push	{r7}
 8002d72:	b083      	sub	sp, #12
 8002d74:	af00      	add	r7, sp, #0
 8002d76:	6078      	str	r0, [r7, #4]
 8002d78:	687b      	ldr	r3, [r7, #4]
 8002d7a:	691b      	ldr	r3, [r3, #16]
 8002d7c:	f003 0301 	and.w	r3, r3, #1
 8002d80:	2b01      	cmp	r3, #1
 8002d82:	bf0c      	ite	eq
 8002d84:	2301      	moveq	r3, #1
 8002d86:	2300      	movne	r3, #0
 8002d88:	b2db      	uxtb	r3, r3
 8002d8a:	4618      	mov	r0, r3
 8002d8c:	370c      	adds	r7, #12
 8002d8e:	46bd      	mov	sp, r7
 8002d90:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d94:	4770      	bx	lr

08002d96 <LL_TIM_EnableIT_UPDATE>:
 8002d96:	b480      	push	{r7}
 8002d98:	b083      	sub	sp, #12
 8002d9a:	af00      	add	r7, sp, #0
 8002d9c:	6078      	str	r0, [r7, #4]
 8002d9e:	687b      	ldr	r3, [r7, #4]
 8002da0:	68db      	ldr	r3, [r3, #12]
 8002da2:	f043 0201 	orr.w	r2, r3, #1
 8002da6:	687b      	ldr	r3, [r7, #4]
 8002da8:	60da      	str	r2, [r3, #12]
 8002daa:	bf00      	nop
 8002dac:	370c      	adds	r7, #12
 8002dae:	46bd      	mov	sp, r7
 8002db0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002db4:	4770      	bx	lr

08002db6 <LL_TIM_GenerateEvent_UPDATE>:
 8002db6:	b480      	push	{r7}
 8002db8:	b083      	sub	sp, #12
 8002dba:	af00      	add	r7, sp, #0
 8002dbc:	6078      	str	r0, [r7, #4]
 8002dbe:	687b      	ldr	r3, [r7, #4]
 8002dc0:	695b      	ldr	r3, [r3, #20]
 8002dc2:	f043 0201 	orr.w	r2, r3, #1
 8002dc6:	687b      	ldr	r3, [r7, #4]
 8002dc8:	615a      	str	r2, [r3, #20]
 8002dca:	bf00      	nop
 8002dcc:	370c      	adds	r7, #12
 8002dce:	46bd      	mov	sp, r7
 8002dd0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002dd4:	4770      	bx	lr
	...

08002dd8 <LL_SYSCFG_SetEXTISource>:
 8002dd8:	b480      	push	{r7}
 8002dda:	b085      	sub	sp, #20
 8002ddc:	af00      	add	r7, sp, #0
 8002dde:	6078      	str	r0, [r7, #4]
 8002de0:	6039      	str	r1, [r7, #0]
 8002de2:	4a12      	ldr	r2, [pc, #72]	; (8002e2c <LL_SYSCFG_SetEXTISource+0x54>)
 8002de4:	683b      	ldr	r3, [r7, #0]
 8002de6:	b2db      	uxtb	r3, r3
 8002de8:	3302      	adds	r3, #2
 8002dea:	f852 2023 	ldr.w	r2, [r2, r3, lsl #2]
 8002dee:	683b      	ldr	r3, [r7, #0]
 8002df0:	0c1b      	lsrs	r3, r3, #16
 8002df2:	43db      	mvns	r3, r3
 8002df4:	ea02 0103 	and.w	r1, r2, r3
 8002df8:	683b      	ldr	r3, [r7, #0]
 8002dfa:	0c1b      	lsrs	r3, r3, #16
 8002dfc:	60fb      	str	r3, [r7, #12]
 8002dfe:	68fb      	ldr	r3, [r7, #12]
 8002e00:	fa93 f3a3 	rbit	r3, r3
 8002e04:	60bb      	str	r3, [r7, #8]
 8002e06:	68bb      	ldr	r3, [r7, #8]
 8002e08:	fab3 f383 	clz	r3, r3
 8002e0c:	687a      	ldr	r2, [r7, #4]
 8002e0e:	409a      	lsls	r2, r3
 8002e10:	4806      	ldr	r0, [pc, #24]	; (8002e2c <LL_SYSCFG_SetEXTISource+0x54>)
 8002e12:	683b      	ldr	r3, [r7, #0]
 8002e14:	b2db      	uxtb	r3, r3
 8002e16:	430a      	orrs	r2, r1
 8002e18:	3302      	adds	r3, #2
 8002e1a:	f840 2023 	str.w	r2, [r0, r3, lsl #2]
 8002e1e:	bf00      	nop
 8002e20:	3714      	adds	r7, #20
 8002e22:	46bd      	mov	sp, r7
 8002e24:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e28:	4770      	bx	lr
 8002e2a:	bf00      	nop
 8002e2c:	40013800 	.word	0x40013800

08002e30 <LL_EXTI_EnableIT_0_31>:
 8002e30:	b480      	push	{r7}
 8002e32:	b083      	sub	sp, #12
 8002e34:	af00      	add	r7, sp, #0
 8002e36:	6078      	str	r0, [r7, #4]
 8002e38:	4b05      	ldr	r3, [pc, #20]	; (8002e50 <LL_EXTI_EnableIT_0_31+0x20>)
 8002e3a:	681a      	ldr	r2, [r3, #0]
 8002e3c:	4904      	ldr	r1, [pc, #16]	; (8002e50 <LL_EXTI_EnableIT_0_31+0x20>)
 8002e3e:	687b      	ldr	r3, [r7, #4]
 8002e40:	4313      	orrs	r3, r2
 8002e42:	600b      	str	r3, [r1, #0]
 8002e44:	bf00      	nop
 8002e46:	370c      	adds	r7, #12
 8002e48:	46bd      	mov	sp, r7
 8002e4a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e4e:	4770      	bx	lr
 8002e50:	40013c00 	.word	0x40013c00

08002e54 <LL_EXTI_EnableRisingTrig_0_31>:
 8002e54:	b480      	push	{r7}
 8002e56:	b083      	sub	sp, #12
 8002e58:	af00      	add	r7, sp, #0
 8002e5a:	6078      	str	r0, [r7, #4]
 8002e5c:	4b05      	ldr	r3, [pc, #20]	; (8002e74 <LL_EXTI_EnableRisingTrig_0_31+0x20>)
 8002e5e:	689a      	ldr	r2, [r3, #8]
 8002e60:	4904      	ldr	r1, [pc, #16]	; (8002e74 <LL_EXTI_EnableRisingTrig_0_31+0x20>)
 8002e62:	687b      	ldr	r3, [r7, #4]
 8002e64:	4313      	orrs	r3, r2
 8002e66:	608b      	str	r3, [r1, #8]
 8002e68:	bf00      	nop
 8002e6a:	370c      	adds	r7, #12
 8002e6c:	46bd      	mov	sp, r7
 8002e6e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e72:	4770      	bx	lr
 8002e74:	40013c00 	.word	0x40013c00

08002e78 <LL_EXTI_EnableFallingTrig_0_31>:
 8002e78:	b480      	push	{r7}
 8002e7a:	b083      	sub	sp, #12
 8002e7c:	af00      	add	r7, sp, #0
 8002e7e:	6078      	str	r0, [r7, #4]
 8002e80:	4b05      	ldr	r3, [pc, #20]	; (8002e98 <LL_EXTI_EnableFallingTrig_0_31+0x20>)
 8002e82:	68da      	ldr	r2, [r3, #12]
 8002e84:	4904      	ldr	r1, [pc, #16]	; (8002e98 <LL_EXTI_EnableFallingTrig_0_31+0x20>)
 8002e86:	687b      	ldr	r3, [r7, #4]
 8002e88:	4313      	orrs	r3, r2
 8002e8a:	60cb      	str	r3, [r1, #12]
 8002e8c:	bf00      	nop
 8002e8e:	370c      	adds	r7, #12
 8002e90:	46bd      	mov	sp, r7
 8002e92:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e96:	4770      	bx	lr
 8002e98:	40013c00 	.word	0x40013c00

08002e9c <LL_EXTI_ClearFlag_0_31>:
 8002e9c:	b480      	push	{r7}
 8002e9e:	b083      	sub	sp, #12
 8002ea0:	af00      	add	r7, sp, #0
 8002ea2:	6078      	str	r0, [r7, #4]
 8002ea4:	4a04      	ldr	r2, [pc, #16]	; (8002eb8 <LL_EXTI_ClearFlag_0_31+0x1c>)
 8002ea6:	687b      	ldr	r3, [r7, #4]
 8002ea8:	6153      	str	r3, [r2, #20]
 8002eaa:	bf00      	nop
 8002eac:	370c      	adds	r7, #12
 8002eae:	46bd      	mov	sp, r7
 8002eb0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002eb4:	4770      	bx	lr
 8002eb6:	bf00      	nop
 8002eb8:	40013c00 	.word	0x40013c00

08002ebc <mk_speed2pwm>:
 8002ebc:	b580      	push	{r7, lr}
 8002ebe:	b086      	sub	sp, #24
 8002ec0:	af00      	add	r7, sp, #0
 8002ec2:	6078      	str	r0, [r7, #4]
 8002ec4:	687b      	ldr	r3, [r7, #4]
 8002ec6:	68db      	ldr	r3, [r3, #12]
 8002ec8:	4a5a      	ldr	r2, [pc, #360]	; (8003034 <mk_speed2pwm+0x178>)
 8002eca:	6013      	str	r3, [r2, #0]
 8002ecc:	687b      	ldr	r3, [r7, #4]
 8002ece:	691b      	ldr	r3, [r3, #16]
 8002ed0:	4a58      	ldr	r2, [pc, #352]	; (8003034 <mk_speed2pwm+0x178>)
 8002ed2:	6053      	str	r3, [r2, #4]
 8002ed4:	687b      	ldr	r3, [r7, #4]
 8002ed6:	695b      	ldr	r3, [r3, #20]
 8002ed8:	4a56      	ldr	r2, [pc, #344]	; (8003034 <mk_speed2pwm+0x178>)
 8002eda:	6093      	str	r3, [r2, #8]
 8002edc:	4a56      	ldr	r2, [pc, #344]	; (8003038 <mk_speed2pwm+0x17c>)
 8002ede:	f107 0308 	add.w	r3, r7, #8
 8002ee2:	ca07      	ldmia	r2, {r0, r1, r2}
 8002ee4:	e883 0007 	stmia.w	r3, {r0, r1, r2}
 8002ee8:	2300      	movs	r3, #0
 8002eea:	617b      	str	r3, [r7, #20]
 8002eec:	4b53      	ldr	r3, [pc, #332]	; (800303c <mk_speed2pwm+0x180>)
 8002eee:	2203      	movs	r2, #3
 8002ef0:	2103      	movs	r1, #3
 8002ef2:	4853      	ldr	r0, [pc, #332]	; (8003040 <mk_speed2pwm+0x184>)
 8002ef4:	f007 f9a6 	bl	800a244 <arm_mat_init_f32>
 8002ef8:	4b52      	ldr	r3, [pc, #328]	; (8003044 <mk_speed2pwm+0x188>)
 8002efa:	2203      	movs	r2, #3
 8002efc:	2103      	movs	r1, #3
 8002efe:	4852      	ldr	r0, [pc, #328]	; (8003048 <mk_speed2pwm+0x18c>)
 8002f00:	f007 f9a0 	bl	800a244 <arm_mat_init_f32>
 8002f04:	4b51      	ldr	r3, [pc, #324]	; (800304c <mk_speed2pwm+0x190>)
 8002f06:	2201      	movs	r2, #1
 8002f08:	2103      	movs	r1, #3
 8002f0a:	4851      	ldr	r0, [pc, #324]	; (8003050 <mk_speed2pwm+0x194>)
 8002f0c:	f007 f99a 	bl	800a244 <arm_mat_init_f32>
 8002f10:	4b50      	ldr	r3, [pc, #320]	; (8003054 <mk_speed2pwm+0x198>)
 8002f12:	2201      	movs	r2, #1
 8002f14:	2103      	movs	r1, #3
 8002f16:	4850      	ldr	r0, [pc, #320]	; (8003058 <mk_speed2pwm+0x19c>)
 8002f18:	f007 f994 	bl	800a244 <arm_mat_init_f32>
 8002f1c:	4b4f      	ldr	r3, [pc, #316]	; (800305c <mk_speed2pwm+0x1a0>)
 8002f1e:	2201      	movs	r2, #1
 8002f20:	2103      	movs	r1, #3
 8002f22:	484f      	ldr	r0, [pc, #316]	; (8003060 <mk_speed2pwm+0x1a4>)
 8002f24:	f007 f98e 	bl	800a244 <arm_mat_init_f32>
 8002f28:	4b42      	ldr	r3, [pc, #264]	; (8003034 <mk_speed2pwm+0x178>)
 8002f2a:	2201      	movs	r2, #1
 8002f2c:	2103      	movs	r1, #3
 8002f2e:	484d      	ldr	r0, [pc, #308]	; (8003064 <mk_speed2pwm+0x1a8>)
 8002f30:	f007 f988 	bl	800a244 <arm_mat_init_f32>
 8002f34:	4a46      	ldr	r2, [pc, #280]	; (8003050 <mk_speed2pwm+0x194>)
 8002f36:	494b      	ldr	r1, [pc, #300]	; (8003064 <mk_speed2pwm+0x1a8>)
 8002f38:	4841      	ldr	r0, [pc, #260]	; (8003040 <mk_speed2pwm+0x184>)
 8002f3a:	f007 f99b 	bl	800a274 <arm_mat_mult_f32>
 8002f3e:	4a46      	ldr	r2, [pc, #280]	; (8003058 <mk_speed2pwm+0x19c>)
 8002f40:	4948      	ldr	r1, [pc, #288]	; (8003064 <mk_speed2pwm+0x1a8>)
 8002f42:	4841      	ldr	r0, [pc, #260]	; (8003048 <mk_speed2pwm+0x18c>)
 8002f44:	f007 f996 	bl	800a274 <arm_mat_mult_f32>
 8002f48:	4a45      	ldr	r2, [pc, #276]	; (8003060 <mk_speed2pwm+0x1a4>)
 8002f4a:	4943      	ldr	r1, [pc, #268]	; (8003058 <mk_speed2pwm+0x19c>)
 8002f4c:	4840      	ldr	r0, [pc, #256]	; (8003050 <mk_speed2pwm+0x194>)
 8002f4e:	f007 f8e2 	bl	800a116 <arm_mat_add_f32>
 8002f52:	2300      	movs	r3, #0
 8002f54:	4a44      	ldr	r2, [pc, #272]	; (8003068 <mk_speed2pwm+0x1ac>)
 8002f56:	2103      	movs	r1, #3
 8002f58:	4840      	ldr	r0, [pc, #256]	; (800305c <mk_speed2pwm+0x1a0>)
 8002f5a:	f007 faf6 	bl	800a54a <arm_max_f32>
 8002f5e:	2300      	movs	r3, #0
 8002f60:	4a42      	ldr	r2, [pc, #264]	; (800306c <mk_speed2pwm+0x1b0>)
 8002f62:	2103      	movs	r1, #3
 8002f64:	483d      	ldr	r0, [pc, #244]	; (800305c <mk_speed2pwm+0x1a0>)
 8002f66:	f007 fb89 	bl	800a67c <arm_min_f32>
 8002f6a:	4b40      	ldr	r3, [pc, #256]	; (800306c <mk_speed2pwm+0x1b0>)
 8002f6c:	edd3 7a00 	vldr	s15, [r3]
 8002f70:	eeb0 7ae7 	vabs.f32	s14, s15
 8002f74:	4b3c      	ldr	r3, [pc, #240]	; (8003068 <mk_speed2pwm+0x1ac>)
 8002f76:	edd3 7a00 	vldr	s15, [r3]
 8002f7a:	eeb4 7ae7 	vcmpe.f32	s14, s15
 8002f7e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8002f82:	dd07      	ble.n	8002f94 <mk_speed2pwm+0xd8>
 8002f84:	4b39      	ldr	r3, [pc, #228]	; (800306c <mk_speed2pwm+0x1b0>)
 8002f86:	edd3 7a00 	vldr	s15, [r3]
 8002f8a:	eef1 7a67 	vneg.f32	s15, s15
 8002f8e:	4b36      	ldr	r3, [pc, #216]	; (8003068 <mk_speed2pwm+0x1ac>)
 8002f90:	edc3 7a00 	vstr	s15, [r3]
 8002f94:	4b34      	ldr	r3, [pc, #208]	; (8003068 <mk_speed2pwm+0x1ac>)
 8002f96:	edd3 7a00 	vldr	s15, [r3]
 8002f9a:	ed9f 7a35 	vldr	s14, [pc, #212]	; 8003070 <mk_speed2pwm+0x1b4>
 8002f9e:	eef4 7ac7 	vcmpe.f32	s15, s14
 8002fa2:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8002fa6:	dd0c      	ble.n	8002fc2 <mk_speed2pwm+0x106>
 8002fa8:	4b2f      	ldr	r3, [pc, #188]	; (8003068 <mk_speed2pwm+0x1ac>)
 8002faa:	edd3 7a00 	vldr	s15, [r3]
 8002fae:	ed9f 7a30 	vldr	s14, [pc, #192]	; 8003070 <mk_speed2pwm+0x1b4>
 8002fb2:	eec7 6a27 	vdiv.f32	s13, s14, s15
 8002fb6:	492a      	ldr	r1, [pc, #168]	; (8003060 <mk_speed2pwm+0x1a4>)
 8002fb8:	eeb0 0a66 	vmov.f32	s0, s13
 8002fbc:	4828      	ldr	r0, [pc, #160]	; (8003060 <mk_speed2pwm+0x1a4>)
 8002fbe:	f007 fa41 	bl	800a444 <arm_mat_scale_f32>
 8002fc2:	2303      	movs	r3, #3
 8002fc4:	4a25      	ldr	r2, [pc, #148]	; (800305c <mk_speed2pwm+0x1a0>)
 8002fc6:	492b      	ldr	r1, [pc, #172]	; (8003074 <mk_speed2pwm+0x1b8>)
 8002fc8:	4824      	ldr	r0, [pc, #144]	; (800305c <mk_speed2pwm+0x1a0>)
 8002fca:	f007 fbf0 	bl	800a7ae <arm_mult_f32>
 8002fce:	2300      	movs	r3, #0
 8002fd0:	617b      	str	r3, [r7, #20]
 8002fd2:	e01f      	b.n	8003014 <mk_speed2pwm+0x158>
 8002fd4:	4a21      	ldr	r2, [pc, #132]	; (800305c <mk_speed2pwm+0x1a0>)
 8002fd6:	697b      	ldr	r3, [r7, #20]
 8002fd8:	009b      	lsls	r3, r3, #2
 8002fda:	4413      	add	r3, r2
 8002fdc:	edd3 7a00 	vldr	s15, [r3]
 8002fe0:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 8002fe4:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8002fe8:	d511      	bpl.n	800300e <mk_speed2pwm+0x152>
 8002fea:	697b      	ldr	r3, [r7, #20]
 8002fec:	009b      	lsls	r3, r3, #2
 8002fee:	f107 0218 	add.w	r2, r7, #24
 8002ff2:	4413      	add	r3, r2
 8002ff4:	3b10      	subs	r3, #16
 8002ff6:	edd3 7a00 	vldr	s15, [r3]
 8002ffa:	eef1 7a67 	vneg.f32	s15, s15
 8002ffe:	697b      	ldr	r3, [r7, #20]
 8003000:	009b      	lsls	r3, r3, #2
 8003002:	f107 0218 	add.w	r2, r7, #24
 8003006:	4413      	add	r3, r2
 8003008:	3b10      	subs	r3, #16
 800300a:	edc3 7a00 	vstr	s15, [r3]
 800300e:	697b      	ldr	r3, [r7, #20]
 8003010:	3301      	adds	r3, #1
 8003012:	617b      	str	r3, [r7, #20]
 8003014:	697b      	ldr	r3, [r7, #20]
 8003016:	2b02      	cmp	r3, #2
 8003018:	dddc      	ble.n	8002fd4 <mk_speed2pwm+0x118>
 800301a:	687b      	ldr	r3, [r7, #4]
 800301c:	f103 0220 	add.w	r2, r3, #32
 8003020:	f107 0108 	add.w	r1, r7, #8
 8003024:	2303      	movs	r3, #3
 8003026:	480d      	ldr	r0, [pc, #52]	; (800305c <mk_speed2pwm+0x1a0>)
 8003028:	f006 fffa 	bl	800a020 <arm_add_f32>
 800302c:	bf00      	nop
 800302e:	3718      	adds	r7, #24
 8003030:	46bd      	mov	sp, r7
 8003032:	bd80      	pop	{r7, pc}
 8003034:	20000274 	.word	0x20000274
 8003038:	0800d0d4 	.word	0x0800d0d4
 800303c:	20000000 	.word	0x20000000
 8003040:	20000280 	.word	0x20000280
 8003044:	20000024 	.word	0x20000024
 8003048:	20000288 	.word	0x20000288
 800304c:	20000298 	.word	0x20000298
 8003050:	20000290 	.word	0x20000290
 8003054:	200002ac 	.word	0x200002ac
 8003058:	200002a4 	.word	0x200002a4
 800305c:	200002c0 	.word	0x200002c0
 8003060:	200002b8 	.word	0x200002b8
 8003064:	200002cc 	.word	0x200002cc
 8003068:	200002d4 	.word	0x200002d4
 800306c:	200002d8 	.word	0x200002d8
 8003070:	41b384ea 	.word	0x41b384ea
 8003074:	20000048 	.word	0x20000048

08003078 <mk_set_pwm>:
 8003078:	b580      	push	{r7, lr}
 800307a:	b086      	sub	sp, #24
 800307c:	af00      	add	r7, sp, #0
 800307e:	6078      	str	r0, [r7, #4]
 8003080:	687b      	ldr	r3, [r7, #4]
 8003082:	681b      	ldr	r3, [r3, #0]
 8003084:	60fb      	str	r3, [r7, #12]
 8003086:	687b      	ldr	r3, [r7, #4]
 8003088:	685b      	ldr	r3, [r3, #4]
 800308a:	613b      	str	r3, [r7, #16]
 800308c:	687b      	ldr	r3, [r7, #4]
 800308e:	689b      	ldr	r3, [r3, #8]
 8003090:	617b      	str	r3, [r7, #20]
 8003092:	687b      	ldr	r3, [r7, #4]
 8003094:	edd3 7a00 	vldr	s15, [r3]
 8003098:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800309c:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80030a0:	dd04      	ble.n	80030ac <mk_set_pwm+0x34>
 80030a2:	2110      	movs	r1, #16
 80030a4:	483f      	ldr	r0, [pc, #252]	; (80031a4 <mk_set_pwm+0x12c>)
 80030a6:	f7ff fc3f 	bl	8002928 <LL_GPIO_SetOutputPin>
 80030aa:	e003      	b.n	80030b4 <mk_set_pwm+0x3c>
 80030ac:	2110      	movs	r1, #16
 80030ae:	483d      	ldr	r0, [pc, #244]	; (80031a4 <mk_set_pwm+0x12c>)
 80030b0:	f7ff fc48 	bl	8002944 <LL_GPIO_ResetOutputPin>
 80030b4:	687b      	ldr	r3, [r7, #4]
 80030b6:	edd3 7a00 	vldr	s15, [r3]
 80030ba:	eef0 7ae7 	vabs.f32	s15, s15
 80030be:	ed9f 7a3a 	vldr	s14, [pc, #232]	; 80031a8 <mk_set_pwm+0x130>
 80030c2:	ee67 7a87 	vmul.f32	s15, s15, s14
 80030c6:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 80030ca:	ee17 1a90 	vmov	r1, s15
 80030ce:	4837      	ldr	r0, [pc, #220]	; (80031ac <mk_set_pwm+0x134>)
 80030d0:	f7ff fe08 	bl	8002ce4 <LL_TIM_OC_SetCompareCH1>
 80030d4:	687b      	ldr	r3, [r7, #4]
 80030d6:	3304      	adds	r3, #4
 80030d8:	edd3 7a00 	vldr	s15, [r3]
 80030dc:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 80030e0:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80030e4:	dd09      	ble.n	80030fa <mk_set_pwm+0x82>
 80030e6:	2120      	movs	r1, #32
 80030e8:	482e      	ldr	r0, [pc, #184]	; (80031a4 <mk_set_pwm+0x12c>)
 80030ea:	f7ff fc1d 	bl	8002928 <LL_GPIO_SetOutputPin>
 80030ee:	f44f 6100 	mov.w	r1, #2048	; 0x800
 80030f2:	482c      	ldr	r0, [pc, #176]	; (80031a4 <mk_set_pwm+0x12c>)
 80030f4:	f7ff fc18 	bl	8002928 <LL_GPIO_SetOutputPin>
 80030f8:	e008      	b.n	800310c <mk_set_pwm+0x94>
 80030fa:	2120      	movs	r1, #32
 80030fc:	4829      	ldr	r0, [pc, #164]	; (80031a4 <mk_set_pwm+0x12c>)
 80030fe:	f7ff fc21 	bl	8002944 <LL_GPIO_ResetOutputPin>
 8003102:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8003106:	4827      	ldr	r0, [pc, #156]	; (80031a4 <mk_set_pwm+0x12c>)
 8003108:	f7ff fc1c 	bl	8002944 <LL_GPIO_ResetOutputPin>
 800310c:	687b      	ldr	r3, [r7, #4]
 800310e:	3304      	adds	r3, #4
 8003110:	edd3 7a00 	vldr	s15, [r3]
 8003114:	eef0 7ae7 	vabs.f32	s15, s15
 8003118:	ed9f 7a23 	vldr	s14, [pc, #140]	; 80031a8 <mk_set_pwm+0x130>
 800311c:	ee67 7a87 	vmul.f32	s15, s15, s14
 8003120:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003124:	ee17 1a90 	vmov	r1, s15
 8003128:	4820      	ldr	r0, [pc, #128]	; (80031ac <mk_set_pwm+0x134>)
 800312a:	f7ff fde9 	bl	8002d00 <LL_TIM_OC_SetCompareCH2>
 800312e:	687b      	ldr	r3, [r7, #4]
 8003130:	3304      	adds	r3, #4
 8003132:	edd3 7a00 	vldr	s15, [r3]
 8003136:	eef0 7ae7 	vabs.f32	s15, s15
 800313a:	ed9f 7a1b 	vldr	s14, [pc, #108]	; 80031a8 <mk_set_pwm+0x130>
 800313e:	ee67 7a87 	vmul.f32	s15, s15, s14
 8003142:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003146:	ee17 1a90 	vmov	r1, s15
 800314a:	4818      	ldr	r0, [pc, #96]	; (80031ac <mk_set_pwm+0x134>)
 800314c:	f7ff fdf4 	bl	8002d38 <LL_TIM_OC_SetCompareCH4>
 8003150:	687b      	ldr	r3, [r7, #4]
 8003152:	3308      	adds	r3, #8
 8003154:	edd3 7a00 	vldr	s15, [r3]
 8003158:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800315c:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003160:	dd05      	ble.n	800316e <mk_set_pwm+0xf6>
 8003162:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8003166:	480f      	ldr	r0, [pc, #60]	; (80031a4 <mk_set_pwm+0x12c>)
 8003168:	f7ff fbde 	bl	8002928 <LL_GPIO_SetOutputPin>
 800316c:	e004      	b.n	8003178 <mk_set_pwm+0x100>
 800316e:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8003172:	480c      	ldr	r0, [pc, #48]	; (80031a4 <mk_set_pwm+0x12c>)
 8003174:	f7ff fbe6 	bl	8002944 <LL_GPIO_ResetOutputPin>
 8003178:	687b      	ldr	r3, [r7, #4]
 800317a:	3308      	adds	r3, #8
 800317c:	edd3 7a00 	vldr	s15, [r3]
 8003180:	eef0 7ae7 	vabs.f32	s15, s15
 8003184:	ed9f 7a08 	vldr	s14, [pc, #32]	; 80031a8 <mk_set_pwm+0x130>
 8003188:	ee67 7a87 	vmul.f32	s15, s15, s14
 800318c:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003190:	ee17 1a90 	vmov	r1, s15
 8003194:	4805      	ldr	r0, [pc, #20]	; (80031ac <mk_set_pwm+0x134>)
 8003196:	f7ff fdc1 	bl	8002d1c <LL_TIM_OC_SetCompareCH3>
 800319a:	bf00      	nop
 800319c:	3718      	adds	r7, #24
 800319e:	46bd      	mov	sp, r7
 80031a0:	bd80      	pop	{r7, pc}
 80031a2:	bf00      	nop
 80031a4:	40020000 	.word	0x40020000
 80031a8:	47241000 	.word	0x47241000
 80031ac:	40000800 	.word	0x40000800

080031b0 <mk_hw_config>:
 80031b0:	b580      	push	{r7, lr}
 80031b2:	af00      	add	r7, sp, #0
 80031b4:	2008      	movs	r0, #8
 80031b6:	f7ff fbd5 	bl	8002964 <LL_AHB1_GRP1_EnableClock>
 80031ba:	2001      	movs	r0, #1
 80031bc:	f7ff fbd2 	bl	8002964 <LL_AHB1_GRP1_EnableClock>
 80031c0:	2010      	movs	r0, #16
 80031c2:	f7ff fbcf 	bl	8002964 <LL_AHB1_GRP1_EnableClock>
 80031c6:	2004      	movs	r0, #4
 80031c8:	f7ff fbe4 	bl	8002994 <LL_APB1_GRP1_EnableClock>
 80031cc:	2201      	movs	r2, #1
 80031ce:	2110      	movs	r1, #16
 80031d0:	48a8      	ldr	r0, [pc, #672]	; (8003474 <mk_hw_config+0x2c4>)
 80031d2:	f7ff faf3 	bl	80027bc <LL_GPIO_SetPinMode>
 80031d6:	2200      	movs	r2, #0
 80031d8:	2110      	movs	r1, #16
 80031da:	48a6      	ldr	r0, [pc, #664]	; (8003474 <mk_hw_config+0x2c4>)
 80031dc:	f7ff fb1b 	bl	8002816 <LL_GPIO_SetPinOutputType>
 80031e0:	2200      	movs	r2, #0
 80031e2:	2110      	movs	r1, #16
 80031e4:	48a3      	ldr	r0, [pc, #652]	; (8003474 <mk_hw_config+0x2c4>)
 80031e6:	f7ff fb2e 	bl	8002846 <LL_GPIO_SetPinPull>
 80031ea:	2201      	movs	r2, #1
 80031ec:	2120      	movs	r1, #32
 80031ee:	48a1      	ldr	r0, [pc, #644]	; (8003474 <mk_hw_config+0x2c4>)
 80031f0:	f7ff fae4 	bl	80027bc <LL_GPIO_SetPinMode>
 80031f4:	2200      	movs	r2, #0
 80031f6:	2120      	movs	r1, #32
 80031f8:	489e      	ldr	r0, [pc, #632]	; (8003474 <mk_hw_config+0x2c4>)
 80031fa:	f7ff fb0c 	bl	8002816 <LL_GPIO_SetPinOutputType>
 80031fe:	2200      	movs	r2, #0
 8003200:	2120      	movs	r1, #32
 8003202:	489c      	ldr	r0, [pc, #624]	; (8003474 <mk_hw_config+0x2c4>)
 8003204:	f7ff fb1f 	bl	8002846 <LL_GPIO_SetPinPull>
 8003208:	2201      	movs	r2, #1
 800320a:	f44f 6180 	mov.w	r1, #1024	; 0x400
 800320e:	4899      	ldr	r0, [pc, #612]	; (8003474 <mk_hw_config+0x2c4>)
 8003210:	f7ff fad4 	bl	80027bc <LL_GPIO_SetPinMode>
 8003214:	2200      	movs	r2, #0
 8003216:	f44f 6180 	mov.w	r1, #1024	; 0x400
 800321a:	4896      	ldr	r0, [pc, #600]	; (8003474 <mk_hw_config+0x2c4>)
 800321c:	f7ff fafb 	bl	8002816 <LL_GPIO_SetPinOutputType>
 8003220:	2200      	movs	r2, #0
 8003222:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8003226:	4893      	ldr	r0, [pc, #588]	; (8003474 <mk_hw_config+0x2c4>)
 8003228:	f7ff fb0d 	bl	8002846 <LL_GPIO_SetPinPull>
 800322c:	2201      	movs	r2, #1
 800322e:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8003232:	4890      	ldr	r0, [pc, #576]	; (8003474 <mk_hw_config+0x2c4>)
 8003234:	f7ff fac2 	bl	80027bc <LL_GPIO_SetPinMode>
 8003238:	2200      	movs	r2, #0
 800323a:	f44f 6100 	mov.w	r1, #2048	; 0x800
 800323e:	488d      	ldr	r0, [pc, #564]	; (8003474 <mk_hw_config+0x2c4>)
 8003240:	f7ff fae9 	bl	8002816 <LL_GPIO_SetPinOutputType>
 8003244:	2200      	movs	r2, #0
 8003246:	f44f 6100 	mov.w	r1, #2048	; 0x800
 800324a:	488a      	ldr	r0, [pc, #552]	; (8003474 <mk_hw_config+0x2c4>)
 800324c:	f7ff fafb 	bl	8002846 <LL_GPIO_SetPinPull>
 8003250:	2202      	movs	r2, #2
 8003252:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 8003256:	4888      	ldr	r0, [pc, #544]	; (8003478 <mk_hw_config+0x2c8>)
 8003258:	f7ff fab0 	bl	80027bc <LL_GPIO_SetPinMode>
 800325c:	2202      	movs	r2, #2
 800325e:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 8003262:	4885      	ldr	r0, [pc, #532]	; (8003478 <mk_hw_config+0x2c8>)
 8003264:	f7ff fb1c 	bl	80028a0 <LL_GPIO_SetAFPin_8_15>
 8003268:	2200      	movs	r2, #0
 800326a:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800326e:	4882      	ldr	r0, [pc, #520]	; (8003478 <mk_hw_config+0x2c8>)
 8003270:	f7ff fad1 	bl	8002816 <LL_GPIO_SetPinOutputType>
 8003274:	2202      	movs	r2, #2
 8003276:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 800327a:	487f      	ldr	r0, [pc, #508]	; (8003478 <mk_hw_config+0x2c8>)
 800327c:	f7ff fa9e 	bl	80027bc <LL_GPIO_SetPinMode>
 8003280:	2202      	movs	r2, #2
 8003282:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8003286:	487c      	ldr	r0, [pc, #496]	; (8003478 <mk_hw_config+0x2c8>)
 8003288:	f7ff fb0a 	bl	80028a0 <LL_GPIO_SetAFPin_8_15>
 800328c:	2200      	movs	r2, #0
 800328e:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8003292:	4879      	ldr	r0, [pc, #484]	; (8003478 <mk_hw_config+0x2c8>)
 8003294:	f7ff fabf 	bl	8002816 <LL_GPIO_SetPinOutputType>
 8003298:	2202      	movs	r2, #2
 800329a:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 800329e:	4876      	ldr	r0, [pc, #472]	; (8003478 <mk_hw_config+0x2c8>)
 80032a0:	f7ff fa8c 	bl	80027bc <LL_GPIO_SetPinMode>
 80032a4:	2202      	movs	r2, #2
 80032a6:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80032aa:	4873      	ldr	r0, [pc, #460]	; (8003478 <mk_hw_config+0x2c8>)
 80032ac:	f7ff faf8 	bl	80028a0 <LL_GPIO_SetAFPin_8_15>
 80032b0:	2200      	movs	r2, #0
 80032b2:	f44f 4180 	mov.w	r1, #16384	; 0x4000
 80032b6:	4870      	ldr	r0, [pc, #448]	; (8003478 <mk_hw_config+0x2c8>)
 80032b8:	f7ff faad 	bl	8002816 <LL_GPIO_SetPinOutputType>
 80032bc:	2202      	movs	r2, #2
 80032be:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80032c2:	486d      	ldr	r0, [pc, #436]	; (8003478 <mk_hw_config+0x2c8>)
 80032c4:	f7ff fa7a 	bl	80027bc <LL_GPIO_SetPinMode>
 80032c8:	2202      	movs	r2, #2
 80032ca:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80032ce:	486a      	ldr	r0, [pc, #424]	; (8003478 <mk_hw_config+0x2c8>)
 80032d0:	f7ff fae6 	bl	80028a0 <LL_GPIO_SetAFPin_8_15>
 80032d4:	2200      	movs	r2, #0
 80032d6:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80032da:	4867      	ldr	r0, [pc, #412]	; (8003478 <mk_hw_config+0x2c8>)
 80032dc:	f7ff fa9b 	bl	8002816 <LL_GPIO_SetPinOutputType>
 80032e0:	4866      	ldr	r0, [pc, #408]	; (800347c <mk_hw_config+0x2cc>)
 80032e2:	f7ff fbaa 	bl	8002a3a <LL_TIM_EnableUpdateEvent>
 80032e6:	f44f 7100 	mov.w	r1, #512	; 0x200
 80032ea:	4864      	ldr	r0, [pc, #400]	; (800347c <mk_hw_config+0x2cc>)
 80032ec:	f7ff fbeb 	bl	8002ac6 <LL_TIM_SetClockDivision>
 80032f0:	2100      	movs	r1, #0
 80032f2:	4862      	ldr	r0, [pc, #392]	; (800347c <mk_hw_config+0x2cc>)
 80032f4:	f7ff fbc4 	bl	8002a80 <LL_TIM_SetCounterMode>
 80032f8:	f24a 4110 	movw	r1, #42000	; 0xa410
 80032fc:	485f      	ldr	r0, [pc, #380]	; (800347c <mk_hw_config+0x2cc>)
 80032fe:	f7ff fc03 	bl	8002b08 <LL_TIM_SetAutoReload>
 8003302:	2100      	movs	r1, #0
 8003304:	485d      	ldr	r0, [pc, #372]	; (800347c <mk_hw_config+0x2cc>)
 8003306:	f7ff fba8 	bl	8002a5a <LL_TIM_SetUpdateSource>
 800330a:	f241 1111 	movw	r1, #4369	; 0x1111
 800330e:	485b      	ldr	r0, [pc, #364]	; (800347c <mk_hw_config+0x2cc>)
 8003310:	f7ff fc08 	bl	8002b24 <LL_TIM_CC_EnableChannel>
 8003314:	2260      	movs	r2, #96	; 0x60
 8003316:	2101      	movs	r1, #1
 8003318:	4858      	ldr	r0, [pc, #352]	; (800347c <mk_hw_config+0x2cc>)
 800331a:	f7ff fc15 	bl	8002b48 <LL_TIM_OC_SetMode>
 800331e:	2260      	movs	r2, #96	; 0x60
 8003320:	2110      	movs	r1, #16
 8003322:	4856      	ldr	r0, [pc, #344]	; (800347c <mk_hw_config+0x2cc>)
 8003324:	f7ff fc10 	bl	8002b48 <LL_TIM_OC_SetMode>
 8003328:	2260      	movs	r2, #96	; 0x60
 800332a:	f44f 7180 	mov.w	r1, #256	; 0x100
 800332e:	4853      	ldr	r0, [pc, #332]	; (800347c <mk_hw_config+0x2cc>)
 8003330:	f7ff fc0a 	bl	8002b48 <LL_TIM_OC_SetMode>
 8003334:	2260      	movs	r2, #96	; 0x60
 8003336:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800333a:	4850      	ldr	r0, [pc, #320]	; (800347c <mk_hw_config+0x2cc>)
 800333c:	f7ff fc04 	bl	8002b48 <LL_TIM_OC_SetMode>
 8003340:	2101      	movs	r1, #1
 8003342:	484e      	ldr	r0, [pc, #312]	; (800347c <mk_hw_config+0x2cc>)
 8003344:	f7ff fc4a 	bl	8002bdc <LL_TIM_OC_EnableFast>
 8003348:	2110      	movs	r1, #16
 800334a:	484c      	ldr	r0, [pc, #304]	; (800347c <mk_hw_config+0x2cc>)
 800334c:	f7ff fc46 	bl	8002bdc <LL_TIM_OC_EnableFast>
 8003350:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003354:	4849      	ldr	r0, [pc, #292]	; (800347c <mk_hw_config+0x2cc>)
 8003356:	f7ff fc41 	bl	8002bdc <LL_TIM_OC_EnableFast>
 800335a:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 800335e:	4847      	ldr	r0, [pc, #284]	; (800347c <mk_hw_config+0x2cc>)
 8003360:	f7ff fc3c 	bl	8002bdc <LL_TIM_OC_EnableFast>
 8003364:	2101      	movs	r1, #1
 8003366:	4845      	ldr	r0, [pc, #276]	; (800347c <mk_hw_config+0x2cc>)
 8003368:	f7ff fc7a 	bl	8002c60 <LL_TIM_OC_EnablePreload>
 800336c:	2110      	movs	r1, #16
 800336e:	4843      	ldr	r0, [pc, #268]	; (800347c <mk_hw_config+0x2cc>)
 8003370:	f7ff fc76 	bl	8002c60 <LL_TIM_OC_EnablePreload>
 8003374:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003378:	4840      	ldr	r0, [pc, #256]	; (800347c <mk_hw_config+0x2cc>)
 800337a:	f7ff fc71 	bl	8002c60 <LL_TIM_OC_EnablePreload>
 800337e:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 8003382:	483e      	ldr	r0, [pc, #248]	; (800347c <mk_hw_config+0x2cc>)
 8003384:	f7ff fc6c 	bl	8002c60 <LL_TIM_OC_EnablePreload>
 8003388:	483c      	ldr	r0, [pc, #240]	; (800347c <mk_hw_config+0x2cc>)
 800338a:	f7ff fb8c 	bl	8002aa6 <LL_TIM_EnableARRPreload>
 800338e:	f241 0168 	movw	r1, #4200	; 0x1068
 8003392:	483a      	ldr	r0, [pc, #232]	; (800347c <mk_hw_config+0x2cc>)
 8003394:	f7ff fca6 	bl	8002ce4 <LL_TIM_OC_SetCompareCH1>
 8003398:	f241 0168 	movw	r1, #4200	; 0x1068
 800339c:	4837      	ldr	r0, [pc, #220]	; (800347c <mk_hw_config+0x2cc>)
 800339e:	f7ff fcaf 	bl	8002d00 <LL_TIM_OC_SetCompareCH2>
 80033a2:	f241 0168 	movw	r1, #4200	; 0x1068
 80033a6:	4835      	ldr	r0, [pc, #212]	; (800347c <mk_hw_config+0x2cc>)
 80033a8:	f7ff fcb8 	bl	8002d1c <LL_TIM_OC_SetCompareCH3>
 80033ac:	f241 0168 	movw	r1, #4200	; 0x1068
 80033b0:	4832      	ldr	r0, [pc, #200]	; (800347c <mk_hw_config+0x2cc>)
 80033b2:	f7ff fcc1 	bl	8002d38 <LL_TIM_OC_SetCompareCH4>
 80033b6:	4831      	ldr	r0, [pc, #196]	; (800347c <mk_hw_config+0x2cc>)
 80033b8:	f7ff fcfd 	bl	8002db6 <LL_TIM_GenerateEvent_UPDATE>
 80033bc:	482f      	ldr	r0, [pc, #188]	; (800347c <mk_hw_config+0x2cc>)
 80033be:	f7ff fb19 	bl	80029f4 <LL_TIM_EnableCounter>
 80033c2:	2010      	movs	r0, #16
 80033c4:	f7ff face 	bl	8002964 <LL_AHB1_GRP1_EnableClock>
 80033c8:	2200      	movs	r2, #0
 80033ca:	2101      	movs	r1, #1
 80033cc:	482c      	ldr	r0, [pc, #176]	; (8003480 <mk_hw_config+0x2d0>)
 80033ce:	f7ff f9f5 	bl	80027bc <LL_GPIO_SetPinMode>
 80033d2:	2200      	movs	r2, #0
 80033d4:	2101      	movs	r1, #1
 80033d6:	482a      	ldr	r0, [pc, #168]	; (8003480 <mk_hw_config+0x2d0>)
 80033d8:	f7ff fa35 	bl	8002846 <LL_GPIO_SetPinPull>
 80033dc:	2010      	movs	r0, #16
 80033de:	f7ff fac1 	bl	8002964 <LL_AHB1_GRP1_EnableClock>
 80033e2:	2200      	movs	r2, #0
 80033e4:	2140      	movs	r1, #64	; 0x40
 80033e6:	4826      	ldr	r0, [pc, #152]	; (8003480 <mk_hw_config+0x2d0>)
 80033e8:	f7ff f9e8 	bl	80027bc <LL_GPIO_SetPinMode>
 80033ec:	2202      	movs	r2, #2
 80033ee:	2140      	movs	r1, #64	; 0x40
 80033f0:	4823      	ldr	r0, [pc, #140]	; (8003480 <mk_hw_config+0x2d0>)
 80033f2:	f7ff fa28 	bl	8002846 <LL_GPIO_SetPinPull>
 80033f6:	f44f 4080 	mov.w	r0, #16384	; 0x4000
 80033fa:	f7ff fae3 	bl	80029c4 <LL_APB2_GRP1_EnableClock>
 80033fe:	4921      	ldr	r1, [pc, #132]	; (8003484 <mk_hw_config+0x2d4>)
 8003400:	2004      	movs	r0, #4
 8003402:	f7ff fce9 	bl	8002dd8 <LL_SYSCFG_SetEXTISource>
 8003406:	2040      	movs	r0, #64	; 0x40
 8003408:	f7ff fd12 	bl	8002e30 <LL_EXTI_EnableIT_0_31>
 800340c:	2040      	movs	r0, #64	; 0x40
 800340e:	f7ff fd21 	bl	8002e54 <LL_EXTI_EnableRisingTrig_0_31>
 8003412:	2040      	movs	r0, #64	; 0x40
 8003414:	f7ff fd30 	bl	8002e78 <LL_EXTI_EnableFallingTrig_0_31>
 8003418:	2106      	movs	r1, #6
 800341a:	2017      	movs	r0, #23
 800341c:	f7ff f9a4 	bl	8002768 <NVIC_SetPriority>
 8003420:	2017      	movs	r0, #23
 8003422:	f7ff f987 	bl	8002734 <NVIC_EnableIRQ>
 8003426:	2200      	movs	r2, #0
 8003428:	2104      	movs	r1, #4
 800342a:	4815      	ldr	r0, [pc, #84]	; (8003480 <mk_hw_config+0x2d0>)
 800342c:	f7ff f9c6 	bl	80027bc <LL_GPIO_SetPinMode>
 8003430:	2200      	movs	r2, #0
 8003432:	2104      	movs	r1, #4
 8003434:	4812      	ldr	r0, [pc, #72]	; (8003480 <mk_hw_config+0x2d0>)
 8003436:	f7ff fa06 	bl	8002846 <LL_GPIO_SetPinPull>
 800343a:	2020      	movs	r0, #32
 800343c:	f7ff faaa 	bl	8002994 <LL_APB1_GRP1_EnableClock>
 8003440:	2100      	movs	r1, #0
 8003442:	4811      	ldr	r0, [pc, #68]	; (8003488 <mk_hw_config+0x2d8>)
 8003444:	f7ff fb1c 	bl	8002a80 <LL_TIM_SetCounterMode>
 8003448:	f240 71cf 	movw	r1, #1999	; 0x7cf
 800344c:	480e      	ldr	r0, [pc, #56]	; (8003488 <mk_hw_config+0x2d8>)
 800344e:	f7ff fb5b 	bl	8002b08 <LL_TIM_SetAutoReload>
 8003452:	f24a 410f 	movw	r1, #41999	; 0xa40f
 8003456:	480c      	ldr	r0, [pc, #48]	; (8003488 <mk_hw_config+0x2d8>)
 8003458:	f7ff fb48 	bl	8002aec <LL_TIM_SetPrescaler>
 800345c:	480a      	ldr	r0, [pc, #40]	; (8003488 <mk_hw_config+0x2d8>)
 800345e:	f7ff fc9a 	bl	8002d96 <LL_TIM_EnableIT_UPDATE>
 8003462:	2106      	movs	r1, #6
 8003464:	2037      	movs	r0, #55	; 0x37
 8003466:	f7ff f97f 	bl	8002768 <NVIC_SetPriority>
 800346a:	2037      	movs	r0, #55	; 0x37
 800346c:	f7ff f962 	bl	8002734 <NVIC_EnableIRQ>
 8003470:	bf00      	nop
 8003472:	bd80      	pop	{r7, pc}
 8003474:	40020000 	.word	0x40020000
 8003478:	40020c00 	.word	0x40020c00
 800347c:	40000800 	.word	0x40000800
 8003480:	40021000 	.word	0x40021000
 8003484:	0f000001 	.word	0x0f000001
 8003488:	40001400 	.word	0x40001400

0800348c <mk_set_pwm_ctrl>:
 800348c:	b480      	push	{r7}
 800348e:	b083      	sub	sp, #12
 8003490:	af00      	add	r7, sp, #0
 8003492:	6078      	str	r0, [r7, #4]
 8003494:	687b      	ldr	r3, [r7, #4]
 8003496:	681b      	ldr	r3, [r3, #0]
 8003498:	f043 0201 	orr.w	r2, r3, #1
 800349c:	687b      	ldr	r3, [r7, #4]
 800349e:	601a      	str	r2, [r3, #0]
 80034a0:	687b      	ldr	r3, [r7, #4]
 80034a2:	681b      	ldr	r3, [r3, #0]
 80034a4:	f023 0202 	bic.w	r2, r3, #2
 80034a8:	687b      	ldr	r3, [r7, #4]
 80034aa:	601a      	str	r2, [r3, #0]
 80034ac:	bf00      	nop
 80034ae:	370c      	adds	r7, #12
 80034b0:	46bd      	mov	sp, r7
 80034b2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80034b6:	4770      	bx	lr

080034b8 <mk_set_speed_ctrl>:
 80034b8:	b480      	push	{r7}
 80034ba:	b083      	sub	sp, #12
 80034bc:	af00      	add	r7, sp, #0
 80034be:	6078      	str	r0, [r7, #4]
 80034c0:	687b      	ldr	r3, [r7, #4]
 80034c2:	681b      	ldr	r3, [r3, #0]
 80034c4:	f043 0202 	orr.w	r2, r3, #2
 80034c8:	687b      	ldr	r3, [r7, #4]
 80034ca:	601a      	str	r2, [r3, #0]
 80034cc:	687b      	ldr	r3, [r7, #4]
 80034ce:	681b      	ldr	r3, [r3, #0]
 80034d0:	f023 0201 	bic.w	r2, r3, #1
 80034d4:	687b      	ldr	r3, [r7, #4]
 80034d6:	601a      	str	r2, [r3, #0]
 80034d8:	bf00      	nop
 80034da:	370c      	adds	r7, #12
 80034dc:	46bd      	mov	sp, r7
 80034de:	f85d 7b04 	ldr.w	r7, [sp], #4
 80034e2:	4770      	bx	lr

080034e4 <mk_set_stop_motors_ctrl>:
 80034e4:	b480      	push	{r7}
 80034e6:	b083      	sub	sp, #12
 80034e8:	af00      	add	r7, sp, #0
 80034ea:	6078      	str	r0, [r7, #4]
 80034ec:	687b      	ldr	r3, [r7, #4]
 80034ee:	681b      	ldr	r3, [r3, #0]
 80034f0:	f043 0204 	orr.w	r2, r3, #4
 80034f4:	687b      	ldr	r3, [r7, #4]
 80034f6:	601a      	str	r2, [r3, #0]
 80034f8:	bf00      	nop
 80034fa:	370c      	adds	r7, #12
 80034fc:	46bd      	mov	sp, r7
 80034fe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003502:	4770      	bx	lr

08003504 <mk_clr_stop_motors_ctrl>:
 8003504:	b480      	push	{r7}
 8003506:	b083      	sub	sp, #12
 8003508:	af00      	add	r7, sp, #0
 800350a:	6078      	str	r0, [r7, #4]
 800350c:	687b      	ldr	r3, [r7, #4]
 800350e:	681b      	ldr	r3, [r3, #0]
 8003510:	f023 0204 	bic.w	r2, r3, #4
 8003514:	687b      	ldr	r3, [r7, #4]
 8003516:	601a      	str	r2, [r3, #0]
 8003518:	bf00      	nop
 800351a:	370c      	adds	r7, #12
 800351c:	46bd      	mov	sp, r7
 800351e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003522:	4770      	bx	lr

08003524 <read_cord_status>:
 8003524:	b580      	push	{r7, lr}
 8003526:	af00      	add	r7, sp, #0
 8003528:	2101      	movs	r1, #1
 800352a:	4803      	ldr	r0, [pc, #12]	; (8003538 <read_cord_status+0x14>)
 800352c:	f7ff f9e7 	bl	80028fe <LL_GPIO_IsInputPinSet>
 8003530:	4603      	mov	r3, r0
 8003532:	b2db      	uxtb	r3, r3
 8003534:	4618      	mov	r0, r3
 8003536:	bd80      	pop	{r7, pc}
 8003538:	40021000 	.word	0x40021000

0800353c <read_side_switch>:
 800353c:	b580      	push	{r7, lr}
 800353e:	af00      	add	r7, sp, #0
 8003540:	2104      	movs	r1, #4
 8003542:	4803      	ldr	r0, [pc, #12]	; (8003550 <read_side_switch+0x14>)
 8003544:	f7ff f9db 	bl	80028fe <LL_GPIO_IsInputPinSet>
 8003548:	4603      	mov	r3, r0
 800354a:	b2db      	uxtb	r3, r3
 800354c:	4618      	mov	r0, r3
 800354e:	bd80      	pop	{r7, pc}
 8003550:	40021000 	.word	0x40021000

08003554 <turn_off_all_motors>:
 8003554:	b580      	push	{r7, lr}
 8003556:	af00      	add	r7, sp, #0
 8003558:	4b05      	ldr	r3, [pc, #20]	; (8003570 <turn_off_all_motors+0x1c>)
 800355a:	681b      	ldr	r3, [r3, #0]
 800355c:	4618      	mov	r0, r3
 800355e:	f7ff ffc1 	bl	80034e4 <mk_set_stop_motors_ctrl>
 8003562:	4804      	ldr	r0, [pc, #16]	; (8003574 <turn_off_all_motors+0x20>)
 8003564:	f7ff fd88 	bl	8003078 <mk_set_pwm>
 8003568:	f7fe fbd6 	bl	8001d18 <manipulators_block>
 800356c:	bf00      	nop
 800356e:	bd80      	pop	{r7, pc}
 8003570:	20000270 	.word	0x20000270
 8003574:	200002dc 	.word	0x200002dc

08003578 <motor_kinematics>:
 8003578:	b580      	push	{r7, lr}
 800357a:	b08e      	sub	sp, #56	; 0x38
 800357c:	af00      	add	r7, sp, #0
 800357e:	6078      	str	r0, [r7, #4]
 8003580:	f107 030c 	add.w	r3, r7, #12
 8003584:	222c      	movs	r2, #44	; 0x2c
 8003586:	2100      	movs	r1, #0
 8003588:	4618      	mov	r0, r3
 800358a:	f007 f9cd 	bl	800a928 <memset>
 800358e:	4b30      	ldr	r3, [pc, #192]	; (8003650 <motor_kinematics+0xd8>)
 8003590:	62fb      	str	r3, [r7, #44]	; 0x2c
 8003592:	4b2f      	ldr	r3, [pc, #188]	; (8003650 <motor_kinematics+0xd8>)
 8003594:	633b      	str	r3, [r7, #48]	; 0x30
 8003596:	4b2e      	ldr	r3, [pc, #184]	; (8003650 <motor_kinematics+0xd8>)
 8003598:	637b      	str	r3, [r7, #52]	; 0x34
 800359a:	f7ff fe09 	bl	80031b0 <mk_hw_config>
 800359e:	492d      	ldr	r1, [pc, #180]	; (8003654 <motor_kinematics+0xdc>)
 80035a0:	2001      	movs	r0, #1
 80035a2:	f003 feaf 	bl	8007304 <xQueueCreateMutexStatic>
 80035a6:	4603      	mov	r3, r0
 80035a8:	62bb      	str	r3, [r7, #40]	; 0x28
 80035aa:	f005 fa1b 	bl	80089e4 <xTaskGetCurrentTaskHandle>
 80035ae:	4603      	mov	r3, r0
 80035b0:	627b      	str	r3, [r7, #36]	; 0x24
 80035b2:	4a29      	ldr	r2, [pc, #164]	; (8003658 <motor_kinematics+0xe0>)
 80035b4:	f107 030c 	add.w	r3, r7, #12
 80035b8:	6013      	str	r3, [r2, #0]
 80035ba:	4b27      	ldr	r3, [pc, #156]	; (8003658 <motor_kinematics+0xe0>)
 80035bc:	681b      	ldr	r3, [r3, #0]
 80035be:	3320      	adds	r3, #32
 80035c0:	4618      	mov	r0, r3
 80035c2:	f7ff fd59 	bl	8003078 <mk_set_pwm>
 80035c6:	f04f 31ff 	mov.w	r1, #4294967295
 80035ca:	2001      	movs	r0, #1
 80035cc:	f005 fbe8 	bl	8008da0 <ulTaskNotifyTake>
 80035d0:	4b21      	ldr	r3, [pc, #132]	; (8003658 <motor_kinematics+0xe0>)
 80035d2:	681b      	ldr	r3, [r3, #0]
 80035d4:	69db      	ldr	r3, [r3, #28]
 80035d6:	f04f 31ff 	mov.w	r1, #4294967295
 80035da:	4618      	mov	r0, r3
 80035dc:	f004 f918 	bl	8007810 <xQueueSemaphoreTake>
 80035e0:	4b1d      	ldr	r3, [pc, #116]	; (8003658 <motor_kinematics+0xe0>)
 80035e2:	681b      	ldr	r3, [r3, #0]
 80035e4:	681b      	ldr	r3, [r3, #0]
 80035e6:	f003 0304 	and.w	r3, r3, #4
 80035ea:	2b00      	cmp	r3, #0
 80035ec:	d00e      	beq.n	800360c <motor_kinematics+0x94>
 80035ee:	4b1a      	ldr	r3, [pc, #104]	; (8003658 <motor_kinematics+0xe0>)
 80035f0:	681b      	ldr	r3, [r3, #0]
 80035f2:	f04f 0200 	mov.w	r2, #0
 80035f6:	621a      	str	r2, [r3, #32]
 80035f8:	4b17      	ldr	r3, [pc, #92]	; (8003658 <motor_kinematics+0xe0>)
 80035fa:	681b      	ldr	r3, [r3, #0]
 80035fc:	f04f 0200 	mov.w	r2, #0
 8003600:	625a      	str	r2, [r3, #36]	; 0x24
 8003602:	4b15      	ldr	r3, [pc, #84]	; (8003658 <motor_kinematics+0xe0>)
 8003604:	681b      	ldr	r3, [r3, #0]
 8003606:	f04f 0200 	mov.w	r2, #0
 800360a:	629a      	str	r2, [r3, #40]	; 0x28
 800360c:	4b12      	ldr	r3, [pc, #72]	; (8003658 <motor_kinematics+0xe0>)
 800360e:	681b      	ldr	r3, [r3, #0]
 8003610:	681b      	ldr	r3, [r3, #0]
 8003612:	f003 0302 	and.w	r3, r3, #2
 8003616:	2b00      	cmp	r3, #0
 8003618:	d00b      	beq.n	8003632 <motor_kinematics+0xba>
 800361a:	4b0f      	ldr	r3, [pc, #60]	; (8003658 <motor_kinematics+0xe0>)
 800361c:	681b      	ldr	r3, [r3, #0]
 800361e:	681b      	ldr	r3, [r3, #0]
 8003620:	f003 0304 	and.w	r3, r3, #4
 8003624:	2b00      	cmp	r3, #0
 8003626:	d104      	bne.n	8003632 <motor_kinematics+0xba>
 8003628:	4b0b      	ldr	r3, [pc, #44]	; (8003658 <motor_kinematics+0xe0>)
 800362a:	681b      	ldr	r3, [r3, #0]
 800362c:	4618      	mov	r0, r3
 800362e:	f7ff fc45 	bl	8002ebc <mk_speed2pwm>
 8003632:	4b09      	ldr	r3, [pc, #36]	; (8003658 <motor_kinematics+0xe0>)
 8003634:	681b      	ldr	r3, [r3, #0]
 8003636:	3320      	adds	r3, #32
 8003638:	4618      	mov	r0, r3
 800363a:	f7ff fd1d 	bl	8003078 <mk_set_pwm>
 800363e:	4b06      	ldr	r3, [pc, #24]	; (8003658 <motor_kinematics+0xe0>)
 8003640:	681b      	ldr	r3, [r3, #0]
 8003642:	69d8      	ldr	r0, [r3, #28]
 8003644:	2300      	movs	r3, #0
 8003646:	2200      	movs	r2, #0
 8003648:	2100      	movs	r1, #0
 800364a:	f003 fe77 	bl	800733c <xQueueGenericSend>
 800364e:	e7ba      	b.n	80035c6 <motor_kinematics+0x4e>
 8003650:	3dcccccd 	.word	0x3dcccccd
 8003654:	20014ea0 	.word	0x20014ea0
 8003658:	20000270 	.word	0x20000270

0800365c <cmd_set_robot_session>:
 800365c:	b580      	push	{r7, lr}
 800365e:	b082      	sub	sp, #8
 8003660:	af00      	add	r7, sp, #0
 8003662:	6078      	str	r0, [r7, #4]
 8003664:	4b0f      	ldr	r3, [pc, #60]	; (80036a4 <cmd_set_robot_session+0x48>)
 8003666:	681b      	ldr	r3, [r3, #0]
 8003668:	2b00      	cmp	r3, #0
 800366a:	d00f      	beq.n	800368c <cmd_set_robot_session+0x30>
 800366c:	4b0d      	ldr	r3, [pc, #52]	; (80036a4 <cmd_set_robot_session+0x48>)
 800366e:	681b      	ldr	r3, [r3, #0]
 8003670:	2201      	movs	r2, #1
 8003672:	711a      	strb	r2, [r3, #4]
 8003674:	4b0b      	ldr	r3, [pc, #44]	; (80036a4 <cmd_set_robot_session+0x48>)
 8003676:	681b      	ldr	r3, [r3, #0]
 8003678:	4618      	mov	r0, r3
 800367a:	f7ff ff43 	bl	8003504 <mk_clr_stop_motors_ctrl>
 800367e:	2203      	movs	r2, #3
 8003680:	4909      	ldr	r1, [pc, #36]	; (80036a8 <cmd_set_robot_session+0x4c>)
 8003682:	6878      	ldr	r0, [r7, #4]
 8003684:	f007 f942 	bl	800a90c <memcpy>
 8003688:	2303      	movs	r3, #3
 800368a:	e006      	b.n	800369a <cmd_set_robot_session+0x3e>
 800368c:	bf00      	nop
 800368e:	2203      	movs	r2, #3
 8003690:	4906      	ldr	r1, [pc, #24]	; (80036ac <cmd_set_robot_session+0x50>)
 8003692:	6878      	ldr	r0, [r7, #4]
 8003694:	f007 f93a 	bl	800a90c <memcpy>
 8003698:	2303      	movs	r3, #3
 800369a:	4618      	mov	r0, r3
 800369c:	3708      	adds	r7, #8
 800369e:	46bd      	mov	sp, r7
 80036a0:	bd80      	pop	{r7, pc}
 80036a2:	bf00      	nop
 80036a4:	20000270 	.word	0x20000270
 80036a8:	0800d0e0 	.word	0x0800d0e0
 80036ac:	0800d0e4 	.word	0x0800d0e4

080036b0 <cmd_read_cord_status>:
 80036b0:	b590      	push	{r4, r7, lr}
 80036b2:	b083      	sub	sp, #12
 80036b4:	af00      	add	r7, sp, #0
 80036b6:	6078      	str	r0, [r7, #4]
 80036b8:	4b15      	ldr	r3, [pc, #84]	; (8003710 <cmd_read_cord_status+0x60>)
 80036ba:	681c      	ldr	r4, [r3, #0]
 80036bc:	f7ff ff32 	bl	8003524 <read_cord_status>
 80036c0:	4603      	mov	r3, r0
 80036c2:	7163      	strb	r3, [r4, #5]
 80036c4:	4b12      	ldr	r3, [pc, #72]	; (8003710 <cmd_read_cord_status+0x60>)
 80036c6:	681b      	ldr	r3, [r3, #0]
 80036c8:	795b      	ldrb	r3, [r3, #5]
 80036ca:	2b01      	cmp	r3, #1
 80036cc:	d115      	bne.n	80036fa <cmd_read_cord_status+0x4a>
 80036ce:	4b10      	ldr	r3, [pc, #64]	; (8003710 <cmd_read_cord_status+0x60>)
 80036d0:	681b      	ldr	r3, [r3, #0]
 80036d2:	4618      	mov	r0, r3
 80036d4:	f7ff ff16 	bl	8003504 <mk_clr_stop_motors_ctrl>
 80036d8:	480e      	ldr	r0, [pc, #56]	; (8003714 <cmd_read_cord_status+0x64>)
 80036da:	f7ff f99b 	bl	8002a14 <LL_TIM_IsEnabledCounter>
 80036de:	4603      	mov	r3, r0
 80036e0:	2b00      	cmp	r3, #0
 80036e2:	d102      	bne.n	80036ea <cmd_read_cord_status+0x3a>
 80036e4:	480b      	ldr	r0, [pc, #44]	; (8003714 <cmd_read_cord_status+0x64>)
 80036e6:	f7ff f985 	bl	80029f4 <LL_TIM_EnableCounter>
 80036ea:	4b09      	ldr	r3, [pc, #36]	; (8003710 <cmd_read_cord_status+0x60>)
 80036ec:	681b      	ldr	r3, [r3, #0]
 80036ee:	6998      	ldr	r0, [r3, #24]
 80036f0:	2300      	movs	r3, #0
 80036f2:	2202      	movs	r2, #2
 80036f4:	2100      	movs	r1, #0
 80036f6:	f005 fb9f 	bl	8008e38 <xTaskGenericNotify>
 80036fa:	4b05      	ldr	r3, [pc, #20]	; (8003710 <cmd_read_cord_status+0x60>)
 80036fc:	681b      	ldr	r3, [r3, #0]
 80036fe:	3305      	adds	r3, #5
 8003700:	781a      	ldrb	r2, [r3, #0]
 8003702:	687b      	ldr	r3, [r7, #4]
 8003704:	701a      	strb	r2, [r3, #0]
 8003706:	2301      	movs	r3, #1
 8003708:	4618      	mov	r0, r3
 800370a:	370c      	adds	r7, #12
 800370c:	46bd      	mov	sp, r7
 800370e:	bd90      	pop	{r4, r7, pc}
 8003710:	20000270 	.word	0x20000270
 8003714:	40001400 	.word	0x40001400

08003718 <cmd_read_side_switch>:
 8003718:	b590      	push	{r4, r7, lr}
 800371a:	b083      	sub	sp, #12
 800371c:	af00      	add	r7, sp, #0
 800371e:	6078      	str	r0, [r7, #4]
 8003720:	4b08      	ldr	r3, [pc, #32]	; (8003744 <cmd_read_side_switch+0x2c>)
 8003722:	681c      	ldr	r4, [r3, #0]
 8003724:	f7ff ff0a 	bl	800353c <read_side_switch>
 8003728:	4603      	mov	r3, r0
 800372a:	72a3      	strb	r3, [r4, #10]
 800372c:	4b05      	ldr	r3, [pc, #20]	; (8003744 <cmd_read_side_switch+0x2c>)
 800372e:	681b      	ldr	r3, [r3, #0]
 8003730:	330a      	adds	r3, #10
 8003732:	781a      	ldrb	r2, [r3, #0]
 8003734:	687b      	ldr	r3, [r7, #4]
 8003736:	701a      	strb	r2, [r3, #0]
 8003738:	2301      	movs	r3, #1
 800373a:	4618      	mov	r0, r3
 800373c:	370c      	adds	r7, #12
 800373e:	46bd      	mov	sp, r7
 8003740:	bd90      	pop	{r4, r7, pc}
 8003742:	bf00      	nop
 8003744:	20000270 	.word	0x20000270

08003748 <cmd_read_strategy>:
 8003748:	b480      	push	{r7}
 800374a:	b083      	sub	sp, #12
 800374c:	af00      	add	r7, sp, #0
 800374e:	6078      	str	r0, [r7, #4]
 8003750:	4b06      	ldr	r3, [pc, #24]	; (800376c <cmd_read_strategy+0x24>)
 8003752:	681b      	ldr	r3, [r3, #0]
 8003754:	3306      	adds	r3, #6
 8003756:	781a      	ldrb	r2, [r3, #0]
 8003758:	687b      	ldr	r3, [r7, #4]
 800375a:	701a      	strb	r2, [r3, #0]
 800375c:	2301      	movs	r3, #1
 800375e:	4618      	mov	r0, r3
 8003760:	370c      	adds	r7, #12
 8003762:	46bd      	mov	sp, r7
 8003764:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003768:	4770      	bx	lr
 800376a:	bf00      	nop
 800376c:	20000270 	.word	0x20000270

08003770 <cmd_set_pwm>:
 8003770:	b590      	push	{r4, r7, lr}
 8003772:	b085      	sub	sp, #20
 8003774:	af00      	add	r7, sp, #0
 8003776:	6078      	str	r0, [r7, #4]
 8003778:	687b      	ldr	r3, [r7, #4]
 800377a:	60fb      	str	r3, [r7, #12]
 800377c:	4b3f      	ldr	r3, [pc, #252]	; (800387c <cmd_set_pwm+0x10c>)
 800377e:	681b      	ldr	r3, [r3, #0]
 8003780:	2b00      	cmp	r3, #0
 8003782:	d06d      	beq.n	8003860 <cmd_set_pwm+0xf0>
 8003784:	68fb      	ldr	r3, [r7, #12]
 8003786:	781b      	ldrb	r3, [r3, #0]
 8003788:	2b03      	cmp	r3, #3
 800378a:	d86b      	bhi.n	8003864 <cmd_set_pwm+0xf4>
 800378c:	68fb      	ldr	r3, [r7, #12]
 800378e:	781b      	ldrb	r3, [r3, #0]
 8003790:	2b00      	cmp	r3, #0
 8003792:	d067      	beq.n	8003864 <cmd_set_pwm+0xf4>
 8003794:	68fb      	ldr	r3, [r7, #12]
 8003796:	785a      	ldrb	r2, [r3, #1]
 8003798:	7899      	ldrb	r1, [r3, #2]
 800379a:	0209      	lsls	r1, r1, #8
 800379c:	430a      	orrs	r2, r1
 800379e:	78d9      	ldrb	r1, [r3, #3]
 80037a0:	0409      	lsls	r1, r1, #16
 80037a2:	430a      	orrs	r2, r1
 80037a4:	791b      	ldrb	r3, [r3, #4]
 80037a6:	061b      	lsls	r3, r3, #24
 80037a8:	4313      	orrs	r3, r2
 80037aa:	ee07 3a90 	vmov	s15, r3
 80037ae:	eef0 7ae7 	vabs.f32	s15, s15
 80037b2:	ed9f 7a33 	vldr	s14, [pc, #204]	; 8003880 <cmd_set_pwm+0x110>
 80037b6:	eef4 7ac7 	vcmpe.f32	s15, s14
 80037ba:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80037be:	d451      	bmi.n	8003864 <cmd_set_pwm+0xf4>
 80037c0:	68fb      	ldr	r3, [r7, #12]
 80037c2:	785a      	ldrb	r2, [r3, #1]
 80037c4:	7899      	ldrb	r1, [r3, #2]
 80037c6:	0209      	lsls	r1, r1, #8
 80037c8:	430a      	orrs	r2, r1
 80037ca:	78d9      	ldrb	r1, [r3, #3]
 80037cc:	0409      	lsls	r1, r1, #16
 80037ce:	430a      	orrs	r2, r1
 80037d0:	791b      	ldrb	r3, [r3, #4]
 80037d2:	061b      	lsls	r3, r3, #24
 80037d4:	4313      	orrs	r3, r2
 80037d6:	ee07 3a90 	vmov	s15, r3
 80037da:	eef0 7ae7 	vabs.f32	s15, s15
 80037de:	eeb7 7a00 	vmov.f32	s14, #112	; 0x3f800000  1.0
 80037e2:	eef4 7ac7 	vcmpe.f32	s15, s14
 80037e6:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80037ea:	dc3b      	bgt.n	8003864 <cmd_set_pwm+0xf4>
 80037ec:	4b23      	ldr	r3, [pc, #140]	; (800387c <cmd_set_pwm+0x10c>)
 80037ee:	681b      	ldr	r3, [r3, #0]
 80037f0:	69db      	ldr	r3, [r3, #28]
 80037f2:	f04f 31ff 	mov.w	r1, #4294967295
 80037f6:	4618      	mov	r0, r3
 80037f8:	f004 f80a 	bl	8007810 <xQueueSemaphoreTake>
 80037fc:	4b1f      	ldr	r3, [pc, #124]	; (800387c <cmd_set_pwm+0x10c>)
 80037fe:	681b      	ldr	r3, [r3, #0]
 8003800:	4618      	mov	r0, r3
 8003802:	f7ff fe43 	bl	800348c <mk_set_pwm_ctrl>
 8003806:	4b1d      	ldr	r3, [pc, #116]	; (800387c <cmd_set_pwm+0x10c>)
 8003808:	681a      	ldr	r2, [r3, #0]
 800380a:	68fb      	ldr	r3, [r7, #12]
 800380c:	781b      	ldrb	r3, [r3, #0]
 800380e:	1e59      	subs	r1, r3, #1
 8003810:	68fb      	ldr	r3, [r7, #12]
 8003812:	7858      	ldrb	r0, [r3, #1]
 8003814:	789c      	ldrb	r4, [r3, #2]
 8003816:	0224      	lsls	r4, r4, #8
 8003818:	4320      	orrs	r0, r4
 800381a:	78dc      	ldrb	r4, [r3, #3]
 800381c:	0424      	lsls	r4, r4, #16
 800381e:	4320      	orrs	r0, r4
 8003820:	791b      	ldrb	r3, [r3, #4]
 8003822:	061b      	lsls	r3, r3, #24
 8003824:	4303      	orrs	r3, r0
 8003826:	4618      	mov	r0, r3
 8003828:	f101 0308 	add.w	r3, r1, #8
 800382c:	009b      	lsls	r3, r3, #2
 800382e:	4413      	add	r3, r2
 8003830:	6018      	str	r0, [r3, #0]
 8003832:	4b12      	ldr	r3, [pc, #72]	; (800387c <cmd_set_pwm+0x10c>)
 8003834:	681b      	ldr	r3, [r3, #0]
 8003836:	69d8      	ldr	r0, [r3, #28]
 8003838:	2300      	movs	r3, #0
 800383a:	2200      	movs	r2, #0
 800383c:	2100      	movs	r1, #0
 800383e:	f003 fd7d 	bl	800733c <xQueueGenericSend>
 8003842:	4b0e      	ldr	r3, [pc, #56]	; (800387c <cmd_set_pwm+0x10c>)
 8003844:	681b      	ldr	r3, [r3, #0]
 8003846:	6998      	ldr	r0, [r3, #24]
 8003848:	2300      	movs	r3, #0
 800384a:	2202      	movs	r2, #2
 800384c:	2100      	movs	r1, #0
 800384e:	f005 faf3 	bl	8008e38 <xTaskGenericNotify>
 8003852:	2203      	movs	r2, #3
 8003854:	490b      	ldr	r1, [pc, #44]	; (8003884 <cmd_set_pwm+0x114>)
 8003856:	6878      	ldr	r0, [r7, #4]
 8003858:	f007 f858 	bl	800a90c <memcpy>
 800385c:	2303      	movs	r3, #3
 800385e:	e008      	b.n	8003872 <cmd_set_pwm+0x102>
 8003860:	bf00      	nop
 8003862:	e000      	b.n	8003866 <cmd_set_pwm+0xf6>
 8003864:	bf00      	nop
 8003866:	2203      	movs	r2, #3
 8003868:	4907      	ldr	r1, [pc, #28]	; (8003888 <cmd_set_pwm+0x118>)
 800386a:	6878      	ldr	r0, [r7, #4]
 800386c:	f007 f84e 	bl	800a90c <memcpy>
 8003870:	2303      	movs	r3, #3
 8003872:	4618      	mov	r0, r3
 8003874:	3714      	adds	r7, #20
 8003876:	46bd      	mov	sp, r7
 8003878:	bd90      	pop	{r4, r7, pc}
 800387a:	bf00      	nop
 800387c:	20000270 	.word	0x20000270
 8003880:	3dcccccd 	.word	0x3dcccccd
 8003884:	0800d0e0 	.word	0x0800d0e0
 8003888:	0800d0e4 	.word	0x0800d0e4

0800388c <cmd_set_speed>:
 800388c:	b580      	push	{r7, lr}
 800388e:	b084      	sub	sp, #16
 8003890:	af00      	add	r7, sp, #0
 8003892:	6078      	str	r0, [r7, #4]
 8003894:	687b      	ldr	r3, [r7, #4]
 8003896:	60fb      	str	r3, [r7, #12]
 8003898:	4b1d      	ldr	r3, [pc, #116]	; (8003910 <cmd_set_speed+0x84>)
 800389a:	681b      	ldr	r3, [r3, #0]
 800389c:	2b00      	cmp	r3, #0
 800389e:	d02b      	beq.n	80038f8 <cmd_set_speed+0x6c>
 80038a0:	4b1b      	ldr	r3, [pc, #108]	; (8003910 <cmd_set_speed+0x84>)
 80038a2:	681b      	ldr	r3, [r3, #0]
 80038a4:	69db      	ldr	r3, [r3, #28]
 80038a6:	f04f 31ff 	mov.w	r1, #4294967295
 80038aa:	4618      	mov	r0, r3
 80038ac:	f003 ffb0 	bl	8007810 <xQueueSemaphoreTake>
 80038b0:	4b17      	ldr	r3, [pc, #92]	; (8003910 <cmd_set_speed+0x84>)
 80038b2:	681b      	ldr	r3, [r3, #0]
 80038b4:	4618      	mov	r0, r3
 80038b6:	f7ff fdff 	bl	80034b8 <mk_set_speed_ctrl>
 80038ba:	4b15      	ldr	r3, [pc, #84]	; (8003910 <cmd_set_speed+0x84>)
 80038bc:	681b      	ldr	r3, [r3, #0]
 80038be:	330c      	adds	r3, #12
 80038c0:	68f9      	ldr	r1, [r7, #12]
 80038c2:	220c      	movs	r2, #12
 80038c4:	4618      	mov	r0, r3
 80038c6:	f007 f821 	bl	800a90c <memcpy>
 80038ca:	4b11      	ldr	r3, [pc, #68]	; (8003910 <cmd_set_speed+0x84>)
 80038cc:	681b      	ldr	r3, [r3, #0]
 80038ce:	69d8      	ldr	r0, [r3, #28]
 80038d0:	2300      	movs	r3, #0
 80038d2:	2200      	movs	r2, #0
 80038d4:	2100      	movs	r1, #0
 80038d6:	f003 fd31 	bl	800733c <xQueueGenericSend>
 80038da:	4b0d      	ldr	r3, [pc, #52]	; (8003910 <cmd_set_speed+0x84>)
 80038dc:	681b      	ldr	r3, [r3, #0]
 80038de:	6998      	ldr	r0, [r3, #24]
 80038e0:	2300      	movs	r3, #0
 80038e2:	2202      	movs	r2, #2
 80038e4:	2100      	movs	r1, #0
 80038e6:	f005 faa7 	bl	8008e38 <xTaskGenericNotify>
 80038ea:	2203      	movs	r2, #3
 80038ec:	4909      	ldr	r1, [pc, #36]	; (8003914 <cmd_set_speed+0x88>)
 80038ee:	6878      	ldr	r0, [r7, #4]
 80038f0:	f007 f80c 	bl	800a90c <memcpy>
 80038f4:	2303      	movs	r3, #3
 80038f6:	e006      	b.n	8003906 <cmd_set_speed+0x7a>
 80038f8:	bf00      	nop
 80038fa:	2203      	movs	r2, #3
 80038fc:	4906      	ldr	r1, [pc, #24]	; (8003918 <cmd_set_speed+0x8c>)
 80038fe:	6878      	ldr	r0, [r7, #4]
 8003900:	f007 f804 	bl	800a90c <memcpy>
 8003904:	2303      	movs	r3, #3
 8003906:	4618      	mov	r0, r3
 8003908:	3710      	adds	r7, #16
 800390a:	46bd      	mov	sp, r7
 800390c:	bd80      	pop	{r7, pc}
 800390e:	bf00      	nop
 8003910:	20000270 	.word	0x20000270
 8003914:	0800d0e0 	.word	0x0800d0e0
 8003918:	0800d0e4 	.word	0x0800d0e4

0800391c <TIM7_IRQHandler>:
 800391c:	b580      	push	{r7, lr}
 800391e:	b082      	sub	sp, #8
 8003920:	af00      	add	r7, sp, #0
 8003922:	2300      	movs	r3, #0
 8003924:	607b      	str	r3, [r7, #4]
 8003926:	481a      	ldr	r0, [pc, #104]	; (8003990 <TIM7_IRQHandler+0x74>)
 8003928:	f7ff fa22 	bl	8002d70 <LL_TIM_IsActiveFlag_UPDATE>
 800392c:	4603      	mov	r3, r0
 800392e:	2b00      	cmp	r3, #0
 8003930:	d01e      	beq.n	8003970 <TIM7_IRQHandler+0x54>
 8003932:	4817      	ldr	r0, [pc, #92]	; (8003990 <TIM7_IRQHandler+0x74>)
 8003934:	f7ff fa0e 	bl	8002d54 <LL_TIM_ClearFlag_UPDATE>
 8003938:	4b16      	ldr	r3, [pc, #88]	; (8003994 <TIM7_IRQHandler+0x78>)
 800393a:	681b      	ldr	r3, [r3, #0]
 800393c:	3301      	adds	r3, #1
 800393e:	4a15      	ldr	r2, [pc, #84]	; (8003994 <TIM7_IRQHandler+0x78>)
 8003940:	6013      	str	r3, [r2, #0]
 8003942:	4b14      	ldr	r3, [pc, #80]	; (8003994 <TIM7_IRQHandler+0x78>)
 8003944:	681b      	ldr	r3, [r3, #0]
 8003946:	2b63      	cmp	r3, #99	; 0x63
 8003948:	d907      	bls.n	800395a <TIM7_IRQHandler+0x3e>
 800394a:	4b13      	ldr	r3, [pc, #76]	; (8003998 <TIM7_IRQHandler+0x7c>)
 800394c:	681b      	ldr	r3, [r3, #0]
 800394e:	791b      	ldrb	r3, [r3, #4]
 8003950:	2b01      	cmp	r3, #1
 8003952:	d002      	beq.n	800395a <TIM7_IRQHandler+0x3e>
 8003954:	f7ff fdfe 	bl	8003554 <turn_off_all_motors>
 8003958:	e00a      	b.n	8003970 <TIM7_IRQHandler+0x54>
 800395a:	4b0e      	ldr	r3, [pc, #56]	; (8003994 <TIM7_IRQHandler+0x78>)
 800395c:	681b      	ldr	r3, [r3, #0]
 800395e:	2b5e      	cmp	r3, #94	; 0x5e
 8003960:	d906      	bls.n	8003970 <TIM7_IRQHandler+0x54>
 8003962:	4b0d      	ldr	r3, [pc, #52]	; (8003998 <TIM7_IRQHandler+0x7c>)
 8003964:	681b      	ldr	r3, [r3, #0]
 8003966:	791b      	ldrb	r3, [r3, #4]
 8003968:	2b01      	cmp	r3, #1
 800396a:	d001      	beq.n	8003970 <TIM7_IRQHandler+0x54>
 800396c:	f7fe f9fa 	bl	8001d64 <manipulators_rise_flag>
 8003970:	687b      	ldr	r3, [r7, #4]
 8003972:	2b00      	cmp	r3, #0
 8003974:	d007      	beq.n	8003986 <TIM7_IRQHandler+0x6a>
 8003976:	4b09      	ldr	r3, [pc, #36]	; (800399c <TIM7_IRQHandler+0x80>)
 8003978:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800397c:	601a      	str	r2, [r3, #0]
 800397e:	f3bf 8f4f 	dsb	sy
 8003982:	f3bf 8f6f 	isb	sy
 8003986:	bf00      	nop
 8003988:	3708      	adds	r7, #8
 800398a:	46bd      	mov	sp, r7
 800398c:	bd80      	pop	{r7, pc}
 800398e:	bf00      	nop
 8003990:	40001400 	.word	0x40001400
 8003994:	200002e8 	.word	0x200002e8
 8003998:	20000270 	.word	0x20000270
 800399c:	e000ed04 	.word	0xe000ed04

080039a0 <EXTI9_5_IRQHandler>:
 80039a0:	b580      	push	{r7, lr}
 80039a2:	b082      	sub	sp, #8
 80039a4:	af00      	add	r7, sp, #0
 80039a6:	2300      	movs	r3, #0
 80039a8:	607b      	str	r3, [r7, #4]
 80039aa:	f004 fcb7 	bl	800831c <xTaskGetTickCountFromISR>
 80039ae:	4603      	mov	r3, r0
 80039b0:	807b      	strh	r3, [r7, #2]
 80039b2:	887a      	ldrh	r2, [r7, #2]
 80039b4:	4b16      	ldr	r3, [pc, #88]	; (8003a10 <EXTI9_5_IRQHandler+0x70>)
 80039b6:	681b      	ldr	r3, [r3, #0]
 80039b8:	891b      	ldrh	r3, [r3, #8]
 80039ba:	f503 737a 	add.w	r3, r3, #1000	; 0x3e8
 80039be:	429a      	cmp	r2, r3
 80039c0:	dd10      	ble.n	80039e4 <EXTI9_5_IRQHandler+0x44>
 80039c2:	4b13      	ldr	r3, [pc, #76]	; (8003a10 <EXTI9_5_IRQHandler+0x70>)
 80039c4:	681b      	ldr	r3, [r3, #0]
 80039c6:	799b      	ldrb	r3, [r3, #6]
 80039c8:	1c5a      	adds	r2, r3, #1
 80039ca:	4b12      	ldr	r3, [pc, #72]	; (8003a14 <EXTI9_5_IRQHandler+0x74>)
 80039cc:	fb83 3102 	smull	r3, r1, r3, r2
 80039d0:	17d3      	asrs	r3, r2, #31
 80039d2:	1ac9      	subs	r1, r1, r3
 80039d4:	460b      	mov	r3, r1
 80039d6:	005b      	lsls	r3, r3, #1
 80039d8:	440b      	add	r3, r1
 80039da:	1ad1      	subs	r1, r2, r3
 80039dc:	4b0c      	ldr	r3, [pc, #48]	; (8003a10 <EXTI9_5_IRQHandler+0x70>)
 80039de:	681b      	ldr	r3, [r3, #0]
 80039e0:	b2ca      	uxtb	r2, r1
 80039e2:	719a      	strb	r2, [r3, #6]
 80039e4:	4b0a      	ldr	r3, [pc, #40]	; (8003a10 <EXTI9_5_IRQHandler+0x70>)
 80039e6:	681b      	ldr	r3, [r3, #0]
 80039e8:	887a      	ldrh	r2, [r7, #2]
 80039ea:	811a      	strh	r2, [r3, #8]
 80039ec:	2040      	movs	r0, #64	; 0x40
 80039ee:	f7ff fa55 	bl	8002e9c <LL_EXTI_ClearFlag_0_31>
 80039f2:	687b      	ldr	r3, [r7, #4]
 80039f4:	2b00      	cmp	r3, #0
 80039f6:	d007      	beq.n	8003a08 <EXTI9_5_IRQHandler+0x68>
 80039f8:	4b07      	ldr	r3, [pc, #28]	; (8003a18 <EXTI9_5_IRQHandler+0x78>)
 80039fa:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80039fe:	601a      	str	r2, [r3, #0]
 8003a00:	f3bf 8f4f 	dsb	sy
 8003a04:	f3bf 8f6f 	isb	sy
 8003a08:	bf00      	nop
 8003a0a:	3708      	adds	r7, #8
 8003a0c:	46bd      	mov	sp, r7
 8003a0e:	bd80      	pop	{r7, pc}
 8003a10:	20000270 	.word	0x20000270
 8003a14:	55555556 	.word	0x55555556
 8003a18:	e000ed04 	.word	0xe000ed04

08003a1c <NVIC_EnableIRQ>:
 8003a1c:	b480      	push	{r7}
 8003a1e:	b083      	sub	sp, #12
 8003a20:	af00      	add	r7, sp, #0
 8003a22:	4603      	mov	r3, r0
 8003a24:	71fb      	strb	r3, [r7, #7]
 8003a26:	79fb      	ldrb	r3, [r7, #7]
 8003a28:	f003 021f 	and.w	r2, r3, #31
 8003a2c:	4907      	ldr	r1, [pc, #28]	; (8003a4c <NVIC_EnableIRQ+0x30>)
 8003a2e:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003a32:	095b      	lsrs	r3, r3, #5
 8003a34:	2001      	movs	r0, #1
 8003a36:	fa00 f202 	lsl.w	r2, r0, r2
 8003a3a:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8003a3e:	bf00      	nop
 8003a40:	370c      	adds	r7, #12
 8003a42:	46bd      	mov	sp, r7
 8003a44:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003a48:	4770      	bx	lr
 8003a4a:	bf00      	nop
 8003a4c:	e000e100 	.word	0xe000e100

08003a50 <NVIC_SetPriority>:
 8003a50:	b480      	push	{r7}
 8003a52:	b083      	sub	sp, #12
 8003a54:	af00      	add	r7, sp, #0
 8003a56:	4603      	mov	r3, r0
 8003a58:	6039      	str	r1, [r7, #0]
 8003a5a:	71fb      	strb	r3, [r7, #7]
 8003a5c:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003a60:	2b00      	cmp	r3, #0
 8003a62:	da0b      	bge.n	8003a7c <NVIC_SetPriority+0x2c>
 8003a64:	683b      	ldr	r3, [r7, #0]
 8003a66:	b2da      	uxtb	r2, r3
 8003a68:	490c      	ldr	r1, [pc, #48]	; (8003a9c <NVIC_SetPriority+0x4c>)
 8003a6a:	79fb      	ldrb	r3, [r7, #7]
 8003a6c:	f003 030f 	and.w	r3, r3, #15
 8003a70:	3b04      	subs	r3, #4
 8003a72:	0112      	lsls	r2, r2, #4
 8003a74:	b2d2      	uxtb	r2, r2
 8003a76:	440b      	add	r3, r1
 8003a78:	761a      	strb	r2, [r3, #24]
 8003a7a:	e009      	b.n	8003a90 <NVIC_SetPriority+0x40>
 8003a7c:	683b      	ldr	r3, [r7, #0]
 8003a7e:	b2da      	uxtb	r2, r3
 8003a80:	4907      	ldr	r1, [pc, #28]	; (8003aa0 <NVIC_SetPriority+0x50>)
 8003a82:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003a86:	0112      	lsls	r2, r2, #4
 8003a88:	b2d2      	uxtb	r2, r2
 8003a8a:	440b      	add	r3, r1
 8003a8c:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8003a90:	bf00      	nop
 8003a92:	370c      	adds	r7, #12
 8003a94:	46bd      	mov	sp, r7
 8003a96:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003a9a:	4770      	bx	lr
 8003a9c:	e000ed00 	.word	0xe000ed00
 8003aa0:	e000e100 	.word	0xe000e100

08003aa4 <LL_AHB1_GRP1_EnableClock>:
 8003aa4:	b480      	push	{r7}
 8003aa6:	b085      	sub	sp, #20
 8003aa8:	af00      	add	r7, sp, #0
 8003aaa:	6078      	str	r0, [r7, #4]
 8003aac:	4b08      	ldr	r3, [pc, #32]	; (8003ad0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003aae:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8003ab0:	4907      	ldr	r1, [pc, #28]	; (8003ad0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003ab2:	687b      	ldr	r3, [r7, #4]
 8003ab4:	4313      	orrs	r3, r2
 8003ab6:	630b      	str	r3, [r1, #48]	; 0x30
 8003ab8:	4b05      	ldr	r3, [pc, #20]	; (8003ad0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003aba:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8003abc:	687b      	ldr	r3, [r7, #4]
 8003abe:	4013      	ands	r3, r2
 8003ac0:	60fb      	str	r3, [r7, #12]
 8003ac2:	68fb      	ldr	r3, [r7, #12]
 8003ac4:	bf00      	nop
 8003ac6:	3714      	adds	r7, #20
 8003ac8:	46bd      	mov	sp, r7
 8003aca:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ace:	4770      	bx	lr
 8003ad0:	40023800 	.word	0x40023800

08003ad4 <LL_APB1_GRP1_EnableClock>:
 8003ad4:	b480      	push	{r7}
 8003ad6:	b085      	sub	sp, #20
 8003ad8:	af00      	add	r7, sp, #0
 8003ada:	6078      	str	r0, [r7, #4]
 8003adc:	4b08      	ldr	r3, [pc, #32]	; (8003b00 <LL_APB1_GRP1_EnableClock+0x2c>)
 8003ade:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8003ae0:	4907      	ldr	r1, [pc, #28]	; (8003b00 <LL_APB1_GRP1_EnableClock+0x2c>)
 8003ae2:	687b      	ldr	r3, [r7, #4]
 8003ae4:	4313      	orrs	r3, r2
 8003ae6:	640b      	str	r3, [r1, #64]	; 0x40
 8003ae8:	4b05      	ldr	r3, [pc, #20]	; (8003b00 <LL_APB1_GRP1_EnableClock+0x2c>)
 8003aea:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8003aec:	687b      	ldr	r3, [r7, #4]
 8003aee:	4013      	ands	r3, r2
 8003af0:	60fb      	str	r3, [r7, #12]
 8003af2:	68fb      	ldr	r3, [r7, #12]
 8003af4:	bf00      	nop
 8003af6:	3714      	adds	r7, #20
 8003af8:	46bd      	mov	sp, r7
 8003afa:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003afe:	4770      	bx	lr
 8003b00:	40023800 	.word	0x40023800

08003b04 <LL_APB2_GRP1_EnableClock>:
 8003b04:	b480      	push	{r7}
 8003b06:	b085      	sub	sp, #20
 8003b08:	af00      	add	r7, sp, #0
 8003b0a:	6078      	str	r0, [r7, #4]
 8003b0c:	4b08      	ldr	r3, [pc, #32]	; (8003b30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8003b0e:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 8003b10:	4907      	ldr	r1, [pc, #28]	; (8003b30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8003b12:	687b      	ldr	r3, [r7, #4]
 8003b14:	4313      	orrs	r3, r2
 8003b16:	644b      	str	r3, [r1, #68]	; 0x44
 8003b18:	4b05      	ldr	r3, [pc, #20]	; (8003b30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8003b1a:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 8003b1c:	687b      	ldr	r3, [r7, #4]
 8003b1e:	4013      	ands	r3, r2
 8003b20:	60fb      	str	r3, [r7, #12]
 8003b22:	68fb      	ldr	r3, [r7, #12]
 8003b24:	bf00      	nop
 8003b26:	3714      	adds	r7, #20
 8003b28:	46bd      	mov	sp, r7
 8003b2a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003b2e:	4770      	bx	lr
 8003b30:	40023800 	.word	0x40023800

08003b34 <LL_TIM_EnableCounter>:
 8003b34:	b480      	push	{r7}
 8003b36:	b083      	sub	sp, #12
 8003b38:	af00      	add	r7, sp, #0
 8003b3a:	6078      	str	r0, [r7, #4]
 8003b3c:	687b      	ldr	r3, [r7, #4]
 8003b3e:	681b      	ldr	r3, [r3, #0]
 8003b40:	f043 0201 	orr.w	r2, r3, #1
 8003b44:	687b      	ldr	r3, [r7, #4]
 8003b46:	601a      	str	r2, [r3, #0]
 8003b48:	bf00      	nop
 8003b4a:	370c      	adds	r7, #12
 8003b4c:	46bd      	mov	sp, r7
 8003b4e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003b52:	4770      	bx	lr

08003b54 <LL_TIM_SetCounterMode>:
 8003b54:	b480      	push	{r7}
 8003b56:	b083      	sub	sp, #12
 8003b58:	af00      	add	r7, sp, #0
 8003b5a:	6078      	str	r0, [r7, #4]
 8003b5c:	6039      	str	r1, [r7, #0]
 8003b5e:	687b      	ldr	r3, [r7, #4]
 8003b60:	681b      	ldr	r3, [r3, #0]
 8003b62:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8003b66:	683b      	ldr	r3, [r7, #0]
 8003b68:	431a      	orrs	r2, r3
 8003b6a:	687b      	ldr	r3, [r7, #4]
 8003b6c:	601a      	str	r2, [r3, #0]
 8003b6e:	bf00      	nop
 8003b70:	370c      	adds	r7, #12
 8003b72:	46bd      	mov	sp, r7
 8003b74:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003b78:	4770      	bx	lr

08003b7a <LL_TIM_DisableARRPreload>:
 8003b7a:	b480      	push	{r7}
 8003b7c:	b083      	sub	sp, #12
 8003b7e:	af00      	add	r7, sp, #0
 8003b80:	6078      	str	r0, [r7, #4]
 8003b82:	687b      	ldr	r3, [r7, #4]
 8003b84:	681b      	ldr	r3, [r3, #0]
 8003b86:	f023 0280 	bic.w	r2, r3, #128	; 0x80
 8003b8a:	687b      	ldr	r3, [r7, #4]
 8003b8c:	601a      	str	r2, [r3, #0]
 8003b8e:	bf00      	nop
 8003b90:	370c      	adds	r7, #12
 8003b92:	46bd      	mov	sp, r7
 8003b94:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003b98:	4770      	bx	lr

08003b9a <LL_TIM_SetClockDivision>:
 8003b9a:	b480      	push	{r7}
 8003b9c:	b083      	sub	sp, #12
 8003b9e:	af00      	add	r7, sp, #0
 8003ba0:	6078      	str	r0, [r7, #4]
 8003ba2:	6039      	str	r1, [r7, #0]
 8003ba4:	687b      	ldr	r3, [r7, #4]
 8003ba6:	681b      	ldr	r3, [r3, #0]
 8003ba8:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8003bac:	683b      	ldr	r3, [r7, #0]
 8003bae:	431a      	orrs	r2, r3
 8003bb0:	687b      	ldr	r3, [r7, #4]
 8003bb2:	601a      	str	r2, [r3, #0]
 8003bb4:	bf00      	nop
 8003bb6:	370c      	adds	r7, #12
 8003bb8:	46bd      	mov	sp, r7
 8003bba:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003bbe:	4770      	bx	lr

08003bc0 <LL_TIM_SetPrescaler>:
 8003bc0:	b480      	push	{r7}
 8003bc2:	b083      	sub	sp, #12
 8003bc4:	af00      	add	r7, sp, #0
 8003bc6:	6078      	str	r0, [r7, #4]
 8003bc8:	6039      	str	r1, [r7, #0]
 8003bca:	687b      	ldr	r3, [r7, #4]
 8003bcc:	683a      	ldr	r2, [r7, #0]
 8003bce:	629a      	str	r2, [r3, #40]	; 0x28
 8003bd0:	bf00      	nop
 8003bd2:	370c      	adds	r7, #12
 8003bd4:	46bd      	mov	sp, r7
 8003bd6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003bda:	4770      	bx	lr

08003bdc <LL_TIM_SetAutoReload>:
 8003bdc:	b480      	push	{r7}
 8003bde:	b083      	sub	sp, #12
 8003be0:	af00      	add	r7, sp, #0
 8003be2:	6078      	str	r0, [r7, #4]
 8003be4:	6039      	str	r1, [r7, #0]
 8003be6:	687b      	ldr	r3, [r7, #4]
 8003be8:	683a      	ldr	r2, [r7, #0]
 8003bea:	62da      	str	r2, [r3, #44]	; 0x2c
 8003bec:	bf00      	nop
 8003bee:	370c      	adds	r7, #12
 8003bf0:	46bd      	mov	sp, r7
 8003bf2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003bf6:	4770      	bx	lr

08003bf8 <LL_TIM_IC_SetActiveInput>:
 8003bf8:	b4b0      	push	{r4, r5, r7}
 8003bfa:	b085      	sub	sp, #20
 8003bfc:	af00      	add	r7, sp, #0
 8003bfe:	60f8      	str	r0, [r7, #12]
 8003c00:	60b9      	str	r1, [r7, #8]
 8003c02:	607a      	str	r2, [r7, #4]
 8003c04:	68bb      	ldr	r3, [r7, #8]
 8003c06:	2b01      	cmp	r3, #1
 8003c08:	d01c      	beq.n	8003c44 <LL_TIM_IC_SetActiveInput+0x4c>
 8003c0a:	68bb      	ldr	r3, [r7, #8]
 8003c0c:	2b04      	cmp	r3, #4
 8003c0e:	d017      	beq.n	8003c40 <LL_TIM_IC_SetActiveInput+0x48>
 8003c10:	68bb      	ldr	r3, [r7, #8]
 8003c12:	2b10      	cmp	r3, #16
 8003c14:	d012      	beq.n	8003c3c <LL_TIM_IC_SetActiveInput+0x44>
 8003c16:	68bb      	ldr	r3, [r7, #8]
 8003c18:	2b40      	cmp	r3, #64	; 0x40
 8003c1a:	d00d      	beq.n	8003c38 <LL_TIM_IC_SetActiveInput+0x40>
 8003c1c:	68bb      	ldr	r3, [r7, #8]
 8003c1e:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003c22:	d007      	beq.n	8003c34 <LL_TIM_IC_SetActiveInput+0x3c>
 8003c24:	68bb      	ldr	r3, [r7, #8]
 8003c26:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8003c2a:	d101      	bne.n	8003c30 <LL_TIM_IC_SetActiveInput+0x38>
 8003c2c:	2305      	movs	r3, #5
 8003c2e:	e00a      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c30:	2306      	movs	r3, #6
 8003c32:	e008      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c34:	2304      	movs	r3, #4
 8003c36:	e006      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c38:	2303      	movs	r3, #3
 8003c3a:	e004      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c3c:	2302      	movs	r3, #2
 8003c3e:	e002      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c40:	2301      	movs	r3, #1
 8003c42:	e000      	b.n	8003c46 <LL_TIM_IC_SetActiveInput+0x4e>
 8003c44:	2300      	movs	r3, #0
 8003c46:	461d      	mov	r5, r3
 8003c48:	68fb      	ldr	r3, [r7, #12]
 8003c4a:	3318      	adds	r3, #24
 8003c4c:	461a      	mov	r2, r3
 8003c4e:	4629      	mov	r1, r5
 8003c50:	4b0c      	ldr	r3, [pc, #48]	; (8003c84 <LL_TIM_IC_SetActiveInput+0x8c>)
 8003c52:	5c5b      	ldrb	r3, [r3, r1]
 8003c54:	4413      	add	r3, r2
 8003c56:	461c      	mov	r4, r3
 8003c58:	6822      	ldr	r2, [r4, #0]
 8003c5a:	4629      	mov	r1, r5
 8003c5c:	4b0a      	ldr	r3, [pc, #40]	; (8003c88 <LL_TIM_IC_SetActiveInput+0x90>)
 8003c5e:	5c5b      	ldrb	r3, [r3, r1]
 8003c60:	4619      	mov	r1, r3
 8003c62:	2303      	movs	r3, #3
 8003c64:	408b      	lsls	r3, r1
 8003c66:	43db      	mvns	r3, r3
 8003c68:	401a      	ands	r2, r3
 8003c6a:	687b      	ldr	r3, [r7, #4]
 8003c6c:	0c1b      	lsrs	r3, r3, #16
 8003c6e:	4628      	mov	r0, r5
 8003c70:	4905      	ldr	r1, [pc, #20]	; (8003c88 <LL_TIM_IC_SetActiveInput+0x90>)
 8003c72:	5c09      	ldrb	r1, [r1, r0]
 8003c74:	408b      	lsls	r3, r1
 8003c76:	4313      	orrs	r3, r2
 8003c78:	6023      	str	r3, [r4, #0]
 8003c7a:	bf00      	nop
 8003c7c:	3714      	adds	r7, #20
 8003c7e:	46bd      	mov	sp, r7
 8003c80:	bcb0      	pop	{r4, r5, r7}
 8003c82:	4770      	bx	lr
 8003c84:	0800d138 	.word	0x0800d138
 8003c88:	0800d140 	.word	0x0800d140

08003c8c <LL_TIM_IC_SetPrescaler>:
 8003c8c:	b4b0      	push	{r4, r5, r7}
 8003c8e:	b085      	sub	sp, #20
 8003c90:	af00      	add	r7, sp, #0
 8003c92:	60f8      	str	r0, [r7, #12]
 8003c94:	60b9      	str	r1, [r7, #8]
 8003c96:	607a      	str	r2, [r7, #4]
 8003c98:	68bb      	ldr	r3, [r7, #8]
 8003c9a:	2b01      	cmp	r3, #1
 8003c9c:	d01c      	beq.n	8003cd8 <LL_TIM_IC_SetPrescaler+0x4c>
 8003c9e:	68bb      	ldr	r3, [r7, #8]
 8003ca0:	2b04      	cmp	r3, #4
 8003ca2:	d017      	beq.n	8003cd4 <LL_TIM_IC_SetPrescaler+0x48>
 8003ca4:	68bb      	ldr	r3, [r7, #8]
 8003ca6:	2b10      	cmp	r3, #16
 8003ca8:	d012      	beq.n	8003cd0 <LL_TIM_IC_SetPrescaler+0x44>
 8003caa:	68bb      	ldr	r3, [r7, #8]
 8003cac:	2b40      	cmp	r3, #64	; 0x40
 8003cae:	d00d      	beq.n	8003ccc <LL_TIM_IC_SetPrescaler+0x40>
 8003cb0:	68bb      	ldr	r3, [r7, #8]
 8003cb2:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003cb6:	d007      	beq.n	8003cc8 <LL_TIM_IC_SetPrescaler+0x3c>
 8003cb8:	68bb      	ldr	r3, [r7, #8]
 8003cba:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8003cbe:	d101      	bne.n	8003cc4 <LL_TIM_IC_SetPrescaler+0x38>
 8003cc0:	2305      	movs	r3, #5
 8003cc2:	e00a      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003cc4:	2306      	movs	r3, #6
 8003cc6:	e008      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003cc8:	2304      	movs	r3, #4
 8003cca:	e006      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003ccc:	2303      	movs	r3, #3
 8003cce:	e004      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003cd0:	2302      	movs	r3, #2
 8003cd2:	e002      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003cd4:	2301      	movs	r3, #1
 8003cd6:	e000      	b.n	8003cda <LL_TIM_IC_SetPrescaler+0x4e>
 8003cd8:	2300      	movs	r3, #0
 8003cda:	461d      	mov	r5, r3
 8003cdc:	68fb      	ldr	r3, [r7, #12]
 8003cde:	3318      	adds	r3, #24
 8003ce0:	461a      	mov	r2, r3
 8003ce2:	4629      	mov	r1, r5
 8003ce4:	4b0c      	ldr	r3, [pc, #48]	; (8003d18 <LL_TIM_IC_SetPrescaler+0x8c>)
 8003ce6:	5c5b      	ldrb	r3, [r3, r1]
 8003ce8:	4413      	add	r3, r2
 8003cea:	461c      	mov	r4, r3
 8003cec:	6822      	ldr	r2, [r4, #0]
 8003cee:	4629      	mov	r1, r5
 8003cf0:	4b0a      	ldr	r3, [pc, #40]	; (8003d1c <LL_TIM_IC_SetPrescaler+0x90>)
 8003cf2:	5c5b      	ldrb	r3, [r3, r1]
 8003cf4:	4619      	mov	r1, r3
 8003cf6:	230c      	movs	r3, #12
 8003cf8:	408b      	lsls	r3, r1
 8003cfa:	43db      	mvns	r3, r3
 8003cfc:	401a      	ands	r2, r3
 8003cfe:	687b      	ldr	r3, [r7, #4]
 8003d00:	0c1b      	lsrs	r3, r3, #16
 8003d02:	4628      	mov	r0, r5
 8003d04:	4905      	ldr	r1, [pc, #20]	; (8003d1c <LL_TIM_IC_SetPrescaler+0x90>)
 8003d06:	5c09      	ldrb	r1, [r1, r0]
 8003d08:	408b      	lsls	r3, r1
 8003d0a:	4313      	orrs	r3, r2
 8003d0c:	6023      	str	r3, [r4, #0]
 8003d0e:	bf00      	nop
 8003d10:	3714      	adds	r7, #20
 8003d12:	46bd      	mov	sp, r7
 8003d14:	bcb0      	pop	{r4, r5, r7}
 8003d16:	4770      	bx	lr
 8003d18:	0800d138 	.word	0x0800d138
 8003d1c:	0800d140 	.word	0x0800d140

08003d20 <LL_TIM_IC_SetFilter>:
 8003d20:	b4b0      	push	{r4, r5, r7}
 8003d22:	b085      	sub	sp, #20
 8003d24:	af00      	add	r7, sp, #0
 8003d26:	60f8      	str	r0, [r7, #12]
 8003d28:	60b9      	str	r1, [r7, #8]
 8003d2a:	607a      	str	r2, [r7, #4]
 8003d2c:	68bb      	ldr	r3, [r7, #8]
 8003d2e:	2b01      	cmp	r3, #1
 8003d30:	d01c      	beq.n	8003d6c <LL_TIM_IC_SetFilter+0x4c>
 8003d32:	68bb      	ldr	r3, [r7, #8]
 8003d34:	2b04      	cmp	r3, #4
 8003d36:	d017      	beq.n	8003d68 <LL_TIM_IC_SetFilter+0x48>
 8003d38:	68bb      	ldr	r3, [r7, #8]
 8003d3a:	2b10      	cmp	r3, #16
 8003d3c:	d012      	beq.n	8003d64 <LL_TIM_IC_SetFilter+0x44>
 8003d3e:	68bb      	ldr	r3, [r7, #8]
 8003d40:	2b40      	cmp	r3, #64	; 0x40
 8003d42:	d00d      	beq.n	8003d60 <LL_TIM_IC_SetFilter+0x40>
 8003d44:	68bb      	ldr	r3, [r7, #8]
 8003d46:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003d4a:	d007      	beq.n	8003d5c <LL_TIM_IC_SetFilter+0x3c>
 8003d4c:	68bb      	ldr	r3, [r7, #8]
 8003d4e:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8003d52:	d101      	bne.n	8003d58 <LL_TIM_IC_SetFilter+0x38>
 8003d54:	2305      	movs	r3, #5
 8003d56:	e00a      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d58:	2306      	movs	r3, #6
 8003d5a:	e008      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d5c:	2304      	movs	r3, #4
 8003d5e:	e006      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d60:	2303      	movs	r3, #3
 8003d62:	e004      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d64:	2302      	movs	r3, #2
 8003d66:	e002      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d68:	2301      	movs	r3, #1
 8003d6a:	e000      	b.n	8003d6e <LL_TIM_IC_SetFilter+0x4e>
 8003d6c:	2300      	movs	r3, #0
 8003d6e:	461d      	mov	r5, r3
 8003d70:	68fb      	ldr	r3, [r7, #12]
 8003d72:	3318      	adds	r3, #24
 8003d74:	461a      	mov	r2, r3
 8003d76:	4629      	mov	r1, r5
 8003d78:	4b0c      	ldr	r3, [pc, #48]	; (8003dac <LL_TIM_IC_SetFilter+0x8c>)
 8003d7a:	5c5b      	ldrb	r3, [r3, r1]
 8003d7c:	4413      	add	r3, r2
 8003d7e:	461c      	mov	r4, r3
 8003d80:	6822      	ldr	r2, [r4, #0]
 8003d82:	4629      	mov	r1, r5
 8003d84:	4b0a      	ldr	r3, [pc, #40]	; (8003db0 <LL_TIM_IC_SetFilter+0x90>)
 8003d86:	5c5b      	ldrb	r3, [r3, r1]
 8003d88:	4619      	mov	r1, r3
 8003d8a:	23f0      	movs	r3, #240	; 0xf0
 8003d8c:	408b      	lsls	r3, r1
 8003d8e:	43db      	mvns	r3, r3
 8003d90:	401a      	ands	r2, r3
 8003d92:	687b      	ldr	r3, [r7, #4]
 8003d94:	0c1b      	lsrs	r3, r3, #16
 8003d96:	4628      	mov	r0, r5
 8003d98:	4905      	ldr	r1, [pc, #20]	; (8003db0 <LL_TIM_IC_SetFilter+0x90>)
 8003d9a:	5c09      	ldrb	r1, [r1, r0]
 8003d9c:	408b      	lsls	r3, r1
 8003d9e:	4313      	orrs	r3, r2
 8003da0:	6023      	str	r3, [r4, #0]
 8003da2:	bf00      	nop
 8003da4:	3714      	adds	r7, #20
 8003da6:	46bd      	mov	sp, r7
 8003da8:	bcb0      	pop	{r4, r5, r7}
 8003daa:	4770      	bx	lr
 8003dac:	0800d138 	.word	0x0800d138
 8003db0:	0800d140 	.word	0x0800d140

08003db4 <LL_TIM_IC_SetPolarity>:
 8003db4:	b490      	push	{r4, r7}
 8003db6:	b084      	sub	sp, #16
 8003db8:	af00      	add	r7, sp, #0
 8003dba:	60f8      	str	r0, [r7, #12]
 8003dbc:	60b9      	str	r1, [r7, #8]
 8003dbe:	607a      	str	r2, [r7, #4]
 8003dc0:	68bb      	ldr	r3, [r7, #8]
 8003dc2:	2b01      	cmp	r3, #1
 8003dc4:	d01c      	beq.n	8003e00 <LL_TIM_IC_SetPolarity+0x4c>
 8003dc6:	68bb      	ldr	r3, [r7, #8]
 8003dc8:	2b04      	cmp	r3, #4
 8003dca:	d017      	beq.n	8003dfc <LL_TIM_IC_SetPolarity+0x48>
 8003dcc:	68bb      	ldr	r3, [r7, #8]
 8003dce:	2b10      	cmp	r3, #16
 8003dd0:	d012      	beq.n	8003df8 <LL_TIM_IC_SetPolarity+0x44>
 8003dd2:	68bb      	ldr	r3, [r7, #8]
 8003dd4:	2b40      	cmp	r3, #64	; 0x40
 8003dd6:	d00d      	beq.n	8003df4 <LL_TIM_IC_SetPolarity+0x40>
 8003dd8:	68bb      	ldr	r3, [r7, #8]
 8003dda:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003dde:	d007      	beq.n	8003df0 <LL_TIM_IC_SetPolarity+0x3c>
 8003de0:	68bb      	ldr	r3, [r7, #8]
 8003de2:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8003de6:	d101      	bne.n	8003dec <LL_TIM_IC_SetPolarity+0x38>
 8003de8:	2305      	movs	r3, #5
 8003dea:	e00a      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003dec:	2306      	movs	r3, #6
 8003dee:	e008      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003df0:	2304      	movs	r3, #4
 8003df2:	e006      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003df4:	2303      	movs	r3, #3
 8003df6:	e004      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003df8:	2302      	movs	r3, #2
 8003dfa:	e002      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003dfc:	2301      	movs	r3, #1
 8003dfe:	e000      	b.n	8003e02 <LL_TIM_IC_SetPolarity+0x4e>
 8003e00:	2300      	movs	r3, #0
 8003e02:	461c      	mov	r4, r3
 8003e04:	68fb      	ldr	r3, [r7, #12]
 8003e06:	6a1a      	ldr	r2, [r3, #32]
 8003e08:	4621      	mov	r1, r4
 8003e0a:	4b0a      	ldr	r3, [pc, #40]	; (8003e34 <LL_TIM_IC_SetPolarity+0x80>)
 8003e0c:	5c5b      	ldrb	r3, [r3, r1]
 8003e0e:	4619      	mov	r1, r3
 8003e10:	230a      	movs	r3, #10
 8003e12:	408b      	lsls	r3, r1
 8003e14:	43db      	mvns	r3, r3
 8003e16:	401a      	ands	r2, r3
 8003e18:	4621      	mov	r1, r4
 8003e1a:	4b06      	ldr	r3, [pc, #24]	; (8003e34 <LL_TIM_IC_SetPolarity+0x80>)
 8003e1c:	5c5b      	ldrb	r3, [r3, r1]
 8003e1e:	4619      	mov	r1, r3
 8003e20:	687b      	ldr	r3, [r7, #4]
 8003e22:	408b      	lsls	r3, r1
 8003e24:	431a      	orrs	r2, r3
 8003e26:	68fb      	ldr	r3, [r7, #12]
 8003e28:	621a      	str	r2, [r3, #32]
 8003e2a:	bf00      	nop
 8003e2c:	3710      	adds	r7, #16
 8003e2e:	46bd      	mov	sp, r7
 8003e30:	bc90      	pop	{r4, r7}
 8003e32:	4770      	bx	lr
 8003e34:	0800d148 	.word	0x0800d148

08003e38 <LL_TIM_SetEncoderMode>:
 8003e38:	b480      	push	{r7}
 8003e3a:	b083      	sub	sp, #12
 8003e3c:	af00      	add	r7, sp, #0
 8003e3e:	6078      	str	r0, [r7, #4]
 8003e40:	6039      	str	r1, [r7, #0]
 8003e42:	687b      	ldr	r3, [r7, #4]
 8003e44:	689b      	ldr	r3, [r3, #8]
 8003e46:	f023 0207 	bic.w	r2, r3, #7
 8003e4a:	683b      	ldr	r3, [r7, #0]
 8003e4c:	431a      	orrs	r2, r3
 8003e4e:	687b      	ldr	r3, [r7, #4]
 8003e50:	609a      	str	r2, [r3, #8]
 8003e52:	bf00      	nop
 8003e54:	370c      	adds	r7, #12
 8003e56:	46bd      	mov	sp, r7
 8003e58:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003e5c:	4770      	bx	lr

08003e5e <LL_TIM_SetTriggerOutput>:
 8003e5e:	b480      	push	{r7}
 8003e60:	b083      	sub	sp, #12
 8003e62:	af00      	add	r7, sp, #0
 8003e64:	6078      	str	r0, [r7, #4]
 8003e66:	6039      	str	r1, [r7, #0]
 8003e68:	687b      	ldr	r3, [r7, #4]
 8003e6a:	685b      	ldr	r3, [r3, #4]
 8003e6c:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8003e70:	683b      	ldr	r3, [r7, #0]
 8003e72:	431a      	orrs	r2, r3
 8003e74:	687b      	ldr	r3, [r7, #4]
 8003e76:	605a      	str	r2, [r3, #4]
 8003e78:	bf00      	nop
 8003e7a:	370c      	adds	r7, #12
 8003e7c:	46bd      	mov	sp, r7
 8003e7e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003e82:	4770      	bx	lr

08003e84 <LL_TIM_DisableMasterSlaveMode>:
 8003e84:	b480      	push	{r7}
 8003e86:	b083      	sub	sp, #12
 8003e88:	af00      	add	r7, sp, #0
 8003e8a:	6078      	str	r0, [r7, #4]
 8003e8c:	687b      	ldr	r3, [r7, #4]
 8003e8e:	689b      	ldr	r3, [r3, #8]
 8003e90:	f023 0280 	bic.w	r2, r3, #128	; 0x80
 8003e94:	687b      	ldr	r3, [r7, #4]
 8003e96:	609a      	str	r2, [r3, #8]
 8003e98:	bf00      	nop
 8003e9a:	370c      	adds	r7, #12
 8003e9c:	46bd      	mov	sp, r7
 8003e9e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ea2:	4770      	bx	lr

08003ea4 <LL_TIM_ClearFlag_UPDATE>:
 8003ea4:	b480      	push	{r7}
 8003ea6:	b083      	sub	sp, #12
 8003ea8:	af00      	add	r7, sp, #0
 8003eaa:	6078      	str	r0, [r7, #4]
 8003eac:	687b      	ldr	r3, [r7, #4]
 8003eae:	f06f 0201 	mvn.w	r2, #1
 8003eb2:	611a      	str	r2, [r3, #16]
 8003eb4:	bf00      	nop
 8003eb6:	370c      	adds	r7, #12
 8003eb8:	46bd      	mov	sp, r7
 8003eba:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ebe:	4770      	bx	lr

08003ec0 <LL_TIM_IsActiveFlag_UPDATE>:
 8003ec0:	b480      	push	{r7}
 8003ec2:	b083      	sub	sp, #12
 8003ec4:	af00      	add	r7, sp, #0
 8003ec6:	6078      	str	r0, [r7, #4]
 8003ec8:	687b      	ldr	r3, [r7, #4]
 8003eca:	691b      	ldr	r3, [r3, #16]
 8003ecc:	f003 0301 	and.w	r3, r3, #1
 8003ed0:	2b01      	cmp	r3, #1
 8003ed2:	bf0c      	ite	eq
 8003ed4:	2301      	moveq	r3, #1
 8003ed6:	2300      	movne	r3, #0
 8003ed8:	b2db      	uxtb	r3, r3
 8003eda:	4618      	mov	r0, r3
 8003edc:	370c      	adds	r7, #12
 8003ede:	46bd      	mov	sp, r7
 8003ee0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ee4:	4770      	bx	lr

08003ee6 <LL_TIM_EnableIT_UPDATE>:
 8003ee6:	b480      	push	{r7}
 8003ee8:	b083      	sub	sp, #12
 8003eea:	af00      	add	r7, sp, #0
 8003eec:	6078      	str	r0, [r7, #4]
 8003eee:	687b      	ldr	r3, [r7, #4]
 8003ef0:	68db      	ldr	r3, [r3, #12]
 8003ef2:	f043 0201 	orr.w	r2, r3, #1
 8003ef6:	687b      	ldr	r3, [r7, #4]
 8003ef8:	60da      	str	r2, [r3, #12]
 8003efa:	bf00      	nop
 8003efc:	370c      	adds	r7, #12
 8003efe:	46bd      	mov	sp, r7
 8003f00:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003f04:	4770      	bx	lr

08003f06 <LL_GPIO_SetPinMode>:
 8003f06:	b480      	push	{r7}
 8003f08:	b089      	sub	sp, #36	; 0x24
 8003f0a:	af00      	add	r7, sp, #0
 8003f0c:	60f8      	str	r0, [r7, #12]
 8003f0e:	60b9      	str	r1, [r7, #8]
 8003f10:	607a      	str	r2, [r7, #4]
 8003f12:	68fb      	ldr	r3, [r7, #12]
 8003f14:	681a      	ldr	r2, [r3, #0]
 8003f16:	68bb      	ldr	r3, [r7, #8]
 8003f18:	617b      	str	r3, [r7, #20]
 8003f1a:	697b      	ldr	r3, [r7, #20]
 8003f1c:	fa93 f3a3 	rbit	r3, r3
 8003f20:	613b      	str	r3, [r7, #16]
 8003f22:	693b      	ldr	r3, [r7, #16]
 8003f24:	fab3 f383 	clz	r3, r3
 8003f28:	005b      	lsls	r3, r3, #1
 8003f2a:	2103      	movs	r1, #3
 8003f2c:	fa01 f303 	lsl.w	r3, r1, r3
 8003f30:	43db      	mvns	r3, r3
 8003f32:	401a      	ands	r2, r3
 8003f34:	68bb      	ldr	r3, [r7, #8]
 8003f36:	61fb      	str	r3, [r7, #28]
 8003f38:	69fb      	ldr	r3, [r7, #28]
 8003f3a:	fa93 f3a3 	rbit	r3, r3
 8003f3e:	61bb      	str	r3, [r7, #24]
 8003f40:	69bb      	ldr	r3, [r7, #24]
 8003f42:	fab3 f383 	clz	r3, r3
 8003f46:	005b      	lsls	r3, r3, #1
 8003f48:	6879      	ldr	r1, [r7, #4]
 8003f4a:	fa01 f303 	lsl.w	r3, r1, r3
 8003f4e:	431a      	orrs	r2, r3
 8003f50:	68fb      	ldr	r3, [r7, #12]
 8003f52:	601a      	str	r2, [r3, #0]
 8003f54:	bf00      	nop
 8003f56:	3724      	adds	r7, #36	; 0x24
 8003f58:	46bd      	mov	sp, r7
 8003f5a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003f5e:	4770      	bx	lr

08003f60 <LL_GPIO_SetPinPull>:
 8003f60:	b480      	push	{r7}
 8003f62:	b089      	sub	sp, #36	; 0x24
 8003f64:	af00      	add	r7, sp, #0
 8003f66:	60f8      	str	r0, [r7, #12]
 8003f68:	60b9      	str	r1, [r7, #8]
 8003f6a:	607a      	str	r2, [r7, #4]
 8003f6c:	68fb      	ldr	r3, [r7, #12]
 8003f6e:	68da      	ldr	r2, [r3, #12]
 8003f70:	68bb      	ldr	r3, [r7, #8]
 8003f72:	617b      	str	r3, [r7, #20]
 8003f74:	697b      	ldr	r3, [r7, #20]
 8003f76:	fa93 f3a3 	rbit	r3, r3
 8003f7a:	613b      	str	r3, [r7, #16]
 8003f7c:	693b      	ldr	r3, [r7, #16]
 8003f7e:	fab3 f383 	clz	r3, r3
 8003f82:	005b      	lsls	r3, r3, #1
 8003f84:	2103      	movs	r1, #3
 8003f86:	fa01 f303 	lsl.w	r3, r1, r3
 8003f8a:	43db      	mvns	r3, r3
 8003f8c:	401a      	ands	r2, r3
 8003f8e:	68bb      	ldr	r3, [r7, #8]
 8003f90:	61fb      	str	r3, [r7, #28]
 8003f92:	69fb      	ldr	r3, [r7, #28]
 8003f94:	fa93 f3a3 	rbit	r3, r3
 8003f98:	61bb      	str	r3, [r7, #24]
 8003f9a:	69bb      	ldr	r3, [r7, #24]
 8003f9c:	fab3 f383 	clz	r3, r3
 8003fa0:	005b      	lsls	r3, r3, #1
 8003fa2:	6879      	ldr	r1, [r7, #4]
 8003fa4:	fa01 f303 	lsl.w	r3, r1, r3
 8003fa8:	431a      	orrs	r2, r3
 8003faa:	68fb      	ldr	r3, [r7, #12]
 8003fac:	60da      	str	r2, [r3, #12]
 8003fae:	bf00      	nop
 8003fb0:	3724      	adds	r7, #36	; 0x24
 8003fb2:	46bd      	mov	sp, r7
 8003fb4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003fb8:	4770      	bx	lr

08003fba <LL_GPIO_SetAFPin_0_7>:
 8003fba:	b480      	push	{r7}
 8003fbc:	b089      	sub	sp, #36	; 0x24
 8003fbe:	af00      	add	r7, sp, #0
 8003fc0:	60f8      	str	r0, [r7, #12]
 8003fc2:	60b9      	str	r1, [r7, #8]
 8003fc4:	607a      	str	r2, [r7, #4]
 8003fc6:	68fb      	ldr	r3, [r7, #12]
 8003fc8:	6a1a      	ldr	r2, [r3, #32]
 8003fca:	68bb      	ldr	r3, [r7, #8]
 8003fcc:	617b      	str	r3, [r7, #20]
 8003fce:	697b      	ldr	r3, [r7, #20]
 8003fd0:	fa93 f3a3 	rbit	r3, r3
 8003fd4:	613b      	str	r3, [r7, #16]
 8003fd6:	693b      	ldr	r3, [r7, #16]
 8003fd8:	fab3 f383 	clz	r3, r3
 8003fdc:	009b      	lsls	r3, r3, #2
 8003fde:	210f      	movs	r1, #15
 8003fe0:	fa01 f303 	lsl.w	r3, r1, r3
 8003fe4:	43db      	mvns	r3, r3
 8003fe6:	401a      	ands	r2, r3
 8003fe8:	68bb      	ldr	r3, [r7, #8]
 8003fea:	61fb      	str	r3, [r7, #28]
 8003fec:	69fb      	ldr	r3, [r7, #28]
 8003fee:	fa93 f3a3 	rbit	r3, r3
 8003ff2:	61bb      	str	r3, [r7, #24]
 8003ff4:	69bb      	ldr	r3, [r7, #24]
 8003ff6:	fab3 f383 	clz	r3, r3
 8003ffa:	009b      	lsls	r3, r3, #2
 8003ffc:	6879      	ldr	r1, [r7, #4]
 8003ffe:	fa01 f303 	lsl.w	r3, r1, r3
 8004002:	431a      	orrs	r2, r3
 8004004:	68fb      	ldr	r3, [r7, #12]
 8004006:	621a      	str	r2, [r3, #32]
 8004008:	bf00      	nop
 800400a:	3724      	adds	r7, #36	; 0x24
 800400c:	46bd      	mov	sp, r7
 800400e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004012:	4770      	bx	lr

08004014 <LL_GPIO_SetAFPin_8_15>:
 8004014:	b480      	push	{r7}
 8004016:	b089      	sub	sp, #36	; 0x24
 8004018:	af00      	add	r7, sp, #0
 800401a:	60f8      	str	r0, [r7, #12]
 800401c:	60b9      	str	r1, [r7, #8]
 800401e:	607a      	str	r2, [r7, #4]
 8004020:	68fb      	ldr	r3, [r7, #12]
 8004022:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 8004024:	68bb      	ldr	r3, [r7, #8]
 8004026:	0a1b      	lsrs	r3, r3, #8
 8004028:	617b      	str	r3, [r7, #20]
 800402a:	697b      	ldr	r3, [r7, #20]
 800402c:	fa93 f3a3 	rbit	r3, r3
 8004030:	613b      	str	r3, [r7, #16]
 8004032:	693b      	ldr	r3, [r7, #16]
 8004034:	fab3 f383 	clz	r3, r3
 8004038:	009b      	lsls	r3, r3, #2
 800403a:	210f      	movs	r1, #15
 800403c:	fa01 f303 	lsl.w	r3, r1, r3
 8004040:	43db      	mvns	r3, r3
 8004042:	401a      	ands	r2, r3
 8004044:	68bb      	ldr	r3, [r7, #8]
 8004046:	0a1b      	lsrs	r3, r3, #8
 8004048:	61fb      	str	r3, [r7, #28]
 800404a:	69fb      	ldr	r3, [r7, #28]
 800404c:	fa93 f3a3 	rbit	r3, r3
 8004050:	61bb      	str	r3, [r7, #24]
 8004052:	69bb      	ldr	r3, [r7, #24]
 8004054:	fab3 f383 	clz	r3, r3
 8004058:	009b      	lsls	r3, r3, #2
 800405a:	6879      	ldr	r1, [r7, #4]
 800405c:	fa01 f303 	lsl.w	r3, r1, r3
 8004060:	431a      	orrs	r2, r3
 8004062:	68fb      	ldr	r3, [r7, #12]
 8004064:	625a      	str	r2, [r3, #36]	; 0x24
 8004066:	bf00      	nop
 8004068:	3724      	adds	r7, #36	; 0x24
 800406a:	46bd      	mov	sp, r7
 800406c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004070:	4770      	bx	lr
	...

08004074 <normalize_angle>:
 8004074:	b480      	push	{r7}
 8004076:	b083      	sub	sp, #12
 8004078:	af00      	add	r7, sp, #0
 800407a:	ed87 0a01 	vstr	s0, [r7, #4]
 800407e:	edd7 7a01 	vldr	s15, [r7, #4]
 8004082:	ed9f 7a13 	vldr	s14, [pc, #76]	; 80040d0 <normalize_angle+0x5c>
 8004086:	eef4 7ac7 	vcmpe.f32	s15, s14
 800408a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800408e:	dd06      	ble.n	800409e <normalize_angle+0x2a>
 8004090:	edd7 7a01 	vldr	s15, [r7, #4]
 8004094:	ed9f 7a0e 	vldr	s14, [pc, #56]	; 80040d0 <normalize_angle+0x5c>
 8004098:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800409c:	e011      	b.n	80040c2 <normalize_angle+0x4e>
 800409e:	edd7 7a01 	vldr	s15, [r7, #4]
 80040a2:	ed9f 7a0c 	vldr	s14, [pc, #48]	; 80040d4 <normalize_angle+0x60>
 80040a6:	eef4 7ac7 	vcmpe.f32	s15, s14
 80040aa:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80040ae:	d506      	bpl.n	80040be <normalize_angle+0x4a>
 80040b0:	edd7 7a01 	vldr	s15, [r7, #4]
 80040b4:	ed9f 7a06 	vldr	s14, [pc, #24]	; 80040d0 <normalize_angle+0x5c>
 80040b8:	ee77 7a87 	vadd.f32	s15, s15, s14
 80040bc:	e001      	b.n	80040c2 <normalize_angle+0x4e>
 80040be:	edd7 7a01 	vldr	s15, [r7, #4]
 80040c2:	eeb0 0a67 	vmov.f32	s0, s15
 80040c6:	370c      	adds	r7, #12
 80040c8:	46bd      	mov	sp, r7
 80040ca:	f85d 7b04 	ldr.w	r7, [sp], #4
 80040ce:	4770      	bx	lr
 80040d0:	40c90fdb 	.word	0x40c90fdb
 80040d4:	c0c90fdb 	.word	0xc0c90fdb

080040d8 <odom_hw_config>:
 80040d8:	b580      	push	{r7, lr}
 80040da:	b082      	sub	sp, #8
 80040dc:	af00      	add	r7, sp, #0
 80040de:	6078      	str	r0, [r7, #4]
 80040e0:	2001      	movs	r0, #1
 80040e2:	f7ff fcdf 	bl	8003aa4 <LL_AHB1_GRP1_EnableClock>
 80040e6:	2002      	movs	r0, #2
 80040e8:	f7ff fcdc 	bl	8003aa4 <LL_AHB1_GRP1_EnableClock>
 80040ec:	2010      	movs	r0, #16
 80040ee:	f7ff fcd9 	bl	8003aa4 <LL_AHB1_GRP1_EnableClock>
 80040f2:	2001      	movs	r0, #1
 80040f4:	f7ff fd06 	bl	8003b04 <LL_APB2_GRP1_EnableClock>
 80040f8:	2001      	movs	r0, #1
 80040fa:	f7ff fceb 	bl	8003ad4 <LL_APB1_GRP1_EnableClock>
 80040fe:	2002      	movs	r0, #2
 8004100:	f7ff fce8 	bl	8003ad4 <LL_APB1_GRP1_EnableClock>
 8004104:	2010      	movs	r0, #16
 8004106:	f7ff fce5 	bl	8003ad4 <LL_APB1_GRP1_EnableClock>
 800410a:	2200      	movs	r2, #0
 800410c:	2108      	movs	r1, #8
 800410e:	48be      	ldr	r0, [pc, #760]	; (8004408 <odom_hw_config+0x330>)
 8004110:	f7ff fef9 	bl	8003f06 <LL_GPIO_SetPinMode>
 8004114:	2200      	movs	r2, #0
 8004116:	2104      	movs	r1, #4
 8004118:	48bb      	ldr	r0, [pc, #748]	; (8004408 <odom_hw_config+0x330>)
 800411a:	f7ff fef4 	bl	8003f06 <LL_GPIO_SetPinMode>
 800411e:	2202      	movs	r2, #2
 8004120:	2102      	movs	r1, #2
 8004122:	48b9      	ldr	r0, [pc, #740]	; (8004408 <odom_hw_config+0x330>)
 8004124:	f7ff feef 	bl	8003f06 <LL_GPIO_SetPinMode>
 8004128:	2201      	movs	r2, #1
 800412a:	2102      	movs	r1, #2
 800412c:	48b6      	ldr	r0, [pc, #728]	; (8004408 <odom_hw_config+0x330>)
 800412e:	f7ff ff17 	bl	8003f60 <LL_GPIO_SetPinPull>
 8004132:	2201      	movs	r2, #1
 8004134:	2102      	movs	r1, #2
 8004136:	48b4      	ldr	r0, [pc, #720]	; (8004408 <odom_hw_config+0x330>)
 8004138:	f7ff ff3f 	bl	8003fba <LL_GPIO_SetAFPin_0_7>
 800413c:	2202      	movs	r2, #2
 800413e:	2101      	movs	r1, #1
 8004140:	48b1      	ldr	r0, [pc, #708]	; (8004408 <odom_hw_config+0x330>)
 8004142:	f7ff fee0 	bl	8003f06 <LL_GPIO_SetPinMode>
 8004146:	2201      	movs	r2, #1
 8004148:	2101      	movs	r1, #1
 800414a:	48af      	ldr	r0, [pc, #700]	; (8004408 <odom_hw_config+0x330>)
 800414c:	f7ff ff35 	bl	8003fba <LL_GPIO_SetAFPin_0_7>
 8004150:	2201      	movs	r2, #1
 8004152:	2101      	movs	r1, #1
 8004154:	48ac      	ldr	r0, [pc, #688]	; (8004408 <odom_hw_config+0x330>)
 8004156:	f7ff ff03 	bl	8003f60 <LL_GPIO_SetPinPull>
 800415a:	2202      	movs	r2, #2
 800415c:	2180      	movs	r1, #128	; 0x80
 800415e:	48aa      	ldr	r0, [pc, #680]	; (8004408 <odom_hw_config+0x330>)
 8004160:	f7ff fed1 	bl	8003f06 <LL_GPIO_SetPinMode>
 8004164:	2202      	movs	r2, #2
 8004166:	2180      	movs	r1, #128	; 0x80
 8004168:	48a7      	ldr	r0, [pc, #668]	; (8004408 <odom_hw_config+0x330>)
 800416a:	f7ff ff26 	bl	8003fba <LL_GPIO_SetAFPin_0_7>
 800416e:	2201      	movs	r2, #1
 8004170:	2180      	movs	r1, #128	; 0x80
 8004172:	48a5      	ldr	r0, [pc, #660]	; (8004408 <odom_hw_config+0x330>)
 8004174:	f7ff fef4 	bl	8003f60 <LL_GPIO_SetPinPull>
 8004178:	2202      	movs	r2, #2
 800417a:	2140      	movs	r1, #64	; 0x40
 800417c:	48a2      	ldr	r0, [pc, #648]	; (8004408 <odom_hw_config+0x330>)
 800417e:	f7ff fec2 	bl	8003f06 <LL_GPIO_SetPinMode>
 8004182:	2201      	movs	r2, #1
 8004184:	2140      	movs	r1, #64	; 0x40
 8004186:	48a0      	ldr	r0, [pc, #640]	; (8004408 <odom_hw_config+0x330>)
 8004188:	f7ff feea 	bl	8003f60 <LL_GPIO_SetPinPull>
 800418c:	2202      	movs	r2, #2
 800418e:	2140      	movs	r1, #64	; 0x40
 8004190:	489d      	ldr	r0, [pc, #628]	; (8004408 <odom_hw_config+0x330>)
 8004192:	f7ff ff12 	bl	8003fba <LL_GPIO_SetAFPin_0_7>
 8004196:	2202      	movs	r2, #2
 8004198:	f44f 7100 	mov.w	r1, #512	; 0x200
 800419c:	489a      	ldr	r0, [pc, #616]	; (8004408 <odom_hw_config+0x330>)
 800419e:	f7ff feb2 	bl	8003f06 <LL_GPIO_SetPinMode>
 80041a2:	2201      	movs	r2, #1
 80041a4:	f44f 7100 	mov.w	r1, #512	; 0x200
 80041a8:	4897      	ldr	r0, [pc, #604]	; (8004408 <odom_hw_config+0x330>)
 80041aa:	f7ff ff33 	bl	8004014 <LL_GPIO_SetAFPin_8_15>
 80041ae:	2200      	movs	r2, #0
 80041b0:	f44f 7100 	mov.w	r1, #512	; 0x200
 80041b4:	4894      	ldr	r0, [pc, #592]	; (8004408 <odom_hw_config+0x330>)
 80041b6:	f7ff fed3 	bl	8003f60 <LL_GPIO_SetPinPull>
 80041ba:	2202      	movs	r2, #2
 80041bc:	f44f 7180 	mov.w	r1, #256	; 0x100
 80041c0:	4891      	ldr	r0, [pc, #580]	; (8004408 <odom_hw_config+0x330>)
 80041c2:	f7ff fea0 	bl	8003f06 <LL_GPIO_SetPinMode>
 80041c6:	2201      	movs	r2, #1
 80041c8:	f44f 7180 	mov.w	r1, #256	; 0x100
 80041cc:	488e      	ldr	r0, [pc, #568]	; (8004408 <odom_hw_config+0x330>)
 80041ce:	f7ff ff21 	bl	8004014 <LL_GPIO_SetAFPin_8_15>
 80041d2:	2200      	movs	r2, #0
 80041d4:	f44f 7180 	mov.w	r1, #256	; 0x100
 80041d8:	488b      	ldr	r0, [pc, #556]	; (8004408 <odom_hw_config+0x330>)
 80041da:	f7ff fec1 	bl	8003f60 <LL_GPIO_SetPinPull>
 80041de:	2103      	movs	r1, #3
 80041e0:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80041e4:	f7ff fe28 	bl	8003e38 <LL_TIM_SetEncoderMode>
 80041e8:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 80041ec:	2101      	movs	r1, #1
 80041ee:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80041f2:	f7ff fd01 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 80041f6:	2200      	movs	r2, #0
 80041f8:	2101      	movs	r1, #1
 80041fa:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80041fe:	f7ff fd45 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 8004202:	2200      	movs	r2, #0
 8004204:	2101      	movs	r1, #1
 8004206:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800420a:	f7ff fd89 	bl	8003d20 <LL_TIM_IC_SetFilter>
 800420e:	2200      	movs	r2, #0
 8004210:	2101      	movs	r1, #1
 8004212:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004216:	f7ff fdcd 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 800421a:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 800421e:	2110      	movs	r1, #16
 8004220:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004224:	f7ff fce8 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 8004228:	2200      	movs	r2, #0
 800422a:	2110      	movs	r1, #16
 800422c:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004230:	f7ff fd2c 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 8004234:	2200      	movs	r2, #0
 8004236:	2110      	movs	r1, #16
 8004238:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800423c:	f7ff fd70 	bl	8003d20 <LL_TIM_IC_SetFilter>
 8004240:	2200      	movs	r2, #0
 8004242:	2110      	movs	r1, #16
 8004244:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004248:	f7ff fdb4 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 800424c:	2100      	movs	r1, #0
 800424e:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004252:	f7ff fc7f 	bl	8003b54 <LL_TIM_SetCounterMode>
 8004256:	f64f 71ff 	movw	r1, #65535	; 0xffff
 800425a:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800425e:	f7ff fcbd 	bl	8003bdc <LL_TIM_SetAutoReload>
 8004262:	2100      	movs	r1, #0
 8004264:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004268:	f7ff fc97 	bl	8003b9a <LL_TIM_SetClockDivision>
 800426c:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004270:	f7ff fc83 	bl	8003b7a <LL_TIM_DisableARRPreload>
 8004274:	2100      	movs	r1, #0
 8004276:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800427a:	f7ff fdf0 	bl	8003e5e <LL_TIM_SetTriggerOutput>
 800427e:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8004282:	f7ff fdff 	bl	8003e84 <LL_TIM_DisableMasterSlaveMode>
 8004286:	687b      	ldr	r3, [r7, #4]
 8004288:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 800428a:	f247 5230 	movw	r2, #30000	; 0x7530
 800428e:	801a      	strh	r2, [r3, #0]
 8004290:	2103      	movs	r1, #3
 8004292:	485e      	ldr	r0, [pc, #376]	; (800440c <odom_hw_config+0x334>)
 8004294:	f7ff fdd0 	bl	8003e38 <LL_TIM_SetEncoderMode>
 8004298:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 800429c:	2101      	movs	r1, #1
 800429e:	485b      	ldr	r0, [pc, #364]	; (800440c <odom_hw_config+0x334>)
 80042a0:	f7ff fcaa 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 80042a4:	2200      	movs	r2, #0
 80042a6:	2101      	movs	r1, #1
 80042a8:	4858      	ldr	r0, [pc, #352]	; (800440c <odom_hw_config+0x334>)
 80042aa:	f7ff fcef 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 80042ae:	2200      	movs	r2, #0
 80042b0:	2101      	movs	r1, #1
 80042b2:	4856      	ldr	r0, [pc, #344]	; (800440c <odom_hw_config+0x334>)
 80042b4:	f7ff fd34 	bl	8003d20 <LL_TIM_IC_SetFilter>
 80042b8:	2200      	movs	r2, #0
 80042ba:	2101      	movs	r1, #1
 80042bc:	4853      	ldr	r0, [pc, #332]	; (800440c <odom_hw_config+0x334>)
 80042be:	f7ff fd79 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 80042c2:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 80042c6:	2110      	movs	r1, #16
 80042c8:	4850      	ldr	r0, [pc, #320]	; (800440c <odom_hw_config+0x334>)
 80042ca:	f7ff fc95 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 80042ce:	2200      	movs	r2, #0
 80042d0:	2110      	movs	r1, #16
 80042d2:	484e      	ldr	r0, [pc, #312]	; (800440c <odom_hw_config+0x334>)
 80042d4:	f7ff fcda 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 80042d8:	2200      	movs	r2, #0
 80042da:	2110      	movs	r1, #16
 80042dc:	484b      	ldr	r0, [pc, #300]	; (800440c <odom_hw_config+0x334>)
 80042de:	f7ff fd1f 	bl	8003d20 <LL_TIM_IC_SetFilter>
 80042e2:	2200      	movs	r2, #0
 80042e4:	2110      	movs	r1, #16
 80042e6:	4849      	ldr	r0, [pc, #292]	; (800440c <odom_hw_config+0x334>)
 80042e8:	f7ff fd64 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 80042ec:	2100      	movs	r1, #0
 80042ee:	4847      	ldr	r0, [pc, #284]	; (800440c <odom_hw_config+0x334>)
 80042f0:	f7ff fc30 	bl	8003b54 <LL_TIM_SetCounterMode>
 80042f4:	f64f 71ff 	movw	r1, #65535	; 0xffff
 80042f8:	4844      	ldr	r0, [pc, #272]	; (800440c <odom_hw_config+0x334>)
 80042fa:	f7ff fc6f 	bl	8003bdc <LL_TIM_SetAutoReload>
 80042fe:	2100      	movs	r1, #0
 8004300:	4842      	ldr	r0, [pc, #264]	; (800440c <odom_hw_config+0x334>)
 8004302:	f7ff fc4a 	bl	8003b9a <LL_TIM_SetClockDivision>
 8004306:	4841      	ldr	r0, [pc, #260]	; (800440c <odom_hw_config+0x334>)
 8004308:	f7ff fc37 	bl	8003b7a <LL_TIM_DisableARRPreload>
 800430c:	2100      	movs	r1, #0
 800430e:	483f      	ldr	r0, [pc, #252]	; (800440c <odom_hw_config+0x334>)
 8004310:	f7ff fda5 	bl	8003e5e <LL_TIM_SetTriggerOutput>
 8004314:	483d      	ldr	r0, [pc, #244]	; (800440c <odom_hw_config+0x334>)
 8004316:	f7ff fdb5 	bl	8003e84 <LL_TIM_DisableMasterSlaveMode>
 800431a:	687b      	ldr	r3, [r7, #4]
 800431c:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 800431e:	f247 5230 	movw	r2, #30000	; 0x7530
 8004322:	801a      	strh	r2, [r3, #0]
 8004324:	2103      	movs	r1, #3
 8004326:	483a      	ldr	r0, [pc, #232]	; (8004410 <odom_hw_config+0x338>)
 8004328:	f7ff fd86 	bl	8003e38 <LL_TIM_SetEncoderMode>
 800432c:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 8004330:	2101      	movs	r1, #1
 8004332:	4837      	ldr	r0, [pc, #220]	; (8004410 <odom_hw_config+0x338>)
 8004334:	f7ff fc60 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 8004338:	2200      	movs	r2, #0
 800433a:	2101      	movs	r1, #1
 800433c:	4834      	ldr	r0, [pc, #208]	; (8004410 <odom_hw_config+0x338>)
 800433e:	f7ff fca5 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 8004342:	2200      	movs	r2, #0
 8004344:	2101      	movs	r1, #1
 8004346:	4832      	ldr	r0, [pc, #200]	; (8004410 <odom_hw_config+0x338>)
 8004348:	f7ff fcea 	bl	8003d20 <LL_TIM_IC_SetFilter>
 800434c:	2200      	movs	r2, #0
 800434e:	2101      	movs	r1, #1
 8004350:	482f      	ldr	r0, [pc, #188]	; (8004410 <odom_hw_config+0x338>)
 8004352:	f7ff fd2f 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 8004356:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 800435a:	2110      	movs	r1, #16
 800435c:	482c      	ldr	r0, [pc, #176]	; (8004410 <odom_hw_config+0x338>)
 800435e:	f7ff fc4b 	bl	8003bf8 <LL_TIM_IC_SetActiveInput>
 8004362:	2200      	movs	r2, #0
 8004364:	2110      	movs	r1, #16
 8004366:	482a      	ldr	r0, [pc, #168]	; (8004410 <odom_hw_config+0x338>)
 8004368:	f7ff fc90 	bl	8003c8c <LL_TIM_IC_SetPrescaler>
 800436c:	2200      	movs	r2, #0
 800436e:	2110      	movs	r1, #16
 8004370:	4827      	ldr	r0, [pc, #156]	; (8004410 <odom_hw_config+0x338>)
 8004372:	f7ff fcd5 	bl	8003d20 <LL_TIM_IC_SetFilter>
 8004376:	2200      	movs	r2, #0
 8004378:	2110      	movs	r1, #16
 800437a:	4825      	ldr	r0, [pc, #148]	; (8004410 <odom_hw_config+0x338>)
 800437c:	f7ff fd1a 	bl	8003db4 <LL_TIM_IC_SetPolarity>
 8004380:	2100      	movs	r1, #0
 8004382:	4823      	ldr	r0, [pc, #140]	; (8004410 <odom_hw_config+0x338>)
 8004384:	f7ff fbe6 	bl	8003b54 <LL_TIM_SetCounterMode>
 8004388:	f64f 71ff 	movw	r1, #65535	; 0xffff
 800438c:	4820      	ldr	r0, [pc, #128]	; (8004410 <odom_hw_config+0x338>)
 800438e:	f7ff fc25 	bl	8003bdc <LL_TIM_SetAutoReload>
 8004392:	2100      	movs	r1, #0
 8004394:	481e      	ldr	r0, [pc, #120]	; (8004410 <odom_hw_config+0x338>)
 8004396:	f7ff fc00 	bl	8003b9a <LL_TIM_SetClockDivision>
 800439a:	481d      	ldr	r0, [pc, #116]	; (8004410 <odom_hw_config+0x338>)
 800439c:	f7ff fbed 	bl	8003b7a <LL_TIM_DisableARRPreload>
 80043a0:	2100      	movs	r1, #0
 80043a2:	481b      	ldr	r0, [pc, #108]	; (8004410 <odom_hw_config+0x338>)
 80043a4:	f7ff fd5b 	bl	8003e5e <LL_TIM_SetTriggerOutput>
 80043a8:	4819      	ldr	r0, [pc, #100]	; (8004410 <odom_hw_config+0x338>)
 80043aa:	f7ff fd6b 	bl	8003e84 <LL_TIM_DisableMasterSlaveMode>
 80043ae:	687b      	ldr	r3, [r7, #4]
 80043b0:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 80043b2:	f247 5230 	movw	r2, #30000	; 0x7530
 80043b6:	801a      	strh	r2, [r3, #0]
 80043b8:	f24a 410f 	movw	r1, #41999	; 0xa40f
 80043bc:	4815      	ldr	r0, [pc, #84]	; (8004414 <odom_hw_config+0x33c>)
 80043be:	f7ff fc0d 	bl	8003bdc <LL_TIM_SetAutoReload>
 80043c2:	2113      	movs	r1, #19
 80043c4:	4813      	ldr	r0, [pc, #76]	; (8004414 <odom_hw_config+0x33c>)
 80043c6:	f7ff fbfb 	bl	8003bc0 <LL_TIM_SetPrescaler>
 80043ca:	2100      	movs	r1, #0
 80043cc:	4811      	ldr	r0, [pc, #68]	; (8004414 <odom_hw_config+0x33c>)
 80043ce:	f7ff fbc1 	bl	8003b54 <LL_TIM_SetCounterMode>
 80043d2:	4810      	ldr	r0, [pc, #64]	; (8004414 <odom_hw_config+0x33c>)
 80043d4:	f7ff fd87 	bl	8003ee6 <LL_TIM_EnableIT_UPDATE>
 80043d8:	2107      	movs	r1, #7
 80043da:	2036      	movs	r0, #54	; 0x36
 80043dc:	f7ff fb38 	bl	8003a50 <NVIC_SetPriority>
 80043e0:	2036      	movs	r0, #54	; 0x36
 80043e2:	f7ff fb1b 	bl	8003a1c <NVIC_EnableIRQ>
 80043e6:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80043ea:	f7ff fba3 	bl	8003b34 <LL_TIM_EnableCounter>
 80043ee:	4807      	ldr	r0, [pc, #28]	; (800440c <odom_hw_config+0x334>)
 80043f0:	f7ff fba0 	bl	8003b34 <LL_TIM_EnableCounter>
 80043f4:	4806      	ldr	r0, [pc, #24]	; (8004410 <odom_hw_config+0x338>)
 80043f6:	f7ff fb9d 	bl	8003b34 <LL_TIM_EnableCounter>
 80043fa:	4806      	ldr	r0, [pc, #24]	; (8004414 <odom_hw_config+0x33c>)
 80043fc:	f7ff fb9a 	bl	8003b34 <LL_TIM_EnableCounter>
 8004400:	bf00      	nop
 8004402:	3708      	adds	r7, #8
 8004404:	46bd      	mov	sp, r7
 8004406:	bd80      	pop	{r7, pc}
 8004408:	40020000 	.word	0x40020000
 800440c:	40000400 	.word	0x40000400
 8004410:	40010000 	.word	0x40010000
 8004414:	40001000 	.word	0x40001000

08004418 <odom_calc_wheels_speeds>:
 8004418:	b480      	push	{r7}
 800441a:	b085      	sub	sp, #20
 800441c:	af00      	add	r7, sp, #0
 800441e:	6078      	str	r0, [r7, #4]
 8004420:	2300      	movs	r3, #0
 8004422:	60fb      	str	r3, [r7, #12]
 8004424:	2300      	movs	r3, #0
 8004426:	60fb      	str	r3, [r7, #12]
 8004428:	e039      	b.n	800449e <odom_calc_wheels_speeds+0x86>
 800442a:	687b      	ldr	r3, [r7, #4]
 800442c:	68fa      	ldr	r2, [r7, #12]
 800442e:	320e      	adds	r2, #14
 8004430:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 8004434:	881b      	ldrh	r3, [r3, #0]
 8004436:	f5a3 43ea 	sub.w	r3, r3, #29952	; 0x7500
 800443a:	3b30      	subs	r3, #48	; 0x30
 800443c:	b29b      	uxth	r3, r3
 800443e:	b219      	sxth	r1, r3
 8004440:	687a      	ldr	r2, [r7, #4]
 8004442:	68fb      	ldr	r3, [r7, #12]
 8004444:	3320      	adds	r3, #32
 8004446:	005b      	lsls	r3, r3, #1
 8004448:	4413      	add	r3, r2
 800444a:	460a      	mov	r2, r1
 800444c:	809a      	strh	r2, [r3, #4]
 800444e:	687b      	ldr	r3, [r7, #4]
 8004450:	68fa      	ldr	r2, [r7, #12]
 8004452:	320e      	adds	r2, #14
 8004454:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 8004458:	f247 5230 	movw	r2, #30000	; 0x7530
 800445c:	801a      	strh	r2, [r3, #0]
 800445e:	687a      	ldr	r2, [r7, #4]
 8004460:	68fb      	ldr	r3, [r7, #12]
 8004462:	3320      	adds	r3, #32
 8004464:	005b      	lsls	r3, r3, #1
 8004466:	4413      	add	r3, r2
 8004468:	f9b3 3004 	ldrsh.w	r3, [r3, #4]
 800446c:	ee07 3a90 	vmov	s15, r3
 8004470:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 8004474:	ee77 7aa7 	vadd.f32	s15, s15, s15
 8004478:	ed9f 7a0d 	vldr	s14, [pc, #52]	; 80044b0 <odom_calc_wheels_speeds+0x98>
 800447c:	ee27 7a87 	vmul.f32	s14, s15, s14
 8004480:	eddf 6a0c 	vldr	s13, [pc, #48]	; 80044b4 <odom_calc_wheels_speeds+0x9c>
 8004484:	eec7 7a26 	vdiv.f32	s15, s14, s13
 8004488:	687a      	ldr	r2, [r7, #4]
 800448a:	68fb      	ldr	r3, [r7, #12]
 800448c:	330a      	adds	r3, #10
 800448e:	009b      	lsls	r3, r3, #2
 8004490:	4413      	add	r3, r2
 8004492:	3304      	adds	r3, #4
 8004494:	edc3 7a00 	vstr	s15, [r3]
 8004498:	68fb      	ldr	r3, [r7, #12]
 800449a:	3301      	adds	r3, #1
 800449c:	60fb      	str	r3, [r7, #12]
 800449e:	68fb      	ldr	r3, [r7, #12]
 80044a0:	2b02      	cmp	r3, #2
 80044a2:	ddc2      	ble.n	800442a <odom_calc_wheels_speeds+0x12>
 80044a4:	bf00      	nop
 80044a6:	3714      	adds	r7, #20
 80044a8:	46bd      	mov	sp, r7
 80044aa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80044ae:	4770      	bx	lr
 80044b0:	40490fdb 	.word	0x40490fdb
 80044b4:	47e00000 	.word	0x47e00000

080044b8 <odom_calc_robot_speed>:
 80044b8:	b580      	push	{r7, lr}
 80044ba:	b082      	sub	sp, #8
 80044bc:	af00      	add	r7, sp, #0
 80044be:	6078      	str	r0, [r7, #4]
 80044c0:	687b      	ldr	r3, [r7, #4]
 80044c2:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80044c4:	4a12      	ldr	r2, [pc, #72]	; (8004510 <odom_calc_robot_speed+0x58>)
 80044c6:	6013      	str	r3, [r2, #0]
 80044c8:	687b      	ldr	r3, [r7, #4]
 80044ca:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 80044cc:	4a10      	ldr	r2, [pc, #64]	; (8004510 <odom_calc_robot_speed+0x58>)
 80044ce:	6053      	str	r3, [r2, #4]
 80044d0:	687b      	ldr	r3, [r7, #4]
 80044d2:	6b5b      	ldr	r3, [r3, #52]	; 0x34
 80044d4:	4a0e      	ldr	r2, [pc, #56]	; (8004510 <odom_calc_robot_speed+0x58>)
 80044d6:	6093      	str	r3, [r2, #8]
 80044d8:	4b0e      	ldr	r3, [pc, #56]	; (8004514 <odom_calc_robot_speed+0x5c>)
 80044da:	2203      	movs	r2, #3
 80044dc:	2103      	movs	r1, #3
 80044de:	480e      	ldr	r0, [pc, #56]	; (8004518 <odom_calc_robot_speed+0x60>)
 80044e0:	f005 feb0 	bl	800a244 <arm_mat_init_f32>
 80044e4:	4b0a      	ldr	r3, [pc, #40]	; (8004510 <odom_calc_robot_speed+0x58>)
 80044e6:	2201      	movs	r2, #1
 80044e8:	2103      	movs	r1, #3
 80044ea:	480c      	ldr	r0, [pc, #48]	; (800451c <odom_calc_robot_speed+0x64>)
 80044ec:	f005 feaa 	bl	800a244 <arm_mat_init_f32>
 80044f0:	687b      	ldr	r3, [r7, #4]
 80044f2:	3320      	adds	r3, #32
 80044f4:	2201      	movs	r2, #1
 80044f6:	2103      	movs	r1, #3
 80044f8:	4809      	ldr	r0, [pc, #36]	; (8004520 <odom_calc_robot_speed+0x68>)
 80044fa:	f005 fea3 	bl	800a244 <arm_mat_init_f32>
 80044fe:	4a08      	ldr	r2, [pc, #32]	; (8004520 <odom_calc_robot_speed+0x68>)
 8004500:	4906      	ldr	r1, [pc, #24]	; (800451c <odom_calc_robot_speed+0x64>)
 8004502:	4805      	ldr	r0, [pc, #20]	; (8004518 <odom_calc_robot_speed+0x60>)
 8004504:	f005 feb6 	bl	800a274 <arm_mat_mult_f32>
 8004508:	bf00      	nop
 800450a:	3708      	adds	r7, #8
 800450c:	46bd      	mov	sp, r7
 800450e:	bd80      	pop	{r7, pc}
 8004510:	200002f0 	.word	0x200002f0
 8004514:	20000054 	.word	0x20000054
 8004518:	200002fc 	.word	0x200002fc
 800451c:	20000304 	.word	0x20000304
 8004520:	2000030c 	.word	0x2000030c

08004524 <odom_calc_glob_params>:
 8004524:	b580      	push	{r7, lr}
 8004526:	b088      	sub	sp, #32
 8004528:	af00      	add	r7, sp, #0
 800452a:	6078      	str	r0, [r7, #4]
 800452c:	687b      	ldr	r3, [r7, #4]
 800452e:	ed93 7a04 	vldr	s14, [r3, #16]
 8004532:	687b      	ldr	r3, [r7, #4]
 8004534:	edd3 7a0a 	vldr	s15, [r3, #40]	; 0x28
 8004538:	ee77 7a27 	vadd.f32	s15, s14, s15
 800453c:	edc7 7a07 	vstr	s15, [r7, #28]
 8004540:	ed97 0a07 	vldr	s0, [r7, #28]
 8004544:	f7ff fd96 	bl	8004074 <normalize_angle>
 8004548:	ed87 0a07 	vstr	s0, [r7, #28]
 800454c:	ed97 0a07 	vldr	s0, [r7, #28]
 8004550:	f007 fedc 	bl	800c30c <cosf>
 8004554:	eef0 7a40 	vmov.f32	s15, s0
 8004558:	edc7 7a03 	vstr	s15, [r7, #12]
 800455c:	ed97 0a07 	vldr	s0, [r7, #28]
 8004560:	f007 ff1c 	bl	800c39c <sinf>
 8004564:	eef0 7a40 	vmov.f32	s15, s0
 8004568:	eef1 7a67 	vneg.f32	s15, s15
 800456c:	edc7 7a04 	vstr	s15, [r7, #16]
 8004570:	ed97 0a07 	vldr	s0, [r7, #28]
 8004574:	f007 ff12 	bl	800c39c <sinf>
 8004578:	eef0 7a40 	vmov.f32	s15, s0
 800457c:	edc7 7a05 	vstr	s15, [r7, #20]
 8004580:	ed97 0a07 	vldr	s0, [r7, #28]
 8004584:	f007 fec2 	bl	800c30c <cosf>
 8004588:	eef0 7a40 	vmov.f32	s15, s0
 800458c:	edc7 7a06 	vstr	s15, [r7, #24]
 8004590:	f107 030c 	add.w	r3, r7, #12
 8004594:	2202      	movs	r2, #2
 8004596:	2102      	movs	r1, #2
 8004598:	481b      	ldr	r0, [pc, #108]	; (8004608 <odom_calc_glob_params+0xe4>)
 800459a:	f005 fe53 	bl	800a244 <arm_mat_init_f32>
 800459e:	687b      	ldr	r3, [r7, #4]
 80045a0:	3314      	adds	r3, #20
 80045a2:	2201      	movs	r2, #1
 80045a4:	2102      	movs	r1, #2
 80045a6:	4819      	ldr	r0, [pc, #100]	; (800460c <odom_calc_glob_params+0xe8>)
 80045a8:	f005 fe4c 	bl	800a244 <arm_mat_init_f32>
 80045ac:	687b      	ldr	r3, [r7, #4]
 80045ae:	3320      	adds	r3, #32
 80045b0:	2201      	movs	r2, #1
 80045b2:	2102      	movs	r1, #2
 80045b4:	4816      	ldr	r0, [pc, #88]	; (8004610 <odom_calc_glob_params+0xec>)
 80045b6:	f005 fe45 	bl	800a244 <arm_mat_init_f32>
 80045ba:	4a14      	ldr	r2, [pc, #80]	; (800460c <odom_calc_glob_params+0xe8>)
 80045bc:	4914      	ldr	r1, [pc, #80]	; (8004610 <odom_calc_glob_params+0xec>)
 80045be:	4812      	ldr	r0, [pc, #72]	; (8004608 <odom_calc_glob_params+0xe4>)
 80045c0:	f005 fe58 	bl	800a274 <arm_mat_mult_f32>
 80045c4:	687b      	ldr	r3, [r7, #4]
 80045c6:	6a9a      	ldr	r2, [r3, #40]	; 0x28
 80045c8:	687b      	ldr	r3, [r7, #4]
 80045ca:	61da      	str	r2, [r3, #28]
 80045cc:	687b      	ldr	r3, [r7, #4]
 80045ce:	ed93 7a02 	vldr	s14, [r3, #8]
 80045d2:	687b      	ldr	r3, [r7, #4]
 80045d4:	edd3 7a05 	vldr	s15, [r3, #20]
 80045d8:	ee77 7a27 	vadd.f32	s15, s14, s15
 80045dc:	687b      	ldr	r3, [r7, #4]
 80045de:	edc3 7a02 	vstr	s15, [r3, #8]
 80045e2:	687b      	ldr	r3, [r7, #4]
 80045e4:	ed93 7a03 	vldr	s14, [r3, #12]
 80045e8:	687b      	ldr	r3, [r7, #4]
 80045ea:	edd3 7a06 	vldr	s15, [r3, #24]
 80045ee:	ee77 7a27 	vadd.f32	s15, s14, s15
 80045f2:	687b      	ldr	r3, [r7, #4]
 80045f4:	edc3 7a03 	vstr	s15, [r3, #12]
 80045f8:	687b      	ldr	r3, [r7, #4]
 80045fa:	69fa      	ldr	r2, [r7, #28]
 80045fc:	611a      	str	r2, [r3, #16]
 80045fe:	bf00      	nop
 8004600:	3720      	adds	r7, #32
 8004602:	46bd      	mov	sp, r7
 8004604:	bd80      	pop	{r7, pc}
 8004606:	bf00      	nop
 8004608:	20000314 	.word	0x20000314
 800460c:	2000031c 	.word	0x2000031c
 8004610:	20000324 	.word	0x20000324

08004614 <odometry>:
 8004614:	b580      	push	{r7, lr}
 8004616:	b096      	sub	sp, #88	; 0x58
 8004618:	af00      	add	r7, sp, #0
 800461a:	6078      	str	r0, [r7, #4]
 800461c:	f107 0308 	add.w	r3, r7, #8
 8004620:	2250      	movs	r2, #80	; 0x50
 8004622:	2100      	movs	r1, #0
 8004624:	4618      	mov	r0, r3
 8004626:	f006 f97f 	bl	800a928 <memset>
 800462a:	4b14      	ldr	r3, [pc, #80]	; (800467c <odometry+0x68>)
 800462c:	643b      	str	r3, [r7, #64]	; 0x40
 800462e:	4b14      	ldr	r3, [pc, #80]	; (8004680 <odometry+0x6c>)
 8004630:	647b      	str	r3, [r7, #68]	; 0x44
 8004632:	4b14      	ldr	r3, [pc, #80]	; (8004684 <odometry+0x70>)
 8004634:	64bb      	str	r3, [r7, #72]	; 0x48
 8004636:	f004 f9d5 	bl	80089e4 <xTaskGetCurrentTaskHandle>
 800463a:	4603      	mov	r3, r0
 800463c:	657b      	str	r3, [r7, #84]	; 0x54
 800463e:	4a12      	ldr	r2, [pc, #72]	; (8004688 <odometry+0x74>)
 8004640:	f107 0308 	add.w	r3, r7, #8
 8004644:	6013      	str	r3, [r2, #0]
 8004646:	f107 0308 	add.w	r3, r7, #8
 800464a:	4618      	mov	r0, r3
 800464c:	f7ff fd44 	bl	80040d8 <odom_hw_config>
 8004650:	f04f 31ff 	mov.w	r1, #4294967295
 8004654:	2001      	movs	r0, #1
 8004656:	f004 fba3 	bl	8008da0 <ulTaskNotifyTake>
 800465a:	f107 0308 	add.w	r3, r7, #8
 800465e:	4618      	mov	r0, r3
 8004660:	f7ff feda 	bl	8004418 <odom_calc_wheels_speeds>
 8004664:	f107 0308 	add.w	r3, r7, #8
 8004668:	4618      	mov	r0, r3
 800466a:	f7ff ff25 	bl	80044b8 <odom_calc_robot_speed>
 800466e:	f107 0308 	add.w	r3, r7, #8
 8004672:	4618      	mov	r0, r3
 8004674:	f7ff ff56 	bl	8004524 <odom_calc_glob_params>
 8004678:	e7ea      	b.n	8004650 <odometry+0x3c>
 800467a:	bf00      	nop
 800467c:	40000024 	.word	0x40000024
 8004680:	40000424 	.word	0x40000424
 8004684:	40010024 	.word	0x40010024
 8004688:	200002ec 	.word	0x200002ec

0800468c <cmd_get_speed>:
 800468c:	b580      	push	{r7, lr}
 800468e:	b082      	sub	sp, #8
 8004690:	af00      	add	r7, sp, #0
 8004692:	6078      	str	r0, [r7, #4]
 8004694:	4b0c      	ldr	r3, [pc, #48]	; (80046c8 <cmd_get_speed+0x3c>)
 8004696:	681b      	ldr	r3, [r3, #0]
 8004698:	2b00      	cmp	r3, #0
 800469a:	d009      	beq.n	80046b0 <cmd_get_speed+0x24>
 800469c:	4b0a      	ldr	r3, [pc, #40]	; (80046c8 <cmd_get_speed+0x3c>)
 800469e:	681b      	ldr	r3, [r3, #0]
 80046a0:	3320      	adds	r3, #32
 80046a2:	220c      	movs	r2, #12
 80046a4:	4619      	mov	r1, r3
 80046a6:	6878      	ldr	r0, [r7, #4]
 80046a8:	f006 f930 	bl	800a90c <memcpy>
 80046ac:	230c      	movs	r3, #12
 80046ae:	e006      	b.n	80046be <cmd_get_speed+0x32>
 80046b0:	bf00      	nop
 80046b2:	2203      	movs	r2, #3
 80046b4:	4905      	ldr	r1, [pc, #20]	; (80046cc <cmd_get_speed+0x40>)
 80046b6:	6878      	ldr	r0, [r7, #4]
 80046b8:	f006 f928 	bl	800a90c <memcpy>
 80046bc:	2303      	movs	r3, #3
 80046be:	4618      	mov	r0, r3
 80046c0:	3708      	adds	r7, #8
 80046c2:	46bd      	mov	sp, r7
 80046c4:	bd80      	pop	{r7, pc}
 80046c6:	bf00      	nop
 80046c8:	200002ec 	.word	0x200002ec
 80046cc:	0800d0e8 	.word	0x0800d0e8

080046d0 <cmd_set_coord>:
 80046d0:	b580      	push	{r7, lr}
 80046d2:	b084      	sub	sp, #16
 80046d4:	af00      	add	r7, sp, #0
 80046d6:	6078      	str	r0, [r7, #4]
 80046d8:	687b      	ldr	r3, [r7, #4]
 80046da:	60fb      	str	r3, [r7, #12]
 80046dc:	4b1f      	ldr	r3, [pc, #124]	; (800475c <cmd_set_coord+0x8c>)
 80046de:	681b      	ldr	r3, [r3, #0]
 80046e0:	2b00      	cmp	r3, #0
 80046e2:	d030      	beq.n	8004746 <cmd_set_coord+0x76>
 80046e4:	4b1d      	ldr	r3, [pc, #116]	; (800475c <cmd_set_coord+0x8c>)
 80046e6:	681a      	ldr	r2, [r3, #0]
 80046e8:	68fb      	ldr	r3, [r7, #12]
 80046ea:	7819      	ldrb	r1, [r3, #0]
 80046ec:	7858      	ldrb	r0, [r3, #1]
 80046ee:	0200      	lsls	r0, r0, #8
 80046f0:	4301      	orrs	r1, r0
 80046f2:	7898      	ldrb	r0, [r3, #2]
 80046f4:	0400      	lsls	r0, r0, #16
 80046f6:	4301      	orrs	r1, r0
 80046f8:	78db      	ldrb	r3, [r3, #3]
 80046fa:	061b      	lsls	r3, r3, #24
 80046fc:	430b      	orrs	r3, r1
 80046fe:	6093      	str	r3, [r2, #8]
 8004700:	4b16      	ldr	r3, [pc, #88]	; (800475c <cmd_set_coord+0x8c>)
 8004702:	681a      	ldr	r2, [r3, #0]
 8004704:	68fb      	ldr	r3, [r7, #12]
 8004706:	7919      	ldrb	r1, [r3, #4]
 8004708:	7958      	ldrb	r0, [r3, #5]
 800470a:	0200      	lsls	r0, r0, #8
 800470c:	4301      	orrs	r1, r0
 800470e:	7998      	ldrb	r0, [r3, #6]
 8004710:	0400      	lsls	r0, r0, #16
 8004712:	4301      	orrs	r1, r0
 8004714:	79db      	ldrb	r3, [r3, #7]
 8004716:	061b      	lsls	r3, r3, #24
 8004718:	430b      	orrs	r3, r1
 800471a:	60d3      	str	r3, [r2, #12]
 800471c:	4b0f      	ldr	r3, [pc, #60]	; (800475c <cmd_set_coord+0x8c>)
 800471e:	681a      	ldr	r2, [r3, #0]
 8004720:	68fb      	ldr	r3, [r7, #12]
 8004722:	7a19      	ldrb	r1, [r3, #8]
 8004724:	7a58      	ldrb	r0, [r3, #9]
 8004726:	0200      	lsls	r0, r0, #8
 8004728:	4301      	orrs	r1, r0
 800472a:	7a98      	ldrb	r0, [r3, #10]
 800472c:	0400      	lsls	r0, r0, #16
 800472e:	4301      	orrs	r1, r0
 8004730:	7adb      	ldrb	r3, [r3, #11]
 8004732:	061b      	lsls	r3, r3, #24
 8004734:	430b      	orrs	r3, r1
 8004736:	6113      	str	r3, [r2, #16]
 8004738:	2203      	movs	r2, #3
 800473a:	4909      	ldr	r1, [pc, #36]	; (8004760 <cmd_set_coord+0x90>)
 800473c:	6878      	ldr	r0, [r7, #4]
 800473e:	f006 f8e5 	bl	800a90c <memcpy>
 8004742:	2303      	movs	r3, #3
 8004744:	e006      	b.n	8004754 <cmd_set_coord+0x84>
 8004746:	bf00      	nop
 8004748:	2203      	movs	r2, #3
 800474a:	4906      	ldr	r1, [pc, #24]	; (8004764 <cmd_set_coord+0x94>)
 800474c:	6878      	ldr	r0, [r7, #4]
 800474e:	f006 f8dd 	bl	800a90c <memcpy>
 8004752:	2303      	movs	r3, #3
 8004754:	4618      	mov	r0, r3
 8004756:	3710      	adds	r7, #16
 8004758:	46bd      	mov	sp, r7
 800475a:	bd80      	pop	{r7, pc}
 800475c:	200002ec 	.word	0x200002ec
 8004760:	0800d0ec 	.word	0x0800d0ec
 8004764:	0800d0e8 	.word	0x0800d0e8

08004768 <cmd_get_coord>:
 8004768:	b580      	push	{r7, lr}
 800476a:	b082      	sub	sp, #8
 800476c:	af00      	add	r7, sp, #0
 800476e:	6078      	str	r0, [r7, #4]
 8004770:	4b0c      	ldr	r3, [pc, #48]	; (80047a4 <cmd_get_coord+0x3c>)
 8004772:	681b      	ldr	r3, [r3, #0]
 8004774:	2b00      	cmp	r3, #0
 8004776:	d009      	beq.n	800478c <cmd_get_coord+0x24>
 8004778:	4b0a      	ldr	r3, [pc, #40]	; (80047a4 <cmd_get_coord+0x3c>)
 800477a:	681b      	ldr	r3, [r3, #0]
 800477c:	3308      	adds	r3, #8
 800477e:	220c      	movs	r2, #12
 8004780:	4619      	mov	r1, r3
 8004782:	6878      	ldr	r0, [r7, #4]
 8004784:	f006 f8c2 	bl	800a90c <memcpy>
 8004788:	230c      	movs	r3, #12
 800478a:	e006      	b.n	800479a <cmd_get_coord+0x32>
 800478c:	bf00      	nop
 800478e:	2206      	movs	r2, #6
 8004790:	4905      	ldr	r1, [pc, #20]	; (80047a8 <cmd_get_coord+0x40>)
 8004792:	6878      	ldr	r0, [r7, #4]
 8004794:	f006 f8ba 	bl	800a90c <memcpy>
 8004798:	2306      	movs	r3, #6
 800479a:	4618      	mov	r0, r3
 800479c:	3708      	adds	r7, #8
 800479e:	46bd      	mov	sp, r7
 80047a0:	bd80      	pop	{r7, pc}
 80047a2:	bf00      	nop
 80047a4:	200002ec 	.word	0x200002ec
 80047a8:	0800d0f0 	.word	0x0800d0f0

080047ac <cmd_get_wheel_speed>:
 80047ac:	b580      	push	{r7, lr}
 80047ae:	b082      	sub	sp, #8
 80047b0:	af00      	add	r7, sp, #0
 80047b2:	6078      	str	r0, [r7, #4]
 80047b4:	4b0c      	ldr	r3, [pc, #48]	; (80047e8 <cmd_get_wheel_speed+0x3c>)
 80047b6:	681b      	ldr	r3, [r3, #0]
 80047b8:	2b00      	cmp	r3, #0
 80047ba:	d009      	beq.n	80047d0 <cmd_get_wheel_speed+0x24>
 80047bc:	4b0a      	ldr	r3, [pc, #40]	; (80047e8 <cmd_get_wheel_speed+0x3c>)
 80047be:	681b      	ldr	r3, [r3, #0]
 80047c0:	332c      	adds	r3, #44	; 0x2c
 80047c2:	220c      	movs	r2, #12
 80047c4:	4619      	mov	r1, r3
 80047c6:	6878      	ldr	r0, [r7, #4]
 80047c8:	f006 f8a0 	bl	800a90c <memcpy>
 80047cc:	230c      	movs	r3, #12
 80047ce:	e006      	b.n	80047de <cmd_get_wheel_speed+0x32>
 80047d0:	bf00      	nop
 80047d2:	2203      	movs	r2, #3
 80047d4:	4905      	ldr	r1, [pc, #20]	; (80047ec <cmd_get_wheel_speed+0x40>)
 80047d6:	6878      	ldr	r0, [r7, #4]
 80047d8:	f006 f898 	bl	800a90c <memcpy>
 80047dc:	2303      	movs	r3, #3
 80047de:	4618      	mov	r0, r3
 80047e0:	3708      	adds	r7, #8
 80047e2:	46bd      	mov	sp, r7
 80047e4:	bd80      	pop	{r7, pc}
 80047e6:	bf00      	nop
 80047e8:	200002ec 	.word	0x200002ec
 80047ec:	0800d0e8 	.word	0x0800d0e8

080047f0 <TIM6_DAC_IRQHandler>:
 80047f0:	b580      	push	{r7, lr}
 80047f2:	b082      	sub	sp, #8
 80047f4:	af00      	add	r7, sp, #0
 80047f6:	2300      	movs	r3, #0
 80047f8:	607b      	str	r3, [r7, #4]
 80047fa:	4819      	ldr	r0, [pc, #100]	; (8004860 <TIM6_DAC_IRQHandler+0x70>)
 80047fc:	f7ff fb60 	bl	8003ec0 <LL_TIM_IsActiveFlag_UPDATE>
 8004800:	4603      	mov	r3, r0
 8004802:	2b00      	cmp	r3, #0
 8004804:	d01c      	beq.n	8004840 <TIM6_DAC_IRQHandler+0x50>
 8004806:	4816      	ldr	r0, [pc, #88]	; (8004860 <TIM6_DAC_IRQHandler+0x70>)
 8004808:	f7ff fb4c 	bl	8003ea4 <LL_TIM_ClearFlag_UPDATE>
 800480c:	4b15      	ldr	r3, [pc, #84]	; (8004864 <TIM6_DAC_IRQHandler+0x74>)
 800480e:	681a      	ldr	r2, [r3, #0]
 8004810:	4b14      	ldr	r3, [pc, #80]	; (8004864 <TIM6_DAC_IRQHandler+0x74>)
 8004812:	681b      	ldr	r3, [r3, #0]
 8004814:	6812      	ldr	r2, [r2, #0]
 8004816:	605a      	str	r2, [r3, #4]
 8004818:	4b12      	ldr	r3, [pc, #72]	; (8004864 <TIM6_DAC_IRQHandler+0x74>)
 800481a:	681b      	ldr	r3, [r3, #0]
 800481c:	edd3 7a00 	vldr	s15, [r3]
 8004820:	4b10      	ldr	r3, [pc, #64]	; (8004864 <TIM6_DAC_IRQHandler+0x74>)
 8004822:	681b      	ldr	r3, [r3, #0]
 8004824:	ed9f 7a10 	vldr	s14, [pc, #64]	; 8004868 <TIM6_DAC_IRQHandler+0x78>
 8004828:	ee77 7a87 	vadd.f32	s15, s15, s14
 800482c:	edc3 7a00 	vstr	s15, [r3]
 8004830:	4b0c      	ldr	r3, [pc, #48]	; (8004864 <TIM6_DAC_IRQHandler+0x74>)
 8004832:	681b      	ldr	r3, [r3, #0]
 8004834:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8004836:	1d3a      	adds	r2, r7, #4
 8004838:	4611      	mov	r1, r2
 800483a:	4618      	mov	r0, r3
 800483c:	f004 fbb8 	bl	8008fb0 <vTaskNotifyGiveFromISR>
 8004840:	687b      	ldr	r3, [r7, #4]
 8004842:	2b00      	cmp	r3, #0
 8004844:	d007      	beq.n	8004856 <TIM6_DAC_IRQHandler+0x66>
 8004846:	4b09      	ldr	r3, [pc, #36]	; (800486c <TIM6_DAC_IRQHandler+0x7c>)
 8004848:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800484c:	601a      	str	r2, [r3, #0]
 800484e:	f3bf 8f4f 	dsb	sy
 8004852:	f3bf 8f6f 	isb	sy
 8004856:	bf00      	nop
 8004858:	3708      	adds	r7, #8
 800485a:	46bd      	mov	sp, r7
 800485c:	bd80      	pop	{r7, pc}
 800485e:	bf00      	nop
 8004860:	40001000 	.word	0x40001000
 8004864:	200002ec 	.word	0x200002ec
 8004868:	3c23d70a 	.word	0x3c23d70a
 800486c:	e000ed04 	.word	0xe000ed04

08004870 <vApplicationMallocFailedHook>:
 8004870:	b480      	push	{r7}
 8004872:	af00      	add	r7, sp, #0
 8004874:	bf00      	nop
 8004876:	46bd      	mov	sp, r7
 8004878:	f85d 7b04 	ldr.w	r7, [sp], #4
 800487c:	4770      	bx	lr

0800487e <vApplicationIdleHook>:
 800487e:	b480      	push	{r7}
 8004880:	af00      	add	r7, sp, #0
 8004882:	bf00      	nop
 8004884:	46bd      	mov	sp, r7
 8004886:	f85d 7b04 	ldr.w	r7, [sp], #4
 800488a:	4770      	bx	lr

0800488c <vApplicationTickHook>:
 800488c:	b480      	push	{r7}
 800488e:	af00      	add	r7, sp, #0
 8004890:	bf00      	nop
 8004892:	46bd      	mov	sp, r7
 8004894:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004898:	4770      	bx	lr

0800489a <vApplicationStackOverflowHook>:
 800489a:	b480      	push	{r7}
 800489c:	b085      	sub	sp, #20
 800489e:	af00      	add	r7, sp, #0
 80048a0:	6078      	str	r0, [r7, #4]
 80048a2:	6039      	str	r1, [r7, #0]
 80048a4:	f04f 0350 	mov.w	r3, #80	; 0x50
 80048a8:	f383 8811 	msr	BASEPRI, r3
 80048ac:	f3bf 8f6f 	isb	sy
 80048b0:	f3bf 8f4f 	dsb	sy
 80048b4:	60fb      	str	r3, [r7, #12]
 80048b6:	e7fe      	b.n	80048b6 <vApplicationStackOverflowHook+0x1c>

080048b8 <vApplicationGetIdleTaskMemory>:
 80048b8:	b480      	push	{r7}
 80048ba:	b085      	sub	sp, #20
 80048bc:	af00      	add	r7, sp, #0
 80048be:	60f8      	str	r0, [r7, #12]
 80048c0:	60b9      	str	r1, [r7, #8]
 80048c2:	607a      	str	r2, [r7, #4]
 80048c4:	68fb      	ldr	r3, [r7, #12]
 80048c6:	4a07      	ldr	r2, [pc, #28]	; (80048e4 <vApplicationGetIdleTaskMemory+0x2c>)
 80048c8:	601a      	str	r2, [r3, #0]
 80048ca:	68bb      	ldr	r3, [r7, #8]
 80048cc:	4a06      	ldr	r2, [pc, #24]	; (80048e8 <vApplicationGetIdleTaskMemory+0x30>)
 80048ce:	601a      	str	r2, [r3, #0]
 80048d0:	687b      	ldr	r3, [r7, #4]
 80048d2:	2282      	movs	r2, #130	; 0x82
 80048d4:	601a      	str	r2, [r3, #0]
 80048d6:	bf00      	nop
 80048d8:	3714      	adds	r7, #20
 80048da:	46bd      	mov	sp, r7
 80048dc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80048e0:	4770      	bx	lr
 80048e2:	bf00      	nop
 80048e4:	20018c80 	.word	0x20018c80
 80048e8:	20019100 	.word	0x20019100

080048ec <vApplicationGetTimerTaskMemory>:
 80048ec:	b480      	push	{r7}
 80048ee:	b085      	sub	sp, #20
 80048f0:	af00      	add	r7, sp, #0
 80048f2:	60f8      	str	r0, [r7, #12]
 80048f4:	60b9      	str	r1, [r7, #8]
 80048f6:	607a      	str	r2, [r7, #4]
 80048f8:	68fb      	ldr	r3, [r7, #12]
 80048fa:	4a07      	ldr	r2, [pc, #28]	; (8004918 <vApplicationGetTimerTaskMemory+0x2c>)
 80048fc:	601a      	str	r2, [r3, #0]
 80048fe:	68bb      	ldr	r3, [r7, #8]
 8004900:	4a06      	ldr	r2, [pc, #24]	; (800491c <vApplicationGetTimerTaskMemory+0x30>)
 8004902:	601a      	str	r2, [r3, #0]
 8004904:	687b      	ldr	r3, [r7, #4]
 8004906:	f44f 7282 	mov.w	r2, #260	; 0x104
 800490a:	601a      	str	r2, [r3, #0]
 800490c:	bf00      	nop
 800490e:	3714      	adds	r7, #20
 8004910:	46bd      	mov	sp, r7
 8004912:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004916:	4770      	bx	lr
 8004918:	20000330 	.word	0x20000330
 800491c:	200007b0 	.word	0x200007b0

08004920 <NVIC_EnableIRQ>:
 8004920:	b480      	push	{r7}
 8004922:	b083      	sub	sp, #12
 8004924:	af00      	add	r7, sp, #0
 8004926:	4603      	mov	r3, r0
 8004928:	71fb      	strb	r3, [r7, #7]
 800492a:	79fb      	ldrb	r3, [r7, #7]
 800492c:	f003 021f 	and.w	r2, r3, #31
 8004930:	4907      	ldr	r1, [pc, #28]	; (8004950 <NVIC_EnableIRQ+0x30>)
 8004932:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004936:	095b      	lsrs	r3, r3, #5
 8004938:	2001      	movs	r0, #1
 800493a:	fa00 f202 	lsl.w	r2, r0, r2
 800493e:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8004942:	bf00      	nop
 8004944:	370c      	adds	r7, #12
 8004946:	46bd      	mov	sp, r7
 8004948:	f85d 7b04 	ldr.w	r7, [sp], #4
 800494c:	4770      	bx	lr
 800494e:	bf00      	nop
 8004950:	e000e100 	.word	0xe000e100

08004954 <NVIC_SetPriority>:
 8004954:	b480      	push	{r7}
 8004956:	b083      	sub	sp, #12
 8004958:	af00      	add	r7, sp, #0
 800495a:	4603      	mov	r3, r0
 800495c:	6039      	str	r1, [r7, #0]
 800495e:	71fb      	strb	r3, [r7, #7]
 8004960:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004964:	2b00      	cmp	r3, #0
 8004966:	da0b      	bge.n	8004980 <NVIC_SetPriority+0x2c>
 8004968:	683b      	ldr	r3, [r7, #0]
 800496a:	b2da      	uxtb	r2, r3
 800496c:	490c      	ldr	r1, [pc, #48]	; (80049a0 <NVIC_SetPriority+0x4c>)
 800496e:	79fb      	ldrb	r3, [r7, #7]
 8004970:	f003 030f 	and.w	r3, r3, #15
 8004974:	3b04      	subs	r3, #4
 8004976:	0112      	lsls	r2, r2, #4
 8004978:	b2d2      	uxtb	r2, r2
 800497a:	440b      	add	r3, r1
 800497c:	761a      	strb	r2, [r3, #24]
 800497e:	e009      	b.n	8004994 <NVIC_SetPriority+0x40>
 8004980:	683b      	ldr	r3, [r7, #0]
 8004982:	b2da      	uxtb	r2, r3
 8004984:	4907      	ldr	r1, [pc, #28]	; (80049a4 <NVIC_SetPriority+0x50>)
 8004986:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800498a:	0112      	lsls	r2, r2, #4
 800498c:	b2d2      	uxtb	r2, r2
 800498e:	440b      	add	r3, r1
 8004990:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8004994:	bf00      	nop
 8004996:	370c      	adds	r7, #12
 8004998:	46bd      	mov	sp, r7
 800499a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800499e:	4770      	bx	lr
 80049a0:	e000ed00 	.word	0xe000ed00
 80049a4:	e000e100 	.word	0xe000e100

080049a8 <LL_GPIO_SetPinMode>:
 80049a8:	b480      	push	{r7}
 80049aa:	b089      	sub	sp, #36	; 0x24
 80049ac:	af00      	add	r7, sp, #0
 80049ae:	60f8      	str	r0, [r7, #12]
 80049b0:	60b9      	str	r1, [r7, #8]
 80049b2:	607a      	str	r2, [r7, #4]
 80049b4:	68fb      	ldr	r3, [r7, #12]
 80049b6:	681a      	ldr	r2, [r3, #0]
 80049b8:	68bb      	ldr	r3, [r7, #8]
 80049ba:	617b      	str	r3, [r7, #20]
 80049bc:	697b      	ldr	r3, [r7, #20]
 80049be:	fa93 f3a3 	rbit	r3, r3
 80049c2:	613b      	str	r3, [r7, #16]
 80049c4:	693b      	ldr	r3, [r7, #16]
 80049c6:	fab3 f383 	clz	r3, r3
 80049ca:	005b      	lsls	r3, r3, #1
 80049cc:	2103      	movs	r1, #3
 80049ce:	fa01 f303 	lsl.w	r3, r1, r3
 80049d2:	43db      	mvns	r3, r3
 80049d4:	401a      	ands	r2, r3
 80049d6:	68bb      	ldr	r3, [r7, #8]
 80049d8:	61fb      	str	r3, [r7, #28]
 80049da:	69fb      	ldr	r3, [r7, #28]
 80049dc:	fa93 f3a3 	rbit	r3, r3
 80049e0:	61bb      	str	r3, [r7, #24]
 80049e2:	69bb      	ldr	r3, [r7, #24]
 80049e4:	fab3 f383 	clz	r3, r3
 80049e8:	005b      	lsls	r3, r3, #1
 80049ea:	6879      	ldr	r1, [r7, #4]
 80049ec:	fa01 f303 	lsl.w	r3, r1, r3
 80049f0:	431a      	orrs	r2, r3
 80049f2:	68fb      	ldr	r3, [r7, #12]
 80049f4:	601a      	str	r2, [r3, #0]
 80049f6:	bf00      	nop
 80049f8:	3724      	adds	r7, #36	; 0x24
 80049fa:	46bd      	mov	sp, r7
 80049fc:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004a00:	4770      	bx	lr

08004a02 <LL_GPIO_SetPinOutputType>:
 8004a02:	b480      	push	{r7}
 8004a04:	b085      	sub	sp, #20
 8004a06:	af00      	add	r7, sp, #0
 8004a08:	60f8      	str	r0, [r7, #12]
 8004a0a:	60b9      	str	r1, [r7, #8]
 8004a0c:	607a      	str	r2, [r7, #4]
 8004a0e:	68fb      	ldr	r3, [r7, #12]
 8004a10:	685a      	ldr	r2, [r3, #4]
 8004a12:	68bb      	ldr	r3, [r7, #8]
 8004a14:	43db      	mvns	r3, r3
 8004a16:	401a      	ands	r2, r3
 8004a18:	68bb      	ldr	r3, [r7, #8]
 8004a1a:	6879      	ldr	r1, [r7, #4]
 8004a1c:	fb01 f303 	mul.w	r3, r1, r3
 8004a20:	431a      	orrs	r2, r3
 8004a22:	68fb      	ldr	r3, [r7, #12]
 8004a24:	605a      	str	r2, [r3, #4]
 8004a26:	bf00      	nop
 8004a28:	3714      	adds	r7, #20
 8004a2a:	46bd      	mov	sp, r7
 8004a2c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004a30:	4770      	bx	lr

08004a32 <LL_GPIO_SetPinSpeed>:
 8004a32:	b480      	push	{r7}
 8004a34:	b089      	sub	sp, #36	; 0x24
 8004a36:	af00      	add	r7, sp, #0
 8004a38:	60f8      	str	r0, [r7, #12]
 8004a3a:	60b9      	str	r1, [r7, #8]
 8004a3c:	607a      	str	r2, [r7, #4]
 8004a3e:	68fb      	ldr	r3, [r7, #12]
 8004a40:	689a      	ldr	r2, [r3, #8]
 8004a42:	68bb      	ldr	r3, [r7, #8]
 8004a44:	617b      	str	r3, [r7, #20]
 8004a46:	697b      	ldr	r3, [r7, #20]
 8004a48:	fa93 f3a3 	rbit	r3, r3
 8004a4c:	613b      	str	r3, [r7, #16]
 8004a4e:	693b      	ldr	r3, [r7, #16]
 8004a50:	fab3 f383 	clz	r3, r3
 8004a54:	005b      	lsls	r3, r3, #1
 8004a56:	2103      	movs	r1, #3
 8004a58:	fa01 f303 	lsl.w	r3, r1, r3
 8004a5c:	43db      	mvns	r3, r3
 8004a5e:	401a      	ands	r2, r3
 8004a60:	68bb      	ldr	r3, [r7, #8]
 8004a62:	61fb      	str	r3, [r7, #28]
 8004a64:	69fb      	ldr	r3, [r7, #28]
 8004a66:	fa93 f3a3 	rbit	r3, r3
 8004a6a:	61bb      	str	r3, [r7, #24]
 8004a6c:	69bb      	ldr	r3, [r7, #24]
 8004a6e:	fab3 f383 	clz	r3, r3
 8004a72:	005b      	lsls	r3, r3, #1
 8004a74:	6879      	ldr	r1, [r7, #4]
 8004a76:	fa01 f303 	lsl.w	r3, r1, r3
 8004a7a:	431a      	orrs	r2, r3
 8004a7c:	68fb      	ldr	r3, [r7, #12]
 8004a7e:	609a      	str	r2, [r3, #8]
 8004a80:	bf00      	nop
 8004a82:	3724      	adds	r7, #36	; 0x24
 8004a84:	46bd      	mov	sp, r7
 8004a86:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004a8a:	4770      	bx	lr

08004a8c <LL_GPIO_SetPinPull>:
 8004a8c:	b480      	push	{r7}
 8004a8e:	b089      	sub	sp, #36	; 0x24
 8004a90:	af00      	add	r7, sp, #0
 8004a92:	60f8      	str	r0, [r7, #12]
 8004a94:	60b9      	str	r1, [r7, #8]
 8004a96:	607a      	str	r2, [r7, #4]
 8004a98:	68fb      	ldr	r3, [r7, #12]
 8004a9a:	68da      	ldr	r2, [r3, #12]
 8004a9c:	68bb      	ldr	r3, [r7, #8]
 8004a9e:	617b      	str	r3, [r7, #20]
 8004aa0:	697b      	ldr	r3, [r7, #20]
 8004aa2:	fa93 f3a3 	rbit	r3, r3
 8004aa6:	613b      	str	r3, [r7, #16]
 8004aa8:	693b      	ldr	r3, [r7, #16]
 8004aaa:	fab3 f383 	clz	r3, r3
 8004aae:	005b      	lsls	r3, r3, #1
 8004ab0:	2103      	movs	r1, #3
 8004ab2:	fa01 f303 	lsl.w	r3, r1, r3
 8004ab6:	43db      	mvns	r3, r3
 8004ab8:	401a      	ands	r2, r3
 8004aba:	68bb      	ldr	r3, [r7, #8]
 8004abc:	61fb      	str	r3, [r7, #28]
 8004abe:	69fb      	ldr	r3, [r7, #28]
 8004ac0:	fa93 f3a3 	rbit	r3, r3
 8004ac4:	61bb      	str	r3, [r7, #24]
 8004ac6:	69bb      	ldr	r3, [r7, #24]
 8004ac8:	fab3 f383 	clz	r3, r3
 8004acc:	005b      	lsls	r3, r3, #1
 8004ace:	6879      	ldr	r1, [r7, #4]
 8004ad0:	fa01 f303 	lsl.w	r3, r1, r3
 8004ad4:	431a      	orrs	r2, r3
 8004ad6:	68fb      	ldr	r3, [r7, #12]
 8004ad8:	60da      	str	r2, [r3, #12]
 8004ada:	bf00      	nop
 8004adc:	3724      	adds	r7, #36	; 0x24
 8004ade:	46bd      	mov	sp, r7
 8004ae0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004ae4:	4770      	bx	lr

08004ae6 <LL_GPIO_SetAFPin_0_7>:
 8004ae6:	b480      	push	{r7}
 8004ae8:	b089      	sub	sp, #36	; 0x24
 8004aea:	af00      	add	r7, sp, #0
 8004aec:	60f8      	str	r0, [r7, #12]
 8004aee:	60b9      	str	r1, [r7, #8]
 8004af0:	607a      	str	r2, [r7, #4]
 8004af2:	68fb      	ldr	r3, [r7, #12]
 8004af4:	6a1a      	ldr	r2, [r3, #32]
 8004af6:	68bb      	ldr	r3, [r7, #8]
 8004af8:	617b      	str	r3, [r7, #20]
 8004afa:	697b      	ldr	r3, [r7, #20]
 8004afc:	fa93 f3a3 	rbit	r3, r3
 8004b00:	613b      	str	r3, [r7, #16]
 8004b02:	693b      	ldr	r3, [r7, #16]
 8004b04:	fab3 f383 	clz	r3, r3
 8004b08:	009b      	lsls	r3, r3, #2
 8004b0a:	210f      	movs	r1, #15
 8004b0c:	fa01 f303 	lsl.w	r3, r1, r3
 8004b10:	43db      	mvns	r3, r3
 8004b12:	401a      	ands	r2, r3
 8004b14:	68bb      	ldr	r3, [r7, #8]
 8004b16:	61fb      	str	r3, [r7, #28]
 8004b18:	69fb      	ldr	r3, [r7, #28]
 8004b1a:	fa93 f3a3 	rbit	r3, r3
 8004b1e:	61bb      	str	r3, [r7, #24]
 8004b20:	69bb      	ldr	r3, [r7, #24]
 8004b22:	fab3 f383 	clz	r3, r3
 8004b26:	009b      	lsls	r3, r3, #2
 8004b28:	6879      	ldr	r1, [r7, #4]
 8004b2a:	fa01 f303 	lsl.w	r3, r1, r3
 8004b2e:	431a      	orrs	r2, r3
 8004b30:	68fb      	ldr	r3, [r7, #12]
 8004b32:	621a      	str	r2, [r3, #32]
 8004b34:	bf00      	nop
 8004b36:	3724      	adds	r7, #36	; 0x24
 8004b38:	46bd      	mov	sp, r7
 8004b3a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004b3e:	4770      	bx	lr

08004b40 <LL_GPIO_SetAFPin_8_15>:
 8004b40:	b480      	push	{r7}
 8004b42:	b089      	sub	sp, #36	; 0x24
 8004b44:	af00      	add	r7, sp, #0
 8004b46:	60f8      	str	r0, [r7, #12]
 8004b48:	60b9      	str	r1, [r7, #8]
 8004b4a:	607a      	str	r2, [r7, #4]
 8004b4c:	68fb      	ldr	r3, [r7, #12]
 8004b4e:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 8004b50:	68bb      	ldr	r3, [r7, #8]
 8004b52:	0a1b      	lsrs	r3, r3, #8
 8004b54:	617b      	str	r3, [r7, #20]
 8004b56:	697b      	ldr	r3, [r7, #20]
 8004b58:	fa93 f3a3 	rbit	r3, r3
 8004b5c:	613b      	str	r3, [r7, #16]
 8004b5e:	693b      	ldr	r3, [r7, #16]
 8004b60:	fab3 f383 	clz	r3, r3
 8004b64:	009b      	lsls	r3, r3, #2
 8004b66:	210f      	movs	r1, #15
 8004b68:	fa01 f303 	lsl.w	r3, r1, r3
 8004b6c:	43db      	mvns	r3, r3
 8004b6e:	401a      	ands	r2, r3
 8004b70:	68bb      	ldr	r3, [r7, #8]
 8004b72:	0a1b      	lsrs	r3, r3, #8
 8004b74:	61fb      	str	r3, [r7, #28]
 8004b76:	69fb      	ldr	r3, [r7, #28]
 8004b78:	fa93 f3a3 	rbit	r3, r3
 8004b7c:	61bb      	str	r3, [r7, #24]
 8004b7e:	69bb      	ldr	r3, [r7, #24]
 8004b80:	fab3 f383 	clz	r3, r3
 8004b84:	009b      	lsls	r3, r3, #2
 8004b86:	6879      	ldr	r1, [r7, #4]
 8004b88:	fa01 f303 	lsl.w	r3, r1, r3
 8004b8c:	431a      	orrs	r2, r3
 8004b8e:	68fb      	ldr	r3, [r7, #12]
 8004b90:	625a      	str	r2, [r3, #36]	; 0x24
 8004b92:	bf00      	nop
 8004b94:	3724      	adds	r7, #36	; 0x24
 8004b96:	46bd      	mov	sp, r7
 8004b98:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004b9c:	4770      	bx	lr

08004b9e <LL_GPIO_ReadInputPort>:
 8004b9e:	b480      	push	{r7}
 8004ba0:	b083      	sub	sp, #12
 8004ba2:	af00      	add	r7, sp, #0
 8004ba4:	6078      	str	r0, [r7, #4]
 8004ba6:	687b      	ldr	r3, [r7, #4]
 8004ba8:	691b      	ldr	r3, [r3, #16]
 8004baa:	4618      	mov	r0, r3
 8004bac:	370c      	adds	r7, #12
 8004bae:	46bd      	mov	sp, r7
 8004bb0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004bb4:	4770      	bx	lr

08004bb6 <LL_GPIO_WriteOutputPort>:
 8004bb6:	b480      	push	{r7}
 8004bb8:	b083      	sub	sp, #12
 8004bba:	af00      	add	r7, sp, #0
 8004bbc:	6078      	str	r0, [r7, #4]
 8004bbe:	6039      	str	r1, [r7, #0]
 8004bc0:	687b      	ldr	r3, [r7, #4]
 8004bc2:	683a      	ldr	r2, [r7, #0]
 8004bc4:	615a      	str	r2, [r3, #20]
 8004bc6:	bf00      	nop
 8004bc8:	370c      	adds	r7, #12
 8004bca:	46bd      	mov	sp, r7
 8004bcc:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004bd0:	4770      	bx	lr
	...

08004bd4 <LL_AHB1_GRP1_EnableClock>:
 8004bd4:	b480      	push	{r7}
 8004bd6:	b085      	sub	sp, #20
 8004bd8:	af00      	add	r7, sp, #0
 8004bda:	6078      	str	r0, [r7, #4]
 8004bdc:	4b08      	ldr	r3, [pc, #32]	; (8004c00 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8004bde:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8004be0:	4907      	ldr	r1, [pc, #28]	; (8004c00 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8004be2:	687b      	ldr	r3, [r7, #4]
 8004be4:	4313      	orrs	r3, r2
 8004be6:	630b      	str	r3, [r1, #48]	; 0x30
 8004be8:	4b05      	ldr	r3, [pc, #20]	; (8004c00 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8004bea:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8004bec:	687b      	ldr	r3, [r7, #4]
 8004bee:	4013      	ands	r3, r2
 8004bf0:	60fb      	str	r3, [r7, #12]
 8004bf2:	68fb      	ldr	r3, [r7, #12]
 8004bf4:	bf00      	nop
 8004bf6:	3714      	adds	r7, #20
 8004bf8:	46bd      	mov	sp, r7
 8004bfa:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004bfe:	4770      	bx	lr
 8004c00:	40023800 	.word	0x40023800

08004c04 <LL_APB2_GRP1_EnableClock>:
 8004c04:	b480      	push	{r7}
 8004c06:	b085      	sub	sp, #20
 8004c08:	af00      	add	r7, sp, #0
 8004c0a:	6078      	str	r0, [r7, #4]
 8004c0c:	4b08      	ldr	r3, [pc, #32]	; (8004c30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8004c0e:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 8004c10:	4907      	ldr	r1, [pc, #28]	; (8004c30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8004c12:	687b      	ldr	r3, [r7, #4]
 8004c14:	4313      	orrs	r3, r2
 8004c16:	644b      	str	r3, [r1, #68]	; 0x44
 8004c18:	4b05      	ldr	r3, [pc, #20]	; (8004c30 <LL_APB2_GRP1_EnableClock+0x2c>)
 8004c1a:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 8004c1c:	687b      	ldr	r3, [r7, #4]
 8004c1e:	4013      	ands	r3, r2
 8004c20:	60fb      	str	r3, [r7, #12]
 8004c22:	68fb      	ldr	r3, [r7, #12]
 8004c24:	bf00      	nop
 8004c26:	3714      	adds	r7, #20
 8004c28:	46bd      	mov	sp, r7
 8004c2a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c2e:	4770      	bx	lr
 8004c30:	40023800 	.word	0x40023800

08004c34 <LL_TIM_EnableCounter>:
 8004c34:	b480      	push	{r7}
 8004c36:	b083      	sub	sp, #12
 8004c38:	af00      	add	r7, sp, #0
 8004c3a:	6078      	str	r0, [r7, #4]
 8004c3c:	687b      	ldr	r3, [r7, #4]
 8004c3e:	681b      	ldr	r3, [r3, #0]
 8004c40:	f043 0201 	orr.w	r2, r3, #1
 8004c44:	687b      	ldr	r3, [r7, #4]
 8004c46:	601a      	str	r2, [r3, #0]
 8004c48:	bf00      	nop
 8004c4a:	370c      	adds	r7, #12
 8004c4c:	46bd      	mov	sp, r7
 8004c4e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c52:	4770      	bx	lr

08004c54 <LL_TIM_DisableCounter>:
 8004c54:	b480      	push	{r7}
 8004c56:	b083      	sub	sp, #12
 8004c58:	af00      	add	r7, sp, #0
 8004c5a:	6078      	str	r0, [r7, #4]
 8004c5c:	687b      	ldr	r3, [r7, #4]
 8004c5e:	681b      	ldr	r3, [r3, #0]
 8004c60:	f023 0201 	bic.w	r2, r3, #1
 8004c64:	687b      	ldr	r3, [r7, #4]
 8004c66:	601a      	str	r2, [r3, #0]
 8004c68:	bf00      	nop
 8004c6a:	370c      	adds	r7, #12
 8004c6c:	46bd      	mov	sp, r7
 8004c6e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c72:	4770      	bx	lr

08004c74 <LL_TIM_EnableUpdateEvent>:
 8004c74:	b480      	push	{r7}
 8004c76:	b083      	sub	sp, #12
 8004c78:	af00      	add	r7, sp, #0
 8004c7a:	6078      	str	r0, [r7, #4]
 8004c7c:	687b      	ldr	r3, [r7, #4]
 8004c7e:	681b      	ldr	r3, [r3, #0]
 8004c80:	f023 0202 	bic.w	r2, r3, #2
 8004c84:	687b      	ldr	r3, [r7, #4]
 8004c86:	601a      	str	r2, [r3, #0]
 8004c88:	bf00      	nop
 8004c8a:	370c      	adds	r7, #12
 8004c8c:	46bd      	mov	sp, r7
 8004c8e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c92:	4770      	bx	lr

08004c94 <LL_TIM_SetUpdateSource>:
 8004c94:	b480      	push	{r7}
 8004c96:	b083      	sub	sp, #12
 8004c98:	af00      	add	r7, sp, #0
 8004c9a:	6078      	str	r0, [r7, #4]
 8004c9c:	6039      	str	r1, [r7, #0]
 8004c9e:	687b      	ldr	r3, [r7, #4]
 8004ca0:	681b      	ldr	r3, [r3, #0]
 8004ca2:	f023 0204 	bic.w	r2, r3, #4
 8004ca6:	683b      	ldr	r3, [r7, #0]
 8004ca8:	431a      	orrs	r2, r3
 8004caa:	687b      	ldr	r3, [r7, #4]
 8004cac:	601a      	str	r2, [r3, #0]
 8004cae:	bf00      	nop
 8004cb0:	370c      	adds	r7, #12
 8004cb2:	46bd      	mov	sp, r7
 8004cb4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004cb8:	4770      	bx	lr

08004cba <LL_TIM_SetCounterMode>:
 8004cba:	b480      	push	{r7}
 8004cbc:	b083      	sub	sp, #12
 8004cbe:	af00      	add	r7, sp, #0
 8004cc0:	6078      	str	r0, [r7, #4]
 8004cc2:	6039      	str	r1, [r7, #0]
 8004cc4:	687b      	ldr	r3, [r7, #4]
 8004cc6:	681b      	ldr	r3, [r3, #0]
 8004cc8:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8004ccc:	683b      	ldr	r3, [r7, #0]
 8004cce:	431a      	orrs	r2, r3
 8004cd0:	687b      	ldr	r3, [r7, #4]
 8004cd2:	601a      	str	r2, [r3, #0]
 8004cd4:	bf00      	nop
 8004cd6:	370c      	adds	r7, #12
 8004cd8:	46bd      	mov	sp, r7
 8004cda:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004cde:	4770      	bx	lr

08004ce0 <LL_TIM_EnableARRPreload>:
 8004ce0:	b480      	push	{r7}
 8004ce2:	b083      	sub	sp, #12
 8004ce4:	af00      	add	r7, sp, #0
 8004ce6:	6078      	str	r0, [r7, #4]
 8004ce8:	687b      	ldr	r3, [r7, #4]
 8004cea:	681b      	ldr	r3, [r3, #0]
 8004cec:	f043 0280 	orr.w	r2, r3, #128	; 0x80
 8004cf0:	687b      	ldr	r3, [r7, #4]
 8004cf2:	601a      	str	r2, [r3, #0]
 8004cf4:	bf00      	nop
 8004cf6:	370c      	adds	r7, #12
 8004cf8:	46bd      	mov	sp, r7
 8004cfa:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004cfe:	4770      	bx	lr

08004d00 <LL_TIM_SetClockDivision>:
 8004d00:	b480      	push	{r7}
 8004d02:	b083      	sub	sp, #12
 8004d04:	af00      	add	r7, sp, #0
 8004d06:	6078      	str	r0, [r7, #4]
 8004d08:	6039      	str	r1, [r7, #0]
 8004d0a:	687b      	ldr	r3, [r7, #4]
 8004d0c:	681b      	ldr	r3, [r3, #0]
 8004d0e:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8004d12:	683b      	ldr	r3, [r7, #0]
 8004d14:	431a      	orrs	r2, r3
 8004d16:	687b      	ldr	r3, [r7, #4]
 8004d18:	601a      	str	r2, [r3, #0]
 8004d1a:	bf00      	nop
 8004d1c:	370c      	adds	r7, #12
 8004d1e:	46bd      	mov	sp, r7
 8004d20:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d24:	4770      	bx	lr

08004d26 <LL_TIM_SetPrescaler>:
 8004d26:	b480      	push	{r7}
 8004d28:	b083      	sub	sp, #12
 8004d2a:	af00      	add	r7, sp, #0
 8004d2c:	6078      	str	r0, [r7, #4]
 8004d2e:	6039      	str	r1, [r7, #0]
 8004d30:	687b      	ldr	r3, [r7, #4]
 8004d32:	683a      	ldr	r2, [r7, #0]
 8004d34:	629a      	str	r2, [r3, #40]	; 0x28
 8004d36:	bf00      	nop
 8004d38:	370c      	adds	r7, #12
 8004d3a:	46bd      	mov	sp, r7
 8004d3c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d40:	4770      	bx	lr

08004d42 <LL_TIM_SetAutoReload>:
 8004d42:	b480      	push	{r7}
 8004d44:	b083      	sub	sp, #12
 8004d46:	af00      	add	r7, sp, #0
 8004d48:	6078      	str	r0, [r7, #4]
 8004d4a:	6039      	str	r1, [r7, #0]
 8004d4c:	687b      	ldr	r3, [r7, #4]
 8004d4e:	683a      	ldr	r2, [r7, #0]
 8004d50:	62da      	str	r2, [r3, #44]	; 0x2c
 8004d52:	bf00      	nop
 8004d54:	370c      	adds	r7, #12
 8004d56:	46bd      	mov	sp, r7
 8004d58:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d5c:	4770      	bx	lr

08004d5e <LL_TIM_CC_SetLockLevel>:
 8004d5e:	b480      	push	{r7}
 8004d60:	b083      	sub	sp, #12
 8004d62:	af00      	add	r7, sp, #0
 8004d64:	6078      	str	r0, [r7, #4]
 8004d66:	6039      	str	r1, [r7, #0]
 8004d68:	687b      	ldr	r3, [r7, #4]
 8004d6a:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 8004d6c:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8004d70:	683b      	ldr	r3, [r7, #0]
 8004d72:	431a      	orrs	r2, r3
 8004d74:	687b      	ldr	r3, [r7, #4]
 8004d76:	645a      	str	r2, [r3, #68]	; 0x44
 8004d78:	bf00      	nop
 8004d7a:	370c      	adds	r7, #12
 8004d7c:	46bd      	mov	sp, r7
 8004d7e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d82:	4770      	bx	lr

08004d84 <LL_TIM_CC_EnableChannel>:
 8004d84:	b480      	push	{r7}
 8004d86:	b083      	sub	sp, #12
 8004d88:	af00      	add	r7, sp, #0
 8004d8a:	6078      	str	r0, [r7, #4]
 8004d8c:	6039      	str	r1, [r7, #0]
 8004d8e:	687b      	ldr	r3, [r7, #4]
 8004d90:	6a1a      	ldr	r2, [r3, #32]
 8004d92:	683b      	ldr	r3, [r7, #0]
 8004d94:	431a      	orrs	r2, r3
 8004d96:	687b      	ldr	r3, [r7, #4]
 8004d98:	621a      	str	r2, [r3, #32]
 8004d9a:	bf00      	nop
 8004d9c:	370c      	adds	r7, #12
 8004d9e:	46bd      	mov	sp, r7
 8004da0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004da4:	4770      	bx	lr
	...

08004da8 <LL_TIM_OC_SetMode>:
 8004da8:	b4b0      	push	{r4, r5, r7}
 8004daa:	b085      	sub	sp, #20
 8004dac:	af00      	add	r7, sp, #0
 8004dae:	60f8      	str	r0, [r7, #12]
 8004db0:	60b9      	str	r1, [r7, #8]
 8004db2:	607a      	str	r2, [r7, #4]
 8004db4:	68bb      	ldr	r3, [r7, #8]
 8004db6:	2b01      	cmp	r3, #1
 8004db8:	d01c      	beq.n	8004df4 <LL_TIM_OC_SetMode+0x4c>
 8004dba:	68bb      	ldr	r3, [r7, #8]
 8004dbc:	2b04      	cmp	r3, #4
 8004dbe:	d017      	beq.n	8004df0 <LL_TIM_OC_SetMode+0x48>
 8004dc0:	68bb      	ldr	r3, [r7, #8]
 8004dc2:	2b10      	cmp	r3, #16
 8004dc4:	d012      	beq.n	8004dec <LL_TIM_OC_SetMode+0x44>
 8004dc6:	68bb      	ldr	r3, [r7, #8]
 8004dc8:	2b40      	cmp	r3, #64	; 0x40
 8004dca:	d00d      	beq.n	8004de8 <LL_TIM_OC_SetMode+0x40>
 8004dcc:	68bb      	ldr	r3, [r7, #8]
 8004dce:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8004dd2:	d007      	beq.n	8004de4 <LL_TIM_OC_SetMode+0x3c>
 8004dd4:	68bb      	ldr	r3, [r7, #8]
 8004dd6:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8004dda:	d101      	bne.n	8004de0 <LL_TIM_OC_SetMode+0x38>
 8004ddc:	2305      	movs	r3, #5
 8004dde:	e00a      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004de0:	2306      	movs	r3, #6
 8004de2:	e008      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004de4:	2304      	movs	r3, #4
 8004de6:	e006      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004de8:	2303      	movs	r3, #3
 8004dea:	e004      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004dec:	2302      	movs	r3, #2
 8004dee:	e002      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004df0:	2301      	movs	r3, #1
 8004df2:	e000      	b.n	8004df6 <LL_TIM_OC_SetMode+0x4e>
 8004df4:	2300      	movs	r3, #0
 8004df6:	461d      	mov	r5, r3
 8004df8:	68fb      	ldr	r3, [r7, #12]
 8004dfa:	3318      	adds	r3, #24
 8004dfc:	461a      	mov	r2, r3
 8004dfe:	4629      	mov	r1, r5
 8004e00:	4b0c      	ldr	r3, [pc, #48]	; (8004e34 <LL_TIM_OC_SetMode+0x8c>)
 8004e02:	5c5b      	ldrb	r3, [r3, r1]
 8004e04:	4413      	add	r3, r2
 8004e06:	461c      	mov	r4, r3
 8004e08:	6822      	ldr	r2, [r4, #0]
 8004e0a:	4629      	mov	r1, r5
 8004e0c:	4b0a      	ldr	r3, [pc, #40]	; (8004e38 <LL_TIM_OC_SetMode+0x90>)
 8004e0e:	5c5b      	ldrb	r3, [r3, r1]
 8004e10:	4619      	mov	r1, r3
 8004e12:	2373      	movs	r3, #115	; 0x73
 8004e14:	408b      	lsls	r3, r1
 8004e16:	43db      	mvns	r3, r3
 8004e18:	401a      	ands	r2, r3
 8004e1a:	4629      	mov	r1, r5
 8004e1c:	4b06      	ldr	r3, [pc, #24]	; (8004e38 <LL_TIM_OC_SetMode+0x90>)
 8004e1e:	5c5b      	ldrb	r3, [r3, r1]
 8004e20:	4619      	mov	r1, r3
 8004e22:	687b      	ldr	r3, [r7, #4]
 8004e24:	408b      	lsls	r3, r1
 8004e26:	4313      	orrs	r3, r2
 8004e28:	6023      	str	r3, [r4, #0]
 8004e2a:	bf00      	nop
 8004e2c:	3714      	adds	r7, #20
 8004e2e:	46bd      	mov	sp, r7
 8004e30:	bcb0      	pop	{r4, r5, r7}
 8004e32:	4770      	bx	lr
 8004e34:	0800d150 	.word	0x0800d150
 8004e38:	0800d158 	.word	0x0800d158

08004e3c <LL_TIM_OC_EnableFast>:
 8004e3c:	b4b0      	push	{r4, r5, r7}
 8004e3e:	b083      	sub	sp, #12
 8004e40:	af00      	add	r7, sp, #0
 8004e42:	6078      	str	r0, [r7, #4]
 8004e44:	6039      	str	r1, [r7, #0]
 8004e46:	683b      	ldr	r3, [r7, #0]
 8004e48:	2b01      	cmp	r3, #1
 8004e4a:	d01c      	beq.n	8004e86 <LL_TIM_OC_EnableFast+0x4a>
 8004e4c:	683b      	ldr	r3, [r7, #0]
 8004e4e:	2b04      	cmp	r3, #4
 8004e50:	d017      	beq.n	8004e82 <LL_TIM_OC_EnableFast+0x46>
 8004e52:	683b      	ldr	r3, [r7, #0]
 8004e54:	2b10      	cmp	r3, #16
 8004e56:	d012      	beq.n	8004e7e <LL_TIM_OC_EnableFast+0x42>
 8004e58:	683b      	ldr	r3, [r7, #0]
 8004e5a:	2b40      	cmp	r3, #64	; 0x40
 8004e5c:	d00d      	beq.n	8004e7a <LL_TIM_OC_EnableFast+0x3e>
 8004e5e:	683b      	ldr	r3, [r7, #0]
 8004e60:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8004e64:	d007      	beq.n	8004e76 <LL_TIM_OC_EnableFast+0x3a>
 8004e66:	683b      	ldr	r3, [r7, #0]
 8004e68:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8004e6c:	d101      	bne.n	8004e72 <LL_TIM_OC_EnableFast+0x36>
 8004e6e:	2305      	movs	r3, #5
 8004e70:	e00a      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e72:	2306      	movs	r3, #6
 8004e74:	e008      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e76:	2304      	movs	r3, #4
 8004e78:	e006      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e7a:	2303      	movs	r3, #3
 8004e7c:	e004      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e7e:	2302      	movs	r3, #2
 8004e80:	e002      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e82:	2301      	movs	r3, #1
 8004e84:	e000      	b.n	8004e88 <LL_TIM_OC_EnableFast+0x4c>
 8004e86:	2300      	movs	r3, #0
 8004e88:	461d      	mov	r5, r3
 8004e8a:	687b      	ldr	r3, [r7, #4]
 8004e8c:	3318      	adds	r3, #24
 8004e8e:	461a      	mov	r2, r3
 8004e90:	4629      	mov	r1, r5
 8004e92:	4b09      	ldr	r3, [pc, #36]	; (8004eb8 <LL_TIM_OC_EnableFast+0x7c>)
 8004e94:	5c5b      	ldrb	r3, [r3, r1]
 8004e96:	4413      	add	r3, r2
 8004e98:	461c      	mov	r4, r3
 8004e9a:	6822      	ldr	r2, [r4, #0]
 8004e9c:	4629      	mov	r1, r5
 8004e9e:	4b07      	ldr	r3, [pc, #28]	; (8004ebc <LL_TIM_OC_EnableFast+0x80>)
 8004ea0:	5c5b      	ldrb	r3, [r3, r1]
 8004ea2:	4619      	mov	r1, r3
 8004ea4:	2304      	movs	r3, #4
 8004ea6:	408b      	lsls	r3, r1
 8004ea8:	4313      	orrs	r3, r2
 8004eaa:	6023      	str	r3, [r4, #0]
 8004eac:	bf00      	nop
 8004eae:	370c      	adds	r7, #12
 8004eb0:	46bd      	mov	sp, r7
 8004eb2:	bcb0      	pop	{r4, r5, r7}
 8004eb4:	4770      	bx	lr
 8004eb6:	bf00      	nop
 8004eb8:	0800d150 	.word	0x0800d150
 8004ebc:	0800d158 	.word	0x0800d158

08004ec0 <LL_TIM_OC_EnablePreload>:
 8004ec0:	b4b0      	push	{r4, r5, r7}
 8004ec2:	b083      	sub	sp, #12
 8004ec4:	af00      	add	r7, sp, #0
 8004ec6:	6078      	str	r0, [r7, #4]
 8004ec8:	6039      	str	r1, [r7, #0]
 8004eca:	683b      	ldr	r3, [r7, #0]
 8004ecc:	2b01      	cmp	r3, #1
 8004ece:	d01c      	beq.n	8004f0a <LL_TIM_OC_EnablePreload+0x4a>
 8004ed0:	683b      	ldr	r3, [r7, #0]
 8004ed2:	2b04      	cmp	r3, #4
 8004ed4:	d017      	beq.n	8004f06 <LL_TIM_OC_EnablePreload+0x46>
 8004ed6:	683b      	ldr	r3, [r7, #0]
 8004ed8:	2b10      	cmp	r3, #16
 8004eda:	d012      	beq.n	8004f02 <LL_TIM_OC_EnablePreload+0x42>
 8004edc:	683b      	ldr	r3, [r7, #0]
 8004ede:	2b40      	cmp	r3, #64	; 0x40
 8004ee0:	d00d      	beq.n	8004efe <LL_TIM_OC_EnablePreload+0x3e>
 8004ee2:	683b      	ldr	r3, [r7, #0]
 8004ee4:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8004ee8:	d007      	beq.n	8004efa <LL_TIM_OC_EnablePreload+0x3a>
 8004eea:	683b      	ldr	r3, [r7, #0]
 8004eec:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8004ef0:	d101      	bne.n	8004ef6 <LL_TIM_OC_EnablePreload+0x36>
 8004ef2:	2305      	movs	r3, #5
 8004ef4:	e00a      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004ef6:	2306      	movs	r3, #6
 8004ef8:	e008      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004efa:	2304      	movs	r3, #4
 8004efc:	e006      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004efe:	2303      	movs	r3, #3
 8004f00:	e004      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004f02:	2302      	movs	r3, #2
 8004f04:	e002      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004f06:	2301      	movs	r3, #1
 8004f08:	e000      	b.n	8004f0c <LL_TIM_OC_EnablePreload+0x4c>
 8004f0a:	2300      	movs	r3, #0
 8004f0c:	461d      	mov	r5, r3
 8004f0e:	687b      	ldr	r3, [r7, #4]
 8004f10:	3318      	adds	r3, #24
 8004f12:	461a      	mov	r2, r3
 8004f14:	4629      	mov	r1, r5
 8004f16:	4b09      	ldr	r3, [pc, #36]	; (8004f3c <LL_TIM_OC_EnablePreload+0x7c>)
 8004f18:	5c5b      	ldrb	r3, [r3, r1]
 8004f1a:	4413      	add	r3, r2
 8004f1c:	461c      	mov	r4, r3
 8004f1e:	6822      	ldr	r2, [r4, #0]
 8004f20:	4629      	mov	r1, r5
 8004f22:	4b07      	ldr	r3, [pc, #28]	; (8004f40 <LL_TIM_OC_EnablePreload+0x80>)
 8004f24:	5c5b      	ldrb	r3, [r3, r1]
 8004f26:	4619      	mov	r1, r3
 8004f28:	2308      	movs	r3, #8
 8004f2a:	408b      	lsls	r3, r1
 8004f2c:	4313      	orrs	r3, r2
 8004f2e:	6023      	str	r3, [r4, #0]
 8004f30:	bf00      	nop
 8004f32:	370c      	adds	r7, #12
 8004f34:	46bd      	mov	sp, r7
 8004f36:	bcb0      	pop	{r4, r5, r7}
 8004f38:	4770      	bx	lr
 8004f3a:	bf00      	nop
 8004f3c:	0800d150 	.word	0x0800d150
 8004f40:	0800d158 	.word	0x0800d158

08004f44 <LL_TIM_OC_EnableClear>:
 8004f44:	b4b0      	push	{r4, r5, r7}
 8004f46:	b083      	sub	sp, #12
 8004f48:	af00      	add	r7, sp, #0
 8004f4a:	6078      	str	r0, [r7, #4]
 8004f4c:	6039      	str	r1, [r7, #0]
 8004f4e:	683b      	ldr	r3, [r7, #0]
 8004f50:	2b01      	cmp	r3, #1
 8004f52:	d01c      	beq.n	8004f8e <LL_TIM_OC_EnableClear+0x4a>
 8004f54:	683b      	ldr	r3, [r7, #0]
 8004f56:	2b04      	cmp	r3, #4
 8004f58:	d017      	beq.n	8004f8a <LL_TIM_OC_EnableClear+0x46>
 8004f5a:	683b      	ldr	r3, [r7, #0]
 8004f5c:	2b10      	cmp	r3, #16
 8004f5e:	d012      	beq.n	8004f86 <LL_TIM_OC_EnableClear+0x42>
 8004f60:	683b      	ldr	r3, [r7, #0]
 8004f62:	2b40      	cmp	r3, #64	; 0x40
 8004f64:	d00d      	beq.n	8004f82 <LL_TIM_OC_EnableClear+0x3e>
 8004f66:	683b      	ldr	r3, [r7, #0]
 8004f68:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8004f6c:	d007      	beq.n	8004f7e <LL_TIM_OC_EnableClear+0x3a>
 8004f6e:	683b      	ldr	r3, [r7, #0]
 8004f70:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8004f74:	d101      	bne.n	8004f7a <LL_TIM_OC_EnableClear+0x36>
 8004f76:	2305      	movs	r3, #5
 8004f78:	e00a      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f7a:	2306      	movs	r3, #6
 8004f7c:	e008      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f7e:	2304      	movs	r3, #4
 8004f80:	e006      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f82:	2303      	movs	r3, #3
 8004f84:	e004      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f86:	2302      	movs	r3, #2
 8004f88:	e002      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f8a:	2301      	movs	r3, #1
 8004f8c:	e000      	b.n	8004f90 <LL_TIM_OC_EnableClear+0x4c>
 8004f8e:	2300      	movs	r3, #0
 8004f90:	461d      	mov	r5, r3
 8004f92:	687b      	ldr	r3, [r7, #4]
 8004f94:	3318      	adds	r3, #24
 8004f96:	461a      	mov	r2, r3
 8004f98:	4629      	mov	r1, r5
 8004f9a:	4b09      	ldr	r3, [pc, #36]	; (8004fc0 <LL_TIM_OC_EnableClear+0x7c>)
 8004f9c:	5c5b      	ldrb	r3, [r3, r1]
 8004f9e:	4413      	add	r3, r2
 8004fa0:	461c      	mov	r4, r3
 8004fa2:	6822      	ldr	r2, [r4, #0]
 8004fa4:	4629      	mov	r1, r5
 8004fa6:	4b07      	ldr	r3, [pc, #28]	; (8004fc4 <LL_TIM_OC_EnableClear+0x80>)
 8004fa8:	5c5b      	ldrb	r3, [r3, r1]
 8004faa:	4619      	mov	r1, r3
 8004fac:	2380      	movs	r3, #128	; 0x80
 8004fae:	408b      	lsls	r3, r1
 8004fb0:	4313      	orrs	r3, r2
 8004fb2:	6023      	str	r3, [r4, #0]
 8004fb4:	bf00      	nop
 8004fb6:	370c      	adds	r7, #12
 8004fb8:	46bd      	mov	sp, r7
 8004fba:	bcb0      	pop	{r4, r5, r7}
 8004fbc:	4770      	bx	lr
 8004fbe:	bf00      	nop
 8004fc0:	0800d150 	.word	0x0800d150
 8004fc4:	0800d158 	.word	0x0800d158

08004fc8 <LL_TIM_OC_SetDeadTime>:
 8004fc8:	b480      	push	{r7}
 8004fca:	b083      	sub	sp, #12
 8004fcc:	af00      	add	r7, sp, #0
 8004fce:	6078      	str	r0, [r7, #4]
 8004fd0:	6039      	str	r1, [r7, #0]
 8004fd2:	687b      	ldr	r3, [r7, #4]
 8004fd4:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 8004fd6:	f023 02ff 	bic.w	r2, r3, #255	; 0xff
 8004fda:	683b      	ldr	r3, [r7, #0]
 8004fdc:	431a      	orrs	r2, r3
 8004fde:	687b      	ldr	r3, [r7, #4]
 8004fe0:	645a      	str	r2, [r3, #68]	; 0x44
 8004fe2:	bf00      	nop
 8004fe4:	370c      	adds	r7, #12
 8004fe6:	46bd      	mov	sp, r7
 8004fe8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004fec:	4770      	bx	lr

08004fee <LL_TIM_OC_SetCompareCH1>:
 8004fee:	b480      	push	{r7}
 8004ff0:	b083      	sub	sp, #12
 8004ff2:	af00      	add	r7, sp, #0
 8004ff4:	6078      	str	r0, [r7, #4]
 8004ff6:	6039      	str	r1, [r7, #0]
 8004ff8:	687b      	ldr	r3, [r7, #4]
 8004ffa:	683a      	ldr	r2, [r7, #0]
 8004ffc:	635a      	str	r2, [r3, #52]	; 0x34
 8004ffe:	bf00      	nop
 8005000:	370c      	adds	r7, #12
 8005002:	46bd      	mov	sp, r7
 8005004:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005008:	4770      	bx	lr

0800500a <LL_TIM_DisableMasterSlaveMode>:
 800500a:	b480      	push	{r7}
 800500c:	b083      	sub	sp, #12
 800500e:	af00      	add	r7, sp, #0
 8005010:	6078      	str	r0, [r7, #4]
 8005012:	687b      	ldr	r3, [r7, #4]
 8005014:	689b      	ldr	r3, [r3, #8]
 8005016:	f023 0280 	bic.w	r2, r3, #128	; 0x80
 800501a:	687b      	ldr	r3, [r7, #4]
 800501c:	609a      	str	r2, [r3, #8]
 800501e:	bf00      	nop
 8005020:	370c      	adds	r7, #12
 8005022:	46bd      	mov	sp, r7
 8005024:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005028:	4770      	bx	lr

0800502a <LL_TIM_DisableBRK>:
 800502a:	b480      	push	{r7}
 800502c:	b083      	sub	sp, #12
 800502e:	af00      	add	r7, sp, #0
 8005030:	6078      	str	r0, [r7, #4]
 8005032:	687b      	ldr	r3, [r7, #4]
 8005034:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 8005036:	f423 5280 	bic.w	r2, r3, #4096	; 0x1000
 800503a:	687b      	ldr	r3, [r7, #4]
 800503c:	645a      	str	r2, [r3, #68]	; 0x44
 800503e:	bf00      	nop
 8005040:	370c      	adds	r7, #12
 8005042:	46bd      	mov	sp, r7
 8005044:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005048:	4770      	bx	lr

0800504a <LL_TIM_ConfigBRK>:
 800504a:	b480      	push	{r7}
 800504c:	b083      	sub	sp, #12
 800504e:	af00      	add	r7, sp, #0
 8005050:	6078      	str	r0, [r7, #4]
 8005052:	6039      	str	r1, [r7, #0]
 8005054:	687b      	ldr	r3, [r7, #4]
 8005056:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 8005058:	f423 5200 	bic.w	r2, r3, #8192	; 0x2000
 800505c:	683b      	ldr	r3, [r7, #0]
 800505e:	431a      	orrs	r2, r3
 8005060:	687b      	ldr	r3, [r7, #4]
 8005062:	645a      	str	r2, [r3, #68]	; 0x44
 8005064:	bf00      	nop
 8005066:	370c      	adds	r7, #12
 8005068:	46bd      	mov	sp, r7
 800506a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800506e:	4770      	bx	lr

08005070 <LL_TIM_SetOffStates>:
 8005070:	b480      	push	{r7}
 8005072:	b085      	sub	sp, #20
 8005074:	af00      	add	r7, sp, #0
 8005076:	60f8      	str	r0, [r7, #12]
 8005078:	60b9      	str	r1, [r7, #8]
 800507a:	607a      	str	r2, [r7, #4]
 800507c:	68fb      	ldr	r3, [r7, #12]
 800507e:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 8005080:	f423 6240 	bic.w	r2, r3, #3072	; 0xc00
 8005084:	68b9      	ldr	r1, [r7, #8]
 8005086:	687b      	ldr	r3, [r7, #4]
 8005088:	430b      	orrs	r3, r1
 800508a:	431a      	orrs	r2, r3
 800508c:	68fb      	ldr	r3, [r7, #12]
 800508e:	645a      	str	r2, [r3, #68]	; 0x44
 8005090:	bf00      	nop
 8005092:	3714      	adds	r7, #20
 8005094:	46bd      	mov	sp, r7
 8005096:	f85d 7b04 	ldr.w	r7, [sp], #4
 800509a:	4770      	bx	lr

0800509c <LL_TIM_EnableAutomaticOutput>:
 800509c:	b480      	push	{r7}
 800509e:	b083      	sub	sp, #12
 80050a0:	af00      	add	r7, sp, #0
 80050a2:	6078      	str	r0, [r7, #4]
 80050a4:	687b      	ldr	r3, [r7, #4]
 80050a6:	6c5b      	ldr	r3, [r3, #68]	; 0x44
 80050a8:	f443 4280 	orr.w	r2, r3, #16384	; 0x4000
 80050ac:	687b      	ldr	r3, [r7, #4]
 80050ae:	645a      	str	r2, [r3, #68]	; 0x44
 80050b0:	bf00      	nop
 80050b2:	370c      	adds	r7, #12
 80050b4:	46bd      	mov	sp, r7
 80050b6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80050ba:	4770      	bx	lr

080050bc <LL_TIM_ClearFlag_UPDATE>:
 80050bc:	b480      	push	{r7}
 80050be:	b083      	sub	sp, #12
 80050c0:	af00      	add	r7, sp, #0
 80050c2:	6078      	str	r0, [r7, #4]
 80050c4:	687b      	ldr	r3, [r7, #4]
 80050c6:	f06f 0201 	mvn.w	r2, #1
 80050ca:	611a      	str	r2, [r3, #16]
 80050cc:	bf00      	nop
 80050ce:	370c      	adds	r7, #12
 80050d0:	46bd      	mov	sp, r7
 80050d2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80050d6:	4770      	bx	lr

080050d8 <LL_TIM_EnableIT_UPDATE>:
 80050d8:	b480      	push	{r7}
 80050da:	b083      	sub	sp, #12
 80050dc:	af00      	add	r7, sp, #0
 80050de:	6078      	str	r0, [r7, #4]
 80050e0:	687b      	ldr	r3, [r7, #4]
 80050e2:	68db      	ldr	r3, [r3, #12]
 80050e4:	f043 0201 	orr.w	r2, r3, #1
 80050e8:	687b      	ldr	r3, [r7, #4]
 80050ea:	60da      	str	r2, [r3, #12]
 80050ec:	bf00      	nop
 80050ee:	370c      	adds	r7, #12
 80050f0:	46bd      	mov	sp, r7
 80050f2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80050f6:	4770      	bx	lr

080050f8 <MX_TIM12_Init>:
 80050f8:	b580      	push	{r7, lr}
 80050fa:	af00      	add	r7, sp, #0
 80050fc:	2004      	movs	r0, #4
 80050fe:	f7ff fd69 	bl	8004bd4 <LL_AHB1_GRP1_EnableClock>
 8005102:	2202      	movs	r2, #2
 8005104:	2140      	movs	r1, #64	; 0x40
 8005106:	4867      	ldr	r0, [pc, #412]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005108:	f7ff fc4e 	bl	80049a8 <LL_GPIO_SetPinMode>
 800510c:	2203      	movs	r2, #3
 800510e:	2140      	movs	r1, #64	; 0x40
 8005110:	4864      	ldr	r0, [pc, #400]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005112:	f7ff fce8 	bl	8004ae6 <LL_GPIO_SetAFPin_0_7>
 8005116:	2200      	movs	r2, #0
 8005118:	2140      	movs	r1, #64	; 0x40
 800511a:	4862      	ldr	r0, [pc, #392]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800511c:	f7ff fc71 	bl	8004a02 <LL_GPIO_SetPinOutputType>
 8005120:	2200      	movs	r2, #0
 8005122:	2140      	movs	r1, #64	; 0x40
 8005124:	485f      	ldr	r0, [pc, #380]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005126:	f7ff fcb1 	bl	8004a8c <LL_GPIO_SetPinPull>
 800512a:	2202      	movs	r2, #2
 800512c:	2140      	movs	r1, #64	; 0x40
 800512e:	485d      	ldr	r0, [pc, #372]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005130:	f7ff fc7f 	bl	8004a32 <LL_GPIO_SetPinSpeed>
 8005134:	2202      	movs	r2, #2
 8005136:	2180      	movs	r1, #128	; 0x80
 8005138:	485a      	ldr	r0, [pc, #360]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800513a:	f7ff fc35 	bl	80049a8 <LL_GPIO_SetPinMode>
 800513e:	2203      	movs	r2, #3
 8005140:	2180      	movs	r1, #128	; 0x80
 8005142:	4858      	ldr	r0, [pc, #352]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005144:	f7ff fccf 	bl	8004ae6 <LL_GPIO_SetAFPin_0_7>
 8005148:	2200      	movs	r2, #0
 800514a:	2180      	movs	r1, #128	; 0x80
 800514c:	4855      	ldr	r0, [pc, #340]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800514e:	f7ff fc58 	bl	8004a02 <LL_GPIO_SetPinOutputType>
 8005152:	2200      	movs	r2, #0
 8005154:	2180      	movs	r1, #128	; 0x80
 8005156:	4853      	ldr	r0, [pc, #332]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005158:	f7ff fc98 	bl	8004a8c <LL_GPIO_SetPinPull>
 800515c:	2202      	movs	r2, #2
 800515e:	2180      	movs	r1, #128	; 0x80
 8005160:	4850      	ldr	r0, [pc, #320]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005162:	f7ff fc66 	bl	8004a32 <LL_GPIO_SetPinSpeed>
 8005166:	2202      	movs	r2, #2
 8005168:	f44f 7180 	mov.w	r1, #256	; 0x100
 800516c:	484d      	ldr	r0, [pc, #308]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800516e:	f7ff fc1b 	bl	80049a8 <LL_GPIO_SetPinMode>
 8005172:	2203      	movs	r2, #3
 8005174:	f44f 7180 	mov.w	r1, #256	; 0x100
 8005178:	484a      	ldr	r0, [pc, #296]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800517a:	f7ff fce1 	bl	8004b40 <LL_GPIO_SetAFPin_8_15>
 800517e:	2200      	movs	r2, #0
 8005180:	f44f 7180 	mov.w	r1, #256	; 0x100
 8005184:	4847      	ldr	r0, [pc, #284]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005186:	f7ff fc3c 	bl	8004a02 <LL_GPIO_SetPinOutputType>
 800518a:	2200      	movs	r2, #0
 800518c:	f44f 7180 	mov.w	r1, #256	; 0x100
 8005190:	4844      	ldr	r0, [pc, #272]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 8005192:	f7ff fc7b 	bl	8004a8c <LL_GPIO_SetPinPull>
 8005196:	2202      	movs	r2, #2
 8005198:	f44f 7180 	mov.w	r1, #256	; 0x100
 800519c:	4841      	ldr	r0, [pc, #260]	; (80052a4 <MX_TIM12_Init+0x1ac>)
 800519e:	f7ff fc48 	bl	8004a32 <LL_GPIO_SetPinSpeed>
 80051a2:	2002      	movs	r0, #2
 80051a4:	f7ff fd2e 	bl	8004c04 <LL_APB2_GRP1_EnableClock>
 80051a8:	483f      	ldr	r0, [pc, #252]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051aa:	f7ff fd63 	bl	8004c74 <LL_TIM_EnableUpdateEvent>
 80051ae:	f44f 7100 	mov.w	r1, #512	; 0x200
 80051b2:	483d      	ldr	r0, [pc, #244]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051b4:	f7ff fda4 	bl	8004d00 <LL_TIM_SetClockDivision>
 80051b8:	2100      	movs	r1, #0
 80051ba:	483b      	ldr	r0, [pc, #236]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051bc:	f7ff fd7d 	bl	8004cba <LL_TIM_SetCounterMode>
 80051c0:	f64f 71ff 	movw	r1, #65535	; 0xffff
 80051c4:	4838      	ldr	r0, [pc, #224]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051c6:	f7ff fdbc 	bl	8004d42 <LL_TIM_SetAutoReload>
 80051ca:	2100      	movs	r1, #0
 80051cc:	4836      	ldr	r0, [pc, #216]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051ce:	f7ff fd61 	bl	8004c94 <LL_TIM_SetUpdateSource>
 80051d2:	f240 1111 	movw	r1, #273	; 0x111
 80051d6:	4834      	ldr	r0, [pc, #208]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051d8:	f7ff fdd4 	bl	8004d84 <LL_TIM_CC_EnableChannel>
 80051dc:	2260      	movs	r2, #96	; 0x60
 80051de:	2101      	movs	r1, #1
 80051e0:	4831      	ldr	r0, [pc, #196]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051e2:	f7ff fde1 	bl	8004da8 <LL_TIM_OC_SetMode>
 80051e6:	2260      	movs	r2, #96	; 0x60
 80051e8:	2110      	movs	r1, #16
 80051ea:	482f      	ldr	r0, [pc, #188]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051ec:	f7ff fddc 	bl	8004da8 <LL_TIM_OC_SetMode>
 80051f0:	2260      	movs	r2, #96	; 0x60
 80051f2:	f44f 7180 	mov.w	r1, #256	; 0x100
 80051f6:	482c      	ldr	r0, [pc, #176]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 80051f8:	f7ff fdd6 	bl	8004da8 <LL_TIM_OC_SetMode>
 80051fc:	2101      	movs	r1, #1
 80051fe:	482a      	ldr	r0, [pc, #168]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005200:	f7ff fe1c 	bl	8004e3c <LL_TIM_OC_EnableFast>
 8005204:	2110      	movs	r1, #16
 8005206:	4828      	ldr	r0, [pc, #160]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005208:	f7ff fe18 	bl	8004e3c <LL_TIM_OC_EnableFast>
 800520c:	f44f 7180 	mov.w	r1, #256	; 0x100
 8005210:	4825      	ldr	r0, [pc, #148]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005212:	f7ff fe13 	bl	8004e3c <LL_TIM_OC_EnableFast>
 8005216:	2101      	movs	r1, #1
 8005218:	4823      	ldr	r0, [pc, #140]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800521a:	f7ff fe51 	bl	8004ec0 <LL_TIM_OC_EnablePreload>
 800521e:	2110      	movs	r1, #16
 8005220:	4821      	ldr	r0, [pc, #132]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005222:	f7ff fe4d 	bl	8004ec0 <LL_TIM_OC_EnablePreload>
 8005226:	f44f 7180 	mov.w	r1, #256	; 0x100
 800522a:	481f      	ldr	r0, [pc, #124]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800522c:	f7ff fe48 	bl	8004ec0 <LL_TIM_OC_EnablePreload>
 8005230:	481d      	ldr	r0, [pc, #116]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005232:	f7ff fd55 	bl	8004ce0 <LL_TIM_EnableARRPreload>
 8005236:	2101      	movs	r1, #1
 8005238:	481b      	ldr	r0, [pc, #108]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800523a:	f7ff fe83 	bl	8004f44 <LL_TIM_OC_EnableClear>
 800523e:	2110      	movs	r1, #16
 8005240:	4819      	ldr	r0, [pc, #100]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005242:	f7ff fe7f 	bl	8004f44 <LL_TIM_OC_EnableClear>
 8005246:	f44f 7180 	mov.w	r1, #256	; 0x100
 800524a:	4817      	ldr	r0, [pc, #92]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800524c:	f7ff fe7a 	bl	8004f44 <LL_TIM_OC_EnableClear>
 8005250:	4815      	ldr	r0, [pc, #84]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005252:	f7ff feda 	bl	800500a <LL_TIM_DisableMasterSlaveMode>
 8005256:	2100      	movs	r1, #0
 8005258:	4813      	ldr	r0, [pc, #76]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800525a:	f7ff fd80 	bl	8004d5e <LL_TIM_CC_SetLockLevel>
 800525e:	2100      	movs	r1, #0
 8005260:	4811      	ldr	r0, [pc, #68]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005262:	f7ff feb1 	bl	8004fc8 <LL_TIM_OC_SetDeadTime>
 8005266:	4810      	ldr	r0, [pc, #64]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005268:	f7ff fedf 	bl	800502a <LL_TIM_DisableBRK>
 800526c:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8005270:	480d      	ldr	r0, [pc, #52]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005272:	f7ff feea 	bl	800504a <LL_TIM_ConfigBRK>
 8005276:	2200      	movs	r2, #0
 8005278:	2100      	movs	r1, #0
 800527a:	480b      	ldr	r0, [pc, #44]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 800527c:	f7ff fef8 	bl	8005070 <LL_TIM_SetOffStates>
 8005280:	4809      	ldr	r0, [pc, #36]	; (80052a8 <MX_TIM12_Init+0x1b0>)
 8005282:	f7ff ff0b 	bl	800509c <LL_TIM_EnableAutomaticOutput>
 8005286:	2004      	movs	r0, #4
 8005288:	f7ff fca4 	bl	8004bd4 <LL_AHB1_GRP1_EnableClock>
 800528c:	2201      	movs	r2, #1
 800528e:	2140      	movs	r1, #64	; 0x40
 8005290:	4806      	ldr	r0, [pc, #24]	; (80052ac <MX_TIM12_Init+0x1b4>)
 8005292:	f7ff fb89 	bl	80049a8 <LL_GPIO_SetPinMode>
 8005296:	2201      	movs	r2, #1
 8005298:	2180      	movs	r1, #128	; 0x80
 800529a:	4804      	ldr	r0, [pc, #16]	; (80052ac <MX_TIM12_Init+0x1b4>)
 800529c:	f7ff fb84 	bl	80049a8 <LL_GPIO_SetPinMode>
 80052a0:	bf00      	nop
 80052a2:	bd80      	pop	{r7, pc}
 80052a4:	40020800 	.word	0x40020800
 80052a8:	40010400 	.word	0x40010400
 80052ac:	40020c00 	.word	0x40020c00

080052b0 <init_sort_mech>:
 80052b0:	b580      	push	{r7, lr}
 80052b2:	b082      	sub	sp, #8
 80052b4:	af00      	add	r7, sp, #0
 80052b6:	4b22      	ldr	r3, [pc, #136]	; (8005340 <init_sort_mech+0x90>)
 80052b8:	2232      	movs	r2, #50	; 0x32
 80052ba:	805a      	strh	r2, [r3, #2]
 80052bc:	4b20      	ldr	r3, [pc, #128]	; (8005340 <init_sort_mech+0x90>)
 80052be:	22c0      	movs	r2, #192	; 0xc0
 80052c0:	809a      	strh	r2, [r3, #4]
 80052c2:	4b1f      	ldr	r3, [pc, #124]	; (8005340 <init_sort_mech+0x90>)
 80052c4:	2240      	movs	r2, #64	; 0x40
 80052c6:	80da      	strh	r2, [r3, #6]
 80052c8:	4b1d      	ldr	r3, [pc, #116]	; (8005340 <init_sort_mech+0x90>)
 80052ca:	2280      	movs	r2, #128	; 0x80
 80052cc:	811a      	strh	r2, [r3, #8]
 80052ce:	4b1c      	ldr	r3, [pc, #112]	; (8005340 <init_sort_mech+0x90>)
 80052d0:	2200      	movs	r2, #0
 80052d2:	815a      	strh	r2, [r3, #10]
 80052d4:	f7ff ff10 	bl	80050f8 <MX_TIM12_Init>
 80052d8:	481a      	ldr	r0, [pc, #104]	; (8005344 <init_sort_mech+0x94>)
 80052da:	f7ff fefd 	bl	80050d8 <LL_TIM_EnableIT_UPDATE>
 80052de:	2104      	movs	r1, #4
 80052e0:	202c      	movs	r0, #44	; 0x2c
 80052e2:	f7ff fb37 	bl	8004954 <NVIC_SetPriority>
 80052e6:	202c      	movs	r0, #44	; 0x2c
 80052e8:	f7ff fb1a 	bl	8004920 <NVIC_EnableIRQ>
 80052ec:	f640 719e 	movw	r1, #3998	; 0xf9e
 80052f0:	4814      	ldr	r0, [pc, #80]	; (8005344 <init_sort_mech+0x94>)
 80052f2:	f7ff fd26 	bl	8004d42 <LL_TIM_SetAutoReload>
 80052f6:	2101      	movs	r1, #1
 80052f8:	4812      	ldr	r0, [pc, #72]	; (8005344 <init_sort_mech+0x94>)
 80052fa:	f7ff fd9f 	bl	8004e3c <LL_TIM_OC_EnableFast>
 80052fe:	f240 3147 	movw	r1, #839	; 0x347
 8005302:	4810      	ldr	r0, [pc, #64]	; (8005344 <init_sort_mech+0x94>)
 8005304:	f7ff fd0f 	bl	8004d26 <LL_TIM_SetPrescaler>
 8005308:	f44f 71a8 	mov.w	r1, #336	; 0x150
 800530c:	480d      	ldr	r0, [pc, #52]	; (8005344 <init_sort_mech+0x94>)
 800530e:	f7ff fe6e 	bl	8004fee <LL_TIM_OC_SetCompareCH1>
 8005312:	480c      	ldr	r0, [pc, #48]	; (8005344 <init_sort_mech+0x94>)
 8005314:	f7ff fc8e 	bl	8004c34 <LL_TIM_EnableCounter>
 8005318:	480b      	ldr	r0, [pc, #44]	; (8005348 <init_sort_mech+0x98>)
 800531a:	f7ff fc40 	bl	8004b9e <LL_GPIO_ReadInputPort>
 800531e:	6078      	str	r0, [r7, #4]
 8005320:	4b07      	ldr	r3, [pc, #28]	; (8005340 <init_sort_mech+0x90>)
 8005322:	889b      	ldrh	r3, [r3, #4]
 8005324:	43db      	mvns	r3, r3
 8005326:	461a      	mov	r2, r3
 8005328:	687b      	ldr	r3, [r7, #4]
 800532a:	4013      	ands	r3, r2
 800532c:	607b      	str	r3, [r7, #4]
 800532e:	6879      	ldr	r1, [r7, #4]
 8005330:	4805      	ldr	r0, [pc, #20]	; (8005348 <init_sort_mech+0x98>)
 8005332:	f7ff fc40 	bl	8004bb6 <LL_GPIO_WriteOutputPort>
 8005336:	bf00      	nop
 8005338:	3708      	adds	r7, #8
 800533a:	46bd      	mov	sp, r7
 800533c:	bd80      	pop	{r7, pc}
 800533e:	bf00      	nop
 8005340:	200177f0 	.word	0x200177f0
 8005344:	40010400 	.word	0x40010400
 8005348:	40020c00 	.word	0x40020c00

0800534c <cmd_set_sorting_dist>:
 800534c:	b580      	push	{r7, lr}
 800534e:	b084      	sub	sp, #16
 8005350:	af00      	add	r7, sp, #0
 8005352:	6078      	str	r0, [r7, #4]
 8005354:	482f      	ldr	r0, [pc, #188]	; (8005414 <cmd_set_sorting_dist+0xc8>)
 8005356:	f7ff fc22 	bl	8004b9e <LL_GPIO_ReadInputPort>
 800535a:	60f8      	str	r0, [r7, #12]
 800535c:	4b2e      	ldr	r3, [pc, #184]	; (8005418 <cmd_set_sorting_dist+0xcc>)
 800535e:	889b      	ldrh	r3, [r3, #4]
 8005360:	43db      	mvns	r3, r3
 8005362:	461a      	mov	r2, r3
 8005364:	68fb      	ldr	r3, [r7, #12]
 8005366:	4013      	ands	r3, r2
 8005368:	60fb      	str	r3, [r7, #12]
 800536a:	68f9      	ldr	r1, [r7, #12]
 800536c:	4829      	ldr	r0, [pc, #164]	; (8005414 <cmd_set_sorting_dist+0xc8>)
 800536e:	f7ff fc22 	bl	8004bb6 <LL_GPIO_WriteOutputPort>
 8005372:	482a      	ldr	r0, [pc, #168]	; (800541c <cmd_set_sorting_dist+0xd0>)
 8005374:	f7ff fc6e 	bl	8004c54 <LL_TIM_DisableCounter>
 8005378:	687b      	ldr	r3, [r7, #4]
 800537a:	781b      	ldrb	r3, [r3, #0]
 800537c:	72fb      	strb	r3, [r7, #11]
 800537e:	7afb      	ldrb	r3, [r7, #11]
 8005380:	2b09      	cmp	r3, #9
 8005382:	d93c      	bls.n	80053fe <cmd_set_sorting_dist+0xb2>
 8005384:	7afb      	ldrb	r3, [r7, #11]
 8005386:	2bb5      	cmp	r3, #181	; 0xb5
 8005388:	d839      	bhi.n	80053fe <cmd_set_sorting_dist+0xb2>
 800538a:	7afb      	ldrb	r3, [r7, #11]
 800538c:	b29a      	uxth	r2, r3
 800538e:	4b22      	ldr	r3, [pc, #136]	; (8005418 <cmd_set_sorting_dist+0xcc>)
 8005390:	805a      	strh	r2, [r3, #2]
 8005392:	687b      	ldr	r3, [r7, #4]
 8005394:	3301      	adds	r3, #1
 8005396:	781b      	ldrb	r3, [r3, #0]
 8005398:	72bb      	strb	r3, [r7, #10]
 800539a:	7abb      	ldrb	r3, [r7, #10]
 800539c:	2b00      	cmp	r3, #0
 800539e:	d002      	beq.n	80053a6 <cmd_set_sorting_dist+0x5a>
 80053a0:	2b01      	cmp	r3, #1
 80053a2:	d013      	beq.n	80053cc <cmd_set_sorting_dist+0x80>
 80053a4:	e02c      	b.n	8005400 <cmd_set_sorting_dist+0xb4>
 80053a6:	f640 719e 	movw	r1, #3998	; 0xf9e
 80053aa:	481c      	ldr	r0, [pc, #112]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053ac:	f7ff fcc9 	bl	8004d42 <LL_TIM_SetAutoReload>
 80053b0:	2101      	movs	r1, #1
 80053b2:	481a      	ldr	r0, [pc, #104]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053b4:	f7ff fd42 	bl	8004e3c <LL_TIM_OC_EnableFast>
 80053b8:	f240 3147 	movw	r1, #839	; 0x347
 80053bc:	4817      	ldr	r0, [pc, #92]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053be:	f7ff fcb2 	bl	8004d26 <LL_TIM_SetPrescaler>
 80053c2:	21c0      	movs	r1, #192	; 0xc0
 80053c4:	4815      	ldr	r0, [pc, #84]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053c6:	f7ff fe12 	bl	8004fee <LL_TIM_OC_SetCompareCH1>
 80053ca:	e013      	b.n	80053f4 <cmd_set_sorting_dist+0xa8>
 80053cc:	f640 719e 	movw	r1, #3998	; 0xf9e
 80053d0:	4812      	ldr	r0, [pc, #72]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053d2:	f7ff fcb6 	bl	8004d42 <LL_TIM_SetAutoReload>
 80053d6:	2101      	movs	r1, #1
 80053d8:	4810      	ldr	r0, [pc, #64]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053da:	f7ff fd2f 	bl	8004e3c <LL_TIM_OC_EnableFast>
 80053de:	f240 3147 	movw	r1, #839	; 0x347
 80053e2:	480e      	ldr	r0, [pc, #56]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053e4:	f7ff fc9f 	bl	8004d26 <LL_TIM_SetPrescaler>
 80053e8:	f44f 71a8 	mov.w	r1, #336	; 0x150
 80053ec:	480b      	ldr	r0, [pc, #44]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053ee:	f7ff fdfe 	bl	8004fee <LL_TIM_OC_SetCompareCH1>
 80053f2:	bf00      	nop
 80053f4:	4809      	ldr	r0, [pc, #36]	; (800541c <cmd_set_sorting_dist+0xd0>)
 80053f6:	f7ff fc1d 	bl	8004c34 <LL_TIM_EnableCounter>
 80053fa:	4b09      	ldr	r3, [pc, #36]	; (8005420 <cmd_set_sorting_dist+0xd4>)
 80053fc:	e006      	b.n	800540c <cmd_set_sorting_dist+0xc0>
 80053fe:	bf00      	nop
 8005400:	2203      	movs	r2, #3
 8005402:	4908      	ldr	r1, [pc, #32]	; (8005424 <cmd_set_sorting_dist+0xd8>)
 8005404:	6878      	ldr	r0, [r7, #4]
 8005406:	f005 fa81 	bl	800a90c <memcpy>
 800540a:	2303      	movs	r3, #3
 800540c:	4618      	mov	r0, r3
 800540e:	3710      	adds	r7, #16
 8005410:	46bd      	mov	sp, r7
 8005412:	bd80      	pop	{r7, pc}
 8005414:	40020c00 	.word	0x40020c00
 8005418:	200177f0 	.word	0x200177f0
 800541c:	40010400 	.word	0x40010400
 8005420:	0800d0f8 	.word	0x0800d0f8
 8005424:	0800d0fc 	.word	0x0800d0fc

08005428 <TIM8_UP_TIM13_IRQHandler>:
 8005428:	b580      	push	{r7, lr}
 800542a:	b082      	sub	sp, #8
 800542c:	af00      	add	r7, sp, #0
 800542e:	2300      	movs	r3, #0
 8005430:	603b      	str	r3, [r7, #0]
 8005432:	4825      	ldr	r0, [pc, #148]	; (80054c8 <TIM8_UP_TIM13_IRQHandler+0xa0>)
 8005434:	f7ff fe42 	bl	80050bc <LL_TIM_ClearFlag_UPDATE>
 8005438:	4824      	ldr	r0, [pc, #144]	; (80054cc <TIM8_UP_TIM13_IRQHandler+0xa4>)
 800543a:	f7ff fbb0 	bl	8004b9e <LL_GPIO_ReadInputPort>
 800543e:	6078      	str	r0, [r7, #4]
 8005440:	4b23      	ldr	r3, [pc, #140]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 8005442:	889b      	ldrh	r3, [r3, #4]
 8005444:	43db      	mvns	r3, r3
 8005446:	461a      	mov	r2, r3
 8005448:	687b      	ldr	r3, [r7, #4]
 800544a:	4013      	ands	r3, r2
 800544c:	607b      	str	r3, [r7, #4]
 800544e:	2000      	movs	r0, #0
 8005450:	f001 fcc4 	bl	8006ddc <cmd_get_col_av_data>
 8005454:	4b1e      	ldr	r3, [pc, #120]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 8005456:	881b      	ldrh	r3, [r3, #0]
 8005458:	2b0e      	cmp	r3, #14
 800545a:	d91e      	bls.n	800549a <TIM8_UP_TIM13_IRQHandler+0x72>
 800545c:	4b1c      	ldr	r3, [pc, #112]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 800545e:	885b      	ldrh	r3, [r3, #2]
 8005460:	461a      	mov	r2, r3
 8005462:	4b1b      	ldr	r3, [pc, #108]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 8005464:	881b      	ldrh	r3, [r3, #0]
 8005466:	1ad3      	subs	r3, r2, r3
 8005468:	2b05      	cmp	r3, #5
 800546a:	dd06      	ble.n	800547a <TIM8_UP_TIM13_IRQHandler+0x52>
 800546c:	4b18      	ldr	r3, [pc, #96]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 800546e:	88db      	ldrh	r3, [r3, #6]
 8005470:	461a      	mov	r2, r3
 8005472:	687b      	ldr	r3, [r7, #4]
 8005474:	4313      	orrs	r3, r2
 8005476:	607b      	str	r3, [r7, #4]
 8005478:	e012      	b.n	80054a0 <TIM8_UP_TIM13_IRQHandler+0x78>
 800547a:	4b15      	ldr	r3, [pc, #84]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 800547c:	885b      	ldrh	r3, [r3, #2]
 800547e:	461a      	mov	r2, r3
 8005480:	4b13      	ldr	r3, [pc, #76]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 8005482:	881b      	ldrh	r3, [r3, #0]
 8005484:	1ad3      	subs	r3, r2, r3
 8005486:	f113 0f05 	cmn.w	r3, #5
 800548a:	da08      	bge.n	800549e <TIM8_UP_TIM13_IRQHandler+0x76>
 800548c:	4b10      	ldr	r3, [pc, #64]	; (80054d0 <TIM8_UP_TIM13_IRQHandler+0xa8>)
 800548e:	891b      	ldrh	r3, [r3, #8]
 8005490:	461a      	mov	r2, r3
 8005492:	687b      	ldr	r3, [r7, #4]
 8005494:	4313      	orrs	r3, r2
 8005496:	607b      	str	r3, [r7, #4]
 8005498:	e002      	b.n	80054a0 <TIM8_UP_TIM13_IRQHandler+0x78>
 800549a:	bf00      	nop
 800549c:	e000      	b.n	80054a0 <TIM8_UP_TIM13_IRQHandler+0x78>
 800549e:	bf00      	nop
 80054a0:	6879      	ldr	r1, [r7, #4]
 80054a2:	480a      	ldr	r0, [pc, #40]	; (80054cc <TIM8_UP_TIM13_IRQHandler+0xa4>)
 80054a4:	f7ff fb87 	bl	8004bb6 <LL_GPIO_WriteOutputPort>
 80054a8:	683b      	ldr	r3, [r7, #0]
 80054aa:	2b00      	cmp	r3, #0
 80054ac:	d007      	beq.n	80054be <TIM8_UP_TIM13_IRQHandler+0x96>
 80054ae:	4b09      	ldr	r3, [pc, #36]	; (80054d4 <TIM8_UP_TIM13_IRQHandler+0xac>)
 80054b0:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80054b4:	601a      	str	r2, [r3, #0]
 80054b6:	f3bf 8f4f 	dsb	sy
 80054ba:	f3bf 8f6f 	isb	sy
 80054be:	bf00      	nop
 80054c0:	3708      	adds	r7, #8
 80054c2:	46bd      	mov	sp, r7
 80054c4:	bd80      	pop	{r7, pc}
 80054c6:	bf00      	nop
 80054c8:	40010400 	.word	0x40010400
 80054cc:	40020c00 	.word	0x40020c00
 80054d0:	200177f0 	.word	0x200177f0
 80054d4:	e000ed04 	.word	0xe000ed04

080054d8 <NVIC_EnableIRQ>:
 80054d8:	b480      	push	{r7}
 80054da:	b083      	sub	sp, #12
 80054dc:	af00      	add	r7, sp, #0
 80054de:	4603      	mov	r3, r0
 80054e0:	71fb      	strb	r3, [r7, #7]
 80054e2:	79fb      	ldrb	r3, [r7, #7]
 80054e4:	f003 021f 	and.w	r2, r3, #31
 80054e8:	4907      	ldr	r1, [pc, #28]	; (8005508 <NVIC_EnableIRQ+0x30>)
 80054ea:	f997 3007 	ldrsb.w	r3, [r7, #7]
 80054ee:	095b      	lsrs	r3, r3, #5
 80054f0:	2001      	movs	r0, #1
 80054f2:	fa00 f202 	lsl.w	r2, r0, r2
 80054f6:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 80054fa:	bf00      	nop
 80054fc:	370c      	adds	r7, #12
 80054fe:	46bd      	mov	sp, r7
 8005500:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005504:	4770      	bx	lr
 8005506:	bf00      	nop
 8005508:	e000e100 	.word	0xe000e100

0800550c <NVIC_SetPriority>:
 800550c:	b480      	push	{r7}
 800550e:	b083      	sub	sp, #12
 8005510:	af00      	add	r7, sp, #0
 8005512:	4603      	mov	r3, r0
 8005514:	6039      	str	r1, [r7, #0]
 8005516:	71fb      	strb	r3, [r7, #7]
 8005518:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800551c:	2b00      	cmp	r3, #0
 800551e:	da0b      	bge.n	8005538 <NVIC_SetPriority+0x2c>
 8005520:	683b      	ldr	r3, [r7, #0]
 8005522:	b2da      	uxtb	r2, r3
 8005524:	490c      	ldr	r1, [pc, #48]	; (8005558 <NVIC_SetPriority+0x4c>)
 8005526:	79fb      	ldrb	r3, [r7, #7]
 8005528:	f003 030f 	and.w	r3, r3, #15
 800552c:	3b04      	subs	r3, #4
 800552e:	0112      	lsls	r2, r2, #4
 8005530:	b2d2      	uxtb	r2, r2
 8005532:	440b      	add	r3, r1
 8005534:	761a      	strb	r2, [r3, #24]
 8005536:	e009      	b.n	800554c <NVIC_SetPriority+0x40>
 8005538:	683b      	ldr	r3, [r7, #0]
 800553a:	b2da      	uxtb	r2, r3
 800553c:	4907      	ldr	r1, [pc, #28]	; (800555c <NVIC_SetPriority+0x50>)
 800553e:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8005542:	0112      	lsls	r2, r2, #4
 8005544:	b2d2      	uxtb	r2, r2
 8005546:	440b      	add	r3, r1
 8005548:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 800554c:	bf00      	nop
 800554e:	370c      	adds	r7, #12
 8005550:	46bd      	mov	sp, r7
 8005552:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005556:	4770      	bx	lr
 8005558:	e000ed00 	.word	0xe000ed00
 800555c:	e000e100 	.word	0xe000e100

08005560 <LL_GPIO_SetPinMode>:
 8005560:	b480      	push	{r7}
 8005562:	b089      	sub	sp, #36	; 0x24
 8005564:	af00      	add	r7, sp, #0
 8005566:	60f8      	str	r0, [r7, #12]
 8005568:	60b9      	str	r1, [r7, #8]
 800556a:	607a      	str	r2, [r7, #4]
 800556c:	68fb      	ldr	r3, [r7, #12]
 800556e:	681a      	ldr	r2, [r3, #0]
 8005570:	68bb      	ldr	r3, [r7, #8]
 8005572:	617b      	str	r3, [r7, #20]
 8005574:	697b      	ldr	r3, [r7, #20]
 8005576:	fa93 f3a3 	rbit	r3, r3
 800557a:	613b      	str	r3, [r7, #16]
 800557c:	693b      	ldr	r3, [r7, #16]
 800557e:	fab3 f383 	clz	r3, r3
 8005582:	005b      	lsls	r3, r3, #1
 8005584:	2103      	movs	r1, #3
 8005586:	fa01 f303 	lsl.w	r3, r1, r3
 800558a:	43db      	mvns	r3, r3
 800558c:	401a      	ands	r2, r3
 800558e:	68bb      	ldr	r3, [r7, #8]
 8005590:	61fb      	str	r3, [r7, #28]
 8005592:	69fb      	ldr	r3, [r7, #28]
 8005594:	fa93 f3a3 	rbit	r3, r3
 8005598:	61bb      	str	r3, [r7, #24]
 800559a:	69bb      	ldr	r3, [r7, #24]
 800559c:	fab3 f383 	clz	r3, r3
 80055a0:	005b      	lsls	r3, r3, #1
 80055a2:	6879      	ldr	r1, [r7, #4]
 80055a4:	fa01 f303 	lsl.w	r3, r1, r3
 80055a8:	431a      	orrs	r2, r3
 80055aa:	68fb      	ldr	r3, [r7, #12]
 80055ac:	601a      	str	r2, [r3, #0]
 80055ae:	bf00      	nop
 80055b0:	3724      	adds	r7, #36	; 0x24
 80055b2:	46bd      	mov	sp, r7
 80055b4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80055b8:	4770      	bx	lr

080055ba <LL_GPIO_SetPinPull>:
 80055ba:	b480      	push	{r7}
 80055bc:	b089      	sub	sp, #36	; 0x24
 80055be:	af00      	add	r7, sp, #0
 80055c0:	60f8      	str	r0, [r7, #12]
 80055c2:	60b9      	str	r1, [r7, #8]
 80055c4:	607a      	str	r2, [r7, #4]
 80055c6:	68fb      	ldr	r3, [r7, #12]
 80055c8:	68da      	ldr	r2, [r3, #12]
 80055ca:	68bb      	ldr	r3, [r7, #8]
 80055cc:	617b      	str	r3, [r7, #20]
 80055ce:	697b      	ldr	r3, [r7, #20]
 80055d0:	fa93 f3a3 	rbit	r3, r3
 80055d4:	613b      	str	r3, [r7, #16]
 80055d6:	693b      	ldr	r3, [r7, #16]
 80055d8:	fab3 f383 	clz	r3, r3
 80055dc:	005b      	lsls	r3, r3, #1
 80055de:	2103      	movs	r1, #3
 80055e0:	fa01 f303 	lsl.w	r3, r1, r3
 80055e4:	43db      	mvns	r3, r3
 80055e6:	401a      	ands	r2, r3
 80055e8:	68bb      	ldr	r3, [r7, #8]
 80055ea:	61fb      	str	r3, [r7, #28]
 80055ec:	69fb      	ldr	r3, [r7, #28]
 80055ee:	fa93 f3a3 	rbit	r3, r3
 80055f2:	61bb      	str	r3, [r7, #24]
 80055f4:	69bb      	ldr	r3, [r7, #24]
 80055f6:	fab3 f383 	clz	r3, r3
 80055fa:	005b      	lsls	r3, r3, #1
 80055fc:	6879      	ldr	r1, [r7, #4]
 80055fe:	fa01 f303 	lsl.w	r3, r1, r3
 8005602:	431a      	orrs	r2, r3
 8005604:	68fb      	ldr	r3, [r7, #12]
 8005606:	60da      	str	r2, [r3, #12]
 8005608:	bf00      	nop
 800560a:	3724      	adds	r7, #36	; 0x24
 800560c:	46bd      	mov	sp, r7
 800560e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005612:	4770      	bx	lr

08005614 <LL_GPIO_ReadInputPort>:
 8005614:	b480      	push	{r7}
 8005616:	b083      	sub	sp, #12
 8005618:	af00      	add	r7, sp, #0
 800561a:	6078      	str	r0, [r7, #4]
 800561c:	687b      	ldr	r3, [r7, #4]
 800561e:	691b      	ldr	r3, [r3, #16]
 8005620:	4618      	mov	r0, r3
 8005622:	370c      	adds	r7, #12
 8005624:	46bd      	mov	sp, r7
 8005626:	f85d 7b04 	ldr.w	r7, [sp], #4
 800562a:	4770      	bx	lr

0800562c <LL_GPIO_IsInputPinSet>:
 800562c:	b480      	push	{r7}
 800562e:	b083      	sub	sp, #12
 8005630:	af00      	add	r7, sp, #0
 8005632:	6078      	str	r0, [r7, #4]
 8005634:	6039      	str	r1, [r7, #0]
 8005636:	687b      	ldr	r3, [r7, #4]
 8005638:	691a      	ldr	r2, [r3, #16]
 800563a:	683b      	ldr	r3, [r7, #0]
 800563c:	4013      	ands	r3, r2
 800563e:	683a      	ldr	r2, [r7, #0]
 8005640:	429a      	cmp	r2, r3
 8005642:	bf0c      	ite	eq
 8005644:	2301      	moveq	r3, #1
 8005646:	2300      	movne	r3, #0
 8005648:	b2db      	uxtb	r3, r3
 800564a:	4618      	mov	r0, r3
 800564c:	370c      	adds	r7, #12
 800564e:	46bd      	mov	sp, r7
 8005650:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005654:	4770      	bx	lr

08005656 <LL_GPIO_WriteOutputPort>:
 8005656:	b480      	push	{r7}
 8005658:	b083      	sub	sp, #12
 800565a:	af00      	add	r7, sp, #0
 800565c:	6078      	str	r0, [r7, #4]
 800565e:	6039      	str	r1, [r7, #0]
 8005660:	687b      	ldr	r3, [r7, #4]
 8005662:	683a      	ldr	r2, [r7, #0]
 8005664:	615a      	str	r2, [r3, #20]
 8005666:	bf00      	nop
 8005668:	370c      	adds	r7, #12
 800566a:	46bd      	mov	sp, r7
 800566c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005670:	4770      	bx	lr
	...

08005674 <LL_AHB1_GRP1_EnableClock>:
 8005674:	b480      	push	{r7}
 8005676:	b085      	sub	sp, #20
 8005678:	af00      	add	r7, sp, #0
 800567a:	6078      	str	r0, [r7, #4]
 800567c:	4b08      	ldr	r3, [pc, #32]	; (80056a0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800567e:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8005680:	4907      	ldr	r1, [pc, #28]	; (80056a0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8005682:	687b      	ldr	r3, [r7, #4]
 8005684:	4313      	orrs	r3, r2
 8005686:	630b      	str	r3, [r1, #48]	; 0x30
 8005688:	4b05      	ldr	r3, [pc, #20]	; (80056a0 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800568a:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800568c:	687b      	ldr	r3, [r7, #4]
 800568e:	4013      	ands	r3, r2
 8005690:	60fb      	str	r3, [r7, #12]
 8005692:	68fb      	ldr	r3, [r7, #12]
 8005694:	bf00      	nop
 8005696:	3714      	adds	r7, #20
 8005698:	46bd      	mov	sp, r7
 800569a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800569e:	4770      	bx	lr
 80056a0:	40023800 	.word	0x40023800

080056a4 <LL_APB2_GRP1_EnableClock>:
 80056a4:	b480      	push	{r7}
 80056a6:	b085      	sub	sp, #20
 80056a8:	af00      	add	r7, sp, #0
 80056aa:	6078      	str	r0, [r7, #4]
 80056ac:	4b08      	ldr	r3, [pc, #32]	; (80056d0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80056ae:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80056b0:	4907      	ldr	r1, [pc, #28]	; (80056d0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80056b2:	687b      	ldr	r3, [r7, #4]
 80056b4:	4313      	orrs	r3, r2
 80056b6:	644b      	str	r3, [r1, #68]	; 0x44
 80056b8:	4b05      	ldr	r3, [pc, #20]	; (80056d0 <LL_APB2_GRP1_EnableClock+0x2c>)
 80056ba:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80056bc:	687b      	ldr	r3, [r7, #4]
 80056be:	4013      	ands	r3, r2
 80056c0:	60fb      	str	r3, [r7, #12]
 80056c2:	68fb      	ldr	r3, [r7, #12]
 80056c4:	bf00      	nop
 80056c6:	3714      	adds	r7, #20
 80056c8:	46bd      	mov	sp, r7
 80056ca:	f85d 7b04 	ldr.w	r7, [sp], #4
 80056ce:	4770      	bx	lr
 80056d0:	40023800 	.word	0x40023800

080056d4 <LL_TIM_EnableCounter>:
 80056d4:	b480      	push	{r7}
 80056d6:	b083      	sub	sp, #12
 80056d8:	af00      	add	r7, sp, #0
 80056da:	6078      	str	r0, [r7, #4]
 80056dc:	687b      	ldr	r3, [r7, #4]
 80056de:	681b      	ldr	r3, [r3, #0]
 80056e0:	f043 0201 	orr.w	r2, r3, #1
 80056e4:	687b      	ldr	r3, [r7, #4]
 80056e6:	601a      	str	r2, [r3, #0]
 80056e8:	bf00      	nop
 80056ea:	370c      	adds	r7, #12
 80056ec:	46bd      	mov	sp, r7
 80056ee:	f85d 7b04 	ldr.w	r7, [sp], #4
 80056f2:	4770      	bx	lr

080056f4 <LL_TIM_DisableCounter>:
 80056f4:	b480      	push	{r7}
 80056f6:	b083      	sub	sp, #12
 80056f8:	af00      	add	r7, sp, #0
 80056fa:	6078      	str	r0, [r7, #4]
 80056fc:	687b      	ldr	r3, [r7, #4]
 80056fe:	681b      	ldr	r3, [r3, #0]
 8005700:	f023 0201 	bic.w	r2, r3, #1
 8005704:	687b      	ldr	r3, [r7, #4]
 8005706:	601a      	str	r2, [r3, #0]
 8005708:	bf00      	nop
 800570a:	370c      	adds	r7, #12
 800570c:	46bd      	mov	sp, r7
 800570e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005712:	4770      	bx	lr

08005714 <LL_TIM_SetCounterMode>:
 8005714:	b480      	push	{r7}
 8005716:	b083      	sub	sp, #12
 8005718:	af00      	add	r7, sp, #0
 800571a:	6078      	str	r0, [r7, #4]
 800571c:	6039      	str	r1, [r7, #0]
 800571e:	687b      	ldr	r3, [r7, #4]
 8005720:	681b      	ldr	r3, [r3, #0]
 8005722:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8005726:	683b      	ldr	r3, [r7, #0]
 8005728:	431a      	orrs	r2, r3
 800572a:	687b      	ldr	r3, [r7, #4]
 800572c:	601a      	str	r2, [r3, #0]
 800572e:	bf00      	nop
 8005730:	370c      	adds	r7, #12
 8005732:	46bd      	mov	sp, r7
 8005734:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005738:	4770      	bx	lr

0800573a <LL_TIM_SetPrescaler>:
 800573a:	b480      	push	{r7}
 800573c:	b083      	sub	sp, #12
 800573e:	af00      	add	r7, sp, #0
 8005740:	6078      	str	r0, [r7, #4]
 8005742:	6039      	str	r1, [r7, #0]
 8005744:	687b      	ldr	r3, [r7, #4]
 8005746:	683a      	ldr	r2, [r7, #0]
 8005748:	629a      	str	r2, [r3, #40]	; 0x28
 800574a:	bf00      	nop
 800574c:	370c      	adds	r7, #12
 800574e:	46bd      	mov	sp, r7
 8005750:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005754:	4770      	bx	lr

08005756 <LL_TIM_SetAutoReload>:
 8005756:	b480      	push	{r7}
 8005758:	b083      	sub	sp, #12
 800575a:	af00      	add	r7, sp, #0
 800575c:	6078      	str	r0, [r7, #4]
 800575e:	6039      	str	r1, [r7, #0]
 8005760:	687b      	ldr	r3, [r7, #4]
 8005762:	683a      	ldr	r2, [r7, #0]
 8005764:	62da      	str	r2, [r3, #44]	; 0x2c
 8005766:	bf00      	nop
 8005768:	370c      	adds	r7, #12
 800576a:	46bd      	mov	sp, r7
 800576c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005770:	4770      	bx	lr

08005772 <LL_TIM_ClearFlag_UPDATE>:
 8005772:	b480      	push	{r7}
 8005774:	b083      	sub	sp, #12
 8005776:	af00      	add	r7, sp, #0
 8005778:	6078      	str	r0, [r7, #4]
 800577a:	687b      	ldr	r3, [r7, #4]
 800577c:	f06f 0201 	mvn.w	r2, #1
 8005780:	611a      	str	r2, [r3, #16]
 8005782:	bf00      	nop
 8005784:	370c      	adds	r7, #12
 8005786:	46bd      	mov	sp, r7
 8005788:	f85d 7b04 	ldr.w	r7, [sp], #4
 800578c:	4770      	bx	lr

0800578e <LL_TIM_EnableIT_UPDATE>:
 800578e:	b480      	push	{r7}
 8005790:	b083      	sub	sp, #12
 8005792:	af00      	add	r7, sp, #0
 8005794:	6078      	str	r0, [r7, #4]
 8005796:	687b      	ldr	r3, [r7, #4]
 8005798:	68db      	ldr	r3, [r3, #12]
 800579a:	f043 0201 	orr.w	r2, r3, #1
 800579e:	687b      	ldr	r3, [r7, #4]
 80057a0:	60da      	str	r2, [r3, #12]
 80057a2:	bf00      	nop
 80057a4:	370c      	adds	r7, #12
 80057a6:	46bd      	mov	sp, r7
 80057a8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80057ac:	4770      	bx	lr
	...

080057b0 <step_hw_config>:
 80057b0:	b580      	push	{r7, lr}
 80057b2:	af00      	add	r7, sp, #0
 80057b4:	2010      	movs	r0, #16
 80057b6:	f7ff ff5d 	bl	8005674 <LL_AHB1_GRP1_EnableClock>
 80057ba:	2201      	movs	r2, #1
 80057bc:	f44f 7180 	mov.w	r1, #256	; 0x100
 80057c0:	481f      	ldr	r0, [pc, #124]	; (8005840 <step_hw_config+0x90>)
 80057c2:	f7ff fecd 	bl	8005560 <LL_GPIO_SetPinMode>
 80057c6:	2201      	movs	r2, #1
 80057c8:	f44f 7100 	mov.w	r1, #512	; 0x200
 80057cc:	481c      	ldr	r0, [pc, #112]	; (8005840 <step_hw_config+0x90>)
 80057ce:	f7ff fec7 	bl	8005560 <LL_GPIO_SetPinMode>
 80057d2:	2201      	movs	r2, #1
 80057d4:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80057d8:	4819      	ldr	r0, [pc, #100]	; (8005840 <step_hw_config+0x90>)
 80057da:	f7ff fec1 	bl	8005560 <LL_GPIO_SetPinMode>
 80057de:	2201      	movs	r2, #1
 80057e0:	2120      	movs	r1, #32
 80057e2:	4817      	ldr	r0, [pc, #92]	; (8005840 <step_hw_config+0x90>)
 80057e4:	f7ff febc 	bl	8005560 <LL_GPIO_SetPinMode>
 80057e8:	2010      	movs	r0, #16
 80057ea:	f7ff ff43 	bl	8005674 <LL_AHB1_GRP1_EnableClock>
 80057ee:	2200      	movs	r2, #0
 80057f0:	2180      	movs	r1, #128	; 0x80
 80057f2:	4813      	ldr	r0, [pc, #76]	; (8005840 <step_hw_config+0x90>)
 80057f4:	f7ff feb4 	bl	8005560 <LL_GPIO_SetPinMode>
 80057f8:	2200      	movs	r2, #0
 80057fa:	2180      	movs	r1, #128	; 0x80
 80057fc:	4810      	ldr	r0, [pc, #64]	; (8005840 <step_hw_config+0x90>)
 80057fe:	f7ff fedc 	bl	80055ba <LL_GPIO_SetPinPull>
 8005802:	f44f 3080 	mov.w	r0, #65536	; 0x10000
 8005806:	f7ff ff4d 	bl	80056a4 <LL_APB2_GRP1_EnableClock>
 800580a:	2100      	movs	r1, #0
 800580c:	480d      	ldr	r0, [pc, #52]	; (8005844 <step_hw_config+0x94>)
 800580e:	f7ff ff81 	bl	8005714 <LL_TIM_SetCounterMode>
 8005812:	f240 11f3 	movw	r1, #499	; 0x1f3
 8005816:	480b      	ldr	r0, [pc, #44]	; (8005844 <step_hw_config+0x94>)
 8005818:	f7ff ff9d 	bl	8005756 <LL_TIM_SetAutoReload>
 800581c:	f240 71cf 	movw	r1, #1999	; 0x7cf
 8005820:	4808      	ldr	r0, [pc, #32]	; (8005844 <step_hw_config+0x94>)
 8005822:	f7ff ff8a 	bl	800573a <LL_TIM_SetPrescaler>
 8005826:	4807      	ldr	r0, [pc, #28]	; (8005844 <step_hw_config+0x94>)
 8005828:	f7ff ffb1 	bl	800578e <LL_TIM_EnableIT_UPDATE>
 800582c:	2106      	movs	r1, #6
 800582e:	2018      	movs	r0, #24
 8005830:	f7ff fe6c 	bl	800550c <NVIC_SetPriority>
 8005834:	2018      	movs	r0, #24
 8005836:	f7ff fe4f 	bl	80054d8 <NVIC_EnableIRQ>
 800583a:	bf00      	nop
 800583c:	bd80      	pop	{r7, pc}
 800583e:	bf00      	nop
 8005840:	40021000 	.word	0x40021000
 8005844:	40014000 	.word	0x40014000

08005848 <step_reg_motor>:
 8005848:	b480      	push	{r7}
 800584a:	b085      	sub	sp, #20
 800584c:	af00      	add	r7, sp, #0
 800584e:	60b9      	str	r1, [r7, #8]
 8005850:	607a      	str	r2, [r7, #4]
 8005852:	603b      	str	r3, [r7, #0]
 8005854:	4603      	mov	r3, r0
 8005856:	73fb      	strb	r3, [r7, #15]
 8005858:	7bfb      	ldrb	r3, [r7, #15]
 800585a:	4a25      	ldr	r2, [pc, #148]	; (80058f0 <step_reg_motor+0xa8>)
 800585c:	2134      	movs	r1, #52	; 0x34
 800585e:	fb01 f303 	mul.w	r3, r1, r3
 8005862:	4413      	add	r3, r2
 8005864:	3310      	adds	r3, #16
 8005866:	68ba      	ldr	r2, [r7, #8]
 8005868:	601a      	str	r2, [r3, #0]
 800586a:	7bfb      	ldrb	r3, [r7, #15]
 800586c:	69f9      	ldr	r1, [r7, #28]
 800586e:	687a      	ldr	r2, [r7, #4]
 8005870:	430a      	orrs	r2, r1
 8005872:	491f      	ldr	r1, [pc, #124]	; (80058f0 <step_reg_motor+0xa8>)
 8005874:	2034      	movs	r0, #52	; 0x34
 8005876:	fb00 f303 	mul.w	r3, r0, r3
 800587a:	440b      	add	r3, r1
 800587c:	3320      	adds	r3, #32
 800587e:	601a      	str	r2, [r3, #0]
 8005880:	7bfb      	ldrb	r3, [r7, #15]
 8005882:	6839      	ldr	r1, [r7, #0]
 8005884:	69fa      	ldr	r2, [r7, #28]
 8005886:	430a      	orrs	r2, r1
 8005888:	4919      	ldr	r1, [pc, #100]	; (80058f0 <step_reg_motor+0xa8>)
 800588a:	2034      	movs	r0, #52	; 0x34
 800588c:	fb00 f303 	mul.w	r3, r0, r3
 8005890:	440b      	add	r3, r1
 8005892:	331c      	adds	r3, #28
 8005894:	601a      	str	r2, [r3, #0]
 8005896:	7bfb      	ldrb	r3, [r7, #15]
 8005898:	6839      	ldr	r1, [r7, #0]
 800589a:	69ba      	ldr	r2, [r7, #24]
 800589c:	430a      	orrs	r2, r1
 800589e:	4914      	ldr	r1, [pc, #80]	; (80058f0 <step_reg_motor+0xa8>)
 80058a0:	2034      	movs	r0, #52	; 0x34
 80058a2:	fb00 f303 	mul.w	r3, r0, r3
 80058a6:	440b      	add	r3, r1
 80058a8:	3318      	adds	r3, #24
 80058aa:	601a      	str	r2, [r3, #0]
 80058ac:	7bfb      	ldrb	r3, [r7, #15]
 80058ae:	6879      	ldr	r1, [r7, #4]
 80058b0:	69ba      	ldr	r2, [r7, #24]
 80058b2:	430a      	orrs	r2, r1
 80058b4:	490e      	ldr	r1, [pc, #56]	; (80058f0 <step_reg_motor+0xa8>)
 80058b6:	2034      	movs	r0, #52	; 0x34
 80058b8:	fb00 f303 	mul.w	r3, r0, r3
 80058bc:	440b      	add	r3, r1
 80058be:	3314      	adds	r3, #20
 80058c0:	601a      	str	r2, [r3, #0]
 80058c2:	687a      	ldr	r2, [r7, #4]
 80058c4:	683b      	ldr	r3, [r7, #0]
 80058c6:	431a      	orrs	r2, r3
 80058c8:	69bb      	ldr	r3, [r7, #24]
 80058ca:	ea42 0103 	orr.w	r1, r2, r3
 80058ce:	7bfb      	ldrb	r3, [r7, #15]
 80058d0:	69fa      	ldr	r2, [r7, #28]
 80058d2:	430a      	orrs	r2, r1
 80058d4:	4906      	ldr	r1, [pc, #24]	; (80058f0 <step_reg_motor+0xa8>)
 80058d6:	2034      	movs	r0, #52	; 0x34
 80058d8:	fb00 f303 	mul.w	r3, r0, r3
 80058dc:	440b      	add	r3, r1
 80058de:	3324      	adds	r3, #36	; 0x24
 80058e0:	601a      	str	r2, [r3, #0]
 80058e2:	bf00      	nop
 80058e4:	3714      	adds	r7, #20
 80058e6:	46bd      	mov	sp, r7
 80058e8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80058ec:	4770      	bx	lr
 80058ee:	bf00      	nop
 80058f0:	20000bc0 	.word	0x20000bc0

080058f4 <step_reg_limit_switch>:
 80058f4:	b480      	push	{r7}
 80058f6:	b085      	sub	sp, #20
 80058f8:	af00      	add	r7, sp, #0
 80058fa:	4603      	mov	r3, r0
 80058fc:	60b9      	str	r1, [r7, #8]
 80058fe:	607a      	str	r2, [r7, #4]
 8005900:	73fb      	strb	r3, [r7, #15]
 8005902:	7bfb      	ldrb	r3, [r7, #15]
 8005904:	4a0b      	ldr	r2, [pc, #44]	; (8005934 <step_reg_limit_switch+0x40>)
 8005906:	2134      	movs	r1, #52	; 0x34
 8005908:	fb01 f303 	mul.w	r3, r1, r3
 800590c:	4413      	add	r3, r2
 800590e:	3328      	adds	r3, #40	; 0x28
 8005910:	68ba      	ldr	r2, [r7, #8]
 8005912:	601a      	str	r2, [r3, #0]
 8005914:	7bfb      	ldrb	r3, [r7, #15]
 8005916:	4a07      	ldr	r2, [pc, #28]	; (8005934 <step_reg_limit_switch+0x40>)
 8005918:	2134      	movs	r1, #52	; 0x34
 800591a:	fb01 f303 	mul.w	r3, r1, r3
 800591e:	4413      	add	r3, r2
 8005920:	332c      	adds	r3, #44	; 0x2c
 8005922:	687a      	ldr	r2, [r7, #4]
 8005924:	601a      	str	r2, [r3, #0]
 8005926:	bf00      	nop
 8005928:	3714      	adds	r7, #20
 800592a:	46bd      	mov	sp, r7
 800592c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005930:	4770      	bx	lr
 8005932:	bf00      	nop
 8005934:	20000bc0 	.word	0x20000bc0

08005938 <step_make_step>:
 8005938:	b580      	push	{r7, lr}
 800593a:	b084      	sub	sp, #16
 800593c:	af00      	add	r7, sp, #0
 800593e:	4603      	mov	r3, r0
 8005940:	71fb      	strb	r3, [r7, #7]
 8005942:	79fb      	ldrb	r3, [r7, #7]
 8005944:	4a1f      	ldr	r2, [pc, #124]	; (80059c4 <step_make_step+0x8c>)
 8005946:	2134      	movs	r1, #52	; 0x34
 8005948:	fb01 f303 	mul.w	r3, r1, r3
 800594c:	4413      	add	r3, r2
 800594e:	3310      	adds	r3, #16
 8005950:	681b      	ldr	r3, [r3, #0]
 8005952:	4618      	mov	r0, r3
 8005954:	f7ff fe5e 	bl	8005614 <LL_GPIO_ReadInputPort>
 8005958:	60f8      	str	r0, [r7, #12]
 800595a:	79fb      	ldrb	r3, [r7, #7]
 800595c:	4a19      	ldr	r2, [pc, #100]	; (80059c4 <step_make_step+0x8c>)
 800595e:	2134      	movs	r1, #52	; 0x34
 8005960:	fb01 f303 	mul.w	r3, r1, r3
 8005964:	4413      	add	r3, r2
 8005966:	3324      	adds	r3, #36	; 0x24
 8005968:	681b      	ldr	r3, [r3, #0]
 800596a:	43db      	mvns	r3, r3
 800596c:	68fa      	ldr	r2, [r7, #12]
 800596e:	4013      	ands	r3, r2
 8005970:	60fb      	str	r3, [r7, #12]
 8005972:	79fa      	ldrb	r2, [r7, #7]
 8005974:	79fb      	ldrb	r3, [r7, #7]
 8005976:	4913      	ldr	r1, [pc, #76]	; (80059c4 <step_make_step+0x8c>)
 8005978:	2034      	movs	r0, #52	; 0x34
 800597a:	fb00 f303 	mul.w	r3, r0, r3
 800597e:	440b      	add	r3, r1
 8005980:	3308      	adds	r3, #8
 8005982:	681b      	ldr	r3, [r3, #0]
 8005984:	f003 0103 	and.w	r1, r3, #3
 8005988:	480e      	ldr	r0, [pc, #56]	; (80059c4 <step_make_step+0x8c>)
 800598a:	4613      	mov	r3, r2
 800598c:	005b      	lsls	r3, r3, #1
 800598e:	4413      	add	r3, r2
 8005990:	009b      	lsls	r3, r3, #2
 8005992:	4413      	add	r3, r2
 8005994:	440b      	add	r3, r1
 8005996:	3304      	adds	r3, #4
 8005998:	009b      	lsls	r3, r3, #2
 800599a:	4403      	add	r3, r0
 800599c:	685b      	ldr	r3, [r3, #4]
 800599e:	68fa      	ldr	r2, [r7, #12]
 80059a0:	4313      	orrs	r3, r2
 80059a2:	60fb      	str	r3, [r7, #12]
 80059a4:	79fb      	ldrb	r3, [r7, #7]
 80059a6:	4a07      	ldr	r2, [pc, #28]	; (80059c4 <step_make_step+0x8c>)
 80059a8:	2134      	movs	r1, #52	; 0x34
 80059aa:	fb01 f303 	mul.w	r3, r1, r3
 80059ae:	4413      	add	r3, r2
 80059b0:	3310      	adds	r3, #16
 80059b2:	681b      	ldr	r3, [r3, #0]
 80059b4:	68f9      	ldr	r1, [r7, #12]
 80059b6:	4618      	mov	r0, r3
 80059b8:	f7ff fe4d 	bl	8005656 <LL_GPIO_WriteOutputPort>
 80059bc:	bf00      	nop
 80059be:	3710      	adds	r7, #16
 80059c0:	46bd      	mov	sp, r7
 80059c2:	bd80      	pop	{r7, pc}
 80059c4:	20000bc0 	.word	0x20000bc0

080059c8 <step_stop>:
 80059c8:	b580      	push	{r7, lr}
 80059ca:	b084      	sub	sp, #16
 80059cc:	af00      	add	r7, sp, #0
 80059ce:	4603      	mov	r3, r0
 80059d0:	71fb      	strb	r3, [r7, #7]
 80059d2:	79fb      	ldrb	r3, [r7, #7]
 80059d4:	4a13      	ldr	r2, [pc, #76]	; (8005a24 <step_stop+0x5c>)
 80059d6:	2134      	movs	r1, #52	; 0x34
 80059d8:	fb01 f303 	mul.w	r3, r1, r3
 80059dc:	4413      	add	r3, r2
 80059de:	3310      	adds	r3, #16
 80059e0:	681b      	ldr	r3, [r3, #0]
 80059e2:	4618      	mov	r0, r3
 80059e4:	f7ff fe16 	bl	8005614 <LL_GPIO_ReadInputPort>
 80059e8:	60f8      	str	r0, [r7, #12]
 80059ea:	79fb      	ldrb	r3, [r7, #7]
 80059ec:	4a0d      	ldr	r2, [pc, #52]	; (8005a24 <step_stop+0x5c>)
 80059ee:	2134      	movs	r1, #52	; 0x34
 80059f0:	fb01 f303 	mul.w	r3, r1, r3
 80059f4:	4413      	add	r3, r2
 80059f6:	3324      	adds	r3, #36	; 0x24
 80059f8:	681b      	ldr	r3, [r3, #0]
 80059fa:	43db      	mvns	r3, r3
 80059fc:	68fa      	ldr	r2, [r7, #12]
 80059fe:	4013      	ands	r3, r2
 8005a00:	60fb      	str	r3, [r7, #12]
 8005a02:	79fb      	ldrb	r3, [r7, #7]
 8005a04:	4a07      	ldr	r2, [pc, #28]	; (8005a24 <step_stop+0x5c>)
 8005a06:	2134      	movs	r1, #52	; 0x34
 8005a08:	fb01 f303 	mul.w	r3, r1, r3
 8005a0c:	4413      	add	r3, r2
 8005a0e:	3310      	adds	r3, #16
 8005a10:	681b      	ldr	r3, [r3, #0]
 8005a12:	68f9      	ldr	r1, [r7, #12]
 8005a14:	4618      	mov	r0, r3
 8005a16:	f7ff fe1e 	bl	8005656 <LL_GPIO_WriteOutputPort>
 8005a1a:	bf00      	nop
 8005a1c:	3710      	adds	r7, #16
 8005a1e:	46bd      	mov	sp, r7
 8005a20:	bd80      	pop	{r7, pc}
 8005a22:	bf00      	nop
 8005a24:	20000bc0 	.word	0x20000bc0

08005a28 <step_is_reached_limit>:
 8005a28:	b580      	push	{r7, lr}
 8005a2a:	b082      	sub	sp, #8
 8005a2c:	af00      	add	r7, sp, #0
 8005a2e:	4603      	mov	r3, r0
 8005a30:	71fb      	strb	r3, [r7, #7]
 8005a32:	79fb      	ldrb	r3, [r7, #7]
 8005a34:	4a0d      	ldr	r2, [pc, #52]	; (8005a6c <step_is_reached_limit+0x44>)
 8005a36:	2134      	movs	r1, #52	; 0x34
 8005a38:	fb01 f303 	mul.w	r3, r1, r3
 8005a3c:	4413      	add	r3, r2
 8005a3e:	3328      	adds	r3, #40	; 0x28
 8005a40:	6818      	ldr	r0, [r3, #0]
 8005a42:	79fb      	ldrb	r3, [r7, #7]
 8005a44:	4a09      	ldr	r2, [pc, #36]	; (8005a6c <step_is_reached_limit+0x44>)
 8005a46:	2134      	movs	r1, #52	; 0x34
 8005a48:	fb01 f303 	mul.w	r3, r1, r3
 8005a4c:	4413      	add	r3, r2
 8005a4e:	332c      	adds	r3, #44	; 0x2c
 8005a50:	681b      	ldr	r3, [r3, #0]
 8005a52:	4619      	mov	r1, r3
 8005a54:	f7ff fdea 	bl	800562c <LL_GPIO_IsInputPinSet>
 8005a58:	4603      	mov	r3, r0
 8005a5a:	2b00      	cmp	r3, #0
 8005a5c:	bf0c      	ite	eq
 8005a5e:	2301      	moveq	r3, #1
 8005a60:	2300      	movne	r3, #0
 8005a62:	b2db      	uxtb	r3, r3
 8005a64:	4618      	mov	r0, r3
 8005a66:	3708      	adds	r7, #8
 8005a68:	46bd      	mov	sp, r7
 8005a6a:	bd80      	pop	{r7, pc}
 8005a6c:	20000bc0 	.word	0x20000bc0

08005a70 <step_init>:
 8005a70:	b580      	push	{r7, lr}
 8005a72:	b084      	sub	sp, #16
 8005a74:	af02      	add	r7, sp, #8
 8005a76:	2300      	movs	r3, #0
 8005a78:	607b      	str	r3, [r7, #4]
 8005a7a:	f7ff fe99 	bl	80057b0 <step_hw_config>
 8005a7e:	2320      	movs	r3, #32
 8005a80:	9301      	str	r3, [sp, #4]
 8005a82:	f44f 6380 	mov.w	r3, #1024	; 0x400
 8005a86:	9300      	str	r3, [sp, #0]
 8005a88:	f44f 7300 	mov.w	r3, #512	; 0x200
 8005a8c:	f44f 7280 	mov.w	r2, #256	; 0x100
 8005a90:	4938      	ldr	r1, [pc, #224]	; (8005b74 <step_init+0x104>)
 8005a92:	2000      	movs	r0, #0
 8005a94:	f7ff fed8 	bl	8005848 <step_reg_motor>
 8005a98:	2280      	movs	r2, #128	; 0x80
 8005a9a:	4936      	ldr	r1, [pc, #216]	; (8005b74 <step_init+0x104>)
 8005a9c:	2000      	movs	r0, #0
 8005a9e:	f7ff ff29 	bl	80058f4 <step_reg_limit_switch>
 8005aa2:	2300      	movs	r3, #0
 8005aa4:	607b      	str	r3, [r7, #4]
 8005aa6:	e05e      	b.n	8005b66 <step_init+0xf6>
 8005aa8:	4a33      	ldr	r2, [pc, #204]	; (8005b78 <step_init+0x108>)
 8005aaa:	687b      	ldr	r3, [r7, #4]
 8005aac:	2134      	movs	r1, #52	; 0x34
 8005aae:	fb01 f303 	mul.w	r3, r1, r3
 8005ab2:	4413      	add	r3, r2
 8005ab4:	2200      	movs	r2, #0
 8005ab6:	601a      	str	r2, [r3, #0]
 8005ab8:	4a2f      	ldr	r2, [pc, #188]	; (8005b78 <step_init+0x108>)
 8005aba:	687b      	ldr	r3, [r7, #4]
 8005abc:	2134      	movs	r1, #52	; 0x34
 8005abe:	fb01 f303 	mul.w	r3, r1, r3
 8005ac2:	4413      	add	r3, r2
 8005ac4:	330c      	adds	r3, #12
 8005ac6:	2200      	movs	r2, #0
 8005ac8:	601a      	str	r2, [r3, #0]
 8005aca:	4a2b      	ldr	r2, [pc, #172]	; (8005b78 <step_init+0x108>)
 8005acc:	687b      	ldr	r3, [r7, #4]
 8005ace:	2134      	movs	r1, #52	; 0x34
 8005ad0:	fb01 f303 	mul.w	r3, r1, r3
 8005ad4:	4413      	add	r3, r2
 8005ad6:	3308      	adds	r3, #8
 8005ad8:	2200      	movs	r2, #0
 8005ada:	601a      	str	r2, [r3, #0]
 8005adc:	687b      	ldr	r3, [r7, #4]
 8005ade:	b2db      	uxtb	r3, r3
 8005ae0:	2101      	movs	r1, #1
 8005ae2:	4618      	mov	r0, r3
 8005ae4:	f000 f8f6 	bl	8005cd4 <step_set_speed>
 8005ae8:	4a23      	ldr	r2, [pc, #140]	; (8005b78 <step_init+0x108>)
 8005aea:	687b      	ldr	r3, [r7, #4]
 8005aec:	2134      	movs	r1, #52	; 0x34
 8005aee:	fb01 f303 	mul.w	r3, r1, r3
 8005af2:	4413      	add	r3, r2
 8005af4:	3330      	adds	r3, #48	; 0x30
 8005af6:	781b      	ldrb	r3, [r3, #0]
 8005af8:	f023 0301 	bic.w	r3, r3, #1
 8005afc:	b2d8      	uxtb	r0, r3
 8005afe:	4a1e      	ldr	r2, [pc, #120]	; (8005b78 <step_init+0x108>)
 8005b00:	687b      	ldr	r3, [r7, #4]
 8005b02:	2134      	movs	r1, #52	; 0x34
 8005b04:	fb01 f303 	mul.w	r3, r1, r3
 8005b08:	4413      	add	r3, r2
 8005b0a:	3330      	adds	r3, #48	; 0x30
 8005b0c:	4602      	mov	r2, r0
 8005b0e:	701a      	strb	r2, [r3, #0]
 8005b10:	4a19      	ldr	r2, [pc, #100]	; (8005b78 <step_init+0x108>)
 8005b12:	687b      	ldr	r3, [r7, #4]
 8005b14:	2134      	movs	r1, #52	; 0x34
 8005b16:	fb01 f303 	mul.w	r3, r1, r3
 8005b1a:	4413      	add	r3, r2
 8005b1c:	3330      	adds	r3, #48	; 0x30
 8005b1e:	781b      	ldrb	r3, [r3, #0]
 8005b20:	f023 0304 	bic.w	r3, r3, #4
 8005b24:	b2d8      	uxtb	r0, r3
 8005b26:	4a14      	ldr	r2, [pc, #80]	; (8005b78 <step_init+0x108>)
 8005b28:	687b      	ldr	r3, [r7, #4]
 8005b2a:	2134      	movs	r1, #52	; 0x34
 8005b2c:	fb01 f303 	mul.w	r3, r1, r3
 8005b30:	4413      	add	r3, r2
 8005b32:	3330      	adds	r3, #48	; 0x30
 8005b34:	4602      	mov	r2, r0
 8005b36:	701a      	strb	r2, [r3, #0]
 8005b38:	4a0f      	ldr	r2, [pc, #60]	; (8005b78 <step_init+0x108>)
 8005b3a:	687b      	ldr	r3, [r7, #4]
 8005b3c:	2134      	movs	r1, #52	; 0x34
 8005b3e:	fb01 f303 	mul.w	r3, r1, r3
 8005b42:	4413      	add	r3, r2
 8005b44:	3330      	adds	r3, #48	; 0x30
 8005b46:	781b      	ldrb	r3, [r3, #0]
 8005b48:	f023 0302 	bic.w	r3, r3, #2
 8005b4c:	b2d8      	uxtb	r0, r3
 8005b4e:	4a0a      	ldr	r2, [pc, #40]	; (8005b78 <step_init+0x108>)
 8005b50:	687b      	ldr	r3, [r7, #4]
 8005b52:	2134      	movs	r1, #52	; 0x34
 8005b54:	fb01 f303 	mul.w	r3, r1, r3
 8005b58:	4413      	add	r3, r2
 8005b5a:	3330      	adds	r3, #48	; 0x30
 8005b5c:	4602      	mov	r2, r0
 8005b5e:	701a      	strb	r2, [r3, #0]
 8005b60:	687b      	ldr	r3, [r7, #4]
 8005b62:	3301      	adds	r3, #1
 8005b64:	607b      	str	r3, [r7, #4]
 8005b66:	687b      	ldr	r3, [r7, #4]
 8005b68:	2b00      	cmp	r3, #0
 8005b6a:	dd9d      	ble.n	8005aa8 <step_init+0x38>
 8005b6c:	bf00      	nop
 8005b6e:	3708      	adds	r7, #8
 8005b70:	46bd      	mov	sp, r7
 8005b72:	bd80      	pop	{r7, pc}
 8005b74:	40021000 	.word	0x40021000
 8005b78:	20000bc0 	.word	0x20000bc0

08005b7c <step_stop_motors>:
 8005b7c:	b580      	push	{r7, lr}
 8005b7e:	b082      	sub	sp, #8
 8005b80:	af00      	add	r7, sp, #0
 8005b82:	2300      	movs	r3, #0
 8005b84:	607b      	str	r3, [r7, #4]
 8005b86:	2300      	movs	r3, #0
 8005b88:	607b      	str	r3, [r7, #4]
 8005b8a:	e007      	b.n	8005b9c <step_stop_motors+0x20>
 8005b8c:	687b      	ldr	r3, [r7, #4]
 8005b8e:	b2db      	uxtb	r3, r3
 8005b90:	4618      	mov	r0, r3
 8005b92:	f7ff ff19 	bl	80059c8 <step_stop>
 8005b96:	687b      	ldr	r3, [r7, #4]
 8005b98:	3301      	adds	r3, #1
 8005b9a:	607b      	str	r3, [r7, #4]
 8005b9c:	687b      	ldr	r3, [r7, #4]
 8005b9e:	2b00      	cmp	r3, #0
 8005ba0:	ddf4      	ble.n	8005b8c <step_stop_motors+0x10>
 8005ba2:	4803      	ldr	r0, [pc, #12]	; (8005bb0 <step_stop_motors+0x34>)
 8005ba4:	f7ff fda6 	bl	80056f4 <LL_TIM_DisableCounter>
 8005ba8:	bf00      	nop
 8005baa:	3708      	adds	r7, #8
 8005bac:	46bd      	mov	sp, r7
 8005bae:	bd80      	pop	{r7, pc}
 8005bb0:	40014000 	.word	0x40014000

08005bb4 <step_start_calibration>:
 8005bb4:	b580      	push	{r7, lr}
 8005bb6:	b082      	sub	sp, #8
 8005bb8:	af00      	add	r7, sp, #0
 8005bba:	4603      	mov	r3, r0
 8005bbc:	71fb      	strb	r3, [r7, #7]
 8005bbe:	79fb      	ldrb	r3, [r7, #7]
 8005bc0:	2b00      	cmp	r3, #0
 8005bc2:	d002      	beq.n	8005bca <step_start_calibration+0x16>
 8005bc4:	f04f 33ff 	mov.w	r3, #4294967295
 8005bc8:	e03f      	b.n	8005c4a <step_start_calibration+0x96>
 8005bca:	79fb      	ldrb	r3, [r7, #7]
 8005bcc:	4a21      	ldr	r2, [pc, #132]	; (8005c54 <step_start_calibration+0xa0>)
 8005bce:	2134      	movs	r1, #52	; 0x34
 8005bd0:	fb01 f303 	mul.w	r3, r1, r3
 8005bd4:	4413      	add	r3, r2
 8005bd6:	3330      	adds	r3, #48	; 0x30
 8005bd8:	781a      	ldrb	r2, [r3, #0]
 8005bda:	79fb      	ldrb	r3, [r7, #7]
 8005bdc:	f022 0204 	bic.w	r2, r2, #4
 8005be0:	b2d0      	uxtb	r0, r2
 8005be2:	4a1c      	ldr	r2, [pc, #112]	; (8005c54 <step_start_calibration+0xa0>)
 8005be4:	2134      	movs	r1, #52	; 0x34
 8005be6:	fb01 f303 	mul.w	r3, r1, r3
 8005bea:	4413      	add	r3, r2
 8005bec:	3330      	adds	r3, #48	; 0x30
 8005bee:	4602      	mov	r2, r0
 8005bf0:	701a      	strb	r2, [r3, #0]
 8005bf2:	79fb      	ldrb	r3, [r7, #7]
 8005bf4:	4a17      	ldr	r2, [pc, #92]	; (8005c54 <step_start_calibration+0xa0>)
 8005bf6:	2134      	movs	r1, #52	; 0x34
 8005bf8:	fb01 f303 	mul.w	r3, r1, r3
 8005bfc:	4413      	add	r3, r2
 8005bfe:	3330      	adds	r3, #48	; 0x30
 8005c00:	781a      	ldrb	r2, [r3, #0]
 8005c02:	79fb      	ldrb	r3, [r7, #7]
 8005c04:	f042 0202 	orr.w	r2, r2, #2
 8005c08:	b2d0      	uxtb	r0, r2
 8005c0a:	4a12      	ldr	r2, [pc, #72]	; (8005c54 <step_start_calibration+0xa0>)
 8005c0c:	2134      	movs	r1, #52	; 0x34
 8005c0e:	fb01 f303 	mul.w	r3, r1, r3
 8005c12:	4413      	add	r3, r2
 8005c14:	3330      	adds	r3, #48	; 0x30
 8005c16:	4602      	mov	r2, r0
 8005c18:	701a      	strb	r2, [r3, #0]
 8005c1a:	79fb      	ldrb	r3, [r7, #7]
 8005c1c:	4a0d      	ldr	r2, [pc, #52]	; (8005c54 <step_start_calibration+0xa0>)
 8005c1e:	2134      	movs	r1, #52	; 0x34
 8005c20:	fb01 f303 	mul.w	r3, r1, r3
 8005c24:	4413      	add	r3, r2
 8005c26:	3330      	adds	r3, #48	; 0x30
 8005c28:	781a      	ldrb	r2, [r3, #0]
 8005c2a:	79fb      	ldrb	r3, [r7, #7]
 8005c2c:	f042 0201 	orr.w	r2, r2, #1
 8005c30:	b2d0      	uxtb	r0, r2
 8005c32:	4a08      	ldr	r2, [pc, #32]	; (8005c54 <step_start_calibration+0xa0>)
 8005c34:	2134      	movs	r1, #52	; 0x34
 8005c36:	fb01 f303 	mul.w	r3, r1, r3
 8005c3a:	4413      	add	r3, r2
 8005c3c:	3330      	adds	r3, #48	; 0x30
 8005c3e:	4602      	mov	r2, r0
 8005c40:	701a      	strb	r2, [r3, #0]
 8005c42:	4805      	ldr	r0, [pc, #20]	; (8005c58 <step_start_calibration+0xa4>)
 8005c44:	f7ff fd46 	bl	80056d4 <LL_TIM_EnableCounter>
 8005c48:	2300      	movs	r3, #0
 8005c4a:	4618      	mov	r0, r3
 8005c4c:	3708      	adds	r7, #8
 8005c4e:	46bd      	mov	sp, r7
 8005c50:	bd80      	pop	{r7, pc}
 8005c52:	bf00      	nop
 8005c54:	20000bc0 	.word	0x20000bc0
 8005c58:	40014000 	.word	0x40014000

08005c5c <step_is_calibrated>:
 8005c5c:	b480      	push	{r7}
 8005c5e:	b083      	sub	sp, #12
 8005c60:	af00      	add	r7, sp, #0
 8005c62:	4603      	mov	r3, r0
 8005c64:	71fb      	strb	r3, [r7, #7]
 8005c66:	79fb      	ldrb	r3, [r7, #7]
 8005c68:	2b00      	cmp	r3, #0
 8005c6a:	d002      	beq.n	8005c72 <step_is_calibrated+0x16>
 8005c6c:	f04f 33ff 	mov.w	r3, #4294967295
 8005c70:	e009      	b.n	8005c86 <step_is_calibrated+0x2a>
 8005c72:	79fb      	ldrb	r3, [r7, #7]
 8005c74:	4a07      	ldr	r2, [pc, #28]	; (8005c94 <step_is_calibrated+0x38>)
 8005c76:	2134      	movs	r1, #52	; 0x34
 8005c78:	fb01 f303 	mul.w	r3, r1, r3
 8005c7c:	4413      	add	r3, r2
 8005c7e:	3330      	adds	r3, #48	; 0x30
 8005c80:	781b      	ldrb	r3, [r3, #0]
 8005c82:	f003 0304 	and.w	r3, r3, #4
 8005c86:	4618      	mov	r0, r3
 8005c88:	370c      	adds	r7, #12
 8005c8a:	46bd      	mov	sp, r7
 8005c8c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005c90:	4770      	bx	lr
 8005c92:	bf00      	nop
 8005c94:	20000bc0 	.word	0x20000bc0

08005c98 <step_is_running>:
 8005c98:	b480      	push	{r7}
 8005c9a:	b083      	sub	sp, #12
 8005c9c:	af00      	add	r7, sp, #0
 8005c9e:	4603      	mov	r3, r0
 8005ca0:	71fb      	strb	r3, [r7, #7]
 8005ca2:	79fb      	ldrb	r3, [r7, #7]
 8005ca4:	2b00      	cmp	r3, #0
 8005ca6:	d002      	beq.n	8005cae <step_is_running+0x16>
 8005ca8:	f04f 33ff 	mov.w	r3, #4294967295
 8005cac:	e009      	b.n	8005cc2 <step_is_running+0x2a>
 8005cae:	79fb      	ldrb	r3, [r7, #7]
 8005cb0:	4a07      	ldr	r2, [pc, #28]	; (8005cd0 <step_is_running+0x38>)
 8005cb2:	2134      	movs	r1, #52	; 0x34
 8005cb4:	fb01 f303 	mul.w	r3, r1, r3
 8005cb8:	4413      	add	r3, r2
 8005cba:	3330      	adds	r3, #48	; 0x30
 8005cbc:	781b      	ldrb	r3, [r3, #0]
 8005cbe:	f003 0301 	and.w	r3, r3, #1
 8005cc2:	4618      	mov	r0, r3
 8005cc4:	370c      	adds	r7, #12
 8005cc6:	46bd      	mov	sp, r7
 8005cc8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005ccc:	4770      	bx	lr
 8005cce:	bf00      	nop
 8005cd0:	20000bc0 	.word	0x20000bc0

08005cd4 <step_set_speed>:
 8005cd4:	b480      	push	{r7}
 8005cd6:	b083      	sub	sp, #12
 8005cd8:	af00      	add	r7, sp, #0
 8005cda:	4603      	mov	r3, r0
 8005cdc:	460a      	mov	r2, r1
 8005cde:	71fb      	strb	r3, [r7, #7]
 8005ce0:	4613      	mov	r3, r2
 8005ce2:	71bb      	strb	r3, [r7, #6]
 8005ce4:	79fb      	ldrb	r3, [r7, #7]
 8005ce6:	2b00      	cmp	r3, #0
 8005ce8:	d002      	beq.n	8005cf0 <step_set_speed+0x1c>
 8005cea:	f04f 33ff 	mov.w	r3, #4294967295
 8005cee:	e009      	b.n	8005d04 <step_set_speed+0x30>
 8005cf0:	79fb      	ldrb	r3, [r7, #7]
 8005cf2:	79ba      	ldrb	r2, [r7, #6]
 8005cf4:	4906      	ldr	r1, [pc, #24]	; (8005d10 <step_set_speed+0x3c>)
 8005cf6:	2034      	movs	r0, #52	; 0x34
 8005cf8:	fb00 f303 	mul.w	r3, r0, r3
 8005cfc:	440b      	add	r3, r1
 8005cfe:	3304      	adds	r3, #4
 8005d00:	601a      	str	r2, [r3, #0]
 8005d02:	2300      	movs	r3, #0
 8005d04:	4618      	mov	r0, r3
 8005d06:	370c      	adds	r7, #12
 8005d08:	46bd      	mov	sp, r7
 8005d0a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005d0e:	4770      	bx	lr
 8005d10:	20000bc0 	.word	0x20000bc0

08005d14 <step_set_step_goal>:
 8005d14:	b580      	push	{r7, lr}
 8005d16:	b082      	sub	sp, #8
 8005d18:	af00      	add	r7, sp, #0
 8005d1a:	4603      	mov	r3, r0
 8005d1c:	6039      	str	r1, [r7, #0]
 8005d1e:	71fb      	strb	r3, [r7, #7]
 8005d20:	79fb      	ldrb	r3, [r7, #7]
 8005d22:	2b00      	cmp	r3, #0
 8005d24:	d002      	beq.n	8005d2c <step_set_step_goal+0x18>
 8005d26:	f04f 33ff 	mov.w	r3, #4294967295
 8005d2a:	e02e      	b.n	8005d8a <step_set_step_goal+0x76>
 8005d2c:	683b      	ldr	r3, [r7, #0]
 8005d2e:	f5b3 7ffc 	cmp.w	r3, #504	; 0x1f8
 8005d32:	d902      	bls.n	8005d3a <step_set_step_goal+0x26>
 8005d34:	f04f 33ff 	mov.w	r3, #4294967295
 8005d38:	e027      	b.n	8005d8a <step_set_step_goal+0x76>
 8005d3a:	79fb      	ldrb	r3, [r7, #7]
 8005d3c:	4a15      	ldr	r2, [pc, #84]	; (8005d94 <step_set_step_goal+0x80>)
 8005d3e:	2134      	movs	r1, #52	; 0x34
 8005d40:	fb01 f303 	mul.w	r3, r1, r3
 8005d44:	4413      	add	r3, r2
 8005d46:	3330      	adds	r3, #48	; 0x30
 8005d48:	781b      	ldrb	r3, [r3, #0]
 8005d4a:	f003 0301 	and.w	r3, r3, #1
 8005d4e:	2b00      	cmp	r3, #0
 8005d50:	d10b      	bne.n	8005d6a <step_set_step_goal+0x56>
 8005d52:	79fb      	ldrb	r3, [r7, #7]
 8005d54:	4a0f      	ldr	r2, [pc, #60]	; (8005d94 <step_set_step_goal+0x80>)
 8005d56:	2134      	movs	r1, #52	; 0x34
 8005d58:	fb01 f303 	mul.w	r3, r1, r3
 8005d5c:	4413      	add	r3, r2
 8005d5e:	3330      	adds	r3, #48	; 0x30
 8005d60:	781b      	ldrb	r3, [r3, #0]
 8005d62:	f003 0304 	and.w	r3, r3, #4
 8005d66:	2b00      	cmp	r3, #0
 8005d68:	d102      	bne.n	8005d70 <step_set_step_goal+0x5c>
 8005d6a:	f04f 33ff 	mov.w	r3, #4294967295
 8005d6e:	e00c      	b.n	8005d8a <step_set_step_goal+0x76>
 8005d70:	79fb      	ldrb	r3, [r7, #7]
 8005d72:	4a08      	ldr	r2, [pc, #32]	; (8005d94 <step_set_step_goal+0x80>)
 8005d74:	2134      	movs	r1, #52	; 0x34
 8005d76:	fb01 f303 	mul.w	r3, r1, r3
 8005d7a:	4413      	add	r3, r2
 8005d7c:	330c      	adds	r3, #12
 8005d7e:	683a      	ldr	r2, [r7, #0]
 8005d80:	601a      	str	r2, [r3, #0]
 8005d82:	4805      	ldr	r0, [pc, #20]	; (8005d98 <step_set_step_goal+0x84>)
 8005d84:	f7ff fca6 	bl	80056d4 <LL_TIM_EnableCounter>
 8005d88:	2300      	movs	r3, #0
 8005d8a:	4618      	mov	r0, r3
 8005d8c:	3708      	adds	r7, #8
 8005d8e:	46bd      	mov	sp, r7
 8005d90:	bd80      	pop	{r7, pc}
 8005d92:	bf00      	nop
 8005d94:	20000bc0 	.word	0x20000bc0
 8005d98:	40014000 	.word	0x40014000

08005d9c <step_get_current_step>:
 8005d9c:	b480      	push	{r7}
 8005d9e:	b083      	sub	sp, #12
 8005da0:	af00      	add	r7, sp, #0
 8005da2:	4603      	mov	r3, r0
 8005da4:	71fb      	strb	r3, [r7, #7]
 8005da6:	79fb      	ldrb	r3, [r7, #7]
 8005da8:	4a06      	ldr	r2, [pc, #24]	; (8005dc4 <step_get_current_step+0x28>)
 8005daa:	2134      	movs	r1, #52	; 0x34
 8005dac:	fb01 f303 	mul.w	r3, r1, r3
 8005db0:	4413      	add	r3, r2
 8005db2:	3308      	adds	r3, #8
 8005db4:	681b      	ldr	r3, [r3, #0]
 8005db6:	4618      	mov	r0, r3
 8005db8:	370c      	adds	r7, #12
 8005dba:	46bd      	mov	sp, r7
 8005dbc:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005dc0:	4770      	bx	lr
 8005dc2:	bf00      	nop
 8005dc4:	20000bc0 	.word	0x20000bc0

08005dc8 <TIM1_BRK_TIM9_IRQHandler>:
 8005dc8:	b580      	push	{r7, lr}
 8005dca:	b084      	sub	sp, #16
 8005dcc:	af00      	add	r7, sp, #0
 8005dce:	2300      	movs	r3, #0
 8005dd0:	60bb      	str	r3, [r7, #8]
 8005dd2:	2300      	movs	r3, #0
 8005dd4:	60fb      	str	r3, [r7, #12]
 8005dd6:	2300      	movs	r3, #0
 8005dd8:	607b      	str	r3, [r7, #4]
 8005dda:	48a1      	ldr	r0, [pc, #644]	; (8006060 <TIM1_BRK_TIM9_IRQHandler+0x298>)
 8005ddc:	f7ff fcc9 	bl	8005772 <LL_TIM_ClearFlag_UPDATE>
 8005de0:	2300      	movs	r3, #0
 8005de2:	60fb      	str	r3, [r7, #12]
 8005de4:	e128      	b.n	8006038 <TIM1_BRK_TIM9_IRQHandler+0x270>
 8005de6:	4a9f      	ldr	r2, [pc, #636]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005de8:	68fb      	ldr	r3, [r7, #12]
 8005dea:	2134      	movs	r1, #52	; 0x34
 8005dec:	fb01 f303 	mul.w	r3, r1, r3
 8005df0:	4413      	add	r3, r2
 8005df2:	681b      	ldr	r3, [r3, #0]
 8005df4:	1c5a      	adds	r2, r3, #1
 8005df6:	499b      	ldr	r1, [pc, #620]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005df8:	68fb      	ldr	r3, [r7, #12]
 8005dfa:	2034      	movs	r0, #52	; 0x34
 8005dfc:	fb00 f303 	mul.w	r3, r0, r3
 8005e00:	440b      	add	r3, r1
 8005e02:	601a      	str	r2, [r3, #0]
 8005e04:	4a97      	ldr	r2, [pc, #604]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e06:	68fb      	ldr	r3, [r7, #12]
 8005e08:	2134      	movs	r1, #52	; 0x34
 8005e0a:	fb01 f303 	mul.w	r3, r1, r3
 8005e0e:	4413      	add	r3, r2
 8005e10:	681a      	ldr	r2, [r3, #0]
 8005e12:	4994      	ldr	r1, [pc, #592]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e14:	68fb      	ldr	r3, [r7, #12]
 8005e16:	2034      	movs	r0, #52	; 0x34
 8005e18:	fb00 f303 	mul.w	r3, r0, r3
 8005e1c:	440b      	add	r3, r1
 8005e1e:	3304      	adds	r3, #4
 8005e20:	681b      	ldr	r3, [r3, #0]
 8005e22:	429a      	cmp	r2, r3
 8005e24:	d371      	bcc.n	8005f0a <TIM1_BRK_TIM9_IRQHandler+0x142>
 8005e26:	4a8f      	ldr	r2, [pc, #572]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e28:	68fb      	ldr	r3, [r7, #12]
 8005e2a:	2134      	movs	r1, #52	; 0x34
 8005e2c:	fb01 f303 	mul.w	r3, r1, r3
 8005e30:	4413      	add	r3, r2
 8005e32:	3330      	adds	r3, #48	; 0x30
 8005e34:	781b      	ldrb	r3, [r3, #0]
 8005e36:	f003 0304 	and.w	r3, r3, #4
 8005e3a:	2b00      	cmp	r3, #0
 8005e3c:	d065      	beq.n	8005f0a <TIM1_BRK_TIM9_IRQHandler+0x142>
 8005e3e:	4a89      	ldr	r2, [pc, #548]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e40:	68fb      	ldr	r3, [r7, #12]
 8005e42:	2134      	movs	r1, #52	; 0x34
 8005e44:	fb01 f303 	mul.w	r3, r1, r3
 8005e48:	4413      	add	r3, r2
 8005e4a:	2200      	movs	r2, #0
 8005e4c:	601a      	str	r2, [r3, #0]
 8005e4e:	4a85      	ldr	r2, [pc, #532]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e50:	68fb      	ldr	r3, [r7, #12]
 8005e52:	2134      	movs	r1, #52	; 0x34
 8005e54:	fb01 f303 	mul.w	r3, r1, r3
 8005e58:	4413      	add	r3, r2
 8005e5a:	330c      	adds	r3, #12
 8005e5c:	681a      	ldr	r2, [r3, #0]
 8005e5e:	4981      	ldr	r1, [pc, #516]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e60:	68fb      	ldr	r3, [r7, #12]
 8005e62:	2034      	movs	r0, #52	; 0x34
 8005e64:	fb00 f303 	mul.w	r3, r0, r3
 8005e68:	440b      	add	r3, r1
 8005e6a:	3308      	adds	r3, #8
 8005e6c:	681b      	ldr	r3, [r3, #0]
 8005e6e:	1ad3      	subs	r3, r2, r3
 8005e70:	607b      	str	r3, [r7, #4]
 8005e72:	687b      	ldr	r3, [r7, #4]
 8005e74:	2b00      	cmp	r3, #0
 8005e76:	d031      	beq.n	8005edc <TIM1_BRK_TIM9_IRQHandler+0x114>
 8005e78:	4a7a      	ldr	r2, [pc, #488]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e7a:	68fb      	ldr	r3, [r7, #12]
 8005e7c:	2134      	movs	r1, #52	; 0x34
 8005e7e:	fb01 f303 	mul.w	r3, r1, r3
 8005e82:	4413      	add	r3, r2
 8005e84:	3330      	adds	r3, #48	; 0x30
 8005e86:	781b      	ldrb	r3, [r3, #0]
 8005e88:	f043 0301 	orr.w	r3, r3, #1
 8005e8c:	b2d8      	uxtb	r0, r3
 8005e8e:	4a75      	ldr	r2, [pc, #468]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005e90:	68fb      	ldr	r3, [r7, #12]
 8005e92:	2134      	movs	r1, #52	; 0x34
 8005e94:	fb01 f303 	mul.w	r3, r1, r3
 8005e98:	4413      	add	r3, r2
 8005e9a:	3330      	adds	r3, #48	; 0x30
 8005e9c:	4602      	mov	r2, r0
 8005e9e:	701a      	strb	r2, [r3, #0]
 8005ea0:	4a70      	ldr	r2, [pc, #448]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005ea2:	68fb      	ldr	r3, [r7, #12]
 8005ea4:	2134      	movs	r1, #52	; 0x34
 8005ea6:	fb01 f303 	mul.w	r3, r1, r3
 8005eaa:	4413      	add	r3, r2
 8005eac:	3308      	adds	r3, #8
 8005eae:	681b      	ldr	r3, [r3, #0]
 8005eb0:	687a      	ldr	r2, [r7, #4]
 8005eb2:	2a00      	cmp	r2, #0
 8005eb4:	dd01      	ble.n	8005eba <TIM1_BRK_TIM9_IRQHandler+0xf2>
 8005eb6:	2201      	movs	r2, #1
 8005eb8:	e001      	b.n	8005ebe <TIM1_BRK_TIM9_IRQHandler+0xf6>
 8005eba:	f04f 32ff 	mov.w	r2, #4294967295
 8005ebe:	441a      	add	r2, r3
 8005ec0:	4968      	ldr	r1, [pc, #416]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005ec2:	68fb      	ldr	r3, [r7, #12]
 8005ec4:	2034      	movs	r0, #52	; 0x34
 8005ec6:	fb00 f303 	mul.w	r3, r0, r3
 8005eca:	440b      	add	r3, r1
 8005ecc:	3308      	adds	r3, #8
 8005ece:	601a      	str	r2, [r3, #0]
 8005ed0:	68fb      	ldr	r3, [r7, #12]
 8005ed2:	b2db      	uxtb	r3, r3
 8005ed4:	4618      	mov	r0, r3
 8005ed6:	f7ff fd2f 	bl	8005938 <step_make_step>
 8005eda:	e016      	b.n	8005f0a <TIM1_BRK_TIM9_IRQHandler+0x142>
 8005edc:	4a61      	ldr	r2, [pc, #388]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005ede:	68fb      	ldr	r3, [r7, #12]
 8005ee0:	2134      	movs	r1, #52	; 0x34
 8005ee2:	fb01 f303 	mul.w	r3, r1, r3
 8005ee6:	4413      	add	r3, r2
 8005ee8:	3330      	adds	r3, #48	; 0x30
 8005eea:	781b      	ldrb	r3, [r3, #0]
 8005eec:	f023 0301 	bic.w	r3, r3, #1
 8005ef0:	b2d8      	uxtb	r0, r3
 8005ef2:	4a5c      	ldr	r2, [pc, #368]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005ef4:	68fb      	ldr	r3, [r7, #12]
 8005ef6:	2134      	movs	r1, #52	; 0x34
 8005ef8:	fb01 f303 	mul.w	r3, r1, r3
 8005efc:	4413      	add	r3, r2
 8005efe:	3330      	adds	r3, #48	; 0x30
 8005f00:	4602      	mov	r2, r0
 8005f02:	701a      	strb	r2, [r3, #0]
 8005f04:	4856      	ldr	r0, [pc, #344]	; (8006060 <TIM1_BRK_TIM9_IRQHandler+0x298>)
 8005f06:	f7ff fbf5 	bl	80056f4 <LL_TIM_DisableCounter>
 8005f0a:	4a56      	ldr	r2, [pc, #344]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f0c:	68fb      	ldr	r3, [r7, #12]
 8005f0e:	2134      	movs	r1, #52	; 0x34
 8005f10:	fb01 f303 	mul.w	r3, r1, r3
 8005f14:	4413      	add	r3, r2
 8005f16:	3330      	adds	r3, #48	; 0x30
 8005f18:	781b      	ldrb	r3, [r3, #0]
 8005f1a:	f003 0302 	and.w	r3, r3, #2
 8005f1e:	2b00      	cmp	r3, #0
 8005f20:	f000 8087 	beq.w	8006032 <TIM1_BRK_TIM9_IRQHandler+0x26a>
 8005f24:	4a4f      	ldr	r2, [pc, #316]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f26:	68fb      	ldr	r3, [r7, #12]
 8005f28:	2134      	movs	r1, #52	; 0x34
 8005f2a:	fb01 f303 	mul.w	r3, r1, r3
 8005f2e:	4413      	add	r3, r2
 8005f30:	3330      	adds	r3, #48	; 0x30
 8005f32:	781b      	ldrb	r3, [r3, #0]
 8005f34:	f043 0301 	orr.w	r3, r3, #1
 8005f38:	b2d8      	uxtb	r0, r3
 8005f3a:	4a4a      	ldr	r2, [pc, #296]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f3c:	68fb      	ldr	r3, [r7, #12]
 8005f3e:	2134      	movs	r1, #52	; 0x34
 8005f40:	fb01 f303 	mul.w	r3, r1, r3
 8005f44:	4413      	add	r3, r2
 8005f46:	3330      	adds	r3, #48	; 0x30
 8005f48:	4602      	mov	r2, r0
 8005f4a:	701a      	strb	r2, [r3, #0]
 8005f4c:	68fb      	ldr	r3, [r7, #12]
 8005f4e:	b2db      	uxtb	r3, r3
 8005f50:	4618      	mov	r0, r3
 8005f52:	f7ff fcf1 	bl	8005938 <step_make_step>
 8005f56:	4a43      	ldr	r2, [pc, #268]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f58:	68fb      	ldr	r3, [r7, #12]
 8005f5a:	2134      	movs	r1, #52	; 0x34
 8005f5c:	fb01 f303 	mul.w	r3, r1, r3
 8005f60:	4413      	add	r3, r2
 8005f62:	3308      	adds	r3, #8
 8005f64:	681b      	ldr	r3, [r3, #0]
 8005f66:	1c5a      	adds	r2, r3, #1
 8005f68:	493e      	ldr	r1, [pc, #248]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f6a:	68fb      	ldr	r3, [r7, #12]
 8005f6c:	2034      	movs	r0, #52	; 0x34
 8005f6e:	fb00 f303 	mul.w	r3, r0, r3
 8005f72:	440b      	add	r3, r1
 8005f74:	3308      	adds	r3, #8
 8005f76:	601a      	str	r2, [r3, #0]
 8005f78:	68fb      	ldr	r3, [r7, #12]
 8005f7a:	b2db      	uxtb	r3, r3
 8005f7c:	4618      	mov	r0, r3
 8005f7e:	f7ff fd53 	bl	8005a28 <step_is_reached_limit>
 8005f82:	4603      	mov	r3, r0
 8005f84:	2b00      	cmp	r3, #0
 8005f86:	d054      	beq.n	8006032 <TIM1_BRK_TIM9_IRQHandler+0x26a>
 8005f88:	68fb      	ldr	r3, [r7, #12]
 8005f8a:	b2db      	uxtb	r3, r3
 8005f8c:	4618      	mov	r0, r3
 8005f8e:	f7ff fd1b 	bl	80059c8 <step_stop>
 8005f92:	4a34      	ldr	r2, [pc, #208]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005f94:	68fb      	ldr	r3, [r7, #12]
 8005f96:	2134      	movs	r1, #52	; 0x34
 8005f98:	fb01 f303 	mul.w	r3, r1, r3
 8005f9c:	4413      	add	r3, r2
 8005f9e:	3308      	adds	r3, #8
 8005fa0:	2200      	movs	r2, #0
 8005fa2:	601a      	str	r2, [r3, #0]
 8005fa4:	4a2f      	ldr	r2, [pc, #188]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005fa6:	68fb      	ldr	r3, [r7, #12]
 8005fa8:	2134      	movs	r1, #52	; 0x34
 8005faa:	fb01 f303 	mul.w	r3, r1, r3
 8005fae:	4413      	add	r3, r2
 8005fb0:	3330      	adds	r3, #48	; 0x30
 8005fb2:	781b      	ldrb	r3, [r3, #0]
 8005fb4:	f043 0304 	orr.w	r3, r3, #4
 8005fb8:	b2d8      	uxtb	r0, r3
 8005fba:	4a2a      	ldr	r2, [pc, #168]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005fbc:	68fb      	ldr	r3, [r7, #12]
 8005fbe:	2134      	movs	r1, #52	; 0x34
 8005fc0:	fb01 f303 	mul.w	r3, r1, r3
 8005fc4:	4413      	add	r3, r2
 8005fc6:	3330      	adds	r3, #48	; 0x30
 8005fc8:	4602      	mov	r2, r0
 8005fca:	701a      	strb	r2, [r3, #0]
 8005fcc:	4a25      	ldr	r2, [pc, #148]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005fce:	68fb      	ldr	r3, [r7, #12]
 8005fd0:	2134      	movs	r1, #52	; 0x34
 8005fd2:	fb01 f303 	mul.w	r3, r1, r3
 8005fd6:	4413      	add	r3, r2
 8005fd8:	3330      	adds	r3, #48	; 0x30
 8005fda:	781b      	ldrb	r3, [r3, #0]
 8005fdc:	f023 0301 	bic.w	r3, r3, #1
 8005fe0:	b2d8      	uxtb	r0, r3
 8005fe2:	4a20      	ldr	r2, [pc, #128]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005fe4:	68fb      	ldr	r3, [r7, #12]
 8005fe6:	2134      	movs	r1, #52	; 0x34
 8005fe8:	fb01 f303 	mul.w	r3, r1, r3
 8005fec:	4413      	add	r3, r2
 8005fee:	3330      	adds	r3, #48	; 0x30
 8005ff0:	4602      	mov	r2, r0
 8005ff2:	701a      	strb	r2, [r3, #0]
 8005ff4:	4a1b      	ldr	r2, [pc, #108]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 8005ff6:	68fb      	ldr	r3, [r7, #12]
 8005ff8:	2134      	movs	r1, #52	; 0x34
 8005ffa:	fb01 f303 	mul.w	r3, r1, r3
 8005ffe:	4413      	add	r3, r2
 8006000:	3330      	adds	r3, #48	; 0x30
 8006002:	781b      	ldrb	r3, [r3, #0]
 8006004:	f023 0302 	bic.w	r3, r3, #2
 8006008:	b2d8      	uxtb	r0, r3
 800600a:	4a16      	ldr	r2, [pc, #88]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 800600c:	68fb      	ldr	r3, [r7, #12]
 800600e:	2134      	movs	r1, #52	; 0x34
 8006010:	fb01 f303 	mul.w	r3, r1, r3
 8006014:	4413      	add	r3, r2
 8006016:	3330      	adds	r3, #48	; 0x30
 8006018:	4602      	mov	r2, r0
 800601a:	701a      	strb	r2, [r3, #0]
 800601c:	4a11      	ldr	r2, [pc, #68]	; (8006064 <TIM1_BRK_TIM9_IRQHandler+0x29c>)
 800601e:	68fb      	ldr	r3, [r7, #12]
 8006020:	2134      	movs	r1, #52	; 0x34
 8006022:	fb01 f303 	mul.w	r3, r1, r3
 8006026:	4413      	add	r3, r2
 8006028:	2200      	movs	r2, #0
 800602a:	601a      	str	r2, [r3, #0]
 800602c:	480c      	ldr	r0, [pc, #48]	; (8006060 <TIM1_BRK_TIM9_IRQHandler+0x298>)
 800602e:	f7ff fb61 	bl	80056f4 <LL_TIM_DisableCounter>
 8006032:	68fb      	ldr	r3, [r7, #12]
 8006034:	3301      	adds	r3, #1
 8006036:	60fb      	str	r3, [r7, #12]
 8006038:	68fb      	ldr	r3, [r7, #12]
 800603a:	2b00      	cmp	r3, #0
 800603c:	f77f aed3 	ble.w	8005de6 <TIM1_BRK_TIM9_IRQHandler+0x1e>
 8006040:	68bb      	ldr	r3, [r7, #8]
 8006042:	2b00      	cmp	r3, #0
 8006044:	d007      	beq.n	8006056 <TIM1_BRK_TIM9_IRQHandler+0x28e>
 8006046:	4b08      	ldr	r3, [pc, #32]	; (8006068 <TIM1_BRK_TIM9_IRQHandler+0x2a0>)
 8006048:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800604c:	601a      	str	r2, [r3, #0]
 800604e:	f3bf 8f4f 	dsb	sy
 8006052:	f3bf 8f6f 	isb	sy
 8006056:	bf00      	nop
 8006058:	3710      	adds	r7, #16
 800605a:	46bd      	mov	sp, r7
 800605c:	bd80      	pop	{r7, pc}
 800605e:	bf00      	nop
 8006060:	40014000 	.word	0x40014000
 8006064:	20000bc0 	.word	0x20000bc0
 8006068:	e000ed04 	.word	0xe000ed04

0800606c <NVIC_EnableIRQ>:
 800606c:	b480      	push	{r7}
 800606e:	b083      	sub	sp, #12
 8006070:	af00      	add	r7, sp, #0
 8006072:	4603      	mov	r3, r0
 8006074:	71fb      	strb	r3, [r7, #7]
 8006076:	79fb      	ldrb	r3, [r7, #7]
 8006078:	f003 021f 	and.w	r2, r3, #31
 800607c:	4907      	ldr	r1, [pc, #28]	; (800609c <NVIC_EnableIRQ+0x30>)
 800607e:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8006082:	095b      	lsrs	r3, r3, #5
 8006084:	2001      	movs	r0, #1
 8006086:	fa00 f202 	lsl.w	r2, r0, r2
 800608a:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 800608e:	bf00      	nop
 8006090:	370c      	adds	r7, #12
 8006092:	46bd      	mov	sp, r7
 8006094:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006098:	4770      	bx	lr
 800609a:	bf00      	nop
 800609c:	e000e100 	.word	0xe000e100

080060a0 <NVIC_SetPriority>:
 80060a0:	b480      	push	{r7}
 80060a2:	b083      	sub	sp, #12
 80060a4:	af00      	add	r7, sp, #0
 80060a6:	4603      	mov	r3, r0
 80060a8:	6039      	str	r1, [r7, #0]
 80060aa:	71fb      	strb	r3, [r7, #7]
 80060ac:	f997 3007 	ldrsb.w	r3, [r7, #7]
 80060b0:	2b00      	cmp	r3, #0
 80060b2:	da0b      	bge.n	80060cc <NVIC_SetPriority+0x2c>
 80060b4:	683b      	ldr	r3, [r7, #0]
 80060b6:	b2da      	uxtb	r2, r3
 80060b8:	490c      	ldr	r1, [pc, #48]	; (80060ec <NVIC_SetPriority+0x4c>)
 80060ba:	79fb      	ldrb	r3, [r7, #7]
 80060bc:	f003 030f 	and.w	r3, r3, #15
 80060c0:	3b04      	subs	r3, #4
 80060c2:	0112      	lsls	r2, r2, #4
 80060c4:	b2d2      	uxtb	r2, r2
 80060c6:	440b      	add	r3, r1
 80060c8:	761a      	strb	r2, [r3, #24]
 80060ca:	e009      	b.n	80060e0 <NVIC_SetPriority+0x40>
 80060cc:	683b      	ldr	r3, [r7, #0]
 80060ce:	b2da      	uxtb	r2, r3
 80060d0:	4907      	ldr	r1, [pc, #28]	; (80060f0 <NVIC_SetPriority+0x50>)
 80060d2:	f997 3007 	ldrsb.w	r3, [r7, #7]
 80060d6:	0112      	lsls	r2, r2, #4
 80060d8:	b2d2      	uxtb	r2, r2
 80060da:	440b      	add	r3, r1
 80060dc:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 80060e0:	bf00      	nop
 80060e2:	370c      	adds	r7, #12
 80060e4:	46bd      	mov	sp, r7
 80060e6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80060ea:	4770      	bx	lr
 80060ec:	e000ed00 	.word	0xe000ed00
 80060f0:	e000e100 	.word	0xe000e100

080060f4 <LL_USART_Enable>:
 80060f4:	b480      	push	{r7}
 80060f6:	b083      	sub	sp, #12
 80060f8:	af00      	add	r7, sp, #0
 80060fa:	6078      	str	r0, [r7, #4]
 80060fc:	687b      	ldr	r3, [r7, #4]
 80060fe:	68db      	ldr	r3, [r3, #12]
 8006100:	f443 5200 	orr.w	r2, r3, #8192	; 0x2000
 8006104:	687b      	ldr	r3, [r7, #4]
 8006106:	60da      	str	r2, [r3, #12]
 8006108:	bf00      	nop
 800610a:	370c      	adds	r7, #12
 800610c:	46bd      	mov	sp, r7
 800610e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006112:	4770      	bx	lr

08006114 <LL_USART_EnableDirectionRx>:
 8006114:	b480      	push	{r7}
 8006116:	b083      	sub	sp, #12
 8006118:	af00      	add	r7, sp, #0
 800611a:	6078      	str	r0, [r7, #4]
 800611c:	687b      	ldr	r3, [r7, #4]
 800611e:	68db      	ldr	r3, [r3, #12]
 8006120:	f043 0204 	orr.w	r2, r3, #4
 8006124:	687b      	ldr	r3, [r7, #4]
 8006126:	60da      	str	r2, [r3, #12]
 8006128:	bf00      	nop
 800612a:	370c      	adds	r7, #12
 800612c:	46bd      	mov	sp, r7
 800612e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006132:	4770      	bx	lr

08006134 <LL_USART_EnableDirectionTx>:
 8006134:	b480      	push	{r7}
 8006136:	b083      	sub	sp, #12
 8006138:	af00      	add	r7, sp, #0
 800613a:	6078      	str	r0, [r7, #4]
 800613c:	687b      	ldr	r3, [r7, #4]
 800613e:	68db      	ldr	r3, [r3, #12]
 8006140:	f043 0208 	orr.w	r2, r3, #8
 8006144:	687b      	ldr	r3, [r7, #4]
 8006146:	60da      	str	r2, [r3, #12]
 8006148:	bf00      	nop
 800614a:	370c      	adds	r7, #12
 800614c:	46bd      	mov	sp, r7
 800614e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006152:	4770      	bx	lr

08006154 <LL_USART_SetTransferDirection>:
 8006154:	b480      	push	{r7}
 8006156:	b083      	sub	sp, #12
 8006158:	af00      	add	r7, sp, #0
 800615a:	6078      	str	r0, [r7, #4]
 800615c:	6039      	str	r1, [r7, #0]
 800615e:	687b      	ldr	r3, [r7, #4]
 8006160:	68db      	ldr	r3, [r3, #12]
 8006162:	f023 020c 	bic.w	r2, r3, #12
 8006166:	683b      	ldr	r3, [r7, #0]
 8006168:	431a      	orrs	r2, r3
 800616a:	687b      	ldr	r3, [r7, #4]
 800616c:	60da      	str	r2, [r3, #12]
 800616e:	bf00      	nop
 8006170:	370c      	adds	r7, #12
 8006172:	46bd      	mov	sp, r7
 8006174:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006178:	4770      	bx	lr

0800617a <LL_USART_SetParity>:
 800617a:	b480      	push	{r7}
 800617c:	b083      	sub	sp, #12
 800617e:	af00      	add	r7, sp, #0
 8006180:	6078      	str	r0, [r7, #4]
 8006182:	6039      	str	r1, [r7, #0]
 8006184:	687b      	ldr	r3, [r7, #4]
 8006186:	68db      	ldr	r3, [r3, #12]
 8006188:	f423 62c0 	bic.w	r2, r3, #1536	; 0x600
 800618c:	683b      	ldr	r3, [r7, #0]
 800618e:	431a      	orrs	r2, r3
 8006190:	687b      	ldr	r3, [r7, #4]
 8006192:	60da      	str	r2, [r3, #12]
 8006194:	bf00      	nop
 8006196:	370c      	adds	r7, #12
 8006198:	46bd      	mov	sp, r7
 800619a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800619e:	4770      	bx	lr

080061a0 <LL_USART_SetDataWidth>:
 80061a0:	b480      	push	{r7}
 80061a2:	b083      	sub	sp, #12
 80061a4:	af00      	add	r7, sp, #0
 80061a6:	6078      	str	r0, [r7, #4]
 80061a8:	6039      	str	r1, [r7, #0]
 80061aa:	687b      	ldr	r3, [r7, #4]
 80061ac:	68db      	ldr	r3, [r3, #12]
 80061ae:	f423 5280 	bic.w	r2, r3, #4096	; 0x1000
 80061b2:	683b      	ldr	r3, [r7, #0]
 80061b4:	431a      	orrs	r2, r3
 80061b6:	687b      	ldr	r3, [r7, #4]
 80061b8:	60da      	str	r2, [r3, #12]
 80061ba:	bf00      	nop
 80061bc:	370c      	adds	r7, #12
 80061be:	46bd      	mov	sp, r7
 80061c0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80061c4:	4770      	bx	lr

080061c6 <LL_USART_SetStopBitsLength>:
 80061c6:	b480      	push	{r7}
 80061c8:	b083      	sub	sp, #12
 80061ca:	af00      	add	r7, sp, #0
 80061cc:	6078      	str	r0, [r7, #4]
 80061ce:	6039      	str	r1, [r7, #0]
 80061d0:	687b      	ldr	r3, [r7, #4]
 80061d2:	691b      	ldr	r3, [r3, #16]
 80061d4:	f423 5240 	bic.w	r2, r3, #12288	; 0x3000
 80061d8:	683b      	ldr	r3, [r7, #0]
 80061da:	431a      	orrs	r2, r3
 80061dc:	687b      	ldr	r3, [r7, #4]
 80061de:	611a      	str	r2, [r3, #16]
 80061e0:	bf00      	nop
 80061e2:	370c      	adds	r7, #12
 80061e4:	46bd      	mov	sp, r7
 80061e6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80061ea:	4770      	bx	lr

080061ec <LL_USART_SetHWFlowCtrl>:
 80061ec:	b480      	push	{r7}
 80061ee:	b083      	sub	sp, #12
 80061f0:	af00      	add	r7, sp, #0
 80061f2:	6078      	str	r0, [r7, #4]
 80061f4:	6039      	str	r1, [r7, #0]
 80061f6:	687b      	ldr	r3, [r7, #4]
 80061f8:	695b      	ldr	r3, [r3, #20]
 80061fa:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 80061fe:	683b      	ldr	r3, [r7, #0]
 8006200:	431a      	orrs	r2, r3
 8006202:	687b      	ldr	r3, [r7, #4]
 8006204:	615a      	str	r2, [r3, #20]
 8006206:	bf00      	nop
 8006208:	370c      	adds	r7, #12
 800620a:	46bd      	mov	sp, r7
 800620c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006210:	4770      	bx	lr
	...

08006214 <LL_USART_SetBaudRate>:
 8006214:	b480      	push	{r7}
 8006216:	b085      	sub	sp, #20
 8006218:	af00      	add	r7, sp, #0
 800621a:	60f8      	str	r0, [r7, #12]
 800621c:	60b9      	str	r1, [r7, #8]
 800621e:	607a      	str	r2, [r7, #4]
 8006220:	603b      	str	r3, [r7, #0]
 8006222:	687b      	ldr	r3, [r7, #4]
 8006224:	f5b3 4f00 	cmp.w	r3, #32768	; 0x8000
 8006228:	d152      	bne.n	80062d0 <LL_USART_SetBaudRate+0xbc>
 800622a:	68ba      	ldr	r2, [r7, #8]
 800622c:	4613      	mov	r3, r2
 800622e:	009b      	lsls	r3, r3, #2
 8006230:	4413      	add	r3, r2
 8006232:	009a      	lsls	r2, r3, #2
 8006234:	441a      	add	r2, r3
 8006236:	683b      	ldr	r3, [r7, #0]
 8006238:	005b      	lsls	r3, r3, #1
 800623a:	fbb2 f3f3 	udiv	r3, r2, r3
 800623e:	4a4f      	ldr	r2, [pc, #316]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006240:	fba2 2303 	umull	r2, r3, r2, r3
 8006244:	095b      	lsrs	r3, r3, #5
 8006246:	b29b      	uxth	r3, r3
 8006248:	011b      	lsls	r3, r3, #4
 800624a:	b299      	uxth	r1, r3
 800624c:	68ba      	ldr	r2, [r7, #8]
 800624e:	4613      	mov	r3, r2
 8006250:	009b      	lsls	r3, r3, #2
 8006252:	4413      	add	r3, r2
 8006254:	009a      	lsls	r2, r3, #2
 8006256:	441a      	add	r2, r3
 8006258:	683b      	ldr	r3, [r7, #0]
 800625a:	005b      	lsls	r3, r3, #1
 800625c:	fbb2 f2f3 	udiv	r2, r2, r3
 8006260:	4b46      	ldr	r3, [pc, #280]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006262:	fba3 0302 	umull	r0, r3, r3, r2
 8006266:	095b      	lsrs	r3, r3, #5
 8006268:	2064      	movs	r0, #100	; 0x64
 800626a:	fb00 f303 	mul.w	r3, r0, r3
 800626e:	1ad3      	subs	r3, r2, r3
 8006270:	00db      	lsls	r3, r3, #3
 8006272:	3332      	adds	r3, #50	; 0x32
 8006274:	4a41      	ldr	r2, [pc, #260]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006276:	fba2 2303 	umull	r2, r3, r2, r3
 800627a:	095b      	lsrs	r3, r3, #5
 800627c:	b29b      	uxth	r3, r3
 800627e:	005b      	lsls	r3, r3, #1
 8006280:	b29b      	uxth	r3, r3
 8006282:	f403 73f8 	and.w	r3, r3, #496	; 0x1f0
 8006286:	b29b      	uxth	r3, r3
 8006288:	440b      	add	r3, r1
 800628a:	b299      	uxth	r1, r3
 800628c:	68ba      	ldr	r2, [r7, #8]
 800628e:	4613      	mov	r3, r2
 8006290:	009b      	lsls	r3, r3, #2
 8006292:	4413      	add	r3, r2
 8006294:	009a      	lsls	r2, r3, #2
 8006296:	441a      	add	r2, r3
 8006298:	683b      	ldr	r3, [r7, #0]
 800629a:	005b      	lsls	r3, r3, #1
 800629c:	fbb2 f2f3 	udiv	r2, r2, r3
 80062a0:	4b36      	ldr	r3, [pc, #216]	; (800637c <LL_USART_SetBaudRate+0x168>)
 80062a2:	fba3 0302 	umull	r0, r3, r3, r2
 80062a6:	095b      	lsrs	r3, r3, #5
 80062a8:	2064      	movs	r0, #100	; 0x64
 80062aa:	fb00 f303 	mul.w	r3, r0, r3
 80062ae:	1ad3      	subs	r3, r2, r3
 80062b0:	00db      	lsls	r3, r3, #3
 80062b2:	3332      	adds	r3, #50	; 0x32
 80062b4:	4a31      	ldr	r2, [pc, #196]	; (800637c <LL_USART_SetBaudRate+0x168>)
 80062b6:	fba2 2303 	umull	r2, r3, r2, r3
 80062ba:	095b      	lsrs	r3, r3, #5
 80062bc:	b29b      	uxth	r3, r3
 80062be:	f003 0307 	and.w	r3, r3, #7
 80062c2:	b29b      	uxth	r3, r3
 80062c4:	440b      	add	r3, r1
 80062c6:	b29b      	uxth	r3, r3
 80062c8:	461a      	mov	r2, r3
 80062ca:	68fb      	ldr	r3, [r7, #12]
 80062cc:	609a      	str	r2, [r3, #8]
 80062ce:	e04f      	b.n	8006370 <LL_USART_SetBaudRate+0x15c>
 80062d0:	68ba      	ldr	r2, [r7, #8]
 80062d2:	4613      	mov	r3, r2
 80062d4:	009b      	lsls	r3, r3, #2
 80062d6:	4413      	add	r3, r2
 80062d8:	009a      	lsls	r2, r3, #2
 80062da:	441a      	add	r2, r3
 80062dc:	683b      	ldr	r3, [r7, #0]
 80062de:	009b      	lsls	r3, r3, #2
 80062e0:	fbb2 f3f3 	udiv	r3, r2, r3
 80062e4:	4a25      	ldr	r2, [pc, #148]	; (800637c <LL_USART_SetBaudRate+0x168>)
 80062e6:	fba2 2303 	umull	r2, r3, r2, r3
 80062ea:	095b      	lsrs	r3, r3, #5
 80062ec:	b29b      	uxth	r3, r3
 80062ee:	011b      	lsls	r3, r3, #4
 80062f0:	b299      	uxth	r1, r3
 80062f2:	68ba      	ldr	r2, [r7, #8]
 80062f4:	4613      	mov	r3, r2
 80062f6:	009b      	lsls	r3, r3, #2
 80062f8:	4413      	add	r3, r2
 80062fa:	009a      	lsls	r2, r3, #2
 80062fc:	441a      	add	r2, r3
 80062fe:	683b      	ldr	r3, [r7, #0]
 8006300:	009b      	lsls	r3, r3, #2
 8006302:	fbb2 f2f3 	udiv	r2, r2, r3
 8006306:	4b1d      	ldr	r3, [pc, #116]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006308:	fba3 0302 	umull	r0, r3, r3, r2
 800630c:	095b      	lsrs	r3, r3, #5
 800630e:	2064      	movs	r0, #100	; 0x64
 8006310:	fb00 f303 	mul.w	r3, r0, r3
 8006314:	1ad3      	subs	r3, r2, r3
 8006316:	011b      	lsls	r3, r3, #4
 8006318:	3332      	adds	r3, #50	; 0x32
 800631a:	4a18      	ldr	r2, [pc, #96]	; (800637c <LL_USART_SetBaudRate+0x168>)
 800631c:	fba2 2303 	umull	r2, r3, r2, r3
 8006320:	095b      	lsrs	r3, r3, #5
 8006322:	b29b      	uxth	r3, r3
 8006324:	f003 03f0 	and.w	r3, r3, #240	; 0xf0
 8006328:	b29b      	uxth	r3, r3
 800632a:	440b      	add	r3, r1
 800632c:	b299      	uxth	r1, r3
 800632e:	68ba      	ldr	r2, [r7, #8]
 8006330:	4613      	mov	r3, r2
 8006332:	009b      	lsls	r3, r3, #2
 8006334:	4413      	add	r3, r2
 8006336:	009a      	lsls	r2, r3, #2
 8006338:	441a      	add	r2, r3
 800633a:	683b      	ldr	r3, [r7, #0]
 800633c:	009b      	lsls	r3, r3, #2
 800633e:	fbb2 f2f3 	udiv	r2, r2, r3
 8006342:	4b0e      	ldr	r3, [pc, #56]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006344:	fba3 0302 	umull	r0, r3, r3, r2
 8006348:	095b      	lsrs	r3, r3, #5
 800634a:	2064      	movs	r0, #100	; 0x64
 800634c:	fb00 f303 	mul.w	r3, r0, r3
 8006350:	1ad3      	subs	r3, r2, r3
 8006352:	011b      	lsls	r3, r3, #4
 8006354:	3332      	adds	r3, #50	; 0x32
 8006356:	4a09      	ldr	r2, [pc, #36]	; (800637c <LL_USART_SetBaudRate+0x168>)
 8006358:	fba2 2303 	umull	r2, r3, r2, r3
 800635c:	095b      	lsrs	r3, r3, #5
 800635e:	b29b      	uxth	r3, r3
 8006360:	f003 030f 	and.w	r3, r3, #15
 8006364:	b29b      	uxth	r3, r3
 8006366:	440b      	add	r3, r1
 8006368:	b29b      	uxth	r3, r3
 800636a:	461a      	mov	r2, r3
 800636c:	68fb      	ldr	r3, [r7, #12]
 800636e:	609a      	str	r2, [r3, #8]
 8006370:	bf00      	nop
 8006372:	3714      	adds	r7, #20
 8006374:	46bd      	mov	sp, r7
 8006376:	f85d 7b04 	ldr.w	r7, [sp], #4
 800637a:	4770      	bx	lr
 800637c:	51eb851f 	.word	0x51eb851f

08006380 <LL_USART_IsActiveFlag_TC>:
 8006380:	b480      	push	{r7}
 8006382:	b083      	sub	sp, #12
 8006384:	af00      	add	r7, sp, #0
 8006386:	6078      	str	r0, [r7, #4]
 8006388:	687b      	ldr	r3, [r7, #4]
 800638a:	681b      	ldr	r3, [r3, #0]
 800638c:	f003 0340 	and.w	r3, r3, #64	; 0x40
 8006390:	2b40      	cmp	r3, #64	; 0x40
 8006392:	bf0c      	ite	eq
 8006394:	2301      	moveq	r3, #1
 8006396:	2300      	movne	r3, #0
 8006398:	b2db      	uxtb	r3, r3
 800639a:	4618      	mov	r0, r3
 800639c:	370c      	adds	r7, #12
 800639e:	46bd      	mov	sp, r7
 80063a0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80063a4:	4770      	bx	lr

080063a6 <LL_USART_IsActiveFlag_TXE>:
 80063a6:	b480      	push	{r7}
 80063a8:	b083      	sub	sp, #12
 80063aa:	af00      	add	r7, sp, #0
 80063ac:	6078      	str	r0, [r7, #4]
 80063ae:	687b      	ldr	r3, [r7, #4]
 80063b0:	681b      	ldr	r3, [r3, #0]
 80063b2:	f003 0380 	and.w	r3, r3, #128	; 0x80
 80063b6:	2b80      	cmp	r3, #128	; 0x80
 80063b8:	bf0c      	ite	eq
 80063ba:	2301      	moveq	r3, #1
 80063bc:	2300      	movne	r3, #0
 80063be:	b2db      	uxtb	r3, r3
 80063c0:	4618      	mov	r0, r3
 80063c2:	370c      	adds	r7, #12
 80063c4:	46bd      	mov	sp, r7
 80063c6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80063ca:	4770      	bx	lr

080063cc <LL_USART_ClearFlag_IDLE>:
 80063cc:	b480      	push	{r7}
 80063ce:	b085      	sub	sp, #20
 80063d0:	af00      	add	r7, sp, #0
 80063d2:	6078      	str	r0, [r7, #4]
 80063d4:	687b      	ldr	r3, [r7, #4]
 80063d6:	681b      	ldr	r3, [r3, #0]
 80063d8:	60fb      	str	r3, [r7, #12]
 80063da:	68fb      	ldr	r3, [r7, #12]
 80063dc:	687b      	ldr	r3, [r7, #4]
 80063de:	685b      	ldr	r3, [r3, #4]
 80063e0:	60fb      	str	r3, [r7, #12]
 80063e2:	68fb      	ldr	r3, [r7, #12]
 80063e4:	bf00      	nop
 80063e6:	3714      	adds	r7, #20
 80063e8:	46bd      	mov	sp, r7
 80063ea:	f85d 7b04 	ldr.w	r7, [sp], #4
 80063ee:	4770      	bx	lr

080063f0 <LL_USART_ClearFlag_TC>:
 80063f0:	b480      	push	{r7}
 80063f2:	b083      	sub	sp, #12
 80063f4:	af00      	add	r7, sp, #0
 80063f6:	6078      	str	r0, [r7, #4]
 80063f8:	687b      	ldr	r3, [r7, #4]
 80063fa:	f06f 0240 	mvn.w	r2, #64	; 0x40
 80063fe:	601a      	str	r2, [r3, #0]
 8006400:	bf00      	nop
 8006402:	370c      	adds	r7, #12
 8006404:	46bd      	mov	sp, r7
 8006406:	f85d 7b04 	ldr.w	r7, [sp], #4
 800640a:	4770      	bx	lr

0800640c <LL_USART_EnableIT_IDLE>:
 800640c:	b480      	push	{r7}
 800640e:	b083      	sub	sp, #12
 8006410:	af00      	add	r7, sp, #0
 8006412:	6078      	str	r0, [r7, #4]
 8006414:	687b      	ldr	r3, [r7, #4]
 8006416:	68db      	ldr	r3, [r3, #12]
 8006418:	f043 0210 	orr.w	r2, r3, #16
 800641c:	687b      	ldr	r3, [r7, #4]
 800641e:	60da      	str	r2, [r3, #12]
 8006420:	bf00      	nop
 8006422:	370c      	adds	r7, #12
 8006424:	46bd      	mov	sp, r7
 8006426:	f85d 7b04 	ldr.w	r7, [sp], #4
 800642a:	4770      	bx	lr

0800642c <LL_USART_EnableDMAReq_RX>:
 800642c:	b480      	push	{r7}
 800642e:	b083      	sub	sp, #12
 8006430:	af00      	add	r7, sp, #0
 8006432:	6078      	str	r0, [r7, #4]
 8006434:	687b      	ldr	r3, [r7, #4]
 8006436:	695b      	ldr	r3, [r3, #20]
 8006438:	f043 0240 	orr.w	r2, r3, #64	; 0x40
 800643c:	687b      	ldr	r3, [r7, #4]
 800643e:	615a      	str	r2, [r3, #20]
 8006440:	bf00      	nop
 8006442:	370c      	adds	r7, #12
 8006444:	46bd      	mov	sp, r7
 8006446:	f85d 7b04 	ldr.w	r7, [sp], #4
 800644a:	4770      	bx	lr

0800644c <LL_USART_TransmitData8>:
 800644c:	b480      	push	{r7}
 800644e:	b083      	sub	sp, #12
 8006450:	af00      	add	r7, sp, #0
 8006452:	6078      	str	r0, [r7, #4]
 8006454:	460b      	mov	r3, r1
 8006456:	70fb      	strb	r3, [r7, #3]
 8006458:	78fa      	ldrb	r2, [r7, #3]
 800645a:	687b      	ldr	r3, [r7, #4]
 800645c:	605a      	str	r2, [r3, #4]
 800645e:	bf00      	nop
 8006460:	370c      	adds	r7, #12
 8006462:	46bd      	mov	sp, r7
 8006464:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006468:	4770      	bx	lr
	...

0800646c <LL_AHB1_GRP1_EnableClock>:
 800646c:	b480      	push	{r7}
 800646e:	b085      	sub	sp, #20
 8006470:	af00      	add	r7, sp, #0
 8006472:	6078      	str	r0, [r7, #4]
 8006474:	4b08      	ldr	r3, [pc, #32]	; (8006498 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8006476:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8006478:	4907      	ldr	r1, [pc, #28]	; (8006498 <LL_AHB1_GRP1_EnableClock+0x2c>)
 800647a:	687b      	ldr	r3, [r7, #4]
 800647c:	4313      	orrs	r3, r2
 800647e:	630b      	str	r3, [r1, #48]	; 0x30
 8006480:	4b05      	ldr	r3, [pc, #20]	; (8006498 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8006482:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8006484:	687b      	ldr	r3, [r7, #4]
 8006486:	4013      	ands	r3, r2
 8006488:	60fb      	str	r3, [r7, #12]
 800648a:	68fb      	ldr	r3, [r7, #12]
 800648c:	bf00      	nop
 800648e:	3714      	adds	r7, #20
 8006490:	46bd      	mov	sp, r7
 8006492:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006496:	4770      	bx	lr
 8006498:	40023800 	.word	0x40023800

0800649c <LL_APB1_GRP1_EnableClock>:
 800649c:	b480      	push	{r7}
 800649e:	b085      	sub	sp, #20
 80064a0:	af00      	add	r7, sp, #0
 80064a2:	6078      	str	r0, [r7, #4]
 80064a4:	4b08      	ldr	r3, [pc, #32]	; (80064c8 <LL_APB1_GRP1_EnableClock+0x2c>)
 80064a6:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 80064a8:	4907      	ldr	r1, [pc, #28]	; (80064c8 <LL_APB1_GRP1_EnableClock+0x2c>)
 80064aa:	687b      	ldr	r3, [r7, #4]
 80064ac:	4313      	orrs	r3, r2
 80064ae:	640b      	str	r3, [r1, #64]	; 0x40
 80064b0:	4b05      	ldr	r3, [pc, #20]	; (80064c8 <LL_APB1_GRP1_EnableClock+0x2c>)
 80064b2:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 80064b4:	687b      	ldr	r3, [r7, #4]
 80064b6:	4013      	ands	r3, r2
 80064b8:	60fb      	str	r3, [r7, #12]
 80064ba:	68fb      	ldr	r3, [r7, #12]
 80064bc:	bf00      	nop
 80064be:	3714      	adds	r7, #20
 80064c0:	46bd      	mov	sp, r7
 80064c2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80064c6:	4770      	bx	lr
 80064c8:	40023800 	.word	0x40023800

080064cc <LL_APB2_GRP1_EnableClock>:
 80064cc:	b480      	push	{r7}
 80064ce:	b085      	sub	sp, #20
 80064d0:	af00      	add	r7, sp, #0
 80064d2:	6078      	str	r0, [r7, #4]
 80064d4:	4b08      	ldr	r3, [pc, #32]	; (80064f8 <LL_APB2_GRP1_EnableClock+0x2c>)
 80064d6:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80064d8:	4907      	ldr	r1, [pc, #28]	; (80064f8 <LL_APB2_GRP1_EnableClock+0x2c>)
 80064da:	687b      	ldr	r3, [r7, #4]
 80064dc:	4313      	orrs	r3, r2
 80064de:	644b      	str	r3, [r1, #68]	; 0x44
 80064e0:	4b05      	ldr	r3, [pc, #20]	; (80064f8 <LL_APB2_GRP1_EnableClock+0x2c>)
 80064e2:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80064e4:	687b      	ldr	r3, [r7, #4]
 80064e6:	4013      	ands	r3, r2
 80064e8:	60fb      	str	r3, [r7, #12]
 80064ea:	68fb      	ldr	r3, [r7, #12]
 80064ec:	bf00      	nop
 80064ee:	3714      	adds	r7, #20
 80064f0:	46bd      	mov	sp, r7
 80064f2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80064f6:	4770      	bx	lr
 80064f8:	40023800 	.word	0x40023800

080064fc <LL_DMA_EnableStream>:
 80064fc:	b480      	push	{r7}
 80064fe:	b083      	sub	sp, #12
 8006500:	af00      	add	r7, sp, #0
 8006502:	6078      	str	r0, [r7, #4]
 8006504:	6039      	str	r1, [r7, #0]
 8006506:	4a0c      	ldr	r2, [pc, #48]	; (8006538 <LL_DMA_EnableStream+0x3c>)
 8006508:	683b      	ldr	r3, [r7, #0]
 800650a:	4413      	add	r3, r2
 800650c:	781b      	ldrb	r3, [r3, #0]
 800650e:	461a      	mov	r2, r3
 8006510:	687b      	ldr	r3, [r7, #4]
 8006512:	4413      	add	r3, r2
 8006514:	681b      	ldr	r3, [r3, #0]
 8006516:	4908      	ldr	r1, [pc, #32]	; (8006538 <LL_DMA_EnableStream+0x3c>)
 8006518:	683a      	ldr	r2, [r7, #0]
 800651a:	440a      	add	r2, r1
 800651c:	7812      	ldrb	r2, [r2, #0]
 800651e:	4611      	mov	r1, r2
 8006520:	687a      	ldr	r2, [r7, #4]
 8006522:	440a      	add	r2, r1
 8006524:	f043 0301 	orr.w	r3, r3, #1
 8006528:	6013      	str	r3, [r2, #0]
 800652a:	bf00      	nop
 800652c:	370c      	adds	r7, #12
 800652e:	46bd      	mov	sp, r7
 8006530:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006534:	4770      	bx	lr
 8006536:	bf00      	nop
 8006538:	0800d160 	.word	0x0800d160

0800653c <LL_DMA_DisableStream>:
 800653c:	b480      	push	{r7}
 800653e:	b083      	sub	sp, #12
 8006540:	af00      	add	r7, sp, #0
 8006542:	6078      	str	r0, [r7, #4]
 8006544:	6039      	str	r1, [r7, #0]
 8006546:	4a0c      	ldr	r2, [pc, #48]	; (8006578 <LL_DMA_DisableStream+0x3c>)
 8006548:	683b      	ldr	r3, [r7, #0]
 800654a:	4413      	add	r3, r2
 800654c:	781b      	ldrb	r3, [r3, #0]
 800654e:	461a      	mov	r2, r3
 8006550:	687b      	ldr	r3, [r7, #4]
 8006552:	4413      	add	r3, r2
 8006554:	681b      	ldr	r3, [r3, #0]
 8006556:	4908      	ldr	r1, [pc, #32]	; (8006578 <LL_DMA_DisableStream+0x3c>)
 8006558:	683a      	ldr	r2, [r7, #0]
 800655a:	440a      	add	r2, r1
 800655c:	7812      	ldrb	r2, [r2, #0]
 800655e:	4611      	mov	r1, r2
 8006560:	687a      	ldr	r2, [r7, #4]
 8006562:	440a      	add	r2, r1
 8006564:	f023 0301 	bic.w	r3, r3, #1
 8006568:	6013      	str	r3, [r2, #0]
 800656a:	bf00      	nop
 800656c:	370c      	adds	r7, #12
 800656e:	46bd      	mov	sp, r7
 8006570:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006574:	4770      	bx	lr
 8006576:	bf00      	nop
 8006578:	0800d160 	.word	0x0800d160

0800657c <LL_DMA_SetMemoryIncMode>:
 800657c:	b480      	push	{r7}
 800657e:	b085      	sub	sp, #20
 8006580:	af00      	add	r7, sp, #0
 8006582:	60f8      	str	r0, [r7, #12]
 8006584:	60b9      	str	r1, [r7, #8]
 8006586:	607a      	str	r2, [r7, #4]
 8006588:	4a0d      	ldr	r2, [pc, #52]	; (80065c0 <LL_DMA_SetMemoryIncMode+0x44>)
 800658a:	68bb      	ldr	r3, [r7, #8]
 800658c:	4413      	add	r3, r2
 800658e:	781b      	ldrb	r3, [r3, #0]
 8006590:	461a      	mov	r2, r3
 8006592:	68fb      	ldr	r3, [r7, #12]
 8006594:	4413      	add	r3, r2
 8006596:	681b      	ldr	r3, [r3, #0]
 8006598:	f423 6280 	bic.w	r2, r3, #1024	; 0x400
 800659c:	4908      	ldr	r1, [pc, #32]	; (80065c0 <LL_DMA_SetMemoryIncMode+0x44>)
 800659e:	68bb      	ldr	r3, [r7, #8]
 80065a0:	440b      	add	r3, r1
 80065a2:	781b      	ldrb	r3, [r3, #0]
 80065a4:	4619      	mov	r1, r3
 80065a6:	68fb      	ldr	r3, [r7, #12]
 80065a8:	440b      	add	r3, r1
 80065aa:	4619      	mov	r1, r3
 80065ac:	687b      	ldr	r3, [r7, #4]
 80065ae:	4313      	orrs	r3, r2
 80065b0:	600b      	str	r3, [r1, #0]
 80065b2:	bf00      	nop
 80065b4:	3714      	adds	r7, #20
 80065b6:	46bd      	mov	sp, r7
 80065b8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80065bc:	4770      	bx	lr
 80065be:	bf00      	nop
 80065c0:	0800d160 	.word	0x0800d160

080065c4 <LL_DMA_SetDataLength>:
 80065c4:	b480      	push	{r7}
 80065c6:	b085      	sub	sp, #20
 80065c8:	af00      	add	r7, sp, #0
 80065ca:	60f8      	str	r0, [r7, #12]
 80065cc:	60b9      	str	r1, [r7, #8]
 80065ce:	607a      	str	r2, [r7, #4]
 80065d0:	4a0d      	ldr	r2, [pc, #52]	; (8006608 <LL_DMA_SetDataLength+0x44>)
 80065d2:	68bb      	ldr	r3, [r7, #8]
 80065d4:	4413      	add	r3, r2
 80065d6:	781b      	ldrb	r3, [r3, #0]
 80065d8:	461a      	mov	r2, r3
 80065da:	68fb      	ldr	r3, [r7, #12]
 80065dc:	4413      	add	r3, r2
 80065de:	685b      	ldr	r3, [r3, #4]
 80065e0:	0c1b      	lsrs	r3, r3, #16
 80065e2:	041b      	lsls	r3, r3, #16
 80065e4:	4908      	ldr	r1, [pc, #32]	; (8006608 <LL_DMA_SetDataLength+0x44>)
 80065e6:	68ba      	ldr	r2, [r7, #8]
 80065e8:	440a      	add	r2, r1
 80065ea:	7812      	ldrb	r2, [r2, #0]
 80065ec:	4611      	mov	r1, r2
 80065ee:	68fa      	ldr	r2, [r7, #12]
 80065f0:	440a      	add	r2, r1
 80065f2:	4611      	mov	r1, r2
 80065f4:	687a      	ldr	r2, [r7, #4]
 80065f6:	4313      	orrs	r3, r2
 80065f8:	604b      	str	r3, [r1, #4]
 80065fa:	bf00      	nop
 80065fc:	3714      	adds	r7, #20
 80065fe:	46bd      	mov	sp, r7
 8006600:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006604:	4770      	bx	lr
 8006606:	bf00      	nop
 8006608:	0800d160 	.word	0x0800d160

0800660c <LL_DMA_SetChannelSelection>:
 800660c:	b480      	push	{r7}
 800660e:	b085      	sub	sp, #20
 8006610:	af00      	add	r7, sp, #0
 8006612:	60f8      	str	r0, [r7, #12]
 8006614:	60b9      	str	r1, [r7, #8]
 8006616:	607a      	str	r2, [r7, #4]
 8006618:	4a0d      	ldr	r2, [pc, #52]	; (8006650 <LL_DMA_SetChannelSelection+0x44>)
 800661a:	68bb      	ldr	r3, [r7, #8]
 800661c:	4413      	add	r3, r2
 800661e:	781b      	ldrb	r3, [r3, #0]
 8006620:	461a      	mov	r2, r3
 8006622:	68fb      	ldr	r3, [r7, #12]
 8006624:	4413      	add	r3, r2
 8006626:	681b      	ldr	r3, [r3, #0]
 8006628:	f023 6260 	bic.w	r2, r3, #234881024	; 0xe000000
 800662c:	4908      	ldr	r1, [pc, #32]	; (8006650 <LL_DMA_SetChannelSelection+0x44>)
 800662e:	68bb      	ldr	r3, [r7, #8]
 8006630:	440b      	add	r3, r1
 8006632:	781b      	ldrb	r3, [r3, #0]
 8006634:	4619      	mov	r1, r3
 8006636:	68fb      	ldr	r3, [r7, #12]
 8006638:	440b      	add	r3, r1
 800663a:	4619      	mov	r1, r3
 800663c:	687b      	ldr	r3, [r7, #4]
 800663e:	4313      	orrs	r3, r2
 8006640:	600b      	str	r3, [r1, #0]
 8006642:	bf00      	nop
 8006644:	3714      	adds	r7, #20
 8006646:	46bd      	mov	sp, r7
 8006648:	f85d 7b04 	ldr.w	r7, [sp], #4
 800664c:	4770      	bx	lr
 800664e:	bf00      	nop
 8006650:	0800d160 	.word	0x0800d160

08006654 <LL_DMA_ConfigAddresses>:
 8006654:	b480      	push	{r7}
 8006656:	b085      	sub	sp, #20
 8006658:	af00      	add	r7, sp, #0
 800665a:	60f8      	str	r0, [r7, #12]
 800665c:	60b9      	str	r1, [r7, #8]
 800665e:	607a      	str	r2, [r7, #4]
 8006660:	603b      	str	r3, [r7, #0]
 8006662:	69bb      	ldr	r3, [r7, #24]
 8006664:	2b40      	cmp	r3, #64	; 0x40
 8006666:	d114      	bne.n	8006692 <LL_DMA_ConfigAddresses+0x3e>
 8006668:	4a17      	ldr	r2, [pc, #92]	; (80066c8 <LL_DMA_ConfigAddresses+0x74>)
 800666a:	68bb      	ldr	r3, [r7, #8]
 800666c:	4413      	add	r3, r2
 800666e:	781b      	ldrb	r3, [r3, #0]
 8006670:	461a      	mov	r2, r3
 8006672:	68fb      	ldr	r3, [r7, #12]
 8006674:	4413      	add	r3, r2
 8006676:	461a      	mov	r2, r3
 8006678:	687b      	ldr	r3, [r7, #4]
 800667a:	60d3      	str	r3, [r2, #12]
 800667c:	4a12      	ldr	r2, [pc, #72]	; (80066c8 <LL_DMA_ConfigAddresses+0x74>)
 800667e:	68bb      	ldr	r3, [r7, #8]
 8006680:	4413      	add	r3, r2
 8006682:	781b      	ldrb	r3, [r3, #0]
 8006684:	461a      	mov	r2, r3
 8006686:	68fb      	ldr	r3, [r7, #12]
 8006688:	4413      	add	r3, r2
 800668a:	461a      	mov	r2, r3
 800668c:	683b      	ldr	r3, [r7, #0]
 800668e:	6093      	str	r3, [r2, #8]
 8006690:	e013      	b.n	80066ba <LL_DMA_ConfigAddresses+0x66>
 8006692:	4a0d      	ldr	r2, [pc, #52]	; (80066c8 <LL_DMA_ConfigAddresses+0x74>)
 8006694:	68bb      	ldr	r3, [r7, #8]
 8006696:	4413      	add	r3, r2
 8006698:	781b      	ldrb	r3, [r3, #0]
 800669a:	461a      	mov	r2, r3
 800669c:	68fb      	ldr	r3, [r7, #12]
 800669e:	4413      	add	r3, r2
 80066a0:	461a      	mov	r2, r3
 80066a2:	687b      	ldr	r3, [r7, #4]
 80066a4:	6093      	str	r3, [r2, #8]
 80066a6:	4a08      	ldr	r2, [pc, #32]	; (80066c8 <LL_DMA_ConfigAddresses+0x74>)
 80066a8:	68bb      	ldr	r3, [r7, #8]
 80066aa:	4413      	add	r3, r2
 80066ac:	781b      	ldrb	r3, [r3, #0]
 80066ae:	461a      	mov	r2, r3
 80066b0:	68fb      	ldr	r3, [r7, #12]
 80066b2:	4413      	add	r3, r2
 80066b4:	461a      	mov	r2, r3
 80066b6:	683b      	ldr	r3, [r7, #0]
 80066b8:	60d3      	str	r3, [r2, #12]
 80066ba:	bf00      	nop
 80066bc:	3714      	adds	r7, #20
 80066be:	46bd      	mov	sp, r7
 80066c0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80066c4:	4770      	bx	lr
 80066c6:	bf00      	nop
 80066c8:	0800d160 	.word	0x0800d160

080066cc <LL_DMA_IsActiveFlag_TC1>:
 80066cc:	b480      	push	{r7}
 80066ce:	b083      	sub	sp, #12
 80066d0:	af00      	add	r7, sp, #0
 80066d2:	6078      	str	r0, [r7, #4]
 80066d4:	687b      	ldr	r3, [r7, #4]
 80066d6:	681b      	ldr	r3, [r3, #0]
 80066d8:	f403 6300 	and.w	r3, r3, #2048	; 0x800
 80066dc:	f5b3 6f00 	cmp.w	r3, #2048	; 0x800
 80066e0:	bf0c      	ite	eq
 80066e2:	2301      	moveq	r3, #1
 80066e4:	2300      	movne	r3, #0
 80066e6:	b2db      	uxtb	r3, r3
 80066e8:	4618      	mov	r0, r3
 80066ea:	370c      	adds	r7, #12
 80066ec:	46bd      	mov	sp, r7
 80066ee:	f85d 7b04 	ldr.w	r7, [sp], #4
 80066f2:	4770      	bx	lr

080066f4 <LL_DMA_IsActiveFlag_TC2>:
 80066f4:	b480      	push	{r7}
 80066f6:	b083      	sub	sp, #12
 80066f8:	af00      	add	r7, sp, #0
 80066fa:	6078      	str	r0, [r7, #4]
 80066fc:	687b      	ldr	r3, [r7, #4]
 80066fe:	681b      	ldr	r3, [r3, #0]
 8006700:	f403 1300 	and.w	r3, r3, #2097152	; 0x200000
 8006704:	f5b3 1f00 	cmp.w	r3, #2097152	; 0x200000
 8006708:	bf0c      	ite	eq
 800670a:	2301      	moveq	r3, #1
 800670c:	2300      	movne	r3, #0
 800670e:	b2db      	uxtb	r3, r3
 8006710:	4618      	mov	r0, r3
 8006712:	370c      	adds	r7, #12
 8006714:	46bd      	mov	sp, r7
 8006716:	f85d 7b04 	ldr.w	r7, [sp], #4
 800671a:	4770      	bx	lr

0800671c <LL_DMA_ClearFlag_HT1>:
 800671c:	b480      	push	{r7}
 800671e:	b083      	sub	sp, #12
 8006720:	af00      	add	r7, sp, #0
 8006722:	6078      	str	r0, [r7, #4]
 8006724:	687b      	ldr	r3, [r7, #4]
 8006726:	f44f 6280 	mov.w	r2, #1024	; 0x400
 800672a:	609a      	str	r2, [r3, #8]
 800672c:	bf00      	nop
 800672e:	370c      	adds	r7, #12
 8006730:	46bd      	mov	sp, r7
 8006732:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006736:	4770      	bx	lr

08006738 <LL_DMA_ClearFlag_HT2>:
 8006738:	b480      	push	{r7}
 800673a:	b083      	sub	sp, #12
 800673c:	af00      	add	r7, sp, #0
 800673e:	6078      	str	r0, [r7, #4]
 8006740:	687b      	ldr	r3, [r7, #4]
 8006742:	f44f 1280 	mov.w	r2, #1048576	; 0x100000
 8006746:	609a      	str	r2, [r3, #8]
 8006748:	bf00      	nop
 800674a:	370c      	adds	r7, #12
 800674c:	46bd      	mov	sp, r7
 800674e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006752:	4770      	bx	lr

08006754 <LL_DMA_ClearFlag_TC1>:
 8006754:	b480      	push	{r7}
 8006756:	b083      	sub	sp, #12
 8006758:	af00      	add	r7, sp, #0
 800675a:	6078      	str	r0, [r7, #4]
 800675c:	687b      	ldr	r3, [r7, #4]
 800675e:	f44f 6200 	mov.w	r2, #2048	; 0x800
 8006762:	609a      	str	r2, [r3, #8]
 8006764:	bf00      	nop
 8006766:	370c      	adds	r7, #12
 8006768:	46bd      	mov	sp, r7
 800676a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800676e:	4770      	bx	lr

08006770 <LL_DMA_ClearFlag_TC2>:
 8006770:	b480      	push	{r7}
 8006772:	b083      	sub	sp, #12
 8006774:	af00      	add	r7, sp, #0
 8006776:	6078      	str	r0, [r7, #4]
 8006778:	687b      	ldr	r3, [r7, #4]
 800677a:	f44f 1200 	mov.w	r2, #2097152	; 0x200000
 800677e:	609a      	str	r2, [r3, #8]
 8006780:	bf00      	nop
 8006782:	370c      	adds	r7, #12
 8006784:	46bd      	mov	sp, r7
 8006786:	f85d 7b04 	ldr.w	r7, [sp], #4
 800678a:	4770      	bx	lr

0800678c <LL_DMA_EnableIT_TC>:
 800678c:	b480      	push	{r7}
 800678e:	b083      	sub	sp, #12
 8006790:	af00      	add	r7, sp, #0
 8006792:	6078      	str	r0, [r7, #4]
 8006794:	6039      	str	r1, [r7, #0]
 8006796:	4a0c      	ldr	r2, [pc, #48]	; (80067c8 <LL_DMA_EnableIT_TC+0x3c>)
 8006798:	683b      	ldr	r3, [r7, #0]
 800679a:	4413      	add	r3, r2
 800679c:	781b      	ldrb	r3, [r3, #0]
 800679e:	461a      	mov	r2, r3
 80067a0:	687b      	ldr	r3, [r7, #4]
 80067a2:	4413      	add	r3, r2
 80067a4:	681b      	ldr	r3, [r3, #0]
 80067a6:	4908      	ldr	r1, [pc, #32]	; (80067c8 <LL_DMA_EnableIT_TC+0x3c>)
 80067a8:	683a      	ldr	r2, [r7, #0]
 80067aa:	440a      	add	r2, r1
 80067ac:	7812      	ldrb	r2, [r2, #0]
 80067ae:	4611      	mov	r1, r2
 80067b0:	687a      	ldr	r2, [r7, #4]
 80067b2:	440a      	add	r2, r1
 80067b4:	f043 0310 	orr.w	r3, r3, #16
 80067b8:	6013      	str	r3, [r2, #0]
 80067ba:	bf00      	nop
 80067bc:	370c      	adds	r7, #12
 80067be:	46bd      	mov	sp, r7
 80067c0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80067c4:	4770      	bx	lr
 80067c6:	bf00      	nop
 80067c8:	0800d160 	.word	0x0800d160

080067cc <LL_GPIO_SetPinMode>:
 80067cc:	b480      	push	{r7}
 80067ce:	b089      	sub	sp, #36	; 0x24
 80067d0:	af00      	add	r7, sp, #0
 80067d2:	60f8      	str	r0, [r7, #12]
 80067d4:	60b9      	str	r1, [r7, #8]
 80067d6:	607a      	str	r2, [r7, #4]
 80067d8:	68fb      	ldr	r3, [r7, #12]
 80067da:	681a      	ldr	r2, [r3, #0]
 80067dc:	68bb      	ldr	r3, [r7, #8]
 80067de:	617b      	str	r3, [r7, #20]
 80067e0:	697b      	ldr	r3, [r7, #20]
 80067e2:	fa93 f3a3 	rbit	r3, r3
 80067e6:	613b      	str	r3, [r7, #16]
 80067e8:	693b      	ldr	r3, [r7, #16]
 80067ea:	fab3 f383 	clz	r3, r3
 80067ee:	005b      	lsls	r3, r3, #1
 80067f0:	2103      	movs	r1, #3
 80067f2:	fa01 f303 	lsl.w	r3, r1, r3
 80067f6:	43db      	mvns	r3, r3
 80067f8:	401a      	ands	r2, r3
 80067fa:	68bb      	ldr	r3, [r7, #8]
 80067fc:	61fb      	str	r3, [r7, #28]
 80067fe:	69fb      	ldr	r3, [r7, #28]
 8006800:	fa93 f3a3 	rbit	r3, r3
 8006804:	61bb      	str	r3, [r7, #24]
 8006806:	69bb      	ldr	r3, [r7, #24]
 8006808:	fab3 f383 	clz	r3, r3
 800680c:	005b      	lsls	r3, r3, #1
 800680e:	6879      	ldr	r1, [r7, #4]
 8006810:	fa01 f303 	lsl.w	r3, r1, r3
 8006814:	431a      	orrs	r2, r3
 8006816:	68fb      	ldr	r3, [r7, #12]
 8006818:	601a      	str	r2, [r3, #0]
 800681a:	bf00      	nop
 800681c:	3724      	adds	r7, #36	; 0x24
 800681e:	46bd      	mov	sp, r7
 8006820:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006824:	4770      	bx	lr

08006826 <LL_GPIO_SetPinOutputType>:
 8006826:	b480      	push	{r7}
 8006828:	b085      	sub	sp, #20
 800682a:	af00      	add	r7, sp, #0
 800682c:	60f8      	str	r0, [r7, #12]
 800682e:	60b9      	str	r1, [r7, #8]
 8006830:	607a      	str	r2, [r7, #4]
 8006832:	68fb      	ldr	r3, [r7, #12]
 8006834:	685a      	ldr	r2, [r3, #4]
 8006836:	68bb      	ldr	r3, [r7, #8]
 8006838:	43db      	mvns	r3, r3
 800683a:	401a      	ands	r2, r3
 800683c:	68bb      	ldr	r3, [r7, #8]
 800683e:	6879      	ldr	r1, [r7, #4]
 8006840:	fb01 f303 	mul.w	r3, r1, r3
 8006844:	431a      	orrs	r2, r3
 8006846:	68fb      	ldr	r3, [r7, #12]
 8006848:	605a      	str	r2, [r3, #4]
 800684a:	bf00      	nop
 800684c:	3714      	adds	r7, #20
 800684e:	46bd      	mov	sp, r7
 8006850:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006854:	4770      	bx	lr

08006856 <LL_GPIO_SetPinSpeed>:
 8006856:	b480      	push	{r7}
 8006858:	b089      	sub	sp, #36	; 0x24
 800685a:	af00      	add	r7, sp, #0
 800685c:	60f8      	str	r0, [r7, #12]
 800685e:	60b9      	str	r1, [r7, #8]
 8006860:	607a      	str	r2, [r7, #4]
 8006862:	68fb      	ldr	r3, [r7, #12]
 8006864:	689a      	ldr	r2, [r3, #8]
 8006866:	68bb      	ldr	r3, [r7, #8]
 8006868:	617b      	str	r3, [r7, #20]
 800686a:	697b      	ldr	r3, [r7, #20]
 800686c:	fa93 f3a3 	rbit	r3, r3
 8006870:	613b      	str	r3, [r7, #16]
 8006872:	693b      	ldr	r3, [r7, #16]
 8006874:	fab3 f383 	clz	r3, r3
 8006878:	005b      	lsls	r3, r3, #1
 800687a:	2103      	movs	r1, #3
 800687c:	fa01 f303 	lsl.w	r3, r1, r3
 8006880:	43db      	mvns	r3, r3
 8006882:	401a      	ands	r2, r3
 8006884:	68bb      	ldr	r3, [r7, #8]
 8006886:	61fb      	str	r3, [r7, #28]
 8006888:	69fb      	ldr	r3, [r7, #28]
 800688a:	fa93 f3a3 	rbit	r3, r3
 800688e:	61bb      	str	r3, [r7, #24]
 8006890:	69bb      	ldr	r3, [r7, #24]
 8006892:	fab3 f383 	clz	r3, r3
 8006896:	005b      	lsls	r3, r3, #1
 8006898:	6879      	ldr	r1, [r7, #4]
 800689a:	fa01 f303 	lsl.w	r3, r1, r3
 800689e:	431a      	orrs	r2, r3
 80068a0:	68fb      	ldr	r3, [r7, #12]
 80068a2:	609a      	str	r2, [r3, #8]
 80068a4:	bf00      	nop
 80068a6:	3724      	adds	r7, #36	; 0x24
 80068a8:	46bd      	mov	sp, r7
 80068aa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80068ae:	4770      	bx	lr

080068b0 <LL_GPIO_SetPinPull>:
 80068b0:	b480      	push	{r7}
 80068b2:	b089      	sub	sp, #36	; 0x24
 80068b4:	af00      	add	r7, sp, #0
 80068b6:	60f8      	str	r0, [r7, #12]
 80068b8:	60b9      	str	r1, [r7, #8]
 80068ba:	607a      	str	r2, [r7, #4]
 80068bc:	68fb      	ldr	r3, [r7, #12]
 80068be:	68da      	ldr	r2, [r3, #12]
 80068c0:	68bb      	ldr	r3, [r7, #8]
 80068c2:	617b      	str	r3, [r7, #20]
 80068c4:	697b      	ldr	r3, [r7, #20]
 80068c6:	fa93 f3a3 	rbit	r3, r3
 80068ca:	613b      	str	r3, [r7, #16]
 80068cc:	693b      	ldr	r3, [r7, #16]
 80068ce:	fab3 f383 	clz	r3, r3
 80068d2:	005b      	lsls	r3, r3, #1
 80068d4:	2103      	movs	r1, #3
 80068d6:	fa01 f303 	lsl.w	r3, r1, r3
 80068da:	43db      	mvns	r3, r3
 80068dc:	401a      	ands	r2, r3
 80068de:	68bb      	ldr	r3, [r7, #8]
 80068e0:	61fb      	str	r3, [r7, #28]
 80068e2:	69fb      	ldr	r3, [r7, #28]
 80068e4:	fa93 f3a3 	rbit	r3, r3
 80068e8:	61bb      	str	r3, [r7, #24]
 80068ea:	69bb      	ldr	r3, [r7, #24]
 80068ec:	fab3 f383 	clz	r3, r3
 80068f0:	005b      	lsls	r3, r3, #1
 80068f2:	6879      	ldr	r1, [r7, #4]
 80068f4:	fa01 f303 	lsl.w	r3, r1, r3
 80068f8:	431a      	orrs	r2, r3
 80068fa:	68fb      	ldr	r3, [r7, #12]
 80068fc:	60da      	str	r2, [r3, #12]
 80068fe:	bf00      	nop
 8006900:	3724      	adds	r7, #36	; 0x24
 8006902:	46bd      	mov	sp, r7
 8006904:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006908:	4770      	bx	lr

0800690a <LL_GPIO_SetAFPin_0_7>:
 800690a:	b480      	push	{r7}
 800690c:	b089      	sub	sp, #36	; 0x24
 800690e:	af00      	add	r7, sp, #0
 8006910:	60f8      	str	r0, [r7, #12]
 8006912:	60b9      	str	r1, [r7, #8]
 8006914:	607a      	str	r2, [r7, #4]
 8006916:	68fb      	ldr	r3, [r7, #12]
 8006918:	6a1a      	ldr	r2, [r3, #32]
 800691a:	68bb      	ldr	r3, [r7, #8]
 800691c:	617b      	str	r3, [r7, #20]
 800691e:	697b      	ldr	r3, [r7, #20]
 8006920:	fa93 f3a3 	rbit	r3, r3
 8006924:	613b      	str	r3, [r7, #16]
 8006926:	693b      	ldr	r3, [r7, #16]
 8006928:	fab3 f383 	clz	r3, r3
 800692c:	009b      	lsls	r3, r3, #2
 800692e:	210f      	movs	r1, #15
 8006930:	fa01 f303 	lsl.w	r3, r1, r3
 8006934:	43db      	mvns	r3, r3
 8006936:	401a      	ands	r2, r3
 8006938:	68bb      	ldr	r3, [r7, #8]
 800693a:	61fb      	str	r3, [r7, #28]
 800693c:	69fb      	ldr	r3, [r7, #28]
 800693e:	fa93 f3a3 	rbit	r3, r3
 8006942:	61bb      	str	r3, [r7, #24]
 8006944:	69bb      	ldr	r3, [r7, #24]
 8006946:	fab3 f383 	clz	r3, r3
 800694a:	009b      	lsls	r3, r3, #2
 800694c:	6879      	ldr	r1, [r7, #4]
 800694e:	fa01 f303 	lsl.w	r3, r1, r3
 8006952:	431a      	orrs	r2, r3
 8006954:	68fb      	ldr	r3, [r7, #12]
 8006956:	621a      	str	r2, [r3, #32]
 8006958:	bf00      	nop
 800695a:	3724      	adds	r7, #36	; 0x24
 800695c:	46bd      	mov	sp, r7
 800695e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006962:	4770      	bx	lr

08006964 <LL_GPIO_SetAFPin_8_15>:
 8006964:	b480      	push	{r7}
 8006966:	b089      	sub	sp, #36	; 0x24
 8006968:	af00      	add	r7, sp, #0
 800696a:	60f8      	str	r0, [r7, #12]
 800696c:	60b9      	str	r1, [r7, #8]
 800696e:	607a      	str	r2, [r7, #4]
 8006970:	68fb      	ldr	r3, [r7, #12]
 8006972:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 8006974:	68bb      	ldr	r3, [r7, #8]
 8006976:	0a1b      	lsrs	r3, r3, #8
 8006978:	617b      	str	r3, [r7, #20]
 800697a:	697b      	ldr	r3, [r7, #20]
 800697c:	fa93 f3a3 	rbit	r3, r3
 8006980:	613b      	str	r3, [r7, #16]
 8006982:	693b      	ldr	r3, [r7, #16]
 8006984:	fab3 f383 	clz	r3, r3
 8006988:	009b      	lsls	r3, r3, #2
 800698a:	210f      	movs	r1, #15
 800698c:	fa01 f303 	lsl.w	r3, r1, r3
 8006990:	43db      	mvns	r3, r3
 8006992:	401a      	ands	r2, r3
 8006994:	68bb      	ldr	r3, [r7, #8]
 8006996:	0a1b      	lsrs	r3, r3, #8
 8006998:	61fb      	str	r3, [r7, #28]
 800699a:	69fb      	ldr	r3, [r7, #28]
 800699c:	fa93 f3a3 	rbit	r3, r3
 80069a0:	61bb      	str	r3, [r7, #24]
 80069a2:	69bb      	ldr	r3, [r7, #24]
 80069a4:	fab3 f383 	clz	r3, r3
 80069a8:	009b      	lsls	r3, r3, #2
 80069aa:	6879      	ldr	r1, [r7, #4]
 80069ac:	fa01 f303 	lsl.w	r3, r1, r3
 80069b0:	431a      	orrs	r2, r3
 80069b2:	68fb      	ldr	r3, [r7, #12]
 80069b4:	625a      	str	r2, [r3, #36]	; 0x24
 80069b6:	bf00      	nop
 80069b8:	3724      	adds	r7, #36	; 0x24
 80069ba:	46bd      	mov	sp, r7
 80069bc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80069c0:	4770      	bx	lr
	...

080069c4 <terminal_hw_config>:
 80069c4:	b580      	push	{r7, lr}
 80069c6:	b082      	sub	sp, #8
 80069c8:	af02      	add	r7, sp, #8
 80069ca:	2004      	movs	r0, #4
 80069cc:	f7ff fd4e 	bl	800646c <LL_AHB1_GRP1_EnableClock>
 80069d0:	2207      	movs	r2, #7
 80069d2:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80069d6:	489f      	ldr	r0, [pc, #636]	; (8006c54 <terminal_hw_config+0x290>)
 80069d8:	f7ff ffc4 	bl	8006964 <LL_GPIO_SetAFPin_8_15>
 80069dc:	2202      	movs	r2, #2
 80069de:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80069e2:	489c      	ldr	r0, [pc, #624]	; (8006c54 <terminal_hw_config+0x290>)
 80069e4:	f7ff fef2 	bl	80067cc <LL_GPIO_SetPinMode>
 80069e8:	2200      	movs	r2, #0
 80069ea:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80069ee:	4899      	ldr	r0, [pc, #612]	; (8006c54 <terminal_hw_config+0x290>)
 80069f0:	f7ff ff19 	bl	8006826 <LL_GPIO_SetPinOutputType>
 80069f4:	2200      	movs	r2, #0
 80069f6:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80069fa:	4896      	ldr	r0, [pc, #600]	; (8006c54 <terminal_hw_config+0x290>)
 80069fc:	f7ff ff58 	bl	80068b0 <LL_GPIO_SetPinPull>
 8006a00:	2202      	movs	r2, #2
 8006a02:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8006a06:	4893      	ldr	r0, [pc, #588]	; (8006c54 <terminal_hw_config+0x290>)
 8006a08:	f7ff ff25 	bl	8006856 <LL_GPIO_SetPinSpeed>
 8006a0c:	2207      	movs	r2, #7
 8006a0e:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8006a12:	4890      	ldr	r0, [pc, #576]	; (8006c54 <terminal_hw_config+0x290>)
 8006a14:	f7ff ffa6 	bl	8006964 <LL_GPIO_SetAFPin_8_15>
 8006a18:	2202      	movs	r2, #2
 8006a1a:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8006a1e:	488d      	ldr	r0, [pc, #564]	; (8006c54 <terminal_hw_config+0x290>)
 8006a20:	f7ff fed4 	bl	80067cc <LL_GPIO_SetPinMode>
 8006a24:	2200      	movs	r2, #0
 8006a26:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8006a2a:	488a      	ldr	r0, [pc, #552]	; (8006c54 <terminal_hw_config+0x290>)
 8006a2c:	f7ff fefb 	bl	8006826 <LL_GPIO_SetPinOutputType>
 8006a30:	2200      	movs	r2, #0
 8006a32:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8006a36:	4887      	ldr	r0, [pc, #540]	; (8006c54 <terminal_hw_config+0x290>)
 8006a38:	f7ff ff3a 	bl	80068b0 <LL_GPIO_SetPinPull>
 8006a3c:	2202      	movs	r2, #2
 8006a3e:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8006a42:	4884      	ldr	r0, [pc, #528]	; (8006c54 <terminal_hw_config+0x290>)
 8006a44:	f7ff ff07 	bl	8006856 <LL_GPIO_SetPinSpeed>
 8006a48:	f44f 1000 	mov.w	r0, #2097152	; 0x200000
 8006a4c:	f7ff fd0e 	bl	800646c <LL_AHB1_GRP1_EnableClock>
 8006a50:	f44f 2080 	mov.w	r0, #262144	; 0x40000
 8006a54:	f7ff fd22 	bl	800649c <LL_APB1_GRP1_EnableClock>
 8006a58:	210c      	movs	r1, #12
 8006a5a:	487f      	ldr	r0, [pc, #508]	; (8006c58 <terminal_hw_config+0x294>)
 8006a5c:	f7ff fb7a 	bl	8006154 <LL_USART_SetTransferDirection>
 8006a60:	2100      	movs	r1, #0
 8006a62:	487d      	ldr	r0, [pc, #500]	; (8006c58 <terminal_hw_config+0x294>)
 8006a64:	f7ff fb89 	bl	800617a <LL_USART_SetParity>
 8006a68:	2100      	movs	r1, #0
 8006a6a:	487b      	ldr	r0, [pc, #492]	; (8006c58 <terminal_hw_config+0x294>)
 8006a6c:	f7ff fb98 	bl	80061a0 <LL_USART_SetDataWidth>
 8006a70:	2100      	movs	r1, #0
 8006a72:	4879      	ldr	r0, [pc, #484]	; (8006c58 <terminal_hw_config+0x294>)
 8006a74:	f7ff fba7 	bl	80061c6 <LL_USART_SetStopBitsLength>
 8006a78:	2100      	movs	r1, #0
 8006a7a:	4877      	ldr	r0, [pc, #476]	; (8006c58 <terminal_hw_config+0x294>)
 8006a7c:	f7ff fbb6 	bl	80061ec <LL_USART_SetHWFlowCtrl>
 8006a80:	4b76      	ldr	r3, [pc, #472]	; (8006c5c <terminal_hw_config+0x298>)
 8006a82:	681b      	ldr	r3, [r3, #0]
 8006a84:	0899      	lsrs	r1, r3, #2
 8006a86:	f44f 33e1 	mov.w	r3, #115200	; 0x1c200
 8006a8a:	2200      	movs	r2, #0
 8006a8c:	4872      	ldr	r0, [pc, #456]	; (8006c58 <terminal_hw_config+0x294>)
 8006a8e:	f7ff fbc1 	bl	8006214 <LL_USART_SetBaudRate>
 8006a92:	4871      	ldr	r0, [pc, #452]	; (8006c58 <terminal_hw_config+0x294>)
 8006a94:	f7ff fb3e 	bl	8006114 <LL_USART_EnableDirectionRx>
 8006a98:	486f      	ldr	r0, [pc, #444]	; (8006c58 <terminal_hw_config+0x294>)
 8006a9a:	f7ff fb4b 	bl	8006134 <LL_USART_EnableDirectionTx>
 8006a9e:	486e      	ldr	r0, [pc, #440]	; (8006c58 <terminal_hw_config+0x294>)
 8006aa0:	f7ff fcc4 	bl	800642c <LL_USART_EnableDMAReq_RX>
 8006aa4:	486c      	ldr	r0, [pc, #432]	; (8006c58 <terminal_hw_config+0x294>)
 8006aa6:	f7ff fcb1 	bl	800640c <LL_USART_EnableIT_IDLE>
 8006aaa:	486b      	ldr	r0, [pc, #428]	; (8006c58 <terminal_hw_config+0x294>)
 8006aac:	f7ff fb22 	bl	80060f4 <LL_USART_Enable>
 8006ab0:	2101      	movs	r1, #1
 8006ab2:	2027      	movs	r0, #39	; 0x27
 8006ab4:	f7ff faf4 	bl	80060a0 <NVIC_SetPriority>
 8006ab8:	2027      	movs	r0, #39	; 0x27
 8006aba:	f7ff fad7 	bl	800606c <NVIC_EnableIRQ>
 8006abe:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 8006ac2:	2101      	movs	r1, #1
 8006ac4:	4866      	ldr	r0, [pc, #408]	; (8006c60 <terminal_hw_config+0x29c>)
 8006ac6:	f7ff fda1 	bl	800660c <LL_DMA_SetChannelSelection>
 8006aca:	4b66      	ldr	r3, [pc, #408]	; (8006c64 <terminal_hw_config+0x2a0>)
 8006acc:	681b      	ldr	r3, [r3, #0]
 8006ace:	685b      	ldr	r3, [r3, #4]
 8006ad0:	461a      	mov	r2, r3
 8006ad2:	2300      	movs	r3, #0
 8006ad4:	9300      	str	r3, [sp, #0]
 8006ad6:	4613      	mov	r3, r2
 8006ad8:	4a63      	ldr	r2, [pc, #396]	; (8006c68 <terminal_hw_config+0x2a4>)
 8006ada:	2101      	movs	r1, #1
 8006adc:	4860      	ldr	r0, [pc, #384]	; (8006c60 <terminal_hw_config+0x29c>)
 8006ade:	f7ff fdb9 	bl	8006654 <LL_DMA_ConfigAddresses>
 8006ae2:	f44f 7280 	mov.w	r2, #256	; 0x100
 8006ae6:	2101      	movs	r1, #1
 8006ae8:	485d      	ldr	r0, [pc, #372]	; (8006c60 <terminal_hw_config+0x29c>)
 8006aea:	f7ff fd6b 	bl	80065c4 <LL_DMA_SetDataLength>
 8006aee:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8006af2:	2101      	movs	r1, #1
 8006af4:	485a      	ldr	r0, [pc, #360]	; (8006c60 <terminal_hw_config+0x29c>)
 8006af6:	f7ff fd41 	bl	800657c <LL_DMA_SetMemoryIncMode>
 8006afa:	2101      	movs	r1, #1
 8006afc:	4858      	ldr	r0, [pc, #352]	; (8006c60 <terminal_hw_config+0x29c>)
 8006afe:	f7ff fcfd 	bl	80064fc <LL_DMA_EnableStream>
 8006b02:	2101      	movs	r1, #1
 8006b04:	4856      	ldr	r0, [pc, #344]	; (8006c60 <terminal_hw_config+0x29c>)
 8006b06:	f7ff fe41 	bl	800678c <LL_DMA_EnableIT_TC>
 8006b0a:	2106      	movs	r1, #6
 8006b0c:	200c      	movs	r0, #12
 8006b0e:	f7ff fac7 	bl	80060a0 <NVIC_SetPriority>
 8006b12:	200c      	movs	r0, #12
 8006b14:	f7ff faaa 	bl	800606c <NVIC_EnableIRQ>
 8006b18:	f44f 0080 	mov.w	r0, #4194304	; 0x400000
 8006b1c:	f7ff fca6 	bl	800646c <LL_AHB1_GRP1_EnableClock>
 8006b20:	2010      	movs	r0, #16
 8006b22:	f7ff fcd3 	bl	80064cc <LL_APB2_GRP1_EnableClock>
 8006b26:	2002      	movs	r0, #2
 8006b28:	f7ff fca0 	bl	800646c <LL_AHB1_GRP1_EnableClock>
 8006b2c:	2202      	movs	r2, #2
 8006b2e:	2140      	movs	r1, #64	; 0x40
 8006b30:	484e      	ldr	r0, [pc, #312]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b32:	f7ff fe4b 	bl	80067cc <LL_GPIO_SetPinMode>
 8006b36:	2207      	movs	r2, #7
 8006b38:	2140      	movs	r1, #64	; 0x40
 8006b3a:	484c      	ldr	r0, [pc, #304]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b3c:	f7ff fee5 	bl	800690a <LL_GPIO_SetAFPin_0_7>
 8006b40:	2200      	movs	r2, #0
 8006b42:	2140      	movs	r1, #64	; 0x40
 8006b44:	4849      	ldr	r0, [pc, #292]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b46:	f7ff fe6e 	bl	8006826 <LL_GPIO_SetPinOutputType>
 8006b4a:	2201      	movs	r2, #1
 8006b4c:	2140      	movs	r1, #64	; 0x40
 8006b4e:	4847      	ldr	r0, [pc, #284]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b50:	f7ff feae 	bl	80068b0 <LL_GPIO_SetPinPull>
 8006b54:	2202      	movs	r2, #2
 8006b56:	2140      	movs	r1, #64	; 0x40
 8006b58:	4844      	ldr	r0, [pc, #272]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b5a:	f7ff fe7c 	bl	8006856 <LL_GPIO_SetPinSpeed>
 8006b5e:	2202      	movs	r2, #2
 8006b60:	2180      	movs	r1, #128	; 0x80
 8006b62:	4842      	ldr	r0, [pc, #264]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b64:	f7ff fe32 	bl	80067cc <LL_GPIO_SetPinMode>
 8006b68:	2207      	movs	r2, #7
 8006b6a:	2180      	movs	r1, #128	; 0x80
 8006b6c:	483f      	ldr	r0, [pc, #252]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b6e:	f7ff fecc 	bl	800690a <LL_GPIO_SetAFPin_0_7>
 8006b72:	2200      	movs	r2, #0
 8006b74:	2180      	movs	r1, #128	; 0x80
 8006b76:	483d      	ldr	r0, [pc, #244]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b78:	f7ff fe55 	bl	8006826 <LL_GPIO_SetPinOutputType>
 8006b7c:	2201      	movs	r2, #1
 8006b7e:	2180      	movs	r1, #128	; 0x80
 8006b80:	483a      	ldr	r0, [pc, #232]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b82:	f7ff fe95 	bl	80068b0 <LL_GPIO_SetPinPull>
 8006b86:	2202      	movs	r2, #2
 8006b88:	2180      	movs	r1, #128	; 0x80
 8006b8a:	4838      	ldr	r0, [pc, #224]	; (8006c6c <terminal_hw_config+0x2a8>)
 8006b8c:	f7ff fe63 	bl	8006856 <LL_GPIO_SetPinSpeed>
 8006b90:	210c      	movs	r1, #12
 8006b92:	4837      	ldr	r0, [pc, #220]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006b94:	f7ff fade 	bl	8006154 <LL_USART_SetTransferDirection>
 8006b98:	2100      	movs	r1, #0
 8006b9a:	4835      	ldr	r0, [pc, #212]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006b9c:	f7ff faed 	bl	800617a <LL_USART_SetParity>
 8006ba0:	2100      	movs	r1, #0
 8006ba2:	4833      	ldr	r0, [pc, #204]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006ba4:	f7ff fafc 	bl	80061a0 <LL_USART_SetDataWidth>
 8006ba8:	2100      	movs	r1, #0
 8006baa:	4831      	ldr	r0, [pc, #196]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bac:	f7ff fb0b 	bl	80061c6 <LL_USART_SetStopBitsLength>
 8006bb0:	2100      	movs	r1, #0
 8006bb2:	482f      	ldr	r0, [pc, #188]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bb4:	f7ff fb1a 	bl	80061ec <LL_USART_SetHWFlowCtrl>
 8006bb8:	4b28      	ldr	r3, [pc, #160]	; (8006c5c <terminal_hw_config+0x298>)
 8006bba:	681b      	ldr	r3, [r3, #0]
 8006bbc:	0859      	lsrs	r1, r3, #1
 8006bbe:	f44f 33e1 	mov.w	r3, #115200	; 0x1c200
 8006bc2:	2200      	movs	r2, #0
 8006bc4:	482a      	ldr	r0, [pc, #168]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bc6:	f7ff fb25 	bl	8006214 <LL_USART_SetBaudRate>
 8006bca:	4829      	ldr	r0, [pc, #164]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bcc:	f7ff faa2 	bl	8006114 <LL_USART_EnableDirectionRx>
 8006bd0:	4827      	ldr	r0, [pc, #156]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bd2:	f7ff faaf 	bl	8006134 <LL_USART_EnableDirectionTx>
 8006bd6:	4826      	ldr	r0, [pc, #152]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bd8:	f7ff fc28 	bl	800642c <LL_USART_EnableDMAReq_RX>
 8006bdc:	4824      	ldr	r0, [pc, #144]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006bde:	f7ff fc15 	bl	800640c <LL_USART_EnableIT_IDLE>
 8006be2:	4823      	ldr	r0, [pc, #140]	; (8006c70 <terminal_hw_config+0x2ac>)
 8006be4:	f7ff fa86 	bl	80060f4 <LL_USART_Enable>
 8006be8:	2101      	movs	r1, #1
 8006bea:	2025      	movs	r0, #37	; 0x25
 8006bec:	f7ff fa58 	bl	80060a0 <NVIC_SetPriority>
 8006bf0:	2025      	movs	r0, #37	; 0x25
 8006bf2:	f7ff fa3b 	bl	800606c <NVIC_EnableIRQ>
 8006bf6:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 8006bfa:	2102      	movs	r1, #2
 8006bfc:	481d      	ldr	r0, [pc, #116]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006bfe:	f7ff fd05 	bl	800660c <LL_DMA_SetChannelSelection>
 8006c02:	4b18      	ldr	r3, [pc, #96]	; (8006c64 <terminal_hw_config+0x2a0>)
 8006c04:	681b      	ldr	r3, [r3, #0]
 8006c06:	68db      	ldr	r3, [r3, #12]
 8006c08:	461a      	mov	r2, r3
 8006c0a:	2300      	movs	r3, #0
 8006c0c:	9300      	str	r3, [sp, #0]
 8006c0e:	4613      	mov	r3, r2
 8006c10:	4a19      	ldr	r2, [pc, #100]	; (8006c78 <terminal_hw_config+0x2b4>)
 8006c12:	2102      	movs	r1, #2
 8006c14:	4817      	ldr	r0, [pc, #92]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006c16:	f7ff fd1d 	bl	8006654 <LL_DMA_ConfigAddresses>
 8006c1a:	2213      	movs	r2, #19
 8006c1c:	2102      	movs	r1, #2
 8006c1e:	4815      	ldr	r0, [pc, #84]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006c20:	f7ff fcd0 	bl	80065c4 <LL_DMA_SetDataLength>
 8006c24:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8006c28:	2102      	movs	r1, #2
 8006c2a:	4812      	ldr	r0, [pc, #72]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006c2c:	f7ff fca6 	bl	800657c <LL_DMA_SetMemoryIncMode>
 8006c30:	2102      	movs	r1, #2
 8006c32:	4810      	ldr	r0, [pc, #64]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006c34:	f7ff fc62 	bl	80064fc <LL_DMA_EnableStream>
 8006c38:	2102      	movs	r1, #2
 8006c3a:	480e      	ldr	r0, [pc, #56]	; (8006c74 <terminal_hw_config+0x2b0>)
 8006c3c:	f7ff fda6 	bl	800678c <LL_DMA_EnableIT_TC>
 8006c40:	2106      	movs	r1, #6
 8006c42:	203a      	movs	r0, #58	; 0x3a
 8006c44:	f7ff fa2c 	bl	80060a0 <NVIC_SetPriority>
 8006c48:	203a      	movs	r0, #58	; 0x3a
 8006c4a:	f7ff fa0f 	bl	800606c <NVIC_EnableIRQ>
 8006c4e:	bf00      	nop
 8006c50:	46bd      	mov	sp, r7
 8006c52:	bd80      	pop	{r7, pc}
 8006c54:	40020800 	.word	0x40020800
 8006c58:	40004800 	.word	0x40004800
 8006c5c:	2000007c 	.word	0x2000007c
 8006c60:	40026000 	.word	0x40026000
 8006c64:	20000bf4 	.word	0x20000bf4
 8006c68:	40004804 	.word	0x40004804
 8006c6c:	40020400 	.word	0x40020400
 8006c70:	40011000 	.word	0x40011000
 8006c74:	40026400 	.word	0x40026400
 8006c78:	40011004 	.word	0x40011004

08006c7c <term_request>:
 8006c7c:	b580      	push	{r7, lr}
 8006c7e:	b082      	sub	sp, #8
 8006c80:	af00      	add	r7, sp, #0
 8006c82:	6078      	str	r0, [r7, #4]
 8006c84:	f04f 31ff 	mov.w	r1, #4294967295
 8006c88:	2001      	movs	r0, #1
 8006c8a:	f002 f889 	bl	8008da0 <ulTaskNotifyTake>
 8006c8e:	4603      	mov	r3, r0
 8006c90:	2b00      	cmp	r3, #0
 8006c92:	d008      	beq.n	8006ca6 <term_request+0x2a>
 8006c94:	687b      	ldr	r3, [r7, #4]
 8006c96:	685b      	ldr	r3, [r3, #4]
 8006c98:	1c5a      	adds	r2, r3, #1
 8006c9a:	687b      	ldr	r3, [r7, #4]
 8006c9c:	609a      	str	r2, [r3, #8]
 8006c9e:	687b      	ldr	r3, [r7, #4]
 8006ca0:	685b      	ldr	r3, [r3, #4]
 8006ca2:	781b      	ldrb	r3, [r3, #0]
 8006ca4:	e000      	b.n	8006ca8 <term_request+0x2c>
 8006ca6:	2300      	movs	r3, #0
 8006ca8:	4618      	mov	r0, r3
 8006caa:	3708      	adds	r7, #8
 8006cac:	46bd      	mov	sp, r7
 8006cae:	bd80      	pop	{r7, pc}

08006cb0 <term_response>:
 8006cb0:	b580      	push	{r7, lr}
 8006cb2:	b084      	sub	sp, #16
 8006cb4:	af00      	add	r7, sp, #0
 8006cb6:	6078      	str	r0, [r7, #4]
 8006cb8:	6039      	str	r1, [r7, #0]
 8006cba:	2300      	movs	r3, #0
 8006cbc:	73fb      	strb	r3, [r7, #15]
 8006cbe:	687b      	ldr	r3, [r7, #4]
 8006cc0:	681b      	ldr	r3, [r3, #0]
 8006cc2:	4618      	mov	r0, r3
 8006cc4:	f7ff fb94 	bl	80063f0 <LL_USART_ClearFlag_TC>
 8006cc8:	e01b      	b.n	8006d02 <term_response+0x52>
 8006cca:	4b1b      	ldr	r3, [pc, #108]	; (8006d38 <term_response+0x88>)
 8006ccc:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006cd0:	601a      	str	r2, [r3, #0]
 8006cd2:	f3bf 8f4f 	dsb	sy
 8006cd6:	f3bf 8f6f 	isb	sy
 8006cda:	687b      	ldr	r3, [r7, #4]
 8006cdc:	681b      	ldr	r3, [r3, #0]
 8006cde:	4618      	mov	r0, r3
 8006ce0:	f7ff fb61 	bl	80063a6 <LL_USART_IsActiveFlag_TXE>
 8006ce4:	4603      	mov	r3, r0
 8006ce6:	2b00      	cmp	r3, #0
 8006ce8:	d0ef      	beq.n	8006cca <term_response+0x1a>
 8006cea:	687b      	ldr	r3, [r7, #4]
 8006cec:	6818      	ldr	r0, [r3, #0]
 8006cee:	687b      	ldr	r3, [r7, #4]
 8006cf0:	689a      	ldr	r2, [r3, #8]
 8006cf2:	7bfb      	ldrb	r3, [r7, #15]
 8006cf4:	1c59      	adds	r1, r3, #1
 8006cf6:	73f9      	strb	r1, [r7, #15]
 8006cf8:	4413      	add	r3, r2
 8006cfa:	781b      	ldrb	r3, [r3, #0]
 8006cfc:	4619      	mov	r1, r3
 8006cfe:	f7ff fba5 	bl	800644c <LL_USART_TransmitData8>
 8006d02:	683b      	ldr	r3, [r7, #0]
 8006d04:	1e5a      	subs	r2, r3, #1
 8006d06:	603a      	str	r2, [r7, #0]
 8006d08:	2b00      	cmp	r3, #0
 8006d0a:	d1e6      	bne.n	8006cda <term_response+0x2a>
 8006d0c:	e007      	b.n	8006d1e <term_response+0x6e>
 8006d0e:	4b0a      	ldr	r3, [pc, #40]	; (8006d38 <term_response+0x88>)
 8006d10:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006d14:	601a      	str	r2, [r3, #0]
 8006d16:	f3bf 8f4f 	dsb	sy
 8006d1a:	f3bf 8f6f 	isb	sy
 8006d1e:	687b      	ldr	r3, [r7, #4]
 8006d20:	681b      	ldr	r3, [r3, #0]
 8006d22:	4618      	mov	r0, r3
 8006d24:	f7ff fb2c 	bl	8006380 <LL_USART_IsActiveFlag_TC>
 8006d28:	4603      	mov	r3, r0
 8006d2a:	2b00      	cmp	r3, #0
 8006d2c:	d0ef      	beq.n	8006d0e <term_response+0x5e>
 8006d2e:	bf00      	nop
 8006d30:	3710      	adds	r7, #16
 8006d32:	46bd      	mov	sp, r7
 8006d34:	bd80      	pop	{r7, pc}
 8006d36:	bf00      	nop
 8006d38:	e000ed04 	.word	0xe000ed04

08006d3c <terminal_manager>:
 8006d3c:	b580      	push	{r7, lr}
 8006d3e:	b088      	sub	sp, #32
 8006d40:	af00      	add	r7, sp, #0
 8006d42:	6078      	str	r0, [r7, #4]
 8006d44:	2300      	movs	r3, #0
 8006d46:	77fb      	strb	r3, [r7, #31]
 8006d48:	2300      	movs	r3, #0
 8006d4a:	77bb      	strb	r3, [r7, #30]
 8006d4c:	4b20      	ldr	r3, [pc, #128]	; (8006dd0 <terminal_manager+0x94>)
 8006d4e:	60bb      	str	r3, [r7, #8]
 8006d50:	f44f 7080 	mov.w	r0, #256	; 0x100
 8006d54:	f003 fdd2 	bl	800a8fc <malloc>
 8006d58:	4603      	mov	r3, r0
 8006d5a:	60fb      	str	r3, [r7, #12]
 8006d5c:	2080      	movs	r0, #128	; 0x80
 8006d5e:	f003 fdcd 	bl	800a8fc <malloc>
 8006d62:	4603      	mov	r3, r0
 8006d64:	613b      	str	r3, [r7, #16]
 8006d66:	2013      	movs	r0, #19
 8006d68:	f003 fdc8 	bl	800a8fc <malloc>
 8006d6c:	4603      	mov	r3, r0
 8006d6e:	617b      	str	r3, [r7, #20]
 8006d70:	f001 fe38 	bl	80089e4 <xTaskGetCurrentTaskHandle>
 8006d74:	4603      	mov	r3, r0
 8006d76:	61bb      	str	r3, [r7, #24]
 8006d78:	4a16      	ldr	r2, [pc, #88]	; (8006dd4 <terminal_manager+0x98>)
 8006d7a:	f107 0308 	add.w	r3, r7, #8
 8006d7e:	6013      	str	r3, [r2, #0]
 8006d80:	f7ff fe20 	bl	80069c4 <terminal_hw_config>
 8006d84:	f107 0308 	add.w	r3, r7, #8
 8006d88:	4618      	mov	r0, r3
 8006d8a:	f7ff ff77 	bl	8006c7c <term_request>
 8006d8e:	4603      	mov	r3, r0
 8006d90:	77fb      	strb	r3, [r7, #31]
 8006d92:	7ffb      	ldrb	r3, [r7, #31]
 8006d94:	2b00      	cmp	r3, #0
 8006d96:	d019      	beq.n	8006dcc <terminal_manager+0x90>
 8006d98:	7ffb      	ldrb	r3, [r7, #31]
 8006d9a:	2b80      	cmp	r3, #128	; 0x80
 8006d9c:	d816      	bhi.n	8006dcc <terminal_manager+0x90>
 8006d9e:	7ffb      	ldrb	r3, [r7, #31]
 8006da0:	4a0d      	ldr	r2, [pc, #52]	; (8006dd8 <terminal_manager+0x9c>)
 8006da2:	f852 3023 	ldr.w	r3, [r2, r3, lsl #2]
 8006da6:	2b00      	cmp	r3, #0
 8006da8:	d010      	beq.n	8006dcc <terminal_manager+0x90>
 8006daa:	7ffb      	ldrb	r3, [r7, #31]
 8006dac:	4a0a      	ldr	r2, [pc, #40]	; (8006dd8 <terminal_manager+0x9c>)
 8006dae:	f852 3023 	ldr.w	r3, [r2, r3, lsl #2]
 8006db2:	693a      	ldr	r2, [r7, #16]
 8006db4:	4610      	mov	r0, r2
 8006db6:	4798      	blx	r3
 8006db8:	4603      	mov	r3, r0
 8006dba:	77bb      	strb	r3, [r7, #30]
 8006dbc:	7fba      	ldrb	r2, [r7, #30]
 8006dbe:	f107 0308 	add.w	r3, r7, #8
 8006dc2:	4611      	mov	r1, r2
 8006dc4:	4618      	mov	r0, r3
 8006dc6:	f7ff ff73 	bl	8006cb0 <term_response>
 8006dca:	e7db      	b.n	8006d84 <terminal_manager+0x48>
 8006dcc:	bf00      	nop
 8006dce:	e7d9      	b.n	8006d84 <terminal_manager+0x48>
 8006dd0:	40004800 	.word	0x40004800
 8006dd4:	20000bf4 	.word	0x20000bf4
 8006dd8:	0800d168 	.word	0x0800d168

08006ddc <cmd_get_col_av_data>:
 8006ddc:	b580      	push	{r7, lr}
 8006dde:	b082      	sub	sp, #8
 8006de0:	af00      	add	r7, sp, #0
 8006de2:	6078      	str	r0, [r7, #4]
 8006de4:	4b0a      	ldr	r3, [pc, #40]	; (8006e10 <cmd_get_col_av_data+0x34>)
 8006de6:	681b      	ldr	r3, [r3, #0]
 8006de8:	68db      	ldr	r3, [r3, #12]
 8006dea:	2203      	movs	r2, #3
 8006dec:	4619      	mov	r1, r3
 8006dee:	6878      	ldr	r0, [r7, #4]
 8006df0:	f003 fd8c 	bl	800a90c <memcpy>
 8006df4:	4b06      	ldr	r3, [pc, #24]	; (8006e10 <cmd_get_col_av_data+0x34>)
 8006df6:	681b      	ldr	r3, [r3, #0]
 8006df8:	68db      	ldr	r3, [r3, #12]
 8006dfa:	3302      	adds	r3, #2
 8006dfc:	781b      	ldrb	r3, [r3, #0]
 8006dfe:	b29a      	uxth	r2, r3
 8006e00:	4b04      	ldr	r3, [pc, #16]	; (8006e14 <cmd_get_col_av_data+0x38>)
 8006e02:	801a      	strh	r2, [r3, #0]
 8006e04:	2303      	movs	r3, #3
 8006e06:	4618      	mov	r0, r3
 8006e08:	3708      	adds	r7, #8
 8006e0a:	46bd      	mov	sp, r7
 8006e0c:	bd80      	pop	{r7, pc}
 8006e0e:	bf00      	nop
 8006e10:	20000bf4 	.word	0x20000bf4
 8006e14:	200177f0 	.word	0x200177f0

08006e18 <USART3_IRQHandler>:
 8006e18:	b580      	push	{r7, lr}
 8006e1a:	b082      	sub	sp, #8
 8006e1c:	af00      	add	r7, sp, #0
 8006e1e:	2300      	movs	r3, #0
 8006e20:	607b      	str	r3, [r7, #4]
 8006e22:	480b      	ldr	r0, [pc, #44]	; (8006e50 <USART3_IRQHandler+0x38>)
 8006e24:	f7ff fad2 	bl	80063cc <LL_USART_ClearFlag_IDLE>
 8006e28:	2101      	movs	r1, #1
 8006e2a:	480a      	ldr	r0, [pc, #40]	; (8006e54 <USART3_IRQHandler+0x3c>)
 8006e2c:	f7ff fb86 	bl	800653c <LL_DMA_DisableStream>
 8006e30:	687b      	ldr	r3, [r7, #4]
 8006e32:	2b00      	cmp	r3, #0
 8006e34:	d007      	beq.n	8006e46 <USART3_IRQHandler+0x2e>
 8006e36:	4b08      	ldr	r3, [pc, #32]	; (8006e58 <USART3_IRQHandler+0x40>)
 8006e38:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006e3c:	601a      	str	r2, [r3, #0]
 8006e3e:	f3bf 8f4f 	dsb	sy
 8006e42:	f3bf 8f6f 	isb	sy
 8006e46:	bf00      	nop
 8006e48:	3708      	adds	r7, #8
 8006e4a:	46bd      	mov	sp, r7
 8006e4c:	bd80      	pop	{r7, pc}
 8006e4e:	bf00      	nop
 8006e50:	40004800 	.word	0x40004800
 8006e54:	40026000 	.word	0x40026000
 8006e58:	e000ed04 	.word	0xe000ed04

08006e5c <DMA1_Stream1_IRQHandler>:
 8006e5c:	b580      	push	{r7, lr}
 8006e5e:	b082      	sub	sp, #8
 8006e60:	af00      	add	r7, sp, #0
 8006e62:	2300      	movs	r3, #0
 8006e64:	607b      	str	r3, [r7, #4]
 8006e66:	4813      	ldr	r0, [pc, #76]	; (8006eb4 <DMA1_Stream1_IRQHandler+0x58>)
 8006e68:	f7ff fc30 	bl	80066cc <LL_DMA_IsActiveFlag_TC1>
 8006e6c:	4603      	mov	r3, r0
 8006e6e:	2b00      	cmp	r3, #0
 8006e70:	d011      	beq.n	8006e96 <DMA1_Stream1_IRQHandler+0x3a>
 8006e72:	4810      	ldr	r0, [pc, #64]	; (8006eb4 <DMA1_Stream1_IRQHandler+0x58>)
 8006e74:	f7ff fc6e 	bl	8006754 <LL_DMA_ClearFlag_TC1>
 8006e78:	480e      	ldr	r0, [pc, #56]	; (8006eb4 <DMA1_Stream1_IRQHandler+0x58>)
 8006e7a:	f7ff fc4f 	bl	800671c <LL_DMA_ClearFlag_HT1>
 8006e7e:	2101      	movs	r1, #1
 8006e80:	480c      	ldr	r0, [pc, #48]	; (8006eb4 <DMA1_Stream1_IRQHandler+0x58>)
 8006e82:	f7ff fb3b 	bl	80064fc <LL_DMA_EnableStream>
 8006e86:	4b0c      	ldr	r3, [pc, #48]	; (8006eb8 <DMA1_Stream1_IRQHandler+0x5c>)
 8006e88:	681b      	ldr	r3, [r3, #0]
 8006e8a:	691b      	ldr	r3, [r3, #16]
 8006e8c:	1d3a      	adds	r2, r7, #4
 8006e8e:	4611      	mov	r1, r2
 8006e90:	4618      	mov	r0, r3
 8006e92:	f002 f88d 	bl	8008fb0 <vTaskNotifyGiveFromISR>
 8006e96:	687b      	ldr	r3, [r7, #4]
 8006e98:	2b00      	cmp	r3, #0
 8006e9a:	d007      	beq.n	8006eac <DMA1_Stream1_IRQHandler+0x50>
 8006e9c:	4b07      	ldr	r3, [pc, #28]	; (8006ebc <DMA1_Stream1_IRQHandler+0x60>)
 8006e9e:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006ea2:	601a      	str	r2, [r3, #0]
 8006ea4:	f3bf 8f4f 	dsb	sy
 8006ea8:	f3bf 8f6f 	isb	sy
 8006eac:	bf00      	nop
 8006eae:	3708      	adds	r7, #8
 8006eb0:	46bd      	mov	sp, r7
 8006eb2:	bd80      	pop	{r7, pc}
 8006eb4:	40026000 	.word	0x40026000
 8006eb8:	20000bf4 	.word	0x20000bf4
 8006ebc:	e000ed04 	.word	0xe000ed04

08006ec0 <USART1_IRQHandler>:
 8006ec0:	b580      	push	{r7, lr}
 8006ec2:	b082      	sub	sp, #8
 8006ec4:	af00      	add	r7, sp, #0
 8006ec6:	2300      	movs	r3, #0
 8006ec8:	607b      	str	r3, [r7, #4]
 8006eca:	480b      	ldr	r0, [pc, #44]	; (8006ef8 <USART1_IRQHandler+0x38>)
 8006ecc:	f7ff fa7e 	bl	80063cc <LL_USART_ClearFlag_IDLE>
 8006ed0:	2102      	movs	r1, #2
 8006ed2:	480a      	ldr	r0, [pc, #40]	; (8006efc <USART1_IRQHandler+0x3c>)
 8006ed4:	f7ff fb32 	bl	800653c <LL_DMA_DisableStream>
 8006ed8:	687b      	ldr	r3, [r7, #4]
 8006eda:	2b00      	cmp	r3, #0
 8006edc:	d007      	beq.n	8006eee <USART1_IRQHandler+0x2e>
 8006ede:	4b08      	ldr	r3, [pc, #32]	; (8006f00 <USART1_IRQHandler+0x40>)
 8006ee0:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006ee4:	601a      	str	r2, [r3, #0]
 8006ee6:	f3bf 8f4f 	dsb	sy
 8006eea:	f3bf 8f6f 	isb	sy
 8006eee:	bf00      	nop
 8006ef0:	3708      	adds	r7, #8
 8006ef2:	46bd      	mov	sp, r7
 8006ef4:	bd80      	pop	{r7, pc}
 8006ef6:	bf00      	nop
 8006ef8:	40011000 	.word	0x40011000
 8006efc:	40026400 	.word	0x40026400
 8006f00:	e000ed04 	.word	0xe000ed04

08006f04 <DMA2_Stream2_IRQHandler>:
 8006f04:	b580      	push	{r7, lr}
 8006f06:	b082      	sub	sp, #8
 8006f08:	af00      	add	r7, sp, #0
 8006f0a:	2300      	movs	r3, #0
 8006f0c:	607b      	str	r3, [r7, #4]
 8006f0e:	480f      	ldr	r0, [pc, #60]	; (8006f4c <DMA2_Stream2_IRQHandler+0x48>)
 8006f10:	f7ff fbf0 	bl	80066f4 <LL_DMA_IsActiveFlag_TC2>
 8006f14:	4603      	mov	r3, r0
 8006f16:	2b00      	cmp	r3, #0
 8006f18:	d009      	beq.n	8006f2e <DMA2_Stream2_IRQHandler+0x2a>
 8006f1a:	480c      	ldr	r0, [pc, #48]	; (8006f4c <DMA2_Stream2_IRQHandler+0x48>)
 8006f1c:	f7ff fc28 	bl	8006770 <LL_DMA_ClearFlag_TC2>
 8006f20:	480a      	ldr	r0, [pc, #40]	; (8006f4c <DMA2_Stream2_IRQHandler+0x48>)
 8006f22:	f7ff fc09 	bl	8006738 <LL_DMA_ClearFlag_HT2>
 8006f26:	2102      	movs	r1, #2
 8006f28:	4808      	ldr	r0, [pc, #32]	; (8006f4c <DMA2_Stream2_IRQHandler+0x48>)
 8006f2a:	f7ff fae7 	bl	80064fc <LL_DMA_EnableStream>
 8006f2e:	687b      	ldr	r3, [r7, #4]
 8006f30:	2b00      	cmp	r3, #0
 8006f32:	d007      	beq.n	8006f44 <DMA2_Stream2_IRQHandler+0x40>
 8006f34:	4b06      	ldr	r3, [pc, #24]	; (8006f50 <DMA2_Stream2_IRQHandler+0x4c>)
 8006f36:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006f3a:	601a      	str	r2, [r3, #0]
 8006f3c:	f3bf 8f4f 	dsb	sy
 8006f40:	f3bf 8f6f 	isb	sy
 8006f44:	bf00      	nop
 8006f46:	3708      	adds	r7, #8
 8006f48:	46bd      	mov	sp, r7
 8006f4a:	bd80      	pop	{r7, pc}
 8006f4c:	40026400 	.word	0x40026400
 8006f50:	e000ed04 	.word	0xe000ed04

08006f54 <cmd_echo_handler>:
 8006f54:	b480      	push	{r7}
 8006f56:	b083      	sub	sp, #12
 8006f58:	af00      	add	r7, sp, #0
 8006f5a:	6078      	str	r0, [r7, #4]
 8006f5c:	2304      	movs	r3, #4
 8006f5e:	4618      	mov	r0, r3
 8006f60:	370c      	adds	r7, #12
 8006f62:	46bd      	mov	sp, r7
 8006f64:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006f68:	4770      	bx	lr

08006f6a <vListInitialise>:
 8006f6a:	b480      	push	{r7}
 8006f6c:	b083      	sub	sp, #12
 8006f6e:	af00      	add	r7, sp, #0
 8006f70:	6078      	str	r0, [r7, #4]
 8006f72:	687b      	ldr	r3, [r7, #4]
 8006f74:	f103 0208 	add.w	r2, r3, #8
 8006f78:	687b      	ldr	r3, [r7, #4]
 8006f7a:	605a      	str	r2, [r3, #4]
 8006f7c:	687b      	ldr	r3, [r7, #4]
 8006f7e:	f04f 32ff 	mov.w	r2, #4294967295
 8006f82:	609a      	str	r2, [r3, #8]
 8006f84:	687b      	ldr	r3, [r7, #4]
 8006f86:	f103 0208 	add.w	r2, r3, #8
 8006f8a:	687b      	ldr	r3, [r7, #4]
 8006f8c:	60da      	str	r2, [r3, #12]
 8006f8e:	687b      	ldr	r3, [r7, #4]
 8006f90:	f103 0208 	add.w	r2, r3, #8
 8006f94:	687b      	ldr	r3, [r7, #4]
 8006f96:	611a      	str	r2, [r3, #16]
 8006f98:	687b      	ldr	r3, [r7, #4]
 8006f9a:	2200      	movs	r2, #0
 8006f9c:	601a      	str	r2, [r3, #0]
 8006f9e:	bf00      	nop
 8006fa0:	370c      	adds	r7, #12
 8006fa2:	46bd      	mov	sp, r7
 8006fa4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006fa8:	4770      	bx	lr

08006faa <vListInitialiseItem>:
 8006faa:	b480      	push	{r7}
 8006fac:	b083      	sub	sp, #12
 8006fae:	af00      	add	r7, sp, #0
 8006fb0:	6078      	str	r0, [r7, #4]
 8006fb2:	687b      	ldr	r3, [r7, #4]
 8006fb4:	2200      	movs	r2, #0
 8006fb6:	611a      	str	r2, [r3, #16]
 8006fb8:	bf00      	nop
 8006fba:	370c      	adds	r7, #12
 8006fbc:	46bd      	mov	sp, r7
 8006fbe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006fc2:	4770      	bx	lr

08006fc4 <vListInsertEnd>:
 8006fc4:	b480      	push	{r7}
 8006fc6:	b085      	sub	sp, #20
 8006fc8:	af00      	add	r7, sp, #0
 8006fca:	6078      	str	r0, [r7, #4]
 8006fcc:	6039      	str	r1, [r7, #0]
 8006fce:	687b      	ldr	r3, [r7, #4]
 8006fd0:	685b      	ldr	r3, [r3, #4]
 8006fd2:	60fb      	str	r3, [r7, #12]
 8006fd4:	683b      	ldr	r3, [r7, #0]
 8006fd6:	68fa      	ldr	r2, [r7, #12]
 8006fd8:	605a      	str	r2, [r3, #4]
 8006fda:	68fb      	ldr	r3, [r7, #12]
 8006fdc:	689a      	ldr	r2, [r3, #8]
 8006fde:	683b      	ldr	r3, [r7, #0]
 8006fe0:	609a      	str	r2, [r3, #8]
 8006fe2:	68fb      	ldr	r3, [r7, #12]
 8006fe4:	689b      	ldr	r3, [r3, #8]
 8006fe6:	683a      	ldr	r2, [r7, #0]
 8006fe8:	605a      	str	r2, [r3, #4]
 8006fea:	68fb      	ldr	r3, [r7, #12]
 8006fec:	683a      	ldr	r2, [r7, #0]
 8006fee:	609a      	str	r2, [r3, #8]
 8006ff0:	683b      	ldr	r3, [r7, #0]
 8006ff2:	687a      	ldr	r2, [r7, #4]
 8006ff4:	611a      	str	r2, [r3, #16]
 8006ff6:	687b      	ldr	r3, [r7, #4]
 8006ff8:	681b      	ldr	r3, [r3, #0]
 8006ffa:	1c5a      	adds	r2, r3, #1
 8006ffc:	687b      	ldr	r3, [r7, #4]
 8006ffe:	601a      	str	r2, [r3, #0]
 8007000:	bf00      	nop
 8007002:	3714      	adds	r7, #20
 8007004:	46bd      	mov	sp, r7
 8007006:	f85d 7b04 	ldr.w	r7, [sp], #4
 800700a:	4770      	bx	lr

0800700c <vListInsert>:
 800700c:	b480      	push	{r7}
 800700e:	b085      	sub	sp, #20
 8007010:	af00      	add	r7, sp, #0
 8007012:	6078      	str	r0, [r7, #4]
 8007014:	6039      	str	r1, [r7, #0]
 8007016:	683b      	ldr	r3, [r7, #0]
 8007018:	681b      	ldr	r3, [r3, #0]
 800701a:	60bb      	str	r3, [r7, #8]
 800701c:	68bb      	ldr	r3, [r7, #8]
 800701e:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007022:	d103      	bne.n	800702c <vListInsert+0x20>
 8007024:	687b      	ldr	r3, [r7, #4]
 8007026:	691b      	ldr	r3, [r3, #16]
 8007028:	60fb      	str	r3, [r7, #12]
 800702a:	e00c      	b.n	8007046 <vListInsert+0x3a>
 800702c:	687b      	ldr	r3, [r7, #4]
 800702e:	3308      	adds	r3, #8
 8007030:	60fb      	str	r3, [r7, #12]
 8007032:	e002      	b.n	800703a <vListInsert+0x2e>
 8007034:	68fb      	ldr	r3, [r7, #12]
 8007036:	685b      	ldr	r3, [r3, #4]
 8007038:	60fb      	str	r3, [r7, #12]
 800703a:	68fb      	ldr	r3, [r7, #12]
 800703c:	685b      	ldr	r3, [r3, #4]
 800703e:	681b      	ldr	r3, [r3, #0]
 8007040:	68ba      	ldr	r2, [r7, #8]
 8007042:	429a      	cmp	r2, r3
 8007044:	d2f6      	bcs.n	8007034 <vListInsert+0x28>
 8007046:	68fb      	ldr	r3, [r7, #12]
 8007048:	685a      	ldr	r2, [r3, #4]
 800704a:	683b      	ldr	r3, [r7, #0]
 800704c:	605a      	str	r2, [r3, #4]
 800704e:	683b      	ldr	r3, [r7, #0]
 8007050:	685b      	ldr	r3, [r3, #4]
 8007052:	683a      	ldr	r2, [r7, #0]
 8007054:	609a      	str	r2, [r3, #8]
 8007056:	683b      	ldr	r3, [r7, #0]
 8007058:	68fa      	ldr	r2, [r7, #12]
 800705a:	609a      	str	r2, [r3, #8]
 800705c:	68fb      	ldr	r3, [r7, #12]
 800705e:	683a      	ldr	r2, [r7, #0]
 8007060:	605a      	str	r2, [r3, #4]
 8007062:	683b      	ldr	r3, [r7, #0]
 8007064:	687a      	ldr	r2, [r7, #4]
 8007066:	611a      	str	r2, [r3, #16]
 8007068:	687b      	ldr	r3, [r7, #4]
 800706a:	681b      	ldr	r3, [r3, #0]
 800706c:	1c5a      	adds	r2, r3, #1
 800706e:	687b      	ldr	r3, [r7, #4]
 8007070:	601a      	str	r2, [r3, #0]
 8007072:	bf00      	nop
 8007074:	3714      	adds	r7, #20
 8007076:	46bd      	mov	sp, r7
 8007078:	f85d 7b04 	ldr.w	r7, [sp], #4
 800707c:	4770      	bx	lr

0800707e <uxListRemove>:
 800707e:	b480      	push	{r7}
 8007080:	b085      	sub	sp, #20
 8007082:	af00      	add	r7, sp, #0
 8007084:	6078      	str	r0, [r7, #4]
 8007086:	687b      	ldr	r3, [r7, #4]
 8007088:	691b      	ldr	r3, [r3, #16]
 800708a:	60fb      	str	r3, [r7, #12]
 800708c:	687b      	ldr	r3, [r7, #4]
 800708e:	685b      	ldr	r3, [r3, #4]
 8007090:	687a      	ldr	r2, [r7, #4]
 8007092:	6892      	ldr	r2, [r2, #8]
 8007094:	609a      	str	r2, [r3, #8]
 8007096:	687b      	ldr	r3, [r7, #4]
 8007098:	689b      	ldr	r3, [r3, #8]
 800709a:	687a      	ldr	r2, [r7, #4]
 800709c:	6852      	ldr	r2, [r2, #4]
 800709e:	605a      	str	r2, [r3, #4]
 80070a0:	68fb      	ldr	r3, [r7, #12]
 80070a2:	685b      	ldr	r3, [r3, #4]
 80070a4:	687a      	ldr	r2, [r7, #4]
 80070a6:	429a      	cmp	r2, r3
 80070a8:	d103      	bne.n	80070b2 <uxListRemove+0x34>
 80070aa:	687b      	ldr	r3, [r7, #4]
 80070ac:	689a      	ldr	r2, [r3, #8]
 80070ae:	68fb      	ldr	r3, [r7, #12]
 80070b0:	605a      	str	r2, [r3, #4]
 80070b2:	687b      	ldr	r3, [r7, #4]
 80070b4:	2200      	movs	r2, #0
 80070b6:	611a      	str	r2, [r3, #16]
 80070b8:	68fb      	ldr	r3, [r7, #12]
 80070ba:	681b      	ldr	r3, [r3, #0]
 80070bc:	1e5a      	subs	r2, r3, #1
 80070be:	68fb      	ldr	r3, [r7, #12]
 80070c0:	601a      	str	r2, [r3, #0]
 80070c2:	68fb      	ldr	r3, [r7, #12]
 80070c4:	681b      	ldr	r3, [r3, #0]
 80070c6:	4618      	mov	r0, r3
 80070c8:	3714      	adds	r7, #20
 80070ca:	46bd      	mov	sp, r7
 80070cc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80070d0:	4770      	bx	lr
	...

080070d4 <xQueueGenericReset>:
 80070d4:	b580      	push	{r7, lr}
 80070d6:	b084      	sub	sp, #16
 80070d8:	af00      	add	r7, sp, #0
 80070da:	6078      	str	r0, [r7, #4]
 80070dc:	6039      	str	r1, [r7, #0]
 80070de:	687b      	ldr	r3, [r7, #4]
 80070e0:	60fb      	str	r3, [r7, #12]
 80070e2:	68fb      	ldr	r3, [r7, #12]
 80070e4:	2b00      	cmp	r3, #0
 80070e6:	d109      	bne.n	80070fc <xQueueGenericReset+0x28>
 80070e8:	f04f 0350 	mov.w	r3, #80	; 0x50
 80070ec:	f383 8811 	msr	BASEPRI, r3
 80070f0:	f3bf 8f6f 	isb	sy
 80070f4:	f3bf 8f4f 	dsb	sy
 80070f8:	60bb      	str	r3, [r7, #8]
 80070fa:	e7fe      	b.n	80070fa <xQueueGenericReset+0x26>
 80070fc:	f002 fc66 	bl	80099cc <vPortEnterCritical>
 8007100:	68fb      	ldr	r3, [r7, #12]
 8007102:	681a      	ldr	r2, [r3, #0]
 8007104:	68fb      	ldr	r3, [r7, #12]
 8007106:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8007108:	68f9      	ldr	r1, [r7, #12]
 800710a:	6c09      	ldr	r1, [r1, #64]	; 0x40
 800710c:	fb01 f303 	mul.w	r3, r1, r3
 8007110:	441a      	add	r2, r3
 8007112:	68fb      	ldr	r3, [r7, #12]
 8007114:	609a      	str	r2, [r3, #8]
 8007116:	68fb      	ldr	r3, [r7, #12]
 8007118:	2200      	movs	r2, #0
 800711a:	639a      	str	r2, [r3, #56]	; 0x38
 800711c:	68fb      	ldr	r3, [r7, #12]
 800711e:	681a      	ldr	r2, [r3, #0]
 8007120:	68fb      	ldr	r3, [r7, #12]
 8007122:	605a      	str	r2, [r3, #4]
 8007124:	68fb      	ldr	r3, [r7, #12]
 8007126:	681a      	ldr	r2, [r3, #0]
 8007128:	68fb      	ldr	r3, [r7, #12]
 800712a:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 800712c:	3b01      	subs	r3, #1
 800712e:	68f9      	ldr	r1, [r7, #12]
 8007130:	6c09      	ldr	r1, [r1, #64]	; 0x40
 8007132:	fb01 f303 	mul.w	r3, r1, r3
 8007136:	441a      	add	r2, r3
 8007138:	68fb      	ldr	r3, [r7, #12]
 800713a:	60da      	str	r2, [r3, #12]
 800713c:	68fb      	ldr	r3, [r7, #12]
 800713e:	22ff      	movs	r2, #255	; 0xff
 8007140:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007144:	68fb      	ldr	r3, [r7, #12]
 8007146:	22ff      	movs	r2, #255	; 0xff
 8007148:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 800714c:	683b      	ldr	r3, [r7, #0]
 800714e:	2b00      	cmp	r3, #0
 8007150:	d114      	bne.n	800717c <xQueueGenericReset+0xa8>
 8007152:	68fb      	ldr	r3, [r7, #12]
 8007154:	691b      	ldr	r3, [r3, #16]
 8007156:	2b00      	cmp	r3, #0
 8007158:	d01a      	beq.n	8007190 <xQueueGenericReset+0xbc>
 800715a:	68fb      	ldr	r3, [r7, #12]
 800715c:	3310      	adds	r3, #16
 800715e:	4618      	mov	r0, r3
 8007160:	f001 fa82 	bl	8008668 <xTaskRemoveFromEventList>
 8007164:	4603      	mov	r3, r0
 8007166:	2b00      	cmp	r3, #0
 8007168:	d012      	beq.n	8007190 <xQueueGenericReset+0xbc>
 800716a:	4b0d      	ldr	r3, [pc, #52]	; (80071a0 <xQueueGenericReset+0xcc>)
 800716c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007170:	601a      	str	r2, [r3, #0]
 8007172:	f3bf 8f4f 	dsb	sy
 8007176:	f3bf 8f6f 	isb	sy
 800717a:	e009      	b.n	8007190 <xQueueGenericReset+0xbc>
 800717c:	68fb      	ldr	r3, [r7, #12]
 800717e:	3310      	adds	r3, #16
 8007180:	4618      	mov	r0, r3
 8007182:	f7ff fef2 	bl	8006f6a <vListInitialise>
 8007186:	68fb      	ldr	r3, [r7, #12]
 8007188:	3324      	adds	r3, #36	; 0x24
 800718a:	4618      	mov	r0, r3
 800718c:	f7ff feed 	bl	8006f6a <vListInitialise>
 8007190:	f002 fc4a 	bl	8009a28 <vPortExitCritical>
 8007194:	2301      	movs	r3, #1
 8007196:	4618      	mov	r0, r3
 8007198:	3710      	adds	r7, #16
 800719a:	46bd      	mov	sp, r7
 800719c:	bd80      	pop	{r7, pc}
 800719e:	bf00      	nop
 80071a0:	e000ed04 	.word	0xe000ed04

080071a4 <xQueueGenericCreateStatic>:
 80071a4:	b580      	push	{r7, lr}
 80071a6:	b08e      	sub	sp, #56	; 0x38
 80071a8:	af02      	add	r7, sp, #8
 80071aa:	60f8      	str	r0, [r7, #12]
 80071ac:	60b9      	str	r1, [r7, #8]
 80071ae:	607a      	str	r2, [r7, #4]
 80071b0:	603b      	str	r3, [r7, #0]
 80071b2:	68fb      	ldr	r3, [r7, #12]
 80071b4:	2b00      	cmp	r3, #0
 80071b6:	d109      	bne.n	80071cc <xQueueGenericCreateStatic+0x28>
 80071b8:	f04f 0350 	mov.w	r3, #80	; 0x50
 80071bc:	f383 8811 	msr	BASEPRI, r3
 80071c0:	f3bf 8f6f 	isb	sy
 80071c4:	f3bf 8f4f 	dsb	sy
 80071c8:	62bb      	str	r3, [r7, #40]	; 0x28
 80071ca:	e7fe      	b.n	80071ca <xQueueGenericCreateStatic+0x26>
 80071cc:	683b      	ldr	r3, [r7, #0]
 80071ce:	2b00      	cmp	r3, #0
 80071d0:	d109      	bne.n	80071e6 <xQueueGenericCreateStatic+0x42>
 80071d2:	f04f 0350 	mov.w	r3, #80	; 0x50
 80071d6:	f383 8811 	msr	BASEPRI, r3
 80071da:	f3bf 8f6f 	isb	sy
 80071de:	f3bf 8f4f 	dsb	sy
 80071e2:	627b      	str	r3, [r7, #36]	; 0x24
 80071e4:	e7fe      	b.n	80071e4 <xQueueGenericCreateStatic+0x40>
 80071e6:	687b      	ldr	r3, [r7, #4]
 80071e8:	2b00      	cmp	r3, #0
 80071ea:	d002      	beq.n	80071f2 <xQueueGenericCreateStatic+0x4e>
 80071ec:	68bb      	ldr	r3, [r7, #8]
 80071ee:	2b00      	cmp	r3, #0
 80071f0:	d001      	beq.n	80071f6 <xQueueGenericCreateStatic+0x52>
 80071f2:	2301      	movs	r3, #1
 80071f4:	e000      	b.n	80071f8 <xQueueGenericCreateStatic+0x54>
 80071f6:	2300      	movs	r3, #0
 80071f8:	2b00      	cmp	r3, #0
 80071fa:	d109      	bne.n	8007210 <xQueueGenericCreateStatic+0x6c>
 80071fc:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007200:	f383 8811 	msr	BASEPRI, r3
 8007204:	f3bf 8f6f 	isb	sy
 8007208:	f3bf 8f4f 	dsb	sy
 800720c:	623b      	str	r3, [r7, #32]
 800720e:	e7fe      	b.n	800720e <xQueueGenericCreateStatic+0x6a>
 8007210:	687b      	ldr	r3, [r7, #4]
 8007212:	2b00      	cmp	r3, #0
 8007214:	d102      	bne.n	800721c <xQueueGenericCreateStatic+0x78>
 8007216:	68bb      	ldr	r3, [r7, #8]
 8007218:	2b00      	cmp	r3, #0
 800721a:	d101      	bne.n	8007220 <xQueueGenericCreateStatic+0x7c>
 800721c:	2301      	movs	r3, #1
 800721e:	e000      	b.n	8007222 <xQueueGenericCreateStatic+0x7e>
 8007220:	2300      	movs	r3, #0
 8007222:	2b00      	cmp	r3, #0
 8007224:	d109      	bne.n	800723a <xQueueGenericCreateStatic+0x96>
 8007226:	f04f 0350 	mov.w	r3, #80	; 0x50
 800722a:	f383 8811 	msr	BASEPRI, r3
 800722e:	f3bf 8f6f 	isb	sy
 8007232:	f3bf 8f4f 	dsb	sy
 8007236:	61fb      	str	r3, [r7, #28]
 8007238:	e7fe      	b.n	8007238 <xQueueGenericCreateStatic+0x94>
 800723a:	2350      	movs	r3, #80	; 0x50
 800723c:	617b      	str	r3, [r7, #20]
 800723e:	697b      	ldr	r3, [r7, #20]
 8007240:	2b50      	cmp	r3, #80	; 0x50
 8007242:	d009      	beq.n	8007258 <xQueueGenericCreateStatic+0xb4>
 8007244:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007248:	f383 8811 	msr	BASEPRI, r3
 800724c:	f3bf 8f6f 	isb	sy
 8007250:	f3bf 8f4f 	dsb	sy
 8007254:	61bb      	str	r3, [r7, #24]
 8007256:	e7fe      	b.n	8007256 <xQueueGenericCreateStatic+0xb2>
 8007258:	697b      	ldr	r3, [r7, #20]
 800725a:	683b      	ldr	r3, [r7, #0]
 800725c:	62fb      	str	r3, [r7, #44]	; 0x2c
 800725e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007260:	2b00      	cmp	r3, #0
 8007262:	d00d      	beq.n	8007280 <xQueueGenericCreateStatic+0xdc>
 8007264:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007266:	2201      	movs	r2, #1
 8007268:	f883 2046 	strb.w	r2, [r3, #70]	; 0x46
 800726c:	f897 2038 	ldrb.w	r2, [r7, #56]	; 0x38
 8007270:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007272:	9300      	str	r3, [sp, #0]
 8007274:	4613      	mov	r3, r2
 8007276:	687a      	ldr	r2, [r7, #4]
 8007278:	68b9      	ldr	r1, [r7, #8]
 800727a:	68f8      	ldr	r0, [r7, #12]
 800727c:	f000 f805 	bl	800728a <prvInitialiseNewQueue>
 8007280:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007282:	4618      	mov	r0, r3
 8007284:	3730      	adds	r7, #48	; 0x30
 8007286:	46bd      	mov	sp, r7
 8007288:	bd80      	pop	{r7, pc}

0800728a <prvInitialiseNewQueue>:
 800728a:	b580      	push	{r7, lr}
 800728c:	b084      	sub	sp, #16
 800728e:	af00      	add	r7, sp, #0
 8007290:	60f8      	str	r0, [r7, #12]
 8007292:	60b9      	str	r1, [r7, #8]
 8007294:	607a      	str	r2, [r7, #4]
 8007296:	70fb      	strb	r3, [r7, #3]
 8007298:	68bb      	ldr	r3, [r7, #8]
 800729a:	2b00      	cmp	r3, #0
 800729c:	d103      	bne.n	80072a6 <prvInitialiseNewQueue+0x1c>
 800729e:	69bb      	ldr	r3, [r7, #24]
 80072a0:	69ba      	ldr	r2, [r7, #24]
 80072a2:	601a      	str	r2, [r3, #0]
 80072a4:	e002      	b.n	80072ac <prvInitialiseNewQueue+0x22>
 80072a6:	69bb      	ldr	r3, [r7, #24]
 80072a8:	687a      	ldr	r2, [r7, #4]
 80072aa:	601a      	str	r2, [r3, #0]
 80072ac:	69bb      	ldr	r3, [r7, #24]
 80072ae:	68fa      	ldr	r2, [r7, #12]
 80072b0:	63da      	str	r2, [r3, #60]	; 0x3c
 80072b2:	69bb      	ldr	r3, [r7, #24]
 80072b4:	68ba      	ldr	r2, [r7, #8]
 80072b6:	641a      	str	r2, [r3, #64]	; 0x40
 80072b8:	2101      	movs	r1, #1
 80072ba:	69b8      	ldr	r0, [r7, #24]
 80072bc:	f7ff ff0a 	bl	80070d4 <xQueueGenericReset>
 80072c0:	69bb      	ldr	r3, [r7, #24]
 80072c2:	78fa      	ldrb	r2, [r7, #3]
 80072c4:	f883 204c 	strb.w	r2, [r3, #76]	; 0x4c
 80072c8:	bf00      	nop
 80072ca:	3710      	adds	r7, #16
 80072cc:	46bd      	mov	sp, r7
 80072ce:	bd80      	pop	{r7, pc}

080072d0 <prvInitialiseMutex>:
 80072d0:	b580      	push	{r7, lr}
 80072d2:	b082      	sub	sp, #8
 80072d4:	af00      	add	r7, sp, #0
 80072d6:	6078      	str	r0, [r7, #4]
 80072d8:	687b      	ldr	r3, [r7, #4]
 80072da:	2b00      	cmp	r3, #0
 80072dc:	d00e      	beq.n	80072fc <prvInitialiseMutex+0x2c>
 80072de:	687b      	ldr	r3, [r7, #4]
 80072e0:	2200      	movs	r2, #0
 80072e2:	609a      	str	r2, [r3, #8]
 80072e4:	687b      	ldr	r3, [r7, #4]
 80072e6:	2200      	movs	r2, #0
 80072e8:	601a      	str	r2, [r3, #0]
 80072ea:	687b      	ldr	r3, [r7, #4]
 80072ec:	2200      	movs	r2, #0
 80072ee:	60da      	str	r2, [r3, #12]
 80072f0:	2300      	movs	r3, #0
 80072f2:	2200      	movs	r2, #0
 80072f4:	2100      	movs	r1, #0
 80072f6:	6878      	ldr	r0, [r7, #4]
 80072f8:	f000 f820 	bl	800733c <xQueueGenericSend>
 80072fc:	bf00      	nop
 80072fe:	3708      	adds	r7, #8
 8007300:	46bd      	mov	sp, r7
 8007302:	bd80      	pop	{r7, pc}

08007304 <xQueueCreateMutexStatic>:
 8007304:	b580      	push	{r7, lr}
 8007306:	b088      	sub	sp, #32
 8007308:	af02      	add	r7, sp, #8
 800730a:	4603      	mov	r3, r0
 800730c:	6039      	str	r1, [r7, #0]
 800730e:	71fb      	strb	r3, [r7, #7]
 8007310:	2301      	movs	r3, #1
 8007312:	617b      	str	r3, [r7, #20]
 8007314:	2300      	movs	r3, #0
 8007316:	613b      	str	r3, [r7, #16]
 8007318:	79fb      	ldrb	r3, [r7, #7]
 800731a:	9300      	str	r3, [sp, #0]
 800731c:	683b      	ldr	r3, [r7, #0]
 800731e:	2200      	movs	r2, #0
 8007320:	6939      	ldr	r1, [r7, #16]
 8007322:	6978      	ldr	r0, [r7, #20]
 8007324:	f7ff ff3e 	bl	80071a4 <xQueueGenericCreateStatic>
 8007328:	60f8      	str	r0, [r7, #12]
 800732a:	68f8      	ldr	r0, [r7, #12]
 800732c:	f7ff ffd0 	bl	80072d0 <prvInitialiseMutex>
 8007330:	68fb      	ldr	r3, [r7, #12]
 8007332:	4618      	mov	r0, r3
 8007334:	3718      	adds	r7, #24
 8007336:	46bd      	mov	sp, r7
 8007338:	bd80      	pop	{r7, pc}
	...

0800733c <xQueueGenericSend>:
 800733c:	b580      	push	{r7, lr}
 800733e:	b08e      	sub	sp, #56	; 0x38
 8007340:	af00      	add	r7, sp, #0
 8007342:	60f8      	str	r0, [r7, #12]
 8007344:	60b9      	str	r1, [r7, #8]
 8007346:	607a      	str	r2, [r7, #4]
 8007348:	603b      	str	r3, [r7, #0]
 800734a:	2300      	movs	r3, #0
 800734c:	637b      	str	r3, [r7, #52]	; 0x34
 800734e:	68fb      	ldr	r3, [r7, #12]
 8007350:	633b      	str	r3, [r7, #48]	; 0x30
 8007352:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007354:	2b00      	cmp	r3, #0
 8007356:	d109      	bne.n	800736c <xQueueGenericSend+0x30>
 8007358:	f04f 0350 	mov.w	r3, #80	; 0x50
 800735c:	f383 8811 	msr	BASEPRI, r3
 8007360:	f3bf 8f6f 	isb	sy
 8007364:	f3bf 8f4f 	dsb	sy
 8007368:	62bb      	str	r3, [r7, #40]	; 0x28
 800736a:	e7fe      	b.n	800736a <xQueueGenericSend+0x2e>
 800736c:	68bb      	ldr	r3, [r7, #8]
 800736e:	2b00      	cmp	r3, #0
 8007370:	d103      	bne.n	800737a <xQueueGenericSend+0x3e>
 8007372:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007374:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007376:	2b00      	cmp	r3, #0
 8007378:	d101      	bne.n	800737e <xQueueGenericSend+0x42>
 800737a:	2301      	movs	r3, #1
 800737c:	e000      	b.n	8007380 <xQueueGenericSend+0x44>
 800737e:	2300      	movs	r3, #0
 8007380:	2b00      	cmp	r3, #0
 8007382:	d109      	bne.n	8007398 <xQueueGenericSend+0x5c>
 8007384:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007388:	f383 8811 	msr	BASEPRI, r3
 800738c:	f3bf 8f6f 	isb	sy
 8007390:	f3bf 8f4f 	dsb	sy
 8007394:	627b      	str	r3, [r7, #36]	; 0x24
 8007396:	e7fe      	b.n	8007396 <xQueueGenericSend+0x5a>
 8007398:	683b      	ldr	r3, [r7, #0]
 800739a:	2b02      	cmp	r3, #2
 800739c:	d103      	bne.n	80073a6 <xQueueGenericSend+0x6a>
 800739e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80073a0:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 80073a2:	2b01      	cmp	r3, #1
 80073a4:	d101      	bne.n	80073aa <xQueueGenericSend+0x6e>
 80073a6:	2301      	movs	r3, #1
 80073a8:	e000      	b.n	80073ac <xQueueGenericSend+0x70>
 80073aa:	2300      	movs	r3, #0
 80073ac:	2b00      	cmp	r3, #0
 80073ae:	d109      	bne.n	80073c4 <xQueueGenericSend+0x88>
 80073b0:	f04f 0350 	mov.w	r3, #80	; 0x50
 80073b4:	f383 8811 	msr	BASEPRI, r3
 80073b8:	f3bf 8f6f 	isb	sy
 80073bc:	f3bf 8f4f 	dsb	sy
 80073c0:	623b      	str	r3, [r7, #32]
 80073c2:	e7fe      	b.n	80073c2 <xQueueGenericSend+0x86>
 80073c4:	f001 fb1e 	bl	8008a04 <xTaskGetSchedulerState>
 80073c8:	4603      	mov	r3, r0
 80073ca:	2b00      	cmp	r3, #0
 80073cc:	d102      	bne.n	80073d4 <xQueueGenericSend+0x98>
 80073ce:	687b      	ldr	r3, [r7, #4]
 80073d0:	2b00      	cmp	r3, #0
 80073d2:	d101      	bne.n	80073d8 <xQueueGenericSend+0x9c>
 80073d4:	2301      	movs	r3, #1
 80073d6:	e000      	b.n	80073da <xQueueGenericSend+0x9e>
 80073d8:	2300      	movs	r3, #0
 80073da:	2b00      	cmp	r3, #0
 80073dc:	d109      	bne.n	80073f2 <xQueueGenericSend+0xb6>
 80073de:	f04f 0350 	mov.w	r3, #80	; 0x50
 80073e2:	f383 8811 	msr	BASEPRI, r3
 80073e6:	f3bf 8f6f 	isb	sy
 80073ea:	f3bf 8f4f 	dsb	sy
 80073ee:	61fb      	str	r3, [r7, #28]
 80073f0:	e7fe      	b.n	80073f0 <xQueueGenericSend+0xb4>
 80073f2:	f002 faeb 	bl	80099cc <vPortEnterCritical>
 80073f6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80073f8:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 80073fa:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80073fc:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 80073fe:	429a      	cmp	r2, r3
 8007400:	d302      	bcc.n	8007408 <xQueueGenericSend+0xcc>
 8007402:	683b      	ldr	r3, [r7, #0]
 8007404:	2b02      	cmp	r3, #2
 8007406:	d129      	bne.n	800745c <xQueueGenericSend+0x120>
 8007408:	683a      	ldr	r2, [r7, #0]
 800740a:	68b9      	ldr	r1, [r7, #8]
 800740c:	6b38      	ldr	r0, [r7, #48]	; 0x30
 800740e:	f000 fb1f 	bl	8007a50 <prvCopyDataToQueue>
 8007412:	62f8      	str	r0, [r7, #44]	; 0x2c
 8007414:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007416:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8007418:	2b00      	cmp	r3, #0
 800741a:	d010      	beq.n	800743e <xQueueGenericSend+0x102>
 800741c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800741e:	3324      	adds	r3, #36	; 0x24
 8007420:	4618      	mov	r0, r3
 8007422:	f001 f921 	bl	8008668 <xTaskRemoveFromEventList>
 8007426:	4603      	mov	r3, r0
 8007428:	2b00      	cmp	r3, #0
 800742a:	d013      	beq.n	8007454 <xQueueGenericSend+0x118>
 800742c:	4b3f      	ldr	r3, [pc, #252]	; (800752c <xQueueGenericSend+0x1f0>)
 800742e:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007432:	601a      	str	r2, [r3, #0]
 8007434:	f3bf 8f4f 	dsb	sy
 8007438:	f3bf 8f6f 	isb	sy
 800743c:	e00a      	b.n	8007454 <xQueueGenericSend+0x118>
 800743e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007440:	2b00      	cmp	r3, #0
 8007442:	d007      	beq.n	8007454 <xQueueGenericSend+0x118>
 8007444:	4b39      	ldr	r3, [pc, #228]	; (800752c <xQueueGenericSend+0x1f0>)
 8007446:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800744a:	601a      	str	r2, [r3, #0]
 800744c:	f3bf 8f4f 	dsb	sy
 8007450:	f3bf 8f6f 	isb	sy
 8007454:	f002 fae8 	bl	8009a28 <vPortExitCritical>
 8007458:	2301      	movs	r3, #1
 800745a:	e063      	b.n	8007524 <xQueueGenericSend+0x1e8>
 800745c:	687b      	ldr	r3, [r7, #4]
 800745e:	2b00      	cmp	r3, #0
 8007460:	d103      	bne.n	800746a <xQueueGenericSend+0x12e>
 8007462:	f002 fae1 	bl	8009a28 <vPortExitCritical>
 8007466:	2300      	movs	r3, #0
 8007468:	e05c      	b.n	8007524 <xQueueGenericSend+0x1e8>
 800746a:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800746c:	2b00      	cmp	r3, #0
 800746e:	d106      	bne.n	800747e <xQueueGenericSend+0x142>
 8007470:	f107 0314 	add.w	r3, r7, #20
 8007474:	4618      	mov	r0, r3
 8007476:	f001 f959 	bl	800872c <vTaskInternalSetTimeOutState>
 800747a:	2301      	movs	r3, #1
 800747c:	637b      	str	r3, [r7, #52]	; 0x34
 800747e:	f002 fad3 	bl	8009a28 <vPortExitCritical>
 8007482:	f000 fe91 	bl	80081a8 <vTaskSuspendAll>
 8007486:	f002 faa1 	bl	80099cc <vPortEnterCritical>
 800748a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800748c:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007490:	b25b      	sxtb	r3, r3
 8007492:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007496:	d103      	bne.n	80074a0 <xQueueGenericSend+0x164>
 8007498:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800749a:	2200      	movs	r2, #0
 800749c:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 80074a0:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80074a2:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 80074a6:	b25b      	sxtb	r3, r3
 80074a8:	f1b3 3fff 	cmp.w	r3, #4294967295
 80074ac:	d103      	bne.n	80074b6 <xQueueGenericSend+0x17a>
 80074ae:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80074b0:	2200      	movs	r2, #0
 80074b2:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 80074b6:	f002 fab7 	bl	8009a28 <vPortExitCritical>
 80074ba:	1d3a      	adds	r2, r7, #4
 80074bc:	f107 0314 	add.w	r3, r7, #20
 80074c0:	4611      	mov	r1, r2
 80074c2:	4618      	mov	r0, r3
 80074c4:	f001 f948 	bl	8008758 <xTaskCheckForTimeOut>
 80074c8:	4603      	mov	r3, r0
 80074ca:	2b00      	cmp	r3, #0
 80074cc:	d124      	bne.n	8007518 <xQueueGenericSend+0x1dc>
 80074ce:	6b38      	ldr	r0, [r7, #48]	; 0x30
 80074d0:	f000 fbb6 	bl	8007c40 <prvIsQueueFull>
 80074d4:	4603      	mov	r3, r0
 80074d6:	2b00      	cmp	r3, #0
 80074d8:	d018      	beq.n	800750c <xQueueGenericSend+0x1d0>
 80074da:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80074dc:	3310      	adds	r3, #16
 80074de:	687a      	ldr	r2, [r7, #4]
 80074e0:	4611      	mov	r1, r2
 80074e2:	4618      	mov	r0, r3
 80074e4:	f001 f872 	bl	80085cc <vTaskPlaceOnEventList>
 80074e8:	6b38      	ldr	r0, [r7, #48]	; 0x30
 80074ea:	f000 fb41 	bl	8007b70 <prvUnlockQueue>
 80074ee:	f000 fe69 	bl	80081c4 <xTaskResumeAll>
 80074f2:	4603      	mov	r3, r0
 80074f4:	2b00      	cmp	r3, #0
 80074f6:	f47f af7c 	bne.w	80073f2 <xQueueGenericSend+0xb6>
 80074fa:	4b0c      	ldr	r3, [pc, #48]	; (800752c <xQueueGenericSend+0x1f0>)
 80074fc:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007500:	601a      	str	r2, [r3, #0]
 8007502:	f3bf 8f4f 	dsb	sy
 8007506:	f3bf 8f6f 	isb	sy
 800750a:	e772      	b.n	80073f2 <xQueueGenericSend+0xb6>
 800750c:	6b38      	ldr	r0, [r7, #48]	; 0x30
 800750e:	f000 fb2f 	bl	8007b70 <prvUnlockQueue>
 8007512:	f000 fe57 	bl	80081c4 <xTaskResumeAll>
 8007516:	e76c      	b.n	80073f2 <xQueueGenericSend+0xb6>
 8007518:	6b38      	ldr	r0, [r7, #48]	; 0x30
 800751a:	f000 fb29 	bl	8007b70 <prvUnlockQueue>
 800751e:	f000 fe51 	bl	80081c4 <xTaskResumeAll>
 8007522:	2300      	movs	r3, #0
 8007524:	4618      	mov	r0, r3
 8007526:	3738      	adds	r7, #56	; 0x38
 8007528:	46bd      	mov	sp, r7
 800752a:	bd80      	pop	{r7, pc}
 800752c:	e000ed04 	.word	0xe000ed04

08007530 <xQueueGenericSendFromISR>:
 8007530:	b580      	push	{r7, lr}
 8007532:	b08e      	sub	sp, #56	; 0x38
 8007534:	af00      	add	r7, sp, #0
 8007536:	60f8      	str	r0, [r7, #12]
 8007538:	60b9      	str	r1, [r7, #8]
 800753a:	607a      	str	r2, [r7, #4]
 800753c:	603b      	str	r3, [r7, #0]
 800753e:	68fb      	ldr	r3, [r7, #12]
 8007540:	633b      	str	r3, [r7, #48]	; 0x30
 8007542:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007544:	2b00      	cmp	r3, #0
 8007546:	d109      	bne.n	800755c <xQueueGenericSendFromISR+0x2c>
 8007548:	f04f 0350 	mov.w	r3, #80	; 0x50
 800754c:	f383 8811 	msr	BASEPRI, r3
 8007550:	f3bf 8f6f 	isb	sy
 8007554:	f3bf 8f4f 	dsb	sy
 8007558:	627b      	str	r3, [r7, #36]	; 0x24
 800755a:	e7fe      	b.n	800755a <xQueueGenericSendFromISR+0x2a>
 800755c:	68bb      	ldr	r3, [r7, #8]
 800755e:	2b00      	cmp	r3, #0
 8007560:	d103      	bne.n	800756a <xQueueGenericSendFromISR+0x3a>
 8007562:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007564:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007566:	2b00      	cmp	r3, #0
 8007568:	d101      	bne.n	800756e <xQueueGenericSendFromISR+0x3e>
 800756a:	2301      	movs	r3, #1
 800756c:	e000      	b.n	8007570 <xQueueGenericSendFromISR+0x40>
 800756e:	2300      	movs	r3, #0
 8007570:	2b00      	cmp	r3, #0
 8007572:	d109      	bne.n	8007588 <xQueueGenericSendFromISR+0x58>
 8007574:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007578:	f383 8811 	msr	BASEPRI, r3
 800757c:	f3bf 8f6f 	isb	sy
 8007580:	f3bf 8f4f 	dsb	sy
 8007584:	623b      	str	r3, [r7, #32]
 8007586:	e7fe      	b.n	8007586 <xQueueGenericSendFromISR+0x56>
 8007588:	683b      	ldr	r3, [r7, #0]
 800758a:	2b02      	cmp	r3, #2
 800758c:	d103      	bne.n	8007596 <xQueueGenericSendFromISR+0x66>
 800758e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007590:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8007592:	2b01      	cmp	r3, #1
 8007594:	d101      	bne.n	800759a <xQueueGenericSendFromISR+0x6a>
 8007596:	2301      	movs	r3, #1
 8007598:	e000      	b.n	800759c <xQueueGenericSendFromISR+0x6c>
 800759a:	2300      	movs	r3, #0
 800759c:	2b00      	cmp	r3, #0
 800759e:	d109      	bne.n	80075b4 <xQueueGenericSendFromISR+0x84>
 80075a0:	f04f 0350 	mov.w	r3, #80	; 0x50
 80075a4:	f383 8811 	msr	BASEPRI, r3
 80075a8:	f3bf 8f6f 	isb	sy
 80075ac:	f3bf 8f4f 	dsb	sy
 80075b0:	61fb      	str	r3, [r7, #28]
 80075b2:	e7fe      	b.n	80075b2 <xQueueGenericSendFromISR+0x82>
 80075b4:	f002 fae6 	bl	8009b84 <vPortValidateInterruptPriority>
 80075b8:	f3ef 8211 	mrs	r2, BASEPRI
 80075bc:	f04f 0350 	mov.w	r3, #80	; 0x50
 80075c0:	f383 8811 	msr	BASEPRI, r3
 80075c4:	f3bf 8f6f 	isb	sy
 80075c8:	f3bf 8f4f 	dsb	sy
 80075cc:	61ba      	str	r2, [r7, #24]
 80075ce:	617b      	str	r3, [r7, #20]
 80075d0:	69bb      	ldr	r3, [r7, #24]
 80075d2:	62fb      	str	r3, [r7, #44]	; 0x2c
 80075d4:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075d6:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 80075d8:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075da:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 80075dc:	429a      	cmp	r2, r3
 80075de:	d302      	bcc.n	80075e6 <xQueueGenericSendFromISR+0xb6>
 80075e0:	683b      	ldr	r3, [r7, #0]
 80075e2:	2b02      	cmp	r3, #2
 80075e4:	d12c      	bne.n	8007640 <xQueueGenericSendFromISR+0x110>
 80075e6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075e8:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 80075ec:	f887 302b 	strb.w	r3, [r7, #43]	; 0x2b
 80075f0:	683a      	ldr	r2, [r7, #0]
 80075f2:	68b9      	ldr	r1, [r7, #8]
 80075f4:	6b38      	ldr	r0, [r7, #48]	; 0x30
 80075f6:	f000 fa2b 	bl	8007a50 <prvCopyDataToQueue>
 80075fa:	f997 302b 	ldrsb.w	r3, [r7, #43]	; 0x2b
 80075fe:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007602:	d112      	bne.n	800762a <xQueueGenericSendFromISR+0xfa>
 8007604:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007606:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8007608:	2b00      	cmp	r3, #0
 800760a:	d016      	beq.n	800763a <xQueueGenericSendFromISR+0x10a>
 800760c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800760e:	3324      	adds	r3, #36	; 0x24
 8007610:	4618      	mov	r0, r3
 8007612:	f001 f829 	bl	8008668 <xTaskRemoveFromEventList>
 8007616:	4603      	mov	r3, r0
 8007618:	2b00      	cmp	r3, #0
 800761a:	d00e      	beq.n	800763a <xQueueGenericSendFromISR+0x10a>
 800761c:	687b      	ldr	r3, [r7, #4]
 800761e:	2b00      	cmp	r3, #0
 8007620:	d00b      	beq.n	800763a <xQueueGenericSendFromISR+0x10a>
 8007622:	687b      	ldr	r3, [r7, #4]
 8007624:	2201      	movs	r2, #1
 8007626:	601a      	str	r2, [r3, #0]
 8007628:	e007      	b.n	800763a <xQueueGenericSendFromISR+0x10a>
 800762a:	f897 302b 	ldrb.w	r3, [r7, #43]	; 0x2b
 800762e:	3301      	adds	r3, #1
 8007630:	b2db      	uxtb	r3, r3
 8007632:	b25a      	sxtb	r2, r3
 8007634:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007636:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 800763a:	2301      	movs	r3, #1
 800763c:	637b      	str	r3, [r7, #52]	; 0x34
 800763e:	e001      	b.n	8007644 <xQueueGenericSendFromISR+0x114>
 8007640:	2300      	movs	r3, #0
 8007642:	637b      	str	r3, [r7, #52]	; 0x34
 8007644:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007646:	613b      	str	r3, [r7, #16]
 8007648:	693b      	ldr	r3, [r7, #16]
 800764a:	f383 8811 	msr	BASEPRI, r3
 800764e:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007650:	4618      	mov	r0, r3
 8007652:	3738      	adds	r7, #56	; 0x38
 8007654:	46bd      	mov	sp, r7
 8007656:	bd80      	pop	{r7, pc}

08007658 <xQueueReceive>:
 8007658:	b580      	push	{r7, lr}
 800765a:	b08c      	sub	sp, #48	; 0x30
 800765c:	af00      	add	r7, sp, #0
 800765e:	60f8      	str	r0, [r7, #12]
 8007660:	60b9      	str	r1, [r7, #8]
 8007662:	607a      	str	r2, [r7, #4]
 8007664:	2300      	movs	r3, #0
 8007666:	62fb      	str	r3, [r7, #44]	; 0x2c
 8007668:	68fb      	ldr	r3, [r7, #12]
 800766a:	62bb      	str	r3, [r7, #40]	; 0x28
 800766c:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800766e:	2b00      	cmp	r3, #0
 8007670:	d109      	bne.n	8007686 <xQueueReceive+0x2e>
 8007672:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007676:	f383 8811 	msr	BASEPRI, r3
 800767a:	f3bf 8f6f 	isb	sy
 800767e:	f3bf 8f4f 	dsb	sy
 8007682:	623b      	str	r3, [r7, #32]
 8007684:	e7fe      	b.n	8007684 <xQueueReceive+0x2c>
 8007686:	68bb      	ldr	r3, [r7, #8]
 8007688:	2b00      	cmp	r3, #0
 800768a:	d103      	bne.n	8007694 <xQueueReceive+0x3c>
 800768c:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800768e:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007690:	2b00      	cmp	r3, #0
 8007692:	d101      	bne.n	8007698 <xQueueReceive+0x40>
 8007694:	2301      	movs	r3, #1
 8007696:	e000      	b.n	800769a <xQueueReceive+0x42>
 8007698:	2300      	movs	r3, #0
 800769a:	2b00      	cmp	r3, #0
 800769c:	d109      	bne.n	80076b2 <xQueueReceive+0x5a>
 800769e:	f04f 0350 	mov.w	r3, #80	; 0x50
 80076a2:	f383 8811 	msr	BASEPRI, r3
 80076a6:	f3bf 8f6f 	isb	sy
 80076aa:	f3bf 8f4f 	dsb	sy
 80076ae:	61fb      	str	r3, [r7, #28]
 80076b0:	e7fe      	b.n	80076b0 <xQueueReceive+0x58>
 80076b2:	f001 f9a7 	bl	8008a04 <xTaskGetSchedulerState>
 80076b6:	4603      	mov	r3, r0
 80076b8:	2b00      	cmp	r3, #0
 80076ba:	d102      	bne.n	80076c2 <xQueueReceive+0x6a>
 80076bc:	687b      	ldr	r3, [r7, #4]
 80076be:	2b00      	cmp	r3, #0
 80076c0:	d101      	bne.n	80076c6 <xQueueReceive+0x6e>
 80076c2:	2301      	movs	r3, #1
 80076c4:	e000      	b.n	80076c8 <xQueueReceive+0x70>
 80076c6:	2300      	movs	r3, #0
 80076c8:	2b00      	cmp	r3, #0
 80076ca:	d109      	bne.n	80076e0 <xQueueReceive+0x88>
 80076cc:	f04f 0350 	mov.w	r3, #80	; 0x50
 80076d0:	f383 8811 	msr	BASEPRI, r3
 80076d4:	f3bf 8f6f 	isb	sy
 80076d8:	f3bf 8f4f 	dsb	sy
 80076dc:	61bb      	str	r3, [r7, #24]
 80076de:	e7fe      	b.n	80076de <xQueueReceive+0x86>
 80076e0:	f002 f974 	bl	80099cc <vPortEnterCritical>
 80076e4:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80076e6:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 80076e8:	627b      	str	r3, [r7, #36]	; 0x24
 80076ea:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80076ec:	2b00      	cmp	r3, #0
 80076ee:	d01f      	beq.n	8007730 <xQueueReceive+0xd8>
 80076f0:	68b9      	ldr	r1, [r7, #8]
 80076f2:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80076f4:	f000 fa16 	bl	8007b24 <prvCopyDataFromQueue>
 80076f8:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80076fa:	1e5a      	subs	r2, r3, #1
 80076fc:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80076fe:	639a      	str	r2, [r3, #56]	; 0x38
 8007700:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007702:	691b      	ldr	r3, [r3, #16]
 8007704:	2b00      	cmp	r3, #0
 8007706:	d00f      	beq.n	8007728 <xQueueReceive+0xd0>
 8007708:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800770a:	3310      	adds	r3, #16
 800770c:	4618      	mov	r0, r3
 800770e:	f000 ffab 	bl	8008668 <xTaskRemoveFromEventList>
 8007712:	4603      	mov	r3, r0
 8007714:	2b00      	cmp	r3, #0
 8007716:	d007      	beq.n	8007728 <xQueueReceive+0xd0>
 8007718:	4b3c      	ldr	r3, [pc, #240]	; (800780c <xQueueReceive+0x1b4>)
 800771a:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800771e:	601a      	str	r2, [r3, #0]
 8007720:	f3bf 8f4f 	dsb	sy
 8007724:	f3bf 8f6f 	isb	sy
 8007728:	f002 f97e 	bl	8009a28 <vPortExitCritical>
 800772c:	2301      	movs	r3, #1
 800772e:	e069      	b.n	8007804 <xQueueReceive+0x1ac>
 8007730:	687b      	ldr	r3, [r7, #4]
 8007732:	2b00      	cmp	r3, #0
 8007734:	d103      	bne.n	800773e <xQueueReceive+0xe6>
 8007736:	f002 f977 	bl	8009a28 <vPortExitCritical>
 800773a:	2300      	movs	r3, #0
 800773c:	e062      	b.n	8007804 <xQueueReceive+0x1ac>
 800773e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007740:	2b00      	cmp	r3, #0
 8007742:	d106      	bne.n	8007752 <xQueueReceive+0xfa>
 8007744:	f107 0310 	add.w	r3, r7, #16
 8007748:	4618      	mov	r0, r3
 800774a:	f000 ffef 	bl	800872c <vTaskInternalSetTimeOutState>
 800774e:	2301      	movs	r3, #1
 8007750:	62fb      	str	r3, [r7, #44]	; 0x2c
 8007752:	f002 f969 	bl	8009a28 <vPortExitCritical>
 8007756:	f000 fd27 	bl	80081a8 <vTaskSuspendAll>
 800775a:	f002 f937 	bl	80099cc <vPortEnterCritical>
 800775e:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007760:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007764:	b25b      	sxtb	r3, r3
 8007766:	f1b3 3fff 	cmp.w	r3, #4294967295
 800776a:	d103      	bne.n	8007774 <xQueueReceive+0x11c>
 800776c:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800776e:	2200      	movs	r2, #0
 8007770:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007774:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007776:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 800777a:	b25b      	sxtb	r3, r3
 800777c:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007780:	d103      	bne.n	800778a <xQueueReceive+0x132>
 8007782:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007784:	2200      	movs	r2, #0
 8007786:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 800778a:	f002 f94d 	bl	8009a28 <vPortExitCritical>
 800778e:	1d3a      	adds	r2, r7, #4
 8007790:	f107 0310 	add.w	r3, r7, #16
 8007794:	4611      	mov	r1, r2
 8007796:	4618      	mov	r0, r3
 8007798:	f000 ffde 	bl	8008758 <xTaskCheckForTimeOut>
 800779c:	4603      	mov	r3, r0
 800779e:	2b00      	cmp	r3, #0
 80077a0:	d123      	bne.n	80077ea <xQueueReceive+0x192>
 80077a2:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80077a4:	f000 fa36 	bl	8007c14 <prvIsQueueEmpty>
 80077a8:	4603      	mov	r3, r0
 80077aa:	2b00      	cmp	r3, #0
 80077ac:	d017      	beq.n	80077de <xQueueReceive+0x186>
 80077ae:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80077b0:	3324      	adds	r3, #36	; 0x24
 80077b2:	687a      	ldr	r2, [r7, #4]
 80077b4:	4611      	mov	r1, r2
 80077b6:	4618      	mov	r0, r3
 80077b8:	f000 ff08 	bl	80085cc <vTaskPlaceOnEventList>
 80077bc:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80077be:	f000 f9d7 	bl	8007b70 <prvUnlockQueue>
 80077c2:	f000 fcff 	bl	80081c4 <xTaskResumeAll>
 80077c6:	4603      	mov	r3, r0
 80077c8:	2b00      	cmp	r3, #0
 80077ca:	d189      	bne.n	80076e0 <xQueueReceive+0x88>
 80077cc:	4b0f      	ldr	r3, [pc, #60]	; (800780c <xQueueReceive+0x1b4>)
 80077ce:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80077d2:	601a      	str	r2, [r3, #0]
 80077d4:	f3bf 8f4f 	dsb	sy
 80077d8:	f3bf 8f6f 	isb	sy
 80077dc:	e780      	b.n	80076e0 <xQueueReceive+0x88>
 80077de:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80077e0:	f000 f9c6 	bl	8007b70 <prvUnlockQueue>
 80077e4:	f000 fcee 	bl	80081c4 <xTaskResumeAll>
 80077e8:	e77a      	b.n	80076e0 <xQueueReceive+0x88>
 80077ea:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80077ec:	f000 f9c0 	bl	8007b70 <prvUnlockQueue>
 80077f0:	f000 fce8 	bl	80081c4 <xTaskResumeAll>
 80077f4:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80077f6:	f000 fa0d 	bl	8007c14 <prvIsQueueEmpty>
 80077fa:	4603      	mov	r3, r0
 80077fc:	2b00      	cmp	r3, #0
 80077fe:	f43f af6f 	beq.w	80076e0 <xQueueReceive+0x88>
 8007802:	2300      	movs	r3, #0
 8007804:	4618      	mov	r0, r3
 8007806:	3730      	adds	r7, #48	; 0x30
 8007808:	46bd      	mov	sp, r7
 800780a:	bd80      	pop	{r7, pc}
 800780c:	e000ed04 	.word	0xe000ed04

08007810 <xQueueSemaphoreTake>:
 8007810:	b580      	push	{r7, lr}
 8007812:	b08e      	sub	sp, #56	; 0x38
 8007814:	af00      	add	r7, sp, #0
 8007816:	6078      	str	r0, [r7, #4]
 8007818:	6039      	str	r1, [r7, #0]
 800781a:	2300      	movs	r3, #0
 800781c:	637b      	str	r3, [r7, #52]	; 0x34
 800781e:	687b      	ldr	r3, [r7, #4]
 8007820:	62fb      	str	r3, [r7, #44]	; 0x2c
 8007822:	2300      	movs	r3, #0
 8007824:	633b      	str	r3, [r7, #48]	; 0x30
 8007826:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007828:	2b00      	cmp	r3, #0
 800782a:	d109      	bne.n	8007840 <xQueueSemaphoreTake+0x30>
 800782c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007830:	f383 8811 	msr	BASEPRI, r3
 8007834:	f3bf 8f6f 	isb	sy
 8007838:	f3bf 8f4f 	dsb	sy
 800783c:	623b      	str	r3, [r7, #32]
 800783e:	e7fe      	b.n	800783e <xQueueSemaphoreTake+0x2e>
 8007840:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007842:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007844:	2b00      	cmp	r3, #0
 8007846:	d009      	beq.n	800785c <xQueueSemaphoreTake+0x4c>
 8007848:	f04f 0350 	mov.w	r3, #80	; 0x50
 800784c:	f383 8811 	msr	BASEPRI, r3
 8007850:	f3bf 8f6f 	isb	sy
 8007854:	f3bf 8f4f 	dsb	sy
 8007858:	61fb      	str	r3, [r7, #28]
 800785a:	e7fe      	b.n	800785a <xQueueSemaphoreTake+0x4a>
 800785c:	f001 f8d2 	bl	8008a04 <xTaskGetSchedulerState>
 8007860:	4603      	mov	r3, r0
 8007862:	2b00      	cmp	r3, #0
 8007864:	d102      	bne.n	800786c <xQueueSemaphoreTake+0x5c>
 8007866:	683b      	ldr	r3, [r7, #0]
 8007868:	2b00      	cmp	r3, #0
 800786a:	d101      	bne.n	8007870 <xQueueSemaphoreTake+0x60>
 800786c:	2301      	movs	r3, #1
 800786e:	e000      	b.n	8007872 <xQueueSemaphoreTake+0x62>
 8007870:	2300      	movs	r3, #0
 8007872:	2b00      	cmp	r3, #0
 8007874:	d109      	bne.n	800788a <xQueueSemaphoreTake+0x7a>
 8007876:	f04f 0350 	mov.w	r3, #80	; 0x50
 800787a:	f383 8811 	msr	BASEPRI, r3
 800787e:	f3bf 8f6f 	isb	sy
 8007882:	f3bf 8f4f 	dsb	sy
 8007886:	61bb      	str	r3, [r7, #24]
 8007888:	e7fe      	b.n	8007888 <xQueueSemaphoreTake+0x78>
 800788a:	f002 f89f 	bl	80099cc <vPortEnterCritical>
 800788e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007890:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 8007892:	62bb      	str	r3, [r7, #40]	; 0x28
 8007894:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007896:	2b00      	cmp	r3, #0
 8007898:	d024      	beq.n	80078e4 <xQueueSemaphoreTake+0xd4>
 800789a:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800789c:	1e5a      	subs	r2, r3, #1
 800789e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80078a0:	639a      	str	r2, [r3, #56]	; 0x38
 80078a2:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80078a4:	681b      	ldr	r3, [r3, #0]
 80078a6:	2b00      	cmp	r3, #0
 80078a8:	d104      	bne.n	80078b4 <xQueueSemaphoreTake+0xa4>
 80078aa:	f001 fa65 	bl	8008d78 <pvTaskIncrementMutexHeldCount>
 80078ae:	4602      	mov	r2, r0
 80078b0:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80078b2:	609a      	str	r2, [r3, #8]
 80078b4:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80078b6:	691b      	ldr	r3, [r3, #16]
 80078b8:	2b00      	cmp	r3, #0
 80078ba:	d00f      	beq.n	80078dc <xQueueSemaphoreTake+0xcc>
 80078bc:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80078be:	3310      	adds	r3, #16
 80078c0:	4618      	mov	r0, r3
 80078c2:	f000 fed1 	bl	8008668 <xTaskRemoveFromEventList>
 80078c6:	4603      	mov	r3, r0
 80078c8:	2b00      	cmp	r3, #0
 80078ca:	d007      	beq.n	80078dc <xQueueSemaphoreTake+0xcc>
 80078cc:	4b53      	ldr	r3, [pc, #332]	; (8007a1c <xQueueSemaphoreTake+0x20c>)
 80078ce:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80078d2:	601a      	str	r2, [r3, #0]
 80078d4:	f3bf 8f4f 	dsb	sy
 80078d8:	f3bf 8f6f 	isb	sy
 80078dc:	f002 f8a4 	bl	8009a28 <vPortExitCritical>
 80078e0:	2301      	movs	r3, #1
 80078e2:	e096      	b.n	8007a12 <xQueueSemaphoreTake+0x202>
 80078e4:	683b      	ldr	r3, [r7, #0]
 80078e6:	2b00      	cmp	r3, #0
 80078e8:	d110      	bne.n	800790c <xQueueSemaphoreTake+0xfc>
 80078ea:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80078ec:	2b00      	cmp	r3, #0
 80078ee:	d009      	beq.n	8007904 <xQueueSemaphoreTake+0xf4>
 80078f0:	f04f 0350 	mov.w	r3, #80	; 0x50
 80078f4:	f383 8811 	msr	BASEPRI, r3
 80078f8:	f3bf 8f6f 	isb	sy
 80078fc:	f3bf 8f4f 	dsb	sy
 8007900:	617b      	str	r3, [r7, #20]
 8007902:	e7fe      	b.n	8007902 <xQueueSemaphoreTake+0xf2>
 8007904:	f002 f890 	bl	8009a28 <vPortExitCritical>
 8007908:	2300      	movs	r3, #0
 800790a:	e082      	b.n	8007a12 <xQueueSemaphoreTake+0x202>
 800790c:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800790e:	2b00      	cmp	r3, #0
 8007910:	d106      	bne.n	8007920 <xQueueSemaphoreTake+0x110>
 8007912:	f107 030c 	add.w	r3, r7, #12
 8007916:	4618      	mov	r0, r3
 8007918:	f000 ff08 	bl	800872c <vTaskInternalSetTimeOutState>
 800791c:	2301      	movs	r3, #1
 800791e:	637b      	str	r3, [r7, #52]	; 0x34
 8007920:	f002 f882 	bl	8009a28 <vPortExitCritical>
 8007924:	f000 fc40 	bl	80081a8 <vTaskSuspendAll>
 8007928:	f002 f850 	bl	80099cc <vPortEnterCritical>
 800792c:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800792e:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007932:	b25b      	sxtb	r3, r3
 8007934:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007938:	d103      	bne.n	8007942 <xQueueSemaphoreTake+0x132>
 800793a:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800793c:	2200      	movs	r2, #0
 800793e:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007942:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007944:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 8007948:	b25b      	sxtb	r3, r3
 800794a:	f1b3 3fff 	cmp.w	r3, #4294967295
 800794e:	d103      	bne.n	8007958 <xQueueSemaphoreTake+0x148>
 8007950:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007952:	2200      	movs	r2, #0
 8007954:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8007958:	f002 f866 	bl	8009a28 <vPortExitCritical>
 800795c:	463a      	mov	r2, r7
 800795e:	f107 030c 	add.w	r3, r7, #12
 8007962:	4611      	mov	r1, r2
 8007964:	4618      	mov	r0, r3
 8007966:	f000 fef7 	bl	8008758 <xTaskCheckForTimeOut>
 800796a:	4603      	mov	r3, r0
 800796c:	2b00      	cmp	r3, #0
 800796e:	d132      	bne.n	80079d6 <xQueueSemaphoreTake+0x1c6>
 8007970:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 8007972:	f000 f94f 	bl	8007c14 <prvIsQueueEmpty>
 8007976:	4603      	mov	r3, r0
 8007978:	2b00      	cmp	r3, #0
 800797a:	d026      	beq.n	80079ca <xQueueSemaphoreTake+0x1ba>
 800797c:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800797e:	681b      	ldr	r3, [r3, #0]
 8007980:	2b00      	cmp	r3, #0
 8007982:	d109      	bne.n	8007998 <xQueueSemaphoreTake+0x188>
 8007984:	f002 f822 	bl	80099cc <vPortEnterCritical>
 8007988:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800798a:	689b      	ldr	r3, [r3, #8]
 800798c:	4618      	mov	r0, r3
 800798e:	f001 f857 	bl	8008a40 <xTaskPriorityInherit>
 8007992:	6338      	str	r0, [r7, #48]	; 0x30
 8007994:	f002 f848 	bl	8009a28 <vPortExitCritical>
 8007998:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800799a:	3324      	adds	r3, #36	; 0x24
 800799c:	683a      	ldr	r2, [r7, #0]
 800799e:	4611      	mov	r1, r2
 80079a0:	4618      	mov	r0, r3
 80079a2:	f000 fe13 	bl	80085cc <vTaskPlaceOnEventList>
 80079a6:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 80079a8:	f000 f8e2 	bl	8007b70 <prvUnlockQueue>
 80079ac:	f000 fc0a 	bl	80081c4 <xTaskResumeAll>
 80079b0:	4603      	mov	r3, r0
 80079b2:	2b00      	cmp	r3, #0
 80079b4:	f47f af69 	bne.w	800788a <xQueueSemaphoreTake+0x7a>
 80079b8:	4b18      	ldr	r3, [pc, #96]	; (8007a1c <xQueueSemaphoreTake+0x20c>)
 80079ba:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80079be:	601a      	str	r2, [r3, #0]
 80079c0:	f3bf 8f4f 	dsb	sy
 80079c4:	f3bf 8f6f 	isb	sy
 80079c8:	e75f      	b.n	800788a <xQueueSemaphoreTake+0x7a>
 80079ca:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 80079cc:	f000 f8d0 	bl	8007b70 <prvUnlockQueue>
 80079d0:	f000 fbf8 	bl	80081c4 <xTaskResumeAll>
 80079d4:	e759      	b.n	800788a <xQueueSemaphoreTake+0x7a>
 80079d6:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 80079d8:	f000 f8ca 	bl	8007b70 <prvUnlockQueue>
 80079dc:	f000 fbf2 	bl	80081c4 <xTaskResumeAll>
 80079e0:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 80079e2:	f000 f917 	bl	8007c14 <prvIsQueueEmpty>
 80079e6:	4603      	mov	r3, r0
 80079e8:	2b00      	cmp	r3, #0
 80079ea:	f43f af4e 	beq.w	800788a <xQueueSemaphoreTake+0x7a>
 80079ee:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80079f0:	2b00      	cmp	r3, #0
 80079f2:	d00d      	beq.n	8007a10 <xQueueSemaphoreTake+0x200>
 80079f4:	f001 ffea 	bl	80099cc <vPortEnterCritical>
 80079f8:	6af8      	ldr	r0, [r7, #44]	; 0x2c
 80079fa:	f000 f811 	bl	8007a20 <prvGetDisinheritPriorityAfterTimeout>
 80079fe:	6278      	str	r0, [r7, #36]	; 0x24
 8007a00:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007a02:	689b      	ldr	r3, [r3, #8]
 8007a04:	6a79      	ldr	r1, [r7, #36]	; 0x24
 8007a06:	4618      	mov	r0, r3
 8007a08:	f001 f91e 	bl	8008c48 <vTaskPriorityDisinheritAfterTimeout>
 8007a0c:	f002 f80c 	bl	8009a28 <vPortExitCritical>
 8007a10:	2300      	movs	r3, #0
 8007a12:	4618      	mov	r0, r3
 8007a14:	3738      	adds	r7, #56	; 0x38
 8007a16:	46bd      	mov	sp, r7
 8007a18:	bd80      	pop	{r7, pc}
 8007a1a:	bf00      	nop
 8007a1c:	e000ed04 	.word	0xe000ed04

08007a20 <prvGetDisinheritPriorityAfterTimeout>:
 8007a20:	b480      	push	{r7}
 8007a22:	b085      	sub	sp, #20
 8007a24:	af00      	add	r7, sp, #0
 8007a26:	6078      	str	r0, [r7, #4]
 8007a28:	687b      	ldr	r3, [r7, #4]
 8007a2a:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8007a2c:	2b00      	cmp	r3, #0
 8007a2e:	d006      	beq.n	8007a3e <prvGetDisinheritPriorityAfterTimeout+0x1e>
 8007a30:	687b      	ldr	r3, [r7, #4]
 8007a32:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 8007a34:	681b      	ldr	r3, [r3, #0]
 8007a36:	f1c3 0305 	rsb	r3, r3, #5
 8007a3a:	60fb      	str	r3, [r7, #12]
 8007a3c:	e001      	b.n	8007a42 <prvGetDisinheritPriorityAfterTimeout+0x22>
 8007a3e:	2300      	movs	r3, #0
 8007a40:	60fb      	str	r3, [r7, #12]
 8007a42:	68fb      	ldr	r3, [r7, #12]
 8007a44:	4618      	mov	r0, r3
 8007a46:	3714      	adds	r7, #20
 8007a48:	46bd      	mov	sp, r7
 8007a4a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007a4e:	4770      	bx	lr

08007a50 <prvCopyDataToQueue>:
 8007a50:	b580      	push	{r7, lr}
 8007a52:	b086      	sub	sp, #24
 8007a54:	af00      	add	r7, sp, #0
 8007a56:	60f8      	str	r0, [r7, #12]
 8007a58:	60b9      	str	r1, [r7, #8]
 8007a5a:	607a      	str	r2, [r7, #4]
 8007a5c:	2300      	movs	r3, #0
 8007a5e:	617b      	str	r3, [r7, #20]
 8007a60:	68fb      	ldr	r3, [r7, #12]
 8007a62:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 8007a64:	613b      	str	r3, [r7, #16]
 8007a66:	68fb      	ldr	r3, [r7, #12]
 8007a68:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007a6a:	2b00      	cmp	r3, #0
 8007a6c:	d10d      	bne.n	8007a8a <prvCopyDataToQueue+0x3a>
 8007a6e:	68fb      	ldr	r3, [r7, #12]
 8007a70:	681b      	ldr	r3, [r3, #0]
 8007a72:	2b00      	cmp	r3, #0
 8007a74:	d14d      	bne.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007a76:	68fb      	ldr	r3, [r7, #12]
 8007a78:	689b      	ldr	r3, [r3, #8]
 8007a7a:	4618      	mov	r0, r3
 8007a7c:	f001 f860 	bl	8008b40 <xTaskPriorityDisinherit>
 8007a80:	6178      	str	r0, [r7, #20]
 8007a82:	68fb      	ldr	r3, [r7, #12]
 8007a84:	2200      	movs	r2, #0
 8007a86:	609a      	str	r2, [r3, #8]
 8007a88:	e043      	b.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007a8a:	687b      	ldr	r3, [r7, #4]
 8007a8c:	2b00      	cmp	r3, #0
 8007a8e:	d119      	bne.n	8007ac4 <prvCopyDataToQueue+0x74>
 8007a90:	68fb      	ldr	r3, [r7, #12]
 8007a92:	6858      	ldr	r0, [r3, #4]
 8007a94:	68fb      	ldr	r3, [r7, #12]
 8007a96:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007a98:	461a      	mov	r2, r3
 8007a9a:	68b9      	ldr	r1, [r7, #8]
 8007a9c:	f002 ff36 	bl	800a90c <memcpy>
 8007aa0:	68fb      	ldr	r3, [r7, #12]
 8007aa2:	685a      	ldr	r2, [r3, #4]
 8007aa4:	68fb      	ldr	r3, [r7, #12]
 8007aa6:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007aa8:	441a      	add	r2, r3
 8007aaa:	68fb      	ldr	r3, [r7, #12]
 8007aac:	605a      	str	r2, [r3, #4]
 8007aae:	68fb      	ldr	r3, [r7, #12]
 8007ab0:	685a      	ldr	r2, [r3, #4]
 8007ab2:	68fb      	ldr	r3, [r7, #12]
 8007ab4:	689b      	ldr	r3, [r3, #8]
 8007ab6:	429a      	cmp	r2, r3
 8007ab8:	d32b      	bcc.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007aba:	68fb      	ldr	r3, [r7, #12]
 8007abc:	681a      	ldr	r2, [r3, #0]
 8007abe:	68fb      	ldr	r3, [r7, #12]
 8007ac0:	605a      	str	r2, [r3, #4]
 8007ac2:	e026      	b.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007ac4:	68fb      	ldr	r3, [r7, #12]
 8007ac6:	68d8      	ldr	r0, [r3, #12]
 8007ac8:	68fb      	ldr	r3, [r7, #12]
 8007aca:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007acc:	461a      	mov	r2, r3
 8007ace:	68b9      	ldr	r1, [r7, #8]
 8007ad0:	f002 ff1c 	bl	800a90c <memcpy>
 8007ad4:	68fb      	ldr	r3, [r7, #12]
 8007ad6:	68da      	ldr	r2, [r3, #12]
 8007ad8:	68fb      	ldr	r3, [r7, #12]
 8007ada:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007adc:	425b      	negs	r3, r3
 8007ade:	441a      	add	r2, r3
 8007ae0:	68fb      	ldr	r3, [r7, #12]
 8007ae2:	60da      	str	r2, [r3, #12]
 8007ae4:	68fb      	ldr	r3, [r7, #12]
 8007ae6:	68da      	ldr	r2, [r3, #12]
 8007ae8:	68fb      	ldr	r3, [r7, #12]
 8007aea:	681b      	ldr	r3, [r3, #0]
 8007aec:	429a      	cmp	r2, r3
 8007aee:	d207      	bcs.n	8007b00 <prvCopyDataToQueue+0xb0>
 8007af0:	68fb      	ldr	r3, [r7, #12]
 8007af2:	689a      	ldr	r2, [r3, #8]
 8007af4:	68fb      	ldr	r3, [r7, #12]
 8007af6:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007af8:	425b      	negs	r3, r3
 8007afa:	441a      	add	r2, r3
 8007afc:	68fb      	ldr	r3, [r7, #12]
 8007afe:	60da      	str	r2, [r3, #12]
 8007b00:	687b      	ldr	r3, [r7, #4]
 8007b02:	2b02      	cmp	r3, #2
 8007b04:	d105      	bne.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007b06:	693b      	ldr	r3, [r7, #16]
 8007b08:	2b00      	cmp	r3, #0
 8007b0a:	d002      	beq.n	8007b12 <prvCopyDataToQueue+0xc2>
 8007b0c:	693b      	ldr	r3, [r7, #16]
 8007b0e:	3b01      	subs	r3, #1
 8007b10:	613b      	str	r3, [r7, #16]
 8007b12:	693b      	ldr	r3, [r7, #16]
 8007b14:	1c5a      	adds	r2, r3, #1
 8007b16:	68fb      	ldr	r3, [r7, #12]
 8007b18:	639a      	str	r2, [r3, #56]	; 0x38
 8007b1a:	697b      	ldr	r3, [r7, #20]
 8007b1c:	4618      	mov	r0, r3
 8007b1e:	3718      	adds	r7, #24
 8007b20:	46bd      	mov	sp, r7
 8007b22:	bd80      	pop	{r7, pc}

08007b24 <prvCopyDataFromQueue>:
 8007b24:	b580      	push	{r7, lr}
 8007b26:	b082      	sub	sp, #8
 8007b28:	af00      	add	r7, sp, #0
 8007b2a:	6078      	str	r0, [r7, #4]
 8007b2c:	6039      	str	r1, [r7, #0]
 8007b2e:	687b      	ldr	r3, [r7, #4]
 8007b30:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007b32:	2b00      	cmp	r3, #0
 8007b34:	d018      	beq.n	8007b68 <prvCopyDataFromQueue+0x44>
 8007b36:	687b      	ldr	r3, [r7, #4]
 8007b38:	68da      	ldr	r2, [r3, #12]
 8007b3a:	687b      	ldr	r3, [r7, #4]
 8007b3c:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007b3e:	441a      	add	r2, r3
 8007b40:	687b      	ldr	r3, [r7, #4]
 8007b42:	60da      	str	r2, [r3, #12]
 8007b44:	687b      	ldr	r3, [r7, #4]
 8007b46:	68da      	ldr	r2, [r3, #12]
 8007b48:	687b      	ldr	r3, [r7, #4]
 8007b4a:	689b      	ldr	r3, [r3, #8]
 8007b4c:	429a      	cmp	r2, r3
 8007b4e:	d303      	bcc.n	8007b58 <prvCopyDataFromQueue+0x34>
 8007b50:	687b      	ldr	r3, [r7, #4]
 8007b52:	681a      	ldr	r2, [r3, #0]
 8007b54:	687b      	ldr	r3, [r7, #4]
 8007b56:	60da      	str	r2, [r3, #12]
 8007b58:	687b      	ldr	r3, [r7, #4]
 8007b5a:	68d9      	ldr	r1, [r3, #12]
 8007b5c:	687b      	ldr	r3, [r7, #4]
 8007b5e:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007b60:	461a      	mov	r2, r3
 8007b62:	6838      	ldr	r0, [r7, #0]
 8007b64:	f002 fed2 	bl	800a90c <memcpy>
 8007b68:	bf00      	nop
 8007b6a:	3708      	adds	r7, #8
 8007b6c:	46bd      	mov	sp, r7
 8007b6e:	bd80      	pop	{r7, pc}

08007b70 <prvUnlockQueue>:
 8007b70:	b580      	push	{r7, lr}
 8007b72:	b084      	sub	sp, #16
 8007b74:	af00      	add	r7, sp, #0
 8007b76:	6078      	str	r0, [r7, #4]
 8007b78:	f001 ff28 	bl	80099cc <vPortEnterCritical>
 8007b7c:	687b      	ldr	r3, [r7, #4]
 8007b7e:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 8007b82:	73fb      	strb	r3, [r7, #15]
 8007b84:	e011      	b.n	8007baa <prvUnlockQueue+0x3a>
 8007b86:	687b      	ldr	r3, [r7, #4]
 8007b88:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8007b8a:	2b00      	cmp	r3, #0
 8007b8c:	d012      	beq.n	8007bb4 <prvUnlockQueue+0x44>
 8007b8e:	687b      	ldr	r3, [r7, #4]
 8007b90:	3324      	adds	r3, #36	; 0x24
 8007b92:	4618      	mov	r0, r3
 8007b94:	f000 fd68 	bl	8008668 <xTaskRemoveFromEventList>
 8007b98:	4603      	mov	r3, r0
 8007b9a:	2b00      	cmp	r3, #0
 8007b9c:	d001      	beq.n	8007ba2 <prvUnlockQueue+0x32>
 8007b9e:	f000 fe3b 	bl	8008818 <vTaskMissedYield>
 8007ba2:	7bfb      	ldrb	r3, [r7, #15]
 8007ba4:	3b01      	subs	r3, #1
 8007ba6:	b2db      	uxtb	r3, r3
 8007ba8:	73fb      	strb	r3, [r7, #15]
 8007baa:	f997 300f 	ldrsb.w	r3, [r7, #15]
 8007bae:	2b00      	cmp	r3, #0
 8007bb0:	dce9      	bgt.n	8007b86 <prvUnlockQueue+0x16>
 8007bb2:	e000      	b.n	8007bb6 <prvUnlockQueue+0x46>
 8007bb4:	bf00      	nop
 8007bb6:	687b      	ldr	r3, [r7, #4]
 8007bb8:	22ff      	movs	r2, #255	; 0xff
 8007bba:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8007bbe:	f001 ff33 	bl	8009a28 <vPortExitCritical>
 8007bc2:	f001 ff03 	bl	80099cc <vPortEnterCritical>
 8007bc6:	687b      	ldr	r3, [r7, #4]
 8007bc8:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007bcc:	73bb      	strb	r3, [r7, #14]
 8007bce:	e011      	b.n	8007bf4 <prvUnlockQueue+0x84>
 8007bd0:	687b      	ldr	r3, [r7, #4]
 8007bd2:	691b      	ldr	r3, [r3, #16]
 8007bd4:	2b00      	cmp	r3, #0
 8007bd6:	d012      	beq.n	8007bfe <prvUnlockQueue+0x8e>
 8007bd8:	687b      	ldr	r3, [r7, #4]
 8007bda:	3310      	adds	r3, #16
 8007bdc:	4618      	mov	r0, r3
 8007bde:	f000 fd43 	bl	8008668 <xTaskRemoveFromEventList>
 8007be2:	4603      	mov	r3, r0
 8007be4:	2b00      	cmp	r3, #0
 8007be6:	d001      	beq.n	8007bec <prvUnlockQueue+0x7c>
 8007be8:	f000 fe16 	bl	8008818 <vTaskMissedYield>
 8007bec:	7bbb      	ldrb	r3, [r7, #14]
 8007bee:	3b01      	subs	r3, #1
 8007bf0:	b2db      	uxtb	r3, r3
 8007bf2:	73bb      	strb	r3, [r7, #14]
 8007bf4:	f997 300e 	ldrsb.w	r3, [r7, #14]
 8007bf8:	2b00      	cmp	r3, #0
 8007bfa:	dce9      	bgt.n	8007bd0 <prvUnlockQueue+0x60>
 8007bfc:	e000      	b.n	8007c00 <prvUnlockQueue+0x90>
 8007bfe:	bf00      	nop
 8007c00:	687b      	ldr	r3, [r7, #4]
 8007c02:	22ff      	movs	r2, #255	; 0xff
 8007c04:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007c08:	f001 ff0e 	bl	8009a28 <vPortExitCritical>
 8007c0c:	bf00      	nop
 8007c0e:	3710      	adds	r7, #16
 8007c10:	46bd      	mov	sp, r7
 8007c12:	bd80      	pop	{r7, pc}

08007c14 <prvIsQueueEmpty>:
 8007c14:	b580      	push	{r7, lr}
 8007c16:	b084      	sub	sp, #16
 8007c18:	af00      	add	r7, sp, #0
 8007c1a:	6078      	str	r0, [r7, #4]
 8007c1c:	f001 fed6 	bl	80099cc <vPortEnterCritical>
 8007c20:	687b      	ldr	r3, [r7, #4]
 8007c22:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 8007c24:	2b00      	cmp	r3, #0
 8007c26:	d102      	bne.n	8007c2e <prvIsQueueEmpty+0x1a>
 8007c28:	2301      	movs	r3, #1
 8007c2a:	60fb      	str	r3, [r7, #12]
 8007c2c:	e001      	b.n	8007c32 <prvIsQueueEmpty+0x1e>
 8007c2e:	2300      	movs	r3, #0
 8007c30:	60fb      	str	r3, [r7, #12]
 8007c32:	f001 fef9 	bl	8009a28 <vPortExitCritical>
 8007c36:	68fb      	ldr	r3, [r7, #12]
 8007c38:	4618      	mov	r0, r3
 8007c3a:	3710      	adds	r7, #16
 8007c3c:	46bd      	mov	sp, r7
 8007c3e:	bd80      	pop	{r7, pc}

08007c40 <prvIsQueueFull>:
 8007c40:	b580      	push	{r7, lr}
 8007c42:	b084      	sub	sp, #16
 8007c44:	af00      	add	r7, sp, #0
 8007c46:	6078      	str	r0, [r7, #4]
 8007c48:	f001 fec0 	bl	80099cc <vPortEnterCritical>
 8007c4c:	687b      	ldr	r3, [r7, #4]
 8007c4e:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 8007c50:	687b      	ldr	r3, [r7, #4]
 8007c52:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8007c54:	429a      	cmp	r2, r3
 8007c56:	d102      	bne.n	8007c5e <prvIsQueueFull+0x1e>
 8007c58:	2301      	movs	r3, #1
 8007c5a:	60fb      	str	r3, [r7, #12]
 8007c5c:	e001      	b.n	8007c62 <prvIsQueueFull+0x22>
 8007c5e:	2300      	movs	r3, #0
 8007c60:	60fb      	str	r3, [r7, #12]
 8007c62:	f001 fee1 	bl	8009a28 <vPortExitCritical>
 8007c66:	68fb      	ldr	r3, [r7, #12]
 8007c68:	4618      	mov	r0, r3
 8007c6a:	3710      	adds	r7, #16
 8007c6c:	46bd      	mov	sp, r7
 8007c6e:	bd80      	pop	{r7, pc}

08007c70 <vQueueAddToRegistry>:
 8007c70:	b480      	push	{r7}
 8007c72:	b085      	sub	sp, #20
 8007c74:	af00      	add	r7, sp, #0
 8007c76:	6078      	str	r0, [r7, #4]
 8007c78:	6039      	str	r1, [r7, #0]
 8007c7a:	2300      	movs	r3, #0
 8007c7c:	60fb      	str	r3, [r7, #12]
 8007c7e:	e014      	b.n	8007caa <vQueueAddToRegistry+0x3a>
 8007c80:	4a0e      	ldr	r2, [pc, #56]	; (8007cbc <vQueueAddToRegistry+0x4c>)
 8007c82:	68fb      	ldr	r3, [r7, #12]
 8007c84:	f852 3033 	ldr.w	r3, [r2, r3, lsl #3]
 8007c88:	2b00      	cmp	r3, #0
 8007c8a:	d10b      	bne.n	8007ca4 <vQueueAddToRegistry+0x34>
 8007c8c:	490b      	ldr	r1, [pc, #44]	; (8007cbc <vQueueAddToRegistry+0x4c>)
 8007c8e:	68fb      	ldr	r3, [r7, #12]
 8007c90:	683a      	ldr	r2, [r7, #0]
 8007c92:	f841 2033 	str.w	r2, [r1, r3, lsl #3]
 8007c96:	4a09      	ldr	r2, [pc, #36]	; (8007cbc <vQueueAddToRegistry+0x4c>)
 8007c98:	68fb      	ldr	r3, [r7, #12]
 8007c9a:	00db      	lsls	r3, r3, #3
 8007c9c:	4413      	add	r3, r2
 8007c9e:	687a      	ldr	r2, [r7, #4]
 8007ca0:	605a      	str	r2, [r3, #4]
 8007ca2:	e005      	b.n	8007cb0 <vQueueAddToRegistry+0x40>
 8007ca4:	68fb      	ldr	r3, [r7, #12]
 8007ca6:	3301      	adds	r3, #1
 8007ca8:	60fb      	str	r3, [r7, #12]
 8007caa:	68fb      	ldr	r3, [r7, #12]
 8007cac:	2b07      	cmp	r3, #7
 8007cae:	d9e7      	bls.n	8007c80 <vQueueAddToRegistry+0x10>
 8007cb0:	bf00      	nop
 8007cb2:	3714      	adds	r7, #20
 8007cb4:	46bd      	mov	sp, r7
 8007cb6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007cba:	4770      	bx	lr
 8007cbc:	20019308 	.word	0x20019308

08007cc0 <vQueueWaitForMessageRestricted>:
 8007cc0:	b580      	push	{r7, lr}
 8007cc2:	b086      	sub	sp, #24
 8007cc4:	af00      	add	r7, sp, #0
 8007cc6:	60f8      	str	r0, [r7, #12]
 8007cc8:	60b9      	str	r1, [r7, #8]
 8007cca:	607a      	str	r2, [r7, #4]
 8007ccc:	68fb      	ldr	r3, [r7, #12]
 8007cce:	617b      	str	r3, [r7, #20]
 8007cd0:	f001 fe7c 	bl	80099cc <vPortEnterCritical>
 8007cd4:	697b      	ldr	r3, [r7, #20]
 8007cd6:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007cda:	b25b      	sxtb	r3, r3
 8007cdc:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007ce0:	d103      	bne.n	8007cea <vQueueWaitForMessageRestricted+0x2a>
 8007ce2:	697b      	ldr	r3, [r7, #20]
 8007ce4:	2200      	movs	r2, #0
 8007ce6:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007cea:	697b      	ldr	r3, [r7, #20]
 8007cec:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 8007cf0:	b25b      	sxtb	r3, r3
 8007cf2:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007cf6:	d103      	bne.n	8007d00 <vQueueWaitForMessageRestricted+0x40>
 8007cf8:	697b      	ldr	r3, [r7, #20]
 8007cfa:	2200      	movs	r2, #0
 8007cfc:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8007d00:	f001 fe92 	bl	8009a28 <vPortExitCritical>
 8007d04:	697b      	ldr	r3, [r7, #20]
 8007d06:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 8007d08:	2b00      	cmp	r3, #0
 8007d0a:	d106      	bne.n	8007d1a <vQueueWaitForMessageRestricted+0x5a>
 8007d0c:	697b      	ldr	r3, [r7, #20]
 8007d0e:	3324      	adds	r3, #36	; 0x24
 8007d10:	687a      	ldr	r2, [r7, #4]
 8007d12:	68b9      	ldr	r1, [r7, #8]
 8007d14:	4618      	mov	r0, r3
 8007d16:	f000 fc7d 	bl	8008614 <vTaskPlaceOnEventListRestricted>
 8007d1a:	6978      	ldr	r0, [r7, #20]
 8007d1c:	f7ff ff28 	bl	8007b70 <prvUnlockQueue>
 8007d20:	bf00      	nop
 8007d22:	3718      	adds	r7, #24
 8007d24:	46bd      	mov	sp, r7
 8007d26:	bd80      	pop	{r7, pc}

08007d28 <xTaskCreateStatic>:
 8007d28:	b580      	push	{r7, lr}
 8007d2a:	b08e      	sub	sp, #56	; 0x38
 8007d2c:	af04      	add	r7, sp, #16
 8007d2e:	60f8      	str	r0, [r7, #12]
 8007d30:	60b9      	str	r1, [r7, #8]
 8007d32:	607a      	str	r2, [r7, #4]
 8007d34:	603b      	str	r3, [r7, #0]
 8007d36:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007d38:	2b00      	cmp	r3, #0
 8007d3a:	d109      	bne.n	8007d50 <xTaskCreateStatic+0x28>
 8007d3c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007d40:	f383 8811 	msr	BASEPRI, r3
 8007d44:	f3bf 8f6f 	isb	sy
 8007d48:	f3bf 8f4f 	dsb	sy
 8007d4c:	623b      	str	r3, [r7, #32]
 8007d4e:	e7fe      	b.n	8007d4e <xTaskCreateStatic+0x26>
 8007d50:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007d52:	2b00      	cmp	r3, #0
 8007d54:	d109      	bne.n	8007d6a <xTaskCreateStatic+0x42>
 8007d56:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007d5a:	f383 8811 	msr	BASEPRI, r3
 8007d5e:	f3bf 8f6f 	isb	sy
 8007d62:	f3bf 8f4f 	dsb	sy
 8007d66:	61fb      	str	r3, [r7, #28]
 8007d68:	e7fe      	b.n	8007d68 <xTaskCreateStatic+0x40>
 8007d6a:	f44f 6390 	mov.w	r3, #1152	; 0x480
 8007d6e:	613b      	str	r3, [r7, #16]
 8007d70:	693b      	ldr	r3, [r7, #16]
 8007d72:	f5b3 6f90 	cmp.w	r3, #1152	; 0x480
 8007d76:	d009      	beq.n	8007d8c <xTaskCreateStatic+0x64>
 8007d78:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007d7c:	f383 8811 	msr	BASEPRI, r3
 8007d80:	f3bf 8f6f 	isb	sy
 8007d84:	f3bf 8f4f 	dsb	sy
 8007d88:	61bb      	str	r3, [r7, #24]
 8007d8a:	e7fe      	b.n	8007d8a <xTaskCreateStatic+0x62>
 8007d8c:	693b      	ldr	r3, [r7, #16]
 8007d8e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007d90:	2b00      	cmp	r3, #0
 8007d92:	d01e      	beq.n	8007dd2 <xTaskCreateStatic+0xaa>
 8007d94:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007d96:	2b00      	cmp	r3, #0
 8007d98:	d01b      	beq.n	8007dd2 <xTaskCreateStatic+0xaa>
 8007d9a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007d9c:	627b      	str	r3, [r7, #36]	; 0x24
 8007d9e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8007da0:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8007da2:	631a      	str	r2, [r3, #48]	; 0x30
 8007da4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8007da6:	2202      	movs	r2, #2
 8007da8:	f883 247d 	strb.w	r2, [r3, #1149]	; 0x47d
 8007dac:	2300      	movs	r3, #0
 8007dae:	9303      	str	r3, [sp, #12]
 8007db0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8007db2:	9302      	str	r3, [sp, #8]
 8007db4:	f107 0314 	add.w	r3, r7, #20
 8007db8:	9301      	str	r3, [sp, #4]
 8007dba:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007dbc:	9300      	str	r3, [sp, #0]
 8007dbe:	683b      	ldr	r3, [r7, #0]
 8007dc0:	687a      	ldr	r2, [r7, #4]
 8007dc2:	68b9      	ldr	r1, [r7, #8]
 8007dc4:	68f8      	ldr	r0, [r7, #12]
 8007dc6:	f000 f80b 	bl	8007de0 <prvInitialiseNewTask>
 8007dca:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8007dcc:	f000 f8dc 	bl	8007f88 <prvAddNewTaskToReadyList>
 8007dd0:	e001      	b.n	8007dd6 <xTaskCreateStatic+0xae>
 8007dd2:	2300      	movs	r3, #0
 8007dd4:	617b      	str	r3, [r7, #20]
 8007dd6:	697b      	ldr	r3, [r7, #20]
 8007dd8:	4618      	mov	r0, r3
 8007dda:	3728      	adds	r7, #40	; 0x28
 8007ddc:	46bd      	mov	sp, r7
 8007dde:	bd80      	pop	{r7, pc}

08007de0 <prvInitialiseNewTask>:
 8007de0:	b590      	push	{r4, r7, lr}
 8007de2:	b089      	sub	sp, #36	; 0x24
 8007de4:	af00      	add	r7, sp, #0
 8007de6:	60f8      	str	r0, [r7, #12]
 8007de8:	60b9      	str	r1, [r7, #8]
 8007dea:	607a      	str	r2, [r7, #4]
 8007dec:	603b      	str	r3, [r7, #0]
 8007dee:	68bb      	ldr	r3, [r7, #8]
 8007df0:	2b00      	cmp	r3, #0
 8007df2:	d109      	bne.n	8007e08 <prvInitialiseNewTask+0x28>
 8007df4:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007df8:	f383 8811 	msr	BASEPRI, r3
 8007dfc:	f3bf 8f6f 	isb	sy
 8007e00:	f3bf 8f4f 	dsb	sy
 8007e04:	617b      	str	r3, [r7, #20]
 8007e06:	e7fe      	b.n	8007e06 <prvInitialiseNewTask+0x26>
 8007e08:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007e0a:	6b18      	ldr	r0, [r3, #48]	; 0x30
 8007e0c:	687b      	ldr	r3, [r7, #4]
 8007e0e:	009b      	lsls	r3, r3, #2
 8007e10:	461a      	mov	r2, r3
 8007e12:	21a5      	movs	r1, #165	; 0xa5
 8007e14:	f002 fd88 	bl	800a928 <memset>
 8007e18:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007e1a:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8007e1c:	687b      	ldr	r3, [r7, #4]
 8007e1e:	f103 4380 	add.w	r3, r3, #1073741824	; 0x40000000
 8007e22:	3b01      	subs	r3, #1
 8007e24:	009b      	lsls	r3, r3, #2
 8007e26:	4413      	add	r3, r2
 8007e28:	61bb      	str	r3, [r7, #24]
 8007e2a:	69bb      	ldr	r3, [r7, #24]
 8007e2c:	f023 0307 	bic.w	r3, r3, #7
 8007e30:	61bb      	str	r3, [r7, #24]
 8007e32:	69bb      	ldr	r3, [r7, #24]
 8007e34:	f003 0307 	and.w	r3, r3, #7
 8007e38:	2b00      	cmp	r3, #0
 8007e3a:	d009      	beq.n	8007e50 <prvInitialiseNewTask+0x70>
 8007e3c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007e40:	f383 8811 	msr	BASEPRI, r3
 8007e44:	f3bf 8f6f 	isb	sy
 8007e48:	f3bf 8f4f 	dsb	sy
 8007e4c:	613b      	str	r3, [r7, #16]
 8007e4e:	e7fe      	b.n	8007e4e <prvInitialiseNewTask+0x6e>
 8007e50:	2300      	movs	r3, #0
 8007e52:	61fb      	str	r3, [r7, #28]
 8007e54:	e012      	b.n	8007e7c <prvInitialiseNewTask+0x9c>
 8007e56:	68ba      	ldr	r2, [r7, #8]
 8007e58:	69fb      	ldr	r3, [r7, #28]
 8007e5a:	4413      	add	r3, r2
 8007e5c:	7819      	ldrb	r1, [r3, #0]
 8007e5e:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007e60:	69fb      	ldr	r3, [r7, #28]
 8007e62:	4413      	add	r3, r2
 8007e64:	3334      	adds	r3, #52	; 0x34
 8007e66:	460a      	mov	r2, r1
 8007e68:	701a      	strb	r2, [r3, #0]
 8007e6a:	68ba      	ldr	r2, [r7, #8]
 8007e6c:	69fb      	ldr	r3, [r7, #28]
 8007e6e:	4413      	add	r3, r2
 8007e70:	781b      	ldrb	r3, [r3, #0]
 8007e72:	2b00      	cmp	r3, #0
 8007e74:	d006      	beq.n	8007e84 <prvInitialiseNewTask+0xa4>
 8007e76:	69fb      	ldr	r3, [r7, #28]
 8007e78:	3301      	adds	r3, #1
 8007e7a:	61fb      	str	r3, [r7, #28]
 8007e7c:	69fb      	ldr	r3, [r7, #28]
 8007e7e:	2b09      	cmp	r3, #9
 8007e80:	d9e9      	bls.n	8007e56 <prvInitialiseNewTask+0x76>
 8007e82:	e000      	b.n	8007e86 <prvInitialiseNewTask+0xa6>
 8007e84:	bf00      	nop
 8007e86:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007e88:	2200      	movs	r2, #0
 8007e8a:	f883 203d 	strb.w	r2, [r3, #61]	; 0x3d
 8007e8e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007e90:	2b04      	cmp	r3, #4
 8007e92:	d901      	bls.n	8007e98 <prvInitialiseNewTask+0xb8>
 8007e94:	2304      	movs	r3, #4
 8007e96:	633b      	str	r3, [r7, #48]	; 0x30
 8007e98:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007e9a:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8007e9c:	62da      	str	r2, [r3, #44]	; 0x2c
 8007e9e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ea0:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8007ea2:	649a      	str	r2, [r3, #72]	; 0x48
 8007ea4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ea6:	2200      	movs	r2, #0
 8007ea8:	64da      	str	r2, [r3, #76]	; 0x4c
 8007eaa:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007eac:	3304      	adds	r3, #4
 8007eae:	4618      	mov	r0, r3
 8007eb0:	f7ff f87b 	bl	8006faa <vListInitialiseItem>
 8007eb4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007eb6:	3318      	adds	r3, #24
 8007eb8:	4618      	mov	r0, r3
 8007eba:	f7ff f876 	bl	8006faa <vListInitialiseItem>
 8007ebe:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ec0:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007ec2:	611a      	str	r2, [r3, #16]
 8007ec4:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007ec6:	f1c3 0205 	rsb	r2, r3, #5
 8007eca:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ecc:	619a      	str	r2, [r3, #24]
 8007ece:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ed0:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007ed2:	625a      	str	r2, [r3, #36]	; 0x24
 8007ed4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ed6:	2200      	movs	r2, #0
 8007ed8:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8007edc:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ede:	2200      	movs	r2, #0
 8007ee0:	f883 247c 	strb.w	r2, [r3, #1148]	; 0x47c
 8007ee4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ee6:	3350      	adds	r3, #80	; 0x50
 8007ee8:	f44f 6285 	mov.w	r2, #1064	; 0x428
 8007eec:	2100      	movs	r1, #0
 8007eee:	4618      	mov	r0, r3
 8007ef0:	f002 fd1a 	bl	800a928 <memset>
 8007ef4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007ef6:	f503 724f 	add.w	r2, r3, #828	; 0x33c
 8007efa:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007efc:	655a      	str	r2, [r3, #84]	; 0x54
 8007efe:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f00:	f503 7269 	add.w	r2, r3, #932	; 0x3a4
 8007f04:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f06:	659a      	str	r2, [r3, #88]	; 0x58
 8007f08:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f0a:	f203 420c 	addw	r2, r3, #1036	; 0x40c
 8007f0e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f10:	65da      	str	r2, [r3, #92]	; 0x5c
 8007f12:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007f14:	f04f 0301 	mov.w	r3, #1
 8007f18:	f04f 0400 	mov.w	r4, #0
 8007f1c:	e9c2 343e 	strd	r3, r4, [r2, #248]	; 0xf8
 8007f20:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f22:	f243 320e 	movw	r2, #13070	; 0x330e
 8007f26:	f8a3 2100 	strh.w	r2, [r3, #256]	; 0x100
 8007f2a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f2c:	f64a 32cd 	movw	r2, #43981	; 0xabcd
 8007f30:	f8a3 2102 	strh.w	r2, [r3, #258]	; 0x102
 8007f34:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f36:	f241 2234 	movw	r2, #4660	; 0x1234
 8007f3a:	f8a3 2104 	strh.w	r2, [r3, #260]	; 0x104
 8007f3e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f40:	f24e 626d 	movw	r2, #58989	; 0xe66d
 8007f44:	f8a3 2106 	strh.w	r2, [r3, #262]	; 0x106
 8007f48:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f4a:	f64d 62ec 	movw	r2, #57068	; 0xdeec
 8007f4e:	f8a3 2108 	strh.w	r2, [r3, #264]	; 0x108
 8007f52:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f54:	2205      	movs	r2, #5
 8007f56:	f8a3 210a 	strh.w	r2, [r3, #266]	; 0x10a
 8007f5a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f5c:	220b      	movs	r2, #11
 8007f5e:	f8a3 210c 	strh.w	r2, [r3, #268]	; 0x10c
 8007f62:	683a      	ldr	r2, [r7, #0]
 8007f64:	68f9      	ldr	r1, [r7, #12]
 8007f66:	69b8      	ldr	r0, [r7, #24]
 8007f68:	f001 fc08 	bl	800977c <pxPortInitialiseStack>
 8007f6c:	4602      	mov	r2, r0
 8007f6e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007f70:	601a      	str	r2, [r3, #0]
 8007f72:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007f74:	2b00      	cmp	r3, #0
 8007f76:	d002      	beq.n	8007f7e <prvInitialiseNewTask+0x19e>
 8007f78:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007f7a:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007f7c:	601a      	str	r2, [r3, #0]
 8007f7e:	bf00      	nop
 8007f80:	3724      	adds	r7, #36	; 0x24
 8007f82:	46bd      	mov	sp, r7
 8007f84:	bd90      	pop	{r4, r7, pc}
	...

08007f88 <prvAddNewTaskToReadyList>:
 8007f88:	b580      	push	{r7, lr}
 8007f8a:	b082      	sub	sp, #8
 8007f8c:	af00      	add	r7, sp, #0
 8007f8e:	6078      	str	r0, [r7, #4]
 8007f90:	f001 fd1c 	bl	80099cc <vPortEnterCritical>
 8007f94:	4b2c      	ldr	r3, [pc, #176]	; (8008048 <prvAddNewTaskToReadyList+0xc0>)
 8007f96:	681b      	ldr	r3, [r3, #0]
 8007f98:	3301      	adds	r3, #1
 8007f9a:	4a2b      	ldr	r2, [pc, #172]	; (8008048 <prvAddNewTaskToReadyList+0xc0>)
 8007f9c:	6013      	str	r3, [r2, #0]
 8007f9e:	4b2b      	ldr	r3, [pc, #172]	; (800804c <prvAddNewTaskToReadyList+0xc4>)
 8007fa0:	681b      	ldr	r3, [r3, #0]
 8007fa2:	2b00      	cmp	r3, #0
 8007fa4:	d109      	bne.n	8007fba <prvAddNewTaskToReadyList+0x32>
 8007fa6:	4a29      	ldr	r2, [pc, #164]	; (800804c <prvAddNewTaskToReadyList+0xc4>)
 8007fa8:	687b      	ldr	r3, [r7, #4]
 8007faa:	6013      	str	r3, [r2, #0]
 8007fac:	4b26      	ldr	r3, [pc, #152]	; (8008048 <prvAddNewTaskToReadyList+0xc0>)
 8007fae:	681b      	ldr	r3, [r3, #0]
 8007fb0:	2b01      	cmp	r3, #1
 8007fb2:	d110      	bne.n	8007fd6 <prvAddNewTaskToReadyList+0x4e>
 8007fb4:	f000 fc56 	bl	8008864 <prvInitialiseTaskLists>
 8007fb8:	e00d      	b.n	8007fd6 <prvAddNewTaskToReadyList+0x4e>
 8007fba:	4b25      	ldr	r3, [pc, #148]	; (8008050 <prvAddNewTaskToReadyList+0xc8>)
 8007fbc:	681b      	ldr	r3, [r3, #0]
 8007fbe:	2b00      	cmp	r3, #0
 8007fc0:	d109      	bne.n	8007fd6 <prvAddNewTaskToReadyList+0x4e>
 8007fc2:	4b22      	ldr	r3, [pc, #136]	; (800804c <prvAddNewTaskToReadyList+0xc4>)
 8007fc4:	681b      	ldr	r3, [r3, #0]
 8007fc6:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007fc8:	687b      	ldr	r3, [r7, #4]
 8007fca:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007fcc:	429a      	cmp	r2, r3
 8007fce:	d802      	bhi.n	8007fd6 <prvAddNewTaskToReadyList+0x4e>
 8007fd0:	4a1e      	ldr	r2, [pc, #120]	; (800804c <prvAddNewTaskToReadyList+0xc4>)
 8007fd2:	687b      	ldr	r3, [r7, #4]
 8007fd4:	6013      	str	r3, [r2, #0]
 8007fd6:	4b1f      	ldr	r3, [pc, #124]	; (8008054 <prvAddNewTaskToReadyList+0xcc>)
 8007fd8:	681b      	ldr	r3, [r3, #0]
 8007fda:	3301      	adds	r3, #1
 8007fdc:	4a1d      	ldr	r2, [pc, #116]	; (8008054 <prvAddNewTaskToReadyList+0xcc>)
 8007fde:	6013      	str	r3, [r2, #0]
 8007fe0:	4b1c      	ldr	r3, [pc, #112]	; (8008054 <prvAddNewTaskToReadyList+0xcc>)
 8007fe2:	681a      	ldr	r2, [r3, #0]
 8007fe4:	687b      	ldr	r3, [r7, #4]
 8007fe6:	641a      	str	r2, [r3, #64]	; 0x40
 8007fe8:	687b      	ldr	r3, [r7, #4]
 8007fea:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007fec:	2201      	movs	r2, #1
 8007fee:	409a      	lsls	r2, r3
 8007ff0:	4b19      	ldr	r3, [pc, #100]	; (8008058 <prvAddNewTaskToReadyList+0xd0>)
 8007ff2:	681b      	ldr	r3, [r3, #0]
 8007ff4:	4313      	orrs	r3, r2
 8007ff6:	4a18      	ldr	r2, [pc, #96]	; (8008058 <prvAddNewTaskToReadyList+0xd0>)
 8007ff8:	6013      	str	r3, [r2, #0]
 8007ffa:	687b      	ldr	r3, [r7, #4]
 8007ffc:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007ffe:	4613      	mov	r3, r2
 8008000:	009b      	lsls	r3, r3, #2
 8008002:	4413      	add	r3, r2
 8008004:	009b      	lsls	r3, r3, #2
 8008006:	4a15      	ldr	r2, [pc, #84]	; (800805c <prvAddNewTaskToReadyList+0xd4>)
 8008008:	441a      	add	r2, r3
 800800a:	687b      	ldr	r3, [r7, #4]
 800800c:	3304      	adds	r3, #4
 800800e:	4619      	mov	r1, r3
 8008010:	4610      	mov	r0, r2
 8008012:	f7fe ffd7 	bl	8006fc4 <vListInsertEnd>
 8008016:	f001 fd07 	bl	8009a28 <vPortExitCritical>
 800801a:	4b0d      	ldr	r3, [pc, #52]	; (8008050 <prvAddNewTaskToReadyList+0xc8>)
 800801c:	681b      	ldr	r3, [r3, #0]
 800801e:	2b00      	cmp	r3, #0
 8008020:	d00e      	beq.n	8008040 <prvAddNewTaskToReadyList+0xb8>
 8008022:	4b0a      	ldr	r3, [pc, #40]	; (800804c <prvAddNewTaskToReadyList+0xc4>)
 8008024:	681b      	ldr	r3, [r3, #0]
 8008026:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008028:	687b      	ldr	r3, [r7, #4]
 800802a:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 800802c:	429a      	cmp	r2, r3
 800802e:	d207      	bcs.n	8008040 <prvAddNewTaskToReadyList+0xb8>
 8008030:	4b0b      	ldr	r3, [pc, #44]	; (8008060 <prvAddNewTaskToReadyList+0xd8>)
 8008032:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8008036:	601a      	str	r2, [r3, #0]
 8008038:	f3bf 8f4f 	dsb	sy
 800803c:	f3bf 8f6f 	isb	sy
 8008040:	bf00      	nop
 8008042:	3708      	adds	r7, #8
 8008044:	46bd      	mov	sp, r7
 8008046:	bd80      	pop	{r7, pc}
 8008048:	20000cd0 	.word	0x20000cd0
 800804c:	20000bf8 	.word	0x20000bf8
 8008050:	20000cdc 	.word	0x20000cdc
 8008054:	20000cec 	.word	0x20000cec
 8008058:	20000cd8 	.word	0x20000cd8
 800805c:	20000bfc 	.word	0x20000bfc
 8008060:	e000ed04 	.word	0xe000ed04

08008064 <vTaskDelay>:
 8008064:	b580      	push	{r7, lr}
 8008066:	b084      	sub	sp, #16
 8008068:	af00      	add	r7, sp, #0
 800806a:	6078      	str	r0, [r7, #4]
 800806c:	2300      	movs	r3, #0
 800806e:	60fb      	str	r3, [r7, #12]
 8008070:	687b      	ldr	r3, [r7, #4]
 8008072:	2b00      	cmp	r3, #0
 8008074:	d016      	beq.n	80080a4 <vTaskDelay+0x40>
 8008076:	4b13      	ldr	r3, [pc, #76]	; (80080c4 <vTaskDelay+0x60>)
 8008078:	681b      	ldr	r3, [r3, #0]
 800807a:	2b00      	cmp	r3, #0
 800807c:	d009      	beq.n	8008092 <vTaskDelay+0x2e>
 800807e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008082:	f383 8811 	msr	BASEPRI, r3
 8008086:	f3bf 8f6f 	isb	sy
 800808a:	f3bf 8f4f 	dsb	sy
 800808e:	60bb      	str	r3, [r7, #8]
 8008090:	e7fe      	b.n	8008090 <vTaskDelay+0x2c>
 8008092:	f000 f889 	bl	80081a8 <vTaskSuspendAll>
 8008096:	2100      	movs	r1, #0
 8008098:	6878      	ldr	r0, [r7, #4]
 800809a:	f001 f819 	bl	80090d0 <prvAddCurrentTaskToDelayedList>
 800809e:	f000 f891 	bl	80081c4 <xTaskResumeAll>
 80080a2:	60f8      	str	r0, [r7, #12]
 80080a4:	68fb      	ldr	r3, [r7, #12]
 80080a6:	2b00      	cmp	r3, #0
 80080a8:	d107      	bne.n	80080ba <vTaskDelay+0x56>
 80080aa:	4b07      	ldr	r3, [pc, #28]	; (80080c8 <vTaskDelay+0x64>)
 80080ac:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80080b0:	601a      	str	r2, [r3, #0]
 80080b2:	f3bf 8f4f 	dsb	sy
 80080b6:	f3bf 8f6f 	isb	sy
 80080ba:	bf00      	nop
 80080bc:	3710      	adds	r7, #16
 80080be:	46bd      	mov	sp, r7
 80080c0:	bd80      	pop	{r7, pc}
 80080c2:	bf00      	nop
 80080c4:	20000cf8 	.word	0x20000cf8
 80080c8:	e000ed04 	.word	0xe000ed04

080080cc <vTaskStartScheduler>:
 80080cc:	b580      	push	{r7, lr}
 80080ce:	b08a      	sub	sp, #40	; 0x28
 80080d0:	af04      	add	r7, sp, #16
 80080d2:	2300      	movs	r3, #0
 80080d4:	60bb      	str	r3, [r7, #8]
 80080d6:	2300      	movs	r3, #0
 80080d8:	607b      	str	r3, [r7, #4]
 80080da:	463a      	mov	r2, r7
 80080dc:	1d39      	adds	r1, r7, #4
 80080de:	f107 0308 	add.w	r3, r7, #8
 80080e2:	4618      	mov	r0, r3
 80080e4:	f7fc fbe8 	bl	80048b8 <vApplicationGetIdleTaskMemory>
 80080e8:	6839      	ldr	r1, [r7, #0]
 80080ea:	687b      	ldr	r3, [r7, #4]
 80080ec:	68ba      	ldr	r2, [r7, #8]
 80080ee:	9202      	str	r2, [sp, #8]
 80080f0:	9301      	str	r3, [sp, #4]
 80080f2:	2300      	movs	r3, #0
 80080f4:	9300      	str	r3, [sp, #0]
 80080f6:	2300      	movs	r3, #0
 80080f8:	460a      	mov	r2, r1
 80080fa:	4923      	ldr	r1, [pc, #140]	; (8008188 <vTaskStartScheduler+0xbc>)
 80080fc:	4823      	ldr	r0, [pc, #140]	; (800818c <vTaskStartScheduler+0xc0>)
 80080fe:	f7ff fe13 	bl	8007d28 <xTaskCreateStatic>
 8008102:	4602      	mov	r2, r0
 8008104:	4b22      	ldr	r3, [pc, #136]	; (8008190 <vTaskStartScheduler+0xc4>)
 8008106:	601a      	str	r2, [r3, #0]
 8008108:	4b21      	ldr	r3, [pc, #132]	; (8008190 <vTaskStartScheduler+0xc4>)
 800810a:	681b      	ldr	r3, [r3, #0]
 800810c:	2b00      	cmp	r3, #0
 800810e:	d002      	beq.n	8008116 <vTaskStartScheduler+0x4a>
 8008110:	2301      	movs	r3, #1
 8008112:	617b      	str	r3, [r7, #20]
 8008114:	e001      	b.n	800811a <vTaskStartScheduler+0x4e>
 8008116:	2300      	movs	r3, #0
 8008118:	617b      	str	r3, [r7, #20]
 800811a:	697b      	ldr	r3, [r7, #20]
 800811c:	2b01      	cmp	r3, #1
 800811e:	d102      	bne.n	8008126 <vTaskStartScheduler+0x5a>
 8008120:	f001 f83c 	bl	800919c <xTimerCreateTimerTask>
 8008124:	6178      	str	r0, [r7, #20]
 8008126:	697b      	ldr	r3, [r7, #20]
 8008128:	2b01      	cmp	r3, #1
 800812a:	d11a      	bne.n	8008162 <vTaskStartScheduler+0x96>
 800812c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008130:	f383 8811 	msr	BASEPRI, r3
 8008134:	f3bf 8f6f 	isb	sy
 8008138:	f3bf 8f4f 	dsb	sy
 800813c:	613b      	str	r3, [r7, #16]
 800813e:	4b15      	ldr	r3, [pc, #84]	; (8008194 <vTaskStartScheduler+0xc8>)
 8008140:	681b      	ldr	r3, [r3, #0]
 8008142:	3350      	adds	r3, #80	; 0x50
 8008144:	4a14      	ldr	r2, [pc, #80]	; (8008198 <vTaskStartScheduler+0xcc>)
 8008146:	6013      	str	r3, [r2, #0]
 8008148:	4b14      	ldr	r3, [pc, #80]	; (800819c <vTaskStartScheduler+0xd0>)
 800814a:	f04f 32ff 	mov.w	r2, #4294967295
 800814e:	601a      	str	r2, [r3, #0]
 8008150:	4b13      	ldr	r3, [pc, #76]	; (80081a0 <vTaskStartScheduler+0xd4>)
 8008152:	2201      	movs	r2, #1
 8008154:	601a      	str	r2, [r3, #0]
 8008156:	4b13      	ldr	r3, [pc, #76]	; (80081a4 <vTaskStartScheduler+0xd8>)
 8008158:	2200      	movs	r2, #0
 800815a:	601a      	str	r2, [r3, #0]
 800815c:	f001 fb98 	bl	8009890 <xPortStartScheduler>
 8008160:	e00d      	b.n	800817e <vTaskStartScheduler+0xb2>
 8008162:	697b      	ldr	r3, [r7, #20]
 8008164:	f1b3 3fff 	cmp.w	r3, #4294967295
 8008168:	d109      	bne.n	800817e <vTaskStartScheduler+0xb2>
 800816a:	f04f 0350 	mov.w	r3, #80	; 0x50
 800816e:	f383 8811 	msr	BASEPRI, r3
 8008172:	f3bf 8f6f 	isb	sy
 8008176:	f3bf 8f4f 	dsb	sy
 800817a:	60fb      	str	r3, [r7, #12]
 800817c:	e7fe      	b.n	800817c <vTaskStartScheduler+0xb0>
 800817e:	bf00      	nop
 8008180:	3718      	adds	r7, #24
 8008182:	46bd      	mov	sp, r7
 8008184:	bd80      	pop	{r7, pc}
 8008186:	bf00      	nop
 8008188:	0800d100 	.word	0x0800d100
 800818c:	08008831 	.word	0x08008831
 8008190:	20000cf4 	.word	0x20000cf4
 8008194:	20000bf8 	.word	0x20000bf8
 8008198:	20000080 	.word	0x20000080
 800819c:	20000cf0 	.word	0x20000cf0
 80081a0:	20000cdc 	.word	0x20000cdc
 80081a4:	20000cd4 	.word	0x20000cd4

080081a8 <vTaskSuspendAll>:
 80081a8:	b480      	push	{r7}
 80081aa:	af00      	add	r7, sp, #0
 80081ac:	4b04      	ldr	r3, [pc, #16]	; (80081c0 <vTaskSuspendAll+0x18>)
 80081ae:	681b      	ldr	r3, [r3, #0]
 80081b0:	3301      	adds	r3, #1
 80081b2:	4a03      	ldr	r2, [pc, #12]	; (80081c0 <vTaskSuspendAll+0x18>)
 80081b4:	6013      	str	r3, [r2, #0]
 80081b6:	bf00      	nop
 80081b8:	46bd      	mov	sp, r7
 80081ba:	f85d 7b04 	ldr.w	r7, [sp], #4
 80081be:	4770      	bx	lr
 80081c0:	20000cf8 	.word	0x20000cf8

080081c4 <xTaskResumeAll>:
 80081c4:	b580      	push	{r7, lr}
 80081c6:	b084      	sub	sp, #16
 80081c8:	af00      	add	r7, sp, #0
 80081ca:	2300      	movs	r3, #0
 80081cc:	60fb      	str	r3, [r7, #12]
 80081ce:	2300      	movs	r3, #0
 80081d0:	60bb      	str	r3, [r7, #8]
 80081d2:	4b41      	ldr	r3, [pc, #260]	; (80082d8 <xTaskResumeAll+0x114>)
 80081d4:	681b      	ldr	r3, [r3, #0]
 80081d6:	2b00      	cmp	r3, #0
 80081d8:	d109      	bne.n	80081ee <xTaskResumeAll+0x2a>
 80081da:	f04f 0350 	mov.w	r3, #80	; 0x50
 80081de:	f383 8811 	msr	BASEPRI, r3
 80081e2:	f3bf 8f6f 	isb	sy
 80081e6:	f3bf 8f4f 	dsb	sy
 80081ea:	603b      	str	r3, [r7, #0]
 80081ec:	e7fe      	b.n	80081ec <xTaskResumeAll+0x28>
 80081ee:	f001 fbed 	bl	80099cc <vPortEnterCritical>
 80081f2:	4b39      	ldr	r3, [pc, #228]	; (80082d8 <xTaskResumeAll+0x114>)
 80081f4:	681b      	ldr	r3, [r3, #0]
 80081f6:	3b01      	subs	r3, #1
 80081f8:	4a37      	ldr	r2, [pc, #220]	; (80082d8 <xTaskResumeAll+0x114>)
 80081fa:	6013      	str	r3, [r2, #0]
 80081fc:	4b36      	ldr	r3, [pc, #216]	; (80082d8 <xTaskResumeAll+0x114>)
 80081fe:	681b      	ldr	r3, [r3, #0]
 8008200:	2b00      	cmp	r3, #0
 8008202:	d161      	bne.n	80082c8 <xTaskResumeAll+0x104>
 8008204:	4b35      	ldr	r3, [pc, #212]	; (80082dc <xTaskResumeAll+0x118>)
 8008206:	681b      	ldr	r3, [r3, #0]
 8008208:	2b00      	cmp	r3, #0
 800820a:	d05d      	beq.n	80082c8 <xTaskResumeAll+0x104>
 800820c:	e02e      	b.n	800826c <xTaskResumeAll+0xa8>
 800820e:	4b34      	ldr	r3, [pc, #208]	; (80082e0 <xTaskResumeAll+0x11c>)
 8008210:	68db      	ldr	r3, [r3, #12]
 8008212:	68db      	ldr	r3, [r3, #12]
 8008214:	60fb      	str	r3, [r7, #12]
 8008216:	68fb      	ldr	r3, [r7, #12]
 8008218:	3318      	adds	r3, #24
 800821a:	4618      	mov	r0, r3
 800821c:	f7fe ff2f 	bl	800707e <uxListRemove>
 8008220:	68fb      	ldr	r3, [r7, #12]
 8008222:	3304      	adds	r3, #4
 8008224:	4618      	mov	r0, r3
 8008226:	f7fe ff2a 	bl	800707e <uxListRemove>
 800822a:	68fb      	ldr	r3, [r7, #12]
 800822c:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 800822e:	2201      	movs	r2, #1
 8008230:	409a      	lsls	r2, r3
 8008232:	4b2c      	ldr	r3, [pc, #176]	; (80082e4 <xTaskResumeAll+0x120>)
 8008234:	681b      	ldr	r3, [r3, #0]
 8008236:	4313      	orrs	r3, r2
 8008238:	4a2a      	ldr	r2, [pc, #168]	; (80082e4 <xTaskResumeAll+0x120>)
 800823a:	6013      	str	r3, [r2, #0]
 800823c:	68fb      	ldr	r3, [r7, #12]
 800823e:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008240:	4613      	mov	r3, r2
 8008242:	009b      	lsls	r3, r3, #2
 8008244:	4413      	add	r3, r2
 8008246:	009b      	lsls	r3, r3, #2
 8008248:	4a27      	ldr	r2, [pc, #156]	; (80082e8 <xTaskResumeAll+0x124>)
 800824a:	441a      	add	r2, r3
 800824c:	68fb      	ldr	r3, [r7, #12]
 800824e:	3304      	adds	r3, #4
 8008250:	4619      	mov	r1, r3
 8008252:	4610      	mov	r0, r2
 8008254:	f7fe feb6 	bl	8006fc4 <vListInsertEnd>
 8008258:	68fb      	ldr	r3, [r7, #12]
 800825a:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 800825c:	4b23      	ldr	r3, [pc, #140]	; (80082ec <xTaskResumeAll+0x128>)
 800825e:	681b      	ldr	r3, [r3, #0]
 8008260:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008262:	429a      	cmp	r2, r3
 8008264:	d302      	bcc.n	800826c <xTaskResumeAll+0xa8>
 8008266:	4b22      	ldr	r3, [pc, #136]	; (80082f0 <xTaskResumeAll+0x12c>)
 8008268:	2201      	movs	r2, #1
 800826a:	601a      	str	r2, [r3, #0]
 800826c:	4b1c      	ldr	r3, [pc, #112]	; (80082e0 <xTaskResumeAll+0x11c>)
 800826e:	681b      	ldr	r3, [r3, #0]
 8008270:	2b00      	cmp	r3, #0
 8008272:	d1cc      	bne.n	800820e <xTaskResumeAll+0x4a>
 8008274:	68fb      	ldr	r3, [r7, #12]
 8008276:	2b00      	cmp	r3, #0
 8008278:	d001      	beq.n	800827e <xTaskResumeAll+0xba>
 800827a:	f000 fb93 	bl	80089a4 <prvResetNextTaskUnblockTime>
 800827e:	4b1d      	ldr	r3, [pc, #116]	; (80082f4 <xTaskResumeAll+0x130>)
 8008280:	681b      	ldr	r3, [r3, #0]
 8008282:	607b      	str	r3, [r7, #4]
 8008284:	687b      	ldr	r3, [r7, #4]
 8008286:	2b00      	cmp	r3, #0
 8008288:	d010      	beq.n	80082ac <xTaskResumeAll+0xe8>
 800828a:	f000 f859 	bl	8008340 <xTaskIncrementTick>
 800828e:	4603      	mov	r3, r0
 8008290:	2b00      	cmp	r3, #0
 8008292:	d002      	beq.n	800829a <xTaskResumeAll+0xd6>
 8008294:	4b16      	ldr	r3, [pc, #88]	; (80082f0 <xTaskResumeAll+0x12c>)
 8008296:	2201      	movs	r2, #1
 8008298:	601a      	str	r2, [r3, #0]
 800829a:	687b      	ldr	r3, [r7, #4]
 800829c:	3b01      	subs	r3, #1
 800829e:	607b      	str	r3, [r7, #4]
 80082a0:	687b      	ldr	r3, [r7, #4]
 80082a2:	2b00      	cmp	r3, #0
 80082a4:	d1f1      	bne.n	800828a <xTaskResumeAll+0xc6>
 80082a6:	4b13      	ldr	r3, [pc, #76]	; (80082f4 <xTaskResumeAll+0x130>)
 80082a8:	2200      	movs	r2, #0
 80082aa:	601a      	str	r2, [r3, #0]
 80082ac:	4b10      	ldr	r3, [pc, #64]	; (80082f0 <xTaskResumeAll+0x12c>)
 80082ae:	681b      	ldr	r3, [r3, #0]
 80082b0:	2b00      	cmp	r3, #0
 80082b2:	d009      	beq.n	80082c8 <xTaskResumeAll+0x104>
 80082b4:	2301      	movs	r3, #1
 80082b6:	60bb      	str	r3, [r7, #8]
 80082b8:	4b0f      	ldr	r3, [pc, #60]	; (80082f8 <xTaskResumeAll+0x134>)
 80082ba:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80082be:	601a      	str	r2, [r3, #0]
 80082c0:	f3bf 8f4f 	dsb	sy
 80082c4:	f3bf 8f6f 	isb	sy
 80082c8:	f001 fbae 	bl	8009a28 <vPortExitCritical>
 80082cc:	68bb      	ldr	r3, [r7, #8]
 80082ce:	4618      	mov	r0, r3
 80082d0:	3710      	adds	r7, #16
 80082d2:	46bd      	mov	sp, r7
 80082d4:	bd80      	pop	{r7, pc}
 80082d6:	bf00      	nop
 80082d8:	20000cf8 	.word	0x20000cf8
 80082dc:	20000cd0 	.word	0x20000cd0
 80082e0:	20000c90 	.word	0x20000c90
 80082e4:	20000cd8 	.word	0x20000cd8
 80082e8:	20000bfc 	.word	0x20000bfc
 80082ec:	20000bf8 	.word	0x20000bf8
 80082f0:	20000ce4 	.word	0x20000ce4
 80082f4:	20000ce0 	.word	0x20000ce0
 80082f8:	e000ed04 	.word	0xe000ed04

080082fc <xTaskGetTickCount>:
 80082fc:	b480      	push	{r7}
 80082fe:	b083      	sub	sp, #12
 8008300:	af00      	add	r7, sp, #0
 8008302:	4b05      	ldr	r3, [pc, #20]	; (8008318 <xTaskGetTickCount+0x1c>)
 8008304:	681b      	ldr	r3, [r3, #0]
 8008306:	607b      	str	r3, [r7, #4]
 8008308:	687b      	ldr	r3, [r7, #4]
 800830a:	4618      	mov	r0, r3
 800830c:	370c      	adds	r7, #12
 800830e:	46bd      	mov	sp, r7
 8008310:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008314:	4770      	bx	lr
 8008316:	bf00      	nop
 8008318:	20000cd4 	.word	0x20000cd4

0800831c <xTaskGetTickCountFromISR>:
 800831c:	b580      	push	{r7, lr}
 800831e:	b082      	sub	sp, #8
 8008320:	af00      	add	r7, sp, #0
 8008322:	f001 fc2f 	bl	8009b84 <vPortValidateInterruptPriority>
 8008326:	2300      	movs	r3, #0
 8008328:	607b      	str	r3, [r7, #4]
 800832a:	4b04      	ldr	r3, [pc, #16]	; (800833c <xTaskGetTickCountFromISR+0x20>)
 800832c:	681b      	ldr	r3, [r3, #0]
 800832e:	603b      	str	r3, [r7, #0]
 8008330:	683b      	ldr	r3, [r7, #0]
 8008332:	4618      	mov	r0, r3
 8008334:	3708      	adds	r7, #8
 8008336:	46bd      	mov	sp, r7
 8008338:	bd80      	pop	{r7, pc}
 800833a:	bf00      	nop
 800833c:	20000cd4 	.word	0x20000cd4

08008340 <xTaskIncrementTick>:
 8008340:	b580      	push	{r7, lr}
 8008342:	b086      	sub	sp, #24
 8008344:	af00      	add	r7, sp, #0
 8008346:	2300      	movs	r3, #0
 8008348:	617b      	str	r3, [r7, #20]
 800834a:	4b52      	ldr	r3, [pc, #328]	; (8008494 <xTaskIncrementTick+0x154>)
 800834c:	681b      	ldr	r3, [r3, #0]
 800834e:	2b00      	cmp	r3, #0
 8008350:	f040 808d 	bne.w	800846e <xTaskIncrementTick+0x12e>
 8008354:	4b50      	ldr	r3, [pc, #320]	; (8008498 <xTaskIncrementTick+0x158>)
 8008356:	681b      	ldr	r3, [r3, #0]
 8008358:	3301      	adds	r3, #1
 800835a:	613b      	str	r3, [r7, #16]
 800835c:	4a4e      	ldr	r2, [pc, #312]	; (8008498 <xTaskIncrementTick+0x158>)
 800835e:	693b      	ldr	r3, [r7, #16]
 8008360:	6013      	str	r3, [r2, #0]
 8008362:	693b      	ldr	r3, [r7, #16]
 8008364:	2b00      	cmp	r3, #0
 8008366:	d11f      	bne.n	80083a8 <xTaskIncrementTick+0x68>
 8008368:	4b4c      	ldr	r3, [pc, #304]	; (800849c <xTaskIncrementTick+0x15c>)
 800836a:	681b      	ldr	r3, [r3, #0]
 800836c:	681b      	ldr	r3, [r3, #0]
 800836e:	2b00      	cmp	r3, #0
 8008370:	d009      	beq.n	8008386 <xTaskIncrementTick+0x46>
 8008372:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008376:	f383 8811 	msr	BASEPRI, r3
 800837a:	f3bf 8f6f 	isb	sy
 800837e:	f3bf 8f4f 	dsb	sy
 8008382:	603b      	str	r3, [r7, #0]
 8008384:	e7fe      	b.n	8008384 <xTaskIncrementTick+0x44>
 8008386:	4b45      	ldr	r3, [pc, #276]	; (800849c <xTaskIncrementTick+0x15c>)
 8008388:	681b      	ldr	r3, [r3, #0]
 800838a:	60fb      	str	r3, [r7, #12]
 800838c:	4b44      	ldr	r3, [pc, #272]	; (80084a0 <xTaskIncrementTick+0x160>)
 800838e:	681b      	ldr	r3, [r3, #0]
 8008390:	4a42      	ldr	r2, [pc, #264]	; (800849c <xTaskIncrementTick+0x15c>)
 8008392:	6013      	str	r3, [r2, #0]
 8008394:	4a42      	ldr	r2, [pc, #264]	; (80084a0 <xTaskIncrementTick+0x160>)
 8008396:	68fb      	ldr	r3, [r7, #12]
 8008398:	6013      	str	r3, [r2, #0]
 800839a:	4b42      	ldr	r3, [pc, #264]	; (80084a4 <xTaskIncrementTick+0x164>)
 800839c:	681b      	ldr	r3, [r3, #0]
 800839e:	3301      	adds	r3, #1
 80083a0:	4a40      	ldr	r2, [pc, #256]	; (80084a4 <xTaskIncrementTick+0x164>)
 80083a2:	6013      	str	r3, [r2, #0]
 80083a4:	f000 fafe 	bl	80089a4 <prvResetNextTaskUnblockTime>
 80083a8:	4b3f      	ldr	r3, [pc, #252]	; (80084a8 <xTaskIncrementTick+0x168>)
 80083aa:	681b      	ldr	r3, [r3, #0]
 80083ac:	693a      	ldr	r2, [r7, #16]
 80083ae:	429a      	cmp	r2, r3
 80083b0:	d348      	bcc.n	8008444 <xTaskIncrementTick+0x104>
 80083b2:	4b3a      	ldr	r3, [pc, #232]	; (800849c <xTaskIncrementTick+0x15c>)
 80083b4:	681b      	ldr	r3, [r3, #0]
 80083b6:	681b      	ldr	r3, [r3, #0]
 80083b8:	2b00      	cmp	r3, #0
 80083ba:	d104      	bne.n	80083c6 <xTaskIncrementTick+0x86>
 80083bc:	4b3a      	ldr	r3, [pc, #232]	; (80084a8 <xTaskIncrementTick+0x168>)
 80083be:	f04f 32ff 	mov.w	r2, #4294967295
 80083c2:	601a      	str	r2, [r3, #0]
 80083c4:	e03e      	b.n	8008444 <xTaskIncrementTick+0x104>
 80083c6:	4b35      	ldr	r3, [pc, #212]	; (800849c <xTaskIncrementTick+0x15c>)
 80083c8:	681b      	ldr	r3, [r3, #0]
 80083ca:	68db      	ldr	r3, [r3, #12]
 80083cc:	68db      	ldr	r3, [r3, #12]
 80083ce:	60bb      	str	r3, [r7, #8]
 80083d0:	68bb      	ldr	r3, [r7, #8]
 80083d2:	685b      	ldr	r3, [r3, #4]
 80083d4:	607b      	str	r3, [r7, #4]
 80083d6:	693a      	ldr	r2, [r7, #16]
 80083d8:	687b      	ldr	r3, [r7, #4]
 80083da:	429a      	cmp	r2, r3
 80083dc:	d203      	bcs.n	80083e6 <xTaskIncrementTick+0xa6>
 80083de:	4a32      	ldr	r2, [pc, #200]	; (80084a8 <xTaskIncrementTick+0x168>)
 80083e0:	687b      	ldr	r3, [r7, #4]
 80083e2:	6013      	str	r3, [r2, #0]
 80083e4:	e02e      	b.n	8008444 <xTaskIncrementTick+0x104>
 80083e6:	68bb      	ldr	r3, [r7, #8]
 80083e8:	3304      	adds	r3, #4
 80083ea:	4618      	mov	r0, r3
 80083ec:	f7fe fe47 	bl	800707e <uxListRemove>
 80083f0:	68bb      	ldr	r3, [r7, #8]
 80083f2:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 80083f4:	2b00      	cmp	r3, #0
 80083f6:	d004      	beq.n	8008402 <xTaskIncrementTick+0xc2>
 80083f8:	68bb      	ldr	r3, [r7, #8]
 80083fa:	3318      	adds	r3, #24
 80083fc:	4618      	mov	r0, r3
 80083fe:	f7fe fe3e 	bl	800707e <uxListRemove>
 8008402:	68bb      	ldr	r3, [r7, #8]
 8008404:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008406:	2201      	movs	r2, #1
 8008408:	409a      	lsls	r2, r3
 800840a:	4b28      	ldr	r3, [pc, #160]	; (80084ac <xTaskIncrementTick+0x16c>)
 800840c:	681b      	ldr	r3, [r3, #0]
 800840e:	4313      	orrs	r3, r2
 8008410:	4a26      	ldr	r2, [pc, #152]	; (80084ac <xTaskIncrementTick+0x16c>)
 8008412:	6013      	str	r3, [r2, #0]
 8008414:	68bb      	ldr	r3, [r7, #8]
 8008416:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008418:	4613      	mov	r3, r2
 800841a:	009b      	lsls	r3, r3, #2
 800841c:	4413      	add	r3, r2
 800841e:	009b      	lsls	r3, r3, #2
 8008420:	4a23      	ldr	r2, [pc, #140]	; (80084b0 <xTaskIncrementTick+0x170>)
 8008422:	441a      	add	r2, r3
 8008424:	68bb      	ldr	r3, [r7, #8]
 8008426:	3304      	adds	r3, #4
 8008428:	4619      	mov	r1, r3
 800842a:	4610      	mov	r0, r2
 800842c:	f7fe fdca 	bl	8006fc4 <vListInsertEnd>
 8008430:	68bb      	ldr	r3, [r7, #8]
 8008432:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008434:	4b1f      	ldr	r3, [pc, #124]	; (80084b4 <xTaskIncrementTick+0x174>)
 8008436:	681b      	ldr	r3, [r3, #0]
 8008438:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 800843a:	429a      	cmp	r2, r3
 800843c:	d3b9      	bcc.n	80083b2 <xTaskIncrementTick+0x72>
 800843e:	2301      	movs	r3, #1
 8008440:	617b      	str	r3, [r7, #20]
 8008442:	e7b6      	b.n	80083b2 <xTaskIncrementTick+0x72>
 8008444:	4b1b      	ldr	r3, [pc, #108]	; (80084b4 <xTaskIncrementTick+0x174>)
 8008446:	681b      	ldr	r3, [r3, #0]
 8008448:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 800844a:	4919      	ldr	r1, [pc, #100]	; (80084b0 <xTaskIncrementTick+0x170>)
 800844c:	4613      	mov	r3, r2
 800844e:	009b      	lsls	r3, r3, #2
 8008450:	4413      	add	r3, r2
 8008452:	009b      	lsls	r3, r3, #2
 8008454:	440b      	add	r3, r1
 8008456:	681b      	ldr	r3, [r3, #0]
 8008458:	2b01      	cmp	r3, #1
 800845a:	d901      	bls.n	8008460 <xTaskIncrementTick+0x120>
 800845c:	2301      	movs	r3, #1
 800845e:	617b      	str	r3, [r7, #20]
 8008460:	4b15      	ldr	r3, [pc, #84]	; (80084b8 <xTaskIncrementTick+0x178>)
 8008462:	681b      	ldr	r3, [r3, #0]
 8008464:	2b00      	cmp	r3, #0
 8008466:	d109      	bne.n	800847c <xTaskIncrementTick+0x13c>
 8008468:	f7fc fa10 	bl	800488c <vApplicationTickHook>
 800846c:	e006      	b.n	800847c <xTaskIncrementTick+0x13c>
 800846e:	4b12      	ldr	r3, [pc, #72]	; (80084b8 <xTaskIncrementTick+0x178>)
 8008470:	681b      	ldr	r3, [r3, #0]
 8008472:	3301      	adds	r3, #1
 8008474:	4a10      	ldr	r2, [pc, #64]	; (80084b8 <xTaskIncrementTick+0x178>)
 8008476:	6013      	str	r3, [r2, #0]
 8008478:	f7fc fa08 	bl	800488c <vApplicationTickHook>
 800847c:	4b0f      	ldr	r3, [pc, #60]	; (80084bc <xTaskIncrementTick+0x17c>)
 800847e:	681b      	ldr	r3, [r3, #0]
 8008480:	2b00      	cmp	r3, #0
 8008482:	d001      	beq.n	8008488 <xTaskIncrementTick+0x148>
 8008484:	2301      	movs	r3, #1
 8008486:	617b      	str	r3, [r7, #20]
 8008488:	697b      	ldr	r3, [r7, #20]
 800848a:	4618      	mov	r0, r3
 800848c:	3718      	adds	r7, #24
 800848e:	46bd      	mov	sp, r7
 8008490:	bd80      	pop	{r7, pc}
 8008492:	bf00      	nop
 8008494:	20000cf8 	.word	0x20000cf8
 8008498:	20000cd4 	.word	0x20000cd4
 800849c:	20000c88 	.word	0x20000c88
 80084a0:	20000c8c 	.word	0x20000c8c
 80084a4:	20000ce8 	.word	0x20000ce8
 80084a8:	20000cf0 	.word	0x20000cf0
 80084ac:	20000cd8 	.word	0x20000cd8
 80084b0:	20000bfc 	.word	0x20000bfc
 80084b4:	20000bf8 	.word	0x20000bf8
 80084b8:	20000ce0 	.word	0x20000ce0
 80084bc:	20000ce4 	.word	0x20000ce4

080084c0 <vTaskSwitchContext>:
 80084c0:	b580      	push	{r7, lr}
 80084c2:	b088      	sub	sp, #32
 80084c4:	af00      	add	r7, sp, #0
 80084c6:	4b3b      	ldr	r3, [pc, #236]	; (80085b4 <vTaskSwitchContext+0xf4>)
 80084c8:	681b      	ldr	r3, [r3, #0]
 80084ca:	2b00      	cmp	r3, #0
 80084cc:	d003      	beq.n	80084d6 <vTaskSwitchContext+0x16>
 80084ce:	4b3a      	ldr	r3, [pc, #232]	; (80085b8 <vTaskSwitchContext+0xf8>)
 80084d0:	2201      	movs	r2, #1
 80084d2:	601a      	str	r2, [r3, #0]
 80084d4:	e06a      	b.n	80085ac <vTaskSwitchContext+0xec>
 80084d6:	4b38      	ldr	r3, [pc, #224]	; (80085b8 <vTaskSwitchContext+0xf8>)
 80084d8:	2200      	movs	r2, #0
 80084da:	601a      	str	r2, [r3, #0]
 80084dc:	4b37      	ldr	r3, [pc, #220]	; (80085bc <vTaskSwitchContext+0xfc>)
 80084de:	681b      	ldr	r3, [r3, #0]
 80084e0:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 80084e2:	61fb      	str	r3, [r7, #28]
 80084e4:	f04f 33a5 	mov.w	r3, #2779096485	; 0xa5a5a5a5
 80084e8:	61bb      	str	r3, [r7, #24]
 80084ea:	69fb      	ldr	r3, [r7, #28]
 80084ec:	681b      	ldr	r3, [r3, #0]
 80084ee:	69ba      	ldr	r2, [r7, #24]
 80084f0:	429a      	cmp	r2, r3
 80084f2:	d111      	bne.n	8008518 <vTaskSwitchContext+0x58>
 80084f4:	69fb      	ldr	r3, [r7, #28]
 80084f6:	3304      	adds	r3, #4
 80084f8:	681b      	ldr	r3, [r3, #0]
 80084fa:	69ba      	ldr	r2, [r7, #24]
 80084fc:	429a      	cmp	r2, r3
 80084fe:	d10b      	bne.n	8008518 <vTaskSwitchContext+0x58>
 8008500:	69fb      	ldr	r3, [r7, #28]
 8008502:	3308      	adds	r3, #8
 8008504:	681b      	ldr	r3, [r3, #0]
 8008506:	69ba      	ldr	r2, [r7, #24]
 8008508:	429a      	cmp	r2, r3
 800850a:	d105      	bne.n	8008518 <vTaskSwitchContext+0x58>
 800850c:	69fb      	ldr	r3, [r7, #28]
 800850e:	330c      	adds	r3, #12
 8008510:	681b      	ldr	r3, [r3, #0]
 8008512:	69ba      	ldr	r2, [r7, #24]
 8008514:	429a      	cmp	r2, r3
 8008516:	d008      	beq.n	800852a <vTaskSwitchContext+0x6a>
 8008518:	4b28      	ldr	r3, [pc, #160]	; (80085bc <vTaskSwitchContext+0xfc>)
 800851a:	681a      	ldr	r2, [r3, #0]
 800851c:	4b27      	ldr	r3, [pc, #156]	; (80085bc <vTaskSwitchContext+0xfc>)
 800851e:	681b      	ldr	r3, [r3, #0]
 8008520:	3334      	adds	r3, #52	; 0x34
 8008522:	4619      	mov	r1, r3
 8008524:	4610      	mov	r0, r2
 8008526:	f7fc f9b8 	bl	800489a <vApplicationStackOverflowHook>
 800852a:	4b25      	ldr	r3, [pc, #148]	; (80085c0 <vTaskSwitchContext+0x100>)
 800852c:	681b      	ldr	r3, [r3, #0]
 800852e:	60fb      	str	r3, [r7, #12]
 8008530:	68fb      	ldr	r3, [r7, #12]
 8008532:	fab3 f383 	clz	r3, r3
 8008536:	72fb      	strb	r3, [r7, #11]
 8008538:	7afb      	ldrb	r3, [r7, #11]
 800853a:	f1c3 031f 	rsb	r3, r3, #31
 800853e:	617b      	str	r3, [r7, #20]
 8008540:	4920      	ldr	r1, [pc, #128]	; (80085c4 <vTaskSwitchContext+0x104>)
 8008542:	697a      	ldr	r2, [r7, #20]
 8008544:	4613      	mov	r3, r2
 8008546:	009b      	lsls	r3, r3, #2
 8008548:	4413      	add	r3, r2
 800854a:	009b      	lsls	r3, r3, #2
 800854c:	440b      	add	r3, r1
 800854e:	681b      	ldr	r3, [r3, #0]
 8008550:	2b00      	cmp	r3, #0
 8008552:	d109      	bne.n	8008568 <vTaskSwitchContext+0xa8>
 8008554:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008558:	f383 8811 	msr	BASEPRI, r3
 800855c:	f3bf 8f6f 	isb	sy
 8008560:	f3bf 8f4f 	dsb	sy
 8008564:	607b      	str	r3, [r7, #4]
 8008566:	e7fe      	b.n	8008566 <vTaskSwitchContext+0xa6>
 8008568:	697a      	ldr	r2, [r7, #20]
 800856a:	4613      	mov	r3, r2
 800856c:	009b      	lsls	r3, r3, #2
 800856e:	4413      	add	r3, r2
 8008570:	009b      	lsls	r3, r3, #2
 8008572:	4a14      	ldr	r2, [pc, #80]	; (80085c4 <vTaskSwitchContext+0x104>)
 8008574:	4413      	add	r3, r2
 8008576:	613b      	str	r3, [r7, #16]
 8008578:	693b      	ldr	r3, [r7, #16]
 800857a:	685b      	ldr	r3, [r3, #4]
 800857c:	685a      	ldr	r2, [r3, #4]
 800857e:	693b      	ldr	r3, [r7, #16]
 8008580:	605a      	str	r2, [r3, #4]
 8008582:	693b      	ldr	r3, [r7, #16]
 8008584:	685a      	ldr	r2, [r3, #4]
 8008586:	693b      	ldr	r3, [r7, #16]
 8008588:	3308      	adds	r3, #8
 800858a:	429a      	cmp	r2, r3
 800858c:	d104      	bne.n	8008598 <vTaskSwitchContext+0xd8>
 800858e:	693b      	ldr	r3, [r7, #16]
 8008590:	685b      	ldr	r3, [r3, #4]
 8008592:	685a      	ldr	r2, [r3, #4]
 8008594:	693b      	ldr	r3, [r7, #16]
 8008596:	605a      	str	r2, [r3, #4]
 8008598:	693b      	ldr	r3, [r7, #16]
 800859a:	685b      	ldr	r3, [r3, #4]
 800859c:	68db      	ldr	r3, [r3, #12]
 800859e:	4a07      	ldr	r2, [pc, #28]	; (80085bc <vTaskSwitchContext+0xfc>)
 80085a0:	6013      	str	r3, [r2, #0]
 80085a2:	4b06      	ldr	r3, [pc, #24]	; (80085bc <vTaskSwitchContext+0xfc>)
 80085a4:	681b      	ldr	r3, [r3, #0]
 80085a6:	3350      	adds	r3, #80	; 0x50
 80085a8:	4a07      	ldr	r2, [pc, #28]	; (80085c8 <vTaskSwitchContext+0x108>)
 80085aa:	6013      	str	r3, [r2, #0]
 80085ac:	bf00      	nop
 80085ae:	3720      	adds	r7, #32
 80085b0:	46bd      	mov	sp, r7
 80085b2:	bd80      	pop	{r7, pc}
 80085b4:	20000cf8 	.word	0x20000cf8
 80085b8:	20000ce4 	.word	0x20000ce4
 80085bc:	20000bf8 	.word	0x20000bf8
 80085c0:	20000cd8 	.word	0x20000cd8
 80085c4:	20000bfc 	.word	0x20000bfc
 80085c8:	20000080 	.word	0x20000080

080085cc <vTaskPlaceOnEventList>:
 80085cc:	b580      	push	{r7, lr}
 80085ce:	b084      	sub	sp, #16
 80085d0:	af00      	add	r7, sp, #0
 80085d2:	6078      	str	r0, [r7, #4]
 80085d4:	6039      	str	r1, [r7, #0]
 80085d6:	687b      	ldr	r3, [r7, #4]
 80085d8:	2b00      	cmp	r3, #0
 80085da:	d109      	bne.n	80085f0 <vTaskPlaceOnEventList+0x24>
 80085dc:	f04f 0350 	mov.w	r3, #80	; 0x50
 80085e0:	f383 8811 	msr	BASEPRI, r3
 80085e4:	f3bf 8f6f 	isb	sy
 80085e8:	f3bf 8f4f 	dsb	sy
 80085ec:	60fb      	str	r3, [r7, #12]
 80085ee:	e7fe      	b.n	80085ee <vTaskPlaceOnEventList+0x22>
 80085f0:	4b07      	ldr	r3, [pc, #28]	; (8008610 <vTaskPlaceOnEventList+0x44>)
 80085f2:	681b      	ldr	r3, [r3, #0]
 80085f4:	3318      	adds	r3, #24
 80085f6:	4619      	mov	r1, r3
 80085f8:	6878      	ldr	r0, [r7, #4]
 80085fa:	f7fe fd07 	bl	800700c <vListInsert>
 80085fe:	2101      	movs	r1, #1
 8008600:	6838      	ldr	r0, [r7, #0]
 8008602:	f000 fd65 	bl	80090d0 <prvAddCurrentTaskToDelayedList>
 8008606:	bf00      	nop
 8008608:	3710      	adds	r7, #16
 800860a:	46bd      	mov	sp, r7
 800860c:	bd80      	pop	{r7, pc}
 800860e:	bf00      	nop
 8008610:	20000bf8 	.word	0x20000bf8

08008614 <vTaskPlaceOnEventListRestricted>:
 8008614:	b580      	push	{r7, lr}
 8008616:	b086      	sub	sp, #24
 8008618:	af00      	add	r7, sp, #0
 800861a:	60f8      	str	r0, [r7, #12]
 800861c:	60b9      	str	r1, [r7, #8]
 800861e:	607a      	str	r2, [r7, #4]
 8008620:	68fb      	ldr	r3, [r7, #12]
 8008622:	2b00      	cmp	r3, #0
 8008624:	d109      	bne.n	800863a <vTaskPlaceOnEventListRestricted+0x26>
 8008626:	f04f 0350 	mov.w	r3, #80	; 0x50
 800862a:	f383 8811 	msr	BASEPRI, r3
 800862e:	f3bf 8f6f 	isb	sy
 8008632:	f3bf 8f4f 	dsb	sy
 8008636:	617b      	str	r3, [r7, #20]
 8008638:	e7fe      	b.n	8008638 <vTaskPlaceOnEventListRestricted+0x24>
 800863a:	4b0a      	ldr	r3, [pc, #40]	; (8008664 <vTaskPlaceOnEventListRestricted+0x50>)
 800863c:	681b      	ldr	r3, [r3, #0]
 800863e:	3318      	adds	r3, #24
 8008640:	4619      	mov	r1, r3
 8008642:	68f8      	ldr	r0, [r7, #12]
 8008644:	f7fe fcbe 	bl	8006fc4 <vListInsertEnd>
 8008648:	687b      	ldr	r3, [r7, #4]
 800864a:	2b00      	cmp	r3, #0
 800864c:	d002      	beq.n	8008654 <vTaskPlaceOnEventListRestricted+0x40>
 800864e:	f04f 33ff 	mov.w	r3, #4294967295
 8008652:	60bb      	str	r3, [r7, #8]
 8008654:	6879      	ldr	r1, [r7, #4]
 8008656:	68b8      	ldr	r0, [r7, #8]
 8008658:	f000 fd3a 	bl	80090d0 <prvAddCurrentTaskToDelayedList>
 800865c:	bf00      	nop
 800865e:	3718      	adds	r7, #24
 8008660:	46bd      	mov	sp, r7
 8008662:	bd80      	pop	{r7, pc}
 8008664:	20000bf8 	.word	0x20000bf8

08008668 <xTaskRemoveFromEventList>:
 8008668:	b580      	push	{r7, lr}
 800866a:	b086      	sub	sp, #24
 800866c:	af00      	add	r7, sp, #0
 800866e:	6078      	str	r0, [r7, #4]
 8008670:	687b      	ldr	r3, [r7, #4]
 8008672:	68db      	ldr	r3, [r3, #12]
 8008674:	68db      	ldr	r3, [r3, #12]
 8008676:	613b      	str	r3, [r7, #16]
 8008678:	693b      	ldr	r3, [r7, #16]
 800867a:	2b00      	cmp	r3, #0
 800867c:	d109      	bne.n	8008692 <xTaskRemoveFromEventList+0x2a>
 800867e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008682:	f383 8811 	msr	BASEPRI, r3
 8008686:	f3bf 8f6f 	isb	sy
 800868a:	f3bf 8f4f 	dsb	sy
 800868e:	60fb      	str	r3, [r7, #12]
 8008690:	e7fe      	b.n	8008690 <xTaskRemoveFromEventList+0x28>
 8008692:	693b      	ldr	r3, [r7, #16]
 8008694:	3318      	adds	r3, #24
 8008696:	4618      	mov	r0, r3
 8008698:	f7fe fcf1 	bl	800707e <uxListRemove>
 800869c:	4b1d      	ldr	r3, [pc, #116]	; (8008714 <xTaskRemoveFromEventList+0xac>)
 800869e:	681b      	ldr	r3, [r3, #0]
 80086a0:	2b00      	cmp	r3, #0
 80086a2:	d11c      	bne.n	80086de <xTaskRemoveFromEventList+0x76>
 80086a4:	693b      	ldr	r3, [r7, #16]
 80086a6:	3304      	adds	r3, #4
 80086a8:	4618      	mov	r0, r3
 80086aa:	f7fe fce8 	bl	800707e <uxListRemove>
 80086ae:	693b      	ldr	r3, [r7, #16]
 80086b0:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80086b2:	2201      	movs	r2, #1
 80086b4:	409a      	lsls	r2, r3
 80086b6:	4b18      	ldr	r3, [pc, #96]	; (8008718 <xTaskRemoveFromEventList+0xb0>)
 80086b8:	681b      	ldr	r3, [r3, #0]
 80086ba:	4313      	orrs	r3, r2
 80086bc:	4a16      	ldr	r2, [pc, #88]	; (8008718 <xTaskRemoveFromEventList+0xb0>)
 80086be:	6013      	str	r3, [r2, #0]
 80086c0:	693b      	ldr	r3, [r7, #16]
 80086c2:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 80086c4:	4613      	mov	r3, r2
 80086c6:	009b      	lsls	r3, r3, #2
 80086c8:	4413      	add	r3, r2
 80086ca:	009b      	lsls	r3, r3, #2
 80086cc:	4a13      	ldr	r2, [pc, #76]	; (800871c <xTaskRemoveFromEventList+0xb4>)
 80086ce:	441a      	add	r2, r3
 80086d0:	693b      	ldr	r3, [r7, #16]
 80086d2:	3304      	adds	r3, #4
 80086d4:	4619      	mov	r1, r3
 80086d6:	4610      	mov	r0, r2
 80086d8:	f7fe fc74 	bl	8006fc4 <vListInsertEnd>
 80086dc:	e005      	b.n	80086ea <xTaskRemoveFromEventList+0x82>
 80086de:	693b      	ldr	r3, [r7, #16]
 80086e0:	3318      	adds	r3, #24
 80086e2:	4619      	mov	r1, r3
 80086e4:	480e      	ldr	r0, [pc, #56]	; (8008720 <xTaskRemoveFromEventList+0xb8>)
 80086e6:	f7fe fc6d 	bl	8006fc4 <vListInsertEnd>
 80086ea:	693b      	ldr	r3, [r7, #16]
 80086ec:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 80086ee:	4b0d      	ldr	r3, [pc, #52]	; (8008724 <xTaskRemoveFromEventList+0xbc>)
 80086f0:	681b      	ldr	r3, [r3, #0]
 80086f2:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80086f4:	429a      	cmp	r2, r3
 80086f6:	d905      	bls.n	8008704 <xTaskRemoveFromEventList+0x9c>
 80086f8:	2301      	movs	r3, #1
 80086fa:	617b      	str	r3, [r7, #20]
 80086fc:	4b0a      	ldr	r3, [pc, #40]	; (8008728 <xTaskRemoveFromEventList+0xc0>)
 80086fe:	2201      	movs	r2, #1
 8008700:	601a      	str	r2, [r3, #0]
 8008702:	e001      	b.n	8008708 <xTaskRemoveFromEventList+0xa0>
 8008704:	2300      	movs	r3, #0
 8008706:	617b      	str	r3, [r7, #20]
 8008708:	697b      	ldr	r3, [r7, #20]
 800870a:	4618      	mov	r0, r3
 800870c:	3718      	adds	r7, #24
 800870e:	46bd      	mov	sp, r7
 8008710:	bd80      	pop	{r7, pc}
 8008712:	bf00      	nop
 8008714:	20000cf8 	.word	0x20000cf8
 8008718:	20000cd8 	.word	0x20000cd8
 800871c:	20000bfc 	.word	0x20000bfc
 8008720:	20000c90 	.word	0x20000c90
 8008724:	20000bf8 	.word	0x20000bf8
 8008728:	20000ce4 	.word	0x20000ce4

0800872c <vTaskInternalSetTimeOutState>:
 800872c:	b480      	push	{r7}
 800872e:	b083      	sub	sp, #12
 8008730:	af00      	add	r7, sp, #0
 8008732:	6078      	str	r0, [r7, #4]
 8008734:	4b06      	ldr	r3, [pc, #24]	; (8008750 <vTaskInternalSetTimeOutState+0x24>)
 8008736:	681a      	ldr	r2, [r3, #0]
 8008738:	687b      	ldr	r3, [r7, #4]
 800873a:	601a      	str	r2, [r3, #0]
 800873c:	4b05      	ldr	r3, [pc, #20]	; (8008754 <vTaskInternalSetTimeOutState+0x28>)
 800873e:	681a      	ldr	r2, [r3, #0]
 8008740:	687b      	ldr	r3, [r7, #4]
 8008742:	605a      	str	r2, [r3, #4]
 8008744:	bf00      	nop
 8008746:	370c      	adds	r7, #12
 8008748:	46bd      	mov	sp, r7
 800874a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800874e:	4770      	bx	lr
 8008750:	20000ce8 	.word	0x20000ce8
 8008754:	20000cd4 	.word	0x20000cd4

08008758 <xTaskCheckForTimeOut>:
 8008758:	b580      	push	{r7, lr}
 800875a:	b088      	sub	sp, #32
 800875c:	af00      	add	r7, sp, #0
 800875e:	6078      	str	r0, [r7, #4]
 8008760:	6039      	str	r1, [r7, #0]
 8008762:	687b      	ldr	r3, [r7, #4]
 8008764:	2b00      	cmp	r3, #0
 8008766:	d109      	bne.n	800877c <xTaskCheckForTimeOut+0x24>
 8008768:	f04f 0350 	mov.w	r3, #80	; 0x50
 800876c:	f383 8811 	msr	BASEPRI, r3
 8008770:	f3bf 8f6f 	isb	sy
 8008774:	f3bf 8f4f 	dsb	sy
 8008778:	613b      	str	r3, [r7, #16]
 800877a:	e7fe      	b.n	800877a <xTaskCheckForTimeOut+0x22>
 800877c:	683b      	ldr	r3, [r7, #0]
 800877e:	2b00      	cmp	r3, #0
 8008780:	d109      	bne.n	8008796 <xTaskCheckForTimeOut+0x3e>
 8008782:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008786:	f383 8811 	msr	BASEPRI, r3
 800878a:	f3bf 8f6f 	isb	sy
 800878e:	f3bf 8f4f 	dsb	sy
 8008792:	60fb      	str	r3, [r7, #12]
 8008794:	e7fe      	b.n	8008794 <xTaskCheckForTimeOut+0x3c>
 8008796:	f001 f919 	bl	80099cc <vPortEnterCritical>
 800879a:	4b1d      	ldr	r3, [pc, #116]	; (8008810 <xTaskCheckForTimeOut+0xb8>)
 800879c:	681b      	ldr	r3, [r3, #0]
 800879e:	61bb      	str	r3, [r7, #24]
 80087a0:	687b      	ldr	r3, [r7, #4]
 80087a2:	685b      	ldr	r3, [r3, #4]
 80087a4:	69ba      	ldr	r2, [r7, #24]
 80087a6:	1ad3      	subs	r3, r2, r3
 80087a8:	617b      	str	r3, [r7, #20]
 80087aa:	683b      	ldr	r3, [r7, #0]
 80087ac:	681b      	ldr	r3, [r3, #0]
 80087ae:	f1b3 3fff 	cmp.w	r3, #4294967295
 80087b2:	d102      	bne.n	80087ba <xTaskCheckForTimeOut+0x62>
 80087b4:	2300      	movs	r3, #0
 80087b6:	61fb      	str	r3, [r7, #28]
 80087b8:	e023      	b.n	8008802 <xTaskCheckForTimeOut+0xaa>
 80087ba:	687b      	ldr	r3, [r7, #4]
 80087bc:	681a      	ldr	r2, [r3, #0]
 80087be:	4b15      	ldr	r3, [pc, #84]	; (8008814 <xTaskCheckForTimeOut+0xbc>)
 80087c0:	681b      	ldr	r3, [r3, #0]
 80087c2:	429a      	cmp	r2, r3
 80087c4:	d007      	beq.n	80087d6 <xTaskCheckForTimeOut+0x7e>
 80087c6:	687b      	ldr	r3, [r7, #4]
 80087c8:	685b      	ldr	r3, [r3, #4]
 80087ca:	69ba      	ldr	r2, [r7, #24]
 80087cc:	429a      	cmp	r2, r3
 80087ce:	d302      	bcc.n	80087d6 <xTaskCheckForTimeOut+0x7e>
 80087d0:	2301      	movs	r3, #1
 80087d2:	61fb      	str	r3, [r7, #28]
 80087d4:	e015      	b.n	8008802 <xTaskCheckForTimeOut+0xaa>
 80087d6:	683b      	ldr	r3, [r7, #0]
 80087d8:	681b      	ldr	r3, [r3, #0]
 80087da:	697a      	ldr	r2, [r7, #20]
 80087dc:	429a      	cmp	r2, r3
 80087de:	d20b      	bcs.n	80087f8 <xTaskCheckForTimeOut+0xa0>
 80087e0:	683b      	ldr	r3, [r7, #0]
 80087e2:	681a      	ldr	r2, [r3, #0]
 80087e4:	697b      	ldr	r3, [r7, #20]
 80087e6:	1ad2      	subs	r2, r2, r3
 80087e8:	683b      	ldr	r3, [r7, #0]
 80087ea:	601a      	str	r2, [r3, #0]
 80087ec:	6878      	ldr	r0, [r7, #4]
 80087ee:	f7ff ff9d 	bl	800872c <vTaskInternalSetTimeOutState>
 80087f2:	2300      	movs	r3, #0
 80087f4:	61fb      	str	r3, [r7, #28]
 80087f6:	e004      	b.n	8008802 <xTaskCheckForTimeOut+0xaa>
 80087f8:	683b      	ldr	r3, [r7, #0]
 80087fa:	2200      	movs	r2, #0
 80087fc:	601a      	str	r2, [r3, #0]
 80087fe:	2301      	movs	r3, #1
 8008800:	61fb      	str	r3, [r7, #28]
 8008802:	f001 f911 	bl	8009a28 <vPortExitCritical>
 8008806:	69fb      	ldr	r3, [r7, #28]
 8008808:	4618      	mov	r0, r3
 800880a:	3720      	adds	r7, #32
 800880c:	46bd      	mov	sp, r7
 800880e:	bd80      	pop	{r7, pc}
 8008810:	20000cd4 	.word	0x20000cd4
 8008814:	20000ce8 	.word	0x20000ce8

08008818 <vTaskMissedYield>:
 8008818:	b480      	push	{r7}
 800881a:	af00      	add	r7, sp, #0
 800881c:	4b03      	ldr	r3, [pc, #12]	; (800882c <vTaskMissedYield+0x14>)
 800881e:	2201      	movs	r2, #1
 8008820:	601a      	str	r2, [r3, #0]
 8008822:	bf00      	nop
 8008824:	46bd      	mov	sp, r7
 8008826:	f85d 7b04 	ldr.w	r7, [sp], #4
 800882a:	4770      	bx	lr
 800882c:	20000ce4 	.word	0x20000ce4

08008830 <prvIdleTask>:
 8008830:	b580      	push	{r7, lr}
 8008832:	b082      	sub	sp, #8
 8008834:	af00      	add	r7, sp, #0
 8008836:	6078      	str	r0, [r7, #4]
 8008838:	f000 f854 	bl	80088e4 <prvCheckTasksWaitingTermination>
 800883c:	4b07      	ldr	r3, [pc, #28]	; (800885c <prvIdleTask+0x2c>)
 800883e:	681b      	ldr	r3, [r3, #0]
 8008840:	2b01      	cmp	r3, #1
 8008842:	d907      	bls.n	8008854 <prvIdleTask+0x24>
 8008844:	4b06      	ldr	r3, [pc, #24]	; (8008860 <prvIdleTask+0x30>)
 8008846:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800884a:	601a      	str	r2, [r3, #0]
 800884c:	f3bf 8f4f 	dsb	sy
 8008850:	f3bf 8f6f 	isb	sy
 8008854:	f7fc f813 	bl	800487e <vApplicationIdleHook>
 8008858:	e7ee      	b.n	8008838 <prvIdleTask+0x8>
 800885a:	bf00      	nop
 800885c:	20000bfc 	.word	0x20000bfc
 8008860:	e000ed04 	.word	0xe000ed04

08008864 <prvInitialiseTaskLists>:
 8008864:	b580      	push	{r7, lr}
 8008866:	b082      	sub	sp, #8
 8008868:	af00      	add	r7, sp, #0
 800886a:	2300      	movs	r3, #0
 800886c:	607b      	str	r3, [r7, #4]
 800886e:	e00c      	b.n	800888a <prvInitialiseTaskLists+0x26>
 8008870:	687a      	ldr	r2, [r7, #4]
 8008872:	4613      	mov	r3, r2
 8008874:	009b      	lsls	r3, r3, #2
 8008876:	4413      	add	r3, r2
 8008878:	009b      	lsls	r3, r3, #2
 800887a:	4a12      	ldr	r2, [pc, #72]	; (80088c4 <prvInitialiseTaskLists+0x60>)
 800887c:	4413      	add	r3, r2
 800887e:	4618      	mov	r0, r3
 8008880:	f7fe fb73 	bl	8006f6a <vListInitialise>
 8008884:	687b      	ldr	r3, [r7, #4]
 8008886:	3301      	adds	r3, #1
 8008888:	607b      	str	r3, [r7, #4]
 800888a:	687b      	ldr	r3, [r7, #4]
 800888c:	2b04      	cmp	r3, #4
 800888e:	d9ef      	bls.n	8008870 <prvInitialiseTaskLists+0xc>
 8008890:	480d      	ldr	r0, [pc, #52]	; (80088c8 <prvInitialiseTaskLists+0x64>)
 8008892:	f7fe fb6a 	bl	8006f6a <vListInitialise>
 8008896:	480d      	ldr	r0, [pc, #52]	; (80088cc <prvInitialiseTaskLists+0x68>)
 8008898:	f7fe fb67 	bl	8006f6a <vListInitialise>
 800889c:	480c      	ldr	r0, [pc, #48]	; (80088d0 <prvInitialiseTaskLists+0x6c>)
 800889e:	f7fe fb64 	bl	8006f6a <vListInitialise>
 80088a2:	480c      	ldr	r0, [pc, #48]	; (80088d4 <prvInitialiseTaskLists+0x70>)
 80088a4:	f7fe fb61 	bl	8006f6a <vListInitialise>
 80088a8:	480b      	ldr	r0, [pc, #44]	; (80088d8 <prvInitialiseTaskLists+0x74>)
 80088aa:	f7fe fb5e 	bl	8006f6a <vListInitialise>
 80088ae:	4b0b      	ldr	r3, [pc, #44]	; (80088dc <prvInitialiseTaskLists+0x78>)
 80088b0:	4a05      	ldr	r2, [pc, #20]	; (80088c8 <prvInitialiseTaskLists+0x64>)
 80088b2:	601a      	str	r2, [r3, #0]
 80088b4:	4b0a      	ldr	r3, [pc, #40]	; (80088e0 <prvInitialiseTaskLists+0x7c>)
 80088b6:	4a05      	ldr	r2, [pc, #20]	; (80088cc <prvInitialiseTaskLists+0x68>)
 80088b8:	601a      	str	r2, [r3, #0]
 80088ba:	bf00      	nop
 80088bc:	3708      	adds	r7, #8
 80088be:	46bd      	mov	sp, r7
 80088c0:	bd80      	pop	{r7, pc}
 80088c2:	bf00      	nop
 80088c4:	20000bfc 	.word	0x20000bfc
 80088c8:	20000c60 	.word	0x20000c60
 80088cc:	20000c74 	.word	0x20000c74
 80088d0:	20000c90 	.word	0x20000c90
 80088d4:	20000ca4 	.word	0x20000ca4
 80088d8:	20000cbc 	.word	0x20000cbc
 80088dc:	20000c88 	.word	0x20000c88
 80088e0:	20000c8c 	.word	0x20000c8c

080088e4 <prvCheckTasksWaitingTermination>:
 80088e4:	b580      	push	{r7, lr}
 80088e6:	b082      	sub	sp, #8
 80088e8:	af00      	add	r7, sp, #0
 80088ea:	e019      	b.n	8008920 <prvCheckTasksWaitingTermination+0x3c>
 80088ec:	f001 f86e 	bl	80099cc <vPortEnterCritical>
 80088f0:	4b0f      	ldr	r3, [pc, #60]	; (8008930 <prvCheckTasksWaitingTermination+0x4c>)
 80088f2:	68db      	ldr	r3, [r3, #12]
 80088f4:	68db      	ldr	r3, [r3, #12]
 80088f6:	607b      	str	r3, [r7, #4]
 80088f8:	687b      	ldr	r3, [r7, #4]
 80088fa:	3304      	adds	r3, #4
 80088fc:	4618      	mov	r0, r3
 80088fe:	f7fe fbbe 	bl	800707e <uxListRemove>
 8008902:	4b0c      	ldr	r3, [pc, #48]	; (8008934 <prvCheckTasksWaitingTermination+0x50>)
 8008904:	681b      	ldr	r3, [r3, #0]
 8008906:	3b01      	subs	r3, #1
 8008908:	4a0a      	ldr	r2, [pc, #40]	; (8008934 <prvCheckTasksWaitingTermination+0x50>)
 800890a:	6013      	str	r3, [r2, #0]
 800890c:	4b0a      	ldr	r3, [pc, #40]	; (8008938 <prvCheckTasksWaitingTermination+0x54>)
 800890e:	681b      	ldr	r3, [r3, #0]
 8008910:	3b01      	subs	r3, #1
 8008912:	4a09      	ldr	r2, [pc, #36]	; (8008938 <prvCheckTasksWaitingTermination+0x54>)
 8008914:	6013      	str	r3, [r2, #0]
 8008916:	f001 f887 	bl	8009a28 <vPortExitCritical>
 800891a:	6878      	ldr	r0, [r7, #4]
 800891c:	f000 f80e 	bl	800893c <prvDeleteTCB>
 8008920:	4b05      	ldr	r3, [pc, #20]	; (8008938 <prvCheckTasksWaitingTermination+0x54>)
 8008922:	681b      	ldr	r3, [r3, #0]
 8008924:	2b00      	cmp	r3, #0
 8008926:	d1e1      	bne.n	80088ec <prvCheckTasksWaitingTermination+0x8>
 8008928:	bf00      	nop
 800892a:	3708      	adds	r7, #8
 800892c:	46bd      	mov	sp, r7
 800892e:	bd80      	pop	{r7, pc}
 8008930:	20000ca4 	.word	0x20000ca4
 8008934:	20000cd0 	.word	0x20000cd0
 8008938:	20000cb8 	.word	0x20000cb8

0800893c <prvDeleteTCB>:
 800893c:	b580      	push	{r7, lr}
 800893e:	b084      	sub	sp, #16
 8008940:	af00      	add	r7, sp, #0
 8008942:	6078      	str	r0, [r7, #4]
 8008944:	687b      	ldr	r3, [r7, #4]
 8008946:	3350      	adds	r3, #80	; 0x50
 8008948:	4618      	mov	r0, r3
 800894a:	f002 fb35 	bl	800afb8 <_reclaim_reent>
 800894e:	687b      	ldr	r3, [r7, #4]
 8008950:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 8008954:	2b00      	cmp	r3, #0
 8008956:	d108      	bne.n	800896a <prvDeleteTCB+0x2e>
 8008958:	687b      	ldr	r3, [r7, #4]
 800895a:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 800895c:	4618      	mov	r0, r3
 800895e:	f001 fa15 	bl	8009d8c <vPortFree>
 8008962:	6878      	ldr	r0, [r7, #4]
 8008964:	f001 fa12 	bl	8009d8c <vPortFree>
 8008968:	e017      	b.n	800899a <prvDeleteTCB+0x5e>
 800896a:	687b      	ldr	r3, [r7, #4]
 800896c:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 8008970:	2b01      	cmp	r3, #1
 8008972:	d103      	bne.n	800897c <prvDeleteTCB+0x40>
 8008974:	6878      	ldr	r0, [r7, #4]
 8008976:	f001 fa09 	bl	8009d8c <vPortFree>
 800897a:	e00e      	b.n	800899a <prvDeleteTCB+0x5e>
 800897c:	687b      	ldr	r3, [r7, #4]
 800897e:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 8008982:	2b02      	cmp	r3, #2
 8008984:	d009      	beq.n	800899a <prvDeleteTCB+0x5e>
 8008986:	f04f 0350 	mov.w	r3, #80	; 0x50
 800898a:	f383 8811 	msr	BASEPRI, r3
 800898e:	f3bf 8f6f 	isb	sy
 8008992:	f3bf 8f4f 	dsb	sy
 8008996:	60fb      	str	r3, [r7, #12]
 8008998:	e7fe      	b.n	8008998 <prvDeleteTCB+0x5c>
 800899a:	bf00      	nop
 800899c:	3710      	adds	r7, #16
 800899e:	46bd      	mov	sp, r7
 80089a0:	bd80      	pop	{r7, pc}
	...

080089a4 <prvResetNextTaskUnblockTime>:
 80089a4:	b480      	push	{r7}
 80089a6:	b083      	sub	sp, #12
 80089a8:	af00      	add	r7, sp, #0
 80089aa:	4b0c      	ldr	r3, [pc, #48]	; (80089dc <prvResetNextTaskUnblockTime+0x38>)
 80089ac:	681b      	ldr	r3, [r3, #0]
 80089ae:	681b      	ldr	r3, [r3, #0]
 80089b0:	2b00      	cmp	r3, #0
 80089b2:	d104      	bne.n	80089be <prvResetNextTaskUnblockTime+0x1a>
 80089b4:	4b0a      	ldr	r3, [pc, #40]	; (80089e0 <prvResetNextTaskUnblockTime+0x3c>)
 80089b6:	f04f 32ff 	mov.w	r2, #4294967295
 80089ba:	601a      	str	r2, [r3, #0]
 80089bc:	e008      	b.n	80089d0 <prvResetNextTaskUnblockTime+0x2c>
 80089be:	4b07      	ldr	r3, [pc, #28]	; (80089dc <prvResetNextTaskUnblockTime+0x38>)
 80089c0:	681b      	ldr	r3, [r3, #0]
 80089c2:	68db      	ldr	r3, [r3, #12]
 80089c4:	68db      	ldr	r3, [r3, #12]
 80089c6:	607b      	str	r3, [r7, #4]
 80089c8:	687b      	ldr	r3, [r7, #4]
 80089ca:	685b      	ldr	r3, [r3, #4]
 80089cc:	4a04      	ldr	r2, [pc, #16]	; (80089e0 <prvResetNextTaskUnblockTime+0x3c>)
 80089ce:	6013      	str	r3, [r2, #0]
 80089d0:	bf00      	nop
 80089d2:	370c      	adds	r7, #12
 80089d4:	46bd      	mov	sp, r7
 80089d6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80089da:	4770      	bx	lr
 80089dc:	20000c88 	.word	0x20000c88
 80089e0:	20000cf0 	.word	0x20000cf0

080089e4 <xTaskGetCurrentTaskHandle>:
 80089e4:	b480      	push	{r7}
 80089e6:	b083      	sub	sp, #12
 80089e8:	af00      	add	r7, sp, #0
 80089ea:	4b05      	ldr	r3, [pc, #20]	; (8008a00 <xTaskGetCurrentTaskHandle+0x1c>)
 80089ec:	681b      	ldr	r3, [r3, #0]
 80089ee:	607b      	str	r3, [r7, #4]
 80089f0:	687b      	ldr	r3, [r7, #4]
 80089f2:	4618      	mov	r0, r3
 80089f4:	370c      	adds	r7, #12
 80089f6:	46bd      	mov	sp, r7
 80089f8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80089fc:	4770      	bx	lr
 80089fe:	bf00      	nop
 8008a00:	20000bf8 	.word	0x20000bf8

08008a04 <xTaskGetSchedulerState>:
 8008a04:	b480      	push	{r7}
 8008a06:	b083      	sub	sp, #12
 8008a08:	af00      	add	r7, sp, #0
 8008a0a:	4b0b      	ldr	r3, [pc, #44]	; (8008a38 <xTaskGetSchedulerState+0x34>)
 8008a0c:	681b      	ldr	r3, [r3, #0]
 8008a0e:	2b00      	cmp	r3, #0
 8008a10:	d102      	bne.n	8008a18 <xTaskGetSchedulerState+0x14>
 8008a12:	2301      	movs	r3, #1
 8008a14:	607b      	str	r3, [r7, #4]
 8008a16:	e008      	b.n	8008a2a <xTaskGetSchedulerState+0x26>
 8008a18:	4b08      	ldr	r3, [pc, #32]	; (8008a3c <xTaskGetSchedulerState+0x38>)
 8008a1a:	681b      	ldr	r3, [r3, #0]
 8008a1c:	2b00      	cmp	r3, #0
 8008a1e:	d102      	bne.n	8008a26 <xTaskGetSchedulerState+0x22>
 8008a20:	2302      	movs	r3, #2
 8008a22:	607b      	str	r3, [r7, #4]
 8008a24:	e001      	b.n	8008a2a <xTaskGetSchedulerState+0x26>
 8008a26:	2300      	movs	r3, #0
 8008a28:	607b      	str	r3, [r7, #4]
 8008a2a:	687b      	ldr	r3, [r7, #4]
 8008a2c:	4618      	mov	r0, r3
 8008a2e:	370c      	adds	r7, #12
 8008a30:	46bd      	mov	sp, r7
 8008a32:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008a36:	4770      	bx	lr
 8008a38:	20000cdc 	.word	0x20000cdc
 8008a3c:	20000cf8 	.word	0x20000cf8

08008a40 <xTaskPriorityInherit>:
 8008a40:	b580      	push	{r7, lr}
 8008a42:	b084      	sub	sp, #16
 8008a44:	af00      	add	r7, sp, #0
 8008a46:	6078      	str	r0, [r7, #4]
 8008a48:	687b      	ldr	r3, [r7, #4]
 8008a4a:	60bb      	str	r3, [r7, #8]
 8008a4c:	2300      	movs	r3, #0
 8008a4e:	60fb      	str	r3, [r7, #12]
 8008a50:	687b      	ldr	r3, [r7, #4]
 8008a52:	2b00      	cmp	r3, #0
 8008a54:	d069      	beq.n	8008b2a <xTaskPriorityInherit+0xea>
 8008a56:	68bb      	ldr	r3, [r7, #8]
 8008a58:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008a5a:	4b36      	ldr	r3, [pc, #216]	; (8008b34 <xTaskPriorityInherit+0xf4>)
 8008a5c:	681b      	ldr	r3, [r3, #0]
 8008a5e:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008a60:	429a      	cmp	r2, r3
 8008a62:	d259      	bcs.n	8008b18 <xTaskPriorityInherit+0xd8>
 8008a64:	68bb      	ldr	r3, [r7, #8]
 8008a66:	699b      	ldr	r3, [r3, #24]
 8008a68:	2b00      	cmp	r3, #0
 8008a6a:	db06      	blt.n	8008a7a <xTaskPriorityInherit+0x3a>
 8008a6c:	4b31      	ldr	r3, [pc, #196]	; (8008b34 <xTaskPriorityInherit+0xf4>)
 8008a6e:	681b      	ldr	r3, [r3, #0]
 8008a70:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008a72:	f1c3 0205 	rsb	r2, r3, #5
 8008a76:	68bb      	ldr	r3, [r7, #8]
 8008a78:	619a      	str	r2, [r3, #24]
 8008a7a:	68bb      	ldr	r3, [r7, #8]
 8008a7c:	6959      	ldr	r1, [r3, #20]
 8008a7e:	68bb      	ldr	r3, [r7, #8]
 8008a80:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008a82:	4613      	mov	r3, r2
 8008a84:	009b      	lsls	r3, r3, #2
 8008a86:	4413      	add	r3, r2
 8008a88:	009b      	lsls	r3, r3, #2
 8008a8a:	4a2b      	ldr	r2, [pc, #172]	; (8008b38 <xTaskPriorityInherit+0xf8>)
 8008a8c:	4413      	add	r3, r2
 8008a8e:	4299      	cmp	r1, r3
 8008a90:	d13a      	bne.n	8008b08 <xTaskPriorityInherit+0xc8>
 8008a92:	68bb      	ldr	r3, [r7, #8]
 8008a94:	3304      	adds	r3, #4
 8008a96:	4618      	mov	r0, r3
 8008a98:	f7fe faf1 	bl	800707e <uxListRemove>
 8008a9c:	4603      	mov	r3, r0
 8008a9e:	2b00      	cmp	r3, #0
 8008aa0:	d115      	bne.n	8008ace <xTaskPriorityInherit+0x8e>
 8008aa2:	68bb      	ldr	r3, [r7, #8]
 8008aa4:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008aa6:	4924      	ldr	r1, [pc, #144]	; (8008b38 <xTaskPriorityInherit+0xf8>)
 8008aa8:	4613      	mov	r3, r2
 8008aaa:	009b      	lsls	r3, r3, #2
 8008aac:	4413      	add	r3, r2
 8008aae:	009b      	lsls	r3, r3, #2
 8008ab0:	440b      	add	r3, r1
 8008ab2:	681b      	ldr	r3, [r3, #0]
 8008ab4:	2b00      	cmp	r3, #0
 8008ab6:	d10a      	bne.n	8008ace <xTaskPriorityInherit+0x8e>
 8008ab8:	68bb      	ldr	r3, [r7, #8]
 8008aba:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008abc:	2201      	movs	r2, #1
 8008abe:	fa02 f303 	lsl.w	r3, r2, r3
 8008ac2:	43da      	mvns	r2, r3
 8008ac4:	4b1d      	ldr	r3, [pc, #116]	; (8008b3c <xTaskPriorityInherit+0xfc>)
 8008ac6:	681b      	ldr	r3, [r3, #0]
 8008ac8:	4013      	ands	r3, r2
 8008aca:	4a1c      	ldr	r2, [pc, #112]	; (8008b3c <xTaskPriorityInherit+0xfc>)
 8008acc:	6013      	str	r3, [r2, #0]
 8008ace:	4b19      	ldr	r3, [pc, #100]	; (8008b34 <xTaskPriorityInherit+0xf4>)
 8008ad0:	681b      	ldr	r3, [r3, #0]
 8008ad2:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008ad4:	68bb      	ldr	r3, [r7, #8]
 8008ad6:	62da      	str	r2, [r3, #44]	; 0x2c
 8008ad8:	68bb      	ldr	r3, [r7, #8]
 8008ada:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008adc:	2201      	movs	r2, #1
 8008ade:	409a      	lsls	r2, r3
 8008ae0:	4b16      	ldr	r3, [pc, #88]	; (8008b3c <xTaskPriorityInherit+0xfc>)
 8008ae2:	681b      	ldr	r3, [r3, #0]
 8008ae4:	4313      	orrs	r3, r2
 8008ae6:	4a15      	ldr	r2, [pc, #84]	; (8008b3c <xTaskPriorityInherit+0xfc>)
 8008ae8:	6013      	str	r3, [r2, #0]
 8008aea:	68bb      	ldr	r3, [r7, #8]
 8008aec:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008aee:	4613      	mov	r3, r2
 8008af0:	009b      	lsls	r3, r3, #2
 8008af2:	4413      	add	r3, r2
 8008af4:	009b      	lsls	r3, r3, #2
 8008af6:	4a10      	ldr	r2, [pc, #64]	; (8008b38 <xTaskPriorityInherit+0xf8>)
 8008af8:	441a      	add	r2, r3
 8008afa:	68bb      	ldr	r3, [r7, #8]
 8008afc:	3304      	adds	r3, #4
 8008afe:	4619      	mov	r1, r3
 8008b00:	4610      	mov	r0, r2
 8008b02:	f7fe fa5f 	bl	8006fc4 <vListInsertEnd>
 8008b06:	e004      	b.n	8008b12 <xTaskPriorityInherit+0xd2>
 8008b08:	4b0a      	ldr	r3, [pc, #40]	; (8008b34 <xTaskPriorityInherit+0xf4>)
 8008b0a:	681b      	ldr	r3, [r3, #0]
 8008b0c:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008b0e:	68bb      	ldr	r3, [r7, #8]
 8008b10:	62da      	str	r2, [r3, #44]	; 0x2c
 8008b12:	2301      	movs	r3, #1
 8008b14:	60fb      	str	r3, [r7, #12]
 8008b16:	e008      	b.n	8008b2a <xTaskPriorityInherit+0xea>
 8008b18:	68bb      	ldr	r3, [r7, #8]
 8008b1a:	6c9a      	ldr	r2, [r3, #72]	; 0x48
 8008b1c:	4b05      	ldr	r3, [pc, #20]	; (8008b34 <xTaskPriorityInherit+0xf4>)
 8008b1e:	681b      	ldr	r3, [r3, #0]
 8008b20:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008b22:	429a      	cmp	r2, r3
 8008b24:	d201      	bcs.n	8008b2a <xTaskPriorityInherit+0xea>
 8008b26:	2301      	movs	r3, #1
 8008b28:	60fb      	str	r3, [r7, #12]
 8008b2a:	68fb      	ldr	r3, [r7, #12]
 8008b2c:	4618      	mov	r0, r3
 8008b2e:	3710      	adds	r7, #16
 8008b30:	46bd      	mov	sp, r7
 8008b32:	bd80      	pop	{r7, pc}
 8008b34:	20000bf8 	.word	0x20000bf8
 8008b38:	20000bfc 	.word	0x20000bfc
 8008b3c:	20000cd8 	.word	0x20000cd8

08008b40 <xTaskPriorityDisinherit>:
 8008b40:	b580      	push	{r7, lr}
 8008b42:	b086      	sub	sp, #24
 8008b44:	af00      	add	r7, sp, #0
 8008b46:	6078      	str	r0, [r7, #4]
 8008b48:	687b      	ldr	r3, [r7, #4]
 8008b4a:	613b      	str	r3, [r7, #16]
 8008b4c:	2300      	movs	r3, #0
 8008b4e:	617b      	str	r3, [r7, #20]
 8008b50:	687b      	ldr	r3, [r7, #4]
 8008b52:	2b00      	cmp	r3, #0
 8008b54:	d06c      	beq.n	8008c30 <xTaskPriorityDisinherit+0xf0>
 8008b56:	4b39      	ldr	r3, [pc, #228]	; (8008c3c <xTaskPriorityDisinherit+0xfc>)
 8008b58:	681b      	ldr	r3, [r3, #0]
 8008b5a:	693a      	ldr	r2, [r7, #16]
 8008b5c:	429a      	cmp	r2, r3
 8008b5e:	d009      	beq.n	8008b74 <xTaskPriorityDisinherit+0x34>
 8008b60:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008b64:	f383 8811 	msr	BASEPRI, r3
 8008b68:	f3bf 8f6f 	isb	sy
 8008b6c:	f3bf 8f4f 	dsb	sy
 8008b70:	60fb      	str	r3, [r7, #12]
 8008b72:	e7fe      	b.n	8008b72 <xTaskPriorityDisinherit+0x32>
 8008b74:	693b      	ldr	r3, [r7, #16]
 8008b76:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008b78:	2b00      	cmp	r3, #0
 8008b7a:	d109      	bne.n	8008b90 <xTaskPriorityDisinherit+0x50>
 8008b7c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008b80:	f383 8811 	msr	BASEPRI, r3
 8008b84:	f3bf 8f6f 	isb	sy
 8008b88:	f3bf 8f4f 	dsb	sy
 8008b8c:	60bb      	str	r3, [r7, #8]
 8008b8e:	e7fe      	b.n	8008b8e <xTaskPriorityDisinherit+0x4e>
 8008b90:	693b      	ldr	r3, [r7, #16]
 8008b92:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008b94:	1e5a      	subs	r2, r3, #1
 8008b96:	693b      	ldr	r3, [r7, #16]
 8008b98:	64da      	str	r2, [r3, #76]	; 0x4c
 8008b9a:	693b      	ldr	r3, [r7, #16]
 8008b9c:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008b9e:	693b      	ldr	r3, [r7, #16]
 8008ba0:	6c9b      	ldr	r3, [r3, #72]	; 0x48
 8008ba2:	429a      	cmp	r2, r3
 8008ba4:	d044      	beq.n	8008c30 <xTaskPriorityDisinherit+0xf0>
 8008ba6:	693b      	ldr	r3, [r7, #16]
 8008ba8:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008baa:	2b00      	cmp	r3, #0
 8008bac:	d140      	bne.n	8008c30 <xTaskPriorityDisinherit+0xf0>
 8008bae:	693b      	ldr	r3, [r7, #16]
 8008bb0:	3304      	adds	r3, #4
 8008bb2:	4618      	mov	r0, r3
 8008bb4:	f7fe fa63 	bl	800707e <uxListRemove>
 8008bb8:	4603      	mov	r3, r0
 8008bba:	2b00      	cmp	r3, #0
 8008bbc:	d115      	bne.n	8008bea <xTaskPriorityDisinherit+0xaa>
 8008bbe:	693b      	ldr	r3, [r7, #16]
 8008bc0:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008bc2:	491f      	ldr	r1, [pc, #124]	; (8008c40 <xTaskPriorityDisinherit+0x100>)
 8008bc4:	4613      	mov	r3, r2
 8008bc6:	009b      	lsls	r3, r3, #2
 8008bc8:	4413      	add	r3, r2
 8008bca:	009b      	lsls	r3, r3, #2
 8008bcc:	440b      	add	r3, r1
 8008bce:	681b      	ldr	r3, [r3, #0]
 8008bd0:	2b00      	cmp	r3, #0
 8008bd2:	d10a      	bne.n	8008bea <xTaskPriorityDisinherit+0xaa>
 8008bd4:	693b      	ldr	r3, [r7, #16]
 8008bd6:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008bd8:	2201      	movs	r2, #1
 8008bda:	fa02 f303 	lsl.w	r3, r2, r3
 8008bde:	43da      	mvns	r2, r3
 8008be0:	4b18      	ldr	r3, [pc, #96]	; (8008c44 <xTaskPriorityDisinherit+0x104>)
 8008be2:	681b      	ldr	r3, [r3, #0]
 8008be4:	4013      	ands	r3, r2
 8008be6:	4a17      	ldr	r2, [pc, #92]	; (8008c44 <xTaskPriorityDisinherit+0x104>)
 8008be8:	6013      	str	r3, [r2, #0]
 8008bea:	693b      	ldr	r3, [r7, #16]
 8008bec:	6c9a      	ldr	r2, [r3, #72]	; 0x48
 8008bee:	693b      	ldr	r3, [r7, #16]
 8008bf0:	62da      	str	r2, [r3, #44]	; 0x2c
 8008bf2:	693b      	ldr	r3, [r7, #16]
 8008bf4:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008bf6:	f1c3 0205 	rsb	r2, r3, #5
 8008bfa:	693b      	ldr	r3, [r7, #16]
 8008bfc:	619a      	str	r2, [r3, #24]
 8008bfe:	693b      	ldr	r3, [r7, #16]
 8008c00:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008c02:	2201      	movs	r2, #1
 8008c04:	409a      	lsls	r2, r3
 8008c06:	4b0f      	ldr	r3, [pc, #60]	; (8008c44 <xTaskPriorityDisinherit+0x104>)
 8008c08:	681b      	ldr	r3, [r3, #0]
 8008c0a:	4313      	orrs	r3, r2
 8008c0c:	4a0d      	ldr	r2, [pc, #52]	; (8008c44 <xTaskPriorityDisinherit+0x104>)
 8008c0e:	6013      	str	r3, [r2, #0]
 8008c10:	693b      	ldr	r3, [r7, #16]
 8008c12:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008c14:	4613      	mov	r3, r2
 8008c16:	009b      	lsls	r3, r3, #2
 8008c18:	4413      	add	r3, r2
 8008c1a:	009b      	lsls	r3, r3, #2
 8008c1c:	4a08      	ldr	r2, [pc, #32]	; (8008c40 <xTaskPriorityDisinherit+0x100>)
 8008c1e:	441a      	add	r2, r3
 8008c20:	693b      	ldr	r3, [r7, #16]
 8008c22:	3304      	adds	r3, #4
 8008c24:	4619      	mov	r1, r3
 8008c26:	4610      	mov	r0, r2
 8008c28:	f7fe f9cc 	bl	8006fc4 <vListInsertEnd>
 8008c2c:	2301      	movs	r3, #1
 8008c2e:	617b      	str	r3, [r7, #20]
 8008c30:	697b      	ldr	r3, [r7, #20]
 8008c32:	4618      	mov	r0, r3
 8008c34:	3718      	adds	r7, #24
 8008c36:	46bd      	mov	sp, r7
 8008c38:	bd80      	pop	{r7, pc}
 8008c3a:	bf00      	nop
 8008c3c:	20000bf8 	.word	0x20000bf8
 8008c40:	20000bfc 	.word	0x20000bfc
 8008c44:	20000cd8 	.word	0x20000cd8

08008c48 <vTaskPriorityDisinheritAfterTimeout>:
 8008c48:	b580      	push	{r7, lr}
 8008c4a:	b088      	sub	sp, #32
 8008c4c:	af00      	add	r7, sp, #0
 8008c4e:	6078      	str	r0, [r7, #4]
 8008c50:	6039      	str	r1, [r7, #0]
 8008c52:	687b      	ldr	r3, [r7, #4]
 8008c54:	61bb      	str	r3, [r7, #24]
 8008c56:	2301      	movs	r3, #1
 8008c58:	617b      	str	r3, [r7, #20]
 8008c5a:	687b      	ldr	r3, [r7, #4]
 8008c5c:	2b00      	cmp	r3, #0
 8008c5e:	f000 8081 	beq.w	8008d64 <vTaskPriorityDisinheritAfterTimeout+0x11c>
 8008c62:	69bb      	ldr	r3, [r7, #24]
 8008c64:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008c66:	2b00      	cmp	r3, #0
 8008c68:	d109      	bne.n	8008c7e <vTaskPriorityDisinheritAfterTimeout+0x36>
 8008c6a:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008c6e:	f383 8811 	msr	BASEPRI, r3
 8008c72:	f3bf 8f6f 	isb	sy
 8008c76:	f3bf 8f4f 	dsb	sy
 8008c7a:	60fb      	str	r3, [r7, #12]
 8008c7c:	e7fe      	b.n	8008c7c <vTaskPriorityDisinheritAfterTimeout+0x34>
 8008c7e:	69bb      	ldr	r3, [r7, #24]
 8008c80:	6c9b      	ldr	r3, [r3, #72]	; 0x48
 8008c82:	683a      	ldr	r2, [r7, #0]
 8008c84:	429a      	cmp	r2, r3
 8008c86:	d902      	bls.n	8008c8e <vTaskPriorityDisinheritAfterTimeout+0x46>
 8008c88:	683b      	ldr	r3, [r7, #0]
 8008c8a:	61fb      	str	r3, [r7, #28]
 8008c8c:	e002      	b.n	8008c94 <vTaskPriorityDisinheritAfterTimeout+0x4c>
 8008c8e:	69bb      	ldr	r3, [r7, #24]
 8008c90:	6c9b      	ldr	r3, [r3, #72]	; 0x48
 8008c92:	61fb      	str	r3, [r7, #28]
 8008c94:	69bb      	ldr	r3, [r7, #24]
 8008c96:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008c98:	69fa      	ldr	r2, [r7, #28]
 8008c9a:	429a      	cmp	r2, r3
 8008c9c:	d062      	beq.n	8008d64 <vTaskPriorityDisinheritAfterTimeout+0x11c>
 8008c9e:	69bb      	ldr	r3, [r7, #24]
 8008ca0:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008ca2:	697a      	ldr	r2, [r7, #20]
 8008ca4:	429a      	cmp	r2, r3
 8008ca6:	d15d      	bne.n	8008d64 <vTaskPriorityDisinheritAfterTimeout+0x11c>
 8008ca8:	4b30      	ldr	r3, [pc, #192]	; (8008d6c <vTaskPriorityDisinheritAfterTimeout+0x124>)
 8008caa:	681b      	ldr	r3, [r3, #0]
 8008cac:	69ba      	ldr	r2, [r7, #24]
 8008cae:	429a      	cmp	r2, r3
 8008cb0:	d109      	bne.n	8008cc6 <vTaskPriorityDisinheritAfterTimeout+0x7e>
 8008cb2:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008cb6:	f383 8811 	msr	BASEPRI, r3
 8008cba:	f3bf 8f6f 	isb	sy
 8008cbe:	f3bf 8f4f 	dsb	sy
 8008cc2:	60bb      	str	r3, [r7, #8]
 8008cc4:	e7fe      	b.n	8008cc4 <vTaskPriorityDisinheritAfterTimeout+0x7c>
 8008cc6:	69bb      	ldr	r3, [r7, #24]
 8008cc8:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008cca:	613b      	str	r3, [r7, #16]
 8008ccc:	69bb      	ldr	r3, [r7, #24]
 8008cce:	69fa      	ldr	r2, [r7, #28]
 8008cd0:	62da      	str	r2, [r3, #44]	; 0x2c
 8008cd2:	69bb      	ldr	r3, [r7, #24]
 8008cd4:	699b      	ldr	r3, [r3, #24]
 8008cd6:	2b00      	cmp	r3, #0
 8008cd8:	db04      	blt.n	8008ce4 <vTaskPriorityDisinheritAfterTimeout+0x9c>
 8008cda:	69fb      	ldr	r3, [r7, #28]
 8008cdc:	f1c3 0205 	rsb	r2, r3, #5
 8008ce0:	69bb      	ldr	r3, [r7, #24]
 8008ce2:	619a      	str	r2, [r3, #24]
 8008ce4:	69bb      	ldr	r3, [r7, #24]
 8008ce6:	6959      	ldr	r1, [r3, #20]
 8008ce8:	693a      	ldr	r2, [r7, #16]
 8008cea:	4613      	mov	r3, r2
 8008cec:	009b      	lsls	r3, r3, #2
 8008cee:	4413      	add	r3, r2
 8008cf0:	009b      	lsls	r3, r3, #2
 8008cf2:	4a1f      	ldr	r2, [pc, #124]	; (8008d70 <vTaskPriorityDisinheritAfterTimeout+0x128>)
 8008cf4:	4413      	add	r3, r2
 8008cf6:	4299      	cmp	r1, r3
 8008cf8:	d134      	bne.n	8008d64 <vTaskPriorityDisinheritAfterTimeout+0x11c>
 8008cfa:	69bb      	ldr	r3, [r7, #24]
 8008cfc:	3304      	adds	r3, #4
 8008cfe:	4618      	mov	r0, r3
 8008d00:	f7fe f9bd 	bl	800707e <uxListRemove>
 8008d04:	4603      	mov	r3, r0
 8008d06:	2b00      	cmp	r3, #0
 8008d08:	d115      	bne.n	8008d36 <vTaskPriorityDisinheritAfterTimeout+0xee>
 8008d0a:	69bb      	ldr	r3, [r7, #24]
 8008d0c:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008d0e:	4918      	ldr	r1, [pc, #96]	; (8008d70 <vTaskPriorityDisinheritAfterTimeout+0x128>)
 8008d10:	4613      	mov	r3, r2
 8008d12:	009b      	lsls	r3, r3, #2
 8008d14:	4413      	add	r3, r2
 8008d16:	009b      	lsls	r3, r3, #2
 8008d18:	440b      	add	r3, r1
 8008d1a:	681b      	ldr	r3, [r3, #0]
 8008d1c:	2b00      	cmp	r3, #0
 8008d1e:	d10a      	bne.n	8008d36 <vTaskPriorityDisinheritAfterTimeout+0xee>
 8008d20:	69bb      	ldr	r3, [r7, #24]
 8008d22:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008d24:	2201      	movs	r2, #1
 8008d26:	fa02 f303 	lsl.w	r3, r2, r3
 8008d2a:	43da      	mvns	r2, r3
 8008d2c:	4b11      	ldr	r3, [pc, #68]	; (8008d74 <vTaskPriorityDisinheritAfterTimeout+0x12c>)
 8008d2e:	681b      	ldr	r3, [r3, #0]
 8008d30:	4013      	ands	r3, r2
 8008d32:	4a10      	ldr	r2, [pc, #64]	; (8008d74 <vTaskPriorityDisinheritAfterTimeout+0x12c>)
 8008d34:	6013      	str	r3, [r2, #0]
 8008d36:	69bb      	ldr	r3, [r7, #24]
 8008d38:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008d3a:	2201      	movs	r2, #1
 8008d3c:	409a      	lsls	r2, r3
 8008d3e:	4b0d      	ldr	r3, [pc, #52]	; (8008d74 <vTaskPriorityDisinheritAfterTimeout+0x12c>)
 8008d40:	681b      	ldr	r3, [r3, #0]
 8008d42:	4313      	orrs	r3, r2
 8008d44:	4a0b      	ldr	r2, [pc, #44]	; (8008d74 <vTaskPriorityDisinheritAfterTimeout+0x12c>)
 8008d46:	6013      	str	r3, [r2, #0]
 8008d48:	69bb      	ldr	r3, [r7, #24]
 8008d4a:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008d4c:	4613      	mov	r3, r2
 8008d4e:	009b      	lsls	r3, r3, #2
 8008d50:	4413      	add	r3, r2
 8008d52:	009b      	lsls	r3, r3, #2
 8008d54:	4a06      	ldr	r2, [pc, #24]	; (8008d70 <vTaskPriorityDisinheritAfterTimeout+0x128>)
 8008d56:	441a      	add	r2, r3
 8008d58:	69bb      	ldr	r3, [r7, #24]
 8008d5a:	3304      	adds	r3, #4
 8008d5c:	4619      	mov	r1, r3
 8008d5e:	4610      	mov	r0, r2
 8008d60:	f7fe f930 	bl	8006fc4 <vListInsertEnd>
 8008d64:	bf00      	nop
 8008d66:	3720      	adds	r7, #32
 8008d68:	46bd      	mov	sp, r7
 8008d6a:	bd80      	pop	{r7, pc}
 8008d6c:	20000bf8 	.word	0x20000bf8
 8008d70:	20000bfc 	.word	0x20000bfc
 8008d74:	20000cd8 	.word	0x20000cd8

08008d78 <pvTaskIncrementMutexHeldCount>:
 8008d78:	b480      	push	{r7}
 8008d7a:	af00      	add	r7, sp, #0
 8008d7c:	4b07      	ldr	r3, [pc, #28]	; (8008d9c <pvTaskIncrementMutexHeldCount+0x24>)
 8008d7e:	681b      	ldr	r3, [r3, #0]
 8008d80:	2b00      	cmp	r3, #0
 8008d82:	d004      	beq.n	8008d8e <pvTaskIncrementMutexHeldCount+0x16>
 8008d84:	4b05      	ldr	r3, [pc, #20]	; (8008d9c <pvTaskIncrementMutexHeldCount+0x24>)
 8008d86:	681b      	ldr	r3, [r3, #0]
 8008d88:	6cda      	ldr	r2, [r3, #76]	; 0x4c
 8008d8a:	3201      	adds	r2, #1
 8008d8c:	64da      	str	r2, [r3, #76]	; 0x4c
 8008d8e:	4b03      	ldr	r3, [pc, #12]	; (8008d9c <pvTaskIncrementMutexHeldCount+0x24>)
 8008d90:	681b      	ldr	r3, [r3, #0]
 8008d92:	4618      	mov	r0, r3
 8008d94:	46bd      	mov	sp, r7
 8008d96:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008d9a:	4770      	bx	lr
 8008d9c:	20000bf8 	.word	0x20000bf8

08008da0 <ulTaskNotifyTake>:
 8008da0:	b580      	push	{r7, lr}
 8008da2:	b084      	sub	sp, #16
 8008da4:	af00      	add	r7, sp, #0
 8008da6:	6078      	str	r0, [r7, #4]
 8008da8:	6039      	str	r1, [r7, #0]
 8008daa:	f000 fe0f 	bl	80099cc <vPortEnterCritical>
 8008dae:	4b20      	ldr	r3, [pc, #128]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008db0:	681b      	ldr	r3, [r3, #0]
 8008db2:	f8d3 3478 	ldr.w	r3, [r3, #1144]	; 0x478
 8008db6:	2b00      	cmp	r3, #0
 8008db8:	d113      	bne.n	8008de2 <ulTaskNotifyTake+0x42>
 8008dba:	4b1d      	ldr	r3, [pc, #116]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008dbc:	681b      	ldr	r3, [r3, #0]
 8008dbe:	2201      	movs	r2, #1
 8008dc0:	f883 247c 	strb.w	r2, [r3, #1148]	; 0x47c
 8008dc4:	683b      	ldr	r3, [r7, #0]
 8008dc6:	2b00      	cmp	r3, #0
 8008dc8:	d00b      	beq.n	8008de2 <ulTaskNotifyTake+0x42>
 8008dca:	2101      	movs	r1, #1
 8008dcc:	6838      	ldr	r0, [r7, #0]
 8008dce:	f000 f97f 	bl	80090d0 <prvAddCurrentTaskToDelayedList>
 8008dd2:	4b18      	ldr	r3, [pc, #96]	; (8008e34 <ulTaskNotifyTake+0x94>)
 8008dd4:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8008dd8:	601a      	str	r2, [r3, #0]
 8008dda:	f3bf 8f4f 	dsb	sy
 8008dde:	f3bf 8f6f 	isb	sy
 8008de2:	f000 fe21 	bl	8009a28 <vPortExitCritical>
 8008de6:	f000 fdf1 	bl	80099cc <vPortEnterCritical>
 8008dea:	4b11      	ldr	r3, [pc, #68]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008dec:	681b      	ldr	r3, [r3, #0]
 8008dee:	f8d3 3478 	ldr.w	r3, [r3, #1144]	; 0x478
 8008df2:	60fb      	str	r3, [r7, #12]
 8008df4:	68fb      	ldr	r3, [r7, #12]
 8008df6:	2b00      	cmp	r3, #0
 8008df8:	d00e      	beq.n	8008e18 <ulTaskNotifyTake+0x78>
 8008dfa:	687b      	ldr	r3, [r7, #4]
 8008dfc:	2b00      	cmp	r3, #0
 8008dfe:	d005      	beq.n	8008e0c <ulTaskNotifyTake+0x6c>
 8008e00:	4b0b      	ldr	r3, [pc, #44]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008e02:	681b      	ldr	r3, [r3, #0]
 8008e04:	2200      	movs	r2, #0
 8008e06:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008e0a:	e005      	b.n	8008e18 <ulTaskNotifyTake+0x78>
 8008e0c:	4b08      	ldr	r3, [pc, #32]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008e0e:	681b      	ldr	r3, [r3, #0]
 8008e10:	68fa      	ldr	r2, [r7, #12]
 8008e12:	3a01      	subs	r2, #1
 8008e14:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008e18:	4b05      	ldr	r3, [pc, #20]	; (8008e30 <ulTaskNotifyTake+0x90>)
 8008e1a:	681b      	ldr	r3, [r3, #0]
 8008e1c:	2200      	movs	r2, #0
 8008e1e:	f883 247c 	strb.w	r2, [r3, #1148]	; 0x47c
 8008e22:	f000 fe01 	bl	8009a28 <vPortExitCritical>
 8008e26:	68fb      	ldr	r3, [r7, #12]
 8008e28:	4618      	mov	r0, r3
 8008e2a:	3710      	adds	r7, #16
 8008e2c:	46bd      	mov	sp, r7
 8008e2e:	bd80      	pop	{r7, pc}
 8008e30:	20000bf8 	.word	0x20000bf8
 8008e34:	e000ed04 	.word	0xe000ed04

08008e38 <xTaskGenericNotify>:
 8008e38:	b580      	push	{r7, lr}
 8008e3a:	b08a      	sub	sp, #40	; 0x28
 8008e3c:	af00      	add	r7, sp, #0
 8008e3e:	60f8      	str	r0, [r7, #12]
 8008e40:	60b9      	str	r1, [r7, #8]
 8008e42:	603b      	str	r3, [r7, #0]
 8008e44:	4613      	mov	r3, r2
 8008e46:	71fb      	strb	r3, [r7, #7]
 8008e48:	2301      	movs	r3, #1
 8008e4a:	627b      	str	r3, [r7, #36]	; 0x24
 8008e4c:	68fb      	ldr	r3, [r7, #12]
 8008e4e:	2b00      	cmp	r3, #0
 8008e50:	d109      	bne.n	8008e66 <xTaskGenericNotify+0x2e>
 8008e52:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008e56:	f383 8811 	msr	BASEPRI, r3
 8008e5a:	f3bf 8f6f 	isb	sy
 8008e5e:	f3bf 8f4f 	dsb	sy
 8008e62:	61bb      	str	r3, [r7, #24]
 8008e64:	e7fe      	b.n	8008e64 <xTaskGenericNotify+0x2c>
 8008e66:	68fb      	ldr	r3, [r7, #12]
 8008e68:	623b      	str	r3, [r7, #32]
 8008e6a:	f000 fdaf 	bl	80099cc <vPortEnterCritical>
 8008e6e:	683b      	ldr	r3, [r7, #0]
 8008e70:	2b00      	cmp	r3, #0
 8008e72:	d004      	beq.n	8008e7e <xTaskGenericNotify+0x46>
 8008e74:	6a3b      	ldr	r3, [r7, #32]
 8008e76:	f8d3 2478 	ldr.w	r2, [r3, #1144]	; 0x478
 8008e7a:	683b      	ldr	r3, [r7, #0]
 8008e7c:	601a      	str	r2, [r3, #0]
 8008e7e:	6a3b      	ldr	r3, [r7, #32]
 8008e80:	f893 347c 	ldrb.w	r3, [r3, #1148]	; 0x47c
 8008e84:	77fb      	strb	r3, [r7, #31]
 8008e86:	6a3b      	ldr	r3, [r7, #32]
 8008e88:	2202      	movs	r2, #2
 8008e8a:	f883 247c 	strb.w	r2, [r3, #1148]	; 0x47c
 8008e8e:	79fb      	ldrb	r3, [r7, #7]
 8008e90:	2b04      	cmp	r3, #4
 8008e92:	d82e      	bhi.n	8008ef2 <xTaskGenericNotify+0xba>
 8008e94:	a201      	add	r2, pc, #4	; (adr r2, 8008e9c <xTaskGenericNotify+0x64>)
 8008e96:	f852 f023 	ldr.w	pc, [r2, r3, lsl #2]
 8008e9a:	bf00      	nop
 8008e9c:	08008f13 	.word	0x08008f13
 8008ea0:	08008eb1 	.word	0x08008eb1
 8008ea4:	08008ec3 	.word	0x08008ec3
 8008ea8:	08008ed3 	.word	0x08008ed3
 8008eac:	08008edd 	.word	0x08008edd
 8008eb0:	6a3b      	ldr	r3, [r7, #32]
 8008eb2:	f8d3 2478 	ldr.w	r2, [r3, #1144]	; 0x478
 8008eb6:	68bb      	ldr	r3, [r7, #8]
 8008eb8:	431a      	orrs	r2, r3
 8008eba:	6a3b      	ldr	r3, [r7, #32]
 8008ebc:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008ec0:	e02a      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008ec2:	6a3b      	ldr	r3, [r7, #32]
 8008ec4:	f8d3 3478 	ldr.w	r3, [r3, #1144]	; 0x478
 8008ec8:	1c5a      	adds	r2, r3, #1
 8008eca:	6a3b      	ldr	r3, [r7, #32]
 8008ecc:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008ed0:	e022      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008ed2:	6a3b      	ldr	r3, [r7, #32]
 8008ed4:	68ba      	ldr	r2, [r7, #8]
 8008ed6:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008eda:	e01d      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008edc:	7ffb      	ldrb	r3, [r7, #31]
 8008ede:	2b02      	cmp	r3, #2
 8008ee0:	d004      	beq.n	8008eec <xTaskGenericNotify+0xb4>
 8008ee2:	6a3b      	ldr	r3, [r7, #32]
 8008ee4:	68ba      	ldr	r2, [r7, #8]
 8008ee6:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8008eea:	e015      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008eec:	2300      	movs	r3, #0
 8008eee:	627b      	str	r3, [r7, #36]	; 0x24
 8008ef0:	e012      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008ef2:	6a3b      	ldr	r3, [r7, #32]
 8008ef4:	f8d3 3478 	ldr.w	r3, [r3, #1144]	; 0x478
 8008ef8:	f1b3 3fff 	cmp.w	r3, #4294967295
 8008efc:	d00b      	beq.n	8008f16 <xTaskGenericNotify+0xde>
 8008efe:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008f02:	f383 8811 	msr	BASEPRI, r3
 8008f06:	f3bf 8f6f 	isb	sy
 8008f0a:	f3bf 8f4f 	dsb	sy
 8008f0e:	617b      	str	r3, [r7, #20]
 8008f10:	e7fe      	b.n	8008f10 <xTaskGenericNotify+0xd8>
 8008f12:	bf00      	nop
 8008f14:	e000      	b.n	8008f18 <xTaskGenericNotify+0xe0>
 8008f16:	bf00      	nop
 8008f18:	7ffb      	ldrb	r3, [r7, #31]
 8008f1a:	2b01      	cmp	r3, #1
 8008f1c:	d138      	bne.n	8008f90 <xTaskGenericNotify+0x158>
 8008f1e:	6a3b      	ldr	r3, [r7, #32]
 8008f20:	3304      	adds	r3, #4
 8008f22:	4618      	mov	r0, r3
 8008f24:	f7fe f8ab 	bl	800707e <uxListRemove>
 8008f28:	6a3b      	ldr	r3, [r7, #32]
 8008f2a:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008f2c:	2201      	movs	r2, #1
 8008f2e:	409a      	lsls	r2, r3
 8008f30:	4b1b      	ldr	r3, [pc, #108]	; (8008fa0 <xTaskGenericNotify+0x168>)
 8008f32:	681b      	ldr	r3, [r3, #0]
 8008f34:	4313      	orrs	r3, r2
 8008f36:	4a1a      	ldr	r2, [pc, #104]	; (8008fa0 <xTaskGenericNotify+0x168>)
 8008f38:	6013      	str	r3, [r2, #0]
 8008f3a:	6a3b      	ldr	r3, [r7, #32]
 8008f3c:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008f3e:	4613      	mov	r3, r2
 8008f40:	009b      	lsls	r3, r3, #2
 8008f42:	4413      	add	r3, r2
 8008f44:	009b      	lsls	r3, r3, #2
 8008f46:	4a17      	ldr	r2, [pc, #92]	; (8008fa4 <xTaskGenericNotify+0x16c>)
 8008f48:	441a      	add	r2, r3
 8008f4a:	6a3b      	ldr	r3, [r7, #32]
 8008f4c:	3304      	adds	r3, #4
 8008f4e:	4619      	mov	r1, r3
 8008f50:	4610      	mov	r0, r2
 8008f52:	f7fe f837 	bl	8006fc4 <vListInsertEnd>
 8008f56:	6a3b      	ldr	r3, [r7, #32]
 8008f58:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 8008f5a:	2b00      	cmp	r3, #0
 8008f5c:	d009      	beq.n	8008f72 <xTaskGenericNotify+0x13a>
 8008f5e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008f62:	f383 8811 	msr	BASEPRI, r3
 8008f66:	f3bf 8f6f 	isb	sy
 8008f6a:	f3bf 8f4f 	dsb	sy
 8008f6e:	613b      	str	r3, [r7, #16]
 8008f70:	e7fe      	b.n	8008f70 <xTaskGenericNotify+0x138>
 8008f72:	6a3b      	ldr	r3, [r7, #32]
 8008f74:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008f76:	4b0c      	ldr	r3, [pc, #48]	; (8008fa8 <xTaskGenericNotify+0x170>)
 8008f78:	681b      	ldr	r3, [r3, #0]
 8008f7a:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008f7c:	429a      	cmp	r2, r3
 8008f7e:	d907      	bls.n	8008f90 <xTaskGenericNotify+0x158>
 8008f80:	4b0a      	ldr	r3, [pc, #40]	; (8008fac <xTaskGenericNotify+0x174>)
 8008f82:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8008f86:	601a      	str	r2, [r3, #0]
 8008f88:	f3bf 8f4f 	dsb	sy
 8008f8c:	f3bf 8f6f 	isb	sy
 8008f90:	f000 fd4a 	bl	8009a28 <vPortExitCritical>
 8008f94:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008f96:	4618      	mov	r0, r3
 8008f98:	3728      	adds	r7, #40	; 0x28
 8008f9a:	46bd      	mov	sp, r7
 8008f9c:	bd80      	pop	{r7, pc}
 8008f9e:	bf00      	nop
 8008fa0:	20000cd8 	.word	0x20000cd8
 8008fa4:	20000bfc 	.word	0x20000bfc
 8008fa8:	20000bf8 	.word	0x20000bf8
 8008fac:	e000ed04 	.word	0xe000ed04

08008fb0 <vTaskNotifyGiveFromISR>:
 8008fb0:	b580      	push	{r7, lr}
 8008fb2:	b08a      	sub	sp, #40	; 0x28
 8008fb4:	af00      	add	r7, sp, #0
 8008fb6:	6078      	str	r0, [r7, #4]
 8008fb8:	6039      	str	r1, [r7, #0]
 8008fba:	687b      	ldr	r3, [r7, #4]
 8008fbc:	2b00      	cmp	r3, #0
 8008fbe:	d109      	bne.n	8008fd4 <vTaskNotifyGiveFromISR+0x24>
 8008fc0:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008fc4:	f383 8811 	msr	BASEPRI, r3
 8008fc8:	f3bf 8f6f 	isb	sy
 8008fcc:	f3bf 8f4f 	dsb	sy
 8008fd0:	61bb      	str	r3, [r7, #24]
 8008fd2:	e7fe      	b.n	8008fd2 <vTaskNotifyGiveFromISR+0x22>
 8008fd4:	f000 fdd6 	bl	8009b84 <vPortValidateInterruptPriority>
 8008fd8:	687b      	ldr	r3, [r7, #4]
 8008fda:	627b      	str	r3, [r7, #36]	; 0x24
 8008fdc:	f3ef 8211 	mrs	r2, BASEPRI
 8008fe0:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008fe4:	f383 8811 	msr	BASEPRI, r3
 8008fe8:	f3bf 8f6f 	isb	sy
 8008fec:	f3bf 8f4f 	dsb	sy
 8008ff0:	617a      	str	r2, [r7, #20]
 8008ff2:	613b      	str	r3, [r7, #16]
 8008ff4:	697b      	ldr	r3, [r7, #20]
 8008ff6:	623b      	str	r3, [r7, #32]
 8008ff8:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008ffa:	f893 347c 	ldrb.w	r3, [r3, #1148]	; 0x47c
 8008ffe:	77fb      	strb	r3, [r7, #31]
 8009000:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009002:	2202      	movs	r2, #2
 8009004:	f883 247c 	strb.w	r2, [r3, #1148]	; 0x47c
 8009008:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800900a:	f8d3 3478 	ldr.w	r3, [r3, #1144]	; 0x478
 800900e:	1c5a      	adds	r2, r3, #1
 8009010:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009012:	f8c3 2478 	str.w	r2, [r3, #1144]	; 0x478
 8009016:	7ffb      	ldrb	r3, [r7, #31]
 8009018:	2b01      	cmp	r3, #1
 800901a:	d144      	bne.n	80090a6 <vTaskNotifyGiveFromISR+0xf6>
 800901c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800901e:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 8009020:	2b00      	cmp	r3, #0
 8009022:	d009      	beq.n	8009038 <vTaskNotifyGiveFromISR+0x88>
 8009024:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009028:	f383 8811 	msr	BASEPRI, r3
 800902c:	f3bf 8f6f 	isb	sy
 8009030:	f3bf 8f4f 	dsb	sy
 8009034:	60fb      	str	r3, [r7, #12]
 8009036:	e7fe      	b.n	8009036 <vTaskNotifyGiveFromISR+0x86>
 8009038:	4b1f      	ldr	r3, [pc, #124]	; (80090b8 <vTaskNotifyGiveFromISR+0x108>)
 800903a:	681b      	ldr	r3, [r3, #0]
 800903c:	2b00      	cmp	r3, #0
 800903e:	d11c      	bne.n	800907a <vTaskNotifyGiveFromISR+0xca>
 8009040:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009042:	3304      	adds	r3, #4
 8009044:	4618      	mov	r0, r3
 8009046:	f7fe f81a 	bl	800707e <uxListRemove>
 800904a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800904c:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 800904e:	2201      	movs	r2, #1
 8009050:	409a      	lsls	r2, r3
 8009052:	4b1a      	ldr	r3, [pc, #104]	; (80090bc <vTaskNotifyGiveFromISR+0x10c>)
 8009054:	681b      	ldr	r3, [r3, #0]
 8009056:	4313      	orrs	r3, r2
 8009058:	4a18      	ldr	r2, [pc, #96]	; (80090bc <vTaskNotifyGiveFromISR+0x10c>)
 800905a:	6013      	str	r3, [r2, #0]
 800905c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800905e:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8009060:	4613      	mov	r3, r2
 8009062:	009b      	lsls	r3, r3, #2
 8009064:	4413      	add	r3, r2
 8009066:	009b      	lsls	r3, r3, #2
 8009068:	4a15      	ldr	r2, [pc, #84]	; (80090c0 <vTaskNotifyGiveFromISR+0x110>)
 800906a:	441a      	add	r2, r3
 800906c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800906e:	3304      	adds	r3, #4
 8009070:	4619      	mov	r1, r3
 8009072:	4610      	mov	r0, r2
 8009074:	f7fd ffa6 	bl	8006fc4 <vListInsertEnd>
 8009078:	e005      	b.n	8009086 <vTaskNotifyGiveFromISR+0xd6>
 800907a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800907c:	3318      	adds	r3, #24
 800907e:	4619      	mov	r1, r3
 8009080:	4810      	ldr	r0, [pc, #64]	; (80090c4 <vTaskNotifyGiveFromISR+0x114>)
 8009082:	f7fd ff9f 	bl	8006fc4 <vListInsertEnd>
 8009086:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009088:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 800908a:	4b0f      	ldr	r3, [pc, #60]	; (80090c8 <vTaskNotifyGiveFromISR+0x118>)
 800908c:	681b      	ldr	r3, [r3, #0]
 800908e:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8009090:	429a      	cmp	r2, r3
 8009092:	d908      	bls.n	80090a6 <vTaskNotifyGiveFromISR+0xf6>
 8009094:	683b      	ldr	r3, [r7, #0]
 8009096:	2b00      	cmp	r3, #0
 8009098:	d002      	beq.n	80090a0 <vTaskNotifyGiveFromISR+0xf0>
 800909a:	683b      	ldr	r3, [r7, #0]
 800909c:	2201      	movs	r2, #1
 800909e:	601a      	str	r2, [r3, #0]
 80090a0:	4b0a      	ldr	r3, [pc, #40]	; (80090cc <vTaskNotifyGiveFromISR+0x11c>)
 80090a2:	2201      	movs	r2, #1
 80090a4:	601a      	str	r2, [r3, #0]
 80090a6:	6a3b      	ldr	r3, [r7, #32]
 80090a8:	60bb      	str	r3, [r7, #8]
 80090aa:	68bb      	ldr	r3, [r7, #8]
 80090ac:	f383 8811 	msr	BASEPRI, r3
 80090b0:	bf00      	nop
 80090b2:	3728      	adds	r7, #40	; 0x28
 80090b4:	46bd      	mov	sp, r7
 80090b6:	bd80      	pop	{r7, pc}
 80090b8:	20000cf8 	.word	0x20000cf8
 80090bc:	20000cd8 	.word	0x20000cd8
 80090c0:	20000bfc 	.word	0x20000bfc
 80090c4:	20000c90 	.word	0x20000c90
 80090c8:	20000bf8 	.word	0x20000bf8
 80090cc:	20000ce4 	.word	0x20000ce4

080090d0 <prvAddCurrentTaskToDelayedList>:
 80090d0:	b580      	push	{r7, lr}
 80090d2:	b084      	sub	sp, #16
 80090d4:	af00      	add	r7, sp, #0
 80090d6:	6078      	str	r0, [r7, #4]
 80090d8:	6039      	str	r1, [r7, #0]
 80090da:	4b29      	ldr	r3, [pc, #164]	; (8009180 <prvAddCurrentTaskToDelayedList+0xb0>)
 80090dc:	681b      	ldr	r3, [r3, #0]
 80090de:	60fb      	str	r3, [r7, #12]
 80090e0:	4b28      	ldr	r3, [pc, #160]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 80090e2:	681b      	ldr	r3, [r3, #0]
 80090e4:	3304      	adds	r3, #4
 80090e6:	4618      	mov	r0, r3
 80090e8:	f7fd ffc9 	bl	800707e <uxListRemove>
 80090ec:	4603      	mov	r3, r0
 80090ee:	2b00      	cmp	r3, #0
 80090f0:	d10b      	bne.n	800910a <prvAddCurrentTaskToDelayedList+0x3a>
 80090f2:	4b24      	ldr	r3, [pc, #144]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 80090f4:	681b      	ldr	r3, [r3, #0]
 80090f6:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80090f8:	2201      	movs	r2, #1
 80090fa:	fa02 f303 	lsl.w	r3, r2, r3
 80090fe:	43da      	mvns	r2, r3
 8009100:	4b21      	ldr	r3, [pc, #132]	; (8009188 <prvAddCurrentTaskToDelayedList+0xb8>)
 8009102:	681b      	ldr	r3, [r3, #0]
 8009104:	4013      	ands	r3, r2
 8009106:	4a20      	ldr	r2, [pc, #128]	; (8009188 <prvAddCurrentTaskToDelayedList+0xb8>)
 8009108:	6013      	str	r3, [r2, #0]
 800910a:	687b      	ldr	r3, [r7, #4]
 800910c:	f1b3 3fff 	cmp.w	r3, #4294967295
 8009110:	d10a      	bne.n	8009128 <prvAddCurrentTaskToDelayedList+0x58>
 8009112:	683b      	ldr	r3, [r7, #0]
 8009114:	2b00      	cmp	r3, #0
 8009116:	d007      	beq.n	8009128 <prvAddCurrentTaskToDelayedList+0x58>
 8009118:	4b1a      	ldr	r3, [pc, #104]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 800911a:	681b      	ldr	r3, [r3, #0]
 800911c:	3304      	adds	r3, #4
 800911e:	4619      	mov	r1, r3
 8009120:	481a      	ldr	r0, [pc, #104]	; (800918c <prvAddCurrentTaskToDelayedList+0xbc>)
 8009122:	f7fd ff4f 	bl	8006fc4 <vListInsertEnd>
 8009126:	e026      	b.n	8009176 <prvAddCurrentTaskToDelayedList+0xa6>
 8009128:	68fa      	ldr	r2, [r7, #12]
 800912a:	687b      	ldr	r3, [r7, #4]
 800912c:	4413      	add	r3, r2
 800912e:	60bb      	str	r3, [r7, #8]
 8009130:	4b14      	ldr	r3, [pc, #80]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 8009132:	681b      	ldr	r3, [r3, #0]
 8009134:	68ba      	ldr	r2, [r7, #8]
 8009136:	605a      	str	r2, [r3, #4]
 8009138:	68ba      	ldr	r2, [r7, #8]
 800913a:	68fb      	ldr	r3, [r7, #12]
 800913c:	429a      	cmp	r2, r3
 800913e:	d209      	bcs.n	8009154 <prvAddCurrentTaskToDelayedList+0x84>
 8009140:	4b13      	ldr	r3, [pc, #76]	; (8009190 <prvAddCurrentTaskToDelayedList+0xc0>)
 8009142:	681a      	ldr	r2, [r3, #0]
 8009144:	4b0f      	ldr	r3, [pc, #60]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 8009146:	681b      	ldr	r3, [r3, #0]
 8009148:	3304      	adds	r3, #4
 800914a:	4619      	mov	r1, r3
 800914c:	4610      	mov	r0, r2
 800914e:	f7fd ff5d 	bl	800700c <vListInsert>
 8009152:	e010      	b.n	8009176 <prvAddCurrentTaskToDelayedList+0xa6>
 8009154:	4b0f      	ldr	r3, [pc, #60]	; (8009194 <prvAddCurrentTaskToDelayedList+0xc4>)
 8009156:	681a      	ldr	r2, [r3, #0]
 8009158:	4b0a      	ldr	r3, [pc, #40]	; (8009184 <prvAddCurrentTaskToDelayedList+0xb4>)
 800915a:	681b      	ldr	r3, [r3, #0]
 800915c:	3304      	adds	r3, #4
 800915e:	4619      	mov	r1, r3
 8009160:	4610      	mov	r0, r2
 8009162:	f7fd ff53 	bl	800700c <vListInsert>
 8009166:	4b0c      	ldr	r3, [pc, #48]	; (8009198 <prvAddCurrentTaskToDelayedList+0xc8>)
 8009168:	681b      	ldr	r3, [r3, #0]
 800916a:	68ba      	ldr	r2, [r7, #8]
 800916c:	429a      	cmp	r2, r3
 800916e:	d202      	bcs.n	8009176 <prvAddCurrentTaskToDelayedList+0xa6>
 8009170:	4a09      	ldr	r2, [pc, #36]	; (8009198 <prvAddCurrentTaskToDelayedList+0xc8>)
 8009172:	68bb      	ldr	r3, [r7, #8]
 8009174:	6013      	str	r3, [r2, #0]
 8009176:	bf00      	nop
 8009178:	3710      	adds	r7, #16
 800917a:	46bd      	mov	sp, r7
 800917c:	bd80      	pop	{r7, pc}
 800917e:	bf00      	nop
 8009180:	20000cd4 	.word	0x20000cd4
 8009184:	20000bf8 	.word	0x20000bf8
 8009188:	20000cd8 	.word	0x20000cd8
 800918c:	20000cbc 	.word	0x20000cbc
 8009190:	20000c8c 	.word	0x20000c8c
 8009194:	20000c88 	.word	0x20000c88
 8009198:	20000cf0 	.word	0x20000cf0

0800919c <xTimerCreateTimerTask>:
 800919c:	b580      	push	{r7, lr}
 800919e:	b08a      	sub	sp, #40	; 0x28
 80091a0:	af04      	add	r7, sp, #16
 80091a2:	2300      	movs	r3, #0
 80091a4:	617b      	str	r3, [r7, #20]
 80091a6:	f000 faa9 	bl	80096fc <prvCheckForValidListAndQueue>
 80091aa:	4b1c      	ldr	r3, [pc, #112]	; (800921c <xTimerCreateTimerTask+0x80>)
 80091ac:	681b      	ldr	r3, [r3, #0]
 80091ae:	2b00      	cmp	r3, #0
 80091b0:	d021      	beq.n	80091f6 <xTimerCreateTimerTask+0x5a>
 80091b2:	2300      	movs	r3, #0
 80091b4:	60fb      	str	r3, [r7, #12]
 80091b6:	2300      	movs	r3, #0
 80091b8:	60bb      	str	r3, [r7, #8]
 80091ba:	1d3a      	adds	r2, r7, #4
 80091bc:	f107 0108 	add.w	r1, r7, #8
 80091c0:	f107 030c 	add.w	r3, r7, #12
 80091c4:	4618      	mov	r0, r3
 80091c6:	f7fb fb91 	bl	80048ec <vApplicationGetTimerTaskMemory>
 80091ca:	6879      	ldr	r1, [r7, #4]
 80091cc:	68bb      	ldr	r3, [r7, #8]
 80091ce:	68fa      	ldr	r2, [r7, #12]
 80091d0:	9202      	str	r2, [sp, #8]
 80091d2:	9301      	str	r3, [sp, #4]
 80091d4:	2302      	movs	r3, #2
 80091d6:	9300      	str	r3, [sp, #0]
 80091d8:	2300      	movs	r3, #0
 80091da:	460a      	mov	r2, r1
 80091dc:	4910      	ldr	r1, [pc, #64]	; (8009220 <xTimerCreateTimerTask+0x84>)
 80091de:	4811      	ldr	r0, [pc, #68]	; (8009224 <xTimerCreateTimerTask+0x88>)
 80091e0:	f7fe fda2 	bl	8007d28 <xTaskCreateStatic>
 80091e4:	4602      	mov	r2, r0
 80091e6:	4b10      	ldr	r3, [pc, #64]	; (8009228 <xTimerCreateTimerTask+0x8c>)
 80091e8:	601a      	str	r2, [r3, #0]
 80091ea:	4b0f      	ldr	r3, [pc, #60]	; (8009228 <xTimerCreateTimerTask+0x8c>)
 80091ec:	681b      	ldr	r3, [r3, #0]
 80091ee:	2b00      	cmp	r3, #0
 80091f0:	d001      	beq.n	80091f6 <xTimerCreateTimerTask+0x5a>
 80091f2:	2301      	movs	r3, #1
 80091f4:	617b      	str	r3, [r7, #20]
 80091f6:	697b      	ldr	r3, [r7, #20]
 80091f8:	2b00      	cmp	r3, #0
 80091fa:	d109      	bne.n	8009210 <xTimerCreateTimerTask+0x74>
 80091fc:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009200:	f383 8811 	msr	BASEPRI, r3
 8009204:	f3bf 8f6f 	isb	sy
 8009208:	f3bf 8f4f 	dsb	sy
 800920c:	613b      	str	r3, [r7, #16]
 800920e:	e7fe      	b.n	800920e <xTimerCreateTimerTask+0x72>
 8009210:	697b      	ldr	r3, [r7, #20]
 8009212:	4618      	mov	r0, r3
 8009214:	3718      	adds	r7, #24
 8009216:	46bd      	mov	sp, r7
 8009218:	bd80      	pop	{r7, pc}
 800921a:	bf00      	nop
 800921c:	20000d2c 	.word	0x20000d2c
 8009220:	0800d108 	.word	0x0800d108
 8009224:	08009345 	.word	0x08009345
 8009228:	20000d30 	.word	0x20000d30

0800922c <xTimerGenericCommand>:
 800922c:	b580      	push	{r7, lr}
 800922e:	b08a      	sub	sp, #40	; 0x28
 8009230:	af00      	add	r7, sp, #0
 8009232:	60f8      	str	r0, [r7, #12]
 8009234:	60b9      	str	r1, [r7, #8]
 8009236:	607a      	str	r2, [r7, #4]
 8009238:	603b      	str	r3, [r7, #0]
 800923a:	2300      	movs	r3, #0
 800923c:	627b      	str	r3, [r7, #36]	; 0x24
 800923e:	68fb      	ldr	r3, [r7, #12]
 8009240:	2b00      	cmp	r3, #0
 8009242:	d109      	bne.n	8009258 <xTimerGenericCommand+0x2c>
 8009244:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009248:	f383 8811 	msr	BASEPRI, r3
 800924c:	f3bf 8f6f 	isb	sy
 8009250:	f3bf 8f4f 	dsb	sy
 8009254:	623b      	str	r3, [r7, #32]
 8009256:	e7fe      	b.n	8009256 <xTimerGenericCommand+0x2a>
 8009258:	4b19      	ldr	r3, [pc, #100]	; (80092c0 <xTimerGenericCommand+0x94>)
 800925a:	681b      	ldr	r3, [r3, #0]
 800925c:	2b00      	cmp	r3, #0
 800925e:	d02a      	beq.n	80092b6 <xTimerGenericCommand+0x8a>
 8009260:	68bb      	ldr	r3, [r7, #8]
 8009262:	617b      	str	r3, [r7, #20]
 8009264:	687b      	ldr	r3, [r7, #4]
 8009266:	61bb      	str	r3, [r7, #24]
 8009268:	68fb      	ldr	r3, [r7, #12]
 800926a:	61fb      	str	r3, [r7, #28]
 800926c:	68bb      	ldr	r3, [r7, #8]
 800926e:	2b05      	cmp	r3, #5
 8009270:	dc18      	bgt.n	80092a4 <xTimerGenericCommand+0x78>
 8009272:	f7ff fbc7 	bl	8008a04 <xTaskGetSchedulerState>
 8009276:	4603      	mov	r3, r0
 8009278:	2b02      	cmp	r3, #2
 800927a:	d109      	bne.n	8009290 <xTimerGenericCommand+0x64>
 800927c:	4b10      	ldr	r3, [pc, #64]	; (80092c0 <xTimerGenericCommand+0x94>)
 800927e:	6818      	ldr	r0, [r3, #0]
 8009280:	f107 0114 	add.w	r1, r7, #20
 8009284:	2300      	movs	r3, #0
 8009286:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8009288:	f7fe f858 	bl	800733c <xQueueGenericSend>
 800928c:	6278      	str	r0, [r7, #36]	; 0x24
 800928e:	e012      	b.n	80092b6 <xTimerGenericCommand+0x8a>
 8009290:	4b0b      	ldr	r3, [pc, #44]	; (80092c0 <xTimerGenericCommand+0x94>)
 8009292:	6818      	ldr	r0, [r3, #0]
 8009294:	f107 0114 	add.w	r1, r7, #20
 8009298:	2300      	movs	r3, #0
 800929a:	2200      	movs	r2, #0
 800929c:	f7fe f84e 	bl	800733c <xQueueGenericSend>
 80092a0:	6278      	str	r0, [r7, #36]	; 0x24
 80092a2:	e008      	b.n	80092b6 <xTimerGenericCommand+0x8a>
 80092a4:	4b06      	ldr	r3, [pc, #24]	; (80092c0 <xTimerGenericCommand+0x94>)
 80092a6:	6818      	ldr	r0, [r3, #0]
 80092a8:	f107 0114 	add.w	r1, r7, #20
 80092ac:	2300      	movs	r3, #0
 80092ae:	683a      	ldr	r2, [r7, #0]
 80092b0:	f7fe f93e 	bl	8007530 <xQueueGenericSendFromISR>
 80092b4:	6278      	str	r0, [r7, #36]	; 0x24
 80092b6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80092b8:	4618      	mov	r0, r3
 80092ba:	3728      	adds	r7, #40	; 0x28
 80092bc:	46bd      	mov	sp, r7
 80092be:	bd80      	pop	{r7, pc}
 80092c0:	20000d2c 	.word	0x20000d2c

080092c4 <prvProcessExpiredTimer>:
 80092c4:	b580      	push	{r7, lr}
 80092c6:	b088      	sub	sp, #32
 80092c8:	af02      	add	r7, sp, #8
 80092ca:	6078      	str	r0, [r7, #4]
 80092cc:	6039      	str	r1, [r7, #0]
 80092ce:	4b1c      	ldr	r3, [pc, #112]	; (8009340 <prvProcessExpiredTimer+0x7c>)
 80092d0:	681b      	ldr	r3, [r3, #0]
 80092d2:	68db      	ldr	r3, [r3, #12]
 80092d4:	68db      	ldr	r3, [r3, #12]
 80092d6:	617b      	str	r3, [r7, #20]
 80092d8:	697b      	ldr	r3, [r7, #20]
 80092da:	3304      	adds	r3, #4
 80092dc:	4618      	mov	r0, r3
 80092de:	f7fd fece 	bl	800707e <uxListRemove>
 80092e2:	697b      	ldr	r3, [r7, #20]
 80092e4:	69db      	ldr	r3, [r3, #28]
 80092e6:	2b01      	cmp	r3, #1
 80092e8:	d121      	bne.n	800932e <prvProcessExpiredTimer+0x6a>
 80092ea:	697b      	ldr	r3, [r7, #20]
 80092ec:	699a      	ldr	r2, [r3, #24]
 80092ee:	687b      	ldr	r3, [r7, #4]
 80092f0:	18d1      	adds	r1, r2, r3
 80092f2:	687b      	ldr	r3, [r7, #4]
 80092f4:	683a      	ldr	r2, [r7, #0]
 80092f6:	6978      	ldr	r0, [r7, #20]
 80092f8:	f000 f8c8 	bl	800948c <prvInsertTimerInActiveList>
 80092fc:	4603      	mov	r3, r0
 80092fe:	2b00      	cmp	r3, #0
 8009300:	d015      	beq.n	800932e <prvProcessExpiredTimer+0x6a>
 8009302:	2300      	movs	r3, #0
 8009304:	9300      	str	r3, [sp, #0]
 8009306:	2300      	movs	r3, #0
 8009308:	687a      	ldr	r2, [r7, #4]
 800930a:	2100      	movs	r1, #0
 800930c:	6978      	ldr	r0, [r7, #20]
 800930e:	f7ff ff8d 	bl	800922c <xTimerGenericCommand>
 8009312:	6138      	str	r0, [r7, #16]
 8009314:	693b      	ldr	r3, [r7, #16]
 8009316:	2b00      	cmp	r3, #0
 8009318:	d109      	bne.n	800932e <prvProcessExpiredTimer+0x6a>
 800931a:	f04f 0350 	mov.w	r3, #80	; 0x50
 800931e:	f383 8811 	msr	BASEPRI, r3
 8009322:	f3bf 8f6f 	isb	sy
 8009326:	f3bf 8f4f 	dsb	sy
 800932a:	60fb      	str	r3, [r7, #12]
 800932c:	e7fe      	b.n	800932c <prvProcessExpiredTimer+0x68>
 800932e:	697b      	ldr	r3, [r7, #20]
 8009330:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8009332:	6978      	ldr	r0, [r7, #20]
 8009334:	4798      	blx	r3
 8009336:	bf00      	nop
 8009338:	3718      	adds	r7, #24
 800933a:	46bd      	mov	sp, r7
 800933c:	bd80      	pop	{r7, pc}
 800933e:	bf00      	nop
 8009340:	20000d24 	.word	0x20000d24

08009344 <prvTimerTask>:
 8009344:	b580      	push	{r7, lr}
 8009346:	b084      	sub	sp, #16
 8009348:	af00      	add	r7, sp, #0
 800934a:	6078      	str	r0, [r7, #4]
 800934c:	f107 0308 	add.w	r3, r7, #8
 8009350:	4618      	mov	r0, r3
 8009352:	f000 f857 	bl	8009404 <prvGetNextExpireTime>
 8009356:	60f8      	str	r0, [r7, #12]
 8009358:	68bb      	ldr	r3, [r7, #8]
 800935a:	4619      	mov	r1, r3
 800935c:	68f8      	ldr	r0, [r7, #12]
 800935e:	f000 f803 	bl	8009368 <prvProcessTimerOrBlockTask>
 8009362:	f000 f8d5 	bl	8009510 <prvProcessReceivedCommands>
 8009366:	e7f1      	b.n	800934c <prvTimerTask+0x8>

08009368 <prvProcessTimerOrBlockTask>:
 8009368:	b580      	push	{r7, lr}
 800936a:	b084      	sub	sp, #16
 800936c:	af00      	add	r7, sp, #0
 800936e:	6078      	str	r0, [r7, #4]
 8009370:	6039      	str	r1, [r7, #0]
 8009372:	f7fe ff19 	bl	80081a8 <vTaskSuspendAll>
 8009376:	f107 0308 	add.w	r3, r7, #8
 800937a:	4618      	mov	r0, r3
 800937c:	f000 f866 	bl	800944c <prvSampleTimeNow>
 8009380:	60f8      	str	r0, [r7, #12]
 8009382:	68bb      	ldr	r3, [r7, #8]
 8009384:	2b00      	cmp	r3, #0
 8009386:	d130      	bne.n	80093ea <prvProcessTimerOrBlockTask+0x82>
 8009388:	683b      	ldr	r3, [r7, #0]
 800938a:	2b00      	cmp	r3, #0
 800938c:	d10a      	bne.n	80093a4 <prvProcessTimerOrBlockTask+0x3c>
 800938e:	687a      	ldr	r2, [r7, #4]
 8009390:	68fb      	ldr	r3, [r7, #12]
 8009392:	429a      	cmp	r2, r3
 8009394:	d806      	bhi.n	80093a4 <prvProcessTimerOrBlockTask+0x3c>
 8009396:	f7fe ff15 	bl	80081c4 <xTaskResumeAll>
 800939a:	68f9      	ldr	r1, [r7, #12]
 800939c:	6878      	ldr	r0, [r7, #4]
 800939e:	f7ff ff91 	bl	80092c4 <prvProcessExpiredTimer>
 80093a2:	e024      	b.n	80093ee <prvProcessTimerOrBlockTask+0x86>
 80093a4:	683b      	ldr	r3, [r7, #0]
 80093a6:	2b00      	cmp	r3, #0
 80093a8:	d008      	beq.n	80093bc <prvProcessTimerOrBlockTask+0x54>
 80093aa:	4b13      	ldr	r3, [pc, #76]	; (80093f8 <prvProcessTimerOrBlockTask+0x90>)
 80093ac:	681b      	ldr	r3, [r3, #0]
 80093ae:	681b      	ldr	r3, [r3, #0]
 80093b0:	2b00      	cmp	r3, #0
 80093b2:	d101      	bne.n	80093b8 <prvProcessTimerOrBlockTask+0x50>
 80093b4:	2301      	movs	r3, #1
 80093b6:	e000      	b.n	80093ba <prvProcessTimerOrBlockTask+0x52>
 80093b8:	2300      	movs	r3, #0
 80093ba:	603b      	str	r3, [r7, #0]
 80093bc:	4b0f      	ldr	r3, [pc, #60]	; (80093fc <prvProcessTimerOrBlockTask+0x94>)
 80093be:	6818      	ldr	r0, [r3, #0]
 80093c0:	687a      	ldr	r2, [r7, #4]
 80093c2:	68fb      	ldr	r3, [r7, #12]
 80093c4:	1ad3      	subs	r3, r2, r3
 80093c6:	683a      	ldr	r2, [r7, #0]
 80093c8:	4619      	mov	r1, r3
 80093ca:	f7fe fc79 	bl	8007cc0 <vQueueWaitForMessageRestricted>
 80093ce:	f7fe fef9 	bl	80081c4 <xTaskResumeAll>
 80093d2:	4603      	mov	r3, r0
 80093d4:	2b00      	cmp	r3, #0
 80093d6:	d10a      	bne.n	80093ee <prvProcessTimerOrBlockTask+0x86>
 80093d8:	4b09      	ldr	r3, [pc, #36]	; (8009400 <prvProcessTimerOrBlockTask+0x98>)
 80093da:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80093de:	601a      	str	r2, [r3, #0]
 80093e0:	f3bf 8f4f 	dsb	sy
 80093e4:	f3bf 8f6f 	isb	sy
 80093e8:	e001      	b.n	80093ee <prvProcessTimerOrBlockTask+0x86>
 80093ea:	f7fe feeb 	bl	80081c4 <xTaskResumeAll>
 80093ee:	bf00      	nop
 80093f0:	3710      	adds	r7, #16
 80093f2:	46bd      	mov	sp, r7
 80093f4:	bd80      	pop	{r7, pc}
 80093f6:	bf00      	nop
 80093f8:	20000d28 	.word	0x20000d28
 80093fc:	20000d2c 	.word	0x20000d2c
 8009400:	e000ed04 	.word	0xe000ed04

08009404 <prvGetNextExpireTime>:
 8009404:	b480      	push	{r7}
 8009406:	b085      	sub	sp, #20
 8009408:	af00      	add	r7, sp, #0
 800940a:	6078      	str	r0, [r7, #4]
 800940c:	4b0e      	ldr	r3, [pc, #56]	; (8009448 <prvGetNextExpireTime+0x44>)
 800940e:	681b      	ldr	r3, [r3, #0]
 8009410:	681b      	ldr	r3, [r3, #0]
 8009412:	2b00      	cmp	r3, #0
 8009414:	d101      	bne.n	800941a <prvGetNextExpireTime+0x16>
 8009416:	2201      	movs	r2, #1
 8009418:	e000      	b.n	800941c <prvGetNextExpireTime+0x18>
 800941a:	2200      	movs	r2, #0
 800941c:	687b      	ldr	r3, [r7, #4]
 800941e:	601a      	str	r2, [r3, #0]
 8009420:	687b      	ldr	r3, [r7, #4]
 8009422:	681b      	ldr	r3, [r3, #0]
 8009424:	2b00      	cmp	r3, #0
 8009426:	d105      	bne.n	8009434 <prvGetNextExpireTime+0x30>
 8009428:	4b07      	ldr	r3, [pc, #28]	; (8009448 <prvGetNextExpireTime+0x44>)
 800942a:	681b      	ldr	r3, [r3, #0]
 800942c:	68db      	ldr	r3, [r3, #12]
 800942e:	681b      	ldr	r3, [r3, #0]
 8009430:	60fb      	str	r3, [r7, #12]
 8009432:	e001      	b.n	8009438 <prvGetNextExpireTime+0x34>
 8009434:	2300      	movs	r3, #0
 8009436:	60fb      	str	r3, [r7, #12]
 8009438:	68fb      	ldr	r3, [r7, #12]
 800943a:	4618      	mov	r0, r3
 800943c:	3714      	adds	r7, #20
 800943e:	46bd      	mov	sp, r7
 8009440:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009444:	4770      	bx	lr
 8009446:	bf00      	nop
 8009448:	20000d24 	.word	0x20000d24

0800944c <prvSampleTimeNow>:
 800944c:	b580      	push	{r7, lr}
 800944e:	b084      	sub	sp, #16
 8009450:	af00      	add	r7, sp, #0
 8009452:	6078      	str	r0, [r7, #4]
 8009454:	f7fe ff52 	bl	80082fc <xTaskGetTickCount>
 8009458:	60f8      	str	r0, [r7, #12]
 800945a:	4b0b      	ldr	r3, [pc, #44]	; (8009488 <prvSampleTimeNow+0x3c>)
 800945c:	681b      	ldr	r3, [r3, #0]
 800945e:	68fa      	ldr	r2, [r7, #12]
 8009460:	429a      	cmp	r2, r3
 8009462:	d205      	bcs.n	8009470 <prvSampleTimeNow+0x24>
 8009464:	f000 f8ea 	bl	800963c <prvSwitchTimerLists>
 8009468:	687b      	ldr	r3, [r7, #4]
 800946a:	2201      	movs	r2, #1
 800946c:	601a      	str	r2, [r3, #0]
 800946e:	e002      	b.n	8009476 <prvSampleTimeNow+0x2a>
 8009470:	687b      	ldr	r3, [r7, #4]
 8009472:	2200      	movs	r2, #0
 8009474:	601a      	str	r2, [r3, #0]
 8009476:	4a04      	ldr	r2, [pc, #16]	; (8009488 <prvSampleTimeNow+0x3c>)
 8009478:	68fb      	ldr	r3, [r7, #12]
 800947a:	6013      	str	r3, [r2, #0]
 800947c:	68fb      	ldr	r3, [r7, #12]
 800947e:	4618      	mov	r0, r3
 8009480:	3710      	adds	r7, #16
 8009482:	46bd      	mov	sp, r7
 8009484:	bd80      	pop	{r7, pc}
 8009486:	bf00      	nop
 8009488:	20000d34 	.word	0x20000d34

0800948c <prvInsertTimerInActiveList>:
 800948c:	b580      	push	{r7, lr}
 800948e:	b086      	sub	sp, #24
 8009490:	af00      	add	r7, sp, #0
 8009492:	60f8      	str	r0, [r7, #12]
 8009494:	60b9      	str	r1, [r7, #8]
 8009496:	607a      	str	r2, [r7, #4]
 8009498:	603b      	str	r3, [r7, #0]
 800949a:	2300      	movs	r3, #0
 800949c:	617b      	str	r3, [r7, #20]
 800949e:	68fb      	ldr	r3, [r7, #12]
 80094a0:	68ba      	ldr	r2, [r7, #8]
 80094a2:	605a      	str	r2, [r3, #4]
 80094a4:	68fb      	ldr	r3, [r7, #12]
 80094a6:	68fa      	ldr	r2, [r7, #12]
 80094a8:	611a      	str	r2, [r3, #16]
 80094aa:	68ba      	ldr	r2, [r7, #8]
 80094ac:	687b      	ldr	r3, [r7, #4]
 80094ae:	429a      	cmp	r2, r3
 80094b0:	d812      	bhi.n	80094d8 <prvInsertTimerInActiveList+0x4c>
 80094b2:	687a      	ldr	r2, [r7, #4]
 80094b4:	683b      	ldr	r3, [r7, #0]
 80094b6:	1ad2      	subs	r2, r2, r3
 80094b8:	68fb      	ldr	r3, [r7, #12]
 80094ba:	699b      	ldr	r3, [r3, #24]
 80094bc:	429a      	cmp	r2, r3
 80094be:	d302      	bcc.n	80094c6 <prvInsertTimerInActiveList+0x3a>
 80094c0:	2301      	movs	r3, #1
 80094c2:	617b      	str	r3, [r7, #20]
 80094c4:	e01b      	b.n	80094fe <prvInsertTimerInActiveList+0x72>
 80094c6:	4b10      	ldr	r3, [pc, #64]	; (8009508 <prvInsertTimerInActiveList+0x7c>)
 80094c8:	681a      	ldr	r2, [r3, #0]
 80094ca:	68fb      	ldr	r3, [r7, #12]
 80094cc:	3304      	adds	r3, #4
 80094ce:	4619      	mov	r1, r3
 80094d0:	4610      	mov	r0, r2
 80094d2:	f7fd fd9b 	bl	800700c <vListInsert>
 80094d6:	e012      	b.n	80094fe <prvInsertTimerInActiveList+0x72>
 80094d8:	687a      	ldr	r2, [r7, #4]
 80094da:	683b      	ldr	r3, [r7, #0]
 80094dc:	429a      	cmp	r2, r3
 80094de:	d206      	bcs.n	80094ee <prvInsertTimerInActiveList+0x62>
 80094e0:	68ba      	ldr	r2, [r7, #8]
 80094e2:	683b      	ldr	r3, [r7, #0]
 80094e4:	429a      	cmp	r2, r3
 80094e6:	d302      	bcc.n	80094ee <prvInsertTimerInActiveList+0x62>
 80094e8:	2301      	movs	r3, #1
 80094ea:	617b      	str	r3, [r7, #20]
 80094ec:	e007      	b.n	80094fe <prvInsertTimerInActiveList+0x72>
 80094ee:	4b07      	ldr	r3, [pc, #28]	; (800950c <prvInsertTimerInActiveList+0x80>)
 80094f0:	681a      	ldr	r2, [r3, #0]
 80094f2:	68fb      	ldr	r3, [r7, #12]
 80094f4:	3304      	adds	r3, #4
 80094f6:	4619      	mov	r1, r3
 80094f8:	4610      	mov	r0, r2
 80094fa:	f7fd fd87 	bl	800700c <vListInsert>
 80094fe:	697b      	ldr	r3, [r7, #20]
 8009500:	4618      	mov	r0, r3
 8009502:	3718      	adds	r7, #24
 8009504:	46bd      	mov	sp, r7
 8009506:	bd80      	pop	{r7, pc}
 8009508:	20000d28 	.word	0x20000d28
 800950c:	20000d24 	.word	0x20000d24

08009510 <prvProcessReceivedCommands>:
 8009510:	b580      	push	{r7, lr}
 8009512:	b08c      	sub	sp, #48	; 0x30
 8009514:	af02      	add	r7, sp, #8
 8009516:	e07f      	b.n	8009618 <prvProcessReceivedCommands+0x108>
 8009518:	68bb      	ldr	r3, [r7, #8]
 800951a:	2b00      	cmp	r3, #0
 800951c:	db7b      	blt.n	8009616 <prvProcessReceivedCommands+0x106>
 800951e:	693b      	ldr	r3, [r7, #16]
 8009520:	627b      	str	r3, [r7, #36]	; 0x24
 8009522:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009524:	695b      	ldr	r3, [r3, #20]
 8009526:	2b00      	cmp	r3, #0
 8009528:	d004      	beq.n	8009534 <prvProcessReceivedCommands+0x24>
 800952a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800952c:	3304      	adds	r3, #4
 800952e:	4618      	mov	r0, r3
 8009530:	f7fd fda5 	bl	800707e <uxListRemove>
 8009534:	1d3b      	adds	r3, r7, #4
 8009536:	4618      	mov	r0, r3
 8009538:	f7ff ff88 	bl	800944c <prvSampleTimeNow>
 800953c:	6238      	str	r0, [r7, #32]
 800953e:	68bb      	ldr	r3, [r7, #8]
 8009540:	2b09      	cmp	r3, #9
 8009542:	d869      	bhi.n	8009618 <prvProcessReceivedCommands+0x108>
 8009544:	a201      	add	r2, pc, #4	; (adr r2, 800954c <prvProcessReceivedCommands+0x3c>)
 8009546:	f852 f023 	ldr.w	pc, [r2, r3, lsl #2]
 800954a:	bf00      	nop
 800954c:	08009575 	.word	0x08009575
 8009550:	08009575 	.word	0x08009575
 8009554:	08009575 	.word	0x08009575
 8009558:	08009619 	.word	0x08009619
 800955c:	080095cf 	.word	0x080095cf
 8009560:	08009605 	.word	0x08009605
 8009564:	08009575 	.word	0x08009575
 8009568:	08009575 	.word	0x08009575
 800956c:	08009619 	.word	0x08009619
 8009570:	080095cf 	.word	0x080095cf
 8009574:	68fa      	ldr	r2, [r7, #12]
 8009576:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009578:	699b      	ldr	r3, [r3, #24]
 800957a:	18d1      	adds	r1, r2, r3
 800957c:	68fb      	ldr	r3, [r7, #12]
 800957e:	6a3a      	ldr	r2, [r7, #32]
 8009580:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8009582:	f7ff ff83 	bl	800948c <prvInsertTimerInActiveList>
 8009586:	4603      	mov	r3, r0
 8009588:	2b00      	cmp	r3, #0
 800958a:	d045      	beq.n	8009618 <prvProcessReceivedCommands+0x108>
 800958c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800958e:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8009590:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8009592:	4798      	blx	r3
 8009594:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009596:	69db      	ldr	r3, [r3, #28]
 8009598:	2b01      	cmp	r3, #1
 800959a:	d13d      	bne.n	8009618 <prvProcessReceivedCommands+0x108>
 800959c:	68fa      	ldr	r2, [r7, #12]
 800959e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80095a0:	699b      	ldr	r3, [r3, #24]
 80095a2:	441a      	add	r2, r3
 80095a4:	2300      	movs	r3, #0
 80095a6:	9300      	str	r3, [sp, #0]
 80095a8:	2300      	movs	r3, #0
 80095aa:	2100      	movs	r1, #0
 80095ac:	6a78      	ldr	r0, [r7, #36]	; 0x24
 80095ae:	f7ff fe3d 	bl	800922c <xTimerGenericCommand>
 80095b2:	61f8      	str	r0, [r7, #28]
 80095b4:	69fb      	ldr	r3, [r7, #28]
 80095b6:	2b00      	cmp	r3, #0
 80095b8:	d12e      	bne.n	8009618 <prvProcessReceivedCommands+0x108>
 80095ba:	f04f 0350 	mov.w	r3, #80	; 0x50
 80095be:	f383 8811 	msr	BASEPRI, r3
 80095c2:	f3bf 8f6f 	isb	sy
 80095c6:	f3bf 8f4f 	dsb	sy
 80095ca:	61bb      	str	r3, [r7, #24]
 80095cc:	e7fe      	b.n	80095cc <prvProcessReceivedCommands+0xbc>
 80095ce:	68fa      	ldr	r2, [r7, #12]
 80095d0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80095d2:	619a      	str	r2, [r3, #24]
 80095d4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80095d6:	699b      	ldr	r3, [r3, #24]
 80095d8:	2b00      	cmp	r3, #0
 80095da:	d109      	bne.n	80095f0 <prvProcessReceivedCommands+0xe0>
 80095dc:	f04f 0350 	mov.w	r3, #80	; 0x50
 80095e0:	f383 8811 	msr	BASEPRI, r3
 80095e4:	f3bf 8f6f 	isb	sy
 80095e8:	f3bf 8f4f 	dsb	sy
 80095ec:	617b      	str	r3, [r7, #20]
 80095ee:	e7fe      	b.n	80095ee <prvProcessReceivedCommands+0xde>
 80095f0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80095f2:	699a      	ldr	r2, [r3, #24]
 80095f4:	6a3b      	ldr	r3, [r7, #32]
 80095f6:	18d1      	adds	r1, r2, r3
 80095f8:	6a3b      	ldr	r3, [r7, #32]
 80095fa:	6a3a      	ldr	r2, [r7, #32]
 80095fc:	6a78      	ldr	r0, [r7, #36]	; 0x24
 80095fe:	f7ff ff45 	bl	800948c <prvInsertTimerInActiveList>
 8009602:	e009      	b.n	8009618 <prvProcessReceivedCommands+0x108>
 8009604:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009606:	f893 302c 	ldrb.w	r3, [r3, #44]	; 0x2c
 800960a:	2b00      	cmp	r3, #0
 800960c:	d104      	bne.n	8009618 <prvProcessReceivedCommands+0x108>
 800960e:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8009610:	f000 fbbc 	bl	8009d8c <vPortFree>
 8009614:	e000      	b.n	8009618 <prvProcessReceivedCommands+0x108>
 8009616:	bf00      	nop
 8009618:	4b07      	ldr	r3, [pc, #28]	; (8009638 <prvProcessReceivedCommands+0x128>)
 800961a:	681b      	ldr	r3, [r3, #0]
 800961c:	f107 0108 	add.w	r1, r7, #8
 8009620:	2200      	movs	r2, #0
 8009622:	4618      	mov	r0, r3
 8009624:	f7fe f818 	bl	8007658 <xQueueReceive>
 8009628:	4603      	mov	r3, r0
 800962a:	2b00      	cmp	r3, #0
 800962c:	f47f af74 	bne.w	8009518 <prvProcessReceivedCommands+0x8>
 8009630:	bf00      	nop
 8009632:	3728      	adds	r7, #40	; 0x28
 8009634:	46bd      	mov	sp, r7
 8009636:	bd80      	pop	{r7, pc}
 8009638:	20000d2c 	.word	0x20000d2c

0800963c <prvSwitchTimerLists>:
 800963c:	b580      	push	{r7, lr}
 800963e:	b088      	sub	sp, #32
 8009640:	af02      	add	r7, sp, #8
 8009642:	e044      	b.n	80096ce <prvSwitchTimerLists+0x92>
 8009644:	4b2b      	ldr	r3, [pc, #172]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 8009646:	681b      	ldr	r3, [r3, #0]
 8009648:	68db      	ldr	r3, [r3, #12]
 800964a:	681b      	ldr	r3, [r3, #0]
 800964c:	617b      	str	r3, [r7, #20]
 800964e:	4b29      	ldr	r3, [pc, #164]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 8009650:	681b      	ldr	r3, [r3, #0]
 8009652:	68db      	ldr	r3, [r3, #12]
 8009654:	68db      	ldr	r3, [r3, #12]
 8009656:	613b      	str	r3, [r7, #16]
 8009658:	693b      	ldr	r3, [r7, #16]
 800965a:	3304      	adds	r3, #4
 800965c:	4618      	mov	r0, r3
 800965e:	f7fd fd0e 	bl	800707e <uxListRemove>
 8009662:	693b      	ldr	r3, [r7, #16]
 8009664:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8009666:	6938      	ldr	r0, [r7, #16]
 8009668:	4798      	blx	r3
 800966a:	693b      	ldr	r3, [r7, #16]
 800966c:	69db      	ldr	r3, [r3, #28]
 800966e:	2b01      	cmp	r3, #1
 8009670:	d12d      	bne.n	80096ce <prvSwitchTimerLists+0x92>
 8009672:	693b      	ldr	r3, [r7, #16]
 8009674:	699b      	ldr	r3, [r3, #24]
 8009676:	697a      	ldr	r2, [r7, #20]
 8009678:	4413      	add	r3, r2
 800967a:	60fb      	str	r3, [r7, #12]
 800967c:	68fa      	ldr	r2, [r7, #12]
 800967e:	697b      	ldr	r3, [r7, #20]
 8009680:	429a      	cmp	r2, r3
 8009682:	d90e      	bls.n	80096a2 <prvSwitchTimerLists+0x66>
 8009684:	693b      	ldr	r3, [r7, #16]
 8009686:	68fa      	ldr	r2, [r7, #12]
 8009688:	605a      	str	r2, [r3, #4]
 800968a:	693b      	ldr	r3, [r7, #16]
 800968c:	693a      	ldr	r2, [r7, #16]
 800968e:	611a      	str	r2, [r3, #16]
 8009690:	4b18      	ldr	r3, [pc, #96]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 8009692:	681a      	ldr	r2, [r3, #0]
 8009694:	693b      	ldr	r3, [r7, #16]
 8009696:	3304      	adds	r3, #4
 8009698:	4619      	mov	r1, r3
 800969a:	4610      	mov	r0, r2
 800969c:	f7fd fcb6 	bl	800700c <vListInsert>
 80096a0:	e015      	b.n	80096ce <prvSwitchTimerLists+0x92>
 80096a2:	2300      	movs	r3, #0
 80096a4:	9300      	str	r3, [sp, #0]
 80096a6:	2300      	movs	r3, #0
 80096a8:	697a      	ldr	r2, [r7, #20]
 80096aa:	2100      	movs	r1, #0
 80096ac:	6938      	ldr	r0, [r7, #16]
 80096ae:	f7ff fdbd 	bl	800922c <xTimerGenericCommand>
 80096b2:	60b8      	str	r0, [r7, #8]
 80096b4:	68bb      	ldr	r3, [r7, #8]
 80096b6:	2b00      	cmp	r3, #0
 80096b8:	d109      	bne.n	80096ce <prvSwitchTimerLists+0x92>
 80096ba:	f04f 0350 	mov.w	r3, #80	; 0x50
 80096be:	f383 8811 	msr	BASEPRI, r3
 80096c2:	f3bf 8f6f 	isb	sy
 80096c6:	f3bf 8f4f 	dsb	sy
 80096ca:	603b      	str	r3, [r7, #0]
 80096cc:	e7fe      	b.n	80096cc <prvSwitchTimerLists+0x90>
 80096ce:	4b09      	ldr	r3, [pc, #36]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 80096d0:	681b      	ldr	r3, [r3, #0]
 80096d2:	681b      	ldr	r3, [r3, #0]
 80096d4:	2b00      	cmp	r3, #0
 80096d6:	d1b5      	bne.n	8009644 <prvSwitchTimerLists+0x8>
 80096d8:	4b06      	ldr	r3, [pc, #24]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 80096da:	681b      	ldr	r3, [r3, #0]
 80096dc:	607b      	str	r3, [r7, #4]
 80096de:	4b06      	ldr	r3, [pc, #24]	; (80096f8 <prvSwitchTimerLists+0xbc>)
 80096e0:	681b      	ldr	r3, [r3, #0]
 80096e2:	4a04      	ldr	r2, [pc, #16]	; (80096f4 <prvSwitchTimerLists+0xb8>)
 80096e4:	6013      	str	r3, [r2, #0]
 80096e6:	4a04      	ldr	r2, [pc, #16]	; (80096f8 <prvSwitchTimerLists+0xbc>)
 80096e8:	687b      	ldr	r3, [r7, #4]
 80096ea:	6013      	str	r3, [r2, #0]
 80096ec:	bf00      	nop
 80096ee:	3718      	adds	r7, #24
 80096f0:	46bd      	mov	sp, r7
 80096f2:	bd80      	pop	{r7, pc}
 80096f4:	20000d24 	.word	0x20000d24
 80096f8:	20000d28 	.word	0x20000d28

080096fc <prvCheckForValidListAndQueue>:
 80096fc:	b580      	push	{r7, lr}
 80096fe:	b082      	sub	sp, #8
 8009700:	af02      	add	r7, sp, #8
 8009702:	f000 f963 	bl	80099cc <vPortEnterCritical>
 8009706:	4b15      	ldr	r3, [pc, #84]	; (800975c <prvCheckForValidListAndQueue+0x60>)
 8009708:	681b      	ldr	r3, [r3, #0]
 800970a:	2b00      	cmp	r3, #0
 800970c:	d120      	bne.n	8009750 <prvCheckForValidListAndQueue+0x54>
 800970e:	4814      	ldr	r0, [pc, #80]	; (8009760 <prvCheckForValidListAndQueue+0x64>)
 8009710:	f7fd fc2b 	bl	8006f6a <vListInitialise>
 8009714:	4813      	ldr	r0, [pc, #76]	; (8009764 <prvCheckForValidListAndQueue+0x68>)
 8009716:	f7fd fc28 	bl	8006f6a <vListInitialise>
 800971a:	4b13      	ldr	r3, [pc, #76]	; (8009768 <prvCheckForValidListAndQueue+0x6c>)
 800971c:	4a10      	ldr	r2, [pc, #64]	; (8009760 <prvCheckForValidListAndQueue+0x64>)
 800971e:	601a      	str	r2, [r3, #0]
 8009720:	4b12      	ldr	r3, [pc, #72]	; (800976c <prvCheckForValidListAndQueue+0x70>)
 8009722:	4a10      	ldr	r2, [pc, #64]	; (8009764 <prvCheckForValidListAndQueue+0x68>)
 8009724:	601a      	str	r2, [r3, #0]
 8009726:	2300      	movs	r3, #0
 8009728:	9300      	str	r3, [sp, #0]
 800972a:	4b11      	ldr	r3, [pc, #68]	; (8009770 <prvCheckForValidListAndQueue+0x74>)
 800972c:	4a11      	ldr	r2, [pc, #68]	; (8009774 <prvCheckForValidListAndQueue+0x78>)
 800972e:	210c      	movs	r1, #12
 8009730:	200a      	movs	r0, #10
 8009732:	f7fd fd37 	bl	80071a4 <xQueueGenericCreateStatic>
 8009736:	4602      	mov	r2, r0
 8009738:	4b08      	ldr	r3, [pc, #32]	; (800975c <prvCheckForValidListAndQueue+0x60>)
 800973a:	601a      	str	r2, [r3, #0]
 800973c:	4b07      	ldr	r3, [pc, #28]	; (800975c <prvCheckForValidListAndQueue+0x60>)
 800973e:	681b      	ldr	r3, [r3, #0]
 8009740:	2b00      	cmp	r3, #0
 8009742:	d005      	beq.n	8009750 <prvCheckForValidListAndQueue+0x54>
 8009744:	4b05      	ldr	r3, [pc, #20]	; (800975c <prvCheckForValidListAndQueue+0x60>)
 8009746:	681b      	ldr	r3, [r3, #0]
 8009748:	490b      	ldr	r1, [pc, #44]	; (8009778 <prvCheckForValidListAndQueue+0x7c>)
 800974a:	4618      	mov	r0, r3
 800974c:	f7fe fa90 	bl	8007c70 <vQueueAddToRegistry>
 8009750:	f000 f96a 	bl	8009a28 <vPortExitCritical>
 8009754:	bf00      	nop
 8009756:	46bd      	mov	sp, r7
 8009758:	bd80      	pop	{r7, pc}
 800975a:	bf00      	nop
 800975c:	20000d2c 	.word	0x20000d2c
 8009760:	20000cfc 	.word	0x20000cfc
 8009764:	20000d10 	.word	0x20000d10
 8009768:	20000d24 	.word	0x20000d24
 800976c:	20000d28 	.word	0x20000d28
 8009770:	20000db0 	.word	0x20000db0
 8009774:	20000d38 	.word	0x20000d38
 8009778:	0800d110 	.word	0x0800d110

0800977c <pxPortInitialiseStack>:
 800977c:	b480      	push	{r7}
 800977e:	b085      	sub	sp, #20
 8009780:	af00      	add	r7, sp, #0
 8009782:	60f8      	str	r0, [r7, #12]
 8009784:	60b9      	str	r1, [r7, #8]
 8009786:	607a      	str	r2, [r7, #4]
 8009788:	68fb      	ldr	r3, [r7, #12]
 800978a:	3b04      	subs	r3, #4
 800978c:	60fb      	str	r3, [r7, #12]
 800978e:	68fb      	ldr	r3, [r7, #12]
 8009790:	f04f 7280 	mov.w	r2, #16777216	; 0x1000000
 8009794:	601a      	str	r2, [r3, #0]
 8009796:	68fb      	ldr	r3, [r7, #12]
 8009798:	3b04      	subs	r3, #4
 800979a:	60fb      	str	r3, [r7, #12]
 800979c:	68bb      	ldr	r3, [r7, #8]
 800979e:	f023 0201 	bic.w	r2, r3, #1
 80097a2:	68fb      	ldr	r3, [r7, #12]
 80097a4:	601a      	str	r2, [r3, #0]
 80097a6:	68fb      	ldr	r3, [r7, #12]
 80097a8:	3b04      	subs	r3, #4
 80097aa:	60fb      	str	r3, [r7, #12]
 80097ac:	4a0c      	ldr	r2, [pc, #48]	; (80097e0 <pxPortInitialiseStack+0x64>)
 80097ae:	68fb      	ldr	r3, [r7, #12]
 80097b0:	601a      	str	r2, [r3, #0]
 80097b2:	68fb      	ldr	r3, [r7, #12]
 80097b4:	3b14      	subs	r3, #20
 80097b6:	60fb      	str	r3, [r7, #12]
 80097b8:	687a      	ldr	r2, [r7, #4]
 80097ba:	68fb      	ldr	r3, [r7, #12]
 80097bc:	601a      	str	r2, [r3, #0]
 80097be:	68fb      	ldr	r3, [r7, #12]
 80097c0:	3b04      	subs	r3, #4
 80097c2:	60fb      	str	r3, [r7, #12]
 80097c4:	68fb      	ldr	r3, [r7, #12]
 80097c6:	f06f 0202 	mvn.w	r2, #2
 80097ca:	601a      	str	r2, [r3, #0]
 80097cc:	68fb      	ldr	r3, [r7, #12]
 80097ce:	3b20      	subs	r3, #32
 80097d0:	60fb      	str	r3, [r7, #12]
 80097d2:	68fb      	ldr	r3, [r7, #12]
 80097d4:	4618      	mov	r0, r3
 80097d6:	3714      	adds	r7, #20
 80097d8:	46bd      	mov	sp, r7
 80097da:	f85d 7b04 	ldr.w	r7, [sp], #4
 80097de:	4770      	bx	lr
 80097e0:	080097e5 	.word	0x080097e5

080097e4 <prvTaskExitError>:
 80097e4:	b480      	push	{r7}
 80097e6:	b085      	sub	sp, #20
 80097e8:	af00      	add	r7, sp, #0
 80097ea:	2300      	movs	r3, #0
 80097ec:	607b      	str	r3, [r7, #4]
 80097ee:	4b11      	ldr	r3, [pc, #68]	; (8009834 <prvTaskExitError+0x50>)
 80097f0:	681b      	ldr	r3, [r3, #0]
 80097f2:	f1b3 3fff 	cmp.w	r3, #4294967295
 80097f6:	d009      	beq.n	800980c <prvTaskExitError+0x28>
 80097f8:	f04f 0350 	mov.w	r3, #80	; 0x50
 80097fc:	f383 8811 	msr	BASEPRI, r3
 8009800:	f3bf 8f6f 	isb	sy
 8009804:	f3bf 8f4f 	dsb	sy
 8009808:	60fb      	str	r3, [r7, #12]
 800980a:	e7fe      	b.n	800980a <prvTaskExitError+0x26>
 800980c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009810:	f383 8811 	msr	BASEPRI, r3
 8009814:	f3bf 8f6f 	isb	sy
 8009818:	f3bf 8f4f 	dsb	sy
 800981c:	60bb      	str	r3, [r7, #8]
 800981e:	bf00      	nop
 8009820:	687b      	ldr	r3, [r7, #4]
 8009822:	2b00      	cmp	r3, #0
 8009824:	d0fc      	beq.n	8009820 <prvTaskExitError+0x3c>
 8009826:	bf00      	nop
 8009828:	3714      	adds	r7, #20
 800982a:	46bd      	mov	sp, r7
 800982c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009830:	4770      	bx	lr
 8009832:	bf00      	nop
 8009834:	20000078 	.word	0x20000078
	...

08009840 <SVC_Handler>:
 8009840:	4b07      	ldr	r3, [pc, #28]	; (8009860 <pxCurrentTCBConst2>)
 8009842:	6819      	ldr	r1, [r3, #0]
 8009844:	6808      	ldr	r0, [r1, #0]
 8009846:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800984a:	f380 8809 	msr	PSP, r0
 800984e:	f3bf 8f6f 	isb	sy
 8009852:	f04f 0000 	mov.w	r0, #0
 8009856:	f380 8811 	msr	BASEPRI, r0
 800985a:	4770      	bx	lr
 800985c:	f3af 8000 	nop.w

08009860 <pxCurrentTCBConst2>:
 8009860:	20000bf8 	.word	0x20000bf8
 8009864:	bf00      	nop
 8009866:	bf00      	nop

08009868 <prvPortStartFirstTask>:
 8009868:	4808      	ldr	r0, [pc, #32]	; (800988c <prvPortStartFirstTask+0x24>)
 800986a:	6800      	ldr	r0, [r0, #0]
 800986c:	6800      	ldr	r0, [r0, #0]
 800986e:	f380 8808 	msr	MSP, r0
 8009872:	f04f 0000 	mov.w	r0, #0
 8009876:	f380 8814 	msr	CONTROL, r0
 800987a:	b662      	cpsie	i
 800987c:	b661      	cpsie	f
 800987e:	f3bf 8f4f 	dsb	sy
 8009882:	f3bf 8f6f 	isb	sy
 8009886:	df00      	svc	0
 8009888:	bf00      	nop
 800988a:	bf00      	nop
 800988c:	e000ed08 	.word	0xe000ed08

08009890 <xPortStartScheduler>:
 8009890:	b580      	push	{r7, lr}
 8009892:	b086      	sub	sp, #24
 8009894:	af00      	add	r7, sp, #0
 8009896:	4b44      	ldr	r3, [pc, #272]	; (80099a8 <xPortStartScheduler+0x118>)
 8009898:	681b      	ldr	r3, [r3, #0]
 800989a:	4a44      	ldr	r2, [pc, #272]	; (80099ac <xPortStartScheduler+0x11c>)
 800989c:	4293      	cmp	r3, r2
 800989e:	d109      	bne.n	80098b4 <xPortStartScheduler+0x24>
 80098a0:	f04f 0350 	mov.w	r3, #80	; 0x50
 80098a4:	f383 8811 	msr	BASEPRI, r3
 80098a8:	f3bf 8f6f 	isb	sy
 80098ac:	f3bf 8f4f 	dsb	sy
 80098b0:	613b      	str	r3, [r7, #16]
 80098b2:	e7fe      	b.n	80098b2 <xPortStartScheduler+0x22>
 80098b4:	4b3c      	ldr	r3, [pc, #240]	; (80099a8 <xPortStartScheduler+0x118>)
 80098b6:	681b      	ldr	r3, [r3, #0]
 80098b8:	4a3d      	ldr	r2, [pc, #244]	; (80099b0 <xPortStartScheduler+0x120>)
 80098ba:	4293      	cmp	r3, r2
 80098bc:	d109      	bne.n	80098d2 <xPortStartScheduler+0x42>
 80098be:	f04f 0350 	mov.w	r3, #80	; 0x50
 80098c2:	f383 8811 	msr	BASEPRI, r3
 80098c6:	f3bf 8f6f 	isb	sy
 80098ca:	f3bf 8f4f 	dsb	sy
 80098ce:	60fb      	str	r3, [r7, #12]
 80098d0:	e7fe      	b.n	80098d0 <xPortStartScheduler+0x40>
 80098d2:	4b38      	ldr	r3, [pc, #224]	; (80099b4 <xPortStartScheduler+0x124>)
 80098d4:	617b      	str	r3, [r7, #20]
 80098d6:	697b      	ldr	r3, [r7, #20]
 80098d8:	781b      	ldrb	r3, [r3, #0]
 80098da:	b2db      	uxtb	r3, r3
 80098dc:	607b      	str	r3, [r7, #4]
 80098de:	697b      	ldr	r3, [r7, #20]
 80098e0:	22ff      	movs	r2, #255	; 0xff
 80098e2:	701a      	strb	r2, [r3, #0]
 80098e4:	697b      	ldr	r3, [r7, #20]
 80098e6:	781b      	ldrb	r3, [r3, #0]
 80098e8:	b2db      	uxtb	r3, r3
 80098ea:	70fb      	strb	r3, [r7, #3]
 80098ec:	78fb      	ldrb	r3, [r7, #3]
 80098ee:	b2db      	uxtb	r3, r3
 80098f0:	f003 0350 	and.w	r3, r3, #80	; 0x50
 80098f4:	b2da      	uxtb	r2, r3
 80098f6:	4b30      	ldr	r3, [pc, #192]	; (80099b8 <xPortStartScheduler+0x128>)
 80098f8:	701a      	strb	r2, [r3, #0]
 80098fa:	4b30      	ldr	r3, [pc, #192]	; (80099bc <xPortStartScheduler+0x12c>)
 80098fc:	2207      	movs	r2, #7
 80098fe:	601a      	str	r2, [r3, #0]
 8009900:	e009      	b.n	8009916 <xPortStartScheduler+0x86>
 8009902:	4b2e      	ldr	r3, [pc, #184]	; (80099bc <xPortStartScheduler+0x12c>)
 8009904:	681b      	ldr	r3, [r3, #0]
 8009906:	3b01      	subs	r3, #1
 8009908:	4a2c      	ldr	r2, [pc, #176]	; (80099bc <xPortStartScheduler+0x12c>)
 800990a:	6013      	str	r3, [r2, #0]
 800990c:	78fb      	ldrb	r3, [r7, #3]
 800990e:	b2db      	uxtb	r3, r3
 8009910:	005b      	lsls	r3, r3, #1
 8009912:	b2db      	uxtb	r3, r3
 8009914:	70fb      	strb	r3, [r7, #3]
 8009916:	78fb      	ldrb	r3, [r7, #3]
 8009918:	b2db      	uxtb	r3, r3
 800991a:	f003 0380 	and.w	r3, r3, #128	; 0x80
 800991e:	2b80      	cmp	r3, #128	; 0x80
 8009920:	d0ef      	beq.n	8009902 <xPortStartScheduler+0x72>
 8009922:	4b26      	ldr	r3, [pc, #152]	; (80099bc <xPortStartScheduler+0x12c>)
 8009924:	681b      	ldr	r3, [r3, #0]
 8009926:	f1c3 0307 	rsb	r3, r3, #7
 800992a:	2b04      	cmp	r3, #4
 800992c:	d009      	beq.n	8009942 <xPortStartScheduler+0xb2>
 800992e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009932:	f383 8811 	msr	BASEPRI, r3
 8009936:	f3bf 8f6f 	isb	sy
 800993a:	f3bf 8f4f 	dsb	sy
 800993e:	60bb      	str	r3, [r7, #8]
 8009940:	e7fe      	b.n	8009940 <xPortStartScheduler+0xb0>
 8009942:	4b1e      	ldr	r3, [pc, #120]	; (80099bc <xPortStartScheduler+0x12c>)
 8009944:	681b      	ldr	r3, [r3, #0]
 8009946:	021b      	lsls	r3, r3, #8
 8009948:	4a1c      	ldr	r2, [pc, #112]	; (80099bc <xPortStartScheduler+0x12c>)
 800994a:	6013      	str	r3, [r2, #0]
 800994c:	4b1b      	ldr	r3, [pc, #108]	; (80099bc <xPortStartScheduler+0x12c>)
 800994e:	681b      	ldr	r3, [r3, #0]
 8009950:	f403 63e0 	and.w	r3, r3, #1792	; 0x700
 8009954:	4a19      	ldr	r2, [pc, #100]	; (80099bc <xPortStartScheduler+0x12c>)
 8009956:	6013      	str	r3, [r2, #0]
 8009958:	687b      	ldr	r3, [r7, #4]
 800995a:	b2da      	uxtb	r2, r3
 800995c:	697b      	ldr	r3, [r7, #20]
 800995e:	701a      	strb	r2, [r3, #0]
 8009960:	4b17      	ldr	r3, [pc, #92]	; (80099c0 <xPortStartScheduler+0x130>)
 8009962:	681b      	ldr	r3, [r3, #0]
 8009964:	4a16      	ldr	r2, [pc, #88]	; (80099c0 <xPortStartScheduler+0x130>)
 8009966:	f443 0370 	orr.w	r3, r3, #15728640	; 0xf00000
 800996a:	6013      	str	r3, [r2, #0]
 800996c:	4b14      	ldr	r3, [pc, #80]	; (80099c0 <xPortStartScheduler+0x130>)
 800996e:	681b      	ldr	r3, [r3, #0]
 8009970:	4a13      	ldr	r2, [pc, #76]	; (80099c0 <xPortStartScheduler+0x130>)
 8009972:	f043 4370 	orr.w	r3, r3, #4026531840	; 0xf0000000
 8009976:	6013      	str	r3, [r2, #0]
 8009978:	f000 f8d6 	bl	8009b28 <vPortSetupTimerInterrupt>
 800997c:	4b11      	ldr	r3, [pc, #68]	; (80099c4 <xPortStartScheduler+0x134>)
 800997e:	2200      	movs	r2, #0
 8009980:	601a      	str	r2, [r3, #0]
 8009982:	f000 f8f5 	bl	8009b70 <vPortEnableVFP>
 8009986:	4b10      	ldr	r3, [pc, #64]	; (80099c8 <xPortStartScheduler+0x138>)
 8009988:	681b      	ldr	r3, [r3, #0]
 800998a:	4a0f      	ldr	r2, [pc, #60]	; (80099c8 <xPortStartScheduler+0x138>)
 800998c:	f043 4340 	orr.w	r3, r3, #3221225472	; 0xc0000000
 8009990:	6013      	str	r3, [r2, #0]
 8009992:	f7ff ff69 	bl	8009868 <prvPortStartFirstTask>
 8009996:	f7fe fd93 	bl	80084c0 <vTaskSwitchContext>
 800999a:	f7ff ff23 	bl	80097e4 <prvTaskExitError>
 800999e:	2300      	movs	r3, #0
 80099a0:	4618      	mov	r0, r3
 80099a2:	3718      	adds	r7, #24
 80099a4:	46bd      	mov	sp, r7
 80099a6:	bd80      	pop	{r7, pc}
 80099a8:	e000ed00 	.word	0xe000ed00
 80099ac:	410fc271 	.word	0x410fc271
 80099b0:	410fc270 	.word	0x410fc270
 80099b4:	e000e400 	.word	0xe000e400
 80099b8:	20000e00 	.word	0x20000e00
 80099bc:	20000e04 	.word	0x20000e04
 80099c0:	e000ed20 	.word	0xe000ed20
 80099c4:	20000078 	.word	0x20000078
 80099c8:	e000ef34 	.word	0xe000ef34

080099cc <vPortEnterCritical>:
 80099cc:	b480      	push	{r7}
 80099ce:	b083      	sub	sp, #12
 80099d0:	af00      	add	r7, sp, #0
 80099d2:	f04f 0350 	mov.w	r3, #80	; 0x50
 80099d6:	f383 8811 	msr	BASEPRI, r3
 80099da:	f3bf 8f6f 	isb	sy
 80099de:	f3bf 8f4f 	dsb	sy
 80099e2:	607b      	str	r3, [r7, #4]
 80099e4:	4b0e      	ldr	r3, [pc, #56]	; (8009a20 <vPortEnterCritical+0x54>)
 80099e6:	681b      	ldr	r3, [r3, #0]
 80099e8:	3301      	adds	r3, #1
 80099ea:	4a0d      	ldr	r2, [pc, #52]	; (8009a20 <vPortEnterCritical+0x54>)
 80099ec:	6013      	str	r3, [r2, #0]
 80099ee:	4b0c      	ldr	r3, [pc, #48]	; (8009a20 <vPortEnterCritical+0x54>)
 80099f0:	681b      	ldr	r3, [r3, #0]
 80099f2:	2b01      	cmp	r3, #1
 80099f4:	d10e      	bne.n	8009a14 <vPortEnterCritical+0x48>
 80099f6:	4b0b      	ldr	r3, [pc, #44]	; (8009a24 <vPortEnterCritical+0x58>)
 80099f8:	681b      	ldr	r3, [r3, #0]
 80099fa:	b2db      	uxtb	r3, r3
 80099fc:	2b00      	cmp	r3, #0
 80099fe:	d009      	beq.n	8009a14 <vPortEnterCritical+0x48>
 8009a00:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009a04:	f383 8811 	msr	BASEPRI, r3
 8009a08:	f3bf 8f6f 	isb	sy
 8009a0c:	f3bf 8f4f 	dsb	sy
 8009a10:	603b      	str	r3, [r7, #0]
 8009a12:	e7fe      	b.n	8009a12 <vPortEnterCritical+0x46>
 8009a14:	bf00      	nop
 8009a16:	370c      	adds	r7, #12
 8009a18:	46bd      	mov	sp, r7
 8009a1a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009a1e:	4770      	bx	lr
 8009a20:	20000078 	.word	0x20000078
 8009a24:	e000ed04 	.word	0xe000ed04

08009a28 <vPortExitCritical>:
 8009a28:	b480      	push	{r7}
 8009a2a:	b083      	sub	sp, #12
 8009a2c:	af00      	add	r7, sp, #0
 8009a2e:	4b11      	ldr	r3, [pc, #68]	; (8009a74 <vPortExitCritical+0x4c>)
 8009a30:	681b      	ldr	r3, [r3, #0]
 8009a32:	2b00      	cmp	r3, #0
 8009a34:	d109      	bne.n	8009a4a <vPortExitCritical+0x22>
 8009a36:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009a3a:	f383 8811 	msr	BASEPRI, r3
 8009a3e:	f3bf 8f6f 	isb	sy
 8009a42:	f3bf 8f4f 	dsb	sy
 8009a46:	607b      	str	r3, [r7, #4]
 8009a48:	e7fe      	b.n	8009a48 <vPortExitCritical+0x20>
 8009a4a:	4b0a      	ldr	r3, [pc, #40]	; (8009a74 <vPortExitCritical+0x4c>)
 8009a4c:	681b      	ldr	r3, [r3, #0]
 8009a4e:	3b01      	subs	r3, #1
 8009a50:	4a08      	ldr	r2, [pc, #32]	; (8009a74 <vPortExitCritical+0x4c>)
 8009a52:	6013      	str	r3, [r2, #0]
 8009a54:	4b07      	ldr	r3, [pc, #28]	; (8009a74 <vPortExitCritical+0x4c>)
 8009a56:	681b      	ldr	r3, [r3, #0]
 8009a58:	2b00      	cmp	r3, #0
 8009a5a:	d104      	bne.n	8009a66 <vPortExitCritical+0x3e>
 8009a5c:	2300      	movs	r3, #0
 8009a5e:	603b      	str	r3, [r7, #0]
 8009a60:	683b      	ldr	r3, [r7, #0]
 8009a62:	f383 8811 	msr	BASEPRI, r3
 8009a66:	bf00      	nop
 8009a68:	370c      	adds	r7, #12
 8009a6a:	46bd      	mov	sp, r7
 8009a6c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009a70:	4770      	bx	lr
 8009a72:	bf00      	nop
 8009a74:	20000078 	.word	0x20000078
	...

08009a80 <PendSV_Handler>:
 8009a80:	f3ef 8009 	mrs	r0, PSP
 8009a84:	f3bf 8f6f 	isb	sy
 8009a88:	4b15      	ldr	r3, [pc, #84]	; (8009ae0 <pxCurrentTCBConst>)
 8009a8a:	681a      	ldr	r2, [r3, #0]
 8009a8c:	f01e 0f10 	tst.w	lr, #16
 8009a90:	bf08      	it	eq
 8009a92:	ed20 8a10 	vstmdbeq	r0!, {s16-s31}
 8009a96:	e920 4ff0 	stmdb	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8009a9a:	6010      	str	r0, [r2, #0]
 8009a9c:	e92d 0009 	stmdb	sp!, {r0, r3}
 8009aa0:	f04f 0050 	mov.w	r0, #80	; 0x50
 8009aa4:	f380 8811 	msr	BASEPRI, r0
 8009aa8:	f3bf 8f4f 	dsb	sy
 8009aac:	f3bf 8f6f 	isb	sy
 8009ab0:	f7fe fd06 	bl	80084c0 <vTaskSwitchContext>
 8009ab4:	f04f 0000 	mov.w	r0, #0
 8009ab8:	f380 8811 	msr	BASEPRI, r0
 8009abc:	bc09      	pop	{r0, r3}
 8009abe:	6819      	ldr	r1, [r3, #0]
 8009ac0:	6808      	ldr	r0, [r1, #0]
 8009ac2:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8009ac6:	f01e 0f10 	tst.w	lr, #16
 8009aca:	bf08      	it	eq
 8009acc:	ecb0 8a10 	vldmiaeq	r0!, {s16-s31}
 8009ad0:	f380 8809 	msr	PSP, r0
 8009ad4:	f3bf 8f6f 	isb	sy
 8009ad8:	4770      	bx	lr
 8009ada:	bf00      	nop
 8009adc:	f3af 8000 	nop.w

08009ae0 <pxCurrentTCBConst>:
 8009ae0:	20000bf8 	.word	0x20000bf8
 8009ae4:	bf00      	nop
 8009ae6:	bf00      	nop

08009ae8 <SysTick_Handler>:
 8009ae8:	b580      	push	{r7, lr}
 8009aea:	b082      	sub	sp, #8
 8009aec:	af00      	add	r7, sp, #0
 8009aee:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009af2:	f383 8811 	msr	BASEPRI, r3
 8009af6:	f3bf 8f6f 	isb	sy
 8009afa:	f3bf 8f4f 	dsb	sy
 8009afe:	607b      	str	r3, [r7, #4]
 8009b00:	f7fe fc1e 	bl	8008340 <xTaskIncrementTick>
 8009b04:	4603      	mov	r3, r0
 8009b06:	2b00      	cmp	r3, #0
 8009b08:	d003      	beq.n	8009b12 <SysTick_Handler+0x2a>
 8009b0a:	4b06      	ldr	r3, [pc, #24]	; (8009b24 <SysTick_Handler+0x3c>)
 8009b0c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8009b10:	601a      	str	r2, [r3, #0]
 8009b12:	2300      	movs	r3, #0
 8009b14:	603b      	str	r3, [r7, #0]
 8009b16:	683b      	ldr	r3, [r7, #0]
 8009b18:	f383 8811 	msr	BASEPRI, r3
 8009b1c:	bf00      	nop
 8009b1e:	3708      	adds	r7, #8
 8009b20:	46bd      	mov	sp, r7
 8009b22:	bd80      	pop	{r7, pc}
 8009b24:	e000ed04 	.word	0xe000ed04

08009b28 <vPortSetupTimerInterrupt>:
 8009b28:	b480      	push	{r7}
 8009b2a:	af00      	add	r7, sp, #0
 8009b2c:	4b0b      	ldr	r3, [pc, #44]	; (8009b5c <vPortSetupTimerInterrupt+0x34>)
 8009b2e:	2200      	movs	r2, #0
 8009b30:	601a      	str	r2, [r3, #0]
 8009b32:	4b0b      	ldr	r3, [pc, #44]	; (8009b60 <vPortSetupTimerInterrupt+0x38>)
 8009b34:	2200      	movs	r2, #0
 8009b36:	601a      	str	r2, [r3, #0]
 8009b38:	4b0a      	ldr	r3, [pc, #40]	; (8009b64 <vPortSetupTimerInterrupt+0x3c>)
 8009b3a:	681b      	ldr	r3, [r3, #0]
 8009b3c:	4a0a      	ldr	r2, [pc, #40]	; (8009b68 <vPortSetupTimerInterrupt+0x40>)
 8009b3e:	fba2 2303 	umull	r2, r3, r2, r3
 8009b42:	099b      	lsrs	r3, r3, #6
 8009b44:	4a09      	ldr	r2, [pc, #36]	; (8009b6c <vPortSetupTimerInterrupt+0x44>)
 8009b46:	3b01      	subs	r3, #1
 8009b48:	6013      	str	r3, [r2, #0]
 8009b4a:	4b04      	ldr	r3, [pc, #16]	; (8009b5c <vPortSetupTimerInterrupt+0x34>)
 8009b4c:	2207      	movs	r2, #7
 8009b4e:	601a      	str	r2, [r3, #0]
 8009b50:	bf00      	nop
 8009b52:	46bd      	mov	sp, r7
 8009b54:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009b58:	4770      	bx	lr
 8009b5a:	bf00      	nop
 8009b5c:	e000e010 	.word	0xe000e010
 8009b60:	e000e018 	.word	0xe000e018
 8009b64:	2000007c 	.word	0x2000007c
 8009b68:	10624dd3 	.word	0x10624dd3
 8009b6c:	e000e014 	.word	0xe000e014

08009b70 <vPortEnableVFP>:
 8009b70:	f8df 000c 	ldr.w	r0, [pc, #12]	; 8009b80 <vPortEnableVFP+0x10>
 8009b74:	6801      	ldr	r1, [r0, #0]
 8009b76:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8009b7a:	6001      	str	r1, [r0, #0]
 8009b7c:	4770      	bx	lr
 8009b7e:	bf00      	nop
 8009b80:	e000ed88 	.word	0xe000ed88

08009b84 <vPortValidateInterruptPriority>:
 8009b84:	b480      	push	{r7}
 8009b86:	b085      	sub	sp, #20
 8009b88:	af00      	add	r7, sp, #0
 8009b8a:	f3ef 8305 	mrs	r3, IPSR
 8009b8e:	60fb      	str	r3, [r7, #12]
 8009b90:	68fb      	ldr	r3, [r7, #12]
 8009b92:	2b0f      	cmp	r3, #15
 8009b94:	d913      	bls.n	8009bbe <vPortValidateInterruptPriority+0x3a>
 8009b96:	4a16      	ldr	r2, [pc, #88]	; (8009bf0 <vPortValidateInterruptPriority+0x6c>)
 8009b98:	68fb      	ldr	r3, [r7, #12]
 8009b9a:	4413      	add	r3, r2
 8009b9c:	781b      	ldrb	r3, [r3, #0]
 8009b9e:	72fb      	strb	r3, [r7, #11]
 8009ba0:	4b14      	ldr	r3, [pc, #80]	; (8009bf4 <vPortValidateInterruptPriority+0x70>)
 8009ba2:	781b      	ldrb	r3, [r3, #0]
 8009ba4:	7afa      	ldrb	r2, [r7, #11]
 8009ba6:	429a      	cmp	r2, r3
 8009ba8:	d209      	bcs.n	8009bbe <vPortValidateInterruptPriority+0x3a>
 8009baa:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009bae:	f383 8811 	msr	BASEPRI, r3
 8009bb2:	f3bf 8f6f 	isb	sy
 8009bb6:	f3bf 8f4f 	dsb	sy
 8009bba:	607b      	str	r3, [r7, #4]
 8009bbc:	e7fe      	b.n	8009bbc <vPortValidateInterruptPriority+0x38>
 8009bbe:	4b0e      	ldr	r3, [pc, #56]	; (8009bf8 <vPortValidateInterruptPriority+0x74>)
 8009bc0:	681b      	ldr	r3, [r3, #0]
 8009bc2:	f403 62e0 	and.w	r2, r3, #1792	; 0x700
 8009bc6:	4b0d      	ldr	r3, [pc, #52]	; (8009bfc <vPortValidateInterruptPriority+0x78>)
 8009bc8:	681b      	ldr	r3, [r3, #0]
 8009bca:	429a      	cmp	r2, r3
 8009bcc:	d909      	bls.n	8009be2 <vPortValidateInterruptPriority+0x5e>
 8009bce:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009bd2:	f383 8811 	msr	BASEPRI, r3
 8009bd6:	f3bf 8f6f 	isb	sy
 8009bda:	f3bf 8f4f 	dsb	sy
 8009bde:	603b      	str	r3, [r7, #0]
 8009be0:	e7fe      	b.n	8009be0 <vPortValidateInterruptPriority+0x5c>
 8009be2:	bf00      	nop
 8009be4:	3714      	adds	r7, #20
 8009be6:	46bd      	mov	sp, r7
 8009be8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009bec:	4770      	bx	lr
 8009bee:	bf00      	nop
 8009bf0:	e000e3f0 	.word	0xe000e3f0
 8009bf4:	20000e00 	.word	0x20000e00
 8009bf8:	e000ed0c 	.word	0xe000ed0c
 8009bfc:	20000e04 	.word	0x20000e04

08009c00 <pvPortMalloc>:
 8009c00:	b580      	push	{r7, lr}
 8009c02:	b08a      	sub	sp, #40	; 0x28
 8009c04:	af00      	add	r7, sp, #0
 8009c06:	6078      	str	r0, [r7, #4]
 8009c08:	2300      	movs	r3, #0
 8009c0a:	61fb      	str	r3, [r7, #28]
 8009c0c:	f7fe facc 	bl	80081a8 <vTaskSuspendAll>
 8009c10:	4b59      	ldr	r3, [pc, #356]	; (8009d78 <pvPortMalloc+0x178>)
 8009c12:	681b      	ldr	r3, [r3, #0]
 8009c14:	2b00      	cmp	r3, #0
 8009c16:	d101      	bne.n	8009c1c <pvPortMalloc+0x1c>
 8009c18:	f000 f910 	bl	8009e3c <prvHeapInit>
 8009c1c:	4b57      	ldr	r3, [pc, #348]	; (8009d7c <pvPortMalloc+0x17c>)
 8009c1e:	681a      	ldr	r2, [r3, #0]
 8009c20:	687b      	ldr	r3, [r7, #4]
 8009c22:	4013      	ands	r3, r2
 8009c24:	2b00      	cmp	r3, #0
 8009c26:	f040 808c 	bne.w	8009d42 <pvPortMalloc+0x142>
 8009c2a:	687b      	ldr	r3, [r7, #4]
 8009c2c:	2b00      	cmp	r3, #0
 8009c2e:	d01c      	beq.n	8009c6a <pvPortMalloc+0x6a>
 8009c30:	2208      	movs	r2, #8
 8009c32:	687b      	ldr	r3, [r7, #4]
 8009c34:	4413      	add	r3, r2
 8009c36:	607b      	str	r3, [r7, #4]
 8009c38:	687b      	ldr	r3, [r7, #4]
 8009c3a:	f003 0307 	and.w	r3, r3, #7
 8009c3e:	2b00      	cmp	r3, #0
 8009c40:	d013      	beq.n	8009c6a <pvPortMalloc+0x6a>
 8009c42:	687b      	ldr	r3, [r7, #4]
 8009c44:	f023 0307 	bic.w	r3, r3, #7
 8009c48:	3308      	adds	r3, #8
 8009c4a:	607b      	str	r3, [r7, #4]
 8009c4c:	687b      	ldr	r3, [r7, #4]
 8009c4e:	f003 0307 	and.w	r3, r3, #7
 8009c52:	2b00      	cmp	r3, #0
 8009c54:	d009      	beq.n	8009c6a <pvPortMalloc+0x6a>
 8009c56:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009c5a:	f383 8811 	msr	BASEPRI, r3
 8009c5e:	f3bf 8f6f 	isb	sy
 8009c62:	f3bf 8f4f 	dsb	sy
 8009c66:	617b      	str	r3, [r7, #20]
 8009c68:	e7fe      	b.n	8009c68 <pvPortMalloc+0x68>
 8009c6a:	687b      	ldr	r3, [r7, #4]
 8009c6c:	2b00      	cmp	r3, #0
 8009c6e:	d068      	beq.n	8009d42 <pvPortMalloc+0x142>
 8009c70:	4b43      	ldr	r3, [pc, #268]	; (8009d80 <pvPortMalloc+0x180>)
 8009c72:	681b      	ldr	r3, [r3, #0]
 8009c74:	687a      	ldr	r2, [r7, #4]
 8009c76:	429a      	cmp	r2, r3
 8009c78:	d863      	bhi.n	8009d42 <pvPortMalloc+0x142>
 8009c7a:	4b42      	ldr	r3, [pc, #264]	; (8009d84 <pvPortMalloc+0x184>)
 8009c7c:	623b      	str	r3, [r7, #32]
 8009c7e:	4b41      	ldr	r3, [pc, #260]	; (8009d84 <pvPortMalloc+0x184>)
 8009c80:	681b      	ldr	r3, [r3, #0]
 8009c82:	627b      	str	r3, [r7, #36]	; 0x24
 8009c84:	e004      	b.n	8009c90 <pvPortMalloc+0x90>
 8009c86:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009c88:	623b      	str	r3, [r7, #32]
 8009c8a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009c8c:	681b      	ldr	r3, [r3, #0]
 8009c8e:	627b      	str	r3, [r7, #36]	; 0x24
 8009c90:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009c92:	685b      	ldr	r3, [r3, #4]
 8009c94:	687a      	ldr	r2, [r7, #4]
 8009c96:	429a      	cmp	r2, r3
 8009c98:	d903      	bls.n	8009ca2 <pvPortMalloc+0xa2>
 8009c9a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009c9c:	681b      	ldr	r3, [r3, #0]
 8009c9e:	2b00      	cmp	r3, #0
 8009ca0:	d1f1      	bne.n	8009c86 <pvPortMalloc+0x86>
 8009ca2:	4b35      	ldr	r3, [pc, #212]	; (8009d78 <pvPortMalloc+0x178>)
 8009ca4:	681b      	ldr	r3, [r3, #0]
 8009ca6:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8009ca8:	429a      	cmp	r2, r3
 8009caa:	d04a      	beq.n	8009d42 <pvPortMalloc+0x142>
 8009cac:	6a3b      	ldr	r3, [r7, #32]
 8009cae:	681b      	ldr	r3, [r3, #0]
 8009cb0:	2208      	movs	r2, #8
 8009cb2:	4413      	add	r3, r2
 8009cb4:	61fb      	str	r3, [r7, #28]
 8009cb6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009cb8:	681a      	ldr	r2, [r3, #0]
 8009cba:	6a3b      	ldr	r3, [r7, #32]
 8009cbc:	601a      	str	r2, [r3, #0]
 8009cbe:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009cc0:	685a      	ldr	r2, [r3, #4]
 8009cc2:	687b      	ldr	r3, [r7, #4]
 8009cc4:	1ad2      	subs	r2, r2, r3
 8009cc6:	2308      	movs	r3, #8
 8009cc8:	005b      	lsls	r3, r3, #1
 8009cca:	429a      	cmp	r2, r3
 8009ccc:	d91e      	bls.n	8009d0c <pvPortMalloc+0x10c>
 8009cce:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8009cd0:	687b      	ldr	r3, [r7, #4]
 8009cd2:	4413      	add	r3, r2
 8009cd4:	61bb      	str	r3, [r7, #24]
 8009cd6:	69bb      	ldr	r3, [r7, #24]
 8009cd8:	f003 0307 	and.w	r3, r3, #7
 8009cdc:	2b00      	cmp	r3, #0
 8009cde:	d009      	beq.n	8009cf4 <pvPortMalloc+0xf4>
 8009ce0:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009ce4:	f383 8811 	msr	BASEPRI, r3
 8009ce8:	f3bf 8f6f 	isb	sy
 8009cec:	f3bf 8f4f 	dsb	sy
 8009cf0:	613b      	str	r3, [r7, #16]
 8009cf2:	e7fe      	b.n	8009cf2 <pvPortMalloc+0xf2>
 8009cf4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009cf6:	685a      	ldr	r2, [r3, #4]
 8009cf8:	687b      	ldr	r3, [r7, #4]
 8009cfa:	1ad2      	subs	r2, r2, r3
 8009cfc:	69bb      	ldr	r3, [r7, #24]
 8009cfe:	605a      	str	r2, [r3, #4]
 8009d00:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009d02:	687a      	ldr	r2, [r7, #4]
 8009d04:	605a      	str	r2, [r3, #4]
 8009d06:	69b8      	ldr	r0, [r7, #24]
 8009d08:	f000 f8fa 	bl	8009f00 <prvInsertBlockIntoFreeList>
 8009d0c:	4b1c      	ldr	r3, [pc, #112]	; (8009d80 <pvPortMalloc+0x180>)
 8009d0e:	681a      	ldr	r2, [r3, #0]
 8009d10:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009d12:	685b      	ldr	r3, [r3, #4]
 8009d14:	1ad3      	subs	r3, r2, r3
 8009d16:	4a1a      	ldr	r2, [pc, #104]	; (8009d80 <pvPortMalloc+0x180>)
 8009d18:	6013      	str	r3, [r2, #0]
 8009d1a:	4b19      	ldr	r3, [pc, #100]	; (8009d80 <pvPortMalloc+0x180>)
 8009d1c:	681a      	ldr	r2, [r3, #0]
 8009d1e:	4b1a      	ldr	r3, [pc, #104]	; (8009d88 <pvPortMalloc+0x188>)
 8009d20:	681b      	ldr	r3, [r3, #0]
 8009d22:	429a      	cmp	r2, r3
 8009d24:	d203      	bcs.n	8009d2e <pvPortMalloc+0x12e>
 8009d26:	4b16      	ldr	r3, [pc, #88]	; (8009d80 <pvPortMalloc+0x180>)
 8009d28:	681b      	ldr	r3, [r3, #0]
 8009d2a:	4a17      	ldr	r2, [pc, #92]	; (8009d88 <pvPortMalloc+0x188>)
 8009d2c:	6013      	str	r3, [r2, #0]
 8009d2e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009d30:	685a      	ldr	r2, [r3, #4]
 8009d32:	4b12      	ldr	r3, [pc, #72]	; (8009d7c <pvPortMalloc+0x17c>)
 8009d34:	681b      	ldr	r3, [r3, #0]
 8009d36:	431a      	orrs	r2, r3
 8009d38:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009d3a:	605a      	str	r2, [r3, #4]
 8009d3c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8009d3e:	2200      	movs	r2, #0
 8009d40:	601a      	str	r2, [r3, #0]
 8009d42:	f7fe fa3f 	bl	80081c4 <xTaskResumeAll>
 8009d46:	69fb      	ldr	r3, [r7, #28]
 8009d48:	2b00      	cmp	r3, #0
 8009d4a:	d101      	bne.n	8009d50 <pvPortMalloc+0x150>
 8009d4c:	f7fa fd90 	bl	8004870 <vApplicationMallocFailedHook>
 8009d50:	69fb      	ldr	r3, [r7, #28]
 8009d52:	f003 0307 	and.w	r3, r3, #7
 8009d56:	2b00      	cmp	r3, #0
 8009d58:	d009      	beq.n	8009d6e <pvPortMalloc+0x16e>
 8009d5a:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009d5e:	f383 8811 	msr	BASEPRI, r3
 8009d62:	f3bf 8f6f 	isb	sy
 8009d66:	f3bf 8f4f 	dsb	sy
 8009d6a:	60fb      	str	r3, [r7, #12]
 8009d6c:	e7fe      	b.n	8009d6c <pvPortMalloc+0x16c>
 8009d6e:	69fb      	ldr	r3, [r7, #28]
 8009d70:	4618      	mov	r0, r3
 8009d72:	3728      	adds	r7, #40	; 0x28
 8009d74:	46bd      	mov	sp, r7
 8009d76:	bd80      	pop	{r7, pc}
 8009d78:	20013a10 	.word	0x20013a10
 8009d7c:	20013a1c 	.word	0x20013a1c
 8009d80:	20013a14 	.word	0x20013a14
 8009d84:	20013a08 	.word	0x20013a08
 8009d88:	20013a18 	.word	0x20013a18

08009d8c <vPortFree>:
 8009d8c:	b580      	push	{r7, lr}
 8009d8e:	b086      	sub	sp, #24
 8009d90:	af00      	add	r7, sp, #0
 8009d92:	6078      	str	r0, [r7, #4]
 8009d94:	687b      	ldr	r3, [r7, #4]
 8009d96:	617b      	str	r3, [r7, #20]
 8009d98:	687b      	ldr	r3, [r7, #4]
 8009d9a:	2b00      	cmp	r3, #0
 8009d9c:	d046      	beq.n	8009e2c <vPortFree+0xa0>
 8009d9e:	2308      	movs	r3, #8
 8009da0:	425b      	negs	r3, r3
 8009da2:	697a      	ldr	r2, [r7, #20]
 8009da4:	4413      	add	r3, r2
 8009da6:	617b      	str	r3, [r7, #20]
 8009da8:	697b      	ldr	r3, [r7, #20]
 8009daa:	613b      	str	r3, [r7, #16]
 8009dac:	693b      	ldr	r3, [r7, #16]
 8009dae:	685a      	ldr	r2, [r3, #4]
 8009db0:	4b20      	ldr	r3, [pc, #128]	; (8009e34 <vPortFree+0xa8>)
 8009db2:	681b      	ldr	r3, [r3, #0]
 8009db4:	4013      	ands	r3, r2
 8009db6:	2b00      	cmp	r3, #0
 8009db8:	d109      	bne.n	8009dce <vPortFree+0x42>
 8009dba:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009dbe:	f383 8811 	msr	BASEPRI, r3
 8009dc2:	f3bf 8f6f 	isb	sy
 8009dc6:	f3bf 8f4f 	dsb	sy
 8009dca:	60fb      	str	r3, [r7, #12]
 8009dcc:	e7fe      	b.n	8009dcc <vPortFree+0x40>
 8009dce:	693b      	ldr	r3, [r7, #16]
 8009dd0:	681b      	ldr	r3, [r3, #0]
 8009dd2:	2b00      	cmp	r3, #0
 8009dd4:	d009      	beq.n	8009dea <vPortFree+0x5e>
 8009dd6:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009dda:	f383 8811 	msr	BASEPRI, r3
 8009dde:	f3bf 8f6f 	isb	sy
 8009de2:	f3bf 8f4f 	dsb	sy
 8009de6:	60bb      	str	r3, [r7, #8]
 8009de8:	e7fe      	b.n	8009de8 <vPortFree+0x5c>
 8009dea:	693b      	ldr	r3, [r7, #16]
 8009dec:	685a      	ldr	r2, [r3, #4]
 8009dee:	4b11      	ldr	r3, [pc, #68]	; (8009e34 <vPortFree+0xa8>)
 8009df0:	681b      	ldr	r3, [r3, #0]
 8009df2:	4013      	ands	r3, r2
 8009df4:	2b00      	cmp	r3, #0
 8009df6:	d019      	beq.n	8009e2c <vPortFree+0xa0>
 8009df8:	693b      	ldr	r3, [r7, #16]
 8009dfa:	681b      	ldr	r3, [r3, #0]
 8009dfc:	2b00      	cmp	r3, #0
 8009dfe:	d115      	bne.n	8009e2c <vPortFree+0xa0>
 8009e00:	693b      	ldr	r3, [r7, #16]
 8009e02:	685a      	ldr	r2, [r3, #4]
 8009e04:	4b0b      	ldr	r3, [pc, #44]	; (8009e34 <vPortFree+0xa8>)
 8009e06:	681b      	ldr	r3, [r3, #0]
 8009e08:	43db      	mvns	r3, r3
 8009e0a:	401a      	ands	r2, r3
 8009e0c:	693b      	ldr	r3, [r7, #16]
 8009e0e:	605a      	str	r2, [r3, #4]
 8009e10:	f7fe f9ca 	bl	80081a8 <vTaskSuspendAll>
 8009e14:	693b      	ldr	r3, [r7, #16]
 8009e16:	685a      	ldr	r2, [r3, #4]
 8009e18:	4b07      	ldr	r3, [pc, #28]	; (8009e38 <vPortFree+0xac>)
 8009e1a:	681b      	ldr	r3, [r3, #0]
 8009e1c:	4413      	add	r3, r2
 8009e1e:	4a06      	ldr	r2, [pc, #24]	; (8009e38 <vPortFree+0xac>)
 8009e20:	6013      	str	r3, [r2, #0]
 8009e22:	6938      	ldr	r0, [r7, #16]
 8009e24:	f000 f86c 	bl	8009f00 <prvInsertBlockIntoFreeList>
 8009e28:	f7fe f9cc 	bl	80081c4 <xTaskResumeAll>
 8009e2c:	bf00      	nop
 8009e2e:	3718      	adds	r7, #24
 8009e30:	46bd      	mov	sp, r7
 8009e32:	bd80      	pop	{r7, pc}
 8009e34:	20013a1c 	.word	0x20013a1c
 8009e38:	20013a14 	.word	0x20013a14

08009e3c <prvHeapInit>:
 8009e3c:	b480      	push	{r7}
 8009e3e:	b085      	sub	sp, #20
 8009e40:	af00      	add	r7, sp, #0
 8009e42:	f44f 3396 	mov.w	r3, #76800	; 0x12c00
 8009e46:	60bb      	str	r3, [r7, #8]
 8009e48:	4b27      	ldr	r3, [pc, #156]	; (8009ee8 <prvHeapInit+0xac>)
 8009e4a:	60fb      	str	r3, [r7, #12]
 8009e4c:	68fb      	ldr	r3, [r7, #12]
 8009e4e:	f003 0307 	and.w	r3, r3, #7
 8009e52:	2b00      	cmp	r3, #0
 8009e54:	d00c      	beq.n	8009e70 <prvHeapInit+0x34>
 8009e56:	68fb      	ldr	r3, [r7, #12]
 8009e58:	3307      	adds	r3, #7
 8009e5a:	60fb      	str	r3, [r7, #12]
 8009e5c:	68fb      	ldr	r3, [r7, #12]
 8009e5e:	f023 0307 	bic.w	r3, r3, #7
 8009e62:	60fb      	str	r3, [r7, #12]
 8009e64:	68ba      	ldr	r2, [r7, #8]
 8009e66:	68fb      	ldr	r3, [r7, #12]
 8009e68:	1ad3      	subs	r3, r2, r3
 8009e6a:	4a1f      	ldr	r2, [pc, #124]	; (8009ee8 <prvHeapInit+0xac>)
 8009e6c:	4413      	add	r3, r2
 8009e6e:	60bb      	str	r3, [r7, #8]
 8009e70:	68fb      	ldr	r3, [r7, #12]
 8009e72:	607b      	str	r3, [r7, #4]
 8009e74:	4a1d      	ldr	r2, [pc, #116]	; (8009eec <prvHeapInit+0xb0>)
 8009e76:	687b      	ldr	r3, [r7, #4]
 8009e78:	6013      	str	r3, [r2, #0]
 8009e7a:	4b1c      	ldr	r3, [pc, #112]	; (8009eec <prvHeapInit+0xb0>)
 8009e7c:	2200      	movs	r2, #0
 8009e7e:	605a      	str	r2, [r3, #4]
 8009e80:	687b      	ldr	r3, [r7, #4]
 8009e82:	68ba      	ldr	r2, [r7, #8]
 8009e84:	4413      	add	r3, r2
 8009e86:	60fb      	str	r3, [r7, #12]
 8009e88:	2208      	movs	r2, #8
 8009e8a:	68fb      	ldr	r3, [r7, #12]
 8009e8c:	1a9b      	subs	r3, r3, r2
 8009e8e:	60fb      	str	r3, [r7, #12]
 8009e90:	68fb      	ldr	r3, [r7, #12]
 8009e92:	f023 0307 	bic.w	r3, r3, #7
 8009e96:	60fb      	str	r3, [r7, #12]
 8009e98:	68fb      	ldr	r3, [r7, #12]
 8009e9a:	4a15      	ldr	r2, [pc, #84]	; (8009ef0 <prvHeapInit+0xb4>)
 8009e9c:	6013      	str	r3, [r2, #0]
 8009e9e:	4b14      	ldr	r3, [pc, #80]	; (8009ef0 <prvHeapInit+0xb4>)
 8009ea0:	681b      	ldr	r3, [r3, #0]
 8009ea2:	2200      	movs	r2, #0
 8009ea4:	605a      	str	r2, [r3, #4]
 8009ea6:	4b12      	ldr	r3, [pc, #72]	; (8009ef0 <prvHeapInit+0xb4>)
 8009ea8:	681b      	ldr	r3, [r3, #0]
 8009eaa:	2200      	movs	r2, #0
 8009eac:	601a      	str	r2, [r3, #0]
 8009eae:	687b      	ldr	r3, [r7, #4]
 8009eb0:	603b      	str	r3, [r7, #0]
 8009eb2:	683b      	ldr	r3, [r7, #0]
 8009eb4:	68fa      	ldr	r2, [r7, #12]
 8009eb6:	1ad2      	subs	r2, r2, r3
 8009eb8:	683b      	ldr	r3, [r7, #0]
 8009eba:	605a      	str	r2, [r3, #4]
 8009ebc:	4b0c      	ldr	r3, [pc, #48]	; (8009ef0 <prvHeapInit+0xb4>)
 8009ebe:	681a      	ldr	r2, [r3, #0]
 8009ec0:	683b      	ldr	r3, [r7, #0]
 8009ec2:	601a      	str	r2, [r3, #0]
 8009ec4:	683b      	ldr	r3, [r7, #0]
 8009ec6:	685b      	ldr	r3, [r3, #4]
 8009ec8:	4a0a      	ldr	r2, [pc, #40]	; (8009ef4 <prvHeapInit+0xb8>)
 8009eca:	6013      	str	r3, [r2, #0]
 8009ecc:	683b      	ldr	r3, [r7, #0]
 8009ece:	685b      	ldr	r3, [r3, #4]
 8009ed0:	4a09      	ldr	r2, [pc, #36]	; (8009ef8 <prvHeapInit+0xbc>)
 8009ed2:	6013      	str	r3, [r2, #0]
 8009ed4:	4b09      	ldr	r3, [pc, #36]	; (8009efc <prvHeapInit+0xc0>)
 8009ed6:	f04f 4200 	mov.w	r2, #2147483648	; 0x80000000
 8009eda:	601a      	str	r2, [r3, #0]
 8009edc:	bf00      	nop
 8009ede:	3714      	adds	r7, #20
 8009ee0:	46bd      	mov	sp, r7
 8009ee2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009ee6:	4770      	bx	lr
 8009ee8:	20000e08 	.word	0x20000e08
 8009eec:	20013a08 	.word	0x20013a08
 8009ef0:	20013a10 	.word	0x20013a10
 8009ef4:	20013a18 	.word	0x20013a18
 8009ef8:	20013a14 	.word	0x20013a14
 8009efc:	20013a1c 	.word	0x20013a1c

08009f00 <prvInsertBlockIntoFreeList>:
 8009f00:	b480      	push	{r7}
 8009f02:	b085      	sub	sp, #20
 8009f04:	af00      	add	r7, sp, #0
 8009f06:	6078      	str	r0, [r7, #4]
 8009f08:	4b28      	ldr	r3, [pc, #160]	; (8009fac <prvInsertBlockIntoFreeList+0xac>)
 8009f0a:	60fb      	str	r3, [r7, #12]
 8009f0c:	e002      	b.n	8009f14 <prvInsertBlockIntoFreeList+0x14>
 8009f0e:	68fb      	ldr	r3, [r7, #12]
 8009f10:	681b      	ldr	r3, [r3, #0]
 8009f12:	60fb      	str	r3, [r7, #12]
 8009f14:	68fb      	ldr	r3, [r7, #12]
 8009f16:	681b      	ldr	r3, [r3, #0]
 8009f18:	687a      	ldr	r2, [r7, #4]
 8009f1a:	429a      	cmp	r2, r3
 8009f1c:	d8f7      	bhi.n	8009f0e <prvInsertBlockIntoFreeList+0xe>
 8009f1e:	68fb      	ldr	r3, [r7, #12]
 8009f20:	60bb      	str	r3, [r7, #8]
 8009f22:	68fb      	ldr	r3, [r7, #12]
 8009f24:	685b      	ldr	r3, [r3, #4]
 8009f26:	68ba      	ldr	r2, [r7, #8]
 8009f28:	4413      	add	r3, r2
 8009f2a:	687a      	ldr	r2, [r7, #4]
 8009f2c:	429a      	cmp	r2, r3
 8009f2e:	d108      	bne.n	8009f42 <prvInsertBlockIntoFreeList+0x42>
 8009f30:	68fb      	ldr	r3, [r7, #12]
 8009f32:	685a      	ldr	r2, [r3, #4]
 8009f34:	687b      	ldr	r3, [r7, #4]
 8009f36:	685b      	ldr	r3, [r3, #4]
 8009f38:	441a      	add	r2, r3
 8009f3a:	68fb      	ldr	r3, [r7, #12]
 8009f3c:	605a      	str	r2, [r3, #4]
 8009f3e:	68fb      	ldr	r3, [r7, #12]
 8009f40:	607b      	str	r3, [r7, #4]
 8009f42:	687b      	ldr	r3, [r7, #4]
 8009f44:	60bb      	str	r3, [r7, #8]
 8009f46:	687b      	ldr	r3, [r7, #4]
 8009f48:	685b      	ldr	r3, [r3, #4]
 8009f4a:	68ba      	ldr	r2, [r7, #8]
 8009f4c:	441a      	add	r2, r3
 8009f4e:	68fb      	ldr	r3, [r7, #12]
 8009f50:	681b      	ldr	r3, [r3, #0]
 8009f52:	429a      	cmp	r2, r3
 8009f54:	d118      	bne.n	8009f88 <prvInsertBlockIntoFreeList+0x88>
 8009f56:	68fb      	ldr	r3, [r7, #12]
 8009f58:	681a      	ldr	r2, [r3, #0]
 8009f5a:	4b15      	ldr	r3, [pc, #84]	; (8009fb0 <prvInsertBlockIntoFreeList+0xb0>)
 8009f5c:	681b      	ldr	r3, [r3, #0]
 8009f5e:	429a      	cmp	r2, r3
 8009f60:	d00d      	beq.n	8009f7e <prvInsertBlockIntoFreeList+0x7e>
 8009f62:	687b      	ldr	r3, [r7, #4]
 8009f64:	685a      	ldr	r2, [r3, #4]
 8009f66:	68fb      	ldr	r3, [r7, #12]
 8009f68:	681b      	ldr	r3, [r3, #0]
 8009f6a:	685b      	ldr	r3, [r3, #4]
 8009f6c:	441a      	add	r2, r3
 8009f6e:	687b      	ldr	r3, [r7, #4]
 8009f70:	605a      	str	r2, [r3, #4]
 8009f72:	68fb      	ldr	r3, [r7, #12]
 8009f74:	681b      	ldr	r3, [r3, #0]
 8009f76:	681a      	ldr	r2, [r3, #0]
 8009f78:	687b      	ldr	r3, [r7, #4]
 8009f7a:	601a      	str	r2, [r3, #0]
 8009f7c:	e008      	b.n	8009f90 <prvInsertBlockIntoFreeList+0x90>
 8009f7e:	4b0c      	ldr	r3, [pc, #48]	; (8009fb0 <prvInsertBlockIntoFreeList+0xb0>)
 8009f80:	681a      	ldr	r2, [r3, #0]
 8009f82:	687b      	ldr	r3, [r7, #4]
 8009f84:	601a      	str	r2, [r3, #0]
 8009f86:	e003      	b.n	8009f90 <prvInsertBlockIntoFreeList+0x90>
 8009f88:	68fb      	ldr	r3, [r7, #12]
 8009f8a:	681a      	ldr	r2, [r3, #0]
 8009f8c:	687b      	ldr	r3, [r7, #4]
 8009f8e:	601a      	str	r2, [r3, #0]
 8009f90:	68fa      	ldr	r2, [r7, #12]
 8009f92:	687b      	ldr	r3, [r7, #4]
 8009f94:	429a      	cmp	r2, r3
 8009f96:	d002      	beq.n	8009f9e <prvInsertBlockIntoFreeList+0x9e>
 8009f98:	68fb      	ldr	r3, [r7, #12]
 8009f9a:	687a      	ldr	r2, [r7, #4]
 8009f9c:	601a      	str	r2, [r3, #0]
 8009f9e:	bf00      	nop
 8009fa0:	3714      	adds	r7, #20
 8009fa2:	46bd      	mov	sp, r7
 8009fa4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009fa8:	4770      	bx	lr
 8009faa:	bf00      	nop
 8009fac:	20013a08 	.word	0x20013a08
 8009fb0:	20013a10 	.word	0x20013a10

08009fb4 <SystemInit>:
 8009fb4:	b480      	push	{r7}
 8009fb6:	af00      	add	r7, sp, #0
 8009fb8:	4b16      	ldr	r3, [pc, #88]	; (800a014 <SystemInit+0x60>)
 8009fba:	f8d3 3088 	ldr.w	r3, [r3, #136]	; 0x88
 8009fbe:	4a15      	ldr	r2, [pc, #84]	; (800a014 <SystemInit+0x60>)
 8009fc0:	f443 0370 	orr.w	r3, r3, #15728640	; 0xf00000
 8009fc4:	f8c2 3088 	str.w	r3, [r2, #136]	; 0x88
 8009fc8:	4b13      	ldr	r3, [pc, #76]	; (800a018 <SystemInit+0x64>)
 8009fca:	681b      	ldr	r3, [r3, #0]
 8009fcc:	4a12      	ldr	r2, [pc, #72]	; (800a018 <SystemInit+0x64>)
 8009fce:	f043 0301 	orr.w	r3, r3, #1
 8009fd2:	6013      	str	r3, [r2, #0]
 8009fd4:	4b10      	ldr	r3, [pc, #64]	; (800a018 <SystemInit+0x64>)
 8009fd6:	2200      	movs	r2, #0
 8009fd8:	609a      	str	r2, [r3, #8]
 8009fda:	4b0f      	ldr	r3, [pc, #60]	; (800a018 <SystemInit+0x64>)
 8009fdc:	681b      	ldr	r3, [r3, #0]
 8009fde:	4a0e      	ldr	r2, [pc, #56]	; (800a018 <SystemInit+0x64>)
 8009fe0:	f023 7384 	bic.w	r3, r3, #17301504	; 0x1080000
 8009fe4:	f423 3380 	bic.w	r3, r3, #65536	; 0x10000
 8009fe8:	6013      	str	r3, [r2, #0]
 8009fea:	4b0b      	ldr	r3, [pc, #44]	; (800a018 <SystemInit+0x64>)
 8009fec:	4a0b      	ldr	r2, [pc, #44]	; (800a01c <SystemInit+0x68>)
 8009fee:	605a      	str	r2, [r3, #4]
 8009ff0:	4b09      	ldr	r3, [pc, #36]	; (800a018 <SystemInit+0x64>)
 8009ff2:	681b      	ldr	r3, [r3, #0]
 8009ff4:	4a08      	ldr	r2, [pc, #32]	; (800a018 <SystemInit+0x64>)
 8009ff6:	f423 2380 	bic.w	r3, r3, #262144	; 0x40000
 8009ffa:	6013      	str	r3, [r2, #0]
 8009ffc:	4b06      	ldr	r3, [pc, #24]	; (800a018 <SystemInit+0x64>)
 8009ffe:	2200      	movs	r2, #0
 800a000:	60da      	str	r2, [r3, #12]
 800a002:	4b04      	ldr	r3, [pc, #16]	; (800a014 <SystemInit+0x60>)
 800a004:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 800a008:	609a      	str	r2, [r3, #8]
 800a00a:	bf00      	nop
 800a00c:	46bd      	mov	sp, r7
 800a00e:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a012:	4770      	bx	lr
 800a014:	e000ed00 	.word	0xe000ed00
 800a018:	40023800 	.word	0x40023800
 800a01c:	24003010 	.word	0x24003010

0800a020 <arm_add_f32>:
 800a020:	b480      	push	{r7}
 800a022:	b08f      	sub	sp, #60	; 0x3c
 800a024:	af00      	add	r7, sp, #0
 800a026:	60f8      	str	r0, [r7, #12]
 800a028:	60b9      	str	r1, [r7, #8]
 800a02a:	607a      	str	r2, [r7, #4]
 800a02c:	603b      	str	r3, [r7, #0]
 800a02e:	683b      	ldr	r3, [r7, #0]
 800a030:	089b      	lsrs	r3, r3, #2
 800a032:	637b      	str	r3, [r7, #52]	; 0x34
 800a034:	e04a      	b.n	800a0cc <arm_add_f32+0xac>
 800a036:	68fb      	ldr	r3, [r7, #12]
 800a038:	681b      	ldr	r3, [r3, #0]
 800a03a:	633b      	str	r3, [r7, #48]	; 0x30
 800a03c:	68bb      	ldr	r3, [r7, #8]
 800a03e:	681b      	ldr	r3, [r3, #0]
 800a040:	62fb      	str	r3, [r7, #44]	; 0x2c
 800a042:	68fb      	ldr	r3, [r7, #12]
 800a044:	685b      	ldr	r3, [r3, #4]
 800a046:	62bb      	str	r3, [r7, #40]	; 0x28
 800a048:	68bb      	ldr	r3, [r7, #8]
 800a04a:	685b      	ldr	r3, [r3, #4]
 800a04c:	627b      	str	r3, [r7, #36]	; 0x24
 800a04e:	68fb      	ldr	r3, [r7, #12]
 800a050:	689b      	ldr	r3, [r3, #8]
 800a052:	623b      	str	r3, [r7, #32]
 800a054:	68bb      	ldr	r3, [r7, #8]
 800a056:	689b      	ldr	r3, [r3, #8]
 800a058:	61fb      	str	r3, [r7, #28]
 800a05a:	68fb      	ldr	r3, [r7, #12]
 800a05c:	68db      	ldr	r3, [r3, #12]
 800a05e:	61bb      	str	r3, [r7, #24]
 800a060:	68bb      	ldr	r3, [r7, #8]
 800a062:	68db      	ldr	r3, [r3, #12]
 800a064:	617b      	str	r3, [r7, #20]
 800a066:	ed97 7a0c 	vldr	s14, [r7, #48]	; 0x30
 800a06a:	edd7 7a0b 	vldr	s15, [r7, #44]	; 0x2c
 800a06e:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a072:	687b      	ldr	r3, [r7, #4]
 800a074:	edc3 7a00 	vstr	s15, [r3]
 800a078:	687b      	ldr	r3, [r7, #4]
 800a07a:	3304      	adds	r3, #4
 800a07c:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 800a080:	edd7 7a09 	vldr	s15, [r7, #36]	; 0x24
 800a084:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a088:	edc3 7a00 	vstr	s15, [r3]
 800a08c:	687b      	ldr	r3, [r7, #4]
 800a08e:	3308      	adds	r3, #8
 800a090:	ed97 7a08 	vldr	s14, [r7, #32]
 800a094:	edd7 7a07 	vldr	s15, [r7, #28]
 800a098:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a09c:	edc3 7a00 	vstr	s15, [r3]
 800a0a0:	687b      	ldr	r3, [r7, #4]
 800a0a2:	330c      	adds	r3, #12
 800a0a4:	ed97 7a06 	vldr	s14, [r7, #24]
 800a0a8:	edd7 7a05 	vldr	s15, [r7, #20]
 800a0ac:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a0b0:	edc3 7a00 	vstr	s15, [r3]
 800a0b4:	68fb      	ldr	r3, [r7, #12]
 800a0b6:	3310      	adds	r3, #16
 800a0b8:	60fb      	str	r3, [r7, #12]
 800a0ba:	68bb      	ldr	r3, [r7, #8]
 800a0bc:	3310      	adds	r3, #16
 800a0be:	60bb      	str	r3, [r7, #8]
 800a0c0:	687b      	ldr	r3, [r7, #4]
 800a0c2:	3310      	adds	r3, #16
 800a0c4:	607b      	str	r3, [r7, #4]
 800a0c6:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a0c8:	3b01      	subs	r3, #1
 800a0ca:	637b      	str	r3, [r7, #52]	; 0x34
 800a0cc:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a0ce:	2b00      	cmp	r3, #0
 800a0d0:	d1b1      	bne.n	800a036 <arm_add_f32+0x16>
 800a0d2:	683b      	ldr	r3, [r7, #0]
 800a0d4:	f003 0303 	and.w	r3, r3, #3
 800a0d8:	637b      	str	r3, [r7, #52]	; 0x34
 800a0da:	e013      	b.n	800a104 <arm_add_f32+0xe4>
 800a0dc:	68fb      	ldr	r3, [r7, #12]
 800a0de:	1d1a      	adds	r2, r3, #4
 800a0e0:	60fa      	str	r2, [r7, #12]
 800a0e2:	ed93 7a00 	vldr	s14, [r3]
 800a0e6:	68bb      	ldr	r3, [r7, #8]
 800a0e8:	1d1a      	adds	r2, r3, #4
 800a0ea:	60ba      	str	r2, [r7, #8]
 800a0ec:	edd3 7a00 	vldr	s15, [r3]
 800a0f0:	687b      	ldr	r3, [r7, #4]
 800a0f2:	1d1a      	adds	r2, r3, #4
 800a0f4:	607a      	str	r2, [r7, #4]
 800a0f6:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a0fa:	edc3 7a00 	vstr	s15, [r3]
 800a0fe:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a100:	3b01      	subs	r3, #1
 800a102:	637b      	str	r3, [r7, #52]	; 0x34
 800a104:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a106:	2b00      	cmp	r3, #0
 800a108:	d1e8      	bne.n	800a0dc <arm_add_f32+0xbc>
 800a10a:	bf00      	nop
 800a10c:	373c      	adds	r7, #60	; 0x3c
 800a10e:	46bd      	mov	sp, r7
 800a110:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a114:	4770      	bx	lr

0800a116 <arm_mat_add_f32>:
 800a116:	b480      	push	{r7}
 800a118:	b091      	sub	sp, #68	; 0x44
 800a11a:	af00      	add	r7, sp, #0
 800a11c:	60f8      	str	r0, [r7, #12]
 800a11e:	60b9      	str	r1, [r7, #8]
 800a120:	607a      	str	r2, [r7, #4]
 800a122:	68fb      	ldr	r3, [r7, #12]
 800a124:	685b      	ldr	r3, [r3, #4]
 800a126:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a128:	68bb      	ldr	r3, [r7, #8]
 800a12a:	685b      	ldr	r3, [r3, #4]
 800a12c:	63bb      	str	r3, [r7, #56]	; 0x38
 800a12e:	687b      	ldr	r3, [r7, #4]
 800a130:	685b      	ldr	r3, [r3, #4]
 800a132:	637b      	str	r3, [r7, #52]	; 0x34
 800a134:	68fb      	ldr	r3, [r7, #12]
 800a136:	881b      	ldrh	r3, [r3, #0]
 800a138:	461a      	mov	r2, r3
 800a13a:	68fb      	ldr	r3, [r7, #12]
 800a13c:	885b      	ldrh	r3, [r3, #2]
 800a13e:	fb03 f302 	mul.w	r3, r3, r2
 800a142:	62fb      	str	r3, [r7, #44]	; 0x2c
 800a144:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800a146:	089b      	lsrs	r3, r3, #2
 800a148:	633b      	str	r3, [r7, #48]	; 0x30
 800a14a:	e052      	b.n	800a1f2 <arm_mat_add_f32+0xdc>
 800a14c:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a14e:	681b      	ldr	r3, [r3, #0]
 800a150:	62bb      	str	r3, [r7, #40]	; 0x28
 800a152:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a154:	681b      	ldr	r3, [r3, #0]
 800a156:	627b      	str	r3, [r7, #36]	; 0x24
 800a158:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a15a:	685b      	ldr	r3, [r3, #4]
 800a15c:	623b      	str	r3, [r7, #32]
 800a15e:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 800a162:	edd7 7a09 	vldr	s15, [r7, #36]	; 0x24
 800a166:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a16a:	edc7 7a07 	vstr	s15, [r7, #28]
 800a16e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a170:	685b      	ldr	r3, [r3, #4]
 800a172:	61bb      	str	r3, [r7, #24]
 800a174:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a176:	689b      	ldr	r3, [r3, #8]
 800a178:	62bb      	str	r3, [r7, #40]	; 0x28
 800a17a:	ed97 7a08 	vldr	s14, [r7, #32]
 800a17e:	edd7 7a06 	vldr	s15, [r7, #24]
 800a182:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a186:	edc7 7a05 	vstr	s15, [r7, #20]
 800a18a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a18c:	689b      	ldr	r3, [r3, #8]
 800a18e:	627b      	str	r3, [r7, #36]	; 0x24
 800a190:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a192:	69fa      	ldr	r2, [r7, #28]
 800a194:	601a      	str	r2, [r3, #0]
 800a196:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a198:	3304      	adds	r3, #4
 800a19a:	697a      	ldr	r2, [r7, #20]
 800a19c:	601a      	str	r2, [r3, #0]
 800a19e:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a1a0:	68db      	ldr	r3, [r3, #12]
 800a1a2:	623b      	str	r3, [r7, #32]
 800a1a4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a1a6:	68db      	ldr	r3, [r3, #12]
 800a1a8:	61bb      	str	r3, [r7, #24]
 800a1aa:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 800a1ae:	edd7 7a09 	vldr	s15, [r7, #36]	; 0x24
 800a1b2:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a1b6:	edc7 7a07 	vstr	s15, [r7, #28]
 800a1ba:	ed97 7a08 	vldr	s14, [r7, #32]
 800a1be:	edd7 7a06 	vldr	s15, [r7, #24]
 800a1c2:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a1c6:	edc7 7a05 	vstr	s15, [r7, #20]
 800a1ca:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a1cc:	3308      	adds	r3, #8
 800a1ce:	69fa      	ldr	r2, [r7, #28]
 800a1d0:	601a      	str	r2, [r3, #0]
 800a1d2:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a1d4:	330c      	adds	r3, #12
 800a1d6:	697a      	ldr	r2, [r7, #20]
 800a1d8:	601a      	str	r2, [r3, #0]
 800a1da:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a1dc:	3310      	adds	r3, #16
 800a1de:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a1e0:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a1e2:	3310      	adds	r3, #16
 800a1e4:	63bb      	str	r3, [r7, #56]	; 0x38
 800a1e6:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a1e8:	3310      	adds	r3, #16
 800a1ea:	637b      	str	r3, [r7, #52]	; 0x34
 800a1ec:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800a1ee:	3b01      	subs	r3, #1
 800a1f0:	633b      	str	r3, [r7, #48]	; 0x30
 800a1f2:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800a1f4:	2b00      	cmp	r3, #0
 800a1f6:	d1a9      	bne.n	800a14c <arm_mat_add_f32+0x36>
 800a1f8:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800a1fa:	f003 0303 	and.w	r3, r3, #3
 800a1fe:	633b      	str	r3, [r7, #48]	; 0x30
 800a200:	e013      	b.n	800a22a <arm_mat_add_f32+0x114>
 800a202:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a204:	1d1a      	adds	r2, r3, #4
 800a206:	63fa      	str	r2, [r7, #60]	; 0x3c
 800a208:	ed93 7a00 	vldr	s14, [r3]
 800a20c:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a20e:	1d1a      	adds	r2, r3, #4
 800a210:	63ba      	str	r2, [r7, #56]	; 0x38
 800a212:	edd3 7a00 	vldr	s15, [r3]
 800a216:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800a218:	1d1a      	adds	r2, r3, #4
 800a21a:	637a      	str	r2, [r7, #52]	; 0x34
 800a21c:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a220:	edc3 7a00 	vstr	s15, [r3]
 800a224:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800a226:	3b01      	subs	r3, #1
 800a228:	633b      	str	r3, [r7, #48]	; 0x30
 800a22a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800a22c:	2b00      	cmp	r3, #0
 800a22e:	d1e8      	bne.n	800a202 <arm_mat_add_f32+0xec>
 800a230:	2300      	movs	r3, #0
 800a232:	74fb      	strb	r3, [r7, #19]
 800a234:	f997 3013 	ldrsb.w	r3, [r7, #19]
 800a238:	4618      	mov	r0, r3
 800a23a:	3744      	adds	r7, #68	; 0x44
 800a23c:	46bd      	mov	sp, r7
 800a23e:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a242:	4770      	bx	lr

0800a244 <arm_mat_init_f32>:
 800a244:	b480      	push	{r7}
 800a246:	b085      	sub	sp, #20
 800a248:	af00      	add	r7, sp, #0
 800a24a:	60f8      	str	r0, [r7, #12]
 800a24c:	607b      	str	r3, [r7, #4]
 800a24e:	460b      	mov	r3, r1
 800a250:	817b      	strh	r3, [r7, #10]
 800a252:	4613      	mov	r3, r2
 800a254:	813b      	strh	r3, [r7, #8]
 800a256:	68fb      	ldr	r3, [r7, #12]
 800a258:	897a      	ldrh	r2, [r7, #10]
 800a25a:	801a      	strh	r2, [r3, #0]
 800a25c:	68fb      	ldr	r3, [r7, #12]
 800a25e:	893a      	ldrh	r2, [r7, #8]
 800a260:	805a      	strh	r2, [r3, #2]
 800a262:	68fb      	ldr	r3, [r7, #12]
 800a264:	687a      	ldr	r2, [r7, #4]
 800a266:	605a      	str	r2, [r3, #4]
 800a268:	bf00      	nop
 800a26a:	3714      	adds	r7, #20
 800a26c:	46bd      	mov	sp, r7
 800a26e:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a272:	4770      	bx	lr

0800a274 <arm_mat_mult_f32>:
 800a274:	b480      	push	{r7}
 800a276:	b095      	sub	sp, #84	; 0x54
 800a278:	af00      	add	r7, sp, #0
 800a27a:	60f8      	str	r0, [r7, #12]
 800a27c:	60b9      	str	r1, [r7, #8]
 800a27e:	607a      	str	r2, [r7, #4]
 800a280:	68fb      	ldr	r3, [r7, #12]
 800a282:	685b      	ldr	r3, [r3, #4]
 800a284:	64fb      	str	r3, [r7, #76]	; 0x4c
 800a286:	68bb      	ldr	r3, [r7, #8]
 800a288:	685b      	ldr	r3, [r3, #4]
 800a28a:	64bb      	str	r3, [r7, #72]	; 0x48
 800a28c:	68fb      	ldr	r3, [r7, #12]
 800a28e:	685b      	ldr	r3, [r3, #4]
 800a290:	647b      	str	r3, [r7, #68]	; 0x44
 800a292:	687b      	ldr	r3, [r7, #4]
 800a294:	685b      	ldr	r3, [r3, #4]
 800a296:	62fb      	str	r3, [r7, #44]	; 0x2c
 800a298:	68fb      	ldr	r3, [r7, #12]
 800a29a:	881b      	ldrh	r3, [r3, #0]
 800a29c:	857b      	strh	r3, [r7, #42]	; 0x2a
 800a29e:	68bb      	ldr	r3, [r7, #8]
 800a2a0:	885b      	ldrh	r3, [r3, #2]
 800a2a2:	853b      	strh	r3, [r7, #40]	; 0x28
 800a2a4:	68fb      	ldr	r3, [r7, #12]
 800a2a6:	885b      	ldrh	r3, [r3, #2]
 800a2a8:	84fb      	strh	r3, [r7, #38]	; 0x26
 800a2aa:	2300      	movs	r3, #0
 800a2ac:	873b      	strh	r3, [r7, #56]	; 0x38
 800a2ae:	8d7b      	ldrh	r3, [r7, #42]	; 0x2a
 800a2b0:	86bb      	strh	r3, [r7, #52]	; 0x34
 800a2b2:	8f3b      	ldrh	r3, [r7, #56]	; 0x38
 800a2b4:	009b      	lsls	r3, r3, #2
 800a2b6:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 800a2b8:	4413      	add	r3, r2
 800a2ba:	643b      	str	r3, [r7, #64]	; 0x40
 800a2bc:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a2be:	877b      	strh	r3, [r7, #58]	; 0x3a
 800a2c0:	68bb      	ldr	r3, [r7, #8]
 800a2c2:	685b      	ldr	r3, [r3, #4]
 800a2c4:	64bb      	str	r3, [r7, #72]	; 0x48
 800a2c6:	2300      	movs	r3, #0
 800a2c8:	86fb      	strh	r3, [r7, #54]	; 0x36
 800a2ca:	f04f 0300 	mov.w	r3, #0
 800a2ce:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a2d0:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a2d2:	64fb      	str	r3, [r7, #76]	; 0x4c
 800a2d4:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 800a2d6:	089b      	lsrs	r3, r3, #2
 800a2d8:	867b      	strh	r3, [r7, #50]	; 0x32
 800a2da:	e061      	b.n	800a3a0 <arm_mat_mult_f32+0x12c>
 800a2dc:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800a2de:	681b      	ldr	r3, [r3, #0]
 800a2e0:	623b      	str	r3, [r7, #32]
 800a2e2:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a2e4:	009b      	lsls	r3, r3, #2
 800a2e6:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800a2e8:	4413      	add	r3, r2
 800a2ea:	64bb      	str	r3, [r7, #72]	; 0x48
 800a2ec:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a2ee:	681b      	ldr	r3, [r3, #0]
 800a2f0:	61fb      	str	r3, [r7, #28]
 800a2f2:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a2f4:	685b      	ldr	r3, [r3, #4]
 800a2f6:	61bb      	str	r3, [r7, #24]
 800a2f8:	ed97 7a07 	vldr	s14, [r7, #28]
 800a2fc:	edd7 7a08 	vldr	s15, [r7, #32]
 800a300:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a304:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800a308:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a30c:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800a310:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800a312:	681b      	ldr	r3, [r3, #0]
 800a314:	617b      	str	r3, [r7, #20]
 800a316:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a318:	009b      	lsls	r3, r3, #2
 800a31a:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800a31c:	4413      	add	r3, r2
 800a31e:	64bb      	str	r3, [r7, #72]	; 0x48
 800a320:	ed97 7a06 	vldr	s14, [r7, #24]
 800a324:	edd7 7a05 	vldr	s15, [r7, #20]
 800a328:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a32c:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800a330:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a334:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800a338:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800a33a:	681b      	ldr	r3, [r3, #0]
 800a33c:	623b      	str	r3, [r7, #32]
 800a33e:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a340:	009b      	lsls	r3, r3, #2
 800a342:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800a344:	4413      	add	r3, r2
 800a346:	64bb      	str	r3, [r7, #72]	; 0x48
 800a348:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a34a:	689b      	ldr	r3, [r3, #8]
 800a34c:	61fb      	str	r3, [r7, #28]
 800a34e:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a350:	68db      	ldr	r3, [r3, #12]
 800a352:	61bb      	str	r3, [r7, #24]
 800a354:	ed97 7a07 	vldr	s14, [r7, #28]
 800a358:	edd7 7a08 	vldr	s15, [r7, #32]
 800a35c:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a360:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800a364:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a368:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800a36c:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800a36e:	681b      	ldr	r3, [r3, #0]
 800a370:	617b      	str	r3, [r7, #20]
 800a372:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a374:	009b      	lsls	r3, r3, #2
 800a376:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800a378:	4413      	add	r3, r2
 800a37a:	64bb      	str	r3, [r7, #72]	; 0x48
 800a37c:	ed97 7a06 	vldr	s14, [r7, #24]
 800a380:	edd7 7a05 	vldr	s15, [r7, #20]
 800a384:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a388:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800a38c:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a390:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800a394:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a396:	3310      	adds	r3, #16
 800a398:	64fb      	str	r3, [r7, #76]	; 0x4c
 800a39a:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 800a39c:	3b01      	subs	r3, #1
 800a39e:	867b      	strh	r3, [r7, #50]	; 0x32
 800a3a0:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 800a3a2:	2b00      	cmp	r3, #0
 800a3a4:	d19a      	bne.n	800a2dc <arm_mat_mult_f32+0x68>
 800a3a6:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 800a3a8:	f003 0303 	and.w	r3, r3, #3
 800a3ac:	867b      	strh	r3, [r7, #50]	; 0x32
 800a3ae:	e017      	b.n	800a3e0 <arm_mat_mult_f32+0x16c>
 800a3b0:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800a3b2:	1d1a      	adds	r2, r3, #4
 800a3b4:	64fa      	str	r2, [r7, #76]	; 0x4c
 800a3b6:	ed93 7a00 	vldr	s14, [r3]
 800a3ba:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800a3bc:	edd3 7a00 	vldr	s15, [r3]
 800a3c0:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a3c4:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800a3c8:	ee77 7a27 	vadd.f32	s15, s14, s15
 800a3cc:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800a3d0:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a3d2:	009b      	lsls	r3, r3, #2
 800a3d4:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800a3d6:	4413      	add	r3, r2
 800a3d8:	64bb      	str	r3, [r7, #72]	; 0x48
 800a3da:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 800a3dc:	3b01      	subs	r3, #1
 800a3de:	867b      	strh	r3, [r7, #50]	; 0x32
 800a3e0:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 800a3e2:	2b00      	cmp	r3, #0
 800a3e4:	d1e4      	bne.n	800a3b0 <arm_mat_mult_f32+0x13c>
 800a3e6:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a3e8:	1d1a      	adds	r2, r3, #4
 800a3ea:	643a      	str	r2, [r7, #64]	; 0x40
 800a3ec:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 800a3ee:	601a      	str	r2, [r3, #0]
 800a3f0:	8efb      	ldrh	r3, [r7, #54]	; 0x36
 800a3f2:	3301      	adds	r3, #1
 800a3f4:	86fb      	strh	r3, [r7, #54]	; 0x36
 800a3f6:	68bb      	ldr	r3, [r7, #8]
 800a3f8:	685a      	ldr	r2, [r3, #4]
 800a3fa:	8efb      	ldrh	r3, [r7, #54]	; 0x36
 800a3fc:	009b      	lsls	r3, r3, #2
 800a3fe:	4413      	add	r3, r2
 800a400:	64bb      	str	r3, [r7, #72]	; 0x48
 800a402:	8f7b      	ldrh	r3, [r7, #58]	; 0x3a
 800a404:	3b01      	subs	r3, #1
 800a406:	877b      	strh	r3, [r7, #58]	; 0x3a
 800a408:	8f7b      	ldrh	r3, [r7, #58]	; 0x3a
 800a40a:	2b00      	cmp	r3, #0
 800a40c:	f47f af5d 	bne.w	800a2ca <arm_mat_mult_f32+0x56>
 800a410:	8f3a      	ldrh	r2, [r7, #56]	; 0x38
 800a412:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800a414:	4413      	add	r3, r2
 800a416:	873b      	strh	r3, [r7, #56]	; 0x38
 800a418:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 800a41a:	009b      	lsls	r3, r3, #2
 800a41c:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 800a41e:	4413      	add	r3, r2
 800a420:	647b      	str	r3, [r7, #68]	; 0x44
 800a422:	8ebb      	ldrh	r3, [r7, #52]	; 0x34
 800a424:	3b01      	subs	r3, #1
 800a426:	86bb      	strh	r3, [r7, #52]	; 0x34
 800a428:	8ebb      	ldrh	r3, [r7, #52]	; 0x34
 800a42a:	2b00      	cmp	r3, #0
 800a42c:	f47f af41 	bne.w	800a2b2 <arm_mat_mult_f32+0x3e>
 800a430:	2300      	movs	r3, #0
 800a432:	74fb      	strb	r3, [r7, #19]
 800a434:	f997 3013 	ldrsb.w	r3, [r7, #19]
 800a438:	4618      	mov	r0, r3
 800a43a:	3754      	adds	r7, #84	; 0x54
 800a43c:	46bd      	mov	sp, r7
 800a43e:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a442:	4770      	bx	lr

0800a444 <arm_mat_scale_f32>:
 800a444:	b480      	push	{r7}
 800a446:	b093      	sub	sp, #76	; 0x4c
 800a448:	af00      	add	r7, sp, #0
 800a44a:	60f8      	str	r0, [r7, #12]
 800a44c:	ed87 0a02 	vstr	s0, [r7, #8]
 800a450:	6079      	str	r1, [r7, #4]
 800a452:	68fb      	ldr	r3, [r7, #12]
 800a454:	685b      	ldr	r3, [r3, #4]
 800a456:	647b      	str	r3, [r7, #68]	; 0x44
 800a458:	687b      	ldr	r3, [r7, #4]
 800a45a:	685b      	ldr	r3, [r3, #4]
 800a45c:	643b      	str	r3, [r7, #64]	; 0x40
 800a45e:	68fb      	ldr	r3, [r7, #12]
 800a460:	881b      	ldrh	r3, [r3, #0]
 800a462:	461a      	mov	r2, r3
 800a464:	68fb      	ldr	r3, [r7, #12]
 800a466:	885b      	ldrh	r3, [r3, #2]
 800a468:	fb03 f302 	mul.w	r3, r3, r2
 800a46c:	63bb      	str	r3, [r7, #56]	; 0x38
 800a46e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a470:	089b      	lsrs	r3, r3, #2
 800a472:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a474:	e043      	b.n	800a4fe <arm_mat_scale_f32+0xba>
 800a476:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a478:	681b      	ldr	r3, [r3, #0]
 800a47a:	637b      	str	r3, [r7, #52]	; 0x34
 800a47c:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a47e:	685b      	ldr	r3, [r3, #4]
 800a480:	633b      	str	r3, [r7, #48]	; 0x30
 800a482:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a484:	689b      	ldr	r3, [r3, #8]
 800a486:	62fb      	str	r3, [r7, #44]	; 0x2c
 800a488:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a48a:	68db      	ldr	r3, [r3, #12]
 800a48c:	62bb      	str	r3, [r7, #40]	; 0x28
 800a48e:	ed97 7a0d 	vldr	s14, [r7, #52]	; 0x34
 800a492:	edd7 7a02 	vldr	s15, [r7, #8]
 800a496:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a49a:	edc7 7a09 	vstr	s15, [r7, #36]	; 0x24
 800a49e:	ed97 7a0c 	vldr	s14, [r7, #48]	; 0x30
 800a4a2:	edd7 7a02 	vldr	s15, [r7, #8]
 800a4a6:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a4aa:	edc7 7a08 	vstr	s15, [r7, #32]
 800a4ae:	ed97 7a0b 	vldr	s14, [r7, #44]	; 0x2c
 800a4b2:	edd7 7a02 	vldr	s15, [r7, #8]
 800a4b6:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a4ba:	edc7 7a07 	vstr	s15, [r7, #28]
 800a4be:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 800a4c2:	edd7 7a02 	vldr	s15, [r7, #8]
 800a4c6:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a4ca:	edc7 7a06 	vstr	s15, [r7, #24]
 800a4ce:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a4d0:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 800a4d2:	601a      	str	r2, [r3, #0]
 800a4d4:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a4d6:	3304      	adds	r3, #4
 800a4d8:	6a3a      	ldr	r2, [r7, #32]
 800a4da:	601a      	str	r2, [r3, #0]
 800a4dc:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a4de:	3308      	adds	r3, #8
 800a4e0:	69fa      	ldr	r2, [r7, #28]
 800a4e2:	601a      	str	r2, [r3, #0]
 800a4e4:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a4e6:	330c      	adds	r3, #12
 800a4e8:	69ba      	ldr	r2, [r7, #24]
 800a4ea:	601a      	str	r2, [r3, #0]
 800a4ec:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a4ee:	3310      	adds	r3, #16
 800a4f0:	647b      	str	r3, [r7, #68]	; 0x44
 800a4f2:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a4f4:	3310      	adds	r3, #16
 800a4f6:	643b      	str	r3, [r7, #64]	; 0x40
 800a4f8:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a4fa:	3b01      	subs	r3, #1
 800a4fc:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a4fe:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a500:	2b00      	cmp	r3, #0
 800a502:	d1b8      	bne.n	800a476 <arm_mat_scale_f32+0x32>
 800a504:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800a506:	f003 0303 	and.w	r3, r3, #3
 800a50a:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a50c:	e010      	b.n	800a530 <arm_mat_scale_f32+0xec>
 800a50e:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a510:	1d1a      	adds	r2, r3, #4
 800a512:	647a      	str	r2, [r7, #68]	; 0x44
 800a514:	ed93 7a00 	vldr	s14, [r3]
 800a518:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 800a51a:	1d1a      	adds	r2, r3, #4
 800a51c:	643a      	str	r2, [r7, #64]	; 0x40
 800a51e:	edd7 7a02 	vldr	s15, [r7, #8]
 800a522:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a526:	edc3 7a00 	vstr	s15, [r3]
 800a52a:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a52c:	3b01      	subs	r3, #1
 800a52e:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a530:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800a532:	2b00      	cmp	r3, #0
 800a534:	d1eb      	bne.n	800a50e <arm_mat_scale_f32+0xca>
 800a536:	2300      	movs	r3, #0
 800a538:	75fb      	strb	r3, [r7, #23]
 800a53a:	f997 3017 	ldrsb.w	r3, [r7, #23]
 800a53e:	4618      	mov	r0, r3
 800a540:	374c      	adds	r7, #76	; 0x4c
 800a542:	46bd      	mov	sp, r7
 800a544:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a548:	4770      	bx	lr

0800a54a <arm_max_f32>:
 800a54a:	b480      	push	{r7}
 800a54c:	b08b      	sub	sp, #44	; 0x2c
 800a54e:	af00      	add	r7, sp, #0
 800a550:	60f8      	str	r0, [r7, #12]
 800a552:	60b9      	str	r1, [r7, #8]
 800a554:	607a      	str	r2, [r7, #4]
 800a556:	603b      	str	r3, [r7, #0]
 800a558:	2300      	movs	r3, #0
 800a55a:	61bb      	str	r3, [r7, #24]
 800a55c:	2300      	movs	r3, #0
 800a55e:	61fb      	str	r3, [r7, #28]
 800a560:	68fb      	ldr	r3, [r7, #12]
 800a562:	1d1a      	adds	r2, r3, #4
 800a564:	60fa      	str	r2, [r7, #12]
 800a566:	681b      	ldr	r3, [r3, #0]
 800a568:	627b      	str	r3, [r7, #36]	; 0x24
 800a56a:	68bb      	ldr	r3, [r7, #8]
 800a56c:	3b01      	subs	r3, #1
 800a56e:	089b      	lsrs	r3, r3, #2
 800a570:	623b      	str	r3, [r7, #32]
 800a572:	e051      	b.n	800a618 <arm_max_f32+0xce>
 800a574:	68fb      	ldr	r3, [r7, #12]
 800a576:	1d1a      	adds	r2, r3, #4
 800a578:	60fa      	str	r2, [r7, #12]
 800a57a:	681b      	ldr	r3, [r3, #0]
 800a57c:	617b      	str	r3, [r7, #20]
 800a57e:	68fb      	ldr	r3, [r7, #12]
 800a580:	1d1a      	adds	r2, r3, #4
 800a582:	60fa      	str	r2, [r7, #12]
 800a584:	681b      	ldr	r3, [r3, #0]
 800a586:	613b      	str	r3, [r7, #16]
 800a588:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a58c:	edd7 7a05 	vldr	s15, [r7, #20]
 800a590:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a594:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a598:	d504      	bpl.n	800a5a4 <arm_max_f32+0x5a>
 800a59a:	697b      	ldr	r3, [r7, #20]
 800a59c:	627b      	str	r3, [r7, #36]	; 0x24
 800a59e:	69bb      	ldr	r3, [r7, #24]
 800a5a0:	3301      	adds	r3, #1
 800a5a2:	61fb      	str	r3, [r7, #28]
 800a5a4:	68fb      	ldr	r3, [r7, #12]
 800a5a6:	1d1a      	adds	r2, r3, #4
 800a5a8:	60fa      	str	r2, [r7, #12]
 800a5aa:	681b      	ldr	r3, [r3, #0]
 800a5ac:	617b      	str	r3, [r7, #20]
 800a5ae:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a5b2:	edd7 7a04 	vldr	s15, [r7, #16]
 800a5b6:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a5ba:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a5be:	d504      	bpl.n	800a5ca <arm_max_f32+0x80>
 800a5c0:	693b      	ldr	r3, [r7, #16]
 800a5c2:	627b      	str	r3, [r7, #36]	; 0x24
 800a5c4:	69bb      	ldr	r3, [r7, #24]
 800a5c6:	3302      	adds	r3, #2
 800a5c8:	61fb      	str	r3, [r7, #28]
 800a5ca:	68fb      	ldr	r3, [r7, #12]
 800a5cc:	1d1a      	adds	r2, r3, #4
 800a5ce:	60fa      	str	r2, [r7, #12]
 800a5d0:	681b      	ldr	r3, [r3, #0]
 800a5d2:	613b      	str	r3, [r7, #16]
 800a5d4:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a5d8:	edd7 7a05 	vldr	s15, [r7, #20]
 800a5dc:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a5e0:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a5e4:	d504      	bpl.n	800a5f0 <arm_max_f32+0xa6>
 800a5e6:	697b      	ldr	r3, [r7, #20]
 800a5e8:	627b      	str	r3, [r7, #36]	; 0x24
 800a5ea:	69bb      	ldr	r3, [r7, #24]
 800a5ec:	3303      	adds	r3, #3
 800a5ee:	61fb      	str	r3, [r7, #28]
 800a5f0:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a5f4:	edd7 7a04 	vldr	s15, [r7, #16]
 800a5f8:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a5fc:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a600:	d504      	bpl.n	800a60c <arm_max_f32+0xc2>
 800a602:	693b      	ldr	r3, [r7, #16]
 800a604:	627b      	str	r3, [r7, #36]	; 0x24
 800a606:	69bb      	ldr	r3, [r7, #24]
 800a608:	3304      	adds	r3, #4
 800a60a:	61fb      	str	r3, [r7, #28]
 800a60c:	69bb      	ldr	r3, [r7, #24]
 800a60e:	3304      	adds	r3, #4
 800a610:	61bb      	str	r3, [r7, #24]
 800a612:	6a3b      	ldr	r3, [r7, #32]
 800a614:	3b01      	subs	r3, #1
 800a616:	623b      	str	r3, [r7, #32]
 800a618:	6a3b      	ldr	r3, [r7, #32]
 800a61a:	2b00      	cmp	r3, #0
 800a61c:	d1aa      	bne.n	800a574 <arm_max_f32+0x2a>
 800a61e:	68bb      	ldr	r3, [r7, #8]
 800a620:	3b01      	subs	r3, #1
 800a622:	f003 0303 	and.w	r3, r3, #3
 800a626:	623b      	str	r3, [r7, #32]
 800a628:	e016      	b.n	800a658 <arm_max_f32+0x10e>
 800a62a:	68fb      	ldr	r3, [r7, #12]
 800a62c:	1d1a      	adds	r2, r3, #4
 800a62e:	60fa      	str	r2, [r7, #12]
 800a630:	681b      	ldr	r3, [r3, #0]
 800a632:	617b      	str	r3, [r7, #20]
 800a634:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a638:	edd7 7a05 	vldr	s15, [r7, #20]
 800a63c:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a640:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a644:	d505      	bpl.n	800a652 <arm_max_f32+0x108>
 800a646:	697b      	ldr	r3, [r7, #20]
 800a648:	627b      	str	r3, [r7, #36]	; 0x24
 800a64a:	68ba      	ldr	r2, [r7, #8]
 800a64c:	6a3b      	ldr	r3, [r7, #32]
 800a64e:	1ad3      	subs	r3, r2, r3
 800a650:	61fb      	str	r3, [r7, #28]
 800a652:	6a3b      	ldr	r3, [r7, #32]
 800a654:	3b01      	subs	r3, #1
 800a656:	623b      	str	r3, [r7, #32]
 800a658:	6a3b      	ldr	r3, [r7, #32]
 800a65a:	2b00      	cmp	r3, #0
 800a65c:	d1e5      	bne.n	800a62a <arm_max_f32+0xe0>
 800a65e:	687b      	ldr	r3, [r7, #4]
 800a660:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 800a662:	601a      	str	r2, [r3, #0]
 800a664:	683b      	ldr	r3, [r7, #0]
 800a666:	2b00      	cmp	r3, #0
 800a668:	d002      	beq.n	800a670 <arm_max_f32+0x126>
 800a66a:	683b      	ldr	r3, [r7, #0]
 800a66c:	69fa      	ldr	r2, [r7, #28]
 800a66e:	601a      	str	r2, [r3, #0]
 800a670:	bf00      	nop
 800a672:	372c      	adds	r7, #44	; 0x2c
 800a674:	46bd      	mov	sp, r7
 800a676:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a67a:	4770      	bx	lr

0800a67c <arm_min_f32>:
 800a67c:	b480      	push	{r7}
 800a67e:	b08b      	sub	sp, #44	; 0x2c
 800a680:	af00      	add	r7, sp, #0
 800a682:	60f8      	str	r0, [r7, #12]
 800a684:	60b9      	str	r1, [r7, #8]
 800a686:	607a      	str	r2, [r7, #4]
 800a688:	603b      	str	r3, [r7, #0]
 800a68a:	2300      	movs	r3, #0
 800a68c:	61bb      	str	r3, [r7, #24]
 800a68e:	2300      	movs	r3, #0
 800a690:	61fb      	str	r3, [r7, #28]
 800a692:	68fb      	ldr	r3, [r7, #12]
 800a694:	1d1a      	adds	r2, r3, #4
 800a696:	60fa      	str	r2, [r7, #12]
 800a698:	681b      	ldr	r3, [r3, #0]
 800a69a:	627b      	str	r3, [r7, #36]	; 0x24
 800a69c:	68bb      	ldr	r3, [r7, #8]
 800a69e:	3b01      	subs	r3, #1
 800a6a0:	089b      	lsrs	r3, r3, #2
 800a6a2:	623b      	str	r3, [r7, #32]
 800a6a4:	e051      	b.n	800a74a <arm_min_f32+0xce>
 800a6a6:	68fb      	ldr	r3, [r7, #12]
 800a6a8:	1d1a      	adds	r2, r3, #4
 800a6aa:	60fa      	str	r2, [r7, #12]
 800a6ac:	681b      	ldr	r3, [r3, #0]
 800a6ae:	617b      	str	r3, [r7, #20]
 800a6b0:	68fb      	ldr	r3, [r7, #12]
 800a6b2:	1d1a      	adds	r2, r3, #4
 800a6b4:	60fa      	str	r2, [r7, #12]
 800a6b6:	681b      	ldr	r3, [r3, #0]
 800a6b8:	613b      	str	r3, [r7, #16]
 800a6ba:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a6be:	edd7 7a05 	vldr	s15, [r7, #20]
 800a6c2:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a6c6:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a6ca:	dd04      	ble.n	800a6d6 <arm_min_f32+0x5a>
 800a6cc:	697b      	ldr	r3, [r7, #20]
 800a6ce:	627b      	str	r3, [r7, #36]	; 0x24
 800a6d0:	69bb      	ldr	r3, [r7, #24]
 800a6d2:	3301      	adds	r3, #1
 800a6d4:	61fb      	str	r3, [r7, #28]
 800a6d6:	68fb      	ldr	r3, [r7, #12]
 800a6d8:	1d1a      	adds	r2, r3, #4
 800a6da:	60fa      	str	r2, [r7, #12]
 800a6dc:	681b      	ldr	r3, [r3, #0]
 800a6de:	617b      	str	r3, [r7, #20]
 800a6e0:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a6e4:	edd7 7a04 	vldr	s15, [r7, #16]
 800a6e8:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a6ec:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a6f0:	dd04      	ble.n	800a6fc <arm_min_f32+0x80>
 800a6f2:	693b      	ldr	r3, [r7, #16]
 800a6f4:	627b      	str	r3, [r7, #36]	; 0x24
 800a6f6:	69bb      	ldr	r3, [r7, #24]
 800a6f8:	3302      	adds	r3, #2
 800a6fa:	61fb      	str	r3, [r7, #28]
 800a6fc:	68fb      	ldr	r3, [r7, #12]
 800a6fe:	1d1a      	adds	r2, r3, #4
 800a700:	60fa      	str	r2, [r7, #12]
 800a702:	681b      	ldr	r3, [r3, #0]
 800a704:	613b      	str	r3, [r7, #16]
 800a706:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a70a:	edd7 7a05 	vldr	s15, [r7, #20]
 800a70e:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a712:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a716:	dd04      	ble.n	800a722 <arm_min_f32+0xa6>
 800a718:	697b      	ldr	r3, [r7, #20]
 800a71a:	627b      	str	r3, [r7, #36]	; 0x24
 800a71c:	69bb      	ldr	r3, [r7, #24]
 800a71e:	3303      	adds	r3, #3
 800a720:	61fb      	str	r3, [r7, #28]
 800a722:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a726:	edd7 7a04 	vldr	s15, [r7, #16]
 800a72a:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a72e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a732:	dd04      	ble.n	800a73e <arm_min_f32+0xc2>
 800a734:	693b      	ldr	r3, [r7, #16]
 800a736:	627b      	str	r3, [r7, #36]	; 0x24
 800a738:	69bb      	ldr	r3, [r7, #24]
 800a73a:	3304      	adds	r3, #4
 800a73c:	61fb      	str	r3, [r7, #28]
 800a73e:	69bb      	ldr	r3, [r7, #24]
 800a740:	3304      	adds	r3, #4
 800a742:	61bb      	str	r3, [r7, #24]
 800a744:	6a3b      	ldr	r3, [r7, #32]
 800a746:	3b01      	subs	r3, #1
 800a748:	623b      	str	r3, [r7, #32]
 800a74a:	6a3b      	ldr	r3, [r7, #32]
 800a74c:	2b00      	cmp	r3, #0
 800a74e:	d1aa      	bne.n	800a6a6 <arm_min_f32+0x2a>
 800a750:	68bb      	ldr	r3, [r7, #8]
 800a752:	3b01      	subs	r3, #1
 800a754:	f003 0303 	and.w	r3, r3, #3
 800a758:	623b      	str	r3, [r7, #32]
 800a75a:	e016      	b.n	800a78a <arm_min_f32+0x10e>
 800a75c:	68fb      	ldr	r3, [r7, #12]
 800a75e:	1d1a      	adds	r2, r3, #4
 800a760:	60fa      	str	r2, [r7, #12]
 800a762:	681b      	ldr	r3, [r3, #0]
 800a764:	617b      	str	r3, [r7, #20]
 800a766:	ed97 7a09 	vldr	s14, [r7, #36]	; 0x24
 800a76a:	edd7 7a05 	vldr	s15, [r7, #20]
 800a76e:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800a772:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800a776:	dd05      	ble.n	800a784 <arm_min_f32+0x108>
 800a778:	697b      	ldr	r3, [r7, #20]
 800a77a:	627b      	str	r3, [r7, #36]	; 0x24
 800a77c:	68ba      	ldr	r2, [r7, #8]
 800a77e:	6a3b      	ldr	r3, [r7, #32]
 800a780:	1ad3      	subs	r3, r2, r3
 800a782:	61fb      	str	r3, [r7, #28]
 800a784:	6a3b      	ldr	r3, [r7, #32]
 800a786:	3b01      	subs	r3, #1
 800a788:	623b      	str	r3, [r7, #32]
 800a78a:	6a3b      	ldr	r3, [r7, #32]
 800a78c:	2b00      	cmp	r3, #0
 800a78e:	d1e5      	bne.n	800a75c <arm_min_f32+0xe0>
 800a790:	687b      	ldr	r3, [r7, #4]
 800a792:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 800a794:	601a      	str	r2, [r3, #0]
 800a796:	683b      	ldr	r3, [r7, #0]
 800a798:	2b00      	cmp	r3, #0
 800a79a:	d002      	beq.n	800a7a2 <arm_min_f32+0x126>
 800a79c:	683b      	ldr	r3, [r7, #0]
 800a79e:	69fa      	ldr	r2, [r7, #28]
 800a7a0:	601a      	str	r2, [r3, #0]
 800a7a2:	bf00      	nop
 800a7a4:	372c      	adds	r7, #44	; 0x2c
 800a7a6:	46bd      	mov	sp, r7
 800a7a8:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a7ac:	4770      	bx	lr

0800a7ae <arm_mult_f32>:
 800a7ae:	b480      	push	{r7}
 800a7b0:	b093      	sub	sp, #76	; 0x4c
 800a7b2:	af00      	add	r7, sp, #0
 800a7b4:	60f8      	str	r0, [r7, #12]
 800a7b6:	60b9      	str	r1, [r7, #8]
 800a7b8:	607a      	str	r2, [r7, #4]
 800a7ba:	603b      	str	r3, [r7, #0]
 800a7bc:	683b      	ldr	r3, [r7, #0]
 800a7be:	089b      	lsrs	r3, r3, #2
 800a7c0:	647b      	str	r3, [r7, #68]	; 0x44
 800a7c2:	e052      	b.n	800a86a <arm_mult_f32+0xbc>
 800a7c4:	68fb      	ldr	r3, [r7, #12]
 800a7c6:	681b      	ldr	r3, [r3, #0]
 800a7c8:	643b      	str	r3, [r7, #64]	; 0x40
 800a7ca:	68bb      	ldr	r3, [r7, #8]
 800a7cc:	681b      	ldr	r3, [r3, #0]
 800a7ce:	63fb      	str	r3, [r7, #60]	; 0x3c
 800a7d0:	68fb      	ldr	r3, [r7, #12]
 800a7d2:	685b      	ldr	r3, [r3, #4]
 800a7d4:	63bb      	str	r3, [r7, #56]	; 0x38
 800a7d6:	68bb      	ldr	r3, [r7, #8]
 800a7d8:	685b      	ldr	r3, [r3, #4]
 800a7da:	637b      	str	r3, [r7, #52]	; 0x34
 800a7dc:	ed97 7a10 	vldr	s14, [r7, #64]	; 0x40
 800a7e0:	edd7 7a0f 	vldr	s15, [r7, #60]	; 0x3c
 800a7e4:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a7e8:	edc7 7a0c 	vstr	s15, [r7, #48]	; 0x30
 800a7ec:	68fb      	ldr	r3, [r7, #12]
 800a7ee:	689b      	ldr	r3, [r3, #8]
 800a7f0:	62fb      	str	r3, [r7, #44]	; 0x2c
 800a7f2:	68bb      	ldr	r3, [r7, #8]
 800a7f4:	689b      	ldr	r3, [r3, #8]
 800a7f6:	62bb      	str	r3, [r7, #40]	; 0x28
 800a7f8:	ed97 7a0e 	vldr	s14, [r7, #56]	; 0x38
 800a7fc:	edd7 7a0d 	vldr	s15, [r7, #52]	; 0x34
 800a800:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a804:	edc7 7a09 	vstr	s15, [r7, #36]	; 0x24
 800a808:	68fb      	ldr	r3, [r7, #12]
 800a80a:	68db      	ldr	r3, [r3, #12]
 800a80c:	623b      	str	r3, [r7, #32]
 800a80e:	687b      	ldr	r3, [r7, #4]
 800a810:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 800a812:	601a      	str	r2, [r3, #0]
 800a814:	68bb      	ldr	r3, [r7, #8]
 800a816:	68db      	ldr	r3, [r3, #12]
 800a818:	61fb      	str	r3, [r7, #28]
 800a81a:	ed97 7a0b 	vldr	s14, [r7, #44]	; 0x2c
 800a81e:	edd7 7a0a 	vldr	s15, [r7, #40]	; 0x28
 800a822:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a826:	edc7 7a06 	vstr	s15, [r7, #24]
 800a82a:	687b      	ldr	r3, [r7, #4]
 800a82c:	3304      	adds	r3, #4
 800a82e:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 800a830:	601a      	str	r2, [r3, #0]
 800a832:	ed97 7a08 	vldr	s14, [r7, #32]
 800a836:	edd7 7a07 	vldr	s15, [r7, #28]
 800a83a:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a83e:	edc7 7a05 	vstr	s15, [r7, #20]
 800a842:	687b      	ldr	r3, [r7, #4]
 800a844:	3308      	adds	r3, #8
 800a846:	69ba      	ldr	r2, [r7, #24]
 800a848:	601a      	str	r2, [r3, #0]
 800a84a:	687b      	ldr	r3, [r7, #4]
 800a84c:	330c      	adds	r3, #12
 800a84e:	697a      	ldr	r2, [r7, #20]
 800a850:	601a      	str	r2, [r3, #0]
 800a852:	68fb      	ldr	r3, [r7, #12]
 800a854:	3310      	adds	r3, #16
 800a856:	60fb      	str	r3, [r7, #12]
 800a858:	68bb      	ldr	r3, [r7, #8]
 800a85a:	3310      	adds	r3, #16
 800a85c:	60bb      	str	r3, [r7, #8]
 800a85e:	687b      	ldr	r3, [r7, #4]
 800a860:	3310      	adds	r3, #16
 800a862:	607b      	str	r3, [r7, #4]
 800a864:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a866:	3b01      	subs	r3, #1
 800a868:	647b      	str	r3, [r7, #68]	; 0x44
 800a86a:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a86c:	2b00      	cmp	r3, #0
 800a86e:	d1a9      	bne.n	800a7c4 <arm_mult_f32+0x16>
 800a870:	683b      	ldr	r3, [r7, #0]
 800a872:	f003 0303 	and.w	r3, r3, #3
 800a876:	647b      	str	r3, [r7, #68]	; 0x44
 800a878:	e013      	b.n	800a8a2 <arm_mult_f32+0xf4>
 800a87a:	68fb      	ldr	r3, [r7, #12]
 800a87c:	1d1a      	adds	r2, r3, #4
 800a87e:	60fa      	str	r2, [r7, #12]
 800a880:	ed93 7a00 	vldr	s14, [r3]
 800a884:	68bb      	ldr	r3, [r7, #8]
 800a886:	1d1a      	adds	r2, r3, #4
 800a888:	60ba      	str	r2, [r7, #8]
 800a88a:	edd3 7a00 	vldr	s15, [r3]
 800a88e:	687b      	ldr	r3, [r7, #4]
 800a890:	1d1a      	adds	r2, r3, #4
 800a892:	607a      	str	r2, [r7, #4]
 800a894:	ee67 7a27 	vmul.f32	s15, s14, s15
 800a898:	edc3 7a00 	vstr	s15, [r3]
 800a89c:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a89e:	3b01      	subs	r3, #1
 800a8a0:	647b      	str	r3, [r7, #68]	; 0x44
 800a8a2:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 800a8a4:	2b00      	cmp	r3, #0
 800a8a6:	d1e8      	bne.n	800a87a <arm_mult_f32+0xcc>
 800a8a8:	bf00      	nop
 800a8aa:	374c      	adds	r7, #76	; 0x4c
 800a8ac:	46bd      	mov	sp, r7
 800a8ae:	f85d 7b04 	ldr.w	r7, [sp], #4
 800a8b2:	4770      	bx	lr

0800a8b4 <__libc_init_array>:
 800a8b4:	b570      	push	{r4, r5, r6, lr}
 800a8b6:	4d0d      	ldr	r5, [pc, #52]	; (800a8ec <__libc_init_array+0x38>)
 800a8b8:	4c0d      	ldr	r4, [pc, #52]	; (800a8f0 <__libc_init_array+0x3c>)
 800a8ba:	1b64      	subs	r4, r4, r5
 800a8bc:	10a4      	asrs	r4, r4, #2
 800a8be:	2600      	movs	r6, #0
 800a8c0:	42a6      	cmp	r6, r4
 800a8c2:	d109      	bne.n	800a8d8 <__libc_init_array+0x24>
 800a8c4:	4d0b      	ldr	r5, [pc, #44]	; (800a8f4 <__libc_init_array+0x40>)
 800a8c6:	4c0c      	ldr	r4, [pc, #48]	; (800a8f8 <__libc_init_array+0x44>)
 800a8c8:	f002 fbb8 	bl	800d03c <_init>
 800a8cc:	1b64      	subs	r4, r4, r5
 800a8ce:	10a4      	asrs	r4, r4, #2
 800a8d0:	2600      	movs	r6, #0
 800a8d2:	42a6      	cmp	r6, r4
 800a8d4:	d105      	bne.n	800a8e2 <__libc_init_array+0x2e>
 800a8d6:	bd70      	pop	{r4, r5, r6, pc}
 800a8d8:	f855 3b04 	ldr.w	r3, [r5], #4
 800a8dc:	4798      	blx	r3
 800a8de:	3601      	adds	r6, #1
 800a8e0:	e7ee      	b.n	800a8c0 <__libc_init_array+0xc>
 800a8e2:	f855 3b04 	ldr.w	r3, [r5], #4
 800a8e6:	4798      	blx	r3
 800a8e8:	3601      	adds	r6, #1
 800a8ea:	e7f2      	b.n	800a8d2 <__libc_init_array+0x1e>
 800a8ec:	0800d9c8 	.word	0x0800d9c8
 800a8f0:	0800d9c8 	.word	0x0800d9c8
 800a8f4:	0800d9c8 	.word	0x0800d9c8
 800a8f8:	0800d9cc 	.word	0x0800d9cc

0800a8fc <malloc>:
 800a8fc:	4b02      	ldr	r3, [pc, #8]	; (800a908 <malloc+0xc>)
 800a8fe:	4601      	mov	r1, r0
 800a900:	6818      	ldr	r0, [r3, #0]
 800a902:	f7f6 bc4b 	b.w	800119c <_malloc_r>
 800a906:	bf00      	nop
 800a908:	20000080 	.word	0x20000080

0800a90c <memcpy>:
 800a90c:	440a      	add	r2, r1
 800a90e:	4291      	cmp	r1, r2
 800a910:	f100 33ff 	add.w	r3, r0, #4294967295
 800a914:	d100      	bne.n	800a918 <memcpy+0xc>
 800a916:	4770      	bx	lr
 800a918:	b510      	push	{r4, lr}
 800a91a:	f811 4b01 	ldrb.w	r4, [r1], #1
 800a91e:	f803 4f01 	strb.w	r4, [r3, #1]!
 800a922:	4291      	cmp	r1, r2
 800a924:	d1f9      	bne.n	800a91a <memcpy+0xe>
 800a926:	bd10      	pop	{r4, pc}

0800a928 <memset>:
 800a928:	4402      	add	r2, r0
 800a92a:	4603      	mov	r3, r0
 800a92c:	4293      	cmp	r3, r2
 800a92e:	d100      	bne.n	800a932 <memset+0xa>
 800a930:	4770      	bx	lr
 800a932:	f803 1b01 	strb.w	r1, [r3], #1
 800a936:	e7f9      	b.n	800a92c <memset+0x4>

0800a938 <__cvt>:
 800a938:	e92d 47ff 	stmdb	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
 800a93c:	ec55 4b10 	vmov	r4, r5, d0
 800a940:	2d00      	cmp	r5, #0
 800a942:	460e      	mov	r6, r1
 800a944:	4691      	mov	r9, r2
 800a946:	4619      	mov	r1, r3
 800a948:	f8dd a030 	ldr.w	sl, [sp, #48]	; 0x30
 800a94c:	f04f 0200 	mov.w	r2, #0
 800a950:	da03      	bge.n	800a95a <__cvt+0x22>
 800a952:	f105 4300 	add.w	r3, r5, #2147483648	; 0x80000000
 800a956:	461d      	mov	r5, r3
 800a958:	222d      	movs	r2, #45	; 0x2d
 800a95a:	9f0d      	ldr	r7, [sp, #52]	; 0x34
 800a95c:	700a      	strb	r2, [r1, #0]
 800a95e:	f027 0720 	bic.w	r7, r7, #32
 800a962:	2f46      	cmp	r7, #70	; 0x46
 800a964:	d004      	beq.n	800a970 <__cvt+0x38>
 800a966:	2f45      	cmp	r7, #69	; 0x45
 800a968:	d100      	bne.n	800a96c <__cvt+0x34>
 800a96a:	3601      	adds	r6, #1
 800a96c:	2102      	movs	r1, #2
 800a96e:	e000      	b.n	800a972 <__cvt+0x3a>
 800a970:	2103      	movs	r1, #3
 800a972:	ab03      	add	r3, sp, #12
 800a974:	9301      	str	r3, [sp, #4]
 800a976:	ab02      	add	r3, sp, #8
 800a978:	9300      	str	r3, [sp, #0]
 800a97a:	4632      	mov	r2, r6
 800a97c:	4653      	mov	r3, sl
 800a97e:	ec45 4b10 	vmov	d0, r4, r5
 800a982:	f000 fc01 	bl	800b188 <_dtoa_r>
 800a986:	2f47      	cmp	r7, #71	; 0x47
 800a988:	4680      	mov	r8, r0
 800a98a:	d102      	bne.n	800a992 <__cvt+0x5a>
 800a98c:	f019 0f01 	tst.w	r9, #1
 800a990:	d022      	beq.n	800a9d8 <__cvt+0xa0>
 800a992:	2f46      	cmp	r7, #70	; 0x46
 800a994:	eb08 0906 	add.w	r9, r8, r6
 800a998:	d111      	bne.n	800a9be <__cvt+0x86>
 800a99a:	f898 3000 	ldrb.w	r3, [r8]
 800a99e:	2b30      	cmp	r3, #48	; 0x30
 800a9a0:	d10a      	bne.n	800a9b8 <__cvt+0x80>
 800a9a2:	2200      	movs	r2, #0
 800a9a4:	2300      	movs	r3, #0
 800a9a6:	4620      	mov	r0, r4
 800a9a8:	4629      	mov	r1, r5
 800a9aa:	f7f6 f83d 	bl	8000a28 <__aeabi_dcmpeq>
 800a9ae:	b918      	cbnz	r0, 800a9b8 <__cvt+0x80>
 800a9b0:	f1c6 0601 	rsb	r6, r6, #1
 800a9b4:	f8ca 6000 	str.w	r6, [sl]
 800a9b8:	f8da 3000 	ldr.w	r3, [sl]
 800a9bc:	4499      	add	r9, r3
 800a9be:	2200      	movs	r2, #0
 800a9c0:	2300      	movs	r3, #0
 800a9c2:	4620      	mov	r0, r4
 800a9c4:	4629      	mov	r1, r5
 800a9c6:	f7f6 f82f 	bl	8000a28 <__aeabi_dcmpeq>
 800a9ca:	b108      	cbz	r0, 800a9d0 <__cvt+0x98>
 800a9cc:	f8cd 900c 	str.w	r9, [sp, #12]
 800a9d0:	2230      	movs	r2, #48	; 0x30
 800a9d2:	9b03      	ldr	r3, [sp, #12]
 800a9d4:	454b      	cmp	r3, r9
 800a9d6:	d308      	bcc.n	800a9ea <__cvt+0xb2>
 800a9d8:	9b03      	ldr	r3, [sp, #12]
 800a9da:	9a0e      	ldr	r2, [sp, #56]	; 0x38
 800a9dc:	eba3 0308 	sub.w	r3, r3, r8
 800a9e0:	4640      	mov	r0, r8
 800a9e2:	6013      	str	r3, [r2, #0]
 800a9e4:	b004      	add	sp, #16
 800a9e6:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 800a9ea:	1c59      	adds	r1, r3, #1
 800a9ec:	9103      	str	r1, [sp, #12]
 800a9ee:	701a      	strb	r2, [r3, #0]
 800a9f0:	e7ef      	b.n	800a9d2 <__cvt+0x9a>

0800a9f2 <__exponent>:
 800a9f2:	b5f7      	push	{r0, r1, r2, r4, r5, r6, r7, lr}
 800a9f4:	2900      	cmp	r1, #0
 800a9f6:	bfba      	itte	lt
 800a9f8:	4249      	neglt	r1, r1
 800a9fa:	232d      	movlt	r3, #45	; 0x2d
 800a9fc:	232b      	movge	r3, #43	; 0x2b
 800a9fe:	2909      	cmp	r1, #9
 800aa00:	7002      	strb	r2, [r0, #0]
 800aa02:	7043      	strb	r3, [r0, #1]
 800aa04:	dd21      	ble.n	800aa4a <__exponent+0x58>
 800aa06:	f10d 0307 	add.w	r3, sp, #7
 800aa0a:	461f      	mov	r7, r3
 800aa0c:	260a      	movs	r6, #10
 800aa0e:	fb91 f2f6 	sdiv	r2, r1, r6
 800aa12:	fb06 1412 	mls	r4, r6, r2, r1
 800aa16:	3430      	adds	r4, #48	; 0x30
 800aa18:	2963      	cmp	r1, #99	; 0x63
 800aa1a:	f103 35ff 	add.w	r5, r3, #4294967295
 800aa1e:	f803 4c01 	strb.w	r4, [r3, #-1]
 800aa22:	dc0a      	bgt.n	800aa3a <__exponent+0x48>
 800aa24:	3230      	adds	r2, #48	; 0x30
 800aa26:	f805 2c01 	strb.w	r2, [r5, #-1]
 800aa2a:	3b02      	subs	r3, #2
 800aa2c:	1c82      	adds	r2, r0, #2
 800aa2e:	42bb      	cmp	r3, r7
 800aa30:	4614      	mov	r4, r2
 800aa32:	d305      	bcc.n	800aa40 <__exponent+0x4e>
 800aa34:	1a20      	subs	r0, r4, r0
 800aa36:	b003      	add	sp, #12
 800aa38:	bdf0      	pop	{r4, r5, r6, r7, pc}
 800aa3a:	462b      	mov	r3, r5
 800aa3c:	4611      	mov	r1, r2
 800aa3e:	e7e6      	b.n	800aa0e <__exponent+0x1c>
 800aa40:	f813 1b01 	ldrb.w	r1, [r3], #1
 800aa44:	f802 1b01 	strb.w	r1, [r2], #1
 800aa48:	e7f1      	b.n	800aa2e <__exponent+0x3c>
 800aa4a:	2330      	movs	r3, #48	; 0x30
 800aa4c:	4419      	add	r1, r3
 800aa4e:	7083      	strb	r3, [r0, #2]
 800aa50:	1d04      	adds	r4, r0, #4
 800aa52:	70c1      	strb	r1, [r0, #3]
 800aa54:	e7ee      	b.n	800aa34 <__exponent+0x42>
	...

0800aa58 <_printf_float>:
 800aa58:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800aa5c:	b08d      	sub	sp, #52	; 0x34
 800aa5e:	460c      	mov	r4, r1
 800aa60:	f8dd 8058 	ldr.w	r8, [sp, #88]	; 0x58
 800aa64:	4616      	mov	r6, r2
 800aa66:	461f      	mov	r7, r3
 800aa68:	4605      	mov	r5, r0
 800aa6a:	f001 f947 	bl	800bcfc <_localeconv_r>
 800aa6e:	6803      	ldr	r3, [r0, #0]
 800aa70:	9306      	str	r3, [sp, #24]
 800aa72:	4618      	mov	r0, r3
 800aa74:	f7f5 ff7a 	bl	800096c <strlen>
 800aa78:	2300      	movs	r3, #0
 800aa7a:	930a      	str	r3, [sp, #40]	; 0x28
 800aa7c:	f8d8 3000 	ldr.w	r3, [r8]
 800aa80:	9007      	str	r0, [sp, #28]
 800aa82:	3307      	adds	r3, #7
 800aa84:	f023 0307 	bic.w	r3, r3, #7
 800aa88:	f103 0208 	add.w	r2, r3, #8
 800aa8c:	f894 a018 	ldrb.w	sl, [r4, #24]
 800aa90:	f8d4 b000 	ldr.w	fp, [r4]
 800aa94:	f8c8 2000 	str.w	r2, [r8]
 800aa98:	e9d3 2300 	ldrd	r2, r3, [r3]
 800aa9c:	e9c4 2312 	strd	r2, r3, [r4, #72]	; 0x48
 800aaa0:	ed94 7b12 	vldr	d7, [r4, #72]	; 0x48
 800aaa4:	ed8d 7b04 	vstr	d7, [sp, #16]
 800aaa8:	e9dd 8304 	ldrd	r8, r3, [sp, #16]
 800aaac:	f023 4900 	bic.w	r9, r3, #2147483648	; 0x80000000
 800aab0:	f04f 32ff 	mov.w	r2, #4294967295
 800aab4:	4ba8      	ldr	r3, [pc, #672]	; (800ad58 <_printf_float+0x300>)
 800aab6:	4640      	mov	r0, r8
 800aab8:	4649      	mov	r1, r9
 800aaba:	f7f5 ffe7 	bl	8000a8c <__aeabi_dcmpun>
 800aabe:	bb70      	cbnz	r0, 800ab1e <_printf_float+0xc6>
 800aac0:	f04f 32ff 	mov.w	r2, #4294967295
 800aac4:	4ba4      	ldr	r3, [pc, #656]	; (800ad58 <_printf_float+0x300>)
 800aac6:	4640      	mov	r0, r8
 800aac8:	4649      	mov	r1, r9
 800aaca:	f7f5 ffc1 	bl	8000a50 <__aeabi_dcmple>
 800aace:	bb30      	cbnz	r0, 800ab1e <_printf_float+0xc6>
 800aad0:	2200      	movs	r2, #0
 800aad2:	2300      	movs	r3, #0
 800aad4:	e9dd 0104 	ldrd	r0, r1, [sp, #16]
 800aad8:	f7f5 ffb0 	bl	8000a3c <__aeabi_dcmplt>
 800aadc:	b110      	cbz	r0, 800aae4 <_printf_float+0x8c>
 800aade:	232d      	movs	r3, #45	; 0x2d
 800aae0:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 800aae4:	4a9d      	ldr	r2, [pc, #628]	; (800ad5c <_printf_float+0x304>)
 800aae6:	4b9e      	ldr	r3, [pc, #632]	; (800ad60 <_printf_float+0x308>)
 800aae8:	f1ba 0f47 	cmp.w	sl, #71	; 0x47
 800aaec:	bf94      	ite	ls
 800aaee:	4690      	movls	r8, r2
 800aaf0:	4698      	movhi	r8, r3
 800aaf2:	2303      	movs	r3, #3
 800aaf4:	f02b 0204 	bic.w	r2, fp, #4
 800aaf8:	6123      	str	r3, [r4, #16]
 800aafa:	6022      	str	r2, [r4, #0]
 800aafc:	f04f 0900 	mov.w	r9, #0
 800ab00:	9700      	str	r7, [sp, #0]
 800ab02:	4633      	mov	r3, r6
 800ab04:	aa0b      	add	r2, sp, #44	; 0x2c
 800ab06:	4621      	mov	r1, r4
 800ab08:	4628      	mov	r0, r5
 800ab0a:	f000 f9d3 	bl	800aeb4 <_printf_common>
 800ab0e:	3001      	adds	r0, #1
 800ab10:	f040 8090 	bne.w	800ac34 <_printf_float+0x1dc>
 800ab14:	f04f 30ff 	mov.w	r0, #4294967295
 800ab18:	b00d      	add	sp, #52	; 0x34
 800ab1a:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800ab1e:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 800ab22:	4610      	mov	r0, r2
 800ab24:	4619      	mov	r1, r3
 800ab26:	f7f5 ffb1 	bl	8000a8c <__aeabi_dcmpun>
 800ab2a:	b160      	cbz	r0, 800ab46 <_printf_float+0xee>
 800ab2c:	2200      	movs	r2, #0
 800ab2e:	2300      	movs	r3, #0
 800ab30:	e9dd 0104 	ldrd	r0, r1, [sp, #16]
 800ab34:	f7f5 ff82 	bl	8000a3c <__aeabi_dcmplt>
 800ab38:	b110      	cbz	r0, 800ab40 <_printf_float+0xe8>
 800ab3a:	232d      	movs	r3, #45	; 0x2d
 800ab3c:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 800ab40:	4a88      	ldr	r2, [pc, #544]	; (800ad64 <_printf_float+0x30c>)
 800ab42:	4b89      	ldr	r3, [pc, #548]	; (800ad68 <_printf_float+0x310>)
 800ab44:	e7d0      	b.n	800aae8 <_printf_float+0x90>
 800ab46:	6863      	ldr	r3, [r4, #4]
 800ab48:	1c5a      	adds	r2, r3, #1
 800ab4a:	f00a 09df 	and.w	r9, sl, #223	; 0xdf
 800ab4e:	d13e      	bne.n	800abce <_printf_float+0x176>
 800ab50:	2306      	movs	r3, #6
 800ab52:	6063      	str	r3, [r4, #4]
 800ab54:	2300      	movs	r3, #0
 800ab56:	9303      	str	r3, [sp, #12]
 800ab58:	ab0a      	add	r3, sp, #40	; 0x28
 800ab5a:	f44b 6280 	orr.w	r2, fp, #1024	; 0x400
 800ab5e:	e9cd a301 	strd	sl, r3, [sp, #4]
 800ab62:	ab09      	add	r3, sp, #36	; 0x24
 800ab64:	9300      	str	r3, [sp, #0]
 800ab66:	6861      	ldr	r1, [r4, #4]
 800ab68:	6022      	str	r2, [r4, #0]
 800ab6a:	f10d 0323 	add.w	r3, sp, #35	; 0x23
 800ab6e:	ed9d 0b04 	vldr	d0, [sp, #16]
 800ab72:	4628      	mov	r0, r5
 800ab74:	f7ff fee0 	bl	800a938 <__cvt>
 800ab78:	f1b9 0f47 	cmp.w	r9, #71	; 0x47
 800ab7c:	4680      	mov	r8, r0
 800ab7e:	9909      	ldr	r1, [sp, #36]	; 0x24
 800ab80:	d108      	bne.n	800ab94 <_printf_float+0x13c>
 800ab82:	1cc8      	adds	r0, r1, #3
 800ab84:	db02      	blt.n	800ab8c <_printf_float+0x134>
 800ab86:	6863      	ldr	r3, [r4, #4]
 800ab88:	4299      	cmp	r1, r3
 800ab8a:	dd41      	ble.n	800ac10 <_printf_float+0x1b8>
 800ab8c:	f1aa 0a02 	sub.w	sl, sl, #2
 800ab90:	fa5f fa8a 	uxtb.w	sl, sl
 800ab94:	f1ba 0f65 	cmp.w	sl, #101	; 0x65
 800ab98:	d820      	bhi.n	800abdc <_printf_float+0x184>
 800ab9a:	3901      	subs	r1, #1
 800ab9c:	4652      	mov	r2, sl
 800ab9e:	f104 0050 	add.w	r0, r4, #80	; 0x50
 800aba2:	9109      	str	r1, [sp, #36]	; 0x24
 800aba4:	f7ff ff25 	bl	800a9f2 <__exponent>
 800aba8:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 800abaa:	1813      	adds	r3, r2, r0
 800abac:	2a01      	cmp	r2, #1
 800abae:	4681      	mov	r9, r0
 800abb0:	6123      	str	r3, [r4, #16]
 800abb2:	dc02      	bgt.n	800abba <_printf_float+0x162>
 800abb4:	6822      	ldr	r2, [r4, #0]
 800abb6:	07d2      	lsls	r2, r2, #31
 800abb8:	d501      	bpl.n	800abbe <_printf_float+0x166>
 800abba:	3301      	adds	r3, #1
 800abbc:	6123      	str	r3, [r4, #16]
 800abbe:	f89d 3023 	ldrb.w	r3, [sp, #35]	; 0x23
 800abc2:	2b00      	cmp	r3, #0
 800abc4:	d09c      	beq.n	800ab00 <_printf_float+0xa8>
 800abc6:	232d      	movs	r3, #45	; 0x2d
 800abc8:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 800abcc:	e798      	b.n	800ab00 <_printf_float+0xa8>
 800abce:	f1b9 0f47 	cmp.w	r9, #71	; 0x47
 800abd2:	d1bf      	bne.n	800ab54 <_printf_float+0xfc>
 800abd4:	2b00      	cmp	r3, #0
 800abd6:	d1bd      	bne.n	800ab54 <_printf_float+0xfc>
 800abd8:	2301      	movs	r3, #1
 800abda:	e7ba      	b.n	800ab52 <_printf_float+0xfa>
 800abdc:	f1ba 0f66 	cmp.w	sl, #102	; 0x66
 800abe0:	d118      	bne.n	800ac14 <_printf_float+0x1bc>
 800abe2:	2900      	cmp	r1, #0
 800abe4:	6863      	ldr	r3, [r4, #4]
 800abe6:	dd0b      	ble.n	800ac00 <_printf_float+0x1a8>
 800abe8:	6121      	str	r1, [r4, #16]
 800abea:	b913      	cbnz	r3, 800abf2 <_printf_float+0x19a>
 800abec:	6822      	ldr	r2, [r4, #0]
 800abee:	07d0      	lsls	r0, r2, #31
 800abf0:	d502      	bpl.n	800abf8 <_printf_float+0x1a0>
 800abf2:	3301      	adds	r3, #1
 800abf4:	440b      	add	r3, r1
 800abf6:	6123      	str	r3, [r4, #16]
 800abf8:	65a1      	str	r1, [r4, #88]	; 0x58
 800abfa:	f04f 0900 	mov.w	r9, #0
 800abfe:	e7de      	b.n	800abbe <_printf_float+0x166>
 800ac00:	b913      	cbnz	r3, 800ac08 <_printf_float+0x1b0>
 800ac02:	6822      	ldr	r2, [r4, #0]
 800ac04:	07d2      	lsls	r2, r2, #31
 800ac06:	d501      	bpl.n	800ac0c <_printf_float+0x1b4>
 800ac08:	3302      	adds	r3, #2
 800ac0a:	e7f4      	b.n	800abf6 <_printf_float+0x19e>
 800ac0c:	2301      	movs	r3, #1
 800ac0e:	e7f2      	b.n	800abf6 <_printf_float+0x19e>
 800ac10:	f04f 0a67 	mov.w	sl, #103	; 0x67
 800ac14:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800ac16:	4299      	cmp	r1, r3
 800ac18:	db05      	blt.n	800ac26 <_printf_float+0x1ce>
 800ac1a:	6823      	ldr	r3, [r4, #0]
 800ac1c:	6121      	str	r1, [r4, #16]
 800ac1e:	07d8      	lsls	r0, r3, #31
 800ac20:	d5ea      	bpl.n	800abf8 <_printf_float+0x1a0>
 800ac22:	1c4b      	adds	r3, r1, #1
 800ac24:	e7e7      	b.n	800abf6 <_printf_float+0x19e>
 800ac26:	2900      	cmp	r1, #0
 800ac28:	bfd4      	ite	le
 800ac2a:	f1c1 0202 	rsble	r2, r1, #2
 800ac2e:	2201      	movgt	r2, #1
 800ac30:	4413      	add	r3, r2
 800ac32:	e7e0      	b.n	800abf6 <_printf_float+0x19e>
 800ac34:	6823      	ldr	r3, [r4, #0]
 800ac36:	055a      	lsls	r2, r3, #21
 800ac38:	d407      	bmi.n	800ac4a <_printf_float+0x1f2>
 800ac3a:	6923      	ldr	r3, [r4, #16]
 800ac3c:	4642      	mov	r2, r8
 800ac3e:	4631      	mov	r1, r6
 800ac40:	4628      	mov	r0, r5
 800ac42:	47b8      	blx	r7
 800ac44:	3001      	adds	r0, #1
 800ac46:	d12b      	bne.n	800aca0 <_printf_float+0x248>
 800ac48:	e764      	b.n	800ab14 <_printf_float+0xbc>
 800ac4a:	f1ba 0f65 	cmp.w	sl, #101	; 0x65
 800ac4e:	f240 80dc 	bls.w	800ae0a <_printf_float+0x3b2>
 800ac52:	2200      	movs	r2, #0
 800ac54:	2300      	movs	r3, #0
 800ac56:	e9d4 0112 	ldrd	r0, r1, [r4, #72]	; 0x48
 800ac5a:	f7f5 fee5 	bl	8000a28 <__aeabi_dcmpeq>
 800ac5e:	2800      	cmp	r0, #0
 800ac60:	d033      	beq.n	800acca <_printf_float+0x272>
 800ac62:	2301      	movs	r3, #1
 800ac64:	4a41      	ldr	r2, [pc, #260]	; (800ad6c <_printf_float+0x314>)
 800ac66:	4631      	mov	r1, r6
 800ac68:	4628      	mov	r0, r5
 800ac6a:	47b8      	blx	r7
 800ac6c:	3001      	adds	r0, #1
 800ac6e:	f43f af51 	beq.w	800ab14 <_printf_float+0xbc>
 800ac72:	e9dd 2309 	ldrd	r2, r3, [sp, #36]	; 0x24
 800ac76:	429a      	cmp	r2, r3
 800ac78:	db02      	blt.n	800ac80 <_printf_float+0x228>
 800ac7a:	6823      	ldr	r3, [r4, #0]
 800ac7c:	07d8      	lsls	r0, r3, #31
 800ac7e:	d50f      	bpl.n	800aca0 <_printf_float+0x248>
 800ac80:	e9dd 2306 	ldrd	r2, r3, [sp, #24]
 800ac84:	4631      	mov	r1, r6
 800ac86:	4628      	mov	r0, r5
 800ac88:	47b8      	blx	r7
 800ac8a:	3001      	adds	r0, #1
 800ac8c:	f43f af42 	beq.w	800ab14 <_printf_float+0xbc>
 800ac90:	f04f 0800 	mov.w	r8, #0
 800ac94:	f104 091a 	add.w	r9, r4, #26
 800ac98:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800ac9a:	3b01      	subs	r3, #1
 800ac9c:	4543      	cmp	r3, r8
 800ac9e:	dc09      	bgt.n	800acb4 <_printf_float+0x25c>
 800aca0:	6823      	ldr	r3, [r4, #0]
 800aca2:	079b      	lsls	r3, r3, #30
 800aca4:	f100 8101 	bmi.w	800aeaa <_printf_float+0x452>
 800aca8:	68e0      	ldr	r0, [r4, #12]
 800acaa:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
 800acac:	4298      	cmp	r0, r3
 800acae:	bfb8      	it	lt
 800acb0:	4618      	movlt	r0, r3
 800acb2:	e731      	b.n	800ab18 <_printf_float+0xc0>
 800acb4:	2301      	movs	r3, #1
 800acb6:	464a      	mov	r2, r9
 800acb8:	4631      	mov	r1, r6
 800acba:	4628      	mov	r0, r5
 800acbc:	47b8      	blx	r7
 800acbe:	3001      	adds	r0, #1
 800acc0:	f43f af28 	beq.w	800ab14 <_printf_float+0xbc>
 800acc4:	f108 0801 	add.w	r8, r8, #1
 800acc8:	e7e6      	b.n	800ac98 <_printf_float+0x240>
 800acca:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800accc:	2b00      	cmp	r3, #0
 800acce:	dc2b      	bgt.n	800ad28 <_printf_float+0x2d0>
 800acd0:	2301      	movs	r3, #1
 800acd2:	4a26      	ldr	r2, [pc, #152]	; (800ad6c <_printf_float+0x314>)
 800acd4:	4631      	mov	r1, r6
 800acd6:	4628      	mov	r0, r5
 800acd8:	47b8      	blx	r7
 800acda:	3001      	adds	r0, #1
 800acdc:	f43f af1a 	beq.w	800ab14 <_printf_float+0xbc>
 800ace0:	e9dd 2309 	ldrd	r2, r3, [sp, #36]	; 0x24
 800ace4:	4313      	orrs	r3, r2
 800ace6:	d102      	bne.n	800acee <_printf_float+0x296>
 800ace8:	6823      	ldr	r3, [r4, #0]
 800acea:	07d9      	lsls	r1, r3, #31
 800acec:	d5d8      	bpl.n	800aca0 <_printf_float+0x248>
 800acee:	e9dd 2306 	ldrd	r2, r3, [sp, #24]
 800acf2:	4631      	mov	r1, r6
 800acf4:	4628      	mov	r0, r5
 800acf6:	47b8      	blx	r7
 800acf8:	3001      	adds	r0, #1
 800acfa:	f43f af0b 	beq.w	800ab14 <_printf_float+0xbc>
 800acfe:	f04f 0900 	mov.w	r9, #0
 800ad02:	f104 0a1a 	add.w	sl, r4, #26
 800ad06:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800ad08:	425b      	negs	r3, r3
 800ad0a:	454b      	cmp	r3, r9
 800ad0c:	dc01      	bgt.n	800ad12 <_printf_float+0x2ba>
 800ad0e:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800ad10:	e794      	b.n	800ac3c <_printf_float+0x1e4>
 800ad12:	2301      	movs	r3, #1
 800ad14:	4652      	mov	r2, sl
 800ad16:	4631      	mov	r1, r6
 800ad18:	4628      	mov	r0, r5
 800ad1a:	47b8      	blx	r7
 800ad1c:	3001      	adds	r0, #1
 800ad1e:	f43f aef9 	beq.w	800ab14 <_printf_float+0xbc>
 800ad22:	f109 0901 	add.w	r9, r9, #1
 800ad26:	e7ee      	b.n	800ad06 <_printf_float+0x2ae>
 800ad28:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 800ad2a:	6da3      	ldr	r3, [r4, #88]	; 0x58
 800ad2c:	429a      	cmp	r2, r3
 800ad2e:	bfa8      	it	ge
 800ad30:	461a      	movge	r2, r3
 800ad32:	2a00      	cmp	r2, #0
 800ad34:	4691      	mov	r9, r2
 800ad36:	dd07      	ble.n	800ad48 <_printf_float+0x2f0>
 800ad38:	4613      	mov	r3, r2
 800ad3a:	4631      	mov	r1, r6
 800ad3c:	4642      	mov	r2, r8
 800ad3e:	4628      	mov	r0, r5
 800ad40:	47b8      	blx	r7
 800ad42:	3001      	adds	r0, #1
 800ad44:	f43f aee6 	beq.w	800ab14 <_printf_float+0xbc>
 800ad48:	f104 031a 	add.w	r3, r4, #26
 800ad4c:	f04f 0b00 	mov.w	fp, #0
 800ad50:	ea29 79e9 	bic.w	r9, r9, r9, asr #31
 800ad54:	9304      	str	r3, [sp, #16]
 800ad56:	e015      	b.n	800ad84 <_printf_float+0x32c>
 800ad58:	7fefffff 	.word	0x7fefffff
 800ad5c:	0800d36c 	.word	0x0800d36c
 800ad60:	0800d370 	.word	0x0800d370
 800ad64:	0800d374 	.word	0x0800d374
 800ad68:	0800d378 	.word	0x0800d378
 800ad6c:	0800d37c 	.word	0x0800d37c
 800ad70:	2301      	movs	r3, #1
 800ad72:	9a04      	ldr	r2, [sp, #16]
 800ad74:	4631      	mov	r1, r6
 800ad76:	4628      	mov	r0, r5
 800ad78:	47b8      	blx	r7
 800ad7a:	3001      	adds	r0, #1
 800ad7c:	f43f aeca 	beq.w	800ab14 <_printf_float+0xbc>
 800ad80:	f10b 0b01 	add.w	fp, fp, #1
 800ad84:	f8d4 a058 	ldr.w	sl, [r4, #88]	; 0x58
 800ad88:	ebaa 0309 	sub.w	r3, sl, r9
 800ad8c:	455b      	cmp	r3, fp
 800ad8e:	dcef      	bgt.n	800ad70 <_printf_float+0x318>
 800ad90:	e9dd 2309 	ldrd	r2, r3, [sp, #36]	; 0x24
 800ad94:	429a      	cmp	r2, r3
 800ad96:	db1b      	blt.n	800add0 <_printf_float+0x378>
 800ad98:	6823      	ldr	r3, [r4, #0]
 800ad9a:	07da      	lsls	r2, r3, #31
 800ad9c:	d418      	bmi.n	800add0 <_printf_float+0x378>
 800ad9e:	e9dd 2309 	ldrd	r2, r3, [sp, #36]	; 0x24
 800ada2:	4592      	cmp	sl, r2
 800ada4:	db1c      	blt.n	800ade0 <_printf_float+0x388>
 800ada6:	eba3 090a 	sub.w	r9, r3, sl
 800adaa:	f1b9 0f00 	cmp.w	r9, #0
 800adae:	dd08      	ble.n	800adc2 <_printf_float+0x36a>
 800adb0:	464b      	mov	r3, r9
 800adb2:	eb08 020a 	add.w	r2, r8, sl
 800adb6:	4631      	mov	r1, r6
 800adb8:	4628      	mov	r0, r5
 800adba:	47b8      	blx	r7
 800adbc:	3001      	adds	r0, #1
 800adbe:	f43f aea9 	beq.w	800ab14 <_printf_float+0xbc>
 800adc2:	f04f 0800 	mov.w	r8, #0
 800adc6:	ea29 79e9 	bic.w	r9, r9, r9, asr #31
 800adca:	f104 0a1a 	add.w	sl, r4, #26
 800adce:	e014      	b.n	800adfa <_printf_float+0x3a2>
 800add0:	e9dd 2306 	ldrd	r2, r3, [sp, #24]
 800add4:	4631      	mov	r1, r6
 800add6:	4628      	mov	r0, r5
 800add8:	47b8      	blx	r7
 800adda:	3001      	adds	r0, #1
 800addc:	d1df      	bne.n	800ad9e <_printf_float+0x346>
 800adde:	e699      	b.n	800ab14 <_printf_float+0xbc>
 800ade0:	eba3 0902 	sub.w	r9, r3, r2
 800ade4:	e7e1      	b.n	800adaa <_printf_float+0x352>
 800ade6:	2301      	movs	r3, #1
 800ade8:	4652      	mov	r2, sl
 800adea:	4631      	mov	r1, r6
 800adec:	4628      	mov	r0, r5
 800adee:	47b8      	blx	r7
 800adf0:	3001      	adds	r0, #1
 800adf2:	f43f ae8f 	beq.w	800ab14 <_printf_float+0xbc>
 800adf6:	f108 0801 	add.w	r8, r8, #1
 800adfa:	e9dd 2309 	ldrd	r2, r3, [sp, #36]	; 0x24
 800adfe:	1a9b      	subs	r3, r3, r2
 800ae00:	eba3 0309 	sub.w	r3, r3, r9
 800ae04:	4543      	cmp	r3, r8
 800ae06:	dcee      	bgt.n	800ade6 <_printf_float+0x38e>
 800ae08:	e74a      	b.n	800aca0 <_printf_float+0x248>
 800ae0a:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 800ae0c:	2a01      	cmp	r2, #1
 800ae0e:	dc01      	bgt.n	800ae14 <_printf_float+0x3bc>
 800ae10:	07db      	lsls	r3, r3, #31
 800ae12:	d537      	bpl.n	800ae84 <_printf_float+0x42c>
 800ae14:	2301      	movs	r3, #1
 800ae16:	4642      	mov	r2, r8
 800ae18:	4631      	mov	r1, r6
 800ae1a:	4628      	mov	r0, r5
 800ae1c:	47b8      	blx	r7
 800ae1e:	3001      	adds	r0, #1
 800ae20:	f43f ae78 	beq.w	800ab14 <_printf_float+0xbc>
 800ae24:	e9dd 2306 	ldrd	r2, r3, [sp, #24]
 800ae28:	4631      	mov	r1, r6
 800ae2a:	4628      	mov	r0, r5
 800ae2c:	47b8      	blx	r7
 800ae2e:	3001      	adds	r0, #1
 800ae30:	f43f ae70 	beq.w	800ab14 <_printf_float+0xbc>
 800ae34:	2200      	movs	r2, #0
 800ae36:	2300      	movs	r3, #0
 800ae38:	e9d4 0112 	ldrd	r0, r1, [r4, #72]	; 0x48
 800ae3c:	f7f5 fdf4 	bl	8000a28 <__aeabi_dcmpeq>
 800ae40:	b9d8      	cbnz	r0, 800ae7a <_printf_float+0x422>
 800ae42:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800ae44:	f108 0201 	add.w	r2, r8, #1
 800ae48:	3b01      	subs	r3, #1
 800ae4a:	4631      	mov	r1, r6
 800ae4c:	4628      	mov	r0, r5
 800ae4e:	47b8      	blx	r7
 800ae50:	3001      	adds	r0, #1
 800ae52:	d10e      	bne.n	800ae72 <_printf_float+0x41a>
 800ae54:	e65e      	b.n	800ab14 <_printf_float+0xbc>
 800ae56:	2301      	movs	r3, #1
 800ae58:	4652      	mov	r2, sl
 800ae5a:	4631      	mov	r1, r6
 800ae5c:	4628      	mov	r0, r5
 800ae5e:	47b8      	blx	r7
 800ae60:	3001      	adds	r0, #1
 800ae62:	f43f ae57 	beq.w	800ab14 <_printf_float+0xbc>
 800ae66:	f108 0801 	add.w	r8, r8, #1
 800ae6a:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800ae6c:	3b01      	subs	r3, #1
 800ae6e:	4543      	cmp	r3, r8
 800ae70:	dcf1      	bgt.n	800ae56 <_printf_float+0x3fe>
 800ae72:	464b      	mov	r3, r9
 800ae74:	f104 0250 	add.w	r2, r4, #80	; 0x50
 800ae78:	e6e1      	b.n	800ac3e <_printf_float+0x1e6>
 800ae7a:	f04f 0800 	mov.w	r8, #0
 800ae7e:	f104 0a1a 	add.w	sl, r4, #26
 800ae82:	e7f2      	b.n	800ae6a <_printf_float+0x412>
 800ae84:	2301      	movs	r3, #1
 800ae86:	4642      	mov	r2, r8
 800ae88:	e7df      	b.n	800ae4a <_printf_float+0x3f2>
 800ae8a:	2301      	movs	r3, #1
 800ae8c:	464a      	mov	r2, r9
 800ae8e:	4631      	mov	r1, r6
 800ae90:	4628      	mov	r0, r5
 800ae92:	47b8      	blx	r7
 800ae94:	3001      	adds	r0, #1
 800ae96:	f43f ae3d 	beq.w	800ab14 <_printf_float+0xbc>
 800ae9a:	f108 0801 	add.w	r8, r8, #1
 800ae9e:	68e3      	ldr	r3, [r4, #12]
 800aea0:	9a0b      	ldr	r2, [sp, #44]	; 0x2c
 800aea2:	1a9b      	subs	r3, r3, r2
 800aea4:	4543      	cmp	r3, r8
 800aea6:	dcf0      	bgt.n	800ae8a <_printf_float+0x432>
 800aea8:	e6fe      	b.n	800aca8 <_printf_float+0x250>
 800aeaa:	f04f 0800 	mov.w	r8, #0
 800aeae:	f104 0919 	add.w	r9, r4, #25
 800aeb2:	e7f4      	b.n	800ae9e <_printf_float+0x446>

0800aeb4 <_printf_common>:
 800aeb4:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 800aeb8:	4691      	mov	r9, r2
 800aeba:	461f      	mov	r7, r3
 800aebc:	688a      	ldr	r2, [r1, #8]
 800aebe:	690b      	ldr	r3, [r1, #16]
 800aec0:	f8dd 8020 	ldr.w	r8, [sp, #32]
 800aec4:	4293      	cmp	r3, r2
 800aec6:	bfb8      	it	lt
 800aec8:	4613      	movlt	r3, r2
 800aeca:	f8c9 3000 	str.w	r3, [r9]
 800aece:	f891 2043 	ldrb.w	r2, [r1, #67]	; 0x43
 800aed2:	4606      	mov	r6, r0
 800aed4:	460c      	mov	r4, r1
 800aed6:	b112      	cbz	r2, 800aede <_printf_common+0x2a>
 800aed8:	3301      	adds	r3, #1
 800aeda:	f8c9 3000 	str.w	r3, [r9]
 800aede:	6823      	ldr	r3, [r4, #0]
 800aee0:	0699      	lsls	r1, r3, #26
 800aee2:	bf42      	ittt	mi
 800aee4:	f8d9 3000 	ldrmi.w	r3, [r9]
 800aee8:	3302      	addmi	r3, #2
 800aeea:	f8c9 3000 	strmi.w	r3, [r9]
 800aeee:	6825      	ldr	r5, [r4, #0]
 800aef0:	f015 0506 	ands.w	r5, r5, #6
 800aef4:	d107      	bne.n	800af06 <_printf_common+0x52>
 800aef6:	f104 0a19 	add.w	sl, r4, #25
 800aefa:	68e3      	ldr	r3, [r4, #12]
 800aefc:	f8d9 2000 	ldr.w	r2, [r9]
 800af00:	1a9b      	subs	r3, r3, r2
 800af02:	42ab      	cmp	r3, r5
 800af04:	dc28      	bgt.n	800af58 <_printf_common+0xa4>
 800af06:	f894 3043 	ldrb.w	r3, [r4, #67]	; 0x43
 800af0a:	6822      	ldr	r2, [r4, #0]
 800af0c:	3300      	adds	r3, #0
 800af0e:	bf18      	it	ne
 800af10:	2301      	movne	r3, #1
 800af12:	0692      	lsls	r2, r2, #26
 800af14:	d42d      	bmi.n	800af72 <_printf_common+0xbe>
 800af16:	f104 0243 	add.w	r2, r4, #67	; 0x43
 800af1a:	4639      	mov	r1, r7
 800af1c:	4630      	mov	r0, r6
 800af1e:	47c0      	blx	r8
 800af20:	3001      	adds	r0, #1
 800af22:	d020      	beq.n	800af66 <_printf_common+0xb2>
 800af24:	6823      	ldr	r3, [r4, #0]
 800af26:	68e5      	ldr	r5, [r4, #12]
 800af28:	f8d9 2000 	ldr.w	r2, [r9]
 800af2c:	f003 0306 	and.w	r3, r3, #6
 800af30:	2b04      	cmp	r3, #4
 800af32:	bf08      	it	eq
 800af34:	1aad      	subeq	r5, r5, r2
 800af36:	68a3      	ldr	r3, [r4, #8]
 800af38:	6922      	ldr	r2, [r4, #16]
 800af3a:	bf0c      	ite	eq
 800af3c:	ea25 75e5 	biceq.w	r5, r5, r5, asr #31
 800af40:	2500      	movne	r5, #0
 800af42:	4293      	cmp	r3, r2
 800af44:	bfc4      	itt	gt
 800af46:	1a9b      	subgt	r3, r3, r2
 800af48:	18ed      	addgt	r5, r5, r3
 800af4a:	f04f 0900 	mov.w	r9, #0
 800af4e:	341a      	adds	r4, #26
 800af50:	454d      	cmp	r5, r9
 800af52:	d11a      	bne.n	800af8a <_printf_common+0xd6>
 800af54:	2000      	movs	r0, #0
 800af56:	e008      	b.n	800af6a <_printf_common+0xb6>
 800af58:	2301      	movs	r3, #1
 800af5a:	4652      	mov	r2, sl
 800af5c:	4639      	mov	r1, r7
 800af5e:	4630      	mov	r0, r6
 800af60:	47c0      	blx	r8
 800af62:	3001      	adds	r0, #1
 800af64:	d103      	bne.n	800af6e <_printf_common+0xba>
 800af66:	f04f 30ff 	mov.w	r0, #4294967295
 800af6a:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 800af6e:	3501      	adds	r5, #1
 800af70:	e7c3      	b.n	800aefa <_printf_common+0x46>
 800af72:	18e1      	adds	r1, r4, r3
 800af74:	1c5a      	adds	r2, r3, #1
 800af76:	2030      	movs	r0, #48	; 0x30
 800af78:	f881 0043 	strb.w	r0, [r1, #67]	; 0x43
 800af7c:	4422      	add	r2, r4
 800af7e:	f894 1045 	ldrb.w	r1, [r4, #69]	; 0x45
 800af82:	f882 1043 	strb.w	r1, [r2, #67]	; 0x43
 800af86:	3302      	adds	r3, #2
 800af88:	e7c5      	b.n	800af16 <_printf_common+0x62>
 800af8a:	2301      	movs	r3, #1
 800af8c:	4622      	mov	r2, r4
 800af8e:	4639      	mov	r1, r7
 800af90:	4630      	mov	r0, r6
 800af92:	47c0      	blx	r8
 800af94:	3001      	adds	r0, #1
 800af96:	d0e6      	beq.n	800af66 <_printf_common+0xb2>
 800af98:	f109 0901 	add.w	r9, r9, #1
 800af9c:	e7d8      	b.n	800af50 <_printf_common+0x9c>

0800af9e <cleanup_glue>:
 800af9e:	b538      	push	{r3, r4, r5, lr}
 800afa0:	460c      	mov	r4, r1
 800afa2:	6809      	ldr	r1, [r1, #0]
 800afa4:	4605      	mov	r5, r0
 800afa6:	b109      	cbz	r1, 800afac <cleanup_glue+0xe>
 800afa8:	f7ff fff9 	bl	800af9e <cleanup_glue>
 800afac:	4621      	mov	r1, r4
 800afae:	4628      	mov	r0, r5
 800afb0:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
 800afb4:	f7f6 b8ff 	b.w	80011b6 <_free_r>

0800afb8 <_reclaim_reent>:
 800afb8:	4b2c      	ldr	r3, [pc, #176]	; (800b06c <_reclaim_reent+0xb4>)
 800afba:	681b      	ldr	r3, [r3, #0]
 800afbc:	4283      	cmp	r3, r0
 800afbe:	b570      	push	{r4, r5, r6, lr}
 800afc0:	4604      	mov	r4, r0
 800afc2:	d051      	beq.n	800b068 <_reclaim_reent+0xb0>
 800afc4:	6a43      	ldr	r3, [r0, #36]	; 0x24
 800afc6:	b143      	cbz	r3, 800afda <_reclaim_reent+0x22>
 800afc8:	68db      	ldr	r3, [r3, #12]
 800afca:	2b00      	cmp	r3, #0
 800afcc:	d14a      	bne.n	800b064 <_reclaim_reent+0xac>
 800afce:	6a63      	ldr	r3, [r4, #36]	; 0x24
 800afd0:	6819      	ldr	r1, [r3, #0]
 800afd2:	b111      	cbz	r1, 800afda <_reclaim_reent+0x22>
 800afd4:	4620      	mov	r0, r4
 800afd6:	f7f6 f8ee 	bl	80011b6 <_free_r>
 800afda:	6961      	ldr	r1, [r4, #20]
 800afdc:	b111      	cbz	r1, 800afe4 <_reclaim_reent+0x2c>
 800afde:	4620      	mov	r0, r4
 800afe0:	f7f6 f8e9 	bl	80011b6 <_free_r>
 800afe4:	6a61      	ldr	r1, [r4, #36]	; 0x24
 800afe6:	b111      	cbz	r1, 800afee <_reclaim_reent+0x36>
 800afe8:	4620      	mov	r0, r4
 800afea:	f7f6 f8e4 	bl	80011b6 <_free_r>
 800afee:	6ba1      	ldr	r1, [r4, #56]	; 0x38
 800aff0:	b111      	cbz	r1, 800aff8 <_reclaim_reent+0x40>
 800aff2:	4620      	mov	r0, r4
 800aff4:	f7f6 f8df 	bl	80011b6 <_free_r>
 800aff8:	6be1      	ldr	r1, [r4, #60]	; 0x3c
 800affa:	b111      	cbz	r1, 800b002 <_reclaim_reent+0x4a>
 800affc:	4620      	mov	r0, r4
 800affe:	f7f6 f8da 	bl	80011b6 <_free_r>
 800b002:	6c21      	ldr	r1, [r4, #64]	; 0x40
 800b004:	b111      	cbz	r1, 800b00c <_reclaim_reent+0x54>
 800b006:	4620      	mov	r0, r4
 800b008:	f7f6 f8d5 	bl	80011b6 <_free_r>
 800b00c:	6de1      	ldr	r1, [r4, #92]	; 0x5c
 800b00e:	b111      	cbz	r1, 800b016 <_reclaim_reent+0x5e>
 800b010:	4620      	mov	r0, r4
 800b012:	f7f6 f8d0 	bl	80011b6 <_free_r>
 800b016:	6da1      	ldr	r1, [r4, #88]	; 0x58
 800b018:	b111      	cbz	r1, 800b020 <_reclaim_reent+0x68>
 800b01a:	4620      	mov	r0, r4
 800b01c:	f7f6 f8cb 	bl	80011b6 <_free_r>
 800b020:	6b61      	ldr	r1, [r4, #52]	; 0x34
 800b022:	b111      	cbz	r1, 800b02a <_reclaim_reent+0x72>
 800b024:	4620      	mov	r0, r4
 800b026:	f7f6 f8c6 	bl	80011b6 <_free_r>
 800b02a:	69a3      	ldr	r3, [r4, #24]
 800b02c:	b1e3      	cbz	r3, 800b068 <_reclaim_reent+0xb0>
 800b02e:	6aa3      	ldr	r3, [r4, #40]	; 0x28
 800b030:	4620      	mov	r0, r4
 800b032:	4798      	blx	r3
 800b034:	6ca1      	ldr	r1, [r4, #72]	; 0x48
 800b036:	b1b9      	cbz	r1, 800b068 <_reclaim_reent+0xb0>
 800b038:	4620      	mov	r0, r4
 800b03a:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
 800b03e:	f7ff bfae 	b.w	800af9e <cleanup_glue>
 800b042:	5949      	ldr	r1, [r1, r5]
 800b044:	b941      	cbnz	r1, 800b058 <_reclaim_reent+0xa0>
 800b046:	3504      	adds	r5, #4
 800b048:	6a63      	ldr	r3, [r4, #36]	; 0x24
 800b04a:	2d80      	cmp	r5, #128	; 0x80
 800b04c:	68d9      	ldr	r1, [r3, #12]
 800b04e:	d1f8      	bne.n	800b042 <_reclaim_reent+0x8a>
 800b050:	4620      	mov	r0, r4
 800b052:	f7f6 f8b0 	bl	80011b6 <_free_r>
 800b056:	e7ba      	b.n	800afce <_reclaim_reent+0x16>
 800b058:	680e      	ldr	r6, [r1, #0]
 800b05a:	4620      	mov	r0, r4
 800b05c:	f7f6 f8ab 	bl	80011b6 <_free_r>
 800b060:	4631      	mov	r1, r6
 800b062:	e7ef      	b.n	800b044 <_reclaim_reent+0x8c>
 800b064:	2500      	movs	r5, #0
 800b066:	e7ef      	b.n	800b048 <_reclaim_reent+0x90>
 800b068:	bd70      	pop	{r4, r5, r6, pc}
 800b06a:	bf00      	nop
 800b06c:	20000080 	.word	0x20000080

0800b070 <quorem>:
 800b070:	e92d 4ff7 	stmdb	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800b074:	6903      	ldr	r3, [r0, #16]
 800b076:	690c      	ldr	r4, [r1, #16]
 800b078:	42a3      	cmp	r3, r4
 800b07a:	4680      	mov	r8, r0
 800b07c:	db7f      	blt.n	800b17e <quorem+0x10e>
 800b07e:	3c01      	subs	r4, #1
 800b080:	f101 0714 	add.w	r7, r1, #20
 800b084:	00a0      	lsls	r0, r4, #2
 800b086:	f108 0514 	add.w	r5, r8, #20
 800b08a:	182b      	adds	r3, r5, r0
 800b08c:	9301      	str	r3, [sp, #4]
 800b08e:	f857 3024 	ldr.w	r3, [r7, r4, lsl #2]
 800b092:	f855 2024 	ldr.w	r2, [r5, r4, lsl #2]
 800b096:	3301      	adds	r3, #1
 800b098:	429a      	cmp	r2, r3
 800b09a:	eb07 0900 	add.w	r9, r7, r0
 800b09e:	fbb2 f6f3 	udiv	r6, r2, r3
 800b0a2:	d331      	bcc.n	800b108 <quorem+0x98>
 800b0a4:	f04f 0a00 	mov.w	sl, #0
 800b0a8:	46bc      	mov	ip, r7
 800b0aa:	46ae      	mov	lr, r5
 800b0ac:	46d3      	mov	fp, sl
 800b0ae:	f85c 2b04 	ldr.w	r2, [ip], #4
 800b0b2:	b293      	uxth	r3, r2
 800b0b4:	fb06 a303 	mla	r3, r6, r3, sl
 800b0b8:	ea4f 4a13 	mov.w	sl, r3, lsr #16
 800b0bc:	b29b      	uxth	r3, r3
 800b0be:	ebab 0303 	sub.w	r3, fp, r3
 800b0c2:	0c12      	lsrs	r2, r2, #16
 800b0c4:	f8de b000 	ldr.w	fp, [lr]
 800b0c8:	fb06 a202 	mla	r2, r6, r2, sl
 800b0cc:	fa13 f38b 	uxtah	r3, r3, fp
 800b0d0:	ea4f 4a12 	mov.w	sl, r2, lsr #16
 800b0d4:	fa1f fb82 	uxth.w	fp, r2
 800b0d8:	f8de 2000 	ldr.w	r2, [lr]
 800b0dc:	ebcb 4212 	rsb	r2, fp, r2, lsr #16
 800b0e0:	eb02 4223 	add.w	r2, r2, r3, asr #16
 800b0e4:	b29b      	uxth	r3, r3
 800b0e6:	ea43 4302 	orr.w	r3, r3, r2, lsl #16
 800b0ea:	45e1      	cmp	r9, ip
 800b0ec:	ea4f 4b22 	mov.w	fp, r2, asr #16
 800b0f0:	f84e 3b04 	str.w	r3, [lr], #4
 800b0f4:	d2db      	bcs.n	800b0ae <quorem+0x3e>
 800b0f6:	582b      	ldr	r3, [r5, r0]
 800b0f8:	b933      	cbnz	r3, 800b108 <quorem+0x98>
 800b0fa:	9b01      	ldr	r3, [sp, #4]
 800b0fc:	3b04      	subs	r3, #4
 800b0fe:	429d      	cmp	r5, r3
 800b100:	461a      	mov	r2, r3
 800b102:	d330      	bcc.n	800b166 <quorem+0xf6>
 800b104:	f8c8 4010 	str.w	r4, [r8, #16]
 800b108:	4640      	mov	r0, r8
 800b10a:	f001 f80c 	bl	800c126 <__mcmp>
 800b10e:	2800      	cmp	r0, #0
 800b110:	db25      	blt.n	800b15e <quorem+0xee>
 800b112:	3601      	adds	r6, #1
 800b114:	4628      	mov	r0, r5
 800b116:	f04f 0c00 	mov.w	ip, #0
 800b11a:	f857 2b04 	ldr.w	r2, [r7], #4
 800b11e:	f8d0 e000 	ldr.w	lr, [r0]
 800b122:	b293      	uxth	r3, r2
 800b124:	ebac 0303 	sub.w	r3, ip, r3
 800b128:	0c12      	lsrs	r2, r2, #16
 800b12a:	fa13 f38e 	uxtah	r3, r3, lr
 800b12e:	ebc2 421e 	rsb	r2, r2, lr, lsr #16
 800b132:	eb02 4223 	add.w	r2, r2, r3, asr #16
 800b136:	b29b      	uxth	r3, r3
 800b138:	ea43 4302 	orr.w	r3, r3, r2, lsl #16
 800b13c:	45b9      	cmp	r9, r7
 800b13e:	ea4f 4c22 	mov.w	ip, r2, asr #16
 800b142:	f840 3b04 	str.w	r3, [r0], #4
 800b146:	d2e8      	bcs.n	800b11a <quorem+0xaa>
 800b148:	f855 2024 	ldr.w	r2, [r5, r4, lsl #2]
 800b14c:	eb05 0384 	add.w	r3, r5, r4, lsl #2
 800b150:	b92a      	cbnz	r2, 800b15e <quorem+0xee>
 800b152:	3b04      	subs	r3, #4
 800b154:	429d      	cmp	r5, r3
 800b156:	461a      	mov	r2, r3
 800b158:	d30b      	bcc.n	800b172 <quorem+0x102>
 800b15a:	f8c8 4010 	str.w	r4, [r8, #16]
 800b15e:	4630      	mov	r0, r6
 800b160:	b003      	add	sp, #12
 800b162:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800b166:	6812      	ldr	r2, [r2, #0]
 800b168:	3b04      	subs	r3, #4
 800b16a:	2a00      	cmp	r2, #0
 800b16c:	d1ca      	bne.n	800b104 <quorem+0x94>
 800b16e:	3c01      	subs	r4, #1
 800b170:	e7c5      	b.n	800b0fe <quorem+0x8e>
 800b172:	6812      	ldr	r2, [r2, #0]
 800b174:	3b04      	subs	r3, #4
 800b176:	2a00      	cmp	r2, #0
 800b178:	d1ef      	bne.n	800b15a <quorem+0xea>
 800b17a:	3c01      	subs	r4, #1
 800b17c:	e7ea      	b.n	800b154 <quorem+0xe4>
 800b17e:	2000      	movs	r0, #0
 800b180:	e7ee      	b.n	800b160 <quorem+0xf0>
 800b182:	0000      	movs	r0, r0
 800b184:	0000      	movs	r0, r0
	...

0800b188 <_dtoa_r>:
 800b188:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800b18c:	ec55 4b10 	vmov	r4, r5, d0
 800b190:	b099      	sub	sp, #100	; 0x64
 800b192:	6a47      	ldr	r7, [r0, #36]	; 0x24
 800b194:	9108      	str	r1, [sp, #32]
 800b196:	4683      	mov	fp, r0
 800b198:	920d      	str	r2, [sp, #52]	; 0x34
 800b19a:	9314      	str	r3, [sp, #80]	; 0x50
 800b19c:	9e22      	ldr	r6, [sp, #136]	; 0x88
 800b19e:	e9cd 4500 	strd	r4, r5, [sp]
 800b1a2:	b947      	cbnz	r7, 800b1b6 <_dtoa_r+0x2e>
 800b1a4:	2010      	movs	r0, #16
 800b1a6:	f7ff fba9 	bl	800a8fc <malloc>
 800b1aa:	e9c0 7701 	strd	r7, r7, [r0, #4]
 800b1ae:	f8cb 0024 	str.w	r0, [fp, #36]	; 0x24
 800b1b2:	6007      	str	r7, [r0, #0]
 800b1b4:	60c7      	str	r7, [r0, #12]
 800b1b6:	f8db 3024 	ldr.w	r3, [fp, #36]	; 0x24
 800b1ba:	6819      	ldr	r1, [r3, #0]
 800b1bc:	b159      	cbz	r1, 800b1d6 <_dtoa_r+0x4e>
 800b1be:	685a      	ldr	r2, [r3, #4]
 800b1c0:	604a      	str	r2, [r1, #4]
 800b1c2:	2301      	movs	r3, #1
 800b1c4:	4093      	lsls	r3, r2
 800b1c6:	608b      	str	r3, [r1, #8]
 800b1c8:	4658      	mov	r0, fp
 800b1ca:	f000 fdcf 	bl	800bd6c <_Bfree>
 800b1ce:	f8db 3024 	ldr.w	r3, [fp, #36]	; 0x24
 800b1d2:	2200      	movs	r2, #0
 800b1d4:	601a      	str	r2, [r3, #0]
 800b1d6:	1e2b      	subs	r3, r5, #0
 800b1d8:	bfb9      	ittee	lt
 800b1da:	f023 4300 	biclt.w	r3, r3, #2147483648	; 0x80000000
 800b1de:	9301      	strlt	r3, [sp, #4]
 800b1e0:	2300      	movge	r3, #0
 800b1e2:	6033      	strge	r3, [r6, #0]
 800b1e4:	9c01      	ldr	r4, [sp, #4]
 800b1e6:	4bb2      	ldr	r3, [pc, #712]	; (800b4b0 <_dtoa_r+0x328>)
 800b1e8:	bfbc      	itt	lt
 800b1ea:	2201      	movlt	r2, #1
 800b1ec:	6032      	strlt	r2, [r6, #0]
 800b1ee:	43a3      	bics	r3, r4
 800b1f0:	d11a      	bne.n	800b228 <_dtoa_r+0xa0>
 800b1f2:	9a14      	ldr	r2, [sp, #80]	; 0x50
 800b1f4:	f242 730f 	movw	r3, #9999	; 0x270f
 800b1f8:	6013      	str	r3, [r2, #0]
 800b1fa:	9a00      	ldr	r2, [sp, #0]
 800b1fc:	f3c4 0313 	ubfx	r3, r4, #0, #20
 800b200:	4313      	orrs	r3, r2
 800b202:	f000 8552 	beq.w	800bcaa <_dtoa_r+0xb22>
 800b206:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800b208:	b953      	cbnz	r3, 800b220 <_dtoa_r+0x98>
 800b20a:	4baa      	ldr	r3, [pc, #680]	; (800b4b4 <_dtoa_r+0x32c>)
 800b20c:	e023      	b.n	800b256 <_dtoa_r+0xce>
 800b20e:	4baa      	ldr	r3, [pc, #680]	; (800b4b8 <_dtoa_r+0x330>)
 800b210:	9303      	str	r3, [sp, #12]
 800b212:	3308      	adds	r3, #8
 800b214:	9a23      	ldr	r2, [sp, #140]	; 0x8c
 800b216:	6013      	str	r3, [r2, #0]
 800b218:	9803      	ldr	r0, [sp, #12]
 800b21a:	b019      	add	sp, #100	; 0x64
 800b21c:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800b220:	4ba4      	ldr	r3, [pc, #656]	; (800b4b4 <_dtoa_r+0x32c>)
 800b222:	9303      	str	r3, [sp, #12]
 800b224:	3303      	adds	r3, #3
 800b226:	e7f5      	b.n	800b214 <_dtoa_r+0x8c>
 800b228:	ed9d 7b00 	vldr	d7, [sp]
 800b22c:	2200      	movs	r2, #0
 800b22e:	2300      	movs	r3, #0
 800b230:	ec51 0b17 	vmov	r0, r1, d7
 800b234:	ed8d 7b0e 	vstr	d7, [sp, #56]	; 0x38
 800b238:	f7f5 fbf6 	bl	8000a28 <__aeabi_dcmpeq>
 800b23c:	4607      	mov	r7, r0
 800b23e:	b160      	cbz	r0, 800b25a <_dtoa_r+0xd2>
 800b240:	9a14      	ldr	r2, [sp, #80]	; 0x50
 800b242:	2301      	movs	r3, #1
 800b244:	6013      	str	r3, [r2, #0]
 800b246:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800b248:	2b00      	cmp	r3, #0
 800b24a:	f000 852b 	beq.w	800bca4 <_dtoa_r+0xb1c>
 800b24e:	4b9b      	ldr	r3, [pc, #620]	; (800b4bc <_dtoa_r+0x334>)
 800b250:	9a23      	ldr	r2, [sp, #140]	; 0x8c
 800b252:	6013      	str	r3, [r2, #0]
 800b254:	3b01      	subs	r3, #1
 800b256:	9303      	str	r3, [sp, #12]
 800b258:	e7de      	b.n	800b218 <_dtoa_r+0x90>
 800b25a:	aa16      	add	r2, sp, #88	; 0x58
 800b25c:	a917      	add	r1, sp, #92	; 0x5c
 800b25e:	ed9d 0b0e 	vldr	d0, [sp, #56]	; 0x38
 800b262:	4658      	mov	r0, fp
 800b264:	f000 ffd6 	bl	800c214 <__d2b>
 800b268:	f3c4 560a 	ubfx	r6, r4, #20, #11
 800b26c:	4680      	mov	r8, r0
 800b26e:	2e00      	cmp	r6, #0
 800b270:	d07f      	beq.n	800b372 <_dtoa_r+0x1ea>
 800b272:	9b0f      	ldr	r3, [sp, #60]	; 0x3c
 800b274:	9715      	str	r7, [sp, #84]	; 0x54
 800b276:	f3c3 0313 	ubfx	r3, r3, #0, #20
 800b27a:	e9dd 450e 	ldrd	r4, r5, [sp, #56]	; 0x38
 800b27e:	f043 557f 	orr.w	r5, r3, #1069547520	; 0x3fc00000
 800b282:	f445 1540 	orr.w	r5, r5, #3145728	; 0x300000
 800b286:	f2a6 36ff 	subw	r6, r6, #1023	; 0x3ff
 800b28a:	2200      	movs	r2, #0
 800b28c:	4b8c      	ldr	r3, [pc, #560]	; (800b4c0 <_dtoa_r+0x338>)
 800b28e:	4620      	mov	r0, r4
 800b290:	4629      	mov	r1, r5
 800b292:	f7f4 ffa1 	bl	80001d8 <__aeabi_dsub>
 800b296:	a380      	add	r3, pc, #512	; (adr r3, 800b498 <_dtoa_r+0x310>)
 800b298:	e9d3 2300 	ldrd	r2, r3, [r3]
 800b29c:	f7f5 f954 	bl	8000548 <__aeabi_dmul>
 800b2a0:	a37f      	add	r3, pc, #508	; (adr r3, 800b4a0 <_dtoa_r+0x318>)
 800b2a2:	e9d3 2300 	ldrd	r2, r3, [r3]
 800b2a6:	f7f4 ff99 	bl	80001dc <__adddf3>
 800b2aa:	4604      	mov	r4, r0
 800b2ac:	4630      	mov	r0, r6
 800b2ae:	460d      	mov	r5, r1
 800b2b0:	f7f5 f8e0 	bl	8000474 <__aeabi_i2d>
 800b2b4:	a37c      	add	r3, pc, #496	; (adr r3, 800b4a8 <_dtoa_r+0x320>)
 800b2b6:	e9d3 2300 	ldrd	r2, r3, [r3]
 800b2ba:	f7f5 f945 	bl	8000548 <__aeabi_dmul>
 800b2be:	4602      	mov	r2, r0
 800b2c0:	460b      	mov	r3, r1
 800b2c2:	4620      	mov	r0, r4
 800b2c4:	4629      	mov	r1, r5
 800b2c6:	f7f4 ff89 	bl	80001dc <__adddf3>
 800b2ca:	4604      	mov	r4, r0
 800b2cc:	460d      	mov	r5, r1
 800b2ce:	f7f5 fbf3 	bl	8000ab8 <__aeabi_d2iz>
 800b2d2:	2200      	movs	r2, #0
 800b2d4:	4682      	mov	sl, r0
 800b2d6:	2300      	movs	r3, #0
 800b2d8:	4620      	mov	r0, r4
 800b2da:	4629      	mov	r1, r5
 800b2dc:	f7f5 fbae 	bl	8000a3c <__aeabi_dcmplt>
 800b2e0:	b148      	cbz	r0, 800b2f6 <_dtoa_r+0x16e>
 800b2e2:	4650      	mov	r0, sl
 800b2e4:	f7f5 f8c6 	bl	8000474 <__aeabi_i2d>
 800b2e8:	4622      	mov	r2, r4
 800b2ea:	462b      	mov	r3, r5
 800b2ec:	f7f5 fb9c 	bl	8000a28 <__aeabi_dcmpeq>
 800b2f0:	b908      	cbnz	r0, 800b2f6 <_dtoa_r+0x16e>
 800b2f2:	f10a 3aff 	add.w	sl, sl, #4294967295
 800b2f6:	f1ba 0f16 	cmp.w	sl, #22
 800b2fa:	d859      	bhi.n	800b3b0 <_dtoa_r+0x228>
 800b2fc:	4b71      	ldr	r3, [pc, #452]	; (800b4c4 <_dtoa_r+0x33c>)
 800b2fe:	eb03 03ca 	add.w	r3, r3, sl, lsl #3
 800b302:	e9d3 2300 	ldrd	r2, r3, [r3]
 800b306:	e9dd 010e 	ldrd	r0, r1, [sp, #56]	; 0x38
 800b30a:	f7f5 fb97 	bl	8000a3c <__aeabi_dcmplt>
 800b30e:	2800      	cmp	r0, #0
 800b310:	d050      	beq.n	800b3b4 <_dtoa_r+0x22c>
 800b312:	f10a 3aff 	add.w	sl, sl, #4294967295
 800b316:	2300      	movs	r3, #0
 800b318:	9311      	str	r3, [sp, #68]	; 0x44
 800b31a:	9b16      	ldr	r3, [sp, #88]	; 0x58
 800b31c:	1b9e      	subs	r6, r3, r6
 800b31e:	1e73      	subs	r3, r6, #1
 800b320:	9307      	str	r3, [sp, #28]
 800b322:	bf45      	ittet	mi
 800b324:	f1c6 0301 	rsbmi	r3, r6, #1
 800b328:	9306      	strmi	r3, [sp, #24]
 800b32a:	2300      	movpl	r3, #0
 800b32c:	2300      	movmi	r3, #0
 800b32e:	bf4c      	ite	mi
 800b330:	9307      	strmi	r3, [sp, #28]
 800b332:	9306      	strpl	r3, [sp, #24]
 800b334:	f1ba 0f00 	cmp.w	sl, #0
 800b338:	db3e      	blt.n	800b3b8 <_dtoa_r+0x230>
 800b33a:	9b07      	ldr	r3, [sp, #28]
 800b33c:	f8cd a040 	str.w	sl, [sp, #64]	; 0x40
 800b340:	4453      	add	r3, sl
 800b342:	9307      	str	r3, [sp, #28]
 800b344:	2300      	movs	r3, #0
 800b346:	9309      	str	r3, [sp, #36]	; 0x24
 800b348:	9b08      	ldr	r3, [sp, #32]
 800b34a:	2b09      	cmp	r3, #9
 800b34c:	f200 808f 	bhi.w	800b46e <_dtoa_r+0x2e6>
 800b350:	2b05      	cmp	r3, #5
 800b352:	bfc4      	itt	gt
 800b354:	3b04      	subgt	r3, #4
 800b356:	9308      	strgt	r3, [sp, #32]
 800b358:	9b08      	ldr	r3, [sp, #32]
 800b35a:	f1a3 0302 	sub.w	r3, r3, #2
 800b35e:	bfcc      	ite	gt
 800b360:	2400      	movgt	r4, #0
 800b362:	2401      	movle	r4, #1
 800b364:	2b03      	cmp	r3, #3
 800b366:	f200 808d 	bhi.w	800b484 <_dtoa_r+0x2fc>
 800b36a:	e8df f003 	tbb	[pc, r3]
 800b36e:	3c2f      	.short	0x3c2f
 800b370:	7e3a      	.short	0x7e3a
 800b372:	e9dd 6316 	ldrd	r6, r3, [sp, #88]	; 0x58
 800b376:	441e      	add	r6, r3
 800b378:	f206 4032 	addw	r0, r6, #1074	; 0x432
 800b37c:	2820      	cmp	r0, #32
 800b37e:	dd11      	ble.n	800b3a4 <_dtoa_r+0x21c>
 800b380:	f1c0 0040 	rsb	r0, r0, #64	; 0x40
 800b384:	9b00      	ldr	r3, [sp, #0]
 800b386:	4084      	lsls	r4, r0
 800b388:	f206 4012 	addw	r0, r6, #1042	; 0x412
 800b38c:	fa23 f000 	lsr.w	r0, r3, r0
 800b390:	4320      	orrs	r0, r4
 800b392:	f7f5 f85f 	bl	8000454 <__aeabi_ui2d>
 800b396:	2301      	movs	r3, #1
 800b398:	4604      	mov	r4, r0
 800b39a:	f1a1 75f8 	sub.w	r5, r1, #32505856	; 0x1f00000
 800b39e:	3e01      	subs	r6, #1
 800b3a0:	9315      	str	r3, [sp, #84]	; 0x54
 800b3a2:	e772      	b.n	800b28a <_dtoa_r+0x102>
 800b3a4:	9b00      	ldr	r3, [sp, #0]
 800b3a6:	f1c0 0020 	rsb	r0, r0, #32
 800b3aa:	fa03 f000 	lsl.w	r0, r3, r0
 800b3ae:	e7f0      	b.n	800b392 <_dtoa_r+0x20a>
 800b3b0:	2301      	movs	r3, #1
 800b3b2:	e7b1      	b.n	800b318 <_dtoa_r+0x190>
 800b3b4:	9011      	str	r0, [sp, #68]	; 0x44
 800b3b6:	e7b0      	b.n	800b31a <_dtoa_r+0x192>
 800b3b8:	9b06      	ldr	r3, [sp, #24]
 800b3ba:	eba3 030a 	sub.w	r3, r3, sl
 800b3be:	9306      	str	r3, [sp, #24]
 800b3c0:	f1ca 0300 	rsb	r3, sl, #0
 800b3c4:	9309      	str	r3, [sp, #36]	; 0x24
 800b3c6:	2300      	movs	r3, #0
 800b3c8:	9310      	str	r3, [sp, #64]	; 0x40
 800b3ca:	e7bd      	b.n	800b348 <_dtoa_r+0x1c0>
 800b3cc:	2300      	movs	r3, #0
 800b3ce:	930c      	str	r3, [sp, #48]	; 0x30
 800b3d0:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800b3d2:	2b00      	cmp	r3, #0
 800b3d4:	dc59      	bgt.n	800b48a <_dtoa_r+0x302>
 800b3d6:	2301      	movs	r3, #1
 800b3d8:	9304      	str	r3, [sp, #16]
 800b3da:	4699      	mov	r9, r3
 800b3dc:	461a      	mov	r2, r3
 800b3de:	920d      	str	r2, [sp, #52]	; 0x34
 800b3e0:	e00c      	b.n	800b3fc <_dtoa_r+0x274>
 800b3e2:	2301      	movs	r3, #1
 800b3e4:	e7f3      	b.n	800b3ce <_dtoa_r+0x246>
 800b3e6:	2300      	movs	r3, #0
 800b3e8:	930c      	str	r3, [sp, #48]	; 0x30
 800b3ea:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800b3ec:	4453      	add	r3, sl
 800b3ee:	f103 0901 	add.w	r9, r3, #1
 800b3f2:	9304      	str	r3, [sp, #16]
 800b3f4:	464b      	mov	r3, r9
 800b3f6:	2b01      	cmp	r3, #1
 800b3f8:	bfb8      	it	lt
 800b3fa:	2301      	movlt	r3, #1
 800b3fc:	f8db 5024 	ldr.w	r5, [fp, #36]	; 0x24
 800b400:	2200      	movs	r2, #0
 800b402:	606a      	str	r2, [r5, #4]
 800b404:	2204      	movs	r2, #4
 800b406:	f102 0014 	add.w	r0, r2, #20
 800b40a:	4298      	cmp	r0, r3
 800b40c:	6869      	ldr	r1, [r5, #4]
 800b40e:	d95d      	bls.n	800b4cc <_dtoa_r+0x344>
 800b410:	4658      	mov	r0, fp
 800b412:	f000 fc77 	bl	800bd04 <_Balloc>
 800b416:	f8db 3024 	ldr.w	r3, [fp, #36]	; 0x24
 800b41a:	6028      	str	r0, [r5, #0]
 800b41c:	681b      	ldr	r3, [r3, #0]
 800b41e:	9303      	str	r3, [sp, #12]
 800b420:	f1b9 0f0e 	cmp.w	r9, #14
 800b424:	f200 80d7 	bhi.w	800b5d6 <_dtoa_r+0x44e>
 800b428:	2c00      	cmp	r4, #0
 800b42a:	f000 80d4 	beq.w	800b5d6 <_dtoa_r+0x44e>
 800b42e:	f1ba 0f00 	cmp.w	sl, #0
 800b432:	dd69      	ble.n	800b508 <_dtoa_r+0x380>
 800b434:	4a23      	ldr	r2, [pc, #140]	; (800b4c4 <_dtoa_r+0x33c>)
 800b436:	f00a 030f 	and.w	r3, sl, #15
 800b43a:	eb02 03c3 	add.w	r3, r2, r3, lsl #3
 800b43e:	ed93 7b00 	vldr	d7, [r3]
 800b442:	ea4f 142a 	mov.w	r4, sl, asr #4
 800b446:	06e2      	lsls	r2, r4, #27
 800b448:	ed8d 7b0a 	vstr	d7, [sp, #40]	; 0x28
 800b44c:	d55a      	bpl.n	800b504 <_dtoa_r+0x37c>
 800b44e:	4b1e      	ldr	r3, [pc, #120]	; (800b4c8 <_dtoa_r+0x340>)
 800b450:	e9dd 010e 	ldrd	r0, r1, [sp, #56]	; 0x38
 800b454:	e9d3 2308 	ldrd	r2, r3, [r3, #32]
 800b458:	f7f5 f9a0 	bl	800079c <__aeabi_ddiv>
 800b45c:	e9cd 0100 	strd	r0, r1, [sp]
 800b460:	f004 040f 	and.w	r4, r4, #15
 800b464:	2603      	movs	r6, #3
 800b466:	4d18      	ldr	r5, [pc, #96]	; (800b4c8 <_dtoa_r+0x340>)
 800b468:	e041      	b.n	800b4ee <_dtoa_r+0x366>
 800b46a:	2301      	movs	r3, #1
 800b46c:	e7bc      	b.n	800b3e8 <_dtoa_r+0x260>
 800b46e:	2401      	movs	r4, #1
 800b470:	2300      	movs	r3, #0
 800b472:	9308      	str	r3, [sp, #32]
 800b474:	940c      	str	r4, [sp, #48]	; 0x30
 800b476:	f04f 33ff 	mov.w	r3, #4294967295
 800b47a:	9304      	str	r3, [sp, #16]
 800b47c:	4699      	mov	r9, r3
 800b47e:	2200      	movs	r2, #0
 800b480:	2312      	movs	r3, #18
 800b482:	e7ac      	b.n	800b3de <_dtoa_r+0x256>
 800b484:	2301      	movs	r3, #1
 800b486:	930c      	str	r3, [sp, #48]	; 0x30
 800b488:	e7f5      	b.n	800b476 <_dtoa_r+0x2ee>
 800b48a:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800b48c:	9304      	str	r3, [sp, #16]
 800b48e:	4699      	mov	r9, r3
 800b490:	e7b4      	b.n	800b3fc <_dtoa_r+0x274>
 800b492:	bf00      	nop
 800b494:	f3af 8000 	nop.w
 800b498:	636f4361 	.word	0x636f4361
 800b49c:	3fd287a7 	.word	0x3fd287a7
 800b4a0:	8b60c8b3 	.word	0x8b60c8b3
 800b4a4:	3fc68a28 	.word	0x3fc68a28
 800b4a8:	509f79fb 	.word	0x509f79fb
 800b4ac:	3fd34413 	.word	0x3fd34413
 800b4b0:	7ff00000 	.word	0x7ff00000
 800b4b4:	0800d387 	.word	0x0800d387
 800b4b8:	0800d37e 	.word	0x0800d37e
 800b4bc:	0800d37d 	.word	0x0800d37d
 800b4c0:	3ff80000 	.word	0x3ff80000
 800b4c4:	0800d418 	.word	0x0800d418
 800b4c8:	0800d3f0 	.word	0x0800d3f0
 800b4cc:	3101      	adds	r1, #1
 800b4ce:	6069      	str	r1, [r5, #4]
 800b4d0:	0052      	lsls	r2, r2, #1
 800b4d2:	e798      	b.n	800b406 <_dtoa_r+0x27e>
 800b4d4:	07e3      	lsls	r3, r4, #31
 800b4d6:	d508      	bpl.n	800b4ea <_dtoa_r+0x362>
 800b4d8:	e9dd 010a 	ldrd	r0, r1, [sp, #40]	; 0x28
 800b4dc:	e9d5 2300 	ldrd	r2, r3, [r5]
 800b4e0:	f7f5 f832 	bl	8000548 <__aeabi_dmul>
 800b4e4:	e9cd 010a 	strd	r0, r1, [sp, #40]	; 0x28
 800b4e8:	3601      	adds	r6, #1
 800b4ea:	1064      	asrs	r4, r4, #1
 800b4ec:	3508      	adds	r5, #8
 800b4ee:	2c00      	cmp	r4, #0
 800b4f0:	d1f0      	bne.n	800b4d4 <_dtoa_r+0x34c>
 800b4f2:	e9dd 230a 	ldrd	r2, r3, [sp, #40]	; 0x28
 800b4f6:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b4fa:	f7f5 f94f 	bl	800079c <__aeabi_ddiv>
 800b4fe:	e9cd 0100 	strd	r0, r1, [sp]
 800b502:	e01a      	b.n	800b53a <_dtoa_r+0x3b2>
 800b504:	2602      	movs	r6, #2
 800b506:	e7ae      	b.n	800b466 <_dtoa_r+0x2de>
 800b508:	f000 809f 	beq.w	800b64a <_dtoa_r+0x4c2>
 800b50c:	f1ca 0400 	rsb	r4, sl, #0
 800b510:	4b9f      	ldr	r3, [pc, #636]	; (800b790 <_dtoa_r+0x608>)
 800b512:	4da0      	ldr	r5, [pc, #640]	; (800b794 <_dtoa_r+0x60c>)
 800b514:	f004 020f 	and.w	r2, r4, #15
 800b518:	eb03 03c2 	add.w	r3, r3, r2, lsl #3
 800b51c:	e9d3 2300 	ldrd	r2, r3, [r3]
 800b520:	e9dd 010e 	ldrd	r0, r1, [sp, #56]	; 0x38
 800b524:	f7f5 f810 	bl	8000548 <__aeabi_dmul>
 800b528:	e9cd 0100 	strd	r0, r1, [sp]
 800b52c:	1124      	asrs	r4, r4, #4
 800b52e:	2300      	movs	r3, #0
 800b530:	2602      	movs	r6, #2
 800b532:	2c00      	cmp	r4, #0
 800b534:	d17e      	bne.n	800b634 <_dtoa_r+0x4ac>
 800b536:	2b00      	cmp	r3, #0
 800b538:	d1e1      	bne.n	800b4fe <_dtoa_r+0x376>
 800b53a:	9b11      	ldr	r3, [sp, #68]	; 0x44
 800b53c:	2b00      	cmp	r3, #0
 800b53e:	f000 8086 	beq.w	800b64e <_dtoa_r+0x4c6>
 800b542:	e9dd 4500 	ldrd	r4, r5, [sp]
 800b546:	2200      	movs	r2, #0
 800b548:	4b93      	ldr	r3, [pc, #588]	; (800b798 <_dtoa_r+0x610>)
 800b54a:	4620      	mov	r0, r4
 800b54c:	4629      	mov	r1, r5
 800b54e:	f7f5 fa75 	bl	8000a3c <__aeabi_dcmplt>
 800b552:	2800      	cmp	r0, #0
 800b554:	d07b      	beq.n	800b64e <_dtoa_r+0x4c6>
 800b556:	f1b9 0f00 	cmp.w	r9, #0
 800b55a:	d078      	beq.n	800b64e <_dtoa_r+0x4c6>
 800b55c:	9b04      	ldr	r3, [sp, #16]
 800b55e:	2b00      	cmp	r3, #0
 800b560:	dd35      	ble.n	800b5ce <_dtoa_r+0x446>
 800b562:	f10a 33ff 	add.w	r3, sl, #4294967295
 800b566:	930a      	str	r3, [sp, #40]	; 0x28
 800b568:	4620      	mov	r0, r4
 800b56a:	2200      	movs	r2, #0
 800b56c:	4b8b      	ldr	r3, [pc, #556]	; (800b79c <_dtoa_r+0x614>)
 800b56e:	4629      	mov	r1, r5
 800b570:	f7f4 ffea 	bl	8000548 <__aeabi_dmul>
 800b574:	e9cd 0100 	strd	r0, r1, [sp]
 800b578:	9c04      	ldr	r4, [sp, #16]
 800b57a:	3601      	adds	r6, #1
 800b57c:	4630      	mov	r0, r6
 800b57e:	f7f4 ff79 	bl	8000474 <__aeabi_i2d>
 800b582:	e9dd 2300 	ldrd	r2, r3, [sp]
 800b586:	f7f4 ffdf 	bl	8000548 <__aeabi_dmul>
 800b58a:	2200      	movs	r2, #0
 800b58c:	4b84      	ldr	r3, [pc, #528]	; (800b7a0 <_dtoa_r+0x618>)
 800b58e:	f7f4 fe25 	bl	80001dc <__adddf3>
 800b592:	4606      	mov	r6, r0
 800b594:	f1a1 7750 	sub.w	r7, r1, #54525952	; 0x3400000
 800b598:	2c00      	cmp	r4, #0
 800b59a:	d15c      	bne.n	800b656 <_dtoa_r+0x4ce>
 800b59c:	2200      	movs	r2, #0
 800b59e:	4b81      	ldr	r3, [pc, #516]	; (800b7a4 <_dtoa_r+0x61c>)
 800b5a0:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b5a4:	f7f4 fe18 	bl	80001d8 <__aeabi_dsub>
 800b5a8:	4632      	mov	r2, r6
 800b5aa:	463b      	mov	r3, r7
 800b5ac:	e9cd 0100 	strd	r0, r1, [sp]
 800b5b0:	f7f5 fa62 	bl	8000a78 <__aeabi_dcmpgt>
 800b5b4:	2800      	cmp	r0, #0
 800b5b6:	f040 828c 	bne.w	800bad2 <_dtoa_r+0x94a>
 800b5ba:	4632      	mov	r2, r6
 800b5bc:	f107 4300 	add.w	r3, r7, #2147483648	; 0x80000000
 800b5c0:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b5c4:	f7f5 fa3a 	bl	8000a3c <__aeabi_dcmplt>
 800b5c8:	2800      	cmp	r0, #0
 800b5ca:	f040 8280 	bne.w	800bace <_dtoa_r+0x946>
 800b5ce:	e9dd 340e 	ldrd	r3, r4, [sp, #56]	; 0x38
 800b5d2:	e9cd 3400 	strd	r3, r4, [sp]
 800b5d6:	9b17      	ldr	r3, [sp, #92]	; 0x5c
 800b5d8:	2b00      	cmp	r3, #0
 800b5da:	f2c0 814f 	blt.w	800b87c <_dtoa_r+0x6f4>
 800b5de:	f1ba 0f0e 	cmp.w	sl, #14
 800b5e2:	f300 814b 	bgt.w	800b87c <_dtoa_r+0x6f4>
 800b5e6:	4b6a      	ldr	r3, [pc, #424]	; (800b790 <_dtoa_r+0x608>)
 800b5e8:	eb03 03ca 	add.w	r3, r3, sl, lsl #3
 800b5ec:	ed93 7b00 	vldr	d7, [r3]
 800b5f0:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800b5f2:	2b00      	cmp	r3, #0
 800b5f4:	ed8d 7b04 	vstr	d7, [sp, #16]
 800b5f8:	f280 80d8 	bge.w	800b7ac <_dtoa_r+0x624>
 800b5fc:	f1b9 0f00 	cmp.w	r9, #0
 800b600:	f300 80d4 	bgt.w	800b7ac <_dtoa_r+0x624>
 800b604:	f040 8262 	bne.w	800bacc <_dtoa_r+0x944>
 800b608:	2200      	movs	r2, #0
 800b60a:	4b66      	ldr	r3, [pc, #408]	; (800b7a4 <_dtoa_r+0x61c>)
 800b60c:	ec51 0b17 	vmov	r0, r1, d7
 800b610:	f7f4 ff9a 	bl	8000548 <__aeabi_dmul>
 800b614:	e9dd 2300 	ldrd	r2, r3, [sp]
 800b618:	f7f5 fa24 	bl	8000a64 <__aeabi_dcmpge>
 800b61c:	464c      	mov	r4, r9
 800b61e:	464e      	mov	r6, r9
 800b620:	2800      	cmp	r0, #0
 800b622:	f040 823b 	bne.w	800ba9c <_dtoa_r+0x914>
 800b626:	9d03      	ldr	r5, [sp, #12]
 800b628:	2331      	movs	r3, #49	; 0x31
 800b62a:	f805 3b01 	strb.w	r3, [r5], #1
 800b62e:	f10a 0a01 	add.w	sl, sl, #1
 800b632:	e237      	b.n	800baa4 <_dtoa_r+0x91c>
 800b634:	07e7      	lsls	r7, r4, #31
 800b636:	d505      	bpl.n	800b644 <_dtoa_r+0x4bc>
 800b638:	e9d5 2300 	ldrd	r2, r3, [r5]
 800b63c:	f7f4 ff84 	bl	8000548 <__aeabi_dmul>
 800b640:	3601      	adds	r6, #1
 800b642:	2301      	movs	r3, #1
 800b644:	1064      	asrs	r4, r4, #1
 800b646:	3508      	adds	r5, #8
 800b648:	e773      	b.n	800b532 <_dtoa_r+0x3aa>
 800b64a:	2602      	movs	r6, #2
 800b64c:	e775      	b.n	800b53a <_dtoa_r+0x3b2>
 800b64e:	f8cd a028 	str.w	sl, [sp, #40]	; 0x28
 800b652:	464c      	mov	r4, r9
 800b654:	e792      	b.n	800b57c <_dtoa_r+0x3f4>
 800b656:	4b4e      	ldr	r3, [pc, #312]	; (800b790 <_dtoa_r+0x608>)
 800b658:	eb03 03c4 	add.w	r3, r3, r4, lsl #3
 800b65c:	e953 0102 	ldrd	r0, r1, [r3, #-8]
 800b660:	9b03      	ldr	r3, [sp, #12]
 800b662:	441c      	add	r4, r3
 800b664:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800b666:	2b00      	cmp	r3, #0
 800b668:	d046      	beq.n	800b6f8 <_dtoa_r+0x570>
 800b66a:	4602      	mov	r2, r0
 800b66c:	460b      	mov	r3, r1
 800b66e:	2000      	movs	r0, #0
 800b670:	494d      	ldr	r1, [pc, #308]	; (800b7a8 <_dtoa_r+0x620>)
 800b672:	f7f5 f893 	bl	800079c <__aeabi_ddiv>
 800b676:	4632      	mov	r2, r6
 800b678:	463b      	mov	r3, r7
 800b67a:	f7f4 fdad 	bl	80001d8 <__aeabi_dsub>
 800b67e:	9d03      	ldr	r5, [sp, #12]
 800b680:	4606      	mov	r6, r0
 800b682:	460f      	mov	r7, r1
 800b684:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b688:	f7f5 fa16 	bl	8000ab8 <__aeabi_d2iz>
 800b68c:	9012      	str	r0, [sp, #72]	; 0x48
 800b68e:	f7f4 fef1 	bl	8000474 <__aeabi_i2d>
 800b692:	4602      	mov	r2, r0
 800b694:	460b      	mov	r3, r1
 800b696:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b69a:	f7f4 fd9d 	bl	80001d8 <__aeabi_dsub>
 800b69e:	9b12      	ldr	r3, [sp, #72]	; 0x48
 800b6a0:	3330      	adds	r3, #48	; 0x30
 800b6a2:	f805 3b01 	strb.w	r3, [r5], #1
 800b6a6:	4632      	mov	r2, r6
 800b6a8:	463b      	mov	r3, r7
 800b6aa:	e9cd 0100 	strd	r0, r1, [sp]
 800b6ae:	f7f5 f9c5 	bl	8000a3c <__aeabi_dcmplt>
 800b6b2:	2800      	cmp	r0, #0
 800b6b4:	d160      	bne.n	800b778 <_dtoa_r+0x5f0>
 800b6b6:	e9dd 2300 	ldrd	r2, r3, [sp]
 800b6ba:	2000      	movs	r0, #0
 800b6bc:	4936      	ldr	r1, [pc, #216]	; (800b798 <_dtoa_r+0x610>)
 800b6be:	f7f4 fd8b 	bl	80001d8 <__aeabi_dsub>
 800b6c2:	4632      	mov	r2, r6
 800b6c4:	463b      	mov	r3, r7
 800b6c6:	f7f5 f9b9 	bl	8000a3c <__aeabi_dcmplt>
 800b6ca:	2800      	cmp	r0, #0
 800b6cc:	f040 80b4 	bne.w	800b838 <_dtoa_r+0x6b0>
 800b6d0:	42a5      	cmp	r5, r4
 800b6d2:	f43f af7c 	beq.w	800b5ce <_dtoa_r+0x446>
 800b6d6:	2200      	movs	r2, #0
 800b6d8:	4b30      	ldr	r3, [pc, #192]	; (800b79c <_dtoa_r+0x614>)
 800b6da:	4630      	mov	r0, r6
 800b6dc:	4639      	mov	r1, r7
 800b6de:	f7f4 ff33 	bl	8000548 <__aeabi_dmul>
 800b6e2:	2200      	movs	r2, #0
 800b6e4:	4606      	mov	r6, r0
 800b6e6:	460f      	mov	r7, r1
 800b6e8:	4b2c      	ldr	r3, [pc, #176]	; (800b79c <_dtoa_r+0x614>)
 800b6ea:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b6ee:	f7f4 ff2b 	bl	8000548 <__aeabi_dmul>
 800b6f2:	e9cd 0100 	strd	r0, r1, [sp]
 800b6f6:	e7c5      	b.n	800b684 <_dtoa_r+0x4fc>
 800b6f8:	4632      	mov	r2, r6
 800b6fa:	463b      	mov	r3, r7
 800b6fc:	f7f4 ff24 	bl	8000548 <__aeabi_dmul>
 800b700:	e9cd 0112 	strd	r0, r1, [sp, #72]	; 0x48
 800b704:	9e03      	ldr	r6, [sp, #12]
 800b706:	4625      	mov	r5, r4
 800b708:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b70c:	f7f5 f9d4 	bl	8000ab8 <__aeabi_d2iz>
 800b710:	4607      	mov	r7, r0
 800b712:	f7f4 feaf 	bl	8000474 <__aeabi_i2d>
 800b716:	3730      	adds	r7, #48	; 0x30
 800b718:	4602      	mov	r2, r0
 800b71a:	460b      	mov	r3, r1
 800b71c:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b720:	f7f4 fd5a 	bl	80001d8 <__aeabi_dsub>
 800b724:	f806 7b01 	strb.w	r7, [r6], #1
 800b728:	42a6      	cmp	r6, r4
 800b72a:	e9cd 0100 	strd	r0, r1, [sp]
 800b72e:	f04f 0200 	mov.w	r2, #0
 800b732:	d126      	bne.n	800b782 <_dtoa_r+0x5fa>
 800b734:	4b1c      	ldr	r3, [pc, #112]	; (800b7a8 <_dtoa_r+0x620>)
 800b736:	e9dd 0112 	ldrd	r0, r1, [sp, #72]	; 0x48
 800b73a:	f7f4 fd4f 	bl	80001dc <__adddf3>
 800b73e:	4602      	mov	r2, r0
 800b740:	460b      	mov	r3, r1
 800b742:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b746:	f7f5 f997 	bl	8000a78 <__aeabi_dcmpgt>
 800b74a:	2800      	cmp	r0, #0
 800b74c:	d174      	bne.n	800b838 <_dtoa_r+0x6b0>
 800b74e:	e9dd 2312 	ldrd	r2, r3, [sp, #72]	; 0x48
 800b752:	2000      	movs	r0, #0
 800b754:	4914      	ldr	r1, [pc, #80]	; (800b7a8 <_dtoa_r+0x620>)
 800b756:	f7f4 fd3f 	bl	80001d8 <__aeabi_dsub>
 800b75a:	4602      	mov	r2, r0
 800b75c:	460b      	mov	r3, r1
 800b75e:	e9dd 0100 	ldrd	r0, r1, [sp]
 800b762:	f7f5 f96b 	bl	8000a3c <__aeabi_dcmplt>
 800b766:	2800      	cmp	r0, #0
 800b768:	f43f af31 	beq.w	800b5ce <_dtoa_r+0x446>
 800b76c:	f815 3c01 	ldrb.w	r3, [r5, #-1]
 800b770:	2b30      	cmp	r3, #48	; 0x30
 800b772:	f105 32ff 	add.w	r2, r5, #4294967295
 800b776:	d002      	beq.n	800b77e <_dtoa_r+0x5f6>
 800b778:	f8dd a028 	ldr.w	sl, [sp, #40]	; 0x28
 800b77c:	e04a      	b.n	800b814 <_dtoa_r+0x68c>
 800b77e:	4615      	mov	r5, r2
 800b780:	e7f4      	b.n	800b76c <_dtoa_r+0x5e4>
 800b782:	4b06      	ldr	r3, [pc, #24]	; (800b79c <_dtoa_r+0x614>)
 800b784:	f7f4 fee0 	bl	8000548 <__aeabi_dmul>
 800b788:	e9cd 0100 	strd	r0, r1, [sp]
 800b78c:	e7bc      	b.n	800b708 <_dtoa_r+0x580>
 800b78e:	bf00      	nop
 800b790:	0800d418 	.word	0x0800d418
 800b794:	0800d3f0 	.word	0x0800d3f0
 800b798:	3ff00000 	.word	0x3ff00000
 800b79c:	40240000 	.word	0x40240000
 800b7a0:	401c0000 	.word	0x401c0000
 800b7a4:	40140000 	.word	0x40140000
 800b7a8:	3fe00000 	.word	0x3fe00000
 800b7ac:	e9dd 6700 	ldrd	r6, r7, [sp]
 800b7b0:	9d03      	ldr	r5, [sp, #12]
 800b7b2:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 800b7b6:	4630      	mov	r0, r6
 800b7b8:	4639      	mov	r1, r7
 800b7ba:	f7f4 ffef 	bl	800079c <__aeabi_ddiv>
 800b7be:	f7f5 f97b 	bl	8000ab8 <__aeabi_d2iz>
 800b7c2:	4604      	mov	r4, r0
 800b7c4:	f7f4 fe56 	bl	8000474 <__aeabi_i2d>
 800b7c8:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 800b7cc:	f7f4 febc 	bl	8000548 <__aeabi_dmul>
 800b7d0:	4602      	mov	r2, r0
 800b7d2:	460b      	mov	r3, r1
 800b7d4:	4630      	mov	r0, r6
 800b7d6:	4639      	mov	r1, r7
 800b7d8:	f104 0630 	add.w	r6, r4, #48	; 0x30
 800b7dc:	f7f4 fcfc 	bl	80001d8 <__aeabi_dsub>
 800b7e0:	f805 6b01 	strb.w	r6, [r5], #1
 800b7e4:	9e03      	ldr	r6, [sp, #12]
 800b7e6:	1bae      	subs	r6, r5, r6
 800b7e8:	45b1      	cmp	r9, r6
 800b7ea:	4602      	mov	r2, r0
 800b7ec:	460b      	mov	r3, r1
 800b7ee:	d138      	bne.n	800b862 <_dtoa_r+0x6da>
 800b7f0:	f7f4 fcf4 	bl	80001dc <__adddf3>
 800b7f4:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 800b7f8:	4606      	mov	r6, r0
 800b7fa:	460f      	mov	r7, r1
 800b7fc:	f7f5 f93c 	bl	8000a78 <__aeabi_dcmpgt>
 800b800:	b9c0      	cbnz	r0, 800b834 <_dtoa_r+0x6ac>
 800b802:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 800b806:	4630      	mov	r0, r6
 800b808:	4639      	mov	r1, r7
 800b80a:	f7f5 f90d 	bl	8000a28 <__aeabi_dcmpeq>
 800b80e:	b108      	cbz	r0, 800b814 <_dtoa_r+0x68c>
 800b810:	07e1      	lsls	r1, r4, #31
 800b812:	d40f      	bmi.n	800b834 <_dtoa_r+0x6ac>
 800b814:	4641      	mov	r1, r8
 800b816:	4658      	mov	r0, fp
 800b818:	f000 faa8 	bl	800bd6c <_Bfree>
 800b81c:	2300      	movs	r3, #0
 800b81e:	9a14      	ldr	r2, [sp, #80]	; 0x50
 800b820:	702b      	strb	r3, [r5, #0]
 800b822:	f10a 0301 	add.w	r3, sl, #1
 800b826:	6013      	str	r3, [r2, #0]
 800b828:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800b82a:	2b00      	cmp	r3, #0
 800b82c:	f43f acf4 	beq.w	800b218 <_dtoa_r+0x90>
 800b830:	601d      	str	r5, [r3, #0]
 800b832:	e4f1      	b.n	800b218 <_dtoa_r+0x90>
 800b834:	f8cd a028 	str.w	sl, [sp, #40]	; 0x28
 800b838:	f815 2c01 	ldrb.w	r2, [r5, #-1]
 800b83c:	2a39      	cmp	r2, #57	; 0x39
 800b83e:	f105 33ff 	add.w	r3, r5, #4294967295
 800b842:	d108      	bne.n	800b856 <_dtoa_r+0x6ce>
 800b844:	9a03      	ldr	r2, [sp, #12]
 800b846:	429a      	cmp	r2, r3
 800b848:	d109      	bne.n	800b85e <_dtoa_r+0x6d6>
 800b84a:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 800b84c:	9903      	ldr	r1, [sp, #12]
 800b84e:	3201      	adds	r2, #1
 800b850:	920a      	str	r2, [sp, #40]	; 0x28
 800b852:	2230      	movs	r2, #48	; 0x30
 800b854:	700a      	strb	r2, [r1, #0]
 800b856:	781a      	ldrb	r2, [r3, #0]
 800b858:	3201      	adds	r2, #1
 800b85a:	701a      	strb	r2, [r3, #0]
 800b85c:	e78c      	b.n	800b778 <_dtoa_r+0x5f0>
 800b85e:	461d      	mov	r5, r3
 800b860:	e7ea      	b.n	800b838 <_dtoa_r+0x6b0>
 800b862:	2200      	movs	r2, #0
 800b864:	4b9d      	ldr	r3, [pc, #628]	; (800badc <_dtoa_r+0x954>)
 800b866:	f7f4 fe6f 	bl	8000548 <__aeabi_dmul>
 800b86a:	2200      	movs	r2, #0
 800b86c:	2300      	movs	r3, #0
 800b86e:	4606      	mov	r6, r0
 800b870:	460f      	mov	r7, r1
 800b872:	f7f5 f8d9 	bl	8000a28 <__aeabi_dcmpeq>
 800b876:	2800      	cmp	r0, #0
 800b878:	d09b      	beq.n	800b7b2 <_dtoa_r+0x62a>
 800b87a:	e7cb      	b.n	800b814 <_dtoa_r+0x68c>
 800b87c:	9a0c      	ldr	r2, [sp, #48]	; 0x30
 800b87e:	2a00      	cmp	r2, #0
 800b880:	f000 80cb 	beq.w	800ba1a <_dtoa_r+0x892>
 800b884:	9a08      	ldr	r2, [sp, #32]
 800b886:	2a01      	cmp	r2, #1
 800b888:	f300 80ae 	bgt.w	800b9e8 <_dtoa_r+0x860>
 800b88c:	9a15      	ldr	r2, [sp, #84]	; 0x54
 800b88e:	2a00      	cmp	r2, #0
 800b890:	f000 80a6 	beq.w	800b9e0 <_dtoa_r+0x858>
 800b894:	f203 4333 	addw	r3, r3, #1075	; 0x433
 800b898:	9c09      	ldr	r4, [sp, #36]	; 0x24
 800b89a:	9d06      	ldr	r5, [sp, #24]
 800b89c:	9a06      	ldr	r2, [sp, #24]
 800b89e:	441a      	add	r2, r3
 800b8a0:	9206      	str	r2, [sp, #24]
 800b8a2:	9a07      	ldr	r2, [sp, #28]
 800b8a4:	2101      	movs	r1, #1
 800b8a6:	441a      	add	r2, r3
 800b8a8:	4658      	mov	r0, fp
 800b8aa:	9207      	str	r2, [sp, #28]
 800b8ac:	f000 fb00 	bl	800beb0 <__i2b>
 800b8b0:	4606      	mov	r6, r0
 800b8b2:	2d00      	cmp	r5, #0
 800b8b4:	dd0c      	ble.n	800b8d0 <_dtoa_r+0x748>
 800b8b6:	9b07      	ldr	r3, [sp, #28]
 800b8b8:	2b00      	cmp	r3, #0
 800b8ba:	dd09      	ble.n	800b8d0 <_dtoa_r+0x748>
 800b8bc:	42ab      	cmp	r3, r5
 800b8be:	9a06      	ldr	r2, [sp, #24]
 800b8c0:	bfa8      	it	ge
 800b8c2:	462b      	movge	r3, r5
 800b8c4:	1ad2      	subs	r2, r2, r3
 800b8c6:	9206      	str	r2, [sp, #24]
 800b8c8:	9a07      	ldr	r2, [sp, #28]
 800b8ca:	1aed      	subs	r5, r5, r3
 800b8cc:	1ad3      	subs	r3, r2, r3
 800b8ce:	9307      	str	r3, [sp, #28]
 800b8d0:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800b8d2:	b1f3      	cbz	r3, 800b912 <_dtoa_r+0x78a>
 800b8d4:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800b8d6:	2b00      	cmp	r3, #0
 800b8d8:	f000 80a3 	beq.w	800ba22 <_dtoa_r+0x89a>
 800b8dc:	2c00      	cmp	r4, #0
 800b8de:	dd10      	ble.n	800b902 <_dtoa_r+0x77a>
 800b8e0:	4631      	mov	r1, r6
 800b8e2:	4622      	mov	r2, r4
 800b8e4:	4658      	mov	r0, fp
 800b8e6:	f000 fb7d 	bl	800bfe4 <__pow5mult>
 800b8ea:	4642      	mov	r2, r8
 800b8ec:	4601      	mov	r1, r0
 800b8ee:	4606      	mov	r6, r0
 800b8f0:	4658      	mov	r0, fp
 800b8f2:	f000 fae6 	bl	800bec2 <__multiply>
 800b8f6:	4641      	mov	r1, r8
 800b8f8:	4607      	mov	r7, r0
 800b8fa:	4658      	mov	r0, fp
 800b8fc:	f000 fa36 	bl	800bd6c <_Bfree>
 800b900:	46b8      	mov	r8, r7
 800b902:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800b904:	1b1a      	subs	r2, r3, r4
 800b906:	d004      	beq.n	800b912 <_dtoa_r+0x78a>
 800b908:	4641      	mov	r1, r8
 800b90a:	4658      	mov	r0, fp
 800b90c:	f000 fb6a 	bl	800bfe4 <__pow5mult>
 800b910:	4680      	mov	r8, r0
 800b912:	2101      	movs	r1, #1
 800b914:	4658      	mov	r0, fp
 800b916:	f000 facb 	bl	800beb0 <__i2b>
 800b91a:	9b10      	ldr	r3, [sp, #64]	; 0x40
 800b91c:	2b00      	cmp	r3, #0
 800b91e:	4604      	mov	r4, r0
 800b920:	f340 8081 	ble.w	800ba26 <_dtoa_r+0x89e>
 800b924:	461a      	mov	r2, r3
 800b926:	4601      	mov	r1, r0
 800b928:	4658      	mov	r0, fp
 800b92a:	f000 fb5b 	bl	800bfe4 <__pow5mult>
 800b92e:	9b08      	ldr	r3, [sp, #32]
 800b930:	2b01      	cmp	r3, #1
 800b932:	4604      	mov	r4, r0
 800b934:	dd7a      	ble.n	800ba2c <_dtoa_r+0x8a4>
 800b936:	2700      	movs	r7, #0
 800b938:	6923      	ldr	r3, [r4, #16]
 800b93a:	eb04 0383 	add.w	r3, r4, r3, lsl #2
 800b93e:	6918      	ldr	r0, [r3, #16]
 800b940:	f000 fa66 	bl	800be10 <__hi0bits>
 800b944:	f1c0 0020 	rsb	r0, r0, #32
 800b948:	9b07      	ldr	r3, [sp, #28]
 800b94a:	4418      	add	r0, r3
 800b94c:	f010 001f 	ands.w	r0, r0, #31
 800b950:	f000 808b 	beq.w	800ba6a <_dtoa_r+0x8e2>
 800b954:	f1c0 0320 	rsb	r3, r0, #32
 800b958:	2b04      	cmp	r3, #4
 800b95a:	f340 8084 	ble.w	800ba66 <_dtoa_r+0x8de>
 800b95e:	f1c0 001c 	rsb	r0, r0, #28
 800b962:	9b06      	ldr	r3, [sp, #24]
 800b964:	4403      	add	r3, r0
 800b966:	9306      	str	r3, [sp, #24]
 800b968:	9b07      	ldr	r3, [sp, #28]
 800b96a:	4403      	add	r3, r0
 800b96c:	4405      	add	r5, r0
 800b96e:	9307      	str	r3, [sp, #28]
 800b970:	9b06      	ldr	r3, [sp, #24]
 800b972:	2b00      	cmp	r3, #0
 800b974:	dd05      	ble.n	800b982 <_dtoa_r+0x7fa>
 800b976:	4641      	mov	r1, r8
 800b978:	461a      	mov	r2, r3
 800b97a:	4658      	mov	r0, fp
 800b97c:	f000 fb80 	bl	800c080 <__lshift>
 800b980:	4680      	mov	r8, r0
 800b982:	9b07      	ldr	r3, [sp, #28]
 800b984:	2b00      	cmp	r3, #0
 800b986:	dd05      	ble.n	800b994 <_dtoa_r+0x80c>
 800b988:	4621      	mov	r1, r4
 800b98a:	461a      	mov	r2, r3
 800b98c:	4658      	mov	r0, fp
 800b98e:	f000 fb77 	bl	800c080 <__lshift>
 800b992:	4604      	mov	r4, r0
 800b994:	9b11      	ldr	r3, [sp, #68]	; 0x44
 800b996:	2b00      	cmp	r3, #0
 800b998:	d069      	beq.n	800ba6e <_dtoa_r+0x8e6>
 800b99a:	4621      	mov	r1, r4
 800b99c:	4640      	mov	r0, r8
 800b99e:	f000 fbc2 	bl	800c126 <__mcmp>
 800b9a2:	2800      	cmp	r0, #0
 800b9a4:	da63      	bge.n	800ba6e <_dtoa_r+0x8e6>
 800b9a6:	2300      	movs	r3, #0
 800b9a8:	4641      	mov	r1, r8
 800b9aa:	220a      	movs	r2, #10
 800b9ac:	4658      	mov	r0, fp
 800b9ae:	f000 f9f4 	bl	800bd9a <__multadd>
 800b9b2:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800b9b4:	f10a 3aff 	add.w	sl, sl, #4294967295
 800b9b8:	4680      	mov	r8, r0
 800b9ba:	2b00      	cmp	r3, #0
 800b9bc:	f000 817c 	beq.w	800bcb8 <_dtoa_r+0xb30>
 800b9c0:	2300      	movs	r3, #0
 800b9c2:	4631      	mov	r1, r6
 800b9c4:	220a      	movs	r2, #10
 800b9c6:	4658      	mov	r0, fp
 800b9c8:	f000 f9e7 	bl	800bd9a <__multadd>
 800b9cc:	9b04      	ldr	r3, [sp, #16]
 800b9ce:	2b00      	cmp	r3, #0
 800b9d0:	4606      	mov	r6, r0
 800b9d2:	f300 808b 	bgt.w	800baec <_dtoa_r+0x964>
 800b9d6:	9b08      	ldr	r3, [sp, #32]
 800b9d8:	2b02      	cmp	r3, #2
 800b9da:	f340 8087 	ble.w	800baec <_dtoa_r+0x964>
 800b9de:	e04e      	b.n	800ba7e <_dtoa_r+0x8f6>
 800b9e0:	9b16      	ldr	r3, [sp, #88]	; 0x58
 800b9e2:	f1c3 0336 	rsb	r3, r3, #54	; 0x36
 800b9e6:	e757      	b.n	800b898 <_dtoa_r+0x710>
 800b9e8:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800b9ea:	f109 34ff 	add.w	r4, r9, #4294967295
 800b9ee:	42a3      	cmp	r3, r4
 800b9f0:	bfbf      	itttt	lt
 800b9f2:	9b09      	ldrlt	r3, [sp, #36]	; 0x24
 800b9f4:	9409      	strlt	r4, [sp, #36]	; 0x24
 800b9f6:	1ae2      	sublt	r2, r4, r3
 800b9f8:	9b10      	ldrlt	r3, [sp, #64]	; 0x40
 800b9fa:	bfbb      	ittet	lt
 800b9fc:	189b      	addlt	r3, r3, r2
 800b9fe:	9310      	strlt	r3, [sp, #64]	; 0x40
 800ba00:	1b1c      	subge	r4, r3, r4
 800ba02:	2400      	movlt	r4, #0
 800ba04:	f1b9 0f00 	cmp.w	r9, #0
 800ba08:	bfb5      	itete	lt
 800ba0a:	9b06      	ldrlt	r3, [sp, #24]
 800ba0c:	9d06      	ldrge	r5, [sp, #24]
 800ba0e:	eba3 0509 	sublt.w	r5, r3, r9
 800ba12:	464b      	movge	r3, r9
 800ba14:	bfb8      	it	lt
 800ba16:	2300      	movlt	r3, #0
 800ba18:	e740      	b.n	800b89c <_dtoa_r+0x714>
 800ba1a:	9c09      	ldr	r4, [sp, #36]	; 0x24
 800ba1c:	9d06      	ldr	r5, [sp, #24]
 800ba1e:	9e0c      	ldr	r6, [sp, #48]	; 0x30
 800ba20:	e747      	b.n	800b8b2 <_dtoa_r+0x72a>
 800ba22:	9a09      	ldr	r2, [sp, #36]	; 0x24
 800ba24:	e770      	b.n	800b908 <_dtoa_r+0x780>
 800ba26:	9b08      	ldr	r3, [sp, #32]
 800ba28:	2b01      	cmp	r3, #1
 800ba2a:	dc18      	bgt.n	800ba5e <_dtoa_r+0x8d6>
 800ba2c:	9b00      	ldr	r3, [sp, #0]
 800ba2e:	b9b3      	cbnz	r3, 800ba5e <_dtoa_r+0x8d6>
 800ba30:	9b01      	ldr	r3, [sp, #4]
 800ba32:	f3c3 0313 	ubfx	r3, r3, #0, #20
 800ba36:	b9a3      	cbnz	r3, 800ba62 <_dtoa_r+0x8da>
 800ba38:	9b01      	ldr	r3, [sp, #4]
 800ba3a:	f023 4700 	bic.w	r7, r3, #2147483648	; 0x80000000
 800ba3e:	0d3f      	lsrs	r7, r7, #20
 800ba40:	053f      	lsls	r7, r7, #20
 800ba42:	b137      	cbz	r7, 800ba52 <_dtoa_r+0x8ca>
 800ba44:	9b06      	ldr	r3, [sp, #24]
 800ba46:	3301      	adds	r3, #1
 800ba48:	9306      	str	r3, [sp, #24]
 800ba4a:	9b07      	ldr	r3, [sp, #28]
 800ba4c:	3301      	adds	r3, #1
 800ba4e:	9307      	str	r3, [sp, #28]
 800ba50:	2701      	movs	r7, #1
 800ba52:	9b10      	ldr	r3, [sp, #64]	; 0x40
 800ba54:	2b00      	cmp	r3, #0
 800ba56:	f47f af6f 	bne.w	800b938 <_dtoa_r+0x7b0>
 800ba5a:	2001      	movs	r0, #1
 800ba5c:	e774      	b.n	800b948 <_dtoa_r+0x7c0>
 800ba5e:	2700      	movs	r7, #0
 800ba60:	e7f7      	b.n	800ba52 <_dtoa_r+0x8ca>
 800ba62:	9f00      	ldr	r7, [sp, #0]
 800ba64:	e7f5      	b.n	800ba52 <_dtoa_r+0x8ca>
 800ba66:	d083      	beq.n	800b970 <_dtoa_r+0x7e8>
 800ba68:	4618      	mov	r0, r3
 800ba6a:	301c      	adds	r0, #28
 800ba6c:	e779      	b.n	800b962 <_dtoa_r+0x7da>
 800ba6e:	f1b9 0f00 	cmp.w	r9, #0
 800ba72:	dc35      	bgt.n	800bae0 <_dtoa_r+0x958>
 800ba74:	9b08      	ldr	r3, [sp, #32]
 800ba76:	2b02      	cmp	r3, #2
 800ba78:	dd32      	ble.n	800bae0 <_dtoa_r+0x958>
 800ba7a:	f8cd 9010 	str.w	r9, [sp, #16]
 800ba7e:	9b04      	ldr	r3, [sp, #16]
 800ba80:	b963      	cbnz	r3, 800ba9c <_dtoa_r+0x914>
 800ba82:	4621      	mov	r1, r4
 800ba84:	2205      	movs	r2, #5
 800ba86:	4658      	mov	r0, fp
 800ba88:	f000 f987 	bl	800bd9a <__multadd>
 800ba8c:	4601      	mov	r1, r0
 800ba8e:	4604      	mov	r4, r0
 800ba90:	4640      	mov	r0, r8
 800ba92:	f000 fb48 	bl	800c126 <__mcmp>
 800ba96:	2800      	cmp	r0, #0
 800ba98:	f73f adc5 	bgt.w	800b626 <_dtoa_r+0x49e>
 800ba9c:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800ba9e:	9d03      	ldr	r5, [sp, #12]
 800baa0:	ea6f 0a03 	mvn.w	sl, r3
 800baa4:	2700      	movs	r7, #0
 800baa6:	4621      	mov	r1, r4
 800baa8:	4658      	mov	r0, fp
 800baaa:	f000 f95f 	bl	800bd6c <_Bfree>
 800baae:	2e00      	cmp	r6, #0
 800bab0:	f43f aeb0 	beq.w	800b814 <_dtoa_r+0x68c>
 800bab4:	b12f      	cbz	r7, 800bac2 <_dtoa_r+0x93a>
 800bab6:	42b7      	cmp	r7, r6
 800bab8:	d003      	beq.n	800bac2 <_dtoa_r+0x93a>
 800baba:	4639      	mov	r1, r7
 800babc:	4658      	mov	r0, fp
 800babe:	f000 f955 	bl	800bd6c <_Bfree>
 800bac2:	4631      	mov	r1, r6
 800bac4:	4658      	mov	r0, fp
 800bac6:	f000 f951 	bl	800bd6c <_Bfree>
 800baca:	e6a3      	b.n	800b814 <_dtoa_r+0x68c>
 800bacc:	2400      	movs	r4, #0
 800bace:	4626      	mov	r6, r4
 800bad0:	e7e4      	b.n	800ba9c <_dtoa_r+0x914>
 800bad2:	f8dd a028 	ldr.w	sl, [sp, #40]	; 0x28
 800bad6:	4626      	mov	r6, r4
 800bad8:	e5a5      	b.n	800b626 <_dtoa_r+0x49e>
 800bada:	bf00      	nop
 800badc:	40240000 	.word	0x40240000
 800bae0:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800bae2:	f8cd 9010 	str.w	r9, [sp, #16]
 800bae6:	2b00      	cmp	r3, #0
 800bae8:	f000 80ed 	beq.w	800bcc6 <_dtoa_r+0xb3e>
 800baec:	2d00      	cmp	r5, #0
 800baee:	dd05      	ble.n	800bafc <_dtoa_r+0x974>
 800baf0:	4631      	mov	r1, r6
 800baf2:	462a      	mov	r2, r5
 800baf4:	4658      	mov	r0, fp
 800baf6:	f000 fac3 	bl	800c080 <__lshift>
 800bafa:	4606      	mov	r6, r0
 800bafc:	2f00      	cmp	r7, #0
 800bafe:	d056      	beq.n	800bbae <_dtoa_r+0xa26>
 800bb00:	6871      	ldr	r1, [r6, #4]
 800bb02:	4658      	mov	r0, fp
 800bb04:	f000 f8fe 	bl	800bd04 <_Balloc>
 800bb08:	6932      	ldr	r2, [r6, #16]
 800bb0a:	3202      	adds	r2, #2
 800bb0c:	4605      	mov	r5, r0
 800bb0e:	0092      	lsls	r2, r2, #2
 800bb10:	f106 010c 	add.w	r1, r6, #12
 800bb14:	300c      	adds	r0, #12
 800bb16:	f7fe fef9 	bl	800a90c <memcpy>
 800bb1a:	2201      	movs	r2, #1
 800bb1c:	4629      	mov	r1, r5
 800bb1e:	4658      	mov	r0, fp
 800bb20:	f000 faae 	bl	800c080 <__lshift>
 800bb24:	9b03      	ldr	r3, [sp, #12]
 800bb26:	f103 0901 	add.w	r9, r3, #1
 800bb2a:	e9dd 2303 	ldrd	r2, r3, [sp, #12]
 800bb2e:	4413      	add	r3, r2
 800bb30:	9309      	str	r3, [sp, #36]	; 0x24
 800bb32:	9b00      	ldr	r3, [sp, #0]
 800bb34:	f003 0301 	and.w	r3, r3, #1
 800bb38:	4637      	mov	r7, r6
 800bb3a:	9307      	str	r3, [sp, #28]
 800bb3c:	4606      	mov	r6, r0
 800bb3e:	f109 33ff 	add.w	r3, r9, #4294967295
 800bb42:	4621      	mov	r1, r4
 800bb44:	4640      	mov	r0, r8
 800bb46:	9300      	str	r3, [sp, #0]
 800bb48:	f7ff fa92 	bl	800b070 <quorem>
 800bb4c:	4603      	mov	r3, r0
 800bb4e:	3330      	adds	r3, #48	; 0x30
 800bb50:	9004      	str	r0, [sp, #16]
 800bb52:	4639      	mov	r1, r7
 800bb54:	4640      	mov	r0, r8
 800bb56:	930a      	str	r3, [sp, #40]	; 0x28
 800bb58:	f000 fae5 	bl	800c126 <__mcmp>
 800bb5c:	4632      	mov	r2, r6
 800bb5e:	9006      	str	r0, [sp, #24]
 800bb60:	4621      	mov	r1, r4
 800bb62:	4658      	mov	r0, fp
 800bb64:	f000 faf9 	bl	800c15a <__mdiff>
 800bb68:	68c2      	ldr	r2, [r0, #12]
 800bb6a:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800bb6c:	4605      	mov	r5, r0
 800bb6e:	bb02      	cbnz	r2, 800bbb2 <_dtoa_r+0xa2a>
 800bb70:	4601      	mov	r1, r0
 800bb72:	4640      	mov	r0, r8
 800bb74:	f000 fad7 	bl	800c126 <__mcmp>
 800bb78:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800bb7a:	4602      	mov	r2, r0
 800bb7c:	4629      	mov	r1, r5
 800bb7e:	4658      	mov	r0, fp
 800bb80:	920c      	str	r2, [sp, #48]	; 0x30
 800bb82:	930a      	str	r3, [sp, #40]	; 0x28
 800bb84:	f000 f8f2 	bl	800bd6c <_Bfree>
 800bb88:	9b08      	ldr	r3, [sp, #32]
 800bb8a:	9a0c      	ldr	r2, [sp, #48]	; 0x30
 800bb8c:	ea43 0102 	orr.w	r1, r3, r2
 800bb90:	9b07      	ldr	r3, [sp, #28]
 800bb92:	430b      	orrs	r3, r1
 800bb94:	464d      	mov	r5, r9
 800bb96:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800bb98:	d10d      	bne.n	800bbb6 <_dtoa_r+0xa2e>
 800bb9a:	2b39      	cmp	r3, #57	; 0x39
 800bb9c:	d027      	beq.n	800bbee <_dtoa_r+0xa66>
 800bb9e:	9a06      	ldr	r2, [sp, #24]
 800bba0:	2a00      	cmp	r2, #0
 800bba2:	dd01      	ble.n	800bba8 <_dtoa_r+0xa20>
 800bba4:	9b04      	ldr	r3, [sp, #16]
 800bba6:	3331      	adds	r3, #49	; 0x31
 800bba8:	9a00      	ldr	r2, [sp, #0]
 800bbaa:	7013      	strb	r3, [r2, #0]
 800bbac:	e77b      	b.n	800baa6 <_dtoa_r+0x91e>
 800bbae:	4630      	mov	r0, r6
 800bbb0:	e7b8      	b.n	800bb24 <_dtoa_r+0x99c>
 800bbb2:	2201      	movs	r2, #1
 800bbb4:	e7e2      	b.n	800bb7c <_dtoa_r+0x9f4>
 800bbb6:	9906      	ldr	r1, [sp, #24]
 800bbb8:	2900      	cmp	r1, #0
 800bbba:	db04      	blt.n	800bbc6 <_dtoa_r+0xa3e>
 800bbbc:	9808      	ldr	r0, [sp, #32]
 800bbbe:	4301      	orrs	r1, r0
 800bbc0:	9807      	ldr	r0, [sp, #28]
 800bbc2:	4301      	orrs	r1, r0
 800bbc4:	d11e      	bne.n	800bc04 <_dtoa_r+0xa7c>
 800bbc6:	2a00      	cmp	r2, #0
 800bbc8:	ddee      	ble.n	800bba8 <_dtoa_r+0xa20>
 800bbca:	4641      	mov	r1, r8
 800bbcc:	2201      	movs	r2, #1
 800bbce:	4658      	mov	r0, fp
 800bbd0:	9306      	str	r3, [sp, #24]
 800bbd2:	f000 fa55 	bl	800c080 <__lshift>
 800bbd6:	4621      	mov	r1, r4
 800bbd8:	4680      	mov	r8, r0
 800bbda:	f000 faa4 	bl	800c126 <__mcmp>
 800bbde:	2800      	cmp	r0, #0
 800bbe0:	9b06      	ldr	r3, [sp, #24]
 800bbe2:	dc02      	bgt.n	800bbea <_dtoa_r+0xa62>
 800bbe4:	d1e0      	bne.n	800bba8 <_dtoa_r+0xa20>
 800bbe6:	07da      	lsls	r2, r3, #31
 800bbe8:	d5de      	bpl.n	800bba8 <_dtoa_r+0xa20>
 800bbea:	2b39      	cmp	r3, #57	; 0x39
 800bbec:	d1da      	bne.n	800bba4 <_dtoa_r+0xa1c>
 800bbee:	9a00      	ldr	r2, [sp, #0]
 800bbf0:	2339      	movs	r3, #57	; 0x39
 800bbf2:	7013      	strb	r3, [r2, #0]
 800bbf4:	f815 3c01 	ldrb.w	r3, [r5, #-1]
 800bbf8:	2b39      	cmp	r3, #57	; 0x39
 800bbfa:	f105 32ff 	add.w	r2, r5, #4294967295
 800bbfe:	d047      	beq.n	800bc90 <_dtoa_r+0xb08>
 800bc00:	3301      	adds	r3, #1
 800bc02:	e7d2      	b.n	800bbaa <_dtoa_r+0xa22>
 800bc04:	2a00      	cmp	r2, #0
 800bc06:	dd03      	ble.n	800bc10 <_dtoa_r+0xa88>
 800bc08:	2b39      	cmp	r3, #57	; 0x39
 800bc0a:	d0f0      	beq.n	800bbee <_dtoa_r+0xa66>
 800bc0c:	3301      	adds	r3, #1
 800bc0e:	e7cb      	b.n	800bba8 <_dtoa_r+0xa20>
 800bc10:	9a09      	ldr	r2, [sp, #36]	; 0x24
 800bc12:	f809 3c01 	strb.w	r3, [r9, #-1]
 800bc16:	4591      	cmp	r9, r2
 800bc18:	d021      	beq.n	800bc5e <_dtoa_r+0xad6>
 800bc1a:	4641      	mov	r1, r8
 800bc1c:	2300      	movs	r3, #0
 800bc1e:	220a      	movs	r2, #10
 800bc20:	4658      	mov	r0, fp
 800bc22:	f000 f8ba 	bl	800bd9a <__multadd>
 800bc26:	42b7      	cmp	r7, r6
 800bc28:	4680      	mov	r8, r0
 800bc2a:	f04f 0300 	mov.w	r3, #0
 800bc2e:	f04f 020a 	mov.w	r2, #10
 800bc32:	4639      	mov	r1, r7
 800bc34:	4658      	mov	r0, fp
 800bc36:	d106      	bne.n	800bc46 <_dtoa_r+0xabe>
 800bc38:	f000 f8af 	bl	800bd9a <__multadd>
 800bc3c:	4607      	mov	r7, r0
 800bc3e:	4606      	mov	r6, r0
 800bc40:	f109 0901 	add.w	r9, r9, #1
 800bc44:	e77b      	b.n	800bb3e <_dtoa_r+0x9b6>
 800bc46:	f000 f8a8 	bl	800bd9a <__multadd>
 800bc4a:	4631      	mov	r1, r6
 800bc4c:	4607      	mov	r7, r0
 800bc4e:	2300      	movs	r3, #0
 800bc50:	220a      	movs	r2, #10
 800bc52:	4658      	mov	r0, fp
 800bc54:	f000 f8a1 	bl	800bd9a <__multadd>
 800bc58:	4606      	mov	r6, r0
 800bc5a:	e7f1      	b.n	800bc40 <_dtoa_r+0xab8>
 800bc5c:	2700      	movs	r7, #0
 800bc5e:	4641      	mov	r1, r8
 800bc60:	2201      	movs	r2, #1
 800bc62:	4658      	mov	r0, fp
 800bc64:	9300      	str	r3, [sp, #0]
 800bc66:	f000 fa0b 	bl	800c080 <__lshift>
 800bc6a:	4621      	mov	r1, r4
 800bc6c:	4680      	mov	r8, r0
 800bc6e:	f000 fa5a 	bl	800c126 <__mcmp>
 800bc72:	2800      	cmp	r0, #0
 800bc74:	dcbe      	bgt.n	800bbf4 <_dtoa_r+0xa6c>
 800bc76:	d102      	bne.n	800bc7e <_dtoa_r+0xaf6>
 800bc78:	9b00      	ldr	r3, [sp, #0]
 800bc7a:	07db      	lsls	r3, r3, #31
 800bc7c:	d4ba      	bmi.n	800bbf4 <_dtoa_r+0xa6c>
 800bc7e:	f815 3c01 	ldrb.w	r3, [r5, #-1]
 800bc82:	2b30      	cmp	r3, #48	; 0x30
 800bc84:	f105 32ff 	add.w	r2, r5, #4294967295
 800bc88:	f47f af0d 	bne.w	800baa6 <_dtoa_r+0x91e>
 800bc8c:	4615      	mov	r5, r2
 800bc8e:	e7f6      	b.n	800bc7e <_dtoa_r+0xaf6>
 800bc90:	9b03      	ldr	r3, [sp, #12]
 800bc92:	4293      	cmp	r3, r2
 800bc94:	d104      	bne.n	800bca0 <_dtoa_r+0xb18>
 800bc96:	f10a 0a01 	add.w	sl, sl, #1
 800bc9a:	2331      	movs	r3, #49	; 0x31
 800bc9c:	9a03      	ldr	r2, [sp, #12]
 800bc9e:	e784      	b.n	800bbaa <_dtoa_r+0xa22>
 800bca0:	4615      	mov	r5, r2
 800bca2:	e7a7      	b.n	800bbf4 <_dtoa_r+0xa6c>
 800bca4:	4b13      	ldr	r3, [pc, #76]	; (800bcf4 <_dtoa_r+0xb6c>)
 800bca6:	f7ff bad6 	b.w	800b256 <_dtoa_r+0xce>
 800bcaa:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800bcac:	2b00      	cmp	r3, #0
 800bcae:	f47f aaae 	bne.w	800b20e <_dtoa_r+0x86>
 800bcb2:	4b11      	ldr	r3, [pc, #68]	; (800bcf8 <_dtoa_r+0xb70>)
 800bcb4:	f7ff bacf 	b.w	800b256 <_dtoa_r+0xce>
 800bcb8:	9b04      	ldr	r3, [sp, #16]
 800bcba:	2b00      	cmp	r3, #0
 800bcbc:	dc03      	bgt.n	800bcc6 <_dtoa_r+0xb3e>
 800bcbe:	9b08      	ldr	r3, [sp, #32]
 800bcc0:	2b02      	cmp	r3, #2
 800bcc2:	f73f aedc 	bgt.w	800ba7e <_dtoa_r+0x8f6>
 800bcc6:	9d03      	ldr	r5, [sp, #12]
 800bcc8:	4621      	mov	r1, r4
 800bcca:	4640      	mov	r0, r8
 800bccc:	f7ff f9d0 	bl	800b070 <quorem>
 800bcd0:	f100 0330 	add.w	r3, r0, #48	; 0x30
 800bcd4:	f805 3b01 	strb.w	r3, [r5], #1
 800bcd8:	9a03      	ldr	r2, [sp, #12]
 800bcda:	9904      	ldr	r1, [sp, #16]
 800bcdc:	1aaa      	subs	r2, r5, r2
 800bcde:	4291      	cmp	r1, r2
 800bce0:	ddbc      	ble.n	800bc5c <_dtoa_r+0xad4>
 800bce2:	4641      	mov	r1, r8
 800bce4:	2300      	movs	r3, #0
 800bce6:	220a      	movs	r2, #10
 800bce8:	4658      	mov	r0, fp
 800bcea:	f000 f856 	bl	800bd9a <__multadd>
 800bcee:	4680      	mov	r8, r0
 800bcf0:	e7ea      	b.n	800bcc8 <_dtoa_r+0xb40>
 800bcf2:	bf00      	nop
 800bcf4:	0800d37c 	.word	0x0800d37c
 800bcf8:	0800d37e 	.word	0x0800d37e

0800bcfc <_localeconv_r>:
 800bcfc:	4800      	ldr	r0, [pc, #0]	; (800bd00 <_localeconv_r+0x4>)
 800bcfe:	4770      	bx	lr
 800bd00:	200001d4 	.word	0x200001d4

0800bd04 <_Balloc>:
 800bd04:	b570      	push	{r4, r5, r6, lr}
 800bd06:	6a45      	ldr	r5, [r0, #36]	; 0x24
 800bd08:	4604      	mov	r4, r0
 800bd0a:	460e      	mov	r6, r1
 800bd0c:	b93d      	cbnz	r5, 800bd1e <_Balloc+0x1a>
 800bd0e:	2010      	movs	r0, #16
 800bd10:	f7fe fdf4 	bl	800a8fc <malloc>
 800bd14:	e9c0 5501 	strd	r5, r5, [r0, #4]
 800bd18:	6260      	str	r0, [r4, #36]	; 0x24
 800bd1a:	6005      	str	r5, [r0, #0]
 800bd1c:	60c5      	str	r5, [r0, #12]
 800bd1e:	6a65      	ldr	r5, [r4, #36]	; 0x24
 800bd20:	68eb      	ldr	r3, [r5, #12]
 800bd22:	b183      	cbz	r3, 800bd46 <_Balloc+0x42>
 800bd24:	6a63      	ldr	r3, [r4, #36]	; 0x24
 800bd26:	68db      	ldr	r3, [r3, #12]
 800bd28:	f853 0026 	ldr.w	r0, [r3, r6, lsl #2]
 800bd2c:	b9b8      	cbnz	r0, 800bd5e <_Balloc+0x5a>
 800bd2e:	2101      	movs	r1, #1
 800bd30:	fa01 f506 	lsl.w	r5, r1, r6
 800bd34:	1d6a      	adds	r2, r5, #5
 800bd36:	0092      	lsls	r2, r2, #2
 800bd38:	4620      	mov	r0, r4
 800bd3a:	f000 fab9 	bl	800c2b0 <_calloc_r>
 800bd3e:	b160      	cbz	r0, 800bd5a <_Balloc+0x56>
 800bd40:	e9c0 6501 	strd	r6, r5, [r0, #4]
 800bd44:	e00e      	b.n	800bd64 <_Balloc+0x60>
 800bd46:	2221      	movs	r2, #33	; 0x21
 800bd48:	2104      	movs	r1, #4
 800bd4a:	4620      	mov	r0, r4
 800bd4c:	f000 fab0 	bl	800c2b0 <_calloc_r>
 800bd50:	6a63      	ldr	r3, [r4, #36]	; 0x24
 800bd52:	60e8      	str	r0, [r5, #12]
 800bd54:	68db      	ldr	r3, [r3, #12]
 800bd56:	2b00      	cmp	r3, #0
 800bd58:	d1e4      	bne.n	800bd24 <_Balloc+0x20>
 800bd5a:	2000      	movs	r0, #0
 800bd5c:	bd70      	pop	{r4, r5, r6, pc}
 800bd5e:	6802      	ldr	r2, [r0, #0]
 800bd60:	f843 2026 	str.w	r2, [r3, r6, lsl #2]
 800bd64:	2300      	movs	r3, #0
 800bd66:	e9c0 3303 	strd	r3, r3, [r0, #12]
 800bd6a:	e7f7      	b.n	800bd5c <_Balloc+0x58>

0800bd6c <_Bfree>:
 800bd6c:	b570      	push	{r4, r5, r6, lr}
 800bd6e:	6a44      	ldr	r4, [r0, #36]	; 0x24
 800bd70:	4606      	mov	r6, r0
 800bd72:	460d      	mov	r5, r1
 800bd74:	b93c      	cbnz	r4, 800bd86 <_Bfree+0x1a>
 800bd76:	2010      	movs	r0, #16
 800bd78:	f7fe fdc0 	bl	800a8fc <malloc>
 800bd7c:	e9c0 4401 	strd	r4, r4, [r0, #4]
 800bd80:	6270      	str	r0, [r6, #36]	; 0x24
 800bd82:	6004      	str	r4, [r0, #0]
 800bd84:	60c4      	str	r4, [r0, #12]
 800bd86:	b13d      	cbz	r5, 800bd98 <_Bfree+0x2c>
 800bd88:	6a73      	ldr	r3, [r6, #36]	; 0x24
 800bd8a:	686a      	ldr	r2, [r5, #4]
 800bd8c:	68db      	ldr	r3, [r3, #12]
 800bd8e:	f853 1022 	ldr.w	r1, [r3, r2, lsl #2]
 800bd92:	6029      	str	r1, [r5, #0]
 800bd94:	f843 5022 	str.w	r5, [r3, r2, lsl #2]
 800bd98:	bd70      	pop	{r4, r5, r6, pc}

0800bd9a <__multadd>:
 800bd9a:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
 800bd9e:	690d      	ldr	r5, [r1, #16]
 800bda0:	461f      	mov	r7, r3
 800bda2:	4606      	mov	r6, r0
 800bda4:	460c      	mov	r4, r1
 800bda6:	f101 0c14 	add.w	ip, r1, #20
 800bdaa:	2300      	movs	r3, #0
 800bdac:	f8dc 0000 	ldr.w	r0, [ip]
 800bdb0:	b281      	uxth	r1, r0
 800bdb2:	fb02 7101 	mla	r1, r2, r1, r7
 800bdb6:	0c0f      	lsrs	r7, r1, #16
 800bdb8:	0c00      	lsrs	r0, r0, #16
 800bdba:	fb02 7000 	mla	r0, r2, r0, r7
 800bdbe:	b289      	uxth	r1, r1
 800bdc0:	3301      	adds	r3, #1
 800bdc2:	eb01 4100 	add.w	r1, r1, r0, lsl #16
 800bdc6:	429d      	cmp	r5, r3
 800bdc8:	ea4f 4710 	mov.w	r7, r0, lsr #16
 800bdcc:	f84c 1b04 	str.w	r1, [ip], #4
 800bdd0:	dcec      	bgt.n	800bdac <__multadd+0x12>
 800bdd2:	b1d7      	cbz	r7, 800be0a <__multadd+0x70>
 800bdd4:	68a3      	ldr	r3, [r4, #8]
 800bdd6:	42ab      	cmp	r3, r5
 800bdd8:	dc12      	bgt.n	800be00 <__multadd+0x66>
 800bdda:	6861      	ldr	r1, [r4, #4]
 800bddc:	4630      	mov	r0, r6
 800bdde:	3101      	adds	r1, #1
 800bde0:	f7ff ff90 	bl	800bd04 <_Balloc>
 800bde4:	6922      	ldr	r2, [r4, #16]
 800bde6:	3202      	adds	r2, #2
 800bde8:	f104 010c 	add.w	r1, r4, #12
 800bdec:	4680      	mov	r8, r0
 800bdee:	0092      	lsls	r2, r2, #2
 800bdf0:	300c      	adds	r0, #12
 800bdf2:	f7fe fd8b 	bl	800a90c <memcpy>
 800bdf6:	4621      	mov	r1, r4
 800bdf8:	4630      	mov	r0, r6
 800bdfa:	f7ff ffb7 	bl	800bd6c <_Bfree>
 800bdfe:	4644      	mov	r4, r8
 800be00:	eb04 0385 	add.w	r3, r4, r5, lsl #2
 800be04:	3501      	adds	r5, #1
 800be06:	615f      	str	r7, [r3, #20]
 800be08:	6125      	str	r5, [r4, #16]
 800be0a:	4620      	mov	r0, r4
 800be0c:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

0800be10 <__hi0bits>:
 800be10:	0c02      	lsrs	r2, r0, #16
 800be12:	0412      	lsls	r2, r2, #16
 800be14:	4603      	mov	r3, r0
 800be16:	b9ca      	cbnz	r2, 800be4c <__hi0bits+0x3c>
 800be18:	0403      	lsls	r3, r0, #16
 800be1a:	2010      	movs	r0, #16
 800be1c:	f013 4f7f 	tst.w	r3, #4278190080	; 0xff000000
 800be20:	bf04      	itt	eq
 800be22:	021b      	lsleq	r3, r3, #8
 800be24:	3008      	addeq	r0, #8
 800be26:	f013 4f70 	tst.w	r3, #4026531840	; 0xf0000000
 800be2a:	bf04      	itt	eq
 800be2c:	011b      	lsleq	r3, r3, #4
 800be2e:	3004      	addeq	r0, #4
 800be30:	f013 4f40 	tst.w	r3, #3221225472	; 0xc0000000
 800be34:	bf04      	itt	eq
 800be36:	009b      	lsleq	r3, r3, #2
 800be38:	3002      	addeq	r0, #2
 800be3a:	2b00      	cmp	r3, #0
 800be3c:	db05      	blt.n	800be4a <__hi0bits+0x3a>
 800be3e:	f013 4f80 	tst.w	r3, #1073741824	; 0x40000000
 800be42:	f100 0001 	add.w	r0, r0, #1
 800be46:	bf08      	it	eq
 800be48:	2020      	moveq	r0, #32
 800be4a:	4770      	bx	lr
 800be4c:	2000      	movs	r0, #0
 800be4e:	e7e5      	b.n	800be1c <__hi0bits+0xc>

0800be50 <__lo0bits>:
 800be50:	6803      	ldr	r3, [r0, #0]
 800be52:	f013 0207 	ands.w	r2, r3, #7
 800be56:	4601      	mov	r1, r0
 800be58:	d00b      	beq.n	800be72 <__lo0bits+0x22>
 800be5a:	07da      	lsls	r2, r3, #31
 800be5c:	d424      	bmi.n	800bea8 <__lo0bits+0x58>
 800be5e:	0798      	lsls	r0, r3, #30
 800be60:	bf49      	itett	mi
 800be62:	085b      	lsrmi	r3, r3, #1
 800be64:	089b      	lsrpl	r3, r3, #2
 800be66:	2001      	movmi	r0, #1
 800be68:	600b      	strmi	r3, [r1, #0]
 800be6a:	bf5c      	itt	pl
 800be6c:	600b      	strpl	r3, [r1, #0]
 800be6e:	2002      	movpl	r0, #2
 800be70:	4770      	bx	lr
 800be72:	b298      	uxth	r0, r3
 800be74:	b9b0      	cbnz	r0, 800bea4 <__lo0bits+0x54>
 800be76:	0c1b      	lsrs	r3, r3, #16
 800be78:	2010      	movs	r0, #16
 800be7a:	f013 0fff 	tst.w	r3, #255	; 0xff
 800be7e:	bf04      	itt	eq
 800be80:	0a1b      	lsreq	r3, r3, #8
 800be82:	3008      	addeq	r0, #8
 800be84:	071a      	lsls	r2, r3, #28
 800be86:	bf04      	itt	eq
 800be88:	091b      	lsreq	r3, r3, #4
 800be8a:	3004      	addeq	r0, #4
 800be8c:	079a      	lsls	r2, r3, #30
 800be8e:	bf04      	itt	eq
 800be90:	089b      	lsreq	r3, r3, #2
 800be92:	3002      	addeq	r0, #2
 800be94:	07da      	lsls	r2, r3, #31
 800be96:	d403      	bmi.n	800bea0 <__lo0bits+0x50>
 800be98:	085b      	lsrs	r3, r3, #1
 800be9a:	f100 0001 	add.w	r0, r0, #1
 800be9e:	d005      	beq.n	800beac <__lo0bits+0x5c>
 800bea0:	600b      	str	r3, [r1, #0]
 800bea2:	4770      	bx	lr
 800bea4:	4610      	mov	r0, r2
 800bea6:	e7e8      	b.n	800be7a <__lo0bits+0x2a>
 800bea8:	2000      	movs	r0, #0
 800beaa:	4770      	bx	lr
 800beac:	2020      	movs	r0, #32
 800beae:	4770      	bx	lr

0800beb0 <__i2b>:
 800beb0:	b510      	push	{r4, lr}
 800beb2:	460c      	mov	r4, r1
 800beb4:	2101      	movs	r1, #1
 800beb6:	f7ff ff25 	bl	800bd04 <_Balloc>
 800beba:	2201      	movs	r2, #1
 800bebc:	6144      	str	r4, [r0, #20]
 800bebe:	6102      	str	r2, [r0, #16]
 800bec0:	bd10      	pop	{r4, pc}

0800bec2 <__multiply>:
 800bec2:	e92d 4ff7 	stmdb	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800bec6:	4690      	mov	r8, r2
 800bec8:	690a      	ldr	r2, [r1, #16]
 800beca:	f8d8 3010 	ldr.w	r3, [r8, #16]
 800bece:	429a      	cmp	r2, r3
 800bed0:	bfb8      	it	lt
 800bed2:	460b      	movlt	r3, r1
 800bed4:	460c      	mov	r4, r1
 800bed6:	bfbc      	itt	lt
 800bed8:	4644      	movlt	r4, r8
 800beda:	4698      	movlt	r8, r3
 800bedc:	6927      	ldr	r7, [r4, #16]
 800bede:	f8d8 9010 	ldr.w	r9, [r8, #16]
 800bee2:	68a3      	ldr	r3, [r4, #8]
 800bee4:	6861      	ldr	r1, [r4, #4]
 800bee6:	eb07 0609 	add.w	r6, r7, r9
 800beea:	42b3      	cmp	r3, r6
 800beec:	bfb8      	it	lt
 800beee:	3101      	addlt	r1, #1
 800bef0:	f7ff ff08 	bl	800bd04 <_Balloc>
 800bef4:	f100 0514 	add.w	r5, r0, #20
 800bef8:	eb05 0e86 	add.w	lr, r5, r6, lsl #2
 800befc:	462b      	mov	r3, r5
 800befe:	2200      	movs	r2, #0
 800bf00:	4573      	cmp	r3, lr
 800bf02:	d316      	bcc.n	800bf32 <__multiply+0x70>
 800bf04:	f104 0314 	add.w	r3, r4, #20
 800bf08:	f108 0214 	add.w	r2, r8, #20
 800bf0c:	eb03 0787 	add.w	r7, r3, r7, lsl #2
 800bf10:	eb02 0389 	add.w	r3, r2, r9, lsl #2
 800bf14:	9300      	str	r3, [sp, #0]
 800bf16:	9b00      	ldr	r3, [sp, #0]
 800bf18:	9201      	str	r2, [sp, #4]
 800bf1a:	4293      	cmp	r3, r2
 800bf1c:	d80c      	bhi.n	800bf38 <__multiply+0x76>
 800bf1e:	2e00      	cmp	r6, #0
 800bf20:	dd03      	ble.n	800bf2a <__multiply+0x68>
 800bf22:	f85e 3d04 	ldr.w	r3, [lr, #-4]!
 800bf26:	2b00      	cmp	r3, #0
 800bf28:	d059      	beq.n	800bfde <__multiply+0x11c>
 800bf2a:	6106      	str	r6, [r0, #16]
 800bf2c:	b003      	add	sp, #12
 800bf2e:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800bf32:	f843 2b04 	str.w	r2, [r3], #4
 800bf36:	e7e3      	b.n	800bf00 <__multiply+0x3e>
 800bf38:	f8b2 a000 	ldrh.w	sl, [r2]
 800bf3c:	f1ba 0f00 	cmp.w	sl, #0
 800bf40:	d023      	beq.n	800bf8a <__multiply+0xc8>
 800bf42:	f104 0914 	add.w	r9, r4, #20
 800bf46:	46ac      	mov	ip, r5
 800bf48:	f04f 0800 	mov.w	r8, #0
 800bf4c:	f859 1b04 	ldr.w	r1, [r9], #4
 800bf50:	f8dc b000 	ldr.w	fp, [ip]
 800bf54:	b28b      	uxth	r3, r1
 800bf56:	fa1f fb8b 	uxth.w	fp, fp
 800bf5a:	fb0a b303 	mla	r3, sl, r3, fp
 800bf5e:	ea4f 4b11 	mov.w	fp, r1, lsr #16
 800bf62:	f8dc 1000 	ldr.w	r1, [ip]
 800bf66:	4443      	add	r3, r8
 800bf68:	ea4f 4811 	mov.w	r8, r1, lsr #16
 800bf6c:	fb0a 810b 	mla	r1, sl, fp, r8
 800bf70:	eb01 4113 	add.w	r1, r1, r3, lsr #16
 800bf74:	b29b      	uxth	r3, r3
 800bf76:	ea43 4301 	orr.w	r3, r3, r1, lsl #16
 800bf7a:	454f      	cmp	r7, r9
 800bf7c:	ea4f 4811 	mov.w	r8, r1, lsr #16
 800bf80:	f84c 3b04 	str.w	r3, [ip], #4
 800bf84:	d8e2      	bhi.n	800bf4c <__multiply+0x8a>
 800bf86:	f8cc 8000 	str.w	r8, [ip]
 800bf8a:	9b01      	ldr	r3, [sp, #4]
 800bf8c:	f8b3 9002 	ldrh.w	r9, [r3, #2]
 800bf90:	3204      	adds	r2, #4
 800bf92:	f1b9 0f00 	cmp.w	r9, #0
 800bf96:	d020      	beq.n	800bfda <__multiply+0x118>
 800bf98:	682b      	ldr	r3, [r5, #0]
 800bf9a:	f104 0814 	add.w	r8, r4, #20
 800bf9e:	46ac      	mov	ip, r5
 800bfa0:	f04f 0a00 	mov.w	sl, #0
 800bfa4:	f8b8 1000 	ldrh.w	r1, [r8]
 800bfa8:	f8bc b002 	ldrh.w	fp, [ip, #2]
 800bfac:	fb09 b101 	mla	r1, r9, r1, fp
 800bfb0:	448a      	add	sl, r1
 800bfb2:	b29b      	uxth	r3, r3
 800bfb4:	ea43 430a 	orr.w	r3, r3, sl, lsl #16
 800bfb8:	f84c 3b04 	str.w	r3, [ip], #4
 800bfbc:	f858 3b04 	ldr.w	r3, [r8], #4
 800bfc0:	f8bc 1000 	ldrh.w	r1, [ip]
 800bfc4:	0c1b      	lsrs	r3, r3, #16
 800bfc6:	fb09 1303 	mla	r3, r9, r3, r1
 800bfca:	eb03 431a 	add.w	r3, r3, sl, lsr #16
 800bfce:	4547      	cmp	r7, r8
 800bfd0:	ea4f 4a13 	mov.w	sl, r3, lsr #16
 800bfd4:	d8e6      	bhi.n	800bfa4 <__multiply+0xe2>
 800bfd6:	f8cc 3000 	str.w	r3, [ip]
 800bfda:	3504      	adds	r5, #4
 800bfdc:	e79b      	b.n	800bf16 <__multiply+0x54>
 800bfde:	3e01      	subs	r6, #1
 800bfe0:	e79d      	b.n	800bf1e <__multiply+0x5c>
	...

0800bfe4 <__pow5mult>:
 800bfe4:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
 800bfe8:	4615      	mov	r5, r2
 800bfea:	f012 0203 	ands.w	r2, r2, #3
 800bfee:	4606      	mov	r6, r0
 800bff0:	460f      	mov	r7, r1
 800bff2:	d007      	beq.n	800c004 <__pow5mult+0x20>
 800bff4:	3a01      	subs	r2, #1
 800bff6:	4c21      	ldr	r4, [pc, #132]	; (800c07c <__pow5mult+0x98>)
 800bff8:	2300      	movs	r3, #0
 800bffa:	f854 2022 	ldr.w	r2, [r4, r2, lsl #2]
 800bffe:	f7ff fecc 	bl	800bd9a <__multadd>
 800c002:	4607      	mov	r7, r0
 800c004:	10ad      	asrs	r5, r5, #2
 800c006:	d035      	beq.n	800c074 <__pow5mult+0x90>
 800c008:	6a74      	ldr	r4, [r6, #36]	; 0x24
 800c00a:	b93c      	cbnz	r4, 800c01c <__pow5mult+0x38>
 800c00c:	2010      	movs	r0, #16
 800c00e:	f7fe fc75 	bl	800a8fc <malloc>
 800c012:	e9c0 4401 	strd	r4, r4, [r0, #4]
 800c016:	6270      	str	r0, [r6, #36]	; 0x24
 800c018:	6004      	str	r4, [r0, #0]
 800c01a:	60c4      	str	r4, [r0, #12]
 800c01c:	f8d6 8024 	ldr.w	r8, [r6, #36]	; 0x24
 800c020:	f8d8 4008 	ldr.w	r4, [r8, #8]
 800c024:	b94c      	cbnz	r4, 800c03a <__pow5mult+0x56>
 800c026:	f240 2171 	movw	r1, #625	; 0x271
 800c02a:	4630      	mov	r0, r6
 800c02c:	f7ff ff40 	bl	800beb0 <__i2b>
 800c030:	2300      	movs	r3, #0
 800c032:	f8c8 0008 	str.w	r0, [r8, #8]
 800c036:	4604      	mov	r4, r0
 800c038:	6003      	str	r3, [r0, #0]
 800c03a:	f04f 0800 	mov.w	r8, #0
 800c03e:	07eb      	lsls	r3, r5, #31
 800c040:	d50a      	bpl.n	800c058 <__pow5mult+0x74>
 800c042:	4639      	mov	r1, r7
 800c044:	4622      	mov	r2, r4
 800c046:	4630      	mov	r0, r6
 800c048:	f7ff ff3b 	bl	800bec2 <__multiply>
 800c04c:	4639      	mov	r1, r7
 800c04e:	4681      	mov	r9, r0
 800c050:	4630      	mov	r0, r6
 800c052:	f7ff fe8b 	bl	800bd6c <_Bfree>
 800c056:	464f      	mov	r7, r9
 800c058:	106d      	asrs	r5, r5, #1
 800c05a:	d00b      	beq.n	800c074 <__pow5mult+0x90>
 800c05c:	6820      	ldr	r0, [r4, #0]
 800c05e:	b938      	cbnz	r0, 800c070 <__pow5mult+0x8c>
 800c060:	4622      	mov	r2, r4
 800c062:	4621      	mov	r1, r4
 800c064:	4630      	mov	r0, r6
 800c066:	f7ff ff2c 	bl	800bec2 <__multiply>
 800c06a:	6020      	str	r0, [r4, #0]
 800c06c:	f8c0 8000 	str.w	r8, [r0]
 800c070:	4604      	mov	r4, r0
 800c072:	e7e4      	b.n	800c03e <__pow5mult+0x5a>
 800c074:	4638      	mov	r0, r7
 800c076:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
 800c07a:	bf00      	nop
 800c07c:	0800d4e0 	.word	0x0800d4e0

0800c080 <__lshift>:
 800c080:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 800c084:	460c      	mov	r4, r1
 800c086:	ea4f 1a62 	mov.w	sl, r2, asr #5
 800c08a:	6923      	ldr	r3, [r4, #16]
 800c08c:	6849      	ldr	r1, [r1, #4]
 800c08e:	eb0a 0903 	add.w	r9, sl, r3
 800c092:	68a3      	ldr	r3, [r4, #8]
 800c094:	4607      	mov	r7, r0
 800c096:	4616      	mov	r6, r2
 800c098:	f109 0501 	add.w	r5, r9, #1
 800c09c:	42ab      	cmp	r3, r5
 800c09e:	db33      	blt.n	800c108 <__lshift+0x88>
 800c0a0:	4638      	mov	r0, r7
 800c0a2:	f7ff fe2f 	bl	800bd04 <_Balloc>
 800c0a6:	2300      	movs	r3, #0
 800c0a8:	4680      	mov	r8, r0
 800c0aa:	f100 0114 	add.w	r1, r0, #20
 800c0ae:	f100 0210 	add.w	r2, r0, #16
 800c0b2:	4618      	mov	r0, r3
 800c0b4:	4553      	cmp	r3, sl
 800c0b6:	db2a      	blt.n	800c10e <__lshift+0x8e>
 800c0b8:	6920      	ldr	r0, [r4, #16]
 800c0ba:	ea2a 7aea 	bic.w	sl, sl, sl, asr #31
 800c0be:	f104 0314 	add.w	r3, r4, #20
 800c0c2:	f016 021f 	ands.w	r2, r6, #31
 800c0c6:	eb01 018a 	add.w	r1, r1, sl, lsl #2
 800c0ca:	eb03 0c80 	add.w	ip, r3, r0, lsl #2
 800c0ce:	d022      	beq.n	800c116 <__lshift+0x96>
 800c0d0:	f1c2 0e20 	rsb	lr, r2, #32
 800c0d4:	2000      	movs	r0, #0
 800c0d6:	681e      	ldr	r6, [r3, #0]
 800c0d8:	4096      	lsls	r6, r2
 800c0da:	4330      	orrs	r0, r6
 800c0dc:	f841 0b04 	str.w	r0, [r1], #4
 800c0e0:	f853 0b04 	ldr.w	r0, [r3], #4
 800c0e4:	459c      	cmp	ip, r3
 800c0e6:	fa20 f00e 	lsr.w	r0, r0, lr
 800c0ea:	d8f4      	bhi.n	800c0d6 <__lshift+0x56>
 800c0ec:	6008      	str	r0, [r1, #0]
 800c0ee:	b108      	cbz	r0, 800c0f4 <__lshift+0x74>
 800c0f0:	f109 0502 	add.w	r5, r9, #2
 800c0f4:	3d01      	subs	r5, #1
 800c0f6:	4638      	mov	r0, r7
 800c0f8:	f8c8 5010 	str.w	r5, [r8, #16]
 800c0fc:	4621      	mov	r1, r4
 800c0fe:	f7ff fe35 	bl	800bd6c <_Bfree>
 800c102:	4640      	mov	r0, r8
 800c104:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 800c108:	3101      	adds	r1, #1
 800c10a:	005b      	lsls	r3, r3, #1
 800c10c:	e7c6      	b.n	800c09c <__lshift+0x1c>
 800c10e:	f842 0f04 	str.w	r0, [r2, #4]!
 800c112:	3301      	adds	r3, #1
 800c114:	e7ce      	b.n	800c0b4 <__lshift+0x34>
 800c116:	3904      	subs	r1, #4
 800c118:	f853 2b04 	ldr.w	r2, [r3], #4
 800c11c:	f841 2f04 	str.w	r2, [r1, #4]!
 800c120:	459c      	cmp	ip, r3
 800c122:	d8f9      	bhi.n	800c118 <__lshift+0x98>
 800c124:	e7e6      	b.n	800c0f4 <__lshift+0x74>

0800c126 <__mcmp>:
 800c126:	6903      	ldr	r3, [r0, #16]
 800c128:	690a      	ldr	r2, [r1, #16]
 800c12a:	1a9b      	subs	r3, r3, r2
 800c12c:	b530      	push	{r4, r5, lr}
 800c12e:	d10c      	bne.n	800c14a <__mcmp+0x24>
 800c130:	0092      	lsls	r2, r2, #2
 800c132:	3014      	adds	r0, #20
 800c134:	3114      	adds	r1, #20
 800c136:	1884      	adds	r4, r0, r2
 800c138:	4411      	add	r1, r2
 800c13a:	f854 5d04 	ldr.w	r5, [r4, #-4]!
 800c13e:	f851 2d04 	ldr.w	r2, [r1, #-4]!
 800c142:	4295      	cmp	r5, r2
 800c144:	d003      	beq.n	800c14e <__mcmp+0x28>
 800c146:	d305      	bcc.n	800c154 <__mcmp+0x2e>
 800c148:	2301      	movs	r3, #1
 800c14a:	4618      	mov	r0, r3
 800c14c:	bd30      	pop	{r4, r5, pc}
 800c14e:	42a0      	cmp	r0, r4
 800c150:	d3f3      	bcc.n	800c13a <__mcmp+0x14>
 800c152:	e7fa      	b.n	800c14a <__mcmp+0x24>
 800c154:	f04f 33ff 	mov.w	r3, #4294967295
 800c158:	e7f7      	b.n	800c14a <__mcmp+0x24>

0800c15a <__mdiff>:
 800c15a:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 800c15e:	460d      	mov	r5, r1
 800c160:	4607      	mov	r7, r0
 800c162:	4611      	mov	r1, r2
 800c164:	4628      	mov	r0, r5
 800c166:	4614      	mov	r4, r2
 800c168:	f7ff ffdd 	bl	800c126 <__mcmp>
 800c16c:	1e06      	subs	r6, r0, #0
 800c16e:	d108      	bne.n	800c182 <__mdiff+0x28>
 800c170:	4631      	mov	r1, r6
 800c172:	4638      	mov	r0, r7
 800c174:	f7ff fdc6 	bl	800bd04 <_Balloc>
 800c178:	2301      	movs	r3, #1
 800c17a:	e9c0 3604 	strd	r3, r6, [r0, #16]
 800c17e:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 800c182:	bfa4      	itt	ge
 800c184:	4623      	movge	r3, r4
 800c186:	462c      	movge	r4, r5
 800c188:	4638      	mov	r0, r7
 800c18a:	6861      	ldr	r1, [r4, #4]
 800c18c:	bfa6      	itte	ge
 800c18e:	461d      	movge	r5, r3
 800c190:	2600      	movge	r6, #0
 800c192:	2601      	movlt	r6, #1
 800c194:	f7ff fdb6 	bl	800bd04 <_Balloc>
 800c198:	692b      	ldr	r3, [r5, #16]
 800c19a:	60c6      	str	r6, [r0, #12]
 800c19c:	6926      	ldr	r6, [r4, #16]
 800c19e:	f105 0914 	add.w	r9, r5, #20
 800c1a2:	f104 0214 	add.w	r2, r4, #20
 800c1a6:	eb02 0786 	add.w	r7, r2, r6, lsl #2
 800c1aa:	eb09 0883 	add.w	r8, r9, r3, lsl #2
 800c1ae:	f100 0514 	add.w	r5, r0, #20
 800c1b2:	f04f 0e00 	mov.w	lr, #0
 800c1b6:	f852 ab04 	ldr.w	sl, [r2], #4
 800c1ba:	f859 4b04 	ldr.w	r4, [r9], #4
 800c1be:	fa1e f18a 	uxtah	r1, lr, sl
 800c1c2:	b2a3      	uxth	r3, r4
 800c1c4:	1ac9      	subs	r1, r1, r3
 800c1c6:	0c23      	lsrs	r3, r4, #16
 800c1c8:	ebc3 431a 	rsb	r3, r3, sl, lsr #16
 800c1cc:	eb03 4321 	add.w	r3, r3, r1, asr #16
 800c1d0:	b289      	uxth	r1, r1
 800c1d2:	ea4f 4e23 	mov.w	lr, r3, asr #16
 800c1d6:	45c8      	cmp	r8, r9
 800c1d8:	ea41 4303 	orr.w	r3, r1, r3, lsl #16
 800c1dc:	4694      	mov	ip, r2
 800c1de:	f845 3b04 	str.w	r3, [r5], #4
 800c1e2:	d8e8      	bhi.n	800c1b6 <__mdiff+0x5c>
 800c1e4:	45bc      	cmp	ip, r7
 800c1e6:	d304      	bcc.n	800c1f2 <__mdiff+0x98>
 800c1e8:	f855 3d04 	ldr.w	r3, [r5, #-4]!
 800c1ec:	b183      	cbz	r3, 800c210 <__mdiff+0xb6>
 800c1ee:	6106      	str	r6, [r0, #16]
 800c1f0:	e7c5      	b.n	800c17e <__mdiff+0x24>
 800c1f2:	f85c 1b04 	ldr.w	r1, [ip], #4
 800c1f6:	fa1e f381 	uxtah	r3, lr, r1
 800c1fa:	141a      	asrs	r2, r3, #16
 800c1fc:	eb02 4211 	add.w	r2, r2, r1, lsr #16
 800c200:	b29b      	uxth	r3, r3
 800c202:	ea43 4302 	orr.w	r3, r3, r2, lsl #16
 800c206:	ea4f 4e22 	mov.w	lr, r2, asr #16
 800c20a:	f845 3b04 	str.w	r3, [r5], #4
 800c20e:	e7e9      	b.n	800c1e4 <__mdiff+0x8a>
 800c210:	3e01      	subs	r6, #1
 800c212:	e7e9      	b.n	800c1e8 <__mdiff+0x8e>

0800c214 <__d2b>:
 800c214:	e92d 43f7 	stmdb	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, lr}
 800c218:	460e      	mov	r6, r1
 800c21a:	2101      	movs	r1, #1
 800c21c:	ec59 8b10 	vmov	r8, r9, d0
 800c220:	4615      	mov	r5, r2
 800c222:	f7ff fd6f 	bl	800bd04 <_Balloc>
 800c226:	f3c9 540a 	ubfx	r4, r9, #20, #11
 800c22a:	4607      	mov	r7, r0
 800c22c:	f3c9 0313 	ubfx	r3, r9, #0, #20
 800c230:	bb2c      	cbnz	r4, 800c27e <__d2b+0x6a>
 800c232:	9301      	str	r3, [sp, #4]
 800c234:	f1b8 0300 	subs.w	r3, r8, #0
 800c238:	d026      	beq.n	800c288 <__d2b+0x74>
 800c23a:	4668      	mov	r0, sp
 800c23c:	9300      	str	r3, [sp, #0]
 800c23e:	f7ff fe07 	bl	800be50 <__lo0bits>
 800c242:	9900      	ldr	r1, [sp, #0]
 800c244:	b1f0      	cbz	r0, 800c284 <__d2b+0x70>
 800c246:	9a01      	ldr	r2, [sp, #4]
 800c248:	f1c0 0320 	rsb	r3, r0, #32
 800c24c:	fa02 f303 	lsl.w	r3, r2, r3
 800c250:	430b      	orrs	r3, r1
 800c252:	40c2      	lsrs	r2, r0
 800c254:	617b      	str	r3, [r7, #20]
 800c256:	9201      	str	r2, [sp, #4]
 800c258:	9b01      	ldr	r3, [sp, #4]
 800c25a:	61bb      	str	r3, [r7, #24]
 800c25c:	2b00      	cmp	r3, #0
 800c25e:	bf14      	ite	ne
 800c260:	2102      	movne	r1, #2
 800c262:	2101      	moveq	r1, #1
 800c264:	6139      	str	r1, [r7, #16]
 800c266:	b1c4      	cbz	r4, 800c29a <__d2b+0x86>
 800c268:	f2a4 4433 	subw	r4, r4, #1075	; 0x433
 800c26c:	4404      	add	r4, r0
 800c26e:	6034      	str	r4, [r6, #0]
 800c270:	f1c0 0035 	rsb	r0, r0, #53	; 0x35
 800c274:	6028      	str	r0, [r5, #0]
 800c276:	4638      	mov	r0, r7
 800c278:	b003      	add	sp, #12
 800c27a:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 800c27e:	f443 1380 	orr.w	r3, r3, #1048576	; 0x100000
 800c282:	e7d6      	b.n	800c232 <__d2b+0x1e>
 800c284:	6179      	str	r1, [r7, #20]
 800c286:	e7e7      	b.n	800c258 <__d2b+0x44>
 800c288:	a801      	add	r0, sp, #4
 800c28a:	f7ff fde1 	bl	800be50 <__lo0bits>
 800c28e:	9b01      	ldr	r3, [sp, #4]
 800c290:	617b      	str	r3, [r7, #20]
 800c292:	2101      	movs	r1, #1
 800c294:	6139      	str	r1, [r7, #16]
 800c296:	3020      	adds	r0, #32
 800c298:	e7e5      	b.n	800c266 <__d2b+0x52>
 800c29a:	eb07 0381 	add.w	r3, r7, r1, lsl #2
 800c29e:	f2a0 4032 	subw	r0, r0, #1074	; 0x432
 800c2a2:	6030      	str	r0, [r6, #0]
 800c2a4:	6918      	ldr	r0, [r3, #16]
 800c2a6:	f7ff fdb3 	bl	800be10 <__hi0bits>
 800c2aa:	ebc0 1041 	rsb	r0, r0, r1, lsl #5
 800c2ae:	e7e1      	b.n	800c274 <__d2b+0x60>

0800c2b0 <_calloc_r>:
 800c2b0:	b538      	push	{r3, r4, r5, lr}
 800c2b2:	fb02 f401 	mul.w	r4, r2, r1
 800c2b6:	4621      	mov	r1, r4
 800c2b8:	f7f4 ff70 	bl	800119c <_malloc_r>
 800c2bc:	4605      	mov	r5, r0
 800c2be:	b118      	cbz	r0, 800c2c8 <_calloc_r+0x18>
 800c2c0:	4622      	mov	r2, r4
 800c2c2:	2100      	movs	r1, #0
 800c2c4:	f7fe fb30 	bl	800a928 <memset>
 800c2c8:	4628      	mov	r0, r5
 800c2ca:	bd38      	pop	{r3, r4, r5, pc}

0800c2cc <__ascii_mbtowc>:
 800c2cc:	b082      	sub	sp, #8
 800c2ce:	b901      	cbnz	r1, 800c2d2 <__ascii_mbtowc+0x6>
 800c2d0:	a901      	add	r1, sp, #4
 800c2d2:	b142      	cbz	r2, 800c2e6 <__ascii_mbtowc+0x1a>
 800c2d4:	b14b      	cbz	r3, 800c2ea <__ascii_mbtowc+0x1e>
 800c2d6:	7813      	ldrb	r3, [r2, #0]
 800c2d8:	600b      	str	r3, [r1, #0]
 800c2da:	7812      	ldrb	r2, [r2, #0]
 800c2dc:	1c10      	adds	r0, r2, #0
 800c2de:	bf18      	it	ne
 800c2e0:	2001      	movne	r0, #1
 800c2e2:	b002      	add	sp, #8
 800c2e4:	4770      	bx	lr
 800c2e6:	4610      	mov	r0, r2
 800c2e8:	e7fb      	b.n	800c2e2 <__ascii_mbtowc+0x16>
 800c2ea:	f06f 0001 	mvn.w	r0, #1
 800c2ee:	e7f8      	b.n	800c2e2 <__ascii_mbtowc+0x16>

0800c2f0 <__ascii_wctomb>:
 800c2f0:	b149      	cbz	r1, 800c306 <__ascii_wctomb+0x16>
 800c2f2:	2aff      	cmp	r2, #255	; 0xff
 800c2f4:	bf85      	ittet	hi
 800c2f6:	238a      	movhi	r3, #138	; 0x8a
 800c2f8:	6003      	strhi	r3, [r0, #0]
 800c2fa:	700a      	strbls	r2, [r1, #0]
 800c2fc:	f04f 30ff 	movhi.w	r0, #4294967295
 800c300:	bf98      	it	ls
 800c302:	2001      	movls	r0, #1
 800c304:	4770      	bx	lr
 800c306:	4608      	mov	r0, r1
 800c308:	4770      	bx	lr
	...

0800c30c <cosf>:
 800c30c:	b500      	push	{lr}
 800c30e:	ee10 3a10 	vmov	r3, s0
 800c312:	4a20      	ldr	r2, [pc, #128]	; (800c394 <cosf+0x88>)
 800c314:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 800c318:	4293      	cmp	r3, r2
 800c31a:	b083      	sub	sp, #12
 800c31c:	dd19      	ble.n	800c352 <cosf+0x46>
 800c31e:	f1b3 4fff 	cmp.w	r3, #2139095040	; 0x7f800000
 800c322:	db04      	blt.n	800c32e <cosf+0x22>
 800c324:	ee30 0a40 	vsub.f32	s0, s0, s0
 800c328:	b003      	add	sp, #12
 800c32a:	f85d fb04 	ldr.w	pc, [sp], #4
 800c32e:	4668      	mov	r0, sp
 800c330:	f000 f87e 	bl	800c430 <__ieee754_rem_pio2f>
 800c334:	f000 0003 	and.w	r0, r0, #3
 800c338:	2801      	cmp	r0, #1
 800c33a:	d011      	beq.n	800c360 <cosf+0x54>
 800c33c:	2802      	cmp	r0, #2
 800c33e:	d01f      	beq.n	800c380 <cosf+0x74>
 800c340:	b1b8      	cbz	r0, 800c372 <cosf+0x66>
 800c342:	2001      	movs	r0, #1
 800c344:	eddd 0a01 	vldr	s1, [sp, #4]
 800c348:	ed9d 0a00 	vldr	s0, [sp]
 800c34c:	f000 fd70 	bl	800ce30 <__kernel_sinf>
 800c350:	e7ea      	b.n	800c328 <cosf+0x1c>
 800c352:	eddf 0a11 	vldr	s1, [pc, #68]	; 800c398 <cosf+0x8c>
 800c356:	f000 f9b7 	bl	800c6c8 <__kernel_cosf>
 800c35a:	b003      	add	sp, #12
 800c35c:	f85d fb04 	ldr.w	pc, [sp], #4
 800c360:	eddd 0a01 	vldr	s1, [sp, #4]
 800c364:	ed9d 0a00 	vldr	s0, [sp]
 800c368:	f000 fd62 	bl	800ce30 <__kernel_sinf>
 800c36c:	eeb1 0a40 	vneg.f32	s0, s0
 800c370:	e7da      	b.n	800c328 <cosf+0x1c>
 800c372:	eddd 0a01 	vldr	s1, [sp, #4]
 800c376:	ed9d 0a00 	vldr	s0, [sp]
 800c37a:	f000 f9a5 	bl	800c6c8 <__kernel_cosf>
 800c37e:	e7d3      	b.n	800c328 <cosf+0x1c>
 800c380:	eddd 0a01 	vldr	s1, [sp, #4]
 800c384:	ed9d 0a00 	vldr	s0, [sp]
 800c388:	f000 f99e 	bl	800c6c8 <__kernel_cosf>
 800c38c:	eeb1 0a40 	vneg.f32	s0, s0
 800c390:	e7ca      	b.n	800c328 <cosf+0x1c>
 800c392:	bf00      	nop
 800c394:	3f490fd8 	.word	0x3f490fd8
 800c398:	00000000 	.word	0x00000000

0800c39c <sinf>:
 800c39c:	b500      	push	{lr}
 800c39e:	ee10 3a10 	vmov	r3, s0
 800c3a2:	4a21      	ldr	r2, [pc, #132]	; (800c428 <sinf+0x8c>)
 800c3a4:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 800c3a8:	4293      	cmp	r3, r2
 800c3aa:	b083      	sub	sp, #12
 800c3ac:	dd1a      	ble.n	800c3e4 <sinf+0x48>
 800c3ae:	f1b3 4fff 	cmp.w	r3, #2139095040	; 0x7f800000
 800c3b2:	db04      	blt.n	800c3be <sinf+0x22>
 800c3b4:	ee30 0a40 	vsub.f32	s0, s0, s0
 800c3b8:	b003      	add	sp, #12
 800c3ba:	f85d fb04 	ldr.w	pc, [sp], #4
 800c3be:	4668      	mov	r0, sp
 800c3c0:	f000 f836 	bl	800c430 <__ieee754_rem_pio2f>
 800c3c4:	f000 0003 	and.w	r0, r0, #3
 800c3c8:	2801      	cmp	r0, #1
 800c3ca:	d013      	beq.n	800c3f4 <sinf+0x58>
 800c3cc:	2802      	cmp	r0, #2
 800c3ce:	d020      	beq.n	800c412 <sinf+0x76>
 800c3d0:	b1b8      	cbz	r0, 800c402 <sinf+0x66>
 800c3d2:	eddd 0a01 	vldr	s1, [sp, #4]
 800c3d6:	ed9d 0a00 	vldr	s0, [sp]
 800c3da:	f000 f975 	bl	800c6c8 <__kernel_cosf>
 800c3de:	eeb1 0a40 	vneg.f32	s0, s0
 800c3e2:	e7e9      	b.n	800c3b8 <sinf+0x1c>
 800c3e4:	2000      	movs	r0, #0
 800c3e6:	eddf 0a11 	vldr	s1, [pc, #68]	; 800c42c <sinf+0x90>
 800c3ea:	f000 fd21 	bl	800ce30 <__kernel_sinf>
 800c3ee:	b003      	add	sp, #12
 800c3f0:	f85d fb04 	ldr.w	pc, [sp], #4
 800c3f4:	eddd 0a01 	vldr	s1, [sp, #4]
 800c3f8:	ed9d 0a00 	vldr	s0, [sp]
 800c3fc:	f000 f964 	bl	800c6c8 <__kernel_cosf>
 800c400:	e7da      	b.n	800c3b8 <sinf+0x1c>
 800c402:	2001      	movs	r0, #1
 800c404:	eddd 0a01 	vldr	s1, [sp, #4]
 800c408:	ed9d 0a00 	vldr	s0, [sp]
 800c40c:	f000 fd10 	bl	800ce30 <__kernel_sinf>
 800c410:	e7d2      	b.n	800c3b8 <sinf+0x1c>
 800c412:	2001      	movs	r0, #1
 800c414:	eddd 0a01 	vldr	s1, [sp, #4]
 800c418:	ed9d 0a00 	vldr	s0, [sp]
 800c41c:	f000 fd08 	bl	800ce30 <__kernel_sinf>
 800c420:	eeb1 0a40 	vneg.f32	s0, s0
 800c424:	e7c8      	b.n	800c3b8 <sinf+0x1c>
 800c426:	bf00      	nop
 800c428:	3f490fd8 	.word	0x3f490fd8
 800c42c:	00000000 	.word	0x00000000

0800c430 <__ieee754_rem_pio2f>:
 800c430:	b570      	push	{r4, r5, r6, lr}
 800c432:	ee10 3a10 	vmov	r3, s0
 800c436:	4a96      	ldr	r2, [pc, #600]	; (800c690 <__ieee754_rem_pio2f+0x260>)
 800c438:	f023 4400 	bic.w	r4, r3, #2147483648	; 0x80000000
 800c43c:	4294      	cmp	r4, r2
 800c43e:	b086      	sub	sp, #24
 800c440:	4605      	mov	r5, r0
 800c442:	dd68      	ble.n	800c516 <__ieee754_rem_pio2f+0xe6>
 800c444:	4a93      	ldr	r2, [pc, #588]	; (800c694 <__ieee754_rem_pio2f+0x264>)
 800c446:	4294      	cmp	r4, r2
 800c448:	ee10 6a10 	vmov	r6, s0
 800c44c:	dc1a      	bgt.n	800c484 <__ieee754_rem_pio2f+0x54>
 800c44e:	2b00      	cmp	r3, #0
 800c450:	f024 040f 	bic.w	r4, r4, #15
 800c454:	eddf 7a90 	vldr	s15, [pc, #576]	; 800c698 <__ieee754_rem_pio2f+0x268>
 800c458:	4a90      	ldr	r2, [pc, #576]	; (800c69c <__ieee754_rem_pio2f+0x26c>)
 800c45a:	f340 80f2 	ble.w	800c642 <__ieee754_rem_pio2f+0x212>
 800c45e:	4294      	cmp	r4, r2
 800c460:	ee70 7a67 	vsub.f32	s15, s0, s15
 800c464:	d066      	beq.n	800c534 <__ieee754_rem_pio2f+0x104>
 800c466:	ed9f 7a8e 	vldr	s14, [pc, #568]	; 800c6a0 <__ieee754_rem_pio2f+0x270>
 800c46a:	ee77 6ac7 	vsub.f32	s13, s15, s14
 800c46e:	2001      	movs	r0, #1
 800c470:	ee77 7ae6 	vsub.f32	s15, s15, s13
 800c474:	edc5 6a00 	vstr	s13, [r5]
 800c478:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800c47c:	edc5 7a01 	vstr	s15, [r5, #4]
 800c480:	b006      	add	sp, #24
 800c482:	bd70      	pop	{r4, r5, r6, pc}
 800c484:	4a87      	ldr	r2, [pc, #540]	; (800c6a4 <__ieee754_rem_pio2f+0x274>)
 800c486:	4294      	cmp	r4, r2
 800c488:	dd67      	ble.n	800c55a <__ieee754_rem_pio2f+0x12a>
 800c48a:	f1b4 4fff 	cmp.w	r4, #2139095040	; 0x7f800000
 800c48e:	da49      	bge.n	800c524 <__ieee754_rem_pio2f+0xf4>
 800c490:	15e2      	asrs	r2, r4, #23
 800c492:	3a86      	subs	r2, #134	; 0x86
 800c494:	eba4 53c2 	sub.w	r3, r4, r2, lsl #23
 800c498:	ee07 3a90 	vmov	s15, r3
 800c49c:	eebd 7ae7 	vcvt.s32.f32	s14, s15
 800c4a0:	eddf 6a81 	vldr	s13, [pc, #516]	; 800c6a8 <__ieee754_rem_pio2f+0x278>
 800c4a4:	eeb8 7ac7 	vcvt.f32.s32	s14, s14
 800c4a8:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800c4ac:	ed8d 7a03 	vstr	s14, [sp, #12]
 800c4b0:	ee67 7aa6 	vmul.f32	s15, s15, s13
 800c4b4:	eebd 7ae7 	vcvt.s32.f32	s14, s15
 800c4b8:	eeb8 7ac7 	vcvt.f32.s32	s14, s14
 800c4bc:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800c4c0:	ed8d 7a04 	vstr	s14, [sp, #16]
 800c4c4:	ee67 7aa6 	vmul.f32	s15, s15, s13
 800c4c8:	eef5 7a40 	vcmp.f32	s15, #0.0
 800c4cc:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800c4d0:	edcd 7a05 	vstr	s15, [sp, #20]
 800c4d4:	f040 80a2 	bne.w	800c61c <__ieee754_rem_pio2f+0x1ec>
 800c4d8:	eeb5 7a40 	vcmp.f32	s14, #0.0
 800c4dc:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800c4e0:	bf14      	ite	ne
 800c4e2:	2302      	movne	r3, #2
 800c4e4:	2301      	moveq	r3, #1
 800c4e6:	4971      	ldr	r1, [pc, #452]	; (800c6ac <__ieee754_rem_pio2f+0x27c>)
 800c4e8:	9101      	str	r1, [sp, #4]
 800c4ea:	2102      	movs	r1, #2
 800c4ec:	9100      	str	r1, [sp, #0]
 800c4ee:	a803      	add	r0, sp, #12
 800c4f0:	4629      	mov	r1, r5
 800c4f2:	f000 f96b 	bl	800c7cc <__kernel_rem_pio2f>
 800c4f6:	2e00      	cmp	r6, #0
 800c4f8:	dac2      	bge.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c4fa:	ed95 7a00 	vldr	s14, [r5]
 800c4fe:	edd5 7a01 	vldr	s15, [r5, #4]
 800c502:	eeb1 7a47 	vneg.f32	s14, s14
 800c506:	eef1 7a67 	vneg.f32	s15, s15
 800c50a:	4240      	negs	r0, r0
 800c50c:	ed85 7a00 	vstr	s14, [r5]
 800c510:	edc5 7a01 	vstr	s15, [r5, #4]
 800c514:	e7b4      	b.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c516:	2200      	movs	r2, #0
 800c518:	ed85 0a00 	vstr	s0, [r5]
 800c51c:	6042      	str	r2, [r0, #4]
 800c51e:	2000      	movs	r0, #0
 800c520:	b006      	add	sp, #24
 800c522:	bd70      	pop	{r4, r5, r6, pc}
 800c524:	ee70 7a40 	vsub.f32	s15, s0, s0
 800c528:	2000      	movs	r0, #0
 800c52a:	edc5 7a01 	vstr	s15, [r5, #4]
 800c52e:	edc5 7a00 	vstr	s15, [r5]
 800c532:	e7a5      	b.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c534:	eddf 6a5e 	vldr	s13, [pc, #376]	; 800c6b0 <__ieee754_rem_pio2f+0x280>
 800c538:	ed9f 7a5e 	vldr	s14, [pc, #376]	; 800c6b4 <__ieee754_rem_pio2f+0x284>
 800c53c:	ee77 7ae6 	vsub.f32	s15, s15, s13
 800c540:	2001      	movs	r0, #1
 800c542:	ee77 6ac7 	vsub.f32	s13, s15, s14
 800c546:	ee77 7ae6 	vsub.f32	s15, s15, s13
 800c54a:	edc5 6a00 	vstr	s13, [r5]
 800c54e:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800c552:	edc5 7a01 	vstr	s15, [r5, #4]
 800c556:	b006      	add	sp, #24
 800c558:	bd70      	pop	{r4, r5, r6, pc}
 800c55a:	f000 fcb1 	bl	800cec0 <fabsf>
 800c55e:	eddf 6a56 	vldr	s13, [pc, #344]	; 800c6b8 <__ieee754_rem_pio2f+0x288>
 800c562:	eddf 5a4d 	vldr	s11, [pc, #308]	; 800c698 <__ieee754_rem_pio2f+0x268>
 800c566:	ed9f 7a4e 	vldr	s14, [pc, #312]	; 800c6a0 <__ieee754_rem_pio2f+0x270>
 800c56a:	eef6 7a00 	vmov.f32	s15, #96	; 0x3f000000  0.5
 800c56e:	eee0 7a26 	vfma.f32	s15, s0, s13
 800c572:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 800c576:	ee17 0a90 	vmov	r0, s15
 800c57a:	eef8 6ae7 	vcvt.f32.s32	s13, s15
 800c57e:	281f      	cmp	r0, #31
 800c580:	eeb1 6a66 	vneg.f32	s12, s13
 800c584:	eea6 0a25 	vfma.f32	s0, s12, s11
 800c588:	ee66 7a87 	vmul.f32	s15, s13, s14
 800c58c:	dc1e      	bgt.n	800c5cc <__ieee754_rem_pio2f+0x19c>
 800c58e:	1e42      	subs	r2, r0, #1
 800c590:	4b4a      	ldr	r3, [pc, #296]	; (800c6bc <__ieee754_rem_pio2f+0x28c>)
 800c592:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 800c596:	f024 02ff 	bic.w	r2, r4, #255	; 0xff
 800c59a:	429a      	cmp	r2, r3
 800c59c:	ee30 7a67 	vsub.f32	s14, s0, s15
 800c5a0:	d016      	beq.n	800c5d0 <__ieee754_rem_pio2f+0x1a0>
 800c5a2:	ed85 7a00 	vstr	s14, [r5]
 800c5a6:	ee30 0a47 	vsub.f32	s0, s0, s14
 800c5aa:	2e00      	cmp	r6, #0
 800c5ac:	ee30 0a67 	vsub.f32	s0, s0, s15
 800c5b0:	ed85 0a01 	vstr	s0, [r5, #4]
 800c5b4:	f6bf af64 	bge.w	800c480 <__ieee754_rem_pio2f+0x50>
 800c5b8:	eeb1 7a47 	vneg.f32	s14, s14
 800c5bc:	eeb1 0a40 	vneg.f32	s0, s0
 800c5c0:	ed85 7a00 	vstr	s14, [r5]
 800c5c4:	ed85 0a01 	vstr	s0, [r5, #4]
 800c5c8:	4240      	negs	r0, r0
 800c5ca:	e759      	b.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c5cc:	ee30 7a67 	vsub.f32	s14, s0, s15
 800c5d0:	ee17 3a10 	vmov	r3, s14
 800c5d4:	15e4      	asrs	r4, r4, #23
 800c5d6:	f3c3 53c7 	ubfx	r3, r3, #23, #8
 800c5da:	1ae3      	subs	r3, r4, r3
 800c5dc:	2b08      	cmp	r3, #8
 800c5de:	dde0      	ble.n	800c5a2 <__ieee754_rem_pio2f+0x172>
 800c5e0:	eddf 7a33 	vldr	s15, [pc, #204]	; 800c6b0 <__ieee754_rem_pio2f+0x280>
 800c5e4:	ed9f 7a33 	vldr	s14, [pc, #204]	; 800c6b4 <__ieee754_rem_pio2f+0x284>
 800c5e8:	eef0 5a40 	vmov.f32	s11, s0
 800c5ec:	eee6 5a27 	vfma.f32	s11, s12, s15
 800c5f0:	ee30 0a65 	vsub.f32	s0, s0, s11
 800c5f4:	eea6 0a27 	vfma.f32	s0, s12, s15
 800c5f8:	eef0 7a40 	vmov.f32	s15, s0
 800c5fc:	eed6 7a87 	vfnms.f32	s15, s13, s14
 800c600:	ee35 7ae7 	vsub.f32	s14, s11, s15
 800c604:	ee17 3a10 	vmov	r3, s14
 800c608:	f3c3 53c7 	ubfx	r3, r3, #23, #8
 800c60c:	1ae4      	subs	r4, r4, r3
 800c60e:	2c19      	cmp	r4, #25
 800c610:	dc06      	bgt.n	800c620 <__ieee754_rem_pio2f+0x1f0>
 800c612:	ed85 7a00 	vstr	s14, [r5]
 800c616:	eeb0 0a65 	vmov.f32	s0, s11
 800c61a:	e7c4      	b.n	800c5a6 <__ieee754_rem_pio2f+0x176>
 800c61c:	2303      	movs	r3, #3
 800c61e:	e762      	b.n	800c4e6 <__ieee754_rem_pio2f+0xb6>
 800c620:	ed9f 7a27 	vldr	s14, [pc, #156]	; 800c6c0 <__ieee754_rem_pio2f+0x290>
 800c624:	ed9f 5a27 	vldr	s10, [pc, #156]	; 800c6c4 <__ieee754_rem_pio2f+0x294>
 800c628:	eeb0 0a65 	vmov.f32	s0, s11
 800c62c:	eea6 0a07 	vfma.f32	s0, s12, s14
 800c630:	ee75 7ac0 	vsub.f32	s15, s11, s0
 800c634:	eee6 7a07 	vfma.f32	s15, s12, s14
 800c638:	eed6 7a85 	vfnms.f32	s15, s13, s10
 800c63c:	ee30 7a67 	vsub.f32	s14, s0, s15
 800c640:	e7af      	b.n	800c5a2 <__ieee754_rem_pio2f+0x172>
 800c642:	4294      	cmp	r4, r2
 800c644:	ee70 7a27 	vadd.f32	s15, s0, s15
 800c648:	d00e      	beq.n	800c668 <__ieee754_rem_pio2f+0x238>
 800c64a:	ed9f 7a15 	vldr	s14, [pc, #84]	; 800c6a0 <__ieee754_rem_pio2f+0x270>
 800c64e:	ee77 6a87 	vadd.f32	s13, s15, s14
 800c652:	f04f 30ff 	mov.w	r0, #4294967295
 800c656:	ee77 7ae6 	vsub.f32	s15, s15, s13
 800c65a:	edc5 6a00 	vstr	s13, [r5]
 800c65e:	ee77 7a87 	vadd.f32	s15, s15, s14
 800c662:	edc5 7a01 	vstr	s15, [r5, #4]
 800c666:	e70b      	b.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c668:	eddf 6a11 	vldr	s13, [pc, #68]	; 800c6b0 <__ieee754_rem_pio2f+0x280>
 800c66c:	ed9f 7a11 	vldr	s14, [pc, #68]	; 800c6b4 <__ieee754_rem_pio2f+0x284>
 800c670:	ee77 7aa6 	vadd.f32	s15, s15, s13
 800c674:	f04f 30ff 	mov.w	r0, #4294967295
 800c678:	ee77 6a87 	vadd.f32	s13, s15, s14
 800c67c:	ee77 7ae6 	vsub.f32	s15, s15, s13
 800c680:	edc5 6a00 	vstr	s13, [r5]
 800c684:	ee77 7a87 	vadd.f32	s15, s15, s14
 800c688:	edc5 7a01 	vstr	s15, [r5, #4]
 800c68c:	e6f8      	b.n	800c480 <__ieee754_rem_pio2f+0x50>
 800c68e:	bf00      	nop
 800c690:	3f490fd8 	.word	0x3f490fd8
 800c694:	4016cbe3 	.word	0x4016cbe3
 800c698:	3fc90f80 	.word	0x3fc90f80
 800c69c:	3fc90fd0 	.word	0x3fc90fd0
 800c6a0:	37354443 	.word	0x37354443
 800c6a4:	43490f80 	.word	0x43490f80
 800c6a8:	43800000 	.word	0x43800000
 800c6ac:	0800d678 	.word	0x0800d678
 800c6b0:	37354400 	.word	0x37354400
 800c6b4:	2e85a308 	.word	0x2e85a308
 800c6b8:	3f22f984 	.word	0x3f22f984
 800c6bc:	0800d5f8 	.word	0x0800d5f8
 800c6c0:	2e85a300 	.word	0x2e85a300
 800c6c4:	248d3132 	.word	0x248d3132

0800c6c8 <__kernel_cosf>:
 800c6c8:	ee10 3a10 	vmov	r3, s0
 800c6cc:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 800c6d0:	f1b3 5f48 	cmp.w	r3, #838860800	; 0x32000000
 800c6d4:	da2c      	bge.n	800c730 <__kernel_cosf+0x68>
 800c6d6:	eefd 7ac0 	vcvt.s32.f32	s15, s0
 800c6da:	ee17 3a90 	vmov	r3, s15
 800c6de:	2b00      	cmp	r3, #0
 800c6e0:	d060      	beq.n	800c7a4 <__kernel_cosf+0xdc>
 800c6e2:	ee20 7a00 	vmul.f32	s14, s0, s0
 800c6e6:	eddf 4a31 	vldr	s9, [pc, #196]	; 800c7ac <__kernel_cosf+0xe4>
 800c6ea:	ed9f 5a31 	vldr	s10, [pc, #196]	; 800c7b0 <__kernel_cosf+0xe8>
 800c6ee:	eddf 5a31 	vldr	s11, [pc, #196]	; 800c7b4 <__kernel_cosf+0xec>
 800c6f2:	ed9f 6a31 	vldr	s12, [pc, #196]	; 800c7b8 <__kernel_cosf+0xf0>
 800c6f6:	eddf 7a31 	vldr	s15, [pc, #196]	; 800c7bc <__kernel_cosf+0xf4>
 800c6fa:	eddf 6a31 	vldr	s13, [pc, #196]	; 800c7c0 <__kernel_cosf+0xf8>
 800c6fe:	eea7 5a24 	vfma.f32	s10, s14, s9
 800c702:	eee7 5a05 	vfma.f32	s11, s14, s10
 800c706:	eea7 6a25 	vfma.f32	s12, s14, s11
 800c70a:	eee7 7a06 	vfma.f32	s15, s14, s12
 800c70e:	eee7 6a27 	vfma.f32	s13, s14, s15
 800c712:	ee66 6a87 	vmul.f32	s13, s13, s14
 800c716:	ee60 0ac0 	vnmul.f32	s1, s1, s0
 800c71a:	eeb6 6a00 	vmov.f32	s12, #96	; 0x3f000000  0.5
 800c71e:	eee7 0a26 	vfma.f32	s1, s14, s13
 800c722:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 800c726:	eed7 0a06 	vfnms.f32	s1, s14, s12
 800c72a:	ee37 0ae0 	vsub.f32	s0, s15, s1
 800c72e:	4770      	bx	lr
 800c730:	ee20 7a00 	vmul.f32	s14, s0, s0
 800c734:	eddf 4a1d 	vldr	s9, [pc, #116]	; 800c7ac <__kernel_cosf+0xe4>
 800c738:	ed9f 5a1d 	vldr	s10, [pc, #116]	; 800c7b0 <__kernel_cosf+0xe8>
 800c73c:	eddf 5a1d 	vldr	s11, [pc, #116]	; 800c7b4 <__kernel_cosf+0xec>
 800c740:	ed9f 6a1d 	vldr	s12, [pc, #116]	; 800c7b8 <__kernel_cosf+0xf0>
 800c744:	eddf 7a1d 	vldr	s15, [pc, #116]	; 800c7bc <__kernel_cosf+0xf4>
 800c748:	eddf 6a1d 	vldr	s13, [pc, #116]	; 800c7c0 <__kernel_cosf+0xf8>
 800c74c:	4a1d      	ldr	r2, [pc, #116]	; (800c7c4 <__kernel_cosf+0xfc>)
 800c74e:	eea7 5a24 	vfma.f32	s10, s14, s9
 800c752:	4293      	cmp	r3, r2
 800c754:	eee5 5a07 	vfma.f32	s11, s10, s14
 800c758:	eea5 6a87 	vfma.f32	s12, s11, s14
 800c75c:	eee6 7a07 	vfma.f32	s15, s12, s14
 800c760:	eee7 6a87 	vfma.f32	s13, s15, s14
 800c764:	ee66 6a87 	vmul.f32	s13, s13, s14
 800c768:	ddd5      	ble.n	800c716 <__kernel_cosf+0x4e>
 800c76a:	4a17      	ldr	r2, [pc, #92]	; (800c7c8 <__kernel_cosf+0x100>)
 800c76c:	4293      	cmp	r3, r2
 800c76e:	dc14      	bgt.n	800c79a <__kernel_cosf+0xd2>
 800c770:	f103 437f 	add.w	r3, r3, #4278190080	; 0xff000000
 800c774:	ee07 3a90 	vmov	s15, r3
 800c778:	eeb7 6a00 	vmov.f32	s12, #112	; 0x3f800000  1.0
 800c77c:	ee36 6a67 	vsub.f32	s12, s12, s15
 800c780:	ee60 0ac0 	vnmul.f32	s1, s1, s0
 800c784:	eef6 5a00 	vmov.f32	s11, #96	; 0x3f000000  0.5
 800c788:	eee7 0a26 	vfma.f32	s1, s14, s13
 800c78c:	eed7 7a25 	vfnms.f32	s15, s14, s11
 800c790:	ee77 7ae0 	vsub.f32	s15, s15, s1
 800c794:	ee36 0a67 	vsub.f32	s0, s12, s15
 800c798:	4770      	bx	lr
 800c79a:	eeb6 6a07 	vmov.f32	s12, #103	; 0x3f380000  0.7187500
 800c79e:	eef5 7a02 	vmov.f32	s15, #82	; 0x3e900000  0.2812500
 800c7a2:	e7ed      	b.n	800c780 <__kernel_cosf+0xb8>
 800c7a4:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 800c7a8:	4770      	bx	lr
 800c7aa:	bf00      	nop
 800c7ac:	ad47d74e 	.word	0xad47d74e
 800c7b0:	310f74f6 	.word	0x310f74f6
 800c7b4:	b493f27c 	.word	0xb493f27c
 800c7b8:	37d00d01 	.word	0x37d00d01
 800c7bc:	bab60b61 	.word	0xbab60b61
 800c7c0:	3d2aaaab 	.word	0x3d2aaaab
 800c7c4:	3e999999 	.word	0x3e999999
 800c7c8:	3f480000 	.word	0x3f480000

0800c7cc <__kernel_rem_pio2f>:
 800c7cc:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800c7d0:	ed2d 8b04 	vpush	{d8-d9}
 800c7d4:	b0db      	sub	sp, #364	; 0x16c
 800c7d6:	4cd6      	ldr	r4, [pc, #856]	; (800cb30 <__kernel_rem_pio2f+0x364>)
 800c7d8:	9d68      	ldr	r5, [sp, #416]	; 0x1a0
 800c7da:	9107      	str	r1, [sp, #28]
 800c7dc:	1d11      	adds	r1, r2, #4
 800c7de:	f854 5025 	ldr.w	r5, [r4, r5, lsl #2]
 800c7e2:	9304      	str	r3, [sp, #16]
 800c7e4:	9209      	str	r2, [sp, #36]	; 0x24
 800c7e6:	4681      	mov	r9, r0
 800c7e8:	f103 38ff 	add.w	r8, r3, #4294967295
 800c7ec:	f2c0 8257 	blt.w	800cc9e <__kernel_rem_pio2f+0x4d2>
 800c7f0:	1ed3      	subs	r3, r2, #3
 800c7f2:	bf44      	itt	mi
 800c7f4:	4613      	movmi	r3, r2
 800c7f6:	3304      	addmi	r3, #4
 800c7f8:	10db      	asrs	r3, r3, #3
 800c7fa:	9303      	str	r3, [sp, #12]
 800c7fc:	3301      	adds	r3, #1
 800c7fe:	00db      	lsls	r3, r3, #3
 800c800:	9308      	str	r3, [sp, #32]
 800c802:	e9dd 2308 	ldrd	r2, r3, [sp, #32]
 800c806:	1a9b      	subs	r3, r3, r2
 800c808:	9306      	str	r3, [sp, #24]
 800c80a:	9b03      	ldr	r3, [sp, #12]
 800c80c:	eb15 0108 	adds.w	r1, r5, r8
 800c810:	eba3 0308 	sub.w	r3, r3, r8
 800c814:	d416      	bmi.n	800c844 <__kernel_rem_pio2f+0x78>
 800c816:	3101      	adds	r1, #1
 800c818:	ed9f 7ac6 	vldr	s14, [pc, #792]	; 800cb34 <__kernel_rem_pio2f+0x368>
 800c81c:	9869      	ldr	r0, [sp, #420]	; 0x1a4
 800c81e:	4419      	add	r1, r3
 800c820:	aa1e      	add	r2, sp, #120	; 0x78
 800c822:	2b00      	cmp	r3, #0
 800c824:	bfa4      	itt	ge
 800c826:	f850 4023 	ldrge.w	r4, [r0, r3, lsl #2]
 800c82a:	ee07 4a90 	vmovge	s15, r4
 800c82e:	f103 0301 	add.w	r3, r3, #1
 800c832:	bfac      	ite	ge
 800c834:	eef8 7ae7 	vcvtge.f32.s32	s15, s15
 800c838:	eef0 7a47 	vmovlt.f32	s15, s14
 800c83c:	428b      	cmp	r3, r1
 800c83e:	ece2 7a01 	vstmia	r2!, {s15}
 800c842:	d1ee      	bne.n	800c822 <__kernel_rem_pio2f+0x56>
 800c844:	9b04      	ldr	r3, [sp, #16]
 800c846:	2d00      	cmp	r5, #0
 800c848:	ea4f 0c83 	mov.w	ip, r3, lsl #2
 800c84c:	ea4f 0a85 	mov.w	sl, r5, lsl #2
 800c850:	db1d      	blt.n	800c88e <__kernel_rem_pio2f+0xc2>
 800c852:	ab1e      	add	r3, sp, #120	; 0x78
 800c854:	eb03 040c 	add.w	r4, r3, ip
 800c858:	ab47      	add	r3, sp, #284	; 0x11c
 800c85a:	eb09 010c 	add.w	r1, r9, ip
 800c85e:	eb03 060a 	add.w	r6, r3, sl
 800c862:	a846      	add	r0, sp, #280	; 0x118
 800c864:	f1b8 0f00 	cmp.w	r8, #0
 800c868:	eddf 7ab2 	vldr	s15, [pc, #712]	; 800cb34 <__kernel_rem_pio2f+0x368>
 800c86c:	db09      	blt.n	800c882 <__kernel_rem_pio2f+0xb6>
 800c86e:	4622      	mov	r2, r4
 800c870:	464b      	mov	r3, r9
 800c872:	ecf3 6a01 	vldmia	r3!, {s13}
 800c876:	ed32 7a01 	vldmdb	r2!, {s14}
 800c87a:	428b      	cmp	r3, r1
 800c87c:	eee6 7a87 	vfma.f32	s15, s13, s14
 800c880:	d1f7      	bne.n	800c872 <__kernel_rem_pio2f+0xa6>
 800c882:	ece0 7a01 	vstmia	r0!, {s15}
 800c886:	42b0      	cmp	r0, r6
 800c888:	f104 0404 	add.w	r4, r4, #4
 800c88c:	d1ea      	bne.n	800c864 <__kernel_rem_pio2f+0x98>
 800c88e:	f1aa 0308 	sub.w	r3, sl, #8
 800c892:	af0a      	add	r7, sp, #40	; 0x28
 800c894:	18fb      	adds	r3, r7, r3
 800c896:	9305      	str	r3, [sp, #20]
 800c898:	46ab      	mov	fp, r5
 800c89a:	ab09      	add	r3, sp, #36	; 0x24
 800c89c:	9501      	str	r5, [sp, #4]
 800c89e:	ed9f 9aa7 	vldr	s18, [pc, #668]	; 800cb3c <__kernel_rem_pio2f+0x370>
 800c8a2:	eddf 8aa5 	vldr	s17, [pc, #660]	; 800cb38 <__kernel_rem_pio2f+0x36c>
 800c8a6:	9d06      	ldr	r5, [sp, #24]
 800c8a8:	eb09 040c 	add.w	r4, r9, ip
 800c8ac:	449a      	add	sl, r3
 800c8ae:	ae46      	add	r6, sp, #280	; 0x118
 800c8b0:	ea4f 018b 	mov.w	r1, fp, lsl #2
 800c8b4:	ab5a      	add	r3, sp, #360	; 0x168
 800c8b6:	440b      	add	r3, r1
 800c8b8:	f1bb 0f00 	cmp.w	fp, #0
 800c8bc:	ed13 0a14 	vldr	s0, [r3, #-80]	; 0xffffffb0
 800c8c0:	dd16      	ble.n	800c8f0 <__kernel_rem_pio2f+0x124>
 800c8c2:	eb06 038b 	add.w	r3, r6, fp, lsl #2
 800c8c6:	463a      	mov	r2, r7
 800c8c8:	ee60 7a09 	vmul.f32	s15, s0, s18
 800c8cc:	eeb0 7a40 	vmov.f32	s14, s0
 800c8d0:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 800c8d4:	ed73 6a01 	vldmdb	r3!, {s13}
 800c8d8:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 800c8dc:	42b3      	cmp	r3, r6
 800c8de:	eea7 7ae8 	vfms.f32	s14, s15, s17
 800c8e2:	ee37 0aa6 	vadd.f32	s0, s15, s13
 800c8e6:	eebd 7ac7 	vcvt.s32.f32	s14, s14
 800c8ea:	eca2 7a01 	vstmia	r2!, {s14}
 800c8ee:	d1eb      	bne.n	800c8c8 <__kernel_rem_pio2f+0xfc>
 800c8f0:	4628      	mov	r0, r5
 800c8f2:	9102      	str	r1, [sp, #8]
 800c8f4:	f000 fb30 	bl	800cf58 <scalbnf>
 800c8f8:	eeb0 8a40 	vmov.f32	s16, s0
 800c8fc:	eeb4 0a00 	vmov.f32	s0, #64	; 0x3e000000  0.125
 800c900:	ee28 0a00 	vmul.f32	s0, s16, s0
 800c904:	f000 fae4 	bl	800ced0 <floorf>
 800c908:	eef2 7a00 	vmov.f32	s15, #32	; 0x41000000  8.0
 800c90c:	eea0 8a67 	vfms.f32	s16, s0, s15
 800c910:	2d00      	cmp	r5, #0
 800c912:	9902      	ldr	r1, [sp, #8]
 800c914:	eefd 9ac8 	vcvt.s32.f32	s19, s16
 800c918:	eef8 7ae9 	vcvt.f32.s32	s15, s19
 800c91c:	ee38 8a67 	vsub.f32	s16, s16, s15
 800c920:	dd75      	ble.n	800ca0e <__kernel_rem_pio2f+0x242>
 800c922:	f10b 3cff 	add.w	ip, fp, #4294967295
 800c926:	f1c5 0308 	rsb	r3, r5, #8
 800c92a:	f857 202c 	ldr.w	r2, [r7, ip, lsl #2]
 800c92e:	fa42 f003 	asr.w	r0, r2, r3
 800c932:	fa00 f303 	lsl.w	r3, r0, r3
 800c936:	1ad3      	subs	r3, r2, r3
 800c938:	f847 302c 	str.w	r3, [r7, ip, lsl #2]
 800c93c:	f1c5 0207 	rsb	r2, r5, #7
 800c940:	ee19 ca90 	vmov	ip, s19
 800c944:	fa43 f202 	asr.w	r2, r3, r2
 800c948:	4484      	add	ip, r0
 800c94a:	2a00      	cmp	r2, #0
 800c94c:	ee09 ca90 	vmov	s19, ip
 800c950:	dc66      	bgt.n	800ca20 <__kernel_rem_pio2f+0x254>
 800c952:	eeb5 8a40 	vcmp.f32	s16, #0.0
 800c956:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800c95a:	f040 80a3 	bne.w	800caa4 <__kernel_rem_pio2f+0x2d8>
 800c95e:	9b01      	ldr	r3, [sp, #4]
 800c960:	f10b 3cff 	add.w	ip, fp, #4294967295
 800c964:	4563      	cmp	r3, ip
 800c966:	dc0e      	bgt.n	800c986 <__kernel_rem_pio2f+0x1ba>
 800c968:	f10b 4180 	add.w	r1, fp, #1073741824	; 0x40000000
 800c96c:	3901      	subs	r1, #1
 800c96e:	eb07 0181 	add.w	r1, r7, r1, lsl #2
 800c972:	2000      	movs	r0, #0
 800c974:	f851 3904 	ldr.w	r3, [r1], #-4
 800c978:	4551      	cmp	r1, sl
 800c97a:	ea40 0003 	orr.w	r0, r0, r3
 800c97e:	d1f9      	bne.n	800c974 <__kernel_rem_pio2f+0x1a8>
 800c980:	2800      	cmp	r0, #0
 800c982:	f040 80ee 	bne.w	800cb62 <__kernel_rem_pio2f+0x396>
 800c986:	9b01      	ldr	r3, [sp, #4]
 800c988:	3b01      	subs	r3, #1
 800c98a:	f857 3023 	ldr.w	r3, [r7, r3, lsl #2]
 800c98e:	2b00      	cmp	r3, #0
 800c990:	f040 80e2 	bne.w	800cb58 <__kernel_rem_pio2f+0x38c>
 800c994:	9b05      	ldr	r3, [sp, #20]
 800c996:	2001      	movs	r0, #1
 800c998:	f853 2904 	ldr.w	r2, [r3], #-4
 800c99c:	3001      	adds	r0, #1
 800c99e:	2a00      	cmp	r2, #0
 800c9a0:	d0fa      	beq.n	800c998 <__kernel_rem_pio2f+0x1cc>
 800c9a2:	f10b 0301 	add.w	r3, fp, #1
 800c9a6:	4458      	add	r0, fp
 800c9a8:	469c      	mov	ip, r3
 800c9aa:	9a03      	ldr	r2, [sp, #12]
 800c9ac:	eb03 0e02 	add.w	lr, r3, r2
 800c9b0:	9a04      	ldr	r2, [sp, #16]
 800c9b2:	a91e      	add	r1, sp, #120	; 0x78
 800c9b4:	445a      	add	r2, fp
 800c9b6:	f10e 4e80 	add.w	lr, lr, #1073741824	; 0x40000000
 800c9ba:	eb01 0282 	add.w	r2, r1, r2, lsl #2
 800c9be:	9969      	ldr	r1, [sp, #420]	; 0x1a4
 800c9c0:	f10e 3eff 	add.w	lr, lr, #4294967295
 800c9c4:	eb01 0e8e 	add.w	lr, r1, lr, lsl #2
 800c9c8:	eb06 0b83 	add.w	fp, r6, r3, lsl #2
 800c9cc:	f85e 3f04 	ldr.w	r3, [lr, #4]!
 800c9d0:	ed9f 7a58 	vldr	s14, [pc, #352]	; 800cb34 <__kernel_rem_pio2f+0x368>
 800c9d4:	ee07 3a90 	vmov	s15, r3
 800c9d8:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 800c9dc:	4611      	mov	r1, r2
 800c9de:	f1b8 0f00 	cmp.w	r8, #0
 800c9e2:	ece1 7a01 	vstmia	r1!, {s15}
 800c9e6:	db09      	blt.n	800c9fc <__kernel_rem_pio2f+0x230>
 800c9e8:	464b      	mov	r3, r9
 800c9ea:	e001      	b.n	800c9f0 <__kernel_rem_pio2f+0x224>
 800c9ec:	ed72 7a01 	vldmdb	r2!, {s15}
 800c9f0:	ecf3 6a01 	vldmia	r3!, {s13}
 800c9f4:	42a3      	cmp	r3, r4
 800c9f6:	eea6 7aa7 	vfma.f32	s14, s13, s15
 800c9fa:	d1f7      	bne.n	800c9ec <__kernel_rem_pio2f+0x220>
 800c9fc:	f10c 0c01 	add.w	ip, ip, #1
 800ca00:	4584      	cmp	ip, r0
 800ca02:	ecab 7a01 	vstmia	fp!, {s14}
 800ca06:	460a      	mov	r2, r1
 800ca08:	dde0      	ble.n	800c9cc <__kernel_rem_pio2f+0x200>
 800ca0a:	4683      	mov	fp, r0
 800ca0c:	e750      	b.n	800c8b0 <__kernel_rem_pio2f+0xe4>
 800ca0e:	f040 8099 	bne.w	800cb44 <__kernel_rem_pio2f+0x378>
 800ca12:	f10b 33ff 	add.w	r3, fp, #4294967295
 800ca16:	f857 2023 	ldr.w	r2, [r7, r3, lsl #2]
 800ca1a:	1212      	asrs	r2, r2, #8
 800ca1c:	2a00      	cmp	r2, #0
 800ca1e:	dd98      	ble.n	800c952 <__kernel_rem_pio2f+0x186>
 800ca20:	ee19 3a90 	vmov	r3, s19
 800ca24:	f1bb 0f00 	cmp.w	fp, #0
 800ca28:	f103 0301 	add.w	r3, r3, #1
 800ca2c:	ee09 3a90 	vmov	s19, r3
 800ca30:	f340 81c4 	ble.w	800cdbc <__kernel_rem_pio2f+0x5f0>
 800ca34:	6838      	ldr	r0, [r7, #0]
 800ca36:	2800      	cmp	r0, #0
 800ca38:	d161      	bne.n	800cafe <__kernel_rem_pio2f+0x332>
 800ca3a:	f1bb 0f01 	cmp.w	fp, #1
 800ca3e:	d00b      	beq.n	800ca58 <__kernel_rem_pio2f+0x28c>
 800ca40:	46be      	mov	lr, r7
 800ca42:	f04f 0c01 	mov.w	ip, #1
 800ca46:	f85e 0f04 	ldr.w	r0, [lr, #4]!
 800ca4a:	f10c 0301 	add.w	r3, ip, #1
 800ca4e:	2800      	cmp	r0, #0
 800ca50:	d158      	bne.n	800cb04 <__kernel_rem_pio2f+0x338>
 800ca52:	459b      	cmp	fp, r3
 800ca54:	469c      	mov	ip, r3
 800ca56:	d1f6      	bne.n	800ca46 <__kernel_rem_pio2f+0x27a>
 800ca58:	2d00      	cmp	r5, #0
 800ca5a:	dd0c      	ble.n	800ca76 <__kernel_rem_pio2f+0x2aa>
 800ca5c:	2d01      	cmp	r5, #1
 800ca5e:	f000 8115 	beq.w	800cc8c <__kernel_rem_pio2f+0x4c0>
 800ca62:	2d02      	cmp	r5, #2
 800ca64:	d107      	bne.n	800ca76 <__kernel_rem_pio2f+0x2aa>
 800ca66:	f10b 31ff 	add.w	r1, fp, #4294967295
 800ca6a:	f857 3021 	ldr.w	r3, [r7, r1, lsl #2]
 800ca6e:	f003 033f 	and.w	r3, r3, #63	; 0x3f
 800ca72:	f847 3021 	str.w	r3, [r7, r1, lsl #2]
 800ca76:	2a02      	cmp	r2, #2
 800ca78:	f47f af6b 	bne.w	800c952 <__kernel_rem_pio2f+0x186>
 800ca7c:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 800ca80:	ee30 8a48 	vsub.f32	s16, s0, s16
 800ca84:	2800      	cmp	r0, #0
 800ca86:	f43f af64 	beq.w	800c952 <__kernel_rem_pio2f+0x186>
 800ca8a:	4628      	mov	r0, r5
 800ca8c:	9202      	str	r2, [sp, #8]
 800ca8e:	f000 fa63 	bl	800cf58 <scalbnf>
 800ca92:	ee38 8a40 	vsub.f32	s16, s16, s0
 800ca96:	9a02      	ldr	r2, [sp, #8]
 800ca98:	eeb5 8a40 	vcmp.f32	s16, #0.0
 800ca9c:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800caa0:	f43f af5d 	beq.w	800c95e <__kernel_rem_pio2f+0x192>
 800caa4:	4690      	mov	r8, r2
 800caa6:	e9dd 3208 	ldrd	r3, r2, [sp, #32]
 800caaa:	eeb0 0a48 	vmov.f32	s0, s16
 800caae:	1a98      	subs	r0, r3, r2
 800cab0:	9d01      	ldr	r5, [sp, #4]
 800cab2:	f000 fa51 	bl	800cf58 <scalbnf>
 800cab6:	ed9f 7a20 	vldr	s14, [pc, #128]	; 800cb38 <__kernel_rem_pio2f+0x36c>
 800caba:	eeb4 0ac7 	vcmpe.f32	s0, s14
 800cabe:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800cac2:	f2c0 817d 	blt.w	800cdc0 <__kernel_rem_pio2f+0x5f4>
 800cac6:	eddf 7a1d 	vldr	s15, [pc, #116]	; 800cb3c <__kernel_rem_pio2f+0x370>
 800caca:	9b06      	ldr	r3, [sp, #24]
 800cacc:	ee60 7a27 	vmul.f32	s15, s0, s15
 800cad0:	3308      	adds	r3, #8
 800cad2:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 800cad6:	9306      	str	r3, [sp, #24]
 800cad8:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 800cadc:	f10b 0301 	add.w	r3, fp, #1
 800cae0:	eea7 0ac7 	vfms.f32	s0, s15, s14
 800cae4:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 800cae8:	eebd 0ac0 	vcvt.s32.f32	s0, s0
 800caec:	ee10 2a10 	vmov	r2, s0
 800caf0:	f847 202b 	str.w	r2, [r7, fp, lsl #2]
 800caf4:	ee17 2a90 	vmov	r2, s15
 800caf8:	f847 2023 	str.w	r2, [r7, r3, lsl #2]
 800cafc:	e046      	b.n	800cb8c <__kernel_rem_pio2f+0x3c0>
 800cafe:	2301      	movs	r3, #1
 800cb00:	f04f 0c00 	mov.w	ip, #0
 800cb04:	f5c0 7080 	rsb	r0, r0, #256	; 0x100
 800cb08:	459b      	cmp	fp, r3
 800cb0a:	f847 002c 	str.w	r0, [r7, ip, lsl #2]
 800cb0e:	dd0c      	ble.n	800cb2a <__kernel_rem_pio2f+0x35e>
 800cb10:	f857 0023 	ldr.w	r0, [r7, r3, lsl #2]
 800cb14:	4439      	add	r1, r7
 800cb16:	eb07 0383 	add.w	r3, r7, r3, lsl #2
 800cb1a:	e000      	b.n	800cb1e <__kernel_rem_pio2f+0x352>
 800cb1c:	6818      	ldr	r0, [r3, #0]
 800cb1e:	f1c0 00ff 	rsb	r0, r0, #255	; 0xff
 800cb22:	f843 0b04 	str.w	r0, [r3], #4
 800cb26:	428b      	cmp	r3, r1
 800cb28:	d1f8      	bne.n	800cb1c <__kernel_rem_pio2f+0x350>
 800cb2a:	2001      	movs	r0, #1
 800cb2c:	e794      	b.n	800ca58 <__kernel_rem_pio2f+0x28c>
 800cb2e:	bf00      	nop
 800cb30:	0800d9bc 	.word	0x0800d9bc
 800cb34:	00000000 	.word	0x00000000
 800cb38:	43800000 	.word	0x43800000
 800cb3c:	3b800000 	.word	0x3b800000
 800cb40:	3fc90000 	.word	0x3fc90000
 800cb44:	eef6 7a00 	vmov.f32	s15, #96	; 0x3f000000  0.5
 800cb48:	eeb4 8ae7 	vcmpe.f32	s16, s15
 800cb4c:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800cb50:	f280 80aa 	bge.w	800cca8 <__kernel_rem_pio2f+0x4dc>
 800cb54:	2200      	movs	r2, #0
 800cb56:	e6fc      	b.n	800c952 <__kernel_rem_pio2f+0x186>
 800cb58:	f10b 0301 	add.w	r3, fp, #1
 800cb5c:	469c      	mov	ip, r3
 800cb5e:	4618      	mov	r0, r3
 800cb60:	e723      	b.n	800c9aa <__kernel_rem_pio2f+0x1de>
 800cb62:	9906      	ldr	r1, [sp, #24]
 800cb64:	9d01      	ldr	r5, [sp, #4]
 800cb66:	4690      	mov	r8, r2
 800cb68:	f857 202c 	ldr.w	r2, [r7, ip, lsl #2]
 800cb6c:	3908      	subs	r1, #8
 800cb6e:	4663      	mov	r3, ip
 800cb70:	9106      	str	r1, [sp, #24]
 800cb72:	b95a      	cbnz	r2, 800cb8c <__kernel_rem_pio2f+0x3c0>
 800cb74:	f10b 4280 	add.w	r2, fp, #1073741824	; 0x40000000
 800cb78:	3a02      	subs	r2, #2
 800cb7a:	eb07 0282 	add.w	r2, r7, r2, lsl #2
 800cb7e:	f852 0904 	ldr.w	r0, [r2], #-4
 800cb82:	3b01      	subs	r3, #1
 800cb84:	3908      	subs	r1, #8
 800cb86:	2800      	cmp	r0, #0
 800cb88:	d0f9      	beq.n	800cb7e <__kernel_rem_pio2f+0x3b2>
 800cb8a:	9106      	str	r1, [sp, #24]
 800cb8c:	9806      	ldr	r0, [sp, #24]
 800cb8e:	9301      	str	r3, [sp, #4]
 800cb90:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 800cb94:	f000 f9e0 	bl	800cf58 <scalbnf>
 800cb98:	9b01      	ldr	r3, [sp, #4]
 800cb9a:	2b00      	cmp	r3, #0
 800cb9c:	f2c0 812f 	blt.w	800cdfe <__kernel_rem_pio2f+0x632>
 800cba0:	009e      	lsls	r6, r3, #2
 800cba2:	aa46      	add	r2, sp, #280	; 0x118
 800cba4:	f106 0c04 	add.w	ip, r6, #4
 800cba8:	ed1f 7a1c 	vldr	s14, [pc, #-112]	; 800cb3c <__kernel_rem_pio2f+0x370>
 800cbac:	4416      	add	r6, r2
 800cbae:	1d31      	adds	r1, r6, #4
 800cbb0:	eb07 020c 	add.w	r2, r7, ip
 800cbb4:	ed72 7a01 	vldmdb	r2!, {s15}
 800cbb8:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 800cbbc:	42ba      	cmp	r2, r7
 800cbbe:	ee67 7a80 	vmul.f32	s15, s15, s0
 800cbc2:	ee20 0a07 	vmul.f32	s0, s0, s14
 800cbc6:	ed61 7a01 	vstmdb	r1!, {s15}
 800cbca:	d1f3      	bne.n	800cbb4 <__kernel_rem_pio2f+0x3e8>
 800cbcc:	f10d 0ec8 	add.w	lr, sp, #200	; 0xc8
 800cbd0:	2d00      	cmp	r5, #0
 800cbd2:	4677      	mov	r7, lr
 800cbd4:	f04f 0400 	mov.w	r4, #0
 800cbd8:	ed1f 6a27 	vldr	s12, [pc, #-156]	; 800cb40 <__kernel_rem_pio2f+0x374>
 800cbdc:	db1d      	blt.n	800cc1a <__kernel_rem_pio2f+0x44e>
 800cbde:	4892      	ldr	r0, [pc, #584]	; (800ce28 <__kernel_rem_pio2f+0x65c>)
 800cbe0:	eddf 7a92 	vldr	s15, [pc, #584]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cbe4:	4631      	mov	r1, r6
 800cbe6:	eeb0 7a46 	vmov.f32	s14, s12
 800cbea:	2200      	movs	r2, #0
 800cbec:	e003      	b.n	800cbf6 <__kernel_rem_pio2f+0x42a>
 800cbee:	42a2      	cmp	r2, r4
 800cbf0:	dc08      	bgt.n	800cc04 <__kernel_rem_pio2f+0x438>
 800cbf2:	ecb0 7a01 	vldmia	r0!, {s14}
 800cbf6:	ecf1 6a01 	vldmia	r1!, {s13}
 800cbfa:	3201      	adds	r2, #1
 800cbfc:	4295      	cmp	r5, r2
 800cbfe:	eee6 7a87 	vfma.f32	s15, s13, s14
 800cc02:	daf4      	bge.n	800cbee <__kernel_rem_pio2f+0x422>
 800cc04:	429c      	cmp	r4, r3
 800cc06:	ece7 7a01 	vstmia	r7!, {s15}
 800cc0a:	f1a6 0604 	sub.w	r6, r6, #4
 800cc0e:	f104 0201 	add.w	r2, r4, #1
 800cc12:	d00c      	beq.n	800cc2e <__kernel_rem_pio2f+0x462>
 800cc14:	2d00      	cmp	r5, #0
 800cc16:	4614      	mov	r4, r2
 800cc18:	dae1      	bge.n	800cbde <__kernel_rem_pio2f+0x412>
 800cc1a:	eddf 7a84 	vldr	s15, [pc, #528]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cc1e:	429c      	cmp	r4, r3
 800cc20:	ece7 7a01 	vstmia	r7!, {s15}
 800cc24:	f1a6 0604 	sub.w	r6, r6, #4
 800cc28:	f104 0201 	add.w	r2, r4, #1
 800cc2c:	d1f2      	bne.n	800cc14 <__kernel_rem_pio2f+0x448>
 800cc2e:	9a68      	ldr	r2, [sp, #416]	; 0x1a0
 800cc30:	2a03      	cmp	r2, #3
 800cc32:	d822      	bhi.n	800cc7a <__kernel_rem_pio2f+0x4ae>
 800cc34:	e8df f002 	tbb	[pc, r2]
 800cc38:	4a9f9fb1 	.word	0x4a9f9fb1
 800cc3c:	eddf 7a7b 	vldr	s15, [pc, #492]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cc40:	ed9d 7a32 	vldr	s14, [sp, #200]	; 0xc8
 800cc44:	4642      	mov	r2, r8
 800cc46:	ee37 7a67 	vsub.f32	s14, s14, s15
 800cc4a:	2a00      	cmp	r2, #0
 800cc4c:	f040 80c0 	bne.w	800cdd0 <__kernel_rem_pio2f+0x604>
 800cc50:	9a07      	ldr	r2, [sp, #28]
 800cc52:	2b00      	cmp	r3, #0
 800cc54:	edc2 7a00 	vstr	s15, [r2]
 800cc58:	dd0c      	ble.n	800cc74 <__kernel_rem_pio2f+0x4a8>
 800cc5a:	a933      	add	r1, sp, #204	; 0xcc
 800cc5c:	2201      	movs	r2, #1
 800cc5e:	ecf1 7a01 	vldmia	r1!, {s15}
 800cc62:	3201      	adds	r2, #1
 800cc64:	4293      	cmp	r3, r2
 800cc66:	ee37 7a27 	vadd.f32	s14, s14, s15
 800cc6a:	daf8      	bge.n	800cc5e <__kernel_rem_pio2f+0x492>
 800cc6c:	4643      	mov	r3, r8
 800cc6e:	b10b      	cbz	r3, 800cc74 <__kernel_rem_pio2f+0x4a8>
 800cc70:	eeb1 7a47 	vneg.f32	s14, s14
 800cc74:	9b07      	ldr	r3, [sp, #28]
 800cc76:	ed83 7a01 	vstr	s14, [r3, #4]
 800cc7a:	ee19 3a90 	vmov	r3, s19
 800cc7e:	f003 0007 	and.w	r0, r3, #7
 800cc82:	b05b      	add	sp, #364	; 0x16c
 800cc84:	ecbd 8b04 	vpop	{d8-d9}
 800cc88:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800cc8c:	f10b 31ff 	add.w	r1, fp, #4294967295
 800cc90:	f857 3021 	ldr.w	r3, [r7, r1, lsl #2]
 800cc94:	f003 037f 	and.w	r3, r3, #127	; 0x7f
 800cc98:	f847 3021 	str.w	r3, [r7, r1, lsl #2]
 800cc9c:	e6eb      	b.n	800ca76 <__kernel_rem_pio2f+0x2aa>
 800cc9e:	2308      	movs	r3, #8
 800cca0:	9308      	str	r3, [sp, #32]
 800cca2:	2300      	movs	r3, #0
 800cca4:	9303      	str	r3, [sp, #12]
 800cca6:	e5ac      	b.n	800c802 <__kernel_rem_pio2f+0x36>
 800cca8:	ee19 3a90 	vmov	r3, s19
 800ccac:	f1bb 0f00 	cmp.w	fp, #0
 800ccb0:	f103 0301 	add.w	r3, r3, #1
 800ccb4:	ee09 3a90 	vmov	s19, r3
 800ccb8:	bfc8      	it	gt
 800ccba:	2202      	movgt	r2, #2
 800ccbc:	f73f aeba 	bgt.w	800ca34 <__kernel_rem_pio2f+0x268>
 800ccc0:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 800ccc4:	ee37 8ac8 	vsub.f32	s16, s15, s16
 800ccc8:	2202      	movs	r2, #2
 800ccca:	e642      	b.n	800c952 <__kernel_rem_pio2f+0x186>
 800cccc:	2b00      	cmp	r3, #0
 800ccce:	f340 80a8 	ble.w	800ce22 <__kernel_rem_pio2f+0x656>
 800ccd2:	0099      	lsls	r1, r3, #2
 800ccd4:	aa5a      	add	r2, sp, #360	; 0x168
 800ccd6:	440a      	add	r2, r1
 800ccd8:	f10d 0ec8 	add.w	lr, sp, #200	; 0xc8
 800ccdc:	ed52 6a28 	vldr	s13, [r2, #-160]	; 0xffffff60
 800cce0:	eb0e 0283 	add.w	r2, lr, r3, lsl #2
 800cce4:	ed72 7a01 	vldmdb	r2!, {s15}
 800cce8:	ee37 7aa6 	vadd.f32	s14, s15, s13
 800ccec:	4596      	cmp	lr, r2
 800ccee:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800ccf2:	ed82 7a00 	vstr	s14, [r2]
 800ccf6:	ee77 7aa6 	vadd.f32	s15, s15, s13
 800ccfa:	eef0 6a47 	vmov.f32	s13, s14
 800ccfe:	edc2 7a01 	vstr	s15, [r2, #4]
 800cd02:	d1ef      	bne.n	800cce4 <__kernel_rem_pio2f+0x518>
 800cd04:	2b01      	cmp	r3, #1
 800cd06:	f340 808c 	ble.w	800ce22 <__kernel_rem_pio2f+0x656>
 800cd0a:	f103 4380 	add.w	r3, r3, #1073741824	; 0x40000000
 800cd0e:	3b01      	subs	r3, #1
 800cd10:	aa5a      	add	r2, sp, #360	; 0x168
 800cd12:	009b      	lsls	r3, r3, #2
 800cd14:	4411      	add	r1, r2
 800cd16:	1d1a      	adds	r2, r3, #4
 800cd18:	ed51 6a28 	vldr	s13, [r1, #-160]	; 0xffffff60
 800cd1c:	4472      	add	r2, lr
 800cd1e:	a933      	add	r1, sp, #204	; 0xcc
 800cd20:	ed72 7a01 	vldmdb	r2!, {s15}
 800cd24:	ee37 7aa6 	vadd.f32	s14, s15, s13
 800cd28:	4291      	cmp	r1, r2
 800cd2a:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800cd2e:	ed82 7a00 	vstr	s14, [r2]
 800cd32:	ee77 7aa6 	vadd.f32	s15, s15, s13
 800cd36:	eef0 6a47 	vmov.f32	s13, s14
 800cd3a:	edc2 7a01 	vstr	s15, [r2, #4]
 800cd3e:	d1ef      	bne.n	800cd20 <__kernel_rem_pio2f+0x554>
 800cd40:	3308      	adds	r3, #8
 800cd42:	eddf 7a3a 	vldr	s15, [pc, #232]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cd46:	4473      	add	r3, lr
 800cd48:	f10e 0e08 	add.w	lr, lr, #8
 800cd4c:	ed33 7a01 	vldmdb	r3!, {s14}
 800cd50:	459e      	cmp	lr, r3
 800cd52:	ee77 7a87 	vadd.f32	s15, s15, s14
 800cd56:	d1f9      	bne.n	800cd4c <__kernel_rem_pio2f+0x580>
 800cd58:	4643      	mov	r3, r8
 800cd5a:	eddd 6a32 	vldr	s13, [sp, #200]	; 0xc8
 800cd5e:	ed9d 7a33 	vldr	s14, [sp, #204]	; 0xcc
 800cd62:	2b00      	cmp	r3, #0
 800cd64:	d13d      	bne.n	800cde2 <__kernel_rem_pio2f+0x616>
 800cd66:	9b07      	ldr	r3, [sp, #28]
 800cd68:	edc3 6a00 	vstr	s13, [r3]
 800cd6c:	ed83 7a01 	vstr	s14, [r3, #4]
 800cd70:	edc3 7a02 	vstr	s15, [r3, #8]
 800cd74:	e781      	b.n	800cc7a <__kernel_rem_pio2f+0x4ae>
 800cd76:	eddf 7a2d 	vldr	s15, [pc, #180]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cd7a:	44f4      	add	ip, lr
 800cd7c:	ed3c 7a01 	vldmdb	ip!, {s14}
 800cd80:	45e6      	cmp	lr, ip
 800cd82:	ee77 7a87 	vadd.f32	s15, s15, s14
 800cd86:	d1f9      	bne.n	800cd7c <__kernel_rem_pio2f+0x5b0>
 800cd88:	ed9d 7a32 	vldr	s14, [sp, #200]	; 0xc8
 800cd8c:	4642      	mov	r2, r8
 800cd8e:	ee37 7a67 	vsub.f32	s14, s14, s15
 800cd92:	2a00      	cmp	r2, #0
 800cd94:	f43f af5c 	beq.w	800cc50 <__kernel_rem_pio2f+0x484>
 800cd98:	e01a      	b.n	800cdd0 <__kernel_rem_pio2f+0x604>
 800cd9a:	eddf 7a24 	vldr	s15, [pc, #144]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800cd9e:	44f4      	add	ip, lr
 800cda0:	ed3c 7a01 	vldmdb	ip!, {s14}
 800cda4:	45e6      	cmp	lr, ip
 800cda6:	ee77 7a87 	vadd.f32	s15, s15, s14
 800cdaa:	d1f9      	bne.n	800cda0 <__kernel_rem_pio2f+0x5d4>
 800cdac:	4643      	mov	r3, r8
 800cdae:	b10b      	cbz	r3, 800cdb4 <__kernel_rem_pio2f+0x5e8>
 800cdb0:	eef1 7a67 	vneg.f32	s15, s15
 800cdb4:	9b07      	ldr	r3, [sp, #28]
 800cdb6:	edc3 7a00 	vstr	s15, [r3]
 800cdba:	e75e      	b.n	800cc7a <__kernel_rem_pio2f+0x4ae>
 800cdbc:	2000      	movs	r0, #0
 800cdbe:	e64b      	b.n	800ca58 <__kernel_rem_pio2f+0x28c>
 800cdc0:	eebd 0ac0 	vcvt.s32.f32	s0, s0
 800cdc4:	465b      	mov	r3, fp
 800cdc6:	ee10 2a10 	vmov	r2, s0
 800cdca:	f847 202b 	str.w	r2, [r7, fp, lsl #2]
 800cdce:	e6dd      	b.n	800cb8c <__kernel_rem_pio2f+0x3c0>
 800cdd0:	9a07      	ldr	r2, [sp, #28]
 800cdd2:	eef1 7a67 	vneg.f32	s15, s15
 800cdd6:	2b00      	cmp	r3, #0
 800cdd8:	edc2 7a00 	vstr	s15, [r2]
 800cddc:	f73f af3d 	bgt.w	800cc5a <__kernel_rem_pio2f+0x48e>
 800cde0:	e746      	b.n	800cc70 <__kernel_rem_pio2f+0x4a4>
 800cde2:	9b07      	ldr	r3, [sp, #28]
 800cde4:	eef1 6a66 	vneg.f32	s13, s13
 800cde8:	eeb1 7a47 	vneg.f32	s14, s14
 800cdec:	eef1 7a67 	vneg.f32	s15, s15
 800cdf0:	edc3 6a00 	vstr	s13, [r3]
 800cdf4:	ed83 7a01 	vstr	s14, [r3, #4]
 800cdf8:	edc3 7a02 	vstr	s15, [r3, #8]
 800cdfc:	e73d      	b.n	800cc7a <__kernel_rem_pio2f+0x4ae>
 800cdfe:	9a68      	ldr	r2, [sp, #416]	; 0x1a0
 800ce00:	2a03      	cmp	r2, #3
 800ce02:	f63f af3a 	bhi.w	800cc7a <__kernel_rem_pio2f+0x4ae>
 800ce06:	a101      	add	r1, pc, #4	; (adr r1, 800ce0c <__kernel_rem_pio2f+0x640>)
 800ce08:	f851 f022 	ldr.w	pc, [r1, r2, lsl #2]
 800ce0c:	0800ce1d 	.word	0x0800ce1d
 800ce10:	0800cc3d 	.word	0x0800cc3d
 800ce14:	0800cc3d 	.word	0x0800cc3d
 800ce18:	0800cccd 	.word	0x0800cccd
 800ce1c:	eddf 7a03 	vldr	s15, [pc, #12]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800ce20:	e7c4      	b.n	800cdac <__kernel_rem_pio2f+0x5e0>
 800ce22:	eddf 7a02 	vldr	s15, [pc, #8]	; 800ce2c <__kernel_rem_pio2f+0x660>
 800ce26:	e797      	b.n	800cd58 <__kernel_rem_pio2f+0x58c>
 800ce28:	0800d994 	.word	0x0800d994
 800ce2c:	00000000 	.word	0x00000000

0800ce30 <__kernel_sinf>:
 800ce30:	ee10 3a10 	vmov	r3, s0
 800ce34:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 800ce38:	f1b3 5f48 	cmp.w	r3, #838860800	; 0x32000000
 800ce3c:	da04      	bge.n	800ce48 <__kernel_sinf+0x18>
 800ce3e:	eefd 7ac0 	vcvt.s32.f32	s15, s0
 800ce42:	ee17 3a90 	vmov	r3, s15
 800ce46:	b35b      	cbz	r3, 800cea0 <__kernel_sinf+0x70>
 800ce48:	ee60 7a00 	vmul.f32	s15, s0, s0
 800ce4c:	ed9f 5a15 	vldr	s10, [pc, #84]	; 800cea4 <__kernel_sinf+0x74>
 800ce50:	eddf 5a15 	vldr	s11, [pc, #84]	; 800cea8 <__kernel_sinf+0x78>
 800ce54:	ed9f 6a15 	vldr	s12, [pc, #84]	; 800ceac <__kernel_sinf+0x7c>
 800ce58:	eddf 6a15 	vldr	s13, [pc, #84]	; 800ceb0 <__kernel_sinf+0x80>
 800ce5c:	ed9f 7a15 	vldr	s14, [pc, #84]	; 800ceb4 <__kernel_sinf+0x84>
 800ce60:	eee7 5a85 	vfma.f32	s11, s15, s10
 800ce64:	ee20 5a27 	vmul.f32	s10, s0, s15
 800ce68:	eea5 6aa7 	vfma.f32	s12, s11, s15
 800ce6c:	eee6 6a27 	vfma.f32	s13, s12, s15
 800ce70:	eea6 7aa7 	vfma.f32	s14, s13, s15
 800ce74:	b930      	cbnz	r0, 800ce84 <__kernel_sinf+0x54>
 800ce76:	eddf 6a10 	vldr	s13, [pc, #64]	; 800ceb8 <__kernel_sinf+0x88>
 800ce7a:	eee7 6a87 	vfma.f32	s13, s15, s14
 800ce7e:	eea6 0a85 	vfma.f32	s0, s13, s10
 800ce82:	4770      	bx	lr
 800ce84:	ee27 7a45 	vnmul.f32	s14, s14, s10
 800ce88:	eef6 6a00 	vmov.f32	s13, #96	; 0x3f000000  0.5
 800ce8c:	eea0 7aa6 	vfma.f32	s14, s1, s13
 800ce90:	eddf 6a0a 	vldr	s13, [pc, #40]	; 800cebc <__kernel_sinf+0x8c>
 800ce94:	eed7 0a27 	vfnms.f32	s1, s14, s15
 800ce98:	eee5 0a26 	vfma.f32	s1, s10, s13
 800ce9c:	ee30 0a60 	vsub.f32	s0, s0, s1
 800cea0:	4770      	bx	lr
 800cea2:	bf00      	nop
 800cea4:	2f2ec9d3 	.word	0x2f2ec9d3
 800cea8:	b2d72f34 	.word	0xb2d72f34
 800ceac:	3638ef1b 	.word	0x3638ef1b
 800ceb0:	b9500d01 	.word	0xb9500d01
 800ceb4:	3c088889 	.word	0x3c088889
 800ceb8:	be2aaaab 	.word	0xbe2aaaab
 800cebc:	3e2aaaab 	.word	0x3e2aaaab

0800cec0 <fabsf>:
 800cec0:	ee10 3a10 	vmov	r3, s0
 800cec4:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 800cec8:	ee00 3a10 	vmov	s0, r3
 800cecc:	4770      	bx	lr
 800cece:	bf00      	nop

0800ced0 <floorf>:
 800ced0:	ee10 2a10 	vmov	r2, s0
 800ced4:	f022 4100 	bic.w	r1, r2, #2147483648	; 0x80000000
 800ced8:	0dcb      	lsrs	r3, r1, #23
 800ceda:	3b7f      	subs	r3, #127	; 0x7f
 800cedc:	2b16      	cmp	r3, #22
 800cede:	dc28      	bgt.n	800cf32 <floorf+0x62>
 800cee0:	2b00      	cmp	r3, #0
 800cee2:	db18      	blt.n	800cf16 <floorf+0x46>
 800cee4:	4919      	ldr	r1, [pc, #100]	; (800cf4c <floorf+0x7c>)
 800cee6:	4119      	asrs	r1, r3
 800cee8:	420a      	tst	r2, r1
 800ceea:	d021      	beq.n	800cf30 <floorf+0x60>
 800ceec:	eddf 7a18 	vldr	s15, [pc, #96]	; 800cf50 <floorf+0x80>
 800cef0:	ee70 7a27 	vadd.f32	s15, s0, s15
 800cef4:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800cef8:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800cefc:	dd18      	ble.n	800cf30 <floorf+0x60>
 800cefe:	2a00      	cmp	r2, #0
 800cf00:	da04      	bge.n	800cf0c <floorf+0x3c>
 800cf02:	f44f 0000 	mov.w	r0, #8388608	; 0x800000
 800cf06:	fa40 f303 	asr.w	r3, r0, r3
 800cf0a:	441a      	add	r2, r3
 800cf0c:	ea22 0301 	bic.w	r3, r2, r1
 800cf10:	ee00 3a10 	vmov	s0, r3
 800cf14:	4770      	bx	lr
 800cf16:	eddf 7a0e 	vldr	s15, [pc, #56]	; 800cf50 <floorf+0x80>
 800cf1a:	ee70 7a27 	vadd.f32	s15, s0, s15
 800cf1e:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800cf22:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800cf26:	dd03      	ble.n	800cf30 <floorf+0x60>
 800cf28:	2a00      	cmp	r2, #0
 800cf2a:	db08      	blt.n	800cf3e <floorf+0x6e>
 800cf2c:	ed9f 0a09 	vldr	s0, [pc, #36]	; 800cf54 <floorf+0x84>
 800cf30:	4770      	bx	lr
 800cf32:	f1b1 4fff 	cmp.w	r1, #2139095040	; 0x7f800000
 800cf36:	d3fb      	bcc.n	800cf30 <floorf+0x60>
 800cf38:	ee30 0a00 	vadd.f32	s0, s0, s0
 800cf3c:	4770      	bx	lr
 800cf3e:	2900      	cmp	r1, #0
 800cf40:	eeff 7a00 	vmov.f32	s15, #240	; 0xbf800000 -1.0
 800cf44:	bf18      	it	ne
 800cf46:	eeb0 0a67 	vmovne.f32	s0, s15
 800cf4a:	4770      	bx	lr
 800cf4c:	007fffff 	.word	0x007fffff
 800cf50:	7149f2ca 	.word	0x7149f2ca
 800cf54:	00000000 	.word	0x00000000

0800cf58 <scalbnf>:
 800cf58:	b082      	sub	sp, #8
 800cf5a:	ed8d 0a01 	vstr	s0, [sp, #4]
 800cf5e:	9b01      	ldr	r3, [sp, #4]
 800cf60:	f033 4200 	bics.w	r2, r3, #2147483648	; 0x80000000
 800cf64:	d02a      	beq.n	800cfbc <scalbnf+0x64>
 800cf66:	f1b2 4fff 	cmp.w	r2, #2139095040	; 0x7f800000
 800cf6a:	d223      	bcs.n	800cfb4 <scalbnf+0x5c>
 800cf6c:	f013 4fff 	tst.w	r3, #2139095040	; 0x7f800000
 800cf70:	d128      	bne.n	800cfc4 <scalbnf+0x6c>
 800cf72:	ed9d 7a01 	vldr	s14, [sp, #4]
 800cf76:	eddf 7a2a 	vldr	s15, [pc, #168]	; 800d020 <scalbnf+0xc8>
 800cf7a:	4b2a      	ldr	r3, [pc, #168]	; (800d024 <scalbnf+0xcc>)
 800cf7c:	ee67 7a27 	vmul.f32	s15, s14, s15
 800cf80:	4298      	cmp	r0, r3
 800cf82:	edcd 7a01 	vstr	s15, [sp, #4]
 800cf86:	db37      	blt.n	800cff8 <scalbnf+0xa0>
 800cf88:	9b01      	ldr	r3, [sp, #4]
 800cf8a:	f3c3 52c7 	ubfx	r2, r3, #23, #8
 800cf8e:	3a19      	subs	r2, #25
 800cf90:	4402      	add	r2, r0
 800cf92:	2afe      	cmp	r2, #254	; 0xfe
 800cf94:	dd1a      	ble.n	800cfcc <scalbnf+0x74>
 800cf96:	ed9f 0a24 	vldr	s0, [pc, #144]	; 800d028 <scalbnf+0xd0>
 800cf9a:	ed9f 7a24 	vldr	s14, [pc, #144]	; 800d02c <scalbnf+0xd4>
 800cf9e:	9b01      	ldr	r3, [sp, #4]
 800cfa0:	2b00      	cmp	r3, #0
 800cfa2:	eef0 7a40 	vmov.f32	s15, s0
 800cfa6:	bfb8      	it	lt
 800cfa8:	eeb0 0a47 	vmovlt.f32	s0, s14
 800cfac:	ee20 0a27 	vmul.f32	s0, s0, s15
 800cfb0:	b002      	add	sp, #8
 800cfb2:	4770      	bx	lr
 800cfb4:	ee30 0a00 	vadd.f32	s0, s0, s0
 800cfb8:	b002      	add	sp, #8
 800cfba:	4770      	bx	lr
 800cfbc:	ed9d 0a01 	vldr	s0, [sp, #4]
 800cfc0:	b002      	add	sp, #8
 800cfc2:	4770      	bx	lr
 800cfc4:	0dd2      	lsrs	r2, r2, #23
 800cfc6:	4402      	add	r2, r0
 800cfc8:	2afe      	cmp	r2, #254	; 0xfe
 800cfca:	dce4      	bgt.n	800cf96 <scalbnf+0x3e>
 800cfcc:	2a00      	cmp	r2, #0
 800cfce:	dc0b      	bgt.n	800cfe8 <scalbnf+0x90>
 800cfd0:	f112 0f16 	cmn.w	r2, #22
 800cfd4:	da17      	bge.n	800d006 <scalbnf+0xae>
 800cfd6:	f24c 3350 	movw	r3, #50000	; 0xc350
 800cfda:	4298      	cmp	r0, r3
 800cfdc:	dcdb      	bgt.n	800cf96 <scalbnf+0x3e>
 800cfde:	ed9f 0a14 	vldr	s0, [pc, #80]	; 800d030 <scalbnf+0xd8>
 800cfe2:	ed9f 7a14 	vldr	s14, [pc, #80]	; 800d034 <scalbnf+0xdc>
 800cfe6:	e7da      	b.n	800cf9e <scalbnf+0x46>
 800cfe8:	f023 43ff 	bic.w	r3, r3, #2139095040	; 0x7f800000
 800cfec:	ea43 53c2 	orr.w	r3, r3, r2, lsl #23
 800cff0:	ee00 3a10 	vmov	s0, r3
 800cff4:	b002      	add	sp, #8
 800cff6:	4770      	bx	lr
 800cff8:	ed9f 0a0d 	vldr	s0, [pc, #52]	; 800d030 <scalbnf+0xd8>
 800cffc:	eddd 7a01 	vldr	s15, [sp, #4]
 800d000:	ee27 0a80 	vmul.f32	s0, s15, s0
 800d004:	e7d8      	b.n	800cfb8 <scalbnf+0x60>
 800d006:	3219      	adds	r2, #25
 800d008:	f023 43ff 	bic.w	r3, r3, #2139095040	; 0x7f800000
 800d00c:	ea43 53c2 	orr.w	r3, r3, r2, lsl #23
 800d010:	eddf 7a09 	vldr	s15, [pc, #36]	; 800d038 <scalbnf+0xe0>
 800d014:	ee07 3a10 	vmov	s14, r3
 800d018:	ee27 0a27 	vmul.f32	s0, s14, s15
 800d01c:	e7cc      	b.n	800cfb8 <scalbnf+0x60>
 800d01e:	bf00      	nop
 800d020:	4c000000 	.word	0x4c000000
 800d024:	ffff3cb0 	.word	0xffff3cb0
 800d028:	7149f2ca 	.word	0x7149f2ca
 800d02c:	f149f2ca 	.word	0xf149f2ca
 800d030:	0da24260 	.word	0x0da24260
 800d034:	8da24260 	.word	0x8da24260
 800d038:	33000000 	.word	0x33000000

0800d03c <_init>:
 800d03c:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800d03e:	bf00      	nop
 800d040:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800d042:	bc08      	pop	{r3}
 800d044:	469e      	mov	lr, r3
 800d046:	4770      	bx	lr

0800d048 <_fini>:
 800d048:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800d04a:	bf00      	nop
 800d04c:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800d04e:	bc08      	pop	{r3}
 800d050:	469e      	mov	lr, r3
 800d052:	4770      	bx	lr
