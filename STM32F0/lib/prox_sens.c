#define USE_FULL_LL_DRIVER 

#include "gpio_map.h"
#include "stm32f0xx_ll_bus.h"
#include "stm32f0xx_ll_system.h"
#include "stm32f0xx_ll_rcc.h"
#include "peripheral.h"
#include "prox_sens.h"

#include <string.h>

void prox_sens_init(){
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    /**I2C1 GPIO Configuration
     PB6   ------> I2C1_SCL
     PB7   ------> I2C1_SDA
    */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    LL_GPIO_SetPinMode(COL_AV_I2C_PORT, COL_AV_I2C_SCL,
                       LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(COL_AV_I2C_PORT, COL_AV_I2C_SCL,
                             LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetAFPin_0_7(COL_AV_I2C_PORT, COL_AV_I2C_SCL, LL_GPIO_AF_1);
    LL_GPIO_SetPinSpeed(COL_AV_I2C_PORT, COL_AV_I2C_SCL,
                        LL_GPIO_SPEED_FREQ_HIGH);

    LL_GPIO_SetPinMode(COL_AV_I2C_PORT, COL_AV_I2C_SDA,
                       LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(COL_AV_I2C_PORT, COL_AV_I2C_SDA,
                             LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetAFPin_0_7(COL_AV_I2C_PORT, COL_AV_I2C_SDA, LL_GPIO_AF_1);
    LL_GPIO_SetPinSpeed(COL_AV_I2C_PORT, COL_AV_I2C_SDA,
                        LL_GPIO_SPEED_FREQ_HIGH);

    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C1);

    /*
     * Clock on the I2C peripheral and set it up
    */
    LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_SYSCLK);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C1);
    LL_I2C_DisableAnalogFilter(COL_AV_I2C);
    LL_I2C_SetDigitalFilter(COL_AV_I2C, 1);
    LL_I2C_SetTiming(COL_AV_I2C, COL_AV_I2C_TIMING);
    LL_I2C_DisableClockStretching(COL_AV_I2C);
    LL_I2C_SetMasterAddressingMode(COL_AV_I2C, COL_AV_I2C_ADDR_MODE);
    LL_I2C_SetMode(COL_AV_I2C, COL_AV_I2C_MODE);
    LL_I2C_Enable(COL_AV_I2C);
}

void prox_sens_test(){
    uint8_t value = 0;
    for (uint32_t id = 0x50; id < 0x60; id++){
        LL_I2C_HandleTransfer(COL_AV_I2C, id, LL_I2C_ADDRSLAVE_7BIT, 0, 
            LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_NOSTARTSTOP);
        LL_I2C_GenerateStartCondition(COL_AV_I2C);
        for(int i = 0; i < 100; i++){};
    }
}