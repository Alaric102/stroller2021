
build\\blank.elf:     file format elf32-littlearm

Sections:
Idx Name              Size      VMA       LMA       File off  Algn  Flags
  0 .isr_vector       000000c0  08000000  08000000  00010000  2**0  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .text             000096ec  080000c0  080000c0  000100c0  2**2  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .rodata           0000092c  080097ac  080097ac  000197ac  2**2  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .init_array       00000008  0800a0d8  0800a0d8  0001a0d8  2**2  CONTENTS, ALLOC, LOAD, DATA
  4 .fini_array       00000004  0800a0e0  0800a0e0  0001a0e0  2**2  CONTENTS, ALLOC, LOAD, DATA
  5 .data             000006ec  20000000  0800a0e4  00020000  2**3  CONTENTS, ALLOC, LOAD, DATA
  6 .bss              00000a98  200006ec  0800a7d0  000206ec  2**2  ALLOC
  7 ._user_heap_stack 00000604  20001184  0800a7d0  00021184  2**0  ALLOC
  8 .ARM.attributes   00000028  00000000  00000000  000206ec  2**0  CONTENTS, READONLY
  9 .debug_info       00011442  00000000  00000000  00020714  2**0  CONTENTS, READONLY, DEBUGGING
 10 .debug_abbrev     00002012  00000000  00000000  00031b56  2**0  CONTENTS, READONLY, DEBUGGING
 11 .debug_loc        00004976  00000000  00000000  00033b68  2**0  CONTENTS, READONLY, DEBUGGING
 12 .debug_aranges    00000bf8  00000000  00000000  000384de  2**0  CONTENTS, READONLY, DEBUGGING
 13 .debug_ranges     00000b10  00000000  00000000  000390d6  2**0  CONTENTS, READONLY, DEBUGGING
 14 .debug_line       00007025  00000000  00000000  00039be6  2**0  CONTENTS, READONLY, DEBUGGING
 15 .debug_str        0000466f  00000000  00000000  00040c0b  2**0  CONTENTS, READONLY, DEBUGGING
 16 .comment          00000075  00000000  00000000  0004527a  2**0  CONTENTS, READONLY
 17 .debug_frame      00002f00  00000000  00000000  000452f0  2**2  CONTENTS, READONLY, DEBUGGING

Disassembly of section .text:

080000c0 <__do_global_dtors_aux>:
 80000c0:	b510      	push	{r4, lr}
 80000c2:	4c06      	ldr	r4, [pc, #24]	; (80000dc <__do_global_dtors_aux+0x1c>)
 80000c4:	7823      	ldrb	r3, [r4, #0]
 80000c6:	2b00      	cmp	r3, #0
 80000c8:	d107      	bne.n	80000da <__do_global_dtors_aux+0x1a>
 80000ca:	4b05      	ldr	r3, [pc, #20]	; (80000e0 <__do_global_dtors_aux+0x20>)
 80000cc:	2b00      	cmp	r3, #0
 80000ce:	d002      	beq.n	80000d6 <__do_global_dtors_aux+0x16>
 80000d0:	4804      	ldr	r0, [pc, #16]	; (80000e4 <__do_global_dtors_aux+0x24>)
 80000d2:	e000      	b.n	80000d6 <__do_global_dtors_aux+0x16>
 80000d4:	bf00      	nop
 80000d6:	2301      	movs	r3, #1
 80000d8:	7023      	strb	r3, [r4, #0]
 80000da:	bd10      	pop	{r4, pc}
 80000dc:	200006ec 	.word	0x200006ec
 80000e0:	00000000 	.word	0x00000000
 80000e4:	08009794 	.word	0x08009794

080000e8 <frame_dummy>:
 80000e8:	4b04      	ldr	r3, [pc, #16]	; (80000fc <frame_dummy+0x14>)
 80000ea:	b510      	push	{r4, lr}
 80000ec:	2b00      	cmp	r3, #0
 80000ee:	d003      	beq.n	80000f8 <frame_dummy+0x10>
 80000f0:	4903      	ldr	r1, [pc, #12]	; (8000100 <frame_dummy+0x18>)
 80000f2:	4804      	ldr	r0, [pc, #16]	; (8000104 <frame_dummy+0x1c>)
 80000f4:	e000      	b.n	80000f8 <frame_dummy+0x10>
 80000f6:	bf00      	nop
 80000f8:	bd10      	pop	{r4, pc}
 80000fa:	46c0      	nop			; (mov r8, r8)
 80000fc:	00000000 	.word	0x00000000
 8000100:	200006f0 	.word	0x200006f0
 8000104:	08009794 	.word	0x08009794

08000108 <__udivsi3>:
 8000108:	2200      	movs	r2, #0
 800010a:	0843      	lsrs	r3, r0, #1
 800010c:	428b      	cmp	r3, r1
 800010e:	d374      	bcc.n	80001fa <__udivsi3+0xf2>
 8000110:	0903      	lsrs	r3, r0, #4
 8000112:	428b      	cmp	r3, r1
 8000114:	d35f      	bcc.n	80001d6 <__udivsi3+0xce>
 8000116:	0a03      	lsrs	r3, r0, #8
 8000118:	428b      	cmp	r3, r1
 800011a:	d344      	bcc.n	80001a6 <__udivsi3+0x9e>
 800011c:	0b03      	lsrs	r3, r0, #12
 800011e:	428b      	cmp	r3, r1
 8000120:	d328      	bcc.n	8000174 <__udivsi3+0x6c>
 8000122:	0c03      	lsrs	r3, r0, #16
 8000124:	428b      	cmp	r3, r1
 8000126:	d30d      	bcc.n	8000144 <__udivsi3+0x3c>
 8000128:	22ff      	movs	r2, #255	; 0xff
 800012a:	0209      	lsls	r1, r1, #8
 800012c:	ba12      	rev	r2, r2
 800012e:	0c03      	lsrs	r3, r0, #16
 8000130:	428b      	cmp	r3, r1
 8000132:	d302      	bcc.n	800013a <__udivsi3+0x32>
 8000134:	1212      	asrs	r2, r2, #8
 8000136:	0209      	lsls	r1, r1, #8
 8000138:	d065      	beq.n	8000206 <__udivsi3+0xfe>
 800013a:	0b03      	lsrs	r3, r0, #12
 800013c:	428b      	cmp	r3, r1
 800013e:	d319      	bcc.n	8000174 <__udivsi3+0x6c>
 8000140:	e000      	b.n	8000144 <__udivsi3+0x3c>
 8000142:	0a09      	lsrs	r1, r1, #8
 8000144:	0bc3      	lsrs	r3, r0, #15
 8000146:	428b      	cmp	r3, r1
 8000148:	d301      	bcc.n	800014e <__udivsi3+0x46>
 800014a:	03cb      	lsls	r3, r1, #15
 800014c:	1ac0      	subs	r0, r0, r3
 800014e:	4152      	adcs	r2, r2
 8000150:	0b83      	lsrs	r3, r0, #14
 8000152:	428b      	cmp	r3, r1
 8000154:	d301      	bcc.n	800015a <__udivsi3+0x52>
 8000156:	038b      	lsls	r3, r1, #14
 8000158:	1ac0      	subs	r0, r0, r3
 800015a:	4152      	adcs	r2, r2
 800015c:	0b43      	lsrs	r3, r0, #13
 800015e:	428b      	cmp	r3, r1
 8000160:	d301      	bcc.n	8000166 <__udivsi3+0x5e>
 8000162:	034b      	lsls	r3, r1, #13
 8000164:	1ac0      	subs	r0, r0, r3
 8000166:	4152      	adcs	r2, r2
 8000168:	0b03      	lsrs	r3, r0, #12
 800016a:	428b      	cmp	r3, r1
 800016c:	d301      	bcc.n	8000172 <__udivsi3+0x6a>
 800016e:	030b      	lsls	r3, r1, #12
 8000170:	1ac0      	subs	r0, r0, r3
 8000172:	4152      	adcs	r2, r2
 8000174:	0ac3      	lsrs	r3, r0, #11
 8000176:	428b      	cmp	r3, r1
 8000178:	d301      	bcc.n	800017e <__udivsi3+0x76>
 800017a:	02cb      	lsls	r3, r1, #11
 800017c:	1ac0      	subs	r0, r0, r3
 800017e:	4152      	adcs	r2, r2
 8000180:	0a83      	lsrs	r3, r0, #10
 8000182:	428b      	cmp	r3, r1
 8000184:	d301      	bcc.n	800018a <__udivsi3+0x82>
 8000186:	028b      	lsls	r3, r1, #10
 8000188:	1ac0      	subs	r0, r0, r3
 800018a:	4152      	adcs	r2, r2
 800018c:	0a43      	lsrs	r3, r0, #9
 800018e:	428b      	cmp	r3, r1
 8000190:	d301      	bcc.n	8000196 <__udivsi3+0x8e>
 8000192:	024b      	lsls	r3, r1, #9
 8000194:	1ac0      	subs	r0, r0, r3
 8000196:	4152      	adcs	r2, r2
 8000198:	0a03      	lsrs	r3, r0, #8
 800019a:	428b      	cmp	r3, r1
 800019c:	d301      	bcc.n	80001a2 <__udivsi3+0x9a>
 800019e:	020b      	lsls	r3, r1, #8
 80001a0:	1ac0      	subs	r0, r0, r3
 80001a2:	4152      	adcs	r2, r2
 80001a4:	d2cd      	bcs.n	8000142 <__udivsi3+0x3a>
 80001a6:	09c3      	lsrs	r3, r0, #7
 80001a8:	428b      	cmp	r3, r1
 80001aa:	d301      	bcc.n	80001b0 <__udivsi3+0xa8>
 80001ac:	01cb      	lsls	r3, r1, #7
 80001ae:	1ac0      	subs	r0, r0, r3
 80001b0:	4152      	adcs	r2, r2
 80001b2:	0983      	lsrs	r3, r0, #6
 80001b4:	428b      	cmp	r3, r1
 80001b6:	d301      	bcc.n	80001bc <__udivsi3+0xb4>
 80001b8:	018b      	lsls	r3, r1, #6
 80001ba:	1ac0      	subs	r0, r0, r3
 80001bc:	4152      	adcs	r2, r2
 80001be:	0943      	lsrs	r3, r0, #5
 80001c0:	428b      	cmp	r3, r1
 80001c2:	d301      	bcc.n	80001c8 <__udivsi3+0xc0>
 80001c4:	014b      	lsls	r3, r1, #5
 80001c6:	1ac0      	subs	r0, r0, r3
 80001c8:	4152      	adcs	r2, r2
 80001ca:	0903      	lsrs	r3, r0, #4
 80001cc:	428b      	cmp	r3, r1
 80001ce:	d301      	bcc.n	80001d4 <__udivsi3+0xcc>
 80001d0:	010b      	lsls	r3, r1, #4
 80001d2:	1ac0      	subs	r0, r0, r3
 80001d4:	4152      	adcs	r2, r2
 80001d6:	08c3      	lsrs	r3, r0, #3
 80001d8:	428b      	cmp	r3, r1
 80001da:	d301      	bcc.n	80001e0 <__udivsi3+0xd8>
 80001dc:	00cb      	lsls	r3, r1, #3
 80001de:	1ac0      	subs	r0, r0, r3
 80001e0:	4152      	adcs	r2, r2
 80001e2:	0883      	lsrs	r3, r0, #2
 80001e4:	428b      	cmp	r3, r1
 80001e6:	d301      	bcc.n	80001ec <__udivsi3+0xe4>
 80001e8:	008b      	lsls	r3, r1, #2
 80001ea:	1ac0      	subs	r0, r0, r3
 80001ec:	4152      	adcs	r2, r2
 80001ee:	0843      	lsrs	r3, r0, #1
 80001f0:	428b      	cmp	r3, r1
 80001f2:	d301      	bcc.n	80001f8 <__udivsi3+0xf0>
 80001f4:	004b      	lsls	r3, r1, #1
 80001f6:	1ac0      	subs	r0, r0, r3
 80001f8:	4152      	adcs	r2, r2
 80001fa:	1a41      	subs	r1, r0, r1
 80001fc:	d200      	bcs.n	8000200 <__udivsi3+0xf8>
 80001fe:	4601      	mov	r1, r0
 8000200:	4152      	adcs	r2, r2
 8000202:	4610      	mov	r0, r2
 8000204:	4770      	bx	lr
 8000206:	e7ff      	b.n	8000208 <__udivsi3+0x100>
 8000208:	b501      	push	{r0, lr}
 800020a:	2000      	movs	r0, #0
 800020c:	f000 f8f0 	bl	80003f0 <__aeabi_idiv0>
 8000210:	bd02      	pop	{r1, pc}
 8000212:	46c0      	nop			; (mov r8, r8)

08000214 <__aeabi_uidivmod>:
 8000214:	2900      	cmp	r1, #0
 8000216:	d0f7      	beq.n	8000208 <__udivsi3+0x100>
 8000218:	e776      	b.n	8000108 <__udivsi3>
 800021a:	4770      	bx	lr

0800021c <__divsi3>:
 800021c:	4603      	mov	r3, r0
 800021e:	430b      	orrs	r3, r1
 8000220:	d47f      	bmi.n	8000322 <__divsi3+0x106>
 8000222:	2200      	movs	r2, #0
 8000224:	0843      	lsrs	r3, r0, #1
 8000226:	428b      	cmp	r3, r1
 8000228:	d374      	bcc.n	8000314 <__divsi3+0xf8>
 800022a:	0903      	lsrs	r3, r0, #4
 800022c:	428b      	cmp	r3, r1
 800022e:	d35f      	bcc.n	80002f0 <__divsi3+0xd4>
 8000230:	0a03      	lsrs	r3, r0, #8
 8000232:	428b      	cmp	r3, r1
 8000234:	d344      	bcc.n	80002c0 <__divsi3+0xa4>
 8000236:	0b03      	lsrs	r3, r0, #12
 8000238:	428b      	cmp	r3, r1
 800023a:	d328      	bcc.n	800028e <__divsi3+0x72>
 800023c:	0c03      	lsrs	r3, r0, #16
 800023e:	428b      	cmp	r3, r1
 8000240:	d30d      	bcc.n	800025e <__divsi3+0x42>
 8000242:	22ff      	movs	r2, #255	; 0xff
 8000244:	0209      	lsls	r1, r1, #8
 8000246:	ba12      	rev	r2, r2
 8000248:	0c03      	lsrs	r3, r0, #16
 800024a:	428b      	cmp	r3, r1
 800024c:	d302      	bcc.n	8000254 <__divsi3+0x38>
 800024e:	1212      	asrs	r2, r2, #8
 8000250:	0209      	lsls	r1, r1, #8
 8000252:	d065      	beq.n	8000320 <__divsi3+0x104>
 8000254:	0b03      	lsrs	r3, r0, #12
 8000256:	428b      	cmp	r3, r1
 8000258:	d319      	bcc.n	800028e <__divsi3+0x72>
 800025a:	e000      	b.n	800025e <__divsi3+0x42>
 800025c:	0a09      	lsrs	r1, r1, #8
 800025e:	0bc3      	lsrs	r3, r0, #15
 8000260:	428b      	cmp	r3, r1
 8000262:	d301      	bcc.n	8000268 <__divsi3+0x4c>
 8000264:	03cb      	lsls	r3, r1, #15
 8000266:	1ac0      	subs	r0, r0, r3
 8000268:	4152      	adcs	r2, r2
 800026a:	0b83      	lsrs	r3, r0, #14
 800026c:	428b      	cmp	r3, r1
 800026e:	d301      	bcc.n	8000274 <__divsi3+0x58>
 8000270:	038b      	lsls	r3, r1, #14
 8000272:	1ac0      	subs	r0, r0, r3
 8000274:	4152      	adcs	r2, r2
 8000276:	0b43      	lsrs	r3, r0, #13
 8000278:	428b      	cmp	r3, r1
 800027a:	d301      	bcc.n	8000280 <__divsi3+0x64>
 800027c:	034b      	lsls	r3, r1, #13
 800027e:	1ac0      	subs	r0, r0, r3
 8000280:	4152      	adcs	r2, r2
 8000282:	0b03      	lsrs	r3, r0, #12
 8000284:	428b      	cmp	r3, r1
 8000286:	d301      	bcc.n	800028c <__divsi3+0x70>
 8000288:	030b      	lsls	r3, r1, #12
 800028a:	1ac0      	subs	r0, r0, r3
 800028c:	4152      	adcs	r2, r2
 800028e:	0ac3      	lsrs	r3, r0, #11
 8000290:	428b      	cmp	r3, r1
 8000292:	d301      	bcc.n	8000298 <__divsi3+0x7c>
 8000294:	02cb      	lsls	r3, r1, #11
 8000296:	1ac0      	subs	r0, r0, r3
 8000298:	4152      	adcs	r2, r2
 800029a:	0a83      	lsrs	r3, r0, #10
 800029c:	428b      	cmp	r3, r1
 800029e:	d301      	bcc.n	80002a4 <__divsi3+0x88>
 80002a0:	028b      	lsls	r3, r1, #10
 80002a2:	1ac0      	subs	r0, r0, r3
 80002a4:	4152      	adcs	r2, r2
 80002a6:	0a43      	lsrs	r3, r0, #9
 80002a8:	428b      	cmp	r3, r1
 80002aa:	d301      	bcc.n	80002b0 <__divsi3+0x94>
 80002ac:	024b      	lsls	r3, r1, #9
 80002ae:	1ac0      	subs	r0, r0, r3
 80002b0:	4152      	adcs	r2, r2
 80002b2:	0a03      	lsrs	r3, r0, #8
 80002b4:	428b      	cmp	r3, r1
 80002b6:	d301      	bcc.n	80002bc <__divsi3+0xa0>
 80002b8:	020b      	lsls	r3, r1, #8
 80002ba:	1ac0      	subs	r0, r0, r3
 80002bc:	4152      	adcs	r2, r2
 80002be:	d2cd      	bcs.n	800025c <__divsi3+0x40>
 80002c0:	09c3      	lsrs	r3, r0, #7
 80002c2:	428b      	cmp	r3, r1
 80002c4:	d301      	bcc.n	80002ca <__divsi3+0xae>
 80002c6:	01cb      	lsls	r3, r1, #7
 80002c8:	1ac0      	subs	r0, r0, r3
 80002ca:	4152      	adcs	r2, r2
 80002cc:	0983      	lsrs	r3, r0, #6
 80002ce:	428b      	cmp	r3, r1
 80002d0:	d301      	bcc.n	80002d6 <__divsi3+0xba>
 80002d2:	018b      	lsls	r3, r1, #6
 80002d4:	1ac0      	subs	r0, r0, r3
 80002d6:	4152      	adcs	r2, r2
 80002d8:	0943      	lsrs	r3, r0, #5
 80002da:	428b      	cmp	r3, r1
 80002dc:	d301      	bcc.n	80002e2 <__divsi3+0xc6>
 80002de:	014b      	lsls	r3, r1, #5
 80002e0:	1ac0      	subs	r0, r0, r3
 80002e2:	4152      	adcs	r2, r2
 80002e4:	0903      	lsrs	r3, r0, #4
 80002e6:	428b      	cmp	r3, r1
 80002e8:	d301      	bcc.n	80002ee <__divsi3+0xd2>
 80002ea:	010b      	lsls	r3, r1, #4
 80002ec:	1ac0      	subs	r0, r0, r3
 80002ee:	4152      	adcs	r2, r2
 80002f0:	08c3      	lsrs	r3, r0, #3
 80002f2:	428b      	cmp	r3, r1
 80002f4:	d301      	bcc.n	80002fa <__divsi3+0xde>
 80002f6:	00cb      	lsls	r3, r1, #3
 80002f8:	1ac0      	subs	r0, r0, r3
 80002fa:	4152      	adcs	r2, r2
 80002fc:	0883      	lsrs	r3, r0, #2
 80002fe:	428b      	cmp	r3, r1
 8000300:	d301      	bcc.n	8000306 <__divsi3+0xea>
 8000302:	008b      	lsls	r3, r1, #2
 8000304:	1ac0      	subs	r0, r0, r3
 8000306:	4152      	adcs	r2, r2
 8000308:	0843      	lsrs	r3, r0, #1
 800030a:	428b      	cmp	r3, r1
 800030c:	d301      	bcc.n	8000312 <__divsi3+0xf6>
 800030e:	004b      	lsls	r3, r1, #1
 8000310:	1ac0      	subs	r0, r0, r3
 8000312:	4152      	adcs	r2, r2
 8000314:	1a41      	subs	r1, r0, r1
 8000316:	d200      	bcs.n	800031a <__divsi3+0xfe>
 8000318:	4601      	mov	r1, r0
 800031a:	4152      	adcs	r2, r2
 800031c:	4610      	mov	r0, r2
 800031e:	4770      	bx	lr
 8000320:	e05d      	b.n	80003de <__divsi3+0x1c2>
 8000322:	0fca      	lsrs	r2, r1, #31
 8000324:	d000      	beq.n	8000328 <__divsi3+0x10c>
 8000326:	4249      	negs	r1, r1
 8000328:	1003      	asrs	r3, r0, #32
 800032a:	d300      	bcc.n	800032e <__divsi3+0x112>
 800032c:	4240      	negs	r0, r0
 800032e:	4053      	eors	r3, r2
 8000330:	2200      	movs	r2, #0
 8000332:	469c      	mov	ip, r3
 8000334:	0903      	lsrs	r3, r0, #4
 8000336:	428b      	cmp	r3, r1
 8000338:	d32d      	bcc.n	8000396 <__divsi3+0x17a>
 800033a:	0a03      	lsrs	r3, r0, #8
 800033c:	428b      	cmp	r3, r1
 800033e:	d312      	bcc.n	8000366 <__divsi3+0x14a>
 8000340:	22fc      	movs	r2, #252	; 0xfc
 8000342:	0189      	lsls	r1, r1, #6
 8000344:	ba12      	rev	r2, r2
 8000346:	0a03      	lsrs	r3, r0, #8
 8000348:	428b      	cmp	r3, r1
 800034a:	d30c      	bcc.n	8000366 <__divsi3+0x14a>
 800034c:	0189      	lsls	r1, r1, #6
 800034e:	1192      	asrs	r2, r2, #6
 8000350:	428b      	cmp	r3, r1
 8000352:	d308      	bcc.n	8000366 <__divsi3+0x14a>
 8000354:	0189      	lsls	r1, r1, #6
 8000356:	1192      	asrs	r2, r2, #6
 8000358:	428b      	cmp	r3, r1
 800035a:	d304      	bcc.n	8000366 <__divsi3+0x14a>
 800035c:	0189      	lsls	r1, r1, #6
 800035e:	d03a      	beq.n	80003d6 <__divsi3+0x1ba>
 8000360:	1192      	asrs	r2, r2, #6
 8000362:	e000      	b.n	8000366 <__divsi3+0x14a>
 8000364:	0989      	lsrs	r1, r1, #6
 8000366:	09c3      	lsrs	r3, r0, #7
 8000368:	428b      	cmp	r3, r1
 800036a:	d301      	bcc.n	8000370 <__divsi3+0x154>
 800036c:	01cb      	lsls	r3, r1, #7
 800036e:	1ac0      	subs	r0, r0, r3
 8000370:	4152      	adcs	r2, r2
 8000372:	0983      	lsrs	r3, r0, #6
 8000374:	428b      	cmp	r3, r1
 8000376:	d301      	bcc.n	800037c <__divsi3+0x160>
 8000378:	018b      	lsls	r3, r1, #6
 800037a:	1ac0      	subs	r0, r0, r3
 800037c:	4152      	adcs	r2, r2
 800037e:	0943      	lsrs	r3, r0, #5
 8000380:	428b      	cmp	r3, r1
 8000382:	d301      	bcc.n	8000388 <__divsi3+0x16c>
 8000384:	014b      	lsls	r3, r1, #5
 8000386:	1ac0      	subs	r0, r0, r3
 8000388:	4152      	adcs	r2, r2
 800038a:	0903      	lsrs	r3, r0, #4
 800038c:	428b      	cmp	r3, r1
 800038e:	d301      	bcc.n	8000394 <__divsi3+0x178>
 8000390:	010b      	lsls	r3, r1, #4
 8000392:	1ac0      	subs	r0, r0, r3
 8000394:	4152      	adcs	r2, r2
 8000396:	08c3      	lsrs	r3, r0, #3
 8000398:	428b      	cmp	r3, r1
 800039a:	d301      	bcc.n	80003a0 <__divsi3+0x184>
 800039c:	00cb      	lsls	r3, r1, #3
 800039e:	1ac0      	subs	r0, r0, r3
 80003a0:	4152      	adcs	r2, r2
 80003a2:	0883      	lsrs	r3, r0, #2
 80003a4:	428b      	cmp	r3, r1
 80003a6:	d301      	bcc.n	80003ac <__divsi3+0x190>
 80003a8:	008b      	lsls	r3, r1, #2
 80003aa:	1ac0      	subs	r0, r0, r3
 80003ac:	4152      	adcs	r2, r2
 80003ae:	d2d9      	bcs.n	8000364 <__divsi3+0x148>
 80003b0:	0843      	lsrs	r3, r0, #1
 80003b2:	428b      	cmp	r3, r1
 80003b4:	d301      	bcc.n	80003ba <__divsi3+0x19e>
 80003b6:	004b      	lsls	r3, r1, #1
 80003b8:	1ac0      	subs	r0, r0, r3
 80003ba:	4152      	adcs	r2, r2
 80003bc:	1a41      	subs	r1, r0, r1
 80003be:	d200      	bcs.n	80003c2 <__divsi3+0x1a6>
 80003c0:	4601      	mov	r1, r0
 80003c2:	4663      	mov	r3, ip
 80003c4:	4152      	adcs	r2, r2
 80003c6:	105b      	asrs	r3, r3, #1
 80003c8:	4610      	mov	r0, r2
 80003ca:	d301      	bcc.n	80003d0 <__divsi3+0x1b4>
 80003cc:	4240      	negs	r0, r0
 80003ce:	2b00      	cmp	r3, #0
 80003d0:	d500      	bpl.n	80003d4 <__divsi3+0x1b8>
 80003d2:	4249      	negs	r1, r1
 80003d4:	4770      	bx	lr
 80003d6:	4663      	mov	r3, ip
 80003d8:	105b      	asrs	r3, r3, #1
 80003da:	d300      	bcc.n	80003de <__divsi3+0x1c2>
 80003dc:	4240      	negs	r0, r0
 80003de:	b501      	push	{r0, lr}
 80003e0:	2000      	movs	r0, #0
 80003e2:	f000 f805 	bl	80003f0 <__aeabi_idiv0>
 80003e6:	bd02      	pop	{r1, pc}

080003e8 <__aeabi_idivmod>:
 80003e8:	2900      	cmp	r1, #0
 80003ea:	d0f8      	beq.n	80003de <__divsi3+0x1c2>
 80003ec:	e716      	b.n	800021c <__divsi3>
 80003ee:	4770      	bx	lr

080003f0 <__aeabi_idiv0>:
 80003f0:	4770      	bx	lr
 80003f2:	46c0      	nop			; (mov r8, r8)

080003f4 <Reset_Handler>:
 80003f4:	480d      	ldr	r0, [pc, #52]	; (800042c <LoopForever+0x2>)
 80003f6:	4685      	mov	sp, r0
 80003f8:	480d      	ldr	r0, [pc, #52]	; (8000430 <LoopForever+0x6>)
 80003fa:	490e      	ldr	r1, [pc, #56]	; (8000434 <LoopForever+0xa>)
 80003fc:	4a0e      	ldr	r2, [pc, #56]	; (8000438 <LoopForever+0xe>)
 80003fe:	2300      	movs	r3, #0
 8000400:	e002      	b.n	8000408 <LoopCopyDataInit>

08000402 <CopyDataInit>:
 8000402:	58d4      	ldr	r4, [r2, r3]
 8000404:	50c4      	str	r4, [r0, r3]
 8000406:	3304      	adds	r3, #4

08000408 <LoopCopyDataInit>:
 8000408:	18c4      	adds	r4, r0, r3
 800040a:	428c      	cmp	r4, r1
 800040c:	d3f9      	bcc.n	8000402 <CopyDataInit>
 800040e:	4a0b      	ldr	r2, [pc, #44]	; (800043c <LoopForever+0x12>)
 8000410:	4c0b      	ldr	r4, [pc, #44]	; (8000440 <LoopForever+0x16>)
 8000412:	2300      	movs	r3, #0
 8000414:	e001      	b.n	800041a <LoopFillZerobss>

08000416 <FillZerobss>:
 8000416:	6013      	str	r3, [r2, #0]
 8000418:	3204      	adds	r2, #4

0800041a <LoopFillZerobss>:
 800041a:	42a2      	cmp	r2, r4
 800041c:	d3fb      	bcc.n	8000416 <FillZerobss>
 800041e:	f000 fa1f 	bl	8000860 <SystemInit>
 8000422:	f009 f863 	bl	80094ec <__libc_init_array>
 8000426:	f000 f9bb 	bl	80007a0 <main>

0800042a <LoopForever>:
 800042a:	e7fe      	b.n	800042a <LoopForever>
 800042c:	20002000 	.word	0x20002000
 8000430:	20000000 	.word	0x20000000
 8000434:	200006ec 	.word	0x200006ec
 8000438:	0800a0e4 	.word	0x0800a0e4
 800043c:	200006ec 	.word	0x200006ec
 8000440:	20001184 	.word	0x20001184

08000444 <ADC1_COMP_IRQHandler>:
 8000444:	e7fe      	b.n	8000444 <ADC1_COMP_IRQHandler>
	...

08000448 <NVIC_SetPriority>:
 8000448:	b590      	push	{r4, r7, lr}
 800044a:	b083      	sub	sp, #12
 800044c:	af00      	add	r7, sp, #0
 800044e:	0002      	movs	r2, r0
 8000450:	6039      	str	r1, [r7, #0]
 8000452:	1dfb      	adds	r3, r7, #7
 8000454:	701a      	strb	r2, [r3, #0]
 8000456:	1dfb      	adds	r3, r7, #7
 8000458:	781b      	ldrb	r3, [r3, #0]
 800045a:	2b7f      	cmp	r3, #127	; 0x7f
 800045c:	d932      	bls.n	80004c4 <NVIC_SetPriority+0x7c>
 800045e:	4a2f      	ldr	r2, [pc, #188]	; (800051c <NVIC_SetPriority+0xd4>)
 8000460:	1dfb      	adds	r3, r7, #7
 8000462:	781b      	ldrb	r3, [r3, #0]
 8000464:	0019      	movs	r1, r3
 8000466:	230f      	movs	r3, #15
 8000468:	400b      	ands	r3, r1
 800046a:	3b08      	subs	r3, #8
 800046c:	089b      	lsrs	r3, r3, #2
 800046e:	3306      	adds	r3, #6
 8000470:	009b      	lsls	r3, r3, #2
 8000472:	18d3      	adds	r3, r2, r3
 8000474:	3304      	adds	r3, #4
 8000476:	681b      	ldr	r3, [r3, #0]
 8000478:	1dfa      	adds	r2, r7, #7
 800047a:	7812      	ldrb	r2, [r2, #0]
 800047c:	0011      	movs	r1, r2
 800047e:	2203      	movs	r2, #3
 8000480:	400a      	ands	r2, r1
 8000482:	00d2      	lsls	r2, r2, #3
 8000484:	21ff      	movs	r1, #255	; 0xff
 8000486:	4091      	lsls	r1, r2
 8000488:	000a      	movs	r2, r1
 800048a:	43d2      	mvns	r2, r2
 800048c:	401a      	ands	r2, r3
 800048e:	0011      	movs	r1, r2
 8000490:	683b      	ldr	r3, [r7, #0]
 8000492:	019b      	lsls	r3, r3, #6
 8000494:	22ff      	movs	r2, #255	; 0xff
 8000496:	401a      	ands	r2, r3
 8000498:	1dfb      	adds	r3, r7, #7
 800049a:	781b      	ldrb	r3, [r3, #0]
 800049c:	0018      	movs	r0, r3
 800049e:	2303      	movs	r3, #3
 80004a0:	4003      	ands	r3, r0
 80004a2:	00db      	lsls	r3, r3, #3
 80004a4:	409a      	lsls	r2, r3
 80004a6:	481d      	ldr	r0, [pc, #116]	; (800051c <NVIC_SetPriority+0xd4>)
 80004a8:	1dfb      	adds	r3, r7, #7
 80004aa:	781b      	ldrb	r3, [r3, #0]
 80004ac:	001c      	movs	r4, r3
 80004ae:	230f      	movs	r3, #15
 80004b0:	4023      	ands	r3, r4
 80004b2:	3b08      	subs	r3, #8
 80004b4:	089b      	lsrs	r3, r3, #2
 80004b6:	430a      	orrs	r2, r1
 80004b8:	3306      	adds	r3, #6
 80004ba:	009b      	lsls	r3, r3, #2
 80004bc:	18c3      	adds	r3, r0, r3
 80004be:	3304      	adds	r3, #4
 80004c0:	601a      	str	r2, [r3, #0]
 80004c2:	e027      	b.n	8000514 <NVIC_SetPriority+0xcc>
 80004c4:	4a16      	ldr	r2, [pc, #88]	; (8000520 <NVIC_SetPriority+0xd8>)
 80004c6:	1dfb      	adds	r3, r7, #7
 80004c8:	781b      	ldrb	r3, [r3, #0]
 80004ca:	b25b      	sxtb	r3, r3
 80004cc:	089b      	lsrs	r3, r3, #2
 80004ce:	33c0      	adds	r3, #192	; 0xc0
 80004d0:	009b      	lsls	r3, r3, #2
 80004d2:	589b      	ldr	r3, [r3, r2]
 80004d4:	1dfa      	adds	r2, r7, #7
 80004d6:	7812      	ldrb	r2, [r2, #0]
 80004d8:	0011      	movs	r1, r2
 80004da:	2203      	movs	r2, #3
 80004dc:	400a      	ands	r2, r1
 80004de:	00d2      	lsls	r2, r2, #3
 80004e0:	21ff      	movs	r1, #255	; 0xff
 80004e2:	4091      	lsls	r1, r2
 80004e4:	000a      	movs	r2, r1
 80004e6:	43d2      	mvns	r2, r2
 80004e8:	401a      	ands	r2, r3
 80004ea:	0011      	movs	r1, r2
 80004ec:	683b      	ldr	r3, [r7, #0]
 80004ee:	019b      	lsls	r3, r3, #6
 80004f0:	22ff      	movs	r2, #255	; 0xff
 80004f2:	401a      	ands	r2, r3
 80004f4:	1dfb      	adds	r3, r7, #7
 80004f6:	781b      	ldrb	r3, [r3, #0]
 80004f8:	0018      	movs	r0, r3
 80004fa:	2303      	movs	r3, #3
 80004fc:	4003      	ands	r3, r0
 80004fe:	00db      	lsls	r3, r3, #3
 8000500:	409a      	lsls	r2, r3
 8000502:	4807      	ldr	r0, [pc, #28]	; (8000520 <NVIC_SetPriority+0xd8>)
 8000504:	1dfb      	adds	r3, r7, #7
 8000506:	781b      	ldrb	r3, [r3, #0]
 8000508:	b25b      	sxtb	r3, r3
 800050a:	089b      	lsrs	r3, r3, #2
 800050c:	430a      	orrs	r2, r1
 800050e:	33c0      	adds	r3, #192	; 0xc0
 8000510:	009b      	lsls	r3, r3, #2
 8000512:	501a      	str	r2, [r3, r0]
 8000514:	46c0      	nop			; (mov r8, r8)
 8000516:	46bd      	mov	sp, r7
 8000518:	b003      	add	sp, #12
 800051a:	bd90      	pop	{r4, r7, pc}
 800051c:	e000ed00 	.word	0xe000ed00
 8000520:	e000e100 	.word	0xe000e100

08000524 <SysTick_Config>:
 8000524:	b580      	push	{r7, lr}
 8000526:	b082      	sub	sp, #8
 8000528:	af00      	add	r7, sp, #0
 800052a:	6078      	str	r0, [r7, #4]
 800052c:	687b      	ldr	r3, [r7, #4]
 800052e:	3b01      	subs	r3, #1
 8000530:	4a0c      	ldr	r2, [pc, #48]	; (8000564 <SysTick_Config+0x40>)
 8000532:	4293      	cmp	r3, r2
 8000534:	d901      	bls.n	800053a <SysTick_Config+0x16>
 8000536:	2301      	movs	r3, #1
 8000538:	e010      	b.n	800055c <SysTick_Config+0x38>
 800053a:	4b0b      	ldr	r3, [pc, #44]	; (8000568 <SysTick_Config+0x44>)
 800053c:	687a      	ldr	r2, [r7, #4]
 800053e:	3a01      	subs	r2, #1
 8000540:	605a      	str	r2, [r3, #4]
 8000542:	2301      	movs	r3, #1
 8000544:	425b      	negs	r3, r3
 8000546:	2103      	movs	r1, #3
 8000548:	0018      	movs	r0, r3
 800054a:	f7ff ff7d 	bl	8000448 <NVIC_SetPriority>
 800054e:	4b06      	ldr	r3, [pc, #24]	; (8000568 <SysTick_Config+0x44>)
 8000550:	2200      	movs	r2, #0
 8000552:	609a      	str	r2, [r3, #8]
 8000554:	4b04      	ldr	r3, [pc, #16]	; (8000568 <SysTick_Config+0x44>)
 8000556:	2207      	movs	r2, #7
 8000558:	601a      	str	r2, [r3, #0]
 800055a:	2300      	movs	r3, #0
 800055c:	0018      	movs	r0, r3
 800055e:	46bd      	mov	sp, r7
 8000560:	b002      	add	sp, #8
 8000562:	bd80      	pop	{r7, pc}
 8000564:	00ffffff 	.word	0x00ffffff
 8000568:	e000e010 	.word	0xe000e010

0800056c <LL_RCC_HSE_Enable>:
 800056c:	b580      	push	{r7, lr}
 800056e:	af00      	add	r7, sp, #0
 8000570:	4b04      	ldr	r3, [pc, #16]	; (8000584 <LL_RCC_HSE_Enable+0x18>)
 8000572:	681a      	ldr	r2, [r3, #0]
 8000574:	4b03      	ldr	r3, [pc, #12]	; (8000584 <LL_RCC_HSE_Enable+0x18>)
 8000576:	2180      	movs	r1, #128	; 0x80
 8000578:	0249      	lsls	r1, r1, #9
 800057a:	430a      	orrs	r2, r1
 800057c:	601a      	str	r2, [r3, #0]
 800057e:	46c0      	nop			; (mov r8, r8)
 8000580:	46bd      	mov	sp, r7
 8000582:	bd80      	pop	{r7, pc}
 8000584:	40021000 	.word	0x40021000

08000588 <LL_RCC_HSE_IsReady>:
 8000588:	b580      	push	{r7, lr}
 800058a:	af00      	add	r7, sp, #0
 800058c:	4b06      	ldr	r3, [pc, #24]	; (80005a8 <LL_RCC_HSE_IsReady+0x20>)
 800058e:	681a      	ldr	r2, [r3, #0]
 8000590:	2380      	movs	r3, #128	; 0x80
 8000592:	029b      	lsls	r3, r3, #10
 8000594:	4013      	ands	r3, r2
 8000596:	4a05      	ldr	r2, [pc, #20]	; (80005ac <LL_RCC_HSE_IsReady+0x24>)
 8000598:	4694      	mov	ip, r2
 800059a:	4463      	add	r3, ip
 800059c:	425a      	negs	r2, r3
 800059e:	4153      	adcs	r3, r2
 80005a0:	b2db      	uxtb	r3, r3
 80005a2:	0018      	movs	r0, r3
 80005a4:	46bd      	mov	sp, r7
 80005a6:	bd80      	pop	{r7, pc}
 80005a8:	40021000 	.word	0x40021000
 80005ac:	fffe0000 	.word	0xfffe0000

080005b0 <LL_RCC_SetSysClkSource>:
 80005b0:	b580      	push	{r7, lr}
 80005b2:	b082      	sub	sp, #8
 80005b4:	af00      	add	r7, sp, #0
 80005b6:	6078      	str	r0, [r7, #4]
 80005b8:	4b06      	ldr	r3, [pc, #24]	; (80005d4 <LL_RCC_SetSysClkSource+0x24>)
 80005ba:	685b      	ldr	r3, [r3, #4]
 80005bc:	2203      	movs	r2, #3
 80005be:	4393      	bics	r3, r2
 80005c0:	0019      	movs	r1, r3
 80005c2:	4b04      	ldr	r3, [pc, #16]	; (80005d4 <LL_RCC_SetSysClkSource+0x24>)
 80005c4:	687a      	ldr	r2, [r7, #4]
 80005c6:	430a      	orrs	r2, r1
 80005c8:	605a      	str	r2, [r3, #4]
 80005ca:	46c0      	nop			; (mov r8, r8)
 80005cc:	46bd      	mov	sp, r7
 80005ce:	b002      	add	sp, #8
 80005d0:	bd80      	pop	{r7, pc}
 80005d2:	46c0      	nop			; (mov r8, r8)
 80005d4:	40021000 	.word	0x40021000

080005d8 <LL_RCC_GetSysClkSource>:
 80005d8:	b580      	push	{r7, lr}
 80005da:	af00      	add	r7, sp, #0
 80005dc:	4b03      	ldr	r3, [pc, #12]	; (80005ec <LL_RCC_GetSysClkSource+0x14>)
 80005de:	685b      	ldr	r3, [r3, #4]
 80005e0:	220c      	movs	r2, #12
 80005e2:	4013      	ands	r3, r2
 80005e4:	0018      	movs	r0, r3
 80005e6:	46bd      	mov	sp, r7
 80005e8:	bd80      	pop	{r7, pc}
 80005ea:	46c0      	nop			; (mov r8, r8)
 80005ec:	40021000 	.word	0x40021000

080005f0 <LL_RCC_SetAHBPrescaler>:
 80005f0:	b580      	push	{r7, lr}
 80005f2:	b082      	sub	sp, #8
 80005f4:	af00      	add	r7, sp, #0
 80005f6:	6078      	str	r0, [r7, #4]
 80005f8:	4b06      	ldr	r3, [pc, #24]	; (8000614 <LL_RCC_SetAHBPrescaler+0x24>)
 80005fa:	685b      	ldr	r3, [r3, #4]
 80005fc:	22f0      	movs	r2, #240	; 0xf0
 80005fe:	4393      	bics	r3, r2
 8000600:	0019      	movs	r1, r3
 8000602:	4b04      	ldr	r3, [pc, #16]	; (8000614 <LL_RCC_SetAHBPrescaler+0x24>)
 8000604:	687a      	ldr	r2, [r7, #4]
 8000606:	430a      	orrs	r2, r1
 8000608:	605a      	str	r2, [r3, #4]
 800060a:	46c0      	nop			; (mov r8, r8)
 800060c:	46bd      	mov	sp, r7
 800060e:	b002      	add	sp, #8
 8000610:	bd80      	pop	{r7, pc}
 8000612:	46c0      	nop			; (mov r8, r8)
 8000614:	40021000 	.word	0x40021000

08000618 <LL_RCC_SetAPB1Prescaler>:
 8000618:	b580      	push	{r7, lr}
 800061a:	b082      	sub	sp, #8
 800061c:	af00      	add	r7, sp, #0
 800061e:	6078      	str	r0, [r7, #4]
 8000620:	4b06      	ldr	r3, [pc, #24]	; (800063c <LL_RCC_SetAPB1Prescaler+0x24>)
 8000622:	685b      	ldr	r3, [r3, #4]
 8000624:	4a06      	ldr	r2, [pc, #24]	; (8000640 <LL_RCC_SetAPB1Prescaler+0x28>)
 8000626:	4013      	ands	r3, r2
 8000628:	0019      	movs	r1, r3
 800062a:	4b04      	ldr	r3, [pc, #16]	; (800063c <LL_RCC_SetAPB1Prescaler+0x24>)
 800062c:	687a      	ldr	r2, [r7, #4]
 800062e:	430a      	orrs	r2, r1
 8000630:	605a      	str	r2, [r3, #4]
 8000632:	46c0      	nop			; (mov r8, r8)
 8000634:	46bd      	mov	sp, r7
 8000636:	b002      	add	sp, #8
 8000638:	bd80      	pop	{r7, pc}
 800063a:	46c0      	nop			; (mov r8, r8)
 800063c:	40021000 	.word	0x40021000
 8000640:	fffff8ff 	.word	0xfffff8ff

08000644 <LL_RCC_PLL_Enable>:
 8000644:	b580      	push	{r7, lr}
 8000646:	af00      	add	r7, sp, #0
 8000648:	4b04      	ldr	r3, [pc, #16]	; (800065c <LL_RCC_PLL_Enable+0x18>)
 800064a:	681a      	ldr	r2, [r3, #0]
 800064c:	4b03      	ldr	r3, [pc, #12]	; (800065c <LL_RCC_PLL_Enable+0x18>)
 800064e:	2180      	movs	r1, #128	; 0x80
 8000650:	0449      	lsls	r1, r1, #17
 8000652:	430a      	orrs	r2, r1
 8000654:	601a      	str	r2, [r3, #0]
 8000656:	46c0      	nop			; (mov r8, r8)
 8000658:	46bd      	mov	sp, r7
 800065a:	bd80      	pop	{r7, pc}
 800065c:	40021000 	.word	0x40021000

08000660 <LL_RCC_PLL_IsReady>:
 8000660:	b580      	push	{r7, lr}
 8000662:	af00      	add	r7, sp, #0
 8000664:	4b07      	ldr	r3, [pc, #28]	; (8000684 <LL_RCC_PLL_IsReady+0x24>)
 8000666:	681a      	ldr	r2, [r3, #0]
 8000668:	2380      	movs	r3, #128	; 0x80
 800066a:	049b      	lsls	r3, r3, #18
 800066c:	4013      	ands	r3, r2
 800066e:	22fe      	movs	r2, #254	; 0xfe
 8000670:	0612      	lsls	r2, r2, #24
 8000672:	4694      	mov	ip, r2
 8000674:	4463      	add	r3, ip
 8000676:	425a      	negs	r2, r3
 8000678:	4153      	adcs	r3, r2
 800067a:	b2db      	uxtb	r3, r3
 800067c:	0018      	movs	r0, r3
 800067e:	46bd      	mov	sp, r7
 8000680:	bd80      	pop	{r7, pc}
 8000682:	46c0      	nop			; (mov r8, r8)
 8000684:	40021000 	.word	0x40021000

08000688 <LL_RCC_PLL_ConfigDomain_SYS>:
 8000688:	b580      	push	{r7, lr}
 800068a:	b082      	sub	sp, #8
 800068c:	af00      	add	r7, sp, #0
 800068e:	6078      	str	r0, [r7, #4]
 8000690:	6039      	str	r1, [r7, #0]
 8000692:	4b0e      	ldr	r3, [pc, #56]	; (80006cc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8000694:	685b      	ldr	r3, [r3, #4]
 8000696:	4a0e      	ldr	r2, [pc, #56]	; (80006d0 <LL_RCC_PLL_ConfigDomain_SYS+0x48>)
 8000698:	4013      	ands	r3, r2
 800069a:	0019      	movs	r1, r3
 800069c:	687a      	ldr	r2, [r7, #4]
 800069e:	2380      	movs	r3, #128	; 0x80
 80006a0:	025b      	lsls	r3, r3, #9
 80006a2:	401a      	ands	r2, r3
 80006a4:	683b      	ldr	r3, [r7, #0]
 80006a6:	431a      	orrs	r2, r3
 80006a8:	4b08      	ldr	r3, [pc, #32]	; (80006cc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 80006aa:	430a      	orrs	r2, r1
 80006ac:	605a      	str	r2, [r3, #4]
 80006ae:	4b07      	ldr	r3, [pc, #28]	; (80006cc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 80006b0:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80006b2:	220f      	movs	r2, #15
 80006b4:	4393      	bics	r3, r2
 80006b6:	0019      	movs	r1, r3
 80006b8:	687b      	ldr	r3, [r7, #4]
 80006ba:	220f      	movs	r2, #15
 80006bc:	401a      	ands	r2, r3
 80006be:	4b03      	ldr	r3, [pc, #12]	; (80006cc <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 80006c0:	430a      	orrs	r2, r1
 80006c2:	62da      	str	r2, [r3, #44]	; 0x2c
 80006c4:	46c0      	nop			; (mov r8, r8)
 80006c6:	46bd      	mov	sp, r7
 80006c8:	b002      	add	sp, #8
 80006ca:	bd80      	pop	{r7, pc}
 80006cc:	40021000 	.word	0x40021000
 80006d0:	ffc2ffff 	.word	0xffc2ffff

080006d4 <LL_FLASH_SetLatency>:
 80006d4:	b580      	push	{r7, lr}
 80006d6:	b082      	sub	sp, #8
 80006d8:	af00      	add	r7, sp, #0
 80006da:	6078      	str	r0, [r7, #4]
 80006dc:	4b06      	ldr	r3, [pc, #24]	; (80006f8 <LL_FLASH_SetLatency+0x24>)
 80006de:	681b      	ldr	r3, [r3, #0]
 80006e0:	2201      	movs	r2, #1
 80006e2:	4393      	bics	r3, r2
 80006e4:	0019      	movs	r1, r3
 80006e6:	4b04      	ldr	r3, [pc, #16]	; (80006f8 <LL_FLASH_SetLatency+0x24>)
 80006e8:	687a      	ldr	r2, [r7, #4]
 80006ea:	430a      	orrs	r2, r1
 80006ec:	601a      	str	r2, [r3, #0]
 80006ee:	46c0      	nop			; (mov r8, r8)
 80006f0:	46bd      	mov	sp, r7
 80006f2:	b002      	add	sp, #8
 80006f4:	bd80      	pop	{r7, pc}
 80006f6:	46c0      	nop			; (mov r8, r8)
 80006f8:	40022000 	.word	0x40022000

080006fc <rcc_config>:
 80006fc:	b580      	push	{r7, lr}
 80006fe:	af00      	add	r7, sp, #0
 8000700:	2001      	movs	r0, #1
 8000702:	f7ff ffe7 	bl	80006d4 <LL_FLASH_SetLatency>
 8000706:	f7ff ff31 	bl	800056c <LL_RCC_HSE_Enable>
 800070a:	46c0      	nop			; (mov r8, r8)
 800070c:	f7ff ff3c 	bl	8000588 <LL_RCC_HSE_IsReady>
 8000710:	0003      	movs	r3, r0
 8000712:	2b01      	cmp	r3, #1
 8000714:	d1fa      	bne.n	800070c <rcc_config+0x10>
 8000716:	23a0      	movs	r3, #160	; 0xa0
 8000718:	039b      	lsls	r3, r3, #14
 800071a:	4a13      	ldr	r2, [pc, #76]	; (8000768 <rcc_config+0x6c>)
 800071c:	0019      	movs	r1, r3
 800071e:	0010      	movs	r0, r2
 8000720:	f7ff ffb2 	bl	8000688 <LL_RCC_PLL_ConfigDomain_SYS>
 8000724:	f7ff ff8e 	bl	8000644 <LL_RCC_PLL_Enable>
 8000728:	46c0      	nop			; (mov r8, r8)
 800072a:	f7ff ff99 	bl	8000660 <LL_RCC_PLL_IsReady>
 800072e:	0003      	movs	r3, r0
 8000730:	2b01      	cmp	r3, #1
 8000732:	d1fa      	bne.n	800072a <rcc_config+0x2e>
 8000734:	2000      	movs	r0, #0
 8000736:	f7ff ff5b 	bl	80005f0 <LL_RCC_SetAHBPrescaler>
 800073a:	2002      	movs	r0, #2
 800073c:	f7ff ff38 	bl	80005b0 <LL_RCC_SetSysClkSource>
 8000740:	46c0      	nop			; (mov r8, r8)
 8000742:	f7ff ff49 	bl	80005d8 <LL_RCC_GetSysClkSource>
 8000746:	0003      	movs	r3, r0
 8000748:	2b08      	cmp	r3, #8
 800074a:	d1fa      	bne.n	8000742 <rcc_config+0x46>
 800074c:	2000      	movs	r0, #0
 800074e:	f7ff ff63 	bl	8000618 <LL_RCC_SetAPB1Prescaler>
 8000752:	4b06      	ldr	r3, [pc, #24]	; (800076c <rcc_config+0x70>)
 8000754:	0018      	movs	r0, r3
 8000756:	f7ff fee5 	bl	8000524 <SysTick_Config>
 800075a:	4b05      	ldr	r3, [pc, #20]	; (8000770 <rcc_config+0x74>)
 800075c:	4a05      	ldr	r2, [pc, #20]	; (8000774 <rcc_config+0x78>)
 800075e:	601a      	str	r2, [r3, #0]
 8000760:	46c0      	nop			; (mov r8, r8)
 8000762:	46bd      	mov	sp, r7
 8000764:	bd80      	pop	{r7, pc}
 8000766:	46c0      	nop			; (mov r8, r8)
 8000768:	00010001 	.word	0x00010001
 800076c:	0000bb80 	.word	0x0000bb80
 8000770:	20000000 	.word	0x20000000
 8000774:	02dc6c00 	.word	0x02dc6c00

08000778 <SysTick_Handler>:
 8000778:	b580      	push	{r7, lr}
 800077a:	af00      	add	r7, sp, #0
 800077c:	4b06      	ldr	r3, [pc, #24]	; (8000798 <SysTick_Handler+0x20>)
 800077e:	681b      	ldr	r3, [r3, #0]
 8000780:	3301      	adds	r3, #1
 8000782:	4906      	ldr	r1, [pc, #24]	; (800079c <SysTick_Handler+0x24>)
 8000784:	0018      	movs	r0, r3
 8000786:	f7ff fe2f 	bl	80003e8 <__aeabi_idivmod>
 800078a:	000b      	movs	r3, r1
 800078c:	001a      	movs	r2, r3
 800078e:	4b02      	ldr	r3, [pc, #8]	; (8000798 <SysTick_Handler+0x20>)
 8000790:	601a      	str	r2, [r3, #0]
 8000792:	46c0      	nop			; (mov r8, r8)
 8000794:	46bd      	mov	sp, r7
 8000796:	bd80      	pop	{r7, pc}
 8000798:	20000708 	.word	0x20000708
 800079c:	00002710 	.word	0x00002710

080007a0 <main>:
 80007a0:	b580      	push	{r7, lr}
 80007a2:	b084      	sub	sp, #16
 80007a4:	af00      	add	r7, sp, #0
 80007a6:	2300      	movs	r3, #0
 80007a8:	60bb      	str	r3, [r7, #8]
 80007aa:	2300      	movs	r3, #0
 80007ac:	607b      	str	r3, [r7, #4]
 80007ae:	2300      	movs	r3, #0
 80007b0:	603b      	str	r3, [r7, #0]
 80007b2:	2300      	movs	r3, #0
 80007b4:	60fb      	str	r3, [r7, #12]
 80007b6:	f7ff ffa1 	bl	80006fc <rcc_config>
 80007ba:	f002 fc2b 	bl	8003014 <term_init>
 80007be:	f001 fe3d 	bl	800243c <dynamixel_init>
 80007c2:	f000 fbb1 	bl	8000f28 <coll_avoid_init>
 80007c6:	f002 f879 	bl	80028bc <err_man_init>
 80007ca:	f000 fbf9 	bl	8000fc0 <col_av_read_status>
 80007ce:	1e03      	subs	r3, r0, #0
 80007d0:	d036      	beq.n	8000840 <main+0xa0>
 80007d2:	68fb      	ldr	r3, [r7, #12]
 80007d4:	1c5a      	adds	r2, r3, #1
 80007d6:	23fa      	movs	r3, #250	; 0xfa
 80007d8:	0099      	lsls	r1, r3, #2
 80007da:	0010      	movs	r0, r2
 80007dc:	f7ff fe04 	bl	80003e8 <__aeabi_idivmod>
 80007e0:	000b      	movs	r3, r1
 80007e2:	60fb      	str	r3, [r7, #12]
 80007e4:	68fb      	ldr	r3, [r7, #12]
 80007e6:	2b00      	cmp	r3, #0
 80007e8:	d133      	bne.n	8000852 <main+0xb2>
 80007ea:	2300      	movs	r3, #0
 80007ec:	60fb      	str	r3, [r7, #12]
 80007ee:	f000 fc0b 	bl	8001008 <col_av_set_block>
 80007f2:	46c0      	nop			; (mov r8, r8)
 80007f4:	f000 fc1c 	bl	8001030 <col_av_get_ack_block>
 80007f8:	1e03      	subs	r3, r0, #0
 80007fa:	d0fb      	beq.n	80007f4 <main+0x54>
 80007fc:	4b16      	ldr	r3, [pc, #88]	; (8000858 <main+0xb8>)
 80007fe:	681b      	ldr	r3, [r3, #0]
 8000800:	60bb      	str	r3, [r7, #8]
 8000802:	f000 fbb3 	bl	8000f6c <reload_sensors>
 8000806:	4b14      	ldr	r3, [pc, #80]	; (8000858 <main+0xb8>)
 8000808:	681b      	ldr	r3, [r3, #0]
 800080a:	607b      	str	r3, [r7, #4]
 800080c:	687a      	ldr	r2, [r7, #4]
 800080e:	68bb      	ldr	r3, [r7, #8]
 8000810:	429a      	cmp	r2, r3
 8000812:	db03      	blt.n	800081c <main+0x7c>
 8000814:	687a      	ldr	r2, [r7, #4]
 8000816:	68bb      	ldr	r3, [r7, #8]
 8000818:	1ad3      	subs	r3, r2, r3
 800081a:	e004      	b.n	8000826 <main+0x86>
 800081c:	687b      	ldr	r3, [r7, #4]
 800081e:	4a0f      	ldr	r2, [pc, #60]	; (800085c <main+0xbc>)
 8000820:	189a      	adds	r2, r3, r2
 8000822:	68bb      	ldr	r3, [r7, #8]
 8000824:	1ad3      	subs	r3, r2, r3
 8000826:	603b      	str	r3, [r7, #0]
 8000828:	683b      	ldr	r3, [r7, #0]
 800082a:	0018      	movs	r0, r3
 800082c:	f002 f824 	bl	8002878 <err_man_set_time>
 8000830:	f000 fc08 	bl	8001044 <col_av_any>
 8000834:	0003      	movs	r3, r0
 8000836:	0018      	movs	r0, r3
 8000838:	f000 fbcc 	bl	8000fd4 <col_av_set_status>
 800083c:	f000 fbee 	bl	800101c <col_av_clr_block>
 8000840:	f002 f854 	bl	80028ec <er_man_disp_get>
 8000844:	1e03      	subs	r3, r0, #0
 8000846:	d0c0      	beq.n	80007ca <main+0x2a>
 8000848:	f001 ffba 	bl	80027c0 <err_man_show_err>
 800084c:	f002 f862 	bl	8002914 <er_man_disp_clr>
 8000850:	e7bb      	b.n	80007ca <main+0x2a>
 8000852:	46c0      	nop			; (mov r8, r8)
 8000854:	e7b9      	b.n	80007ca <main+0x2a>
 8000856:	46c0      	nop			; (mov r8, r8)
 8000858:	20000708 	.word	0x20000708
 800085c:	00002710 	.word	0x00002710

08000860 <SystemInit>:
 8000860:	b580      	push	{r7, lr}
 8000862:	af00      	add	r7, sp, #0
 8000864:	4b1a      	ldr	r3, [pc, #104]	; (80008d0 <SystemInit+0x70>)
 8000866:	681a      	ldr	r2, [r3, #0]
 8000868:	4b19      	ldr	r3, [pc, #100]	; (80008d0 <SystemInit+0x70>)
 800086a:	2101      	movs	r1, #1
 800086c:	430a      	orrs	r2, r1
 800086e:	601a      	str	r2, [r3, #0]
 8000870:	4b17      	ldr	r3, [pc, #92]	; (80008d0 <SystemInit+0x70>)
 8000872:	685a      	ldr	r2, [r3, #4]
 8000874:	4b16      	ldr	r3, [pc, #88]	; (80008d0 <SystemInit+0x70>)
 8000876:	4917      	ldr	r1, [pc, #92]	; (80008d4 <SystemInit+0x74>)
 8000878:	400a      	ands	r2, r1
 800087a:	605a      	str	r2, [r3, #4]
 800087c:	4b14      	ldr	r3, [pc, #80]	; (80008d0 <SystemInit+0x70>)
 800087e:	681a      	ldr	r2, [r3, #0]
 8000880:	4b13      	ldr	r3, [pc, #76]	; (80008d0 <SystemInit+0x70>)
 8000882:	4915      	ldr	r1, [pc, #84]	; (80008d8 <SystemInit+0x78>)
 8000884:	400a      	ands	r2, r1
 8000886:	601a      	str	r2, [r3, #0]
 8000888:	4b11      	ldr	r3, [pc, #68]	; (80008d0 <SystemInit+0x70>)
 800088a:	681a      	ldr	r2, [r3, #0]
 800088c:	4b10      	ldr	r3, [pc, #64]	; (80008d0 <SystemInit+0x70>)
 800088e:	4913      	ldr	r1, [pc, #76]	; (80008dc <SystemInit+0x7c>)
 8000890:	400a      	ands	r2, r1
 8000892:	601a      	str	r2, [r3, #0]
 8000894:	4b0e      	ldr	r3, [pc, #56]	; (80008d0 <SystemInit+0x70>)
 8000896:	685a      	ldr	r2, [r3, #4]
 8000898:	4b0d      	ldr	r3, [pc, #52]	; (80008d0 <SystemInit+0x70>)
 800089a:	4911      	ldr	r1, [pc, #68]	; (80008e0 <SystemInit+0x80>)
 800089c:	400a      	ands	r2, r1
 800089e:	605a      	str	r2, [r3, #4]
 80008a0:	4b0b      	ldr	r3, [pc, #44]	; (80008d0 <SystemInit+0x70>)
 80008a2:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 80008a4:	4b0a      	ldr	r3, [pc, #40]	; (80008d0 <SystemInit+0x70>)
 80008a6:	210f      	movs	r1, #15
 80008a8:	438a      	bics	r2, r1
 80008aa:	62da      	str	r2, [r3, #44]	; 0x2c
 80008ac:	4b08      	ldr	r3, [pc, #32]	; (80008d0 <SystemInit+0x70>)
 80008ae:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 80008b0:	4b07      	ldr	r3, [pc, #28]	; (80008d0 <SystemInit+0x70>)
 80008b2:	490c      	ldr	r1, [pc, #48]	; (80008e4 <SystemInit+0x84>)
 80008b4:	400a      	ands	r2, r1
 80008b6:	631a      	str	r2, [r3, #48]	; 0x30
 80008b8:	4b05      	ldr	r3, [pc, #20]	; (80008d0 <SystemInit+0x70>)
 80008ba:	6b5a      	ldr	r2, [r3, #52]	; 0x34
 80008bc:	4b04      	ldr	r3, [pc, #16]	; (80008d0 <SystemInit+0x70>)
 80008be:	2101      	movs	r1, #1
 80008c0:	438a      	bics	r2, r1
 80008c2:	635a      	str	r2, [r3, #52]	; 0x34
 80008c4:	4b02      	ldr	r3, [pc, #8]	; (80008d0 <SystemInit+0x70>)
 80008c6:	2200      	movs	r2, #0
 80008c8:	609a      	str	r2, [r3, #8]
 80008ca:	46c0      	nop			; (mov r8, r8)
 80008cc:	46bd      	mov	sp, r7
 80008ce:	bd80      	pop	{r7, pc}
 80008d0:	40021000 	.word	0x40021000
 80008d4:	f8ffb80c 	.word	0xf8ffb80c
 80008d8:	fef6ffff 	.word	0xfef6ffff
 80008dc:	fffbffff 	.word	0xfffbffff
 80008e0:	ffc0ffff 	.word	0xffc0ffff
 80008e4:	fffffeac 	.word	0xfffffeac

080008e8 <NVIC_EnableIRQ>:
 80008e8:	b580      	push	{r7, lr}
 80008ea:	b082      	sub	sp, #8
 80008ec:	af00      	add	r7, sp, #0
 80008ee:	0002      	movs	r2, r0
 80008f0:	1dfb      	adds	r3, r7, #7
 80008f2:	701a      	strb	r2, [r3, #0]
 80008f4:	1dfb      	adds	r3, r7, #7
 80008f6:	781b      	ldrb	r3, [r3, #0]
 80008f8:	001a      	movs	r2, r3
 80008fa:	231f      	movs	r3, #31
 80008fc:	401a      	ands	r2, r3
 80008fe:	4b04      	ldr	r3, [pc, #16]	; (8000910 <NVIC_EnableIRQ+0x28>)
 8000900:	2101      	movs	r1, #1
 8000902:	4091      	lsls	r1, r2
 8000904:	000a      	movs	r2, r1
 8000906:	601a      	str	r2, [r3, #0]
 8000908:	46c0      	nop			; (mov r8, r8)
 800090a:	46bd      	mov	sp, r7
 800090c:	b002      	add	sp, #8
 800090e:	bd80      	pop	{r7, pc}
 8000910:	e000e100 	.word	0xe000e100

08000914 <NVIC_DisableIRQ>:
 8000914:	b580      	push	{r7, lr}
 8000916:	b082      	sub	sp, #8
 8000918:	af00      	add	r7, sp, #0
 800091a:	0002      	movs	r2, r0
 800091c:	1dfb      	adds	r3, r7, #7
 800091e:	701a      	strb	r2, [r3, #0]
 8000920:	1dfb      	adds	r3, r7, #7
 8000922:	781b      	ldrb	r3, [r3, #0]
 8000924:	001a      	movs	r2, r3
 8000926:	231f      	movs	r3, #31
 8000928:	4013      	ands	r3, r2
 800092a:	4905      	ldr	r1, [pc, #20]	; (8000940 <NVIC_DisableIRQ+0x2c>)
 800092c:	2201      	movs	r2, #1
 800092e:	409a      	lsls	r2, r3
 8000930:	0013      	movs	r3, r2
 8000932:	2280      	movs	r2, #128	; 0x80
 8000934:	508b      	str	r3, [r1, r2]
 8000936:	46c0      	nop			; (mov r8, r8)
 8000938:	46bd      	mov	sp, r7
 800093a:	b002      	add	sp, #8
 800093c:	bd80      	pop	{r7, pc}
 800093e:	46c0      	nop			; (mov r8, r8)
 8000940:	e000e100 	.word	0xe000e100

08000944 <NVIC_SetPriority>:
 8000944:	b590      	push	{r4, r7, lr}
 8000946:	b083      	sub	sp, #12
 8000948:	af00      	add	r7, sp, #0
 800094a:	0002      	movs	r2, r0
 800094c:	6039      	str	r1, [r7, #0]
 800094e:	1dfb      	adds	r3, r7, #7
 8000950:	701a      	strb	r2, [r3, #0]
 8000952:	1dfb      	adds	r3, r7, #7
 8000954:	781b      	ldrb	r3, [r3, #0]
 8000956:	2b7f      	cmp	r3, #127	; 0x7f
 8000958:	d932      	bls.n	80009c0 <NVIC_SetPriority+0x7c>
 800095a:	4a2f      	ldr	r2, [pc, #188]	; (8000a18 <NVIC_SetPriority+0xd4>)
 800095c:	1dfb      	adds	r3, r7, #7
 800095e:	781b      	ldrb	r3, [r3, #0]
 8000960:	0019      	movs	r1, r3
 8000962:	230f      	movs	r3, #15
 8000964:	400b      	ands	r3, r1
 8000966:	3b08      	subs	r3, #8
 8000968:	089b      	lsrs	r3, r3, #2
 800096a:	3306      	adds	r3, #6
 800096c:	009b      	lsls	r3, r3, #2
 800096e:	18d3      	adds	r3, r2, r3
 8000970:	3304      	adds	r3, #4
 8000972:	681b      	ldr	r3, [r3, #0]
 8000974:	1dfa      	adds	r2, r7, #7
 8000976:	7812      	ldrb	r2, [r2, #0]
 8000978:	0011      	movs	r1, r2
 800097a:	2203      	movs	r2, #3
 800097c:	400a      	ands	r2, r1
 800097e:	00d2      	lsls	r2, r2, #3
 8000980:	21ff      	movs	r1, #255	; 0xff
 8000982:	4091      	lsls	r1, r2
 8000984:	000a      	movs	r2, r1
 8000986:	43d2      	mvns	r2, r2
 8000988:	401a      	ands	r2, r3
 800098a:	0011      	movs	r1, r2
 800098c:	683b      	ldr	r3, [r7, #0]
 800098e:	019b      	lsls	r3, r3, #6
 8000990:	22ff      	movs	r2, #255	; 0xff
 8000992:	401a      	ands	r2, r3
 8000994:	1dfb      	adds	r3, r7, #7
 8000996:	781b      	ldrb	r3, [r3, #0]
 8000998:	0018      	movs	r0, r3
 800099a:	2303      	movs	r3, #3
 800099c:	4003      	ands	r3, r0
 800099e:	00db      	lsls	r3, r3, #3
 80009a0:	409a      	lsls	r2, r3
 80009a2:	481d      	ldr	r0, [pc, #116]	; (8000a18 <NVIC_SetPriority+0xd4>)
 80009a4:	1dfb      	adds	r3, r7, #7
 80009a6:	781b      	ldrb	r3, [r3, #0]
 80009a8:	001c      	movs	r4, r3
 80009aa:	230f      	movs	r3, #15
 80009ac:	4023      	ands	r3, r4
 80009ae:	3b08      	subs	r3, #8
 80009b0:	089b      	lsrs	r3, r3, #2
 80009b2:	430a      	orrs	r2, r1
 80009b4:	3306      	adds	r3, #6
 80009b6:	009b      	lsls	r3, r3, #2
 80009b8:	18c3      	adds	r3, r0, r3
 80009ba:	3304      	adds	r3, #4
 80009bc:	601a      	str	r2, [r3, #0]
 80009be:	e027      	b.n	8000a10 <NVIC_SetPriority+0xcc>
 80009c0:	4a16      	ldr	r2, [pc, #88]	; (8000a1c <NVIC_SetPriority+0xd8>)
 80009c2:	1dfb      	adds	r3, r7, #7
 80009c4:	781b      	ldrb	r3, [r3, #0]
 80009c6:	b25b      	sxtb	r3, r3
 80009c8:	089b      	lsrs	r3, r3, #2
 80009ca:	33c0      	adds	r3, #192	; 0xc0
 80009cc:	009b      	lsls	r3, r3, #2
 80009ce:	589b      	ldr	r3, [r3, r2]
 80009d0:	1dfa      	adds	r2, r7, #7
 80009d2:	7812      	ldrb	r2, [r2, #0]
 80009d4:	0011      	movs	r1, r2
 80009d6:	2203      	movs	r2, #3
 80009d8:	400a      	ands	r2, r1
 80009da:	00d2      	lsls	r2, r2, #3
 80009dc:	21ff      	movs	r1, #255	; 0xff
 80009de:	4091      	lsls	r1, r2
 80009e0:	000a      	movs	r2, r1
 80009e2:	43d2      	mvns	r2, r2
 80009e4:	401a      	ands	r2, r3
 80009e6:	0011      	movs	r1, r2
 80009e8:	683b      	ldr	r3, [r7, #0]
 80009ea:	019b      	lsls	r3, r3, #6
 80009ec:	22ff      	movs	r2, #255	; 0xff
 80009ee:	401a      	ands	r2, r3
 80009f0:	1dfb      	adds	r3, r7, #7
 80009f2:	781b      	ldrb	r3, [r3, #0]
 80009f4:	0018      	movs	r0, r3
 80009f6:	2303      	movs	r3, #3
 80009f8:	4003      	ands	r3, r0
 80009fa:	00db      	lsls	r3, r3, #3
 80009fc:	409a      	lsls	r2, r3
 80009fe:	4807      	ldr	r0, [pc, #28]	; (8000a1c <NVIC_SetPriority+0xd8>)
 8000a00:	1dfb      	adds	r3, r7, #7
 8000a02:	781b      	ldrb	r3, [r3, #0]
 8000a04:	b25b      	sxtb	r3, r3
 8000a06:	089b      	lsrs	r3, r3, #2
 8000a08:	430a      	orrs	r2, r1
 8000a0a:	33c0      	adds	r3, #192	; 0xc0
 8000a0c:	009b      	lsls	r3, r3, #2
 8000a0e:	501a      	str	r2, [r3, r0]
 8000a10:	46c0      	nop			; (mov r8, r8)
 8000a12:	46bd      	mov	sp, r7
 8000a14:	b003      	add	sp, #12
 8000a16:	bd90      	pop	{r4, r7, pc}
 8000a18:	e000ed00 	.word	0xe000ed00
 8000a1c:	e000e100 	.word	0xe000e100

08000a20 <LL_GPIO_SetOutputPin>:
 8000a20:	b580      	push	{r7, lr}
 8000a22:	b082      	sub	sp, #8
 8000a24:	af00      	add	r7, sp, #0
 8000a26:	6078      	str	r0, [r7, #4]
 8000a28:	6039      	str	r1, [r7, #0]
 8000a2a:	687b      	ldr	r3, [r7, #4]
 8000a2c:	683a      	ldr	r2, [r7, #0]
 8000a2e:	619a      	str	r2, [r3, #24]
 8000a30:	46c0      	nop			; (mov r8, r8)
 8000a32:	46bd      	mov	sp, r7
 8000a34:	b002      	add	sp, #8
 8000a36:	bd80      	pop	{r7, pc}

08000a38 <LL_GPIO_ResetOutputPin>:
 8000a38:	b580      	push	{r7, lr}
 8000a3a:	b082      	sub	sp, #8
 8000a3c:	af00      	add	r7, sp, #0
 8000a3e:	6078      	str	r0, [r7, #4]
 8000a40:	6039      	str	r1, [r7, #0]
 8000a42:	687b      	ldr	r3, [r7, #4]
 8000a44:	683a      	ldr	r2, [r7, #0]
 8000a46:	629a      	str	r2, [r3, #40]	; 0x28
 8000a48:	46c0      	nop			; (mov r8, r8)
 8000a4a:	46bd      	mov	sp, r7
 8000a4c:	b002      	add	sp, #8
 8000a4e:	bd80      	pop	{r7, pc}

08000a50 <LL_APB1_GRP1_EnableClock>:
 8000a50:	b580      	push	{r7, lr}
 8000a52:	b084      	sub	sp, #16
 8000a54:	af00      	add	r7, sp, #0
 8000a56:	6078      	str	r0, [r7, #4]
 8000a58:	4b07      	ldr	r3, [pc, #28]	; (8000a78 <LL_APB1_GRP1_EnableClock+0x28>)
 8000a5a:	69d9      	ldr	r1, [r3, #28]
 8000a5c:	4b06      	ldr	r3, [pc, #24]	; (8000a78 <LL_APB1_GRP1_EnableClock+0x28>)
 8000a5e:	687a      	ldr	r2, [r7, #4]
 8000a60:	430a      	orrs	r2, r1
 8000a62:	61da      	str	r2, [r3, #28]
 8000a64:	4b04      	ldr	r3, [pc, #16]	; (8000a78 <LL_APB1_GRP1_EnableClock+0x28>)
 8000a66:	69db      	ldr	r3, [r3, #28]
 8000a68:	687a      	ldr	r2, [r7, #4]
 8000a6a:	4013      	ands	r3, r2
 8000a6c:	60fb      	str	r3, [r7, #12]
 8000a6e:	68fb      	ldr	r3, [r7, #12]
 8000a70:	46c0      	nop			; (mov r8, r8)
 8000a72:	46bd      	mov	sp, r7
 8000a74:	b004      	add	sp, #16
 8000a76:	bd80      	pop	{r7, pc}
 8000a78:	40021000 	.word	0x40021000

08000a7c <LL_TIM_EnableCounter>:
 8000a7c:	b580      	push	{r7, lr}
 8000a7e:	b082      	sub	sp, #8
 8000a80:	af00      	add	r7, sp, #0
 8000a82:	6078      	str	r0, [r7, #4]
 8000a84:	687b      	ldr	r3, [r7, #4]
 8000a86:	681b      	ldr	r3, [r3, #0]
 8000a88:	2201      	movs	r2, #1
 8000a8a:	431a      	orrs	r2, r3
 8000a8c:	687b      	ldr	r3, [r7, #4]
 8000a8e:	601a      	str	r2, [r3, #0]
 8000a90:	46c0      	nop			; (mov r8, r8)
 8000a92:	46bd      	mov	sp, r7
 8000a94:	b002      	add	sp, #8
 8000a96:	bd80      	pop	{r7, pc}

08000a98 <LL_TIM_SetCounterMode>:
 8000a98:	b580      	push	{r7, lr}
 8000a9a:	b082      	sub	sp, #8
 8000a9c:	af00      	add	r7, sp, #0
 8000a9e:	6078      	str	r0, [r7, #4]
 8000aa0:	6039      	str	r1, [r7, #0]
 8000aa2:	687b      	ldr	r3, [r7, #4]
 8000aa4:	681b      	ldr	r3, [r3, #0]
 8000aa6:	2270      	movs	r2, #112	; 0x70
 8000aa8:	4393      	bics	r3, r2
 8000aaa:	001a      	movs	r2, r3
 8000aac:	683b      	ldr	r3, [r7, #0]
 8000aae:	431a      	orrs	r2, r3
 8000ab0:	687b      	ldr	r3, [r7, #4]
 8000ab2:	601a      	str	r2, [r3, #0]
 8000ab4:	46c0      	nop			; (mov r8, r8)
 8000ab6:	46bd      	mov	sp, r7
 8000ab8:	b002      	add	sp, #8
 8000aba:	bd80      	pop	{r7, pc}

08000abc <LL_TIM_SetPrescaler>:
 8000abc:	b580      	push	{r7, lr}
 8000abe:	b082      	sub	sp, #8
 8000ac0:	af00      	add	r7, sp, #0
 8000ac2:	6078      	str	r0, [r7, #4]
 8000ac4:	6039      	str	r1, [r7, #0]
 8000ac6:	687b      	ldr	r3, [r7, #4]
 8000ac8:	683a      	ldr	r2, [r7, #0]
 8000aca:	629a      	str	r2, [r3, #40]	; 0x28
 8000acc:	46c0      	nop			; (mov r8, r8)
 8000ace:	46bd      	mov	sp, r7
 8000ad0:	b002      	add	sp, #8
 8000ad2:	bd80      	pop	{r7, pc}

08000ad4 <LL_TIM_SetAutoReload>:
 8000ad4:	b580      	push	{r7, lr}
 8000ad6:	b082      	sub	sp, #8
 8000ad8:	af00      	add	r7, sp, #0
 8000ada:	6078      	str	r0, [r7, #4]
 8000adc:	6039      	str	r1, [r7, #0]
 8000ade:	687b      	ldr	r3, [r7, #4]
 8000ae0:	683a      	ldr	r2, [r7, #0]
 8000ae2:	62da      	str	r2, [r3, #44]	; 0x2c
 8000ae4:	46c0      	nop			; (mov r8, r8)
 8000ae6:	46bd      	mov	sp, r7
 8000ae8:	b002      	add	sp, #8
 8000aea:	bd80      	pop	{r7, pc}

08000aec <LL_TIM_ClearFlag_UPDATE>:
 8000aec:	b580      	push	{r7, lr}
 8000aee:	b082      	sub	sp, #8
 8000af0:	af00      	add	r7, sp, #0
 8000af2:	6078      	str	r0, [r7, #4]
 8000af4:	687b      	ldr	r3, [r7, #4]
 8000af6:	2202      	movs	r2, #2
 8000af8:	4252      	negs	r2, r2
 8000afa:	611a      	str	r2, [r3, #16]
 8000afc:	46c0      	nop			; (mov r8, r8)
 8000afe:	46bd      	mov	sp, r7
 8000b00:	b002      	add	sp, #8
 8000b02:	bd80      	pop	{r7, pc}

08000b04 <LL_TIM_EnableIT_UPDATE>:
 8000b04:	b580      	push	{r7, lr}
 8000b06:	b082      	sub	sp, #8
 8000b08:	af00      	add	r7, sp, #0
 8000b0a:	6078      	str	r0, [r7, #4]
 8000b0c:	687b      	ldr	r3, [r7, #4]
 8000b0e:	68db      	ldr	r3, [r3, #12]
 8000b10:	2201      	movs	r2, #1
 8000b12:	431a      	orrs	r2, r3
 8000b14:	687b      	ldr	r3, [r7, #4]
 8000b16:	60da      	str	r2, [r3, #12]
 8000b18:	46c0      	nop			; (mov r8, r8)
 8000b1a:	46bd      	mov	sp, r7
 8000b1c:	b002      	add	sp, #8
 8000b1e:	bd80      	pop	{r7, pc}

08000b20 <tim_init>:
 8000b20:	b580      	push	{r7, lr}
 8000b22:	af00      	add	r7, sp, #0
 8000b24:	2001      	movs	r0, #1
 8000b26:	f7ff ff93 	bl	8000a50 <LL_APB1_GRP1_EnableClock>
 8000b2a:	4a13      	ldr	r2, [pc, #76]	; (8000b78 <tim_init+0x58>)
 8000b2c:	2380      	movs	r3, #128	; 0x80
 8000b2e:	05db      	lsls	r3, r3, #23
 8000b30:	0011      	movs	r1, r2
 8000b32:	0018      	movs	r0, r3
 8000b34:	f7ff ffc2 	bl	8000abc <LL_TIM_SetPrescaler>
 8000b38:	2380      	movs	r3, #128	; 0x80
 8000b3a:	05db      	lsls	r3, r3, #23
 8000b3c:	2109      	movs	r1, #9
 8000b3e:	0018      	movs	r0, r3
 8000b40:	f7ff ffc8 	bl	8000ad4 <LL_TIM_SetAutoReload>
 8000b44:	2380      	movs	r3, #128	; 0x80
 8000b46:	05db      	lsls	r3, r3, #23
 8000b48:	2100      	movs	r1, #0
 8000b4a:	0018      	movs	r0, r3
 8000b4c:	f7ff ffa4 	bl	8000a98 <LL_TIM_SetCounterMode>
 8000b50:	2380      	movs	r3, #128	; 0x80
 8000b52:	05db      	lsls	r3, r3, #23
 8000b54:	0018      	movs	r0, r3
 8000b56:	f7ff ffd5 	bl	8000b04 <LL_TIM_EnableIT_UPDATE>
 8000b5a:	2380      	movs	r3, #128	; 0x80
 8000b5c:	05db      	lsls	r3, r3, #23
 8000b5e:	0018      	movs	r0, r3
 8000b60:	f7ff ff8c 	bl	8000a7c <LL_TIM_EnableCounter>
 8000b64:	200f      	movs	r0, #15
 8000b66:	f7ff febf 	bl	80008e8 <NVIC_EnableIRQ>
 8000b6a:	2145      	movs	r1, #69	; 0x45
 8000b6c:	200f      	movs	r0, #15
 8000b6e:	f7ff fee9 	bl	8000944 <NVIC_SetPriority>
 8000b72:	46c0      	nop			; (mov r8, r8)
 8000b74:	46bd      	mov	sp, r7
 8000b76:	bd80      	pop	{r7, pc}
 8000b78:	0000bb7f 	.word	0x0000bb7f

08000b7c <init_sensors>:
 8000b7c:	b590      	push	{r4, r7, lr}
 8000b7e:	b085      	sub	sp, #20
 8000b80:	af00      	add	r7, sp, #0
 8000b82:	4b3b      	ldr	r3, [pc, #236]	; (8000c70 <init_sensors+0xf4>)
 8000b84:	0018      	movs	r0, r3
 8000b86:	f002 fd0d 	bl	80035a4 <VL53L0X_hw_config>
 8000b8a:	2300      	movs	r3, #0
 8000b8c:	60bb      	str	r3, [r7, #8]
 8000b8e:	2300      	movs	r3, #0
 8000b90:	60fb      	str	r3, [r7, #12]
 8000b92:	e065      	b.n	8000c60 <init_sensors+0xe4>
 8000b94:	68fb      	ldr	r3, [r7, #12]
 8000b96:	22a6      	movs	r2, #166	; 0xa6
 8000b98:	0052      	lsls	r2, r2, #1
 8000b9a:	435a      	muls	r2, r3
 8000b9c:	4b35      	ldr	r3, [pc, #212]	; (8000c74 <init_sensors+0xf8>)
 8000b9e:	18d3      	adds	r3, r2, r3
 8000ba0:	60bb      	str	r3, [r7, #8]
 8000ba2:	4b33      	ldr	r3, [pc, #204]	; (8000c70 <init_sensors+0xf4>)
 8000ba4:	681a      	ldr	r2, [r3, #0]
 8000ba6:	68fb      	ldr	r3, [r7, #12]
 8000ba8:	00db      	lsls	r3, r3, #3
 8000baa:	18d3      	adds	r3, r2, r3
 8000bac:	6818      	ldr	r0, [r3, #0]
 8000bae:	4b30      	ldr	r3, [pc, #192]	; (8000c70 <init_sensors+0xf4>)
 8000bb0:	681a      	ldr	r2, [r3, #0]
 8000bb2:	68fb      	ldr	r3, [r7, #12]
 8000bb4:	00db      	lsls	r3, r3, #3
 8000bb6:	18d3      	adds	r3, r2, r3
 8000bb8:	685b      	ldr	r3, [r3, #4]
 8000bba:	0019      	movs	r1, r3
 8000bbc:	f7ff ff30 	bl	8000a20 <LL_GPIO_SetOutputPin>
 8000bc0:	68bb      	ldr	r3, [r7, #8]
 8000bc2:	0018      	movs	r0, r3
 8000bc4:	f002 ff64 	bl	8003a90 <VL53L0X_PollingDelay>
 8000bc8:	68ba      	ldr	r2, [r7, #8]
 8000bca:	239e      	movs	r3, #158	; 0x9e
 8000bcc:	005b      	lsls	r3, r3, #1
 8000bce:	2152      	movs	r1, #82	; 0x52
 8000bd0:	54d1      	strb	r1, [r2, r3]
 8000bd2:	68bb      	ldr	r3, [r7, #8]
 8000bd4:	1dfc      	adds	r4, r7, #7
 8000bd6:	0018      	movs	r0, r3
 8000bd8:	f003 f97e 	bl	8003ed8 <VL53L0X_DataInit>
 8000bdc:	0003      	movs	r3, r0
 8000bde:	7023      	strb	r3, [r4, #0]
 8000be0:	1dfb      	adds	r3, r7, #7
 8000be2:	781b      	ldrb	r3, [r3, #0]
 8000be4:	b25b      	sxtb	r3, r3
 8000be6:	2b00      	cmp	r3, #0
 8000be8:	d136      	bne.n	8000c58 <init_sensors+0xdc>
 8000bea:	68bb      	ldr	r3, [r7, #8]
 8000bec:	0018      	movs	r0, r3
 8000bee:	f003 fb15 	bl	800421c <VL53L0X_StaticInit>
 8000bf2:	68b8      	ldr	r0, [r7, #8]
 8000bf4:	68bb      	ldr	r3, [r7, #8]
 8000bf6:	3341      	adds	r3, #65	; 0x41
 8000bf8:	33ff      	adds	r3, #255	; 0xff
 8000bfa:	0019      	movs	r1, r3
 8000bfc:	68bb      	ldr	r3, [r7, #8]
 8000bfe:	3342      	adds	r3, #66	; 0x42
 8000c00:	33ff      	adds	r3, #255	; 0xff
 8000c02:	001a      	movs	r2, r3
 8000c04:	f004 fb27 	bl	8005256 <VL53L0X_PerformRefCalibration>
 8000c08:	68b8      	ldr	r0, [r7, #8]
 8000c0a:	68bb      	ldr	r3, [r7, #8]
 8000c0c:	3345      	adds	r3, #69	; 0x45
 8000c0e:	33ff      	adds	r3, #255	; 0xff
 8000c10:	0019      	movs	r1, r3
 8000c12:	68bb      	ldr	r3, [r7, #8]
 8000c14:	3349      	adds	r3, #73	; 0x49
 8000c16:	33ff      	adds	r3, #255	; 0xff
 8000c18:	001a      	movs	r2, r3
 8000c1a:	f005 f94d 	bl	8005eb8 <VL53L0X_PerformRefSpadManagement>
 8000c1e:	68bb      	ldr	r3, [r7, #8]
 8000c20:	2101      	movs	r1, #1
 8000c22:	0018      	movs	r0, r3
 8000c24:	f003 fd80 	bl	8004728 <VL53L0X_SetDeviceMode>
 8000c28:	68ba      	ldr	r2, [r7, #8]
 8000c2a:	68fb      	ldr	r3, [r7, #12]
 8000c2c:	3308      	adds	r3, #8
 8000c2e:	b2db      	uxtb	r3, r3
 8000c30:	18db      	adds	r3, r3, r3
 8000c32:	b2db      	uxtb	r3, r3
 8000c34:	0019      	movs	r1, r3
 8000c36:	0010      	movs	r0, r2
 8000c38:	f003 f92f 	bl	8003e9a <VL53L0X_SetDeviceAddress>
 8000c3c:	68fb      	ldr	r3, [r7, #12]
 8000c3e:	3308      	adds	r3, #8
 8000c40:	b2db      	uxtb	r3, r3
 8000c42:	18db      	adds	r3, r3, r3
 8000c44:	b2d9      	uxtb	r1, r3
 8000c46:	68ba      	ldr	r2, [r7, #8]
 8000c48:	239e      	movs	r3, #158	; 0x9e
 8000c4a:	005b      	lsls	r3, r3, #1
 8000c4c:	54d1      	strb	r1, [r2, r3]
 8000c4e:	68bb      	ldr	r3, [r7, #8]
 8000c50:	0018      	movs	r0, r3
 8000c52:	f004 fba3 	bl	800539c <VL53L0X_StartMeasurement>
 8000c56:	e000      	b.n	8000c5a <init_sensors+0xde>
 8000c58:	46c0      	nop			; (mov r8, r8)
 8000c5a:	68fb      	ldr	r3, [r7, #12]
 8000c5c:	3301      	adds	r3, #1
 8000c5e:	60fb      	str	r3, [r7, #12]
 8000c60:	68fb      	ldr	r3, [r7, #12]
 8000c62:	2b02      	cmp	r3, #2
 8000c64:	dd96      	ble.n	8000b94 <init_sensors+0x18>
 8000c66:	46c0      	nop			; (mov r8, r8)
 8000c68:	46bd      	mov	sp, r7
 8000c6a:	b005      	add	sp, #20
 8000c6c:	bd90      	pop	{r4, r7, pc}
 8000c6e:	46c0      	nop			; (mov r8, r8)
 8000c70:	20000b00 	.word	0x20000b00
 8000c74:	2000071c 	.word	0x2000071c

08000c78 <reset_sensor>:
 8000c78:	b5b0      	push	{r4, r5, r7, lr}
 8000c7a:	b084      	sub	sp, #16
 8000c7c:	af00      	add	r7, sp, #0
 8000c7e:	0002      	movs	r2, r0
 8000c80:	1dfb      	adds	r3, r7, #7
 8000c82:	701a      	strb	r2, [r3, #0]
 8000c84:	200f      	movs	r0, #15
 8000c86:	f7ff fe45 	bl	8000914 <NVIC_DisableIRQ>
 8000c8a:	2010      	movs	r0, #16
 8000c8c:	f7ff fe42 	bl	8000914 <NVIC_DisableIRQ>
 8000c90:	f002 fc78 	bl	8003584 <VL53L0X_hw_reset>
 8000c94:	1dfb      	adds	r3, r7, #7
 8000c96:	781b      	ldrb	r3, [r3, #0]
 8000c98:	22a6      	movs	r2, #166	; 0xa6
 8000c9a:	0052      	lsls	r2, r2, #1
 8000c9c:	435a      	muls	r2, r3
 8000c9e:	4ba0      	ldr	r3, [pc, #640]	; (8000f20 <reset_sensor+0x2a8>)
 8000ca0:	18d3      	adds	r3, r2, r3
 8000ca2:	0018      	movs	r0, r3
 8000ca4:	f002 fef4 	bl	8003a90 <VL53L0X_PollingDelay>
 8000ca8:	4b9e      	ldr	r3, [pc, #632]	; (8000f24 <reset_sensor+0x2ac>)
 8000caa:	681a      	ldr	r2, [r3, #0]
 8000cac:	1dfb      	adds	r3, r7, #7
 8000cae:	781b      	ldrb	r3, [r3, #0]
 8000cb0:	00db      	lsls	r3, r3, #3
 8000cb2:	18d3      	adds	r3, r2, r3
 8000cb4:	6818      	ldr	r0, [r3, #0]
 8000cb6:	4b9b      	ldr	r3, [pc, #620]	; (8000f24 <reset_sensor+0x2ac>)
 8000cb8:	681a      	ldr	r2, [r3, #0]
 8000cba:	1dfb      	adds	r3, r7, #7
 8000cbc:	781b      	ldrb	r3, [r3, #0]
 8000cbe:	00db      	lsls	r3, r3, #3
 8000cc0:	18d3      	adds	r3, r2, r3
 8000cc2:	685b      	ldr	r3, [r3, #4]
 8000cc4:	0019      	movs	r1, r3
 8000cc6:	f7ff feab 	bl	8000a20 <LL_GPIO_SetOutputPin>
 8000cca:	1dfb      	adds	r3, r7, #7
 8000ccc:	781b      	ldrb	r3, [r3, #0]
 8000cce:	22a6      	movs	r2, #166	; 0xa6
 8000cd0:	0052      	lsls	r2, r2, #1
 8000cd2:	435a      	muls	r2, r3
 8000cd4:	4b92      	ldr	r3, [pc, #584]	; (8000f20 <reset_sensor+0x2a8>)
 8000cd6:	18d3      	adds	r3, r2, r3
 8000cd8:	0018      	movs	r0, r3
 8000cda:	f002 fed9 	bl	8003a90 <VL53L0X_PollingDelay>
 8000cde:	1dfb      	adds	r3, r7, #7
 8000ce0:	781a      	ldrb	r2, [r3, #0]
 8000ce2:	498f      	ldr	r1, [pc, #572]	; (8000f20 <reset_sensor+0x2a8>)
 8000ce4:	239e      	movs	r3, #158	; 0x9e
 8000ce6:	005b      	lsls	r3, r3, #1
 8000ce8:	20a6      	movs	r0, #166	; 0xa6
 8000cea:	0040      	lsls	r0, r0, #1
 8000cec:	4342      	muls	r2, r0
 8000cee:	188a      	adds	r2, r1, r2
 8000cf0:	18d3      	adds	r3, r2, r3
 8000cf2:	2252      	movs	r2, #82	; 0x52
 8000cf4:	701a      	strb	r2, [r3, #0]
 8000cf6:	1dfb      	adds	r3, r7, #7
 8000cf8:	781b      	ldrb	r3, [r3, #0]
 8000cfa:	22a6      	movs	r2, #166	; 0xa6
 8000cfc:	0052      	lsls	r2, r2, #1
 8000cfe:	435a      	muls	r2, r3
 8000d00:	4b87      	ldr	r3, [pc, #540]	; (8000f20 <reset_sensor+0x2a8>)
 8000d02:	18d3      	adds	r3, r2, r3
 8000d04:	250f      	movs	r5, #15
 8000d06:	197c      	adds	r4, r7, r5
 8000d08:	0018      	movs	r0, r3
 8000d0a:	f003 f8e5 	bl	8003ed8 <VL53L0X_DataInit>
 8000d0e:	0003      	movs	r3, r0
 8000d10:	7023      	strb	r3, [r4, #0]
 8000d12:	002c      	movs	r4, r5
 8000d14:	193b      	adds	r3, r7, r4
 8000d16:	781b      	ldrb	r3, [r3, #0]
 8000d18:	b25b      	sxtb	r3, r3
 8000d1a:	2b00      	cmp	r3, #0
 8000d1c:	d000      	beq.n	8000d20 <reset_sensor+0xa8>
 8000d1e:	e0d4      	b.n	8000eca <reset_sensor+0x252>
 8000d20:	1dfb      	adds	r3, r7, #7
 8000d22:	781b      	ldrb	r3, [r3, #0]
 8000d24:	22a6      	movs	r2, #166	; 0xa6
 8000d26:	0052      	lsls	r2, r2, #1
 8000d28:	435a      	muls	r2, r3
 8000d2a:	4b7d      	ldr	r3, [pc, #500]	; (8000f20 <reset_sensor+0x2a8>)
 8000d2c:	18d3      	adds	r3, r2, r3
 8000d2e:	0018      	movs	r0, r3
 8000d30:	f002 feae 	bl	8003a90 <VL53L0X_PollingDelay>
 8000d34:	1dfb      	adds	r3, r7, #7
 8000d36:	781b      	ldrb	r3, [r3, #0]
 8000d38:	22a6      	movs	r2, #166	; 0xa6
 8000d3a:	0052      	lsls	r2, r2, #1
 8000d3c:	435a      	muls	r2, r3
 8000d3e:	4b78      	ldr	r3, [pc, #480]	; (8000f20 <reset_sensor+0x2a8>)
 8000d40:	18d3      	adds	r3, r2, r3
 8000d42:	0025      	movs	r5, r4
 8000d44:	193c      	adds	r4, r7, r4
 8000d46:	0018      	movs	r0, r3
 8000d48:	f003 fa68 	bl	800421c <VL53L0X_StaticInit>
 8000d4c:	0003      	movs	r3, r0
 8000d4e:	7023      	strb	r3, [r4, #0]
 8000d50:	002c      	movs	r4, r5
 8000d52:	193b      	adds	r3, r7, r4
 8000d54:	781b      	ldrb	r3, [r3, #0]
 8000d56:	b25b      	sxtb	r3, r3
 8000d58:	2b00      	cmp	r3, #0
 8000d5a:	d000      	beq.n	8000d5e <reset_sensor+0xe6>
 8000d5c:	e0b7      	b.n	8000ece <reset_sensor+0x256>
 8000d5e:	1dfb      	adds	r3, r7, #7
 8000d60:	781b      	ldrb	r3, [r3, #0]
 8000d62:	22a6      	movs	r2, #166	; 0xa6
 8000d64:	0052      	lsls	r2, r2, #1
 8000d66:	435a      	muls	r2, r3
 8000d68:	4b6d      	ldr	r3, [pc, #436]	; (8000f20 <reset_sensor+0x2a8>)
 8000d6a:	18d3      	adds	r3, r2, r3
 8000d6c:	0018      	movs	r0, r3
 8000d6e:	f002 fe8f 	bl	8003a90 <VL53L0X_PollingDelay>
 8000d72:	1dfb      	adds	r3, r7, #7
 8000d74:	781b      	ldrb	r3, [r3, #0]
 8000d76:	22a6      	movs	r2, #166	; 0xa6
 8000d78:	0052      	lsls	r2, r2, #1
 8000d7a:	435a      	muls	r2, r3
 8000d7c:	4b68      	ldr	r3, [pc, #416]	; (8000f20 <reset_sensor+0x2a8>)
 8000d7e:	18d0      	adds	r0, r2, r3
 8000d80:	1dfb      	adds	r3, r7, #7
 8000d82:	781b      	ldrb	r3, [r3, #0]
 8000d84:	22a6      	movs	r2, #166	; 0xa6
 8000d86:	0052      	lsls	r2, r2, #1
 8000d88:	4353      	muls	r3, r2
 8000d8a:	3341      	adds	r3, #65	; 0x41
 8000d8c:	33ff      	adds	r3, #255	; 0xff
 8000d8e:	001a      	movs	r2, r3
 8000d90:	4b63      	ldr	r3, [pc, #396]	; (8000f20 <reset_sensor+0x2a8>)
 8000d92:	18d1      	adds	r1, r2, r3
 8000d94:	1dfb      	adds	r3, r7, #7
 8000d96:	781b      	ldrb	r3, [r3, #0]
 8000d98:	22a6      	movs	r2, #166	; 0xa6
 8000d9a:	0052      	lsls	r2, r2, #1
 8000d9c:	4353      	muls	r3, r2
 8000d9e:	3341      	adds	r3, #65	; 0x41
 8000da0:	33ff      	adds	r3, #255	; 0xff
 8000da2:	001a      	movs	r2, r3
 8000da4:	4b5e      	ldr	r3, [pc, #376]	; (8000f20 <reset_sensor+0x2a8>)
 8000da6:	18d3      	adds	r3, r2, r3
 8000da8:	3301      	adds	r3, #1
 8000daa:	0025      	movs	r5, r4
 8000dac:	197c      	adds	r4, r7, r5
 8000dae:	001a      	movs	r2, r3
 8000db0:	f004 fa51 	bl	8005256 <VL53L0X_PerformRefCalibration>
 8000db4:	0003      	movs	r3, r0
 8000db6:	7023      	strb	r3, [r4, #0]
 8000db8:	1dfb      	adds	r3, r7, #7
 8000dba:	781b      	ldrb	r3, [r3, #0]
 8000dbc:	22a6      	movs	r2, #166	; 0xa6
 8000dbe:	0052      	lsls	r2, r2, #1
 8000dc0:	435a      	muls	r2, r3
 8000dc2:	4b57      	ldr	r3, [pc, #348]	; (8000f20 <reset_sensor+0x2a8>)
 8000dc4:	18d3      	adds	r3, r2, r3
 8000dc6:	0018      	movs	r0, r3
 8000dc8:	f002 fe62 	bl	8003a90 <VL53L0X_PollingDelay>
 8000dcc:	197b      	adds	r3, r7, r5
 8000dce:	781b      	ldrb	r3, [r3, #0]
 8000dd0:	b25b      	sxtb	r3, r3
 8000dd2:	2b00      	cmp	r3, #0
 8000dd4:	d000      	beq.n	8000dd8 <reset_sensor+0x160>
 8000dd6:	e07c      	b.n	8000ed2 <reset_sensor+0x25a>
 8000dd8:	1dfb      	adds	r3, r7, #7
 8000dda:	781b      	ldrb	r3, [r3, #0]
 8000ddc:	22a6      	movs	r2, #166	; 0xa6
 8000dde:	0052      	lsls	r2, r2, #1
 8000de0:	435a      	muls	r2, r3
 8000de2:	4b4f      	ldr	r3, [pc, #316]	; (8000f20 <reset_sensor+0x2a8>)
 8000de4:	18d3      	adds	r3, r2, r3
 8000de6:	197c      	adds	r4, r7, r5
 8000de8:	2101      	movs	r1, #1
 8000dea:	0018      	movs	r0, r3
 8000dec:	f003 fc9c 	bl	8004728 <VL53L0X_SetDeviceMode>
 8000df0:	0003      	movs	r3, r0
 8000df2:	7023      	strb	r3, [r4, #0]
 8000df4:	1dfb      	adds	r3, r7, #7
 8000df6:	781b      	ldrb	r3, [r3, #0]
 8000df8:	22a6      	movs	r2, #166	; 0xa6
 8000dfa:	0052      	lsls	r2, r2, #1
 8000dfc:	435a      	muls	r2, r3
 8000dfe:	4b48      	ldr	r3, [pc, #288]	; (8000f20 <reset_sensor+0x2a8>)
 8000e00:	18d3      	adds	r3, r2, r3
 8000e02:	0018      	movs	r0, r3
 8000e04:	f002 fe44 	bl	8003a90 <VL53L0X_PollingDelay>
 8000e08:	197b      	adds	r3, r7, r5
 8000e0a:	781b      	ldrb	r3, [r3, #0]
 8000e0c:	b25b      	sxtb	r3, r3
 8000e0e:	2b00      	cmp	r3, #0
 8000e10:	d161      	bne.n	8000ed6 <reset_sensor+0x25e>
 8000e12:	1dfb      	adds	r3, r7, #7
 8000e14:	781b      	ldrb	r3, [r3, #0]
 8000e16:	22a6      	movs	r2, #166	; 0xa6
 8000e18:	0052      	lsls	r2, r2, #1
 8000e1a:	435a      	muls	r2, r3
 8000e1c:	4b40      	ldr	r3, [pc, #256]	; (8000f20 <reset_sensor+0x2a8>)
 8000e1e:	18d2      	adds	r2, r2, r3
 8000e20:	1dfb      	adds	r3, r7, #7
 8000e22:	781b      	ldrb	r3, [r3, #0]
 8000e24:	3308      	adds	r3, #8
 8000e26:	b2db      	uxtb	r3, r3
 8000e28:	18db      	adds	r3, r3, r3
 8000e2a:	b2db      	uxtb	r3, r3
 8000e2c:	197c      	adds	r4, r7, r5
 8000e2e:	0019      	movs	r1, r3
 8000e30:	0010      	movs	r0, r2
 8000e32:	f003 f832 	bl	8003e9a <VL53L0X_SetDeviceAddress>
 8000e36:	0003      	movs	r3, r0
 8000e38:	7023      	strb	r3, [r4, #0]
 8000e3a:	1dfb      	adds	r3, r7, #7
 8000e3c:	781b      	ldrb	r3, [r3, #0]
 8000e3e:	22a6      	movs	r2, #166	; 0xa6
 8000e40:	0052      	lsls	r2, r2, #1
 8000e42:	435a      	muls	r2, r3
 8000e44:	4b36      	ldr	r3, [pc, #216]	; (8000f20 <reset_sensor+0x2a8>)
 8000e46:	18d3      	adds	r3, r2, r3
 8000e48:	0018      	movs	r0, r3
 8000e4a:	f002 fe21 	bl	8003a90 <VL53L0X_PollingDelay>
 8000e4e:	197b      	adds	r3, r7, r5
 8000e50:	781b      	ldrb	r3, [r3, #0]
 8000e52:	b25b      	sxtb	r3, r3
 8000e54:	2b00      	cmp	r3, #0
 8000e56:	d140      	bne.n	8000eda <reset_sensor+0x262>
 8000e58:	1dfb      	adds	r3, r7, #7
 8000e5a:	781b      	ldrb	r3, [r3, #0]
 8000e5c:	3308      	adds	r3, #8
 8000e5e:	b2d9      	uxtb	r1, r3
 8000e60:	1dfb      	adds	r3, r7, #7
 8000e62:	781a      	ldrb	r2, [r3, #0]
 8000e64:	1c0b      	adds	r3, r1, #0
 8000e66:	18db      	adds	r3, r3, r3
 8000e68:	b2dc      	uxtb	r4, r3
 8000e6a:	492d      	ldr	r1, [pc, #180]	; (8000f20 <reset_sensor+0x2a8>)
 8000e6c:	239e      	movs	r3, #158	; 0x9e
 8000e6e:	005b      	lsls	r3, r3, #1
 8000e70:	20a6      	movs	r0, #166	; 0xa6
 8000e72:	0040      	lsls	r0, r0, #1
 8000e74:	4342      	muls	r2, r0
 8000e76:	188a      	adds	r2, r1, r2
 8000e78:	18d3      	adds	r3, r2, r3
 8000e7a:	1c22      	adds	r2, r4, #0
 8000e7c:	701a      	strb	r2, [r3, #0]
 8000e7e:	1dfb      	adds	r3, r7, #7
 8000e80:	781b      	ldrb	r3, [r3, #0]
 8000e82:	22a6      	movs	r2, #166	; 0xa6
 8000e84:	0052      	lsls	r2, r2, #1
 8000e86:	435a      	muls	r2, r3
 8000e88:	4b25      	ldr	r3, [pc, #148]	; (8000f20 <reset_sensor+0x2a8>)
 8000e8a:	18d3      	adds	r3, r2, r3
 8000e8c:	197c      	adds	r4, r7, r5
 8000e8e:	0018      	movs	r0, r3
 8000e90:	f004 fa84 	bl	800539c <VL53L0X_StartMeasurement>
 8000e94:	0003      	movs	r3, r0
 8000e96:	7023      	strb	r3, [r4, #0]
 8000e98:	197b      	adds	r3, r7, r5
 8000e9a:	781b      	ldrb	r3, [r3, #0]
 8000e9c:	b25b      	sxtb	r3, r3
 8000e9e:	2b00      	cmp	r3, #0
 8000ea0:	d11d      	bne.n	8000ede <reset_sensor+0x266>
 8000ea2:	1dfb      	adds	r3, r7, #7
 8000ea4:	781b      	ldrb	r3, [r3, #0]
 8000ea6:	22a6      	movs	r2, #166	; 0xa6
 8000ea8:	0052      	lsls	r2, r2, #1
 8000eaa:	435a      	muls	r2, r3
 8000eac:	4b1c      	ldr	r3, [pc, #112]	; (8000f20 <reset_sensor+0x2a8>)
 8000eae:	18d3      	adds	r3, r2, r3
 8000eb0:	0018      	movs	r0, r3
 8000eb2:	f002 fded 	bl	8003a90 <VL53L0X_PollingDelay>
 8000eb6:	200f      	movs	r0, #15
 8000eb8:	f7ff fd16 	bl	80008e8 <NVIC_EnableIRQ>
 8000ebc:	2010      	movs	r0, #16
 8000ebe:	f7ff fd13 	bl	80008e8 <NVIC_EnableIRQ>
 8000ec2:	197b      	adds	r3, r7, r5
 8000ec4:	781b      	ldrb	r3, [r3, #0]
 8000ec6:	b25b      	sxtb	r3, r3
 8000ec8:	e025      	b.n	8000f16 <reset_sensor+0x29e>
 8000eca:	46c0      	nop			; (mov r8, r8)
 8000ecc:	e008      	b.n	8000ee0 <reset_sensor+0x268>
 8000ece:	46c0      	nop			; (mov r8, r8)
 8000ed0:	e006      	b.n	8000ee0 <reset_sensor+0x268>
 8000ed2:	46c0      	nop			; (mov r8, r8)
 8000ed4:	e004      	b.n	8000ee0 <reset_sensor+0x268>
 8000ed6:	46c0      	nop			; (mov r8, r8)
 8000ed8:	e002      	b.n	8000ee0 <reset_sensor+0x268>
 8000eda:	46c0      	nop			; (mov r8, r8)
 8000edc:	e000      	b.n	8000ee0 <reset_sensor+0x268>
 8000ede:	46c0      	nop			; (mov r8, r8)
 8000ee0:	4b10      	ldr	r3, [pc, #64]	; (8000f24 <reset_sensor+0x2ac>)
 8000ee2:	681a      	ldr	r2, [r3, #0]
 8000ee4:	1dfb      	adds	r3, r7, #7
 8000ee6:	781b      	ldrb	r3, [r3, #0]
 8000ee8:	00db      	lsls	r3, r3, #3
 8000eea:	18d3      	adds	r3, r2, r3
 8000eec:	6818      	ldr	r0, [r3, #0]
 8000eee:	4b0d      	ldr	r3, [pc, #52]	; (8000f24 <reset_sensor+0x2ac>)
 8000ef0:	681a      	ldr	r2, [r3, #0]
 8000ef2:	1dfb      	adds	r3, r7, #7
 8000ef4:	781b      	ldrb	r3, [r3, #0]
 8000ef6:	00db      	lsls	r3, r3, #3
 8000ef8:	18d3      	adds	r3, r2, r3
 8000efa:	685b      	ldr	r3, [r3, #4]
 8000efc:	0019      	movs	r1, r3
 8000efe:	f7ff fd9b 	bl	8000a38 <LL_GPIO_ResetOutputPin>
 8000f02:	200f      	movs	r0, #15
 8000f04:	f7ff fcf0 	bl	80008e8 <NVIC_EnableIRQ>
 8000f08:	2010      	movs	r0, #16
 8000f0a:	f7ff fced 	bl	80008e8 <NVIC_EnableIRQ>
 8000f0e:	230f      	movs	r3, #15
 8000f10:	18fb      	adds	r3, r7, r3
 8000f12:	781b      	ldrb	r3, [r3, #0]
 8000f14:	b25b      	sxtb	r3, r3
 8000f16:	0018      	movs	r0, r3
 8000f18:	46bd      	mov	sp, r7
 8000f1a:	b004      	add	sp, #16
 8000f1c:	bdb0      	pop	{r4, r5, r7, pc}
 8000f1e:	46c0      	nop			; (mov r8, r8)
 8000f20:	2000071c 	.word	0x2000071c
 8000f24:	20000b00 	.word	0x20000b00

08000f28 <coll_avoid_init>:
 8000f28:	b580      	push	{r7, lr}
 8000f2a:	b082      	sub	sp, #8
 8000f2c:	af00      	add	r7, sp, #0
 8000f2e:	4b0e      	ldr	r3, [pc, #56]	; (8000f68 <coll_avoid_init+0x40>)
 8000f30:	2200      	movs	r2, #0
 8000f32:	71da      	strb	r2, [r3, #7]
 8000f34:	4b0c      	ldr	r3, [pc, #48]	; (8000f68 <coll_avoid_init+0x40>)
 8000f36:	2200      	movs	r2, #0
 8000f38:	719a      	strb	r2, [r3, #6]
 8000f3a:	2300      	movs	r3, #0
 8000f3c:	607b      	str	r3, [r7, #4]
 8000f3e:	e008      	b.n	8000f52 <coll_avoid_init+0x2a>
 8000f40:	4a09      	ldr	r2, [pc, #36]	; (8000f68 <coll_avoid_init+0x40>)
 8000f42:	687b      	ldr	r3, [r7, #4]
 8000f44:	18d3      	adds	r3, r2, r3
 8000f46:	3303      	adds	r3, #3
 8000f48:	2200      	movs	r2, #0
 8000f4a:	701a      	strb	r2, [r3, #0]
 8000f4c:	687b      	ldr	r3, [r7, #4]
 8000f4e:	3301      	adds	r3, #1
 8000f50:	607b      	str	r3, [r7, #4]
 8000f52:	687b      	ldr	r3, [r7, #4]
 8000f54:	2b02      	cmp	r3, #2
 8000f56:	ddf3      	ble.n	8000f40 <coll_avoid_init+0x18>
 8000f58:	f7ff fe10 	bl	8000b7c <init_sensors>
 8000f5c:	f7ff fde0 	bl	8000b20 <tim_init>
 8000f60:	46c0      	nop			; (mov r8, r8)
 8000f62:	46bd      	mov	sp, r7
 8000f64:	b002      	add	sp, #8
 8000f66:	bd80      	pop	{r7, pc}
 8000f68:	2000070c 	.word	0x2000070c

08000f6c <reload_sensors>:
 8000f6c:	b580      	push	{r7, lr}
 8000f6e:	b082      	sub	sp, #8
 8000f70:	af00      	add	r7, sp, #0
 8000f72:	2300      	movs	r3, #0
 8000f74:	607b      	str	r3, [r7, #4]
 8000f76:	e01a      	b.n	8000fae <reload_sensors+0x42>
 8000f78:	4a10      	ldr	r2, [pc, #64]	; (8000fbc <reload_sensors+0x50>)
 8000f7a:	687b      	ldr	r3, [r7, #4]
 8000f7c:	18d3      	adds	r3, r2, r3
 8000f7e:	3303      	adds	r3, #3
 8000f80:	781b      	ldrb	r3, [r3, #0]
 8000f82:	2b00      	cmp	r3, #0
 8000f84:	d00d      	beq.n	8000fa2 <reload_sensors+0x36>
 8000f86:	687b      	ldr	r3, [r7, #4]
 8000f88:	b2db      	uxtb	r3, r3
 8000f8a:	0018      	movs	r0, r3
 8000f8c:	f7ff fe74 	bl	8000c78 <reset_sensor>
 8000f90:	1e03      	subs	r3, r0, #0
 8000f92:	d108      	bne.n	8000fa6 <reload_sensors+0x3a>
 8000f94:	4a09      	ldr	r2, [pc, #36]	; (8000fbc <reload_sensors+0x50>)
 8000f96:	687b      	ldr	r3, [r7, #4]
 8000f98:	18d3      	adds	r3, r2, r3
 8000f9a:	3303      	adds	r3, #3
 8000f9c:	2200      	movs	r2, #0
 8000f9e:	701a      	strb	r2, [r3, #0]
 8000fa0:	e002      	b.n	8000fa8 <reload_sensors+0x3c>
 8000fa2:	46c0      	nop			; (mov r8, r8)
 8000fa4:	e000      	b.n	8000fa8 <reload_sensors+0x3c>
 8000fa6:	46c0      	nop			; (mov r8, r8)
 8000fa8:	687b      	ldr	r3, [r7, #4]
 8000faa:	3301      	adds	r3, #1
 8000fac:	607b      	str	r3, [r7, #4]
 8000fae:	687b      	ldr	r3, [r7, #4]
 8000fb0:	2b02      	cmp	r3, #2
 8000fb2:	dde1      	ble.n	8000f78 <reload_sensors+0xc>
 8000fb4:	46c0      	nop			; (mov r8, r8)
 8000fb6:	46bd      	mov	sp, r7
 8000fb8:	b002      	add	sp, #8
 8000fba:	bd80      	pop	{r7, pc}
 8000fbc:	2000070c 	.word	0x2000070c

08000fc0 <col_av_read_status>:
 8000fc0:	b580      	push	{r7, lr}
 8000fc2:	af00      	add	r7, sp, #0
 8000fc4:	4b02      	ldr	r3, [pc, #8]	; (8000fd0 <col_av_read_status+0x10>)
 8000fc6:	79db      	ldrb	r3, [r3, #7]
 8000fc8:	0018      	movs	r0, r3
 8000fca:	46bd      	mov	sp, r7
 8000fcc:	bd80      	pop	{r7, pc}
 8000fce:	46c0      	nop			; (mov r8, r8)
 8000fd0:	2000070c 	.word	0x2000070c

08000fd4 <col_av_set_status>:
 8000fd4:	b580      	push	{r7, lr}
 8000fd6:	b082      	sub	sp, #8
 8000fd8:	af00      	add	r7, sp, #0
 8000fda:	0002      	movs	r2, r0
 8000fdc:	1dfb      	adds	r3, r7, #7
 8000fde:	701a      	strb	r2, [r3, #0]
 8000fe0:	4b08      	ldr	r3, [pc, #32]	; (8001004 <col_av_set_status+0x30>)
 8000fe2:	79db      	ldrb	r3, [r3, #7]
 8000fe4:	b25a      	sxtb	r2, r3
 8000fe6:	1dfb      	adds	r3, r7, #7
 8000fe8:	781b      	ldrb	r3, [r3, #0]
 8000fea:	2101      	movs	r1, #1
 8000fec:	4099      	lsls	r1, r3
 8000fee:	000b      	movs	r3, r1
 8000ff0:	b25b      	sxtb	r3, r3
 8000ff2:	4313      	orrs	r3, r2
 8000ff4:	b25b      	sxtb	r3, r3
 8000ff6:	b2da      	uxtb	r2, r3
 8000ff8:	4b02      	ldr	r3, [pc, #8]	; (8001004 <col_av_set_status+0x30>)
 8000ffa:	71da      	strb	r2, [r3, #7]
 8000ffc:	46c0      	nop			; (mov r8, r8)
 8000ffe:	46bd      	mov	sp, r7
 8001000:	b002      	add	sp, #8
 8001002:	bd80      	pop	{r7, pc}
 8001004:	2000070c 	.word	0x2000070c

08001008 <col_av_set_block>:
 8001008:	b580      	push	{r7, lr}
 800100a:	af00      	add	r7, sp, #0
 800100c:	4b02      	ldr	r3, [pc, #8]	; (8001018 <col_av_set_block+0x10>)
 800100e:	2201      	movs	r2, #1
 8001010:	721a      	strb	r2, [r3, #8]
 8001012:	46c0      	nop			; (mov r8, r8)
 8001014:	46bd      	mov	sp, r7
 8001016:	bd80      	pop	{r7, pc}
 8001018:	2000070c 	.word	0x2000070c

0800101c <col_av_clr_block>:
 800101c:	b580      	push	{r7, lr}
 800101e:	af00      	add	r7, sp, #0
 8001020:	4b02      	ldr	r3, [pc, #8]	; (800102c <col_av_clr_block+0x10>)
 8001022:	2200      	movs	r2, #0
 8001024:	721a      	strb	r2, [r3, #8]
 8001026:	46c0      	nop			; (mov r8, r8)
 8001028:	46bd      	mov	sp, r7
 800102a:	bd80      	pop	{r7, pc}
 800102c:	2000070c 	.word	0x2000070c

08001030 <col_av_get_ack_block>:
 8001030:	b580      	push	{r7, lr}
 8001032:	af00      	add	r7, sp, #0
 8001034:	4b02      	ldr	r3, [pc, #8]	; (8001040 <col_av_get_ack_block+0x10>)
 8001036:	7a5b      	ldrb	r3, [r3, #9]
 8001038:	0018      	movs	r0, r3
 800103a:	46bd      	mov	sp, r7
 800103c:	bd80      	pop	{r7, pc}
 800103e:	46c0      	nop			; (mov r8, r8)
 8001040:	2000070c 	.word	0x2000070c

08001044 <col_av_any>:
 8001044:	b580      	push	{r7, lr}
 8001046:	b082      	sub	sp, #8
 8001048:	af00      	add	r7, sp, #0
 800104a:	1dbb      	adds	r3, r7, #6
 800104c:	2200      	movs	r2, #0
 800104e:	701a      	strb	r2, [r3, #0]
 8001050:	1dfb      	adds	r3, r7, #7
 8001052:	2200      	movs	r2, #0
 8001054:	701a      	strb	r2, [r3, #0]
 8001056:	e010      	b.n	800107a <col_av_any+0x36>
 8001058:	1dfb      	adds	r3, r7, #7
 800105a:	781b      	ldrb	r3, [r3, #0]
 800105c:	4a0c      	ldr	r2, [pc, #48]	; (8001090 <col_av_any+0x4c>)
 800105e:	18d3      	adds	r3, r2, r3
 8001060:	78db      	ldrb	r3, [r3, #3]
 8001062:	2b00      	cmp	r3, #0
 8001064:	d004      	beq.n	8001070 <col_av_any+0x2c>
 8001066:	1dbb      	adds	r3, r7, #6
 8001068:	781a      	ldrb	r2, [r3, #0]
 800106a:	1dbb      	adds	r3, r7, #6
 800106c:	3201      	adds	r2, #1
 800106e:	701a      	strb	r2, [r3, #0]
 8001070:	1dfb      	adds	r3, r7, #7
 8001072:	781a      	ldrb	r2, [r3, #0]
 8001074:	1dfb      	adds	r3, r7, #7
 8001076:	3201      	adds	r2, #1
 8001078:	701a      	strb	r2, [r3, #0]
 800107a:	1dfb      	adds	r3, r7, #7
 800107c:	781b      	ldrb	r3, [r3, #0]
 800107e:	2b02      	cmp	r3, #2
 8001080:	d9ea      	bls.n	8001058 <col_av_any+0x14>
 8001082:	1dbb      	adds	r3, r7, #6
 8001084:	781b      	ldrb	r3, [r3, #0]
 8001086:	0018      	movs	r0, r3
 8001088:	46bd      	mov	sp, r7
 800108a:	b002      	add	sp, #8
 800108c:	bd80      	pop	{r7, pc}
 800108e:	46c0      	nop			; (mov r8, r8)
 8001090:	2000070c 	.word	0x2000070c

08001094 <get_dist_mm>:
 8001094:	b5b0      	push	{r4, r5, r7, lr}
 8001096:	b084      	sub	sp, #16
 8001098:	af00      	add	r7, sp, #0
 800109a:	0002      	movs	r2, r0
 800109c:	1dfb      	adds	r3, r7, #7
 800109e:	701a      	strb	r2, [r3, #0]
 80010a0:	240e      	movs	r4, #14
 80010a2:	193b      	adds	r3, r7, r4
 80010a4:	2200      	movs	r2, #0
 80010a6:	701a      	strb	r2, [r3, #0]
 80010a8:	1dfb      	adds	r3, r7, #7
 80010aa:	781b      	ldrb	r3, [r3, #0]
 80010ac:	22a6      	movs	r2, #166	; 0xa6
 80010ae:	0052      	lsls	r2, r2, #1
 80010b0:	435a      	muls	r2, r3
 80010b2:	4b4d      	ldr	r3, [pc, #308]	; (80011e8 <get_dist_mm+0x154>)
 80010b4:	18d3      	adds	r3, r2, r3
 80010b6:	0025      	movs	r5, r4
 80010b8:	193c      	adds	r4, r7, r4
 80010ba:	4a4c      	ldr	r2, [pc, #304]	; (80011ec <get_dist_mm+0x158>)
 80010bc:	0011      	movs	r1, r2
 80010be:	0018      	movs	r0, r3
 80010c0:	f004 fb18 	bl	80056f4 <VL53L0X_GetRangingMeasurementData>
 80010c4:	0003      	movs	r3, r0
 80010c6:	7023      	strb	r3, [r4, #0]
 80010c8:	002c      	movs	r4, r5
 80010ca:	193b      	adds	r3, r7, r4
 80010cc:	781b      	ldrb	r3, [r3, #0]
 80010ce:	b25b      	sxtb	r3, r3
 80010d0:	2b00      	cmp	r3, #0
 80010d2:	d16b      	bne.n	80011ac <get_dist_mm+0x118>
 80010d4:	1dfb      	adds	r3, r7, #7
 80010d6:	781b      	ldrb	r3, [r3, #0]
 80010d8:	22a6      	movs	r2, #166	; 0xa6
 80010da:	0052      	lsls	r2, r2, #1
 80010dc:	435a      	muls	r2, r3
 80010de:	4b42      	ldr	r3, [pc, #264]	; (80011e8 <get_dist_mm+0x154>)
 80010e0:	18d3      	adds	r3, r2, r3
 80010e2:	0025      	movs	r5, r4
 80010e4:	193c      	adds	r4, r7, r4
 80010e6:	2104      	movs	r1, #4
 80010e8:	0018      	movs	r0, r3
 80010ea:	f004 fe5f 	bl	8005dac <VL53L0X_ClearInterruptMask>
 80010ee:	0003      	movs	r3, r0
 80010f0:	7023      	strb	r3, [r4, #0]
 80010f2:	002c      	movs	r4, r5
 80010f4:	193b      	adds	r3, r7, r4
 80010f6:	781b      	ldrb	r3, [r3, #0]
 80010f8:	b25b      	sxtb	r3, r3
 80010fa:	2b00      	cmp	r3, #0
 80010fc:	d158      	bne.n	80011b0 <get_dist_mm+0x11c>
 80010fe:	4b3b      	ldr	r3, [pc, #236]	; (80011ec <get_dist_mm+0x158>)
 8001100:	7e1b      	ldrb	r3, [r3, #24]
 8001102:	2b00      	cmp	r3, #0
 8001104:	d040      	beq.n	8001188 <get_dist_mm+0xf4>
 8001106:	1dfb      	adds	r3, r7, #7
 8001108:	781b      	ldrb	r3, [r3, #0]
 800110a:	4a39      	ldr	r2, [pc, #228]	; (80011f0 <get_dist_mm+0x15c>)
 800110c:	18d3      	adds	r3, r2, r3
 800110e:	7a9b      	ldrb	r3, [r3, #10]
 8001110:	3301      	adds	r3, #1
 8001112:	210a      	movs	r1, #10
 8001114:	0018      	movs	r0, r3
 8001116:	f7ff f967 	bl	80003e8 <__aeabi_idivmod>
 800111a:	000b      	movs	r3, r1
 800111c:	001a      	movs	r2, r3
 800111e:	1dfb      	adds	r3, r7, #7
 8001120:	781b      	ldrb	r3, [r3, #0]
 8001122:	b2d1      	uxtb	r1, r2
 8001124:	4a32      	ldr	r2, [pc, #200]	; (80011f0 <get_dist_mm+0x15c>)
 8001126:	18d3      	adds	r3, r2, r3
 8001128:	1c0a      	adds	r2, r1, #0
 800112a:	729a      	strb	r2, [r3, #10]
 800112c:	1dfb      	adds	r3, r7, #7
 800112e:	781b      	ldrb	r3, [r3, #0]
 8001130:	4a2f      	ldr	r2, [pc, #188]	; (80011f0 <get_dist_mm+0x15c>)
 8001132:	18d3      	adds	r3, r2, r3
 8001134:	7a9b      	ldrb	r3, [r3, #10]
 8001136:	2b00      	cmp	r3, #0
 8001138:	d126      	bne.n	8001188 <get_dist_mm+0xf4>
 800113a:	1dfb      	adds	r3, r7, #7
 800113c:	781b      	ldrb	r3, [r3, #0]
 800113e:	22a6      	movs	r2, #166	; 0xa6
 8001140:	0052      	lsls	r2, r2, #1
 8001142:	435a      	muls	r2, r3
 8001144:	4b28      	ldr	r3, [pc, #160]	; (80011e8 <get_dist_mm+0x154>)
 8001146:	18d3      	adds	r3, r2, r3
 8001148:	0025      	movs	r5, r4
 800114a:	193c      	adds	r4, r7, r4
 800114c:	0018      	movs	r0, r3
 800114e:	f004 fa25 	bl	800559c <VL53L0X_StopMeasurement>
 8001152:	0003      	movs	r3, r0
 8001154:	7023      	strb	r3, [r4, #0]
 8001156:	002c      	movs	r4, r5
 8001158:	193b      	adds	r3, r7, r4
 800115a:	781b      	ldrb	r3, [r3, #0]
 800115c:	b25b      	sxtb	r3, r3
 800115e:	2b00      	cmp	r3, #0
 8001160:	d128      	bne.n	80011b4 <get_dist_mm+0x120>
 8001162:	1dfb      	adds	r3, r7, #7
 8001164:	781b      	ldrb	r3, [r3, #0]
 8001166:	22a6      	movs	r2, #166	; 0xa6
 8001168:	0052      	lsls	r2, r2, #1
 800116a:	435a      	muls	r2, r3
 800116c:	4b1e      	ldr	r3, [pc, #120]	; (80011e8 <get_dist_mm+0x154>)
 800116e:	18d3      	adds	r3, r2, r3
 8001170:	0025      	movs	r5, r4
 8001172:	193c      	adds	r4, r7, r4
 8001174:	0018      	movs	r0, r3
 8001176:	f004 f911 	bl	800539c <VL53L0X_StartMeasurement>
 800117a:	0003      	movs	r3, r0
 800117c:	7023      	strb	r3, [r4, #0]
 800117e:	197b      	adds	r3, r7, r5
 8001180:	781b      	ldrb	r3, [r3, #0]
 8001182:	b25b      	sxtb	r3, r3
 8001184:	2b00      	cmp	r3, #0
 8001186:	d117      	bne.n	80011b8 <get_dist_mm+0x124>
 8001188:	4b18      	ldr	r3, [pc, #96]	; (80011ec <get_dist_mm+0x158>)
 800118a:	891b      	ldrh	r3, [r3, #8]
 800118c:	2be1      	cmp	r3, #225	; 0xe1
 800118e:	d904      	bls.n	800119a <get_dist_mm+0x106>
 8001190:	230f      	movs	r3, #15
 8001192:	18fb      	adds	r3, r7, r3
 8001194:	22e1      	movs	r2, #225	; 0xe1
 8001196:	701a      	strb	r2, [r3, #0]
 8001198:	e004      	b.n	80011a4 <get_dist_mm+0x110>
 800119a:	4b14      	ldr	r3, [pc, #80]	; (80011ec <get_dist_mm+0x158>)
 800119c:	891a      	ldrh	r2, [r3, #8]
 800119e:	230f      	movs	r3, #15
 80011a0:	18fb      	adds	r3, r7, r3
 80011a2:	701a      	strb	r2, [r3, #0]
 80011a4:	230f      	movs	r3, #15
 80011a6:	18fb      	adds	r3, r7, r3
 80011a8:	781b      	ldrb	r3, [r3, #0]
 80011aa:	e018      	b.n	80011de <get_dist_mm+0x14a>
 80011ac:	46c0      	nop			; (mov r8, r8)
 80011ae:	e004      	b.n	80011ba <get_dist_mm+0x126>
 80011b0:	46c0      	nop			; (mov r8, r8)
 80011b2:	e002      	b.n	80011ba <get_dist_mm+0x126>
 80011b4:	46c0      	nop			; (mov r8, r8)
 80011b6:	e000      	b.n	80011ba <get_dist_mm+0x126>
 80011b8:	46c0      	nop			; (mov r8, r8)
 80011ba:	4b0e      	ldr	r3, [pc, #56]	; (80011f4 <get_dist_mm+0x160>)
 80011bc:	681a      	ldr	r2, [r3, #0]
 80011be:	1dfb      	adds	r3, r7, #7
 80011c0:	781b      	ldrb	r3, [r3, #0]
 80011c2:	00db      	lsls	r3, r3, #3
 80011c4:	18d3      	adds	r3, r2, r3
 80011c6:	6818      	ldr	r0, [r3, #0]
 80011c8:	4b0a      	ldr	r3, [pc, #40]	; (80011f4 <get_dist_mm+0x160>)
 80011ca:	681a      	ldr	r2, [r3, #0]
 80011cc:	1dfb      	adds	r3, r7, #7
 80011ce:	781b      	ldrb	r3, [r3, #0]
 80011d0:	00db      	lsls	r3, r3, #3
 80011d2:	18d3      	adds	r3, r2, r3
 80011d4:	685b      	ldr	r3, [r3, #4]
 80011d6:	0019      	movs	r1, r3
 80011d8:	f7ff fc2e 	bl	8000a38 <LL_GPIO_ResetOutputPin>
 80011dc:	23ff      	movs	r3, #255	; 0xff
 80011de:	0018      	movs	r0, r3
 80011e0:	46bd      	mov	sp, r7
 80011e2:	b004      	add	sp, #16
 80011e4:	bdb0      	pop	{r4, r5, r7, pc}
 80011e6:	46c0      	nop			; (mov r8, r8)
 80011e8:	2000071c 	.word	0x2000071c
 80011ec:	20000b04 	.word	0x20000b04
 80011f0:	2000070c 	.word	0x2000070c
 80011f4:	20000b00 	.word	0x20000b00

080011f8 <get_dist>:
 80011f8:	b5b0      	push	{r4, r5, r7, lr}
 80011fa:	b084      	sub	sp, #16
 80011fc:	af00      	add	r7, sp, #0
 80011fe:	0002      	movs	r2, r0
 8001200:	1dfb      	adds	r3, r7, #7
 8001202:	701a      	strb	r2, [r3, #0]
 8001204:	240e      	movs	r4, #14
 8001206:	193b      	adds	r3, r7, r4
 8001208:	2200      	movs	r2, #0
 800120a:	701a      	strb	r2, [r3, #0]
 800120c:	1dfb      	adds	r3, r7, #7
 800120e:	781b      	ldrb	r3, [r3, #0]
 8001210:	22a6      	movs	r2, #166	; 0xa6
 8001212:	0052      	lsls	r2, r2, #1
 8001214:	435a      	muls	r2, r3
 8001216:	4b56      	ldr	r3, [pc, #344]	; (8001370 <get_dist+0x178>)
 8001218:	18d3      	adds	r3, r2, r3
 800121a:	0025      	movs	r5, r4
 800121c:	193c      	adds	r4, r7, r4
 800121e:	4a55      	ldr	r2, [pc, #340]	; (8001374 <get_dist+0x17c>)
 8001220:	0011      	movs	r1, r2
 8001222:	0018      	movs	r0, r3
 8001224:	f004 fa66 	bl	80056f4 <VL53L0X_GetRangingMeasurementData>
 8001228:	0003      	movs	r3, r0
 800122a:	7023      	strb	r3, [r4, #0]
 800122c:	002c      	movs	r4, r5
 800122e:	193b      	adds	r3, r7, r4
 8001230:	781b      	ldrb	r3, [r3, #0]
 8001232:	b25b      	sxtb	r3, r3
 8001234:	2b00      	cmp	r3, #0
 8001236:	d000      	beq.n	800123a <get_dist+0x42>
 8001238:	e07c      	b.n	8001334 <get_dist+0x13c>
 800123a:	1dfb      	adds	r3, r7, #7
 800123c:	781b      	ldrb	r3, [r3, #0]
 800123e:	22a6      	movs	r2, #166	; 0xa6
 8001240:	0052      	lsls	r2, r2, #1
 8001242:	435a      	muls	r2, r3
 8001244:	4b4a      	ldr	r3, [pc, #296]	; (8001370 <get_dist+0x178>)
 8001246:	18d3      	adds	r3, r2, r3
 8001248:	0025      	movs	r5, r4
 800124a:	193c      	adds	r4, r7, r4
 800124c:	2104      	movs	r1, #4
 800124e:	0018      	movs	r0, r3
 8001250:	f004 fdac 	bl	8005dac <VL53L0X_ClearInterruptMask>
 8001254:	0003      	movs	r3, r0
 8001256:	7023      	strb	r3, [r4, #0]
 8001258:	002c      	movs	r4, r5
 800125a:	193b      	adds	r3, r7, r4
 800125c:	781b      	ldrb	r3, [r3, #0]
 800125e:	b25b      	sxtb	r3, r3
 8001260:	2b00      	cmp	r3, #0
 8001262:	d169      	bne.n	8001338 <get_dist+0x140>
 8001264:	4b43      	ldr	r3, [pc, #268]	; (8001374 <get_dist+0x17c>)
 8001266:	7e1b      	ldrb	r3, [r3, #24]
 8001268:	2b00      	cmp	r3, #0
 800126a:	d040      	beq.n	80012ee <get_dist+0xf6>
 800126c:	1dfb      	adds	r3, r7, #7
 800126e:	781b      	ldrb	r3, [r3, #0]
 8001270:	4a41      	ldr	r2, [pc, #260]	; (8001378 <get_dist+0x180>)
 8001272:	18d3      	adds	r3, r2, r3
 8001274:	7a9b      	ldrb	r3, [r3, #10]
 8001276:	3301      	adds	r3, #1
 8001278:	210a      	movs	r1, #10
 800127a:	0018      	movs	r0, r3
 800127c:	f7ff f8b4 	bl	80003e8 <__aeabi_idivmod>
 8001280:	000b      	movs	r3, r1
 8001282:	001a      	movs	r2, r3
 8001284:	1dfb      	adds	r3, r7, #7
 8001286:	781b      	ldrb	r3, [r3, #0]
 8001288:	b2d1      	uxtb	r1, r2
 800128a:	4a3b      	ldr	r2, [pc, #236]	; (8001378 <get_dist+0x180>)
 800128c:	18d3      	adds	r3, r2, r3
 800128e:	1c0a      	adds	r2, r1, #0
 8001290:	729a      	strb	r2, [r3, #10]
 8001292:	1dfb      	adds	r3, r7, #7
 8001294:	781b      	ldrb	r3, [r3, #0]
 8001296:	4a38      	ldr	r2, [pc, #224]	; (8001378 <get_dist+0x180>)
 8001298:	18d3      	adds	r3, r2, r3
 800129a:	7a9b      	ldrb	r3, [r3, #10]
 800129c:	2b00      	cmp	r3, #0
 800129e:	d126      	bne.n	80012ee <get_dist+0xf6>
 80012a0:	1dfb      	adds	r3, r7, #7
 80012a2:	781b      	ldrb	r3, [r3, #0]
 80012a4:	22a6      	movs	r2, #166	; 0xa6
 80012a6:	0052      	lsls	r2, r2, #1
 80012a8:	435a      	muls	r2, r3
 80012aa:	4b31      	ldr	r3, [pc, #196]	; (8001370 <get_dist+0x178>)
 80012ac:	18d3      	adds	r3, r2, r3
 80012ae:	0025      	movs	r5, r4
 80012b0:	193c      	adds	r4, r7, r4
 80012b2:	0018      	movs	r0, r3
 80012b4:	f004 f972 	bl	800559c <VL53L0X_StopMeasurement>
 80012b8:	0003      	movs	r3, r0
 80012ba:	7023      	strb	r3, [r4, #0]
 80012bc:	002c      	movs	r4, r5
 80012be:	193b      	adds	r3, r7, r4
 80012c0:	781b      	ldrb	r3, [r3, #0]
 80012c2:	b25b      	sxtb	r3, r3
 80012c4:	2b00      	cmp	r3, #0
 80012c6:	d139      	bne.n	800133c <get_dist+0x144>
 80012c8:	1dfb      	adds	r3, r7, #7
 80012ca:	781b      	ldrb	r3, [r3, #0]
 80012cc:	22a6      	movs	r2, #166	; 0xa6
 80012ce:	0052      	lsls	r2, r2, #1
 80012d0:	435a      	muls	r2, r3
 80012d2:	4b27      	ldr	r3, [pc, #156]	; (8001370 <get_dist+0x178>)
 80012d4:	18d3      	adds	r3, r2, r3
 80012d6:	0025      	movs	r5, r4
 80012d8:	193c      	adds	r4, r7, r4
 80012da:	0018      	movs	r0, r3
 80012dc:	f004 f85e 	bl	800539c <VL53L0X_StartMeasurement>
 80012e0:	0003      	movs	r3, r0
 80012e2:	7023      	strb	r3, [r4, #0]
 80012e4:	197b      	adds	r3, r7, r5
 80012e6:	781b      	ldrb	r3, [r3, #0]
 80012e8:	b25b      	sxtb	r3, r3
 80012ea:	2b00      	cmp	r3, #0
 80012ec:	d128      	bne.n	8001340 <get_dist+0x148>
 80012ee:	4b21      	ldr	r3, [pc, #132]	; (8001374 <get_dist+0x17c>)
 80012f0:	891b      	ldrh	r3, [r3, #8]
 80012f2:	210a      	movs	r1, #10
 80012f4:	0018      	movs	r0, r3
 80012f6:	f7fe ff07 	bl	8000108 <__udivsi3>
 80012fa:	0003      	movs	r3, r0
 80012fc:	b29a      	uxth	r2, r3
 80012fe:	240f      	movs	r4, #15
 8001300:	193b      	adds	r3, r7, r4
 8001302:	701a      	strb	r2, [r3, #0]
 8001304:	4b1b      	ldr	r3, [pc, #108]	; (8001374 <get_dist+0x17c>)
 8001306:	891b      	ldrh	r3, [r3, #8]
 8001308:	210a      	movs	r1, #10
 800130a:	0018      	movs	r0, r3
 800130c:	f7fe ff82 	bl	8000214 <__aeabi_uidivmod>
 8001310:	000b      	movs	r3, r1
 8001312:	b29b      	uxth	r3, r3
 8001314:	2b04      	cmp	r3, #4
 8001316:	d904      	bls.n	8001322 <get_dist+0x12a>
 8001318:	193b      	adds	r3, r7, r4
 800131a:	193a      	adds	r2, r7, r4
 800131c:	7812      	ldrb	r2, [r2, #0]
 800131e:	3201      	adds	r2, #1
 8001320:	701a      	strb	r2, [r3, #0]
 8001322:	230f      	movs	r3, #15
 8001324:	18fb      	adds	r3, r7, r3
 8001326:	781b      	ldrb	r3, [r3, #0]
 8001328:	b2da      	uxtb	r2, r3
 800132a:	2a32      	cmp	r2, #50	; 0x32
 800132c:	d900      	bls.n	8001330 <get_dist+0x138>
 800132e:	2332      	movs	r3, #50	; 0x32
 8001330:	b2db      	uxtb	r3, r3
 8001332:	e018      	b.n	8001366 <get_dist+0x16e>
 8001334:	46c0      	nop			; (mov r8, r8)
 8001336:	e004      	b.n	8001342 <get_dist+0x14a>
 8001338:	46c0      	nop			; (mov r8, r8)
 800133a:	e002      	b.n	8001342 <get_dist+0x14a>
 800133c:	46c0      	nop			; (mov r8, r8)
 800133e:	e000      	b.n	8001342 <get_dist+0x14a>
 8001340:	46c0      	nop			; (mov r8, r8)
 8001342:	4b0e      	ldr	r3, [pc, #56]	; (800137c <get_dist+0x184>)
 8001344:	681a      	ldr	r2, [r3, #0]
 8001346:	1dfb      	adds	r3, r7, #7
 8001348:	781b      	ldrb	r3, [r3, #0]
 800134a:	00db      	lsls	r3, r3, #3
 800134c:	18d3      	adds	r3, r2, r3
 800134e:	6818      	ldr	r0, [r3, #0]
 8001350:	4b0a      	ldr	r3, [pc, #40]	; (800137c <get_dist+0x184>)
 8001352:	681a      	ldr	r2, [r3, #0]
 8001354:	1dfb      	adds	r3, r7, #7
 8001356:	781b      	ldrb	r3, [r3, #0]
 8001358:	00db      	lsls	r3, r3, #3
 800135a:	18d3      	adds	r3, r2, r3
 800135c:	685b      	ldr	r3, [r3, #4]
 800135e:	0019      	movs	r1, r3
 8001360:	f7ff fb6a 	bl	8000a38 <LL_GPIO_ResetOutputPin>
 8001364:	23ff      	movs	r3, #255	; 0xff
 8001366:	0018      	movs	r0, r3
 8001368:	46bd      	mov	sp, r7
 800136a:	b004      	add	sp, #16
 800136c:	bdb0      	pop	{r4, r5, r7, pc}
 800136e:	46c0      	nop			; (mov r8, r8)
 8001370:	2000071c 	.word	0x2000071c
 8001374:	20000b20 	.word	0x20000b20
 8001378:	2000070c 	.word	0x2000070c
 800137c:	20000b00 	.word	0x20000b00

08001380 <TIM2_IRQHandler>:
 8001380:	b5b0      	push	{r4, r5, r7, lr}
 8001382:	af00      	add	r7, sp, #0
 8001384:	2380      	movs	r3, #128	; 0x80
 8001386:	05db      	lsls	r3, r3, #23
 8001388:	0018      	movs	r0, r3
 800138a:	f7ff fbaf 	bl	8000aec <LL_TIM_ClearFlag_UPDATE>
 800138e:	4b35      	ldr	r3, [pc, #212]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001390:	7a1b      	ldrb	r3, [r3, #8]
 8001392:	2b00      	cmp	r3, #0
 8001394:	d003      	beq.n	800139e <TIM2_IRQHandler+0x1e>
 8001396:	4b33      	ldr	r3, [pc, #204]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001398:	2201      	movs	r2, #1
 800139a:	725a      	strb	r2, [r3, #9]
 800139c:	e05f      	b.n	800145e <TIM2_IRQHandler+0xde>
 800139e:	4b31      	ldr	r3, [pc, #196]	; (8001464 <TIM2_IRQHandler+0xe4>)
 80013a0:	2200      	movs	r2, #0
 80013a2:	725a      	strb	r2, [r3, #9]
 80013a4:	4b30      	ldr	r3, [pc, #192]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013a6:	781b      	ldrb	r3, [r3, #0]
 80013a8:	3301      	adds	r3, #1
 80013aa:	2103      	movs	r1, #3
 80013ac:	0018      	movs	r0, r3
 80013ae:	f7ff f81b 	bl	80003e8 <__aeabi_idivmod>
 80013b2:	000b      	movs	r3, r1
 80013b4:	b2da      	uxtb	r2, r3
 80013b6:	4b2c      	ldr	r3, [pc, #176]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013b8:	701a      	strb	r2, [r3, #0]
 80013ba:	4b2b      	ldr	r3, [pc, #172]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013bc:	781b      	ldrb	r3, [r3, #0]
 80013be:	001a      	movs	r2, r3
 80013c0:	4b28      	ldr	r3, [pc, #160]	; (8001464 <TIM2_IRQHandler+0xe4>)
 80013c2:	189b      	adds	r3, r3, r2
 80013c4:	78db      	ldrb	r3, [r3, #3]
 80013c6:	2b00      	cmp	r3, #0
 80013c8:	d120      	bne.n	800140c <TIM2_IRQHandler+0x8c>
 80013ca:	4b27      	ldr	r3, [pc, #156]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013cc:	781a      	ldrb	r2, [r3, #0]
 80013ce:	4b26      	ldr	r3, [pc, #152]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013d0:	781b      	ldrb	r3, [r3, #0]
 80013d2:	001c      	movs	r4, r3
 80013d4:	0010      	movs	r0, r2
 80013d6:	f7ff ff0f 	bl	80011f8 <get_dist>
 80013da:	0003      	movs	r3, r0
 80013dc:	001a      	movs	r2, r3
 80013de:	4b21      	ldr	r3, [pc, #132]	; (8001464 <TIM2_IRQHandler+0xe4>)
 80013e0:	551a      	strb	r2, [r3, r4]
 80013e2:	4b21      	ldr	r3, [pc, #132]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013e4:	781b      	ldrb	r3, [r3, #0]
 80013e6:	001a      	movs	r2, r3
 80013e8:	4b1e      	ldr	r3, [pc, #120]	; (8001464 <TIM2_IRQHandler+0xe4>)
 80013ea:	5c9b      	ldrb	r3, [r3, r2]
 80013ec:	2bff      	cmp	r3, #255	; 0xff
 80013ee:	d10f      	bne.n	8001410 <TIM2_IRQHandler+0x90>
 80013f0:	4b1d      	ldr	r3, [pc, #116]	; (8001468 <TIM2_IRQHandler+0xe8>)
 80013f2:	781b      	ldrb	r3, [r3, #0]
 80013f4:	001a      	movs	r2, r3
 80013f6:	4b1b      	ldr	r3, [pc, #108]	; (8001464 <TIM2_IRQHandler+0xe4>)
 80013f8:	189b      	adds	r3, r3, r2
 80013fa:	2201      	movs	r2, #1
 80013fc:	70da      	strb	r2, [r3, #3]
 80013fe:	4b19      	ldr	r3, [pc, #100]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001400:	799b      	ldrb	r3, [r3, #6]
 8001402:	3301      	adds	r3, #1
 8001404:	b2da      	uxtb	r2, r3
 8001406:	4b17      	ldr	r3, [pc, #92]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001408:	719a      	strb	r2, [r3, #6]
 800140a:	e002      	b.n	8001412 <TIM2_IRQHandler+0x92>
 800140c:	46c0      	nop			; (mov r8, r8)
 800140e:	e000      	b.n	8001412 <TIM2_IRQHandler+0x92>
 8001410:	46c0      	nop			; (mov r8, r8)
 8001412:	f7ff fe17 	bl	8001044 <col_av_any>
 8001416:	0003      	movs	r3, r0
 8001418:	001a      	movs	r2, r3
 800141a:	4b12      	ldr	r3, [pc, #72]	; (8001464 <TIM2_IRQHandler+0xe4>)
 800141c:	71da      	strb	r2, [r3, #7]
 800141e:	4b11      	ldr	r3, [pc, #68]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001420:	79db      	ldrb	r3, [r3, #7]
 8001422:	2b00      	cmp	r3, #0
 8001424:	d006      	beq.n	8001434 <TIM2_IRQHandler+0xb4>
 8001426:	4b0f      	ldr	r3, [pc, #60]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001428:	799a      	ldrb	r2, [r3, #6]
 800142a:	4b10      	ldr	r3, [pc, #64]	; (800146c <TIM2_IRQHandler+0xec>)
 800142c:	0011      	movs	r1, r2
 800142e:	0018      	movs	r0, r3
 8001430:	f001 fa7a 	bl	8002928 <er_man_add_err>
 8001434:	4b0c      	ldr	r3, [pc, #48]	; (8001468 <TIM2_IRQHandler+0xe8>)
 8001436:	781b      	ldrb	r3, [r3, #0]
 8001438:	2b02      	cmp	r3, #2
 800143a:	d110      	bne.n	800145e <TIM2_IRQHandler+0xde>
 800143c:	2002      	movs	r0, #2
 800143e:	f7ff fe29 	bl	8001094 <get_dist_mm>
 8001442:	0003      	movs	r3, r0
 8001444:	001a      	movs	r2, r3
 8001446:	4b07      	ldr	r3, [pc, #28]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001448:	709a      	strb	r2, [r3, #2]
 800144a:	4b06      	ldr	r3, [pc, #24]	; (8001464 <TIM2_IRQHandler+0xe4>)
 800144c:	2103      	movs	r1, #3
 800144e:	0018      	movs	r0, r3
 8001450:	f001 fa20 	bl	8002894 <err_man_set_dist>
 8001454:	4b03      	ldr	r3, [pc, #12]	; (8001464 <TIM2_IRQHandler+0xe4>)
 8001456:	2103      	movs	r1, #3
 8001458:	0018      	movs	r0, r3
 800145a:	f001 fe89 	bl	8003170 <comm_send_msg>
 800145e:	46bd      	mov	sp, r7
 8001460:	bdb0      	pop	{r4, r5, r7, pc}
 8001462:	46c0      	nop			; (mov r8, r8)
 8001464:	2000070c 	.word	0x2000070c
 8001468:	20000b3c 	.word	0x20000b3c
 800146c:	2000070f 	.word	0x2000070f

08001470 <LL_SPI_Enable>:
 8001470:	b580      	push	{r7, lr}
 8001472:	b082      	sub	sp, #8
 8001474:	af00      	add	r7, sp, #0
 8001476:	6078      	str	r0, [r7, #4]
 8001478:	687b      	ldr	r3, [r7, #4]
 800147a:	681b      	ldr	r3, [r3, #0]
 800147c:	2240      	movs	r2, #64	; 0x40
 800147e:	431a      	orrs	r2, r3
 8001480:	687b      	ldr	r3, [r7, #4]
 8001482:	601a      	str	r2, [r3, #0]
 8001484:	46c0      	nop			; (mov r8, r8)
 8001486:	46bd      	mov	sp, r7
 8001488:	b002      	add	sp, #8
 800148a:	bd80      	pop	{r7, pc}

0800148c <LL_SPI_SetMode>:
 800148c:	b580      	push	{r7, lr}
 800148e:	b082      	sub	sp, #8
 8001490:	af00      	add	r7, sp, #0
 8001492:	6078      	str	r0, [r7, #4]
 8001494:	6039      	str	r1, [r7, #0]
 8001496:	687b      	ldr	r3, [r7, #4]
 8001498:	681b      	ldr	r3, [r3, #0]
 800149a:	4a05      	ldr	r2, [pc, #20]	; (80014b0 <LL_SPI_SetMode+0x24>)
 800149c:	401a      	ands	r2, r3
 800149e:	683b      	ldr	r3, [r7, #0]
 80014a0:	431a      	orrs	r2, r3
 80014a2:	687b      	ldr	r3, [r7, #4]
 80014a4:	601a      	str	r2, [r3, #0]
 80014a6:	46c0      	nop			; (mov r8, r8)
 80014a8:	46bd      	mov	sp, r7
 80014aa:	b002      	add	sp, #8
 80014ac:	bd80      	pop	{r7, pc}
 80014ae:	46c0      	nop			; (mov r8, r8)
 80014b0:	fffffefb 	.word	0xfffffefb

080014b4 <LL_SPI_SetBaudRatePrescaler>:
 80014b4:	b580      	push	{r7, lr}
 80014b6:	b082      	sub	sp, #8
 80014b8:	af00      	add	r7, sp, #0
 80014ba:	6078      	str	r0, [r7, #4]
 80014bc:	6039      	str	r1, [r7, #0]
 80014be:	687b      	ldr	r3, [r7, #4]
 80014c0:	681b      	ldr	r3, [r3, #0]
 80014c2:	2238      	movs	r2, #56	; 0x38
 80014c4:	4393      	bics	r3, r2
 80014c6:	001a      	movs	r2, r3
 80014c8:	683b      	ldr	r3, [r7, #0]
 80014ca:	431a      	orrs	r2, r3
 80014cc:	687b      	ldr	r3, [r7, #4]
 80014ce:	601a      	str	r2, [r3, #0]
 80014d0:	46c0      	nop			; (mov r8, r8)
 80014d2:	46bd      	mov	sp, r7
 80014d4:	b002      	add	sp, #8
 80014d6:	bd80      	pop	{r7, pc}

080014d8 <LL_SPI_SetTransferBitOrder>:
 80014d8:	b580      	push	{r7, lr}
 80014da:	b082      	sub	sp, #8
 80014dc:	af00      	add	r7, sp, #0
 80014de:	6078      	str	r0, [r7, #4]
 80014e0:	6039      	str	r1, [r7, #0]
 80014e2:	687b      	ldr	r3, [r7, #4]
 80014e4:	681b      	ldr	r3, [r3, #0]
 80014e6:	2280      	movs	r2, #128	; 0x80
 80014e8:	4393      	bics	r3, r2
 80014ea:	001a      	movs	r2, r3
 80014ec:	683b      	ldr	r3, [r7, #0]
 80014ee:	431a      	orrs	r2, r3
 80014f0:	687b      	ldr	r3, [r7, #4]
 80014f2:	601a      	str	r2, [r3, #0]
 80014f4:	46c0      	nop			; (mov r8, r8)
 80014f6:	46bd      	mov	sp, r7
 80014f8:	b002      	add	sp, #8
 80014fa:	bd80      	pop	{r7, pc}

080014fc <LL_SPI_SetTransferDirection>:
 80014fc:	b580      	push	{r7, lr}
 80014fe:	b082      	sub	sp, #8
 8001500:	af00      	add	r7, sp, #0
 8001502:	6078      	str	r0, [r7, #4]
 8001504:	6039      	str	r1, [r7, #0]
 8001506:	687b      	ldr	r3, [r7, #4]
 8001508:	681b      	ldr	r3, [r3, #0]
 800150a:	4a05      	ldr	r2, [pc, #20]	; (8001520 <LL_SPI_SetTransferDirection+0x24>)
 800150c:	401a      	ands	r2, r3
 800150e:	683b      	ldr	r3, [r7, #0]
 8001510:	431a      	orrs	r2, r3
 8001512:	687b      	ldr	r3, [r7, #4]
 8001514:	601a      	str	r2, [r3, #0]
 8001516:	46c0      	nop			; (mov r8, r8)
 8001518:	46bd      	mov	sp, r7
 800151a:	b002      	add	sp, #8
 800151c:	bd80      	pop	{r7, pc}
 800151e:	46c0      	nop			; (mov r8, r8)
 8001520:	ffff3bff 	.word	0xffff3bff

08001524 <LL_SPI_SetDataWidth>:
 8001524:	b580      	push	{r7, lr}
 8001526:	b082      	sub	sp, #8
 8001528:	af00      	add	r7, sp, #0
 800152a:	6078      	str	r0, [r7, #4]
 800152c:	6039      	str	r1, [r7, #0]
 800152e:	687b      	ldr	r3, [r7, #4]
 8001530:	685b      	ldr	r3, [r3, #4]
 8001532:	4a05      	ldr	r2, [pc, #20]	; (8001548 <LL_SPI_SetDataWidth+0x24>)
 8001534:	401a      	ands	r2, r3
 8001536:	683b      	ldr	r3, [r7, #0]
 8001538:	431a      	orrs	r2, r3
 800153a:	687b      	ldr	r3, [r7, #4]
 800153c:	605a      	str	r2, [r3, #4]
 800153e:	46c0      	nop			; (mov r8, r8)
 8001540:	46bd      	mov	sp, r7
 8001542:	b002      	add	sp, #8
 8001544:	bd80      	pop	{r7, pc}
 8001546:	46c0      	nop			; (mov r8, r8)
 8001548:	fffff0ff 	.word	0xfffff0ff

0800154c <LL_SPI_EnableNSSPulseMgt>:
 800154c:	b580      	push	{r7, lr}
 800154e:	b082      	sub	sp, #8
 8001550:	af00      	add	r7, sp, #0
 8001552:	6078      	str	r0, [r7, #4]
 8001554:	687b      	ldr	r3, [r7, #4]
 8001556:	685b      	ldr	r3, [r3, #4]
 8001558:	2208      	movs	r2, #8
 800155a:	431a      	orrs	r2, r3
 800155c:	687b      	ldr	r3, [r7, #4]
 800155e:	605a      	str	r2, [r3, #4]
 8001560:	46c0      	nop			; (mov r8, r8)
 8001562:	46bd      	mov	sp, r7
 8001564:	b002      	add	sp, #8
 8001566:	bd80      	pop	{r7, pc}

08001568 <LL_SPI_IsActiveFlag_TXE>:
 8001568:	b580      	push	{r7, lr}
 800156a:	b082      	sub	sp, #8
 800156c:	af00      	add	r7, sp, #0
 800156e:	6078      	str	r0, [r7, #4]
 8001570:	687b      	ldr	r3, [r7, #4]
 8001572:	689b      	ldr	r3, [r3, #8]
 8001574:	2202      	movs	r2, #2
 8001576:	4013      	ands	r3, r2
 8001578:	3b02      	subs	r3, #2
 800157a:	425a      	negs	r2, r3
 800157c:	4153      	adcs	r3, r2
 800157e:	b2db      	uxtb	r3, r3
 8001580:	0018      	movs	r0, r3
 8001582:	46bd      	mov	sp, r7
 8001584:	b002      	add	sp, #8
 8001586:	bd80      	pop	{r7, pc}

08001588 <LL_SPI_TransmitData8>:
 8001588:	b580      	push	{r7, lr}
 800158a:	b082      	sub	sp, #8
 800158c:	af00      	add	r7, sp, #0
 800158e:	6078      	str	r0, [r7, #4]
 8001590:	000a      	movs	r2, r1
 8001592:	1cfb      	adds	r3, r7, #3
 8001594:	701a      	strb	r2, [r3, #0]
 8001596:	687b      	ldr	r3, [r7, #4]
 8001598:	330c      	adds	r3, #12
 800159a:	1cfa      	adds	r2, r7, #3
 800159c:	7812      	ldrb	r2, [r2, #0]
 800159e:	701a      	strb	r2, [r3, #0]
 80015a0:	46c0      	nop			; (mov r8, r8)
 80015a2:	46bd      	mov	sp, r7
 80015a4:	b002      	add	sp, #8
 80015a6:	bd80      	pop	{r7, pc}

080015a8 <LL_GPIO_SetPinMode>:
 80015a8:	b580      	push	{r7, lr}
 80015aa:	b084      	sub	sp, #16
 80015ac:	af00      	add	r7, sp, #0
 80015ae:	60f8      	str	r0, [r7, #12]
 80015b0:	60b9      	str	r1, [r7, #8]
 80015b2:	607a      	str	r2, [r7, #4]
 80015b4:	68fb      	ldr	r3, [r7, #12]
 80015b6:	6819      	ldr	r1, [r3, #0]
 80015b8:	68bb      	ldr	r3, [r7, #8]
 80015ba:	68ba      	ldr	r2, [r7, #8]
 80015bc:	435a      	muls	r2, r3
 80015be:	0013      	movs	r3, r2
 80015c0:	005b      	lsls	r3, r3, #1
 80015c2:	189b      	adds	r3, r3, r2
 80015c4:	43db      	mvns	r3, r3
 80015c6:	400b      	ands	r3, r1
 80015c8:	001a      	movs	r2, r3
 80015ca:	68bb      	ldr	r3, [r7, #8]
 80015cc:	68b9      	ldr	r1, [r7, #8]
 80015ce:	434b      	muls	r3, r1
 80015d0:	6879      	ldr	r1, [r7, #4]
 80015d2:	434b      	muls	r3, r1
 80015d4:	431a      	orrs	r2, r3
 80015d6:	68fb      	ldr	r3, [r7, #12]
 80015d8:	601a      	str	r2, [r3, #0]
 80015da:	46c0      	nop			; (mov r8, r8)
 80015dc:	46bd      	mov	sp, r7
 80015de:	b004      	add	sp, #16
 80015e0:	bd80      	pop	{r7, pc}

080015e2 <LL_GPIO_SetPinSpeed>:
 80015e2:	b580      	push	{r7, lr}
 80015e4:	b084      	sub	sp, #16
 80015e6:	af00      	add	r7, sp, #0
 80015e8:	60f8      	str	r0, [r7, #12]
 80015ea:	60b9      	str	r1, [r7, #8]
 80015ec:	607a      	str	r2, [r7, #4]
 80015ee:	68fb      	ldr	r3, [r7, #12]
 80015f0:	6899      	ldr	r1, [r3, #8]
 80015f2:	68bb      	ldr	r3, [r7, #8]
 80015f4:	68ba      	ldr	r2, [r7, #8]
 80015f6:	435a      	muls	r2, r3
 80015f8:	0013      	movs	r3, r2
 80015fa:	005b      	lsls	r3, r3, #1
 80015fc:	189b      	adds	r3, r3, r2
 80015fe:	43db      	mvns	r3, r3
 8001600:	400b      	ands	r3, r1
 8001602:	001a      	movs	r2, r3
 8001604:	68bb      	ldr	r3, [r7, #8]
 8001606:	68b9      	ldr	r1, [r7, #8]
 8001608:	434b      	muls	r3, r1
 800160a:	6879      	ldr	r1, [r7, #4]
 800160c:	434b      	muls	r3, r1
 800160e:	431a      	orrs	r2, r3
 8001610:	68fb      	ldr	r3, [r7, #12]
 8001612:	609a      	str	r2, [r3, #8]
 8001614:	46c0      	nop			; (mov r8, r8)
 8001616:	46bd      	mov	sp, r7
 8001618:	b004      	add	sp, #16
 800161a:	bd80      	pop	{r7, pc}

0800161c <LL_GPIO_SetAFPin_0_7>:
 800161c:	b580      	push	{r7, lr}
 800161e:	b084      	sub	sp, #16
 8001620:	af00      	add	r7, sp, #0
 8001622:	60f8      	str	r0, [r7, #12]
 8001624:	60b9      	str	r1, [r7, #8]
 8001626:	607a      	str	r2, [r7, #4]
 8001628:	68fb      	ldr	r3, [r7, #12]
 800162a:	6a19      	ldr	r1, [r3, #32]
 800162c:	68bb      	ldr	r3, [r7, #8]
 800162e:	68ba      	ldr	r2, [r7, #8]
 8001630:	4353      	muls	r3, r2
 8001632:	68ba      	ldr	r2, [r7, #8]
 8001634:	4353      	muls	r3, r2
 8001636:	68ba      	ldr	r2, [r7, #8]
 8001638:	435a      	muls	r2, r3
 800163a:	0013      	movs	r3, r2
 800163c:	011b      	lsls	r3, r3, #4
 800163e:	1a9b      	subs	r3, r3, r2
 8001640:	43db      	mvns	r3, r3
 8001642:	400b      	ands	r3, r1
 8001644:	001a      	movs	r2, r3
 8001646:	68bb      	ldr	r3, [r7, #8]
 8001648:	68b9      	ldr	r1, [r7, #8]
 800164a:	434b      	muls	r3, r1
 800164c:	68b9      	ldr	r1, [r7, #8]
 800164e:	434b      	muls	r3, r1
 8001650:	68b9      	ldr	r1, [r7, #8]
 8001652:	434b      	muls	r3, r1
 8001654:	6879      	ldr	r1, [r7, #4]
 8001656:	434b      	muls	r3, r1
 8001658:	431a      	orrs	r2, r3
 800165a:	68fb      	ldr	r3, [r7, #12]
 800165c:	621a      	str	r2, [r3, #32]
 800165e:	46c0      	nop			; (mov r8, r8)
 8001660:	46bd      	mov	sp, r7
 8001662:	b004      	add	sp, #16
 8001664:	bd80      	pop	{r7, pc}

08001666 <LL_GPIO_SetOutputPin>:
 8001666:	b580      	push	{r7, lr}
 8001668:	b082      	sub	sp, #8
 800166a:	af00      	add	r7, sp, #0
 800166c:	6078      	str	r0, [r7, #4]
 800166e:	6039      	str	r1, [r7, #0]
 8001670:	687b      	ldr	r3, [r7, #4]
 8001672:	683a      	ldr	r2, [r7, #0]
 8001674:	619a      	str	r2, [r3, #24]
 8001676:	46c0      	nop			; (mov r8, r8)
 8001678:	46bd      	mov	sp, r7
 800167a:	b002      	add	sp, #8
 800167c:	bd80      	pop	{r7, pc}

0800167e <LL_GPIO_ResetOutputPin>:
 800167e:	b580      	push	{r7, lr}
 8001680:	b082      	sub	sp, #8
 8001682:	af00      	add	r7, sp, #0
 8001684:	6078      	str	r0, [r7, #4]
 8001686:	6039      	str	r1, [r7, #0]
 8001688:	687b      	ldr	r3, [r7, #4]
 800168a:	683a      	ldr	r2, [r7, #0]
 800168c:	629a      	str	r2, [r3, #40]	; 0x28
 800168e:	46c0      	nop			; (mov r8, r8)
 8001690:	46bd      	mov	sp, r7
 8001692:	b002      	add	sp, #8
 8001694:	bd80      	pop	{r7, pc}
	...

08001698 <LL_AHB1_GRP1_EnableClock>:
 8001698:	b580      	push	{r7, lr}
 800169a:	b084      	sub	sp, #16
 800169c:	af00      	add	r7, sp, #0
 800169e:	6078      	str	r0, [r7, #4]
 80016a0:	4b07      	ldr	r3, [pc, #28]	; (80016c0 <LL_AHB1_GRP1_EnableClock+0x28>)
 80016a2:	6959      	ldr	r1, [r3, #20]
 80016a4:	4b06      	ldr	r3, [pc, #24]	; (80016c0 <LL_AHB1_GRP1_EnableClock+0x28>)
 80016a6:	687a      	ldr	r2, [r7, #4]
 80016a8:	430a      	orrs	r2, r1
 80016aa:	615a      	str	r2, [r3, #20]
 80016ac:	4b04      	ldr	r3, [pc, #16]	; (80016c0 <LL_AHB1_GRP1_EnableClock+0x28>)
 80016ae:	695b      	ldr	r3, [r3, #20]
 80016b0:	687a      	ldr	r2, [r7, #4]
 80016b2:	4013      	ands	r3, r2
 80016b4:	60fb      	str	r3, [r7, #12]
 80016b6:	68fb      	ldr	r3, [r7, #12]
 80016b8:	46c0      	nop			; (mov r8, r8)
 80016ba:	46bd      	mov	sp, r7
 80016bc:	b004      	add	sp, #16
 80016be:	bd80      	pop	{r7, pc}
 80016c0:	40021000 	.word	0x40021000

080016c4 <LL_APB1_GRP2_EnableClock>:
 80016c4:	b580      	push	{r7, lr}
 80016c6:	b084      	sub	sp, #16
 80016c8:	af00      	add	r7, sp, #0
 80016ca:	6078      	str	r0, [r7, #4]
 80016cc:	4b07      	ldr	r3, [pc, #28]	; (80016ec <LL_APB1_GRP2_EnableClock+0x28>)
 80016ce:	6999      	ldr	r1, [r3, #24]
 80016d0:	4b06      	ldr	r3, [pc, #24]	; (80016ec <LL_APB1_GRP2_EnableClock+0x28>)
 80016d2:	687a      	ldr	r2, [r7, #4]
 80016d4:	430a      	orrs	r2, r1
 80016d6:	619a      	str	r2, [r3, #24]
 80016d8:	4b04      	ldr	r3, [pc, #16]	; (80016ec <LL_APB1_GRP2_EnableClock+0x28>)
 80016da:	699b      	ldr	r3, [r3, #24]
 80016dc:	687a      	ldr	r2, [r7, #4]
 80016de:	4013      	ands	r3, r2
 80016e0:	60fb      	str	r3, [r7, #12]
 80016e2:	68fb      	ldr	r3, [r7, #12]
 80016e4:	46c0      	nop			; (mov r8, r8)
 80016e6:	46bd      	mov	sp, r7
 80016e8:	b004      	add	sp, #16
 80016ea:	bd80      	pop	{r7, pc}
 80016ec:	40021000 	.word	0x40021000

080016f0 <disp_hw_config>:
 80016f0:	b580      	push	{r7, lr}
 80016f2:	af00      	add	r7, sp, #0
 80016f4:	2380      	movs	r3, #128	; 0x80
 80016f6:	031b      	lsls	r3, r3, #12
 80016f8:	0018      	movs	r0, r3
 80016fa:	f7ff ffcd 	bl	8001698 <LL_AHB1_GRP1_EnableClock>
 80016fe:	4b34      	ldr	r3, [pc, #208]	; (80017d0 <disp_hw_config+0xe0>)
 8001700:	2201      	movs	r2, #1
 8001702:	2110      	movs	r1, #16
 8001704:	0018      	movs	r0, r3
 8001706:	f7ff ff4f 	bl	80015a8 <LL_GPIO_SetPinMode>
 800170a:	4b31      	ldr	r3, [pc, #196]	; (80017d0 <disp_hw_config+0xe0>)
 800170c:	2201      	movs	r2, #1
 800170e:	2120      	movs	r1, #32
 8001710:	0018      	movs	r0, r3
 8001712:	f7ff ff49 	bl	80015a8 <LL_GPIO_SetPinMode>
 8001716:	2380      	movs	r3, #128	; 0x80
 8001718:	029b      	lsls	r3, r3, #10
 800171a:	0018      	movs	r0, r3
 800171c:	f7ff ffbc 	bl	8001698 <LL_AHB1_GRP1_EnableClock>
 8001720:	2390      	movs	r3, #144	; 0x90
 8001722:	05db      	lsls	r3, r3, #23
 8001724:	2202      	movs	r2, #2
 8001726:	2120      	movs	r1, #32
 8001728:	0018      	movs	r0, r3
 800172a:	f7ff ff3d 	bl	80015a8 <LL_GPIO_SetPinMode>
 800172e:	2390      	movs	r3, #144	; 0x90
 8001730:	05db      	lsls	r3, r3, #23
 8001732:	2200      	movs	r2, #0
 8001734:	2120      	movs	r1, #32
 8001736:	0018      	movs	r0, r3
 8001738:	f7ff ff70 	bl	800161c <LL_GPIO_SetAFPin_0_7>
 800173c:	2390      	movs	r3, #144	; 0x90
 800173e:	05db      	lsls	r3, r3, #23
 8001740:	2203      	movs	r2, #3
 8001742:	2120      	movs	r1, #32
 8001744:	0018      	movs	r0, r3
 8001746:	f7ff ff4c 	bl	80015e2 <LL_GPIO_SetPinSpeed>
 800174a:	2390      	movs	r3, #144	; 0x90
 800174c:	05db      	lsls	r3, r3, #23
 800174e:	2202      	movs	r2, #2
 8001750:	2180      	movs	r1, #128	; 0x80
 8001752:	0018      	movs	r0, r3
 8001754:	f7ff ff28 	bl	80015a8 <LL_GPIO_SetPinMode>
 8001758:	2390      	movs	r3, #144	; 0x90
 800175a:	05db      	lsls	r3, r3, #23
 800175c:	2200      	movs	r2, #0
 800175e:	2180      	movs	r1, #128	; 0x80
 8001760:	0018      	movs	r0, r3
 8001762:	f7ff ff5b 	bl	800161c <LL_GPIO_SetAFPin_0_7>
 8001766:	2390      	movs	r3, #144	; 0x90
 8001768:	05db      	lsls	r3, r3, #23
 800176a:	2203      	movs	r2, #3
 800176c:	2180      	movs	r1, #128	; 0x80
 800176e:	0018      	movs	r0, r3
 8001770:	f7ff ff37 	bl	80015e2 <LL_GPIO_SetPinSpeed>
 8001774:	2380      	movs	r3, #128	; 0x80
 8001776:	015b      	lsls	r3, r3, #5
 8001778:	0018      	movs	r0, r3
 800177a:	f7ff ffa3 	bl	80016c4 <LL_APB1_GRP2_EnableClock>
 800177e:	2382      	movs	r3, #130	; 0x82
 8001780:	005b      	lsls	r3, r3, #1
 8001782:	4a14      	ldr	r2, [pc, #80]	; (80017d4 <disp_hw_config+0xe4>)
 8001784:	0019      	movs	r1, r3
 8001786:	0010      	movs	r0, r2
 8001788:	f7ff fe80 	bl	800148c <LL_SPI_SetMode>
 800178c:	4b11      	ldr	r3, [pc, #68]	; (80017d4 <disp_hw_config+0xe4>)
 800178e:	2100      	movs	r1, #0
 8001790:	0018      	movs	r0, r3
 8001792:	f7ff fe8f 	bl	80014b4 <LL_SPI_SetBaudRatePrescaler>
 8001796:	4b0f      	ldr	r3, [pc, #60]	; (80017d4 <disp_hw_config+0xe4>)
 8001798:	2100      	movs	r1, #0
 800179a:	0018      	movs	r0, r3
 800179c:	f7ff feae 	bl	80014fc <LL_SPI_SetTransferDirection>
 80017a0:	4b0c      	ldr	r3, [pc, #48]	; (80017d4 <disp_hw_config+0xe4>)
 80017a2:	2100      	movs	r1, #0
 80017a4:	0018      	movs	r0, r3
 80017a6:	f7ff fe97 	bl	80014d8 <LL_SPI_SetTransferBitOrder>
 80017aa:	23e0      	movs	r3, #224	; 0xe0
 80017ac:	00db      	lsls	r3, r3, #3
 80017ae:	4a09      	ldr	r2, [pc, #36]	; (80017d4 <disp_hw_config+0xe4>)
 80017b0:	0019      	movs	r1, r3
 80017b2:	0010      	movs	r0, r2
 80017b4:	f7ff feb6 	bl	8001524 <LL_SPI_SetDataWidth>
 80017b8:	4b06      	ldr	r3, [pc, #24]	; (80017d4 <disp_hw_config+0xe4>)
 80017ba:	0018      	movs	r0, r3
 80017bc:	f7ff fec6 	bl	800154c <LL_SPI_EnableNSSPulseMgt>
 80017c0:	4b04      	ldr	r3, [pc, #16]	; (80017d4 <disp_hw_config+0xe4>)
 80017c2:	0018      	movs	r0, r3
 80017c4:	f7ff fe54 	bl	8001470 <LL_SPI_Enable>
 80017c8:	46c0      	nop			; (mov r8, r8)
 80017ca:	46bd      	mov	sp, r7
 80017cc:	bd80      	pop	{r7, pc}
 80017ce:	46c0      	nop			; (mov r8, r8)
 80017d0:	48000800 	.word	0x48000800
 80017d4:	40013000 	.word	0x40013000

080017d8 <disp_reset>:
 80017d8:	b580      	push	{r7, lr}
 80017da:	b082      	sub	sp, #8
 80017dc:	af00      	add	r7, sp, #0
 80017de:	2396      	movs	r3, #150	; 0x96
 80017e0:	015b      	lsls	r3, r3, #5
 80017e2:	607b      	str	r3, [r7, #4]
 80017e4:	4b0e      	ldr	r3, [pc, #56]	; (8001820 <disp_reset+0x48>)
 80017e6:	2110      	movs	r1, #16
 80017e8:	0018      	movs	r0, r3
 80017ea:	f7ff ff48 	bl	800167e <LL_GPIO_ResetOutputPin>
 80017ee:	687b      	ldr	r3, [r7, #4]
 80017f0:	3b01      	subs	r3, #1
 80017f2:	607b      	str	r3, [r7, #4]
 80017f4:	687b      	ldr	r3, [r7, #4]
 80017f6:	2b00      	cmp	r3, #0
 80017f8:	d1f9      	bne.n	80017ee <disp_reset+0x16>
 80017fa:	4b09      	ldr	r3, [pc, #36]	; (8001820 <disp_reset+0x48>)
 80017fc:	2110      	movs	r1, #16
 80017fe:	0018      	movs	r0, r3
 8001800:	f7ff ff31 	bl	8001666 <LL_GPIO_SetOutputPin>
 8001804:	2396      	movs	r3, #150	; 0x96
 8001806:	015b      	lsls	r3, r3, #5
 8001808:	607b      	str	r3, [r7, #4]
 800180a:	687b      	ldr	r3, [r7, #4]
 800180c:	3b01      	subs	r3, #1
 800180e:	607b      	str	r3, [r7, #4]
 8001810:	687b      	ldr	r3, [r7, #4]
 8001812:	2b00      	cmp	r3, #0
 8001814:	d1f9      	bne.n	800180a <disp_reset+0x32>
 8001816:	46c0      	nop			; (mov r8, r8)
 8001818:	46bd      	mov	sp, r7
 800181a:	b002      	add	sp, #8
 800181c:	bd80      	pop	{r7, pc}
 800181e:	46c0      	nop			; (mov r8, r8)
 8001820:	48000800 	.word	0x48000800

08001824 <disp_send_cmd>:
 8001824:	b580      	push	{r7, lr}
 8001826:	b082      	sub	sp, #8
 8001828:	af00      	add	r7, sp, #0
 800182a:	0002      	movs	r2, r0
 800182c:	1dfb      	adds	r3, r7, #7
 800182e:	701a      	strb	r2, [r3, #0]
 8001830:	4b0b      	ldr	r3, [pc, #44]	; (8001860 <disp_send_cmd+0x3c>)
 8001832:	2120      	movs	r1, #32
 8001834:	0018      	movs	r0, r3
 8001836:	f7ff ff22 	bl	800167e <LL_GPIO_ResetOutputPin>
 800183a:	1dfb      	adds	r3, r7, #7
 800183c:	781b      	ldrb	r3, [r3, #0]
 800183e:	4a09      	ldr	r2, [pc, #36]	; (8001864 <disp_send_cmd+0x40>)
 8001840:	0019      	movs	r1, r3
 8001842:	0010      	movs	r0, r2
 8001844:	f7ff fea0 	bl	8001588 <LL_SPI_TransmitData8>
 8001848:	46c0      	nop			; (mov r8, r8)
 800184a:	4b06      	ldr	r3, [pc, #24]	; (8001864 <disp_send_cmd+0x40>)
 800184c:	0018      	movs	r0, r3
 800184e:	f7ff fe8b 	bl	8001568 <LL_SPI_IsActiveFlag_TXE>
 8001852:	1e03      	subs	r3, r0, #0
 8001854:	d0f9      	beq.n	800184a <disp_send_cmd+0x26>
 8001856:	46c0      	nop			; (mov r8, r8)
 8001858:	46bd      	mov	sp, r7
 800185a:	b002      	add	sp, #8
 800185c:	bd80      	pop	{r7, pc}
 800185e:	46c0      	nop			; (mov r8, r8)
 8001860:	48000800 	.word	0x48000800
 8001864:	40013000 	.word	0x40013000

08001868 <disp_send_data>:
 8001868:	b580      	push	{r7, lr}
 800186a:	b084      	sub	sp, #16
 800186c:	af00      	add	r7, sp, #0
 800186e:	6078      	str	r0, [r7, #4]
 8001870:	000a      	movs	r2, r1
 8001872:	1cfb      	adds	r3, r7, #3
 8001874:	701a      	strb	r2, [r3, #0]
 8001876:	230f      	movs	r3, #15
 8001878:	18fb      	adds	r3, r7, r3
 800187a:	2200      	movs	r2, #0
 800187c:	701a      	strb	r2, [r3, #0]
 800187e:	1cfb      	adds	r3, r7, #3
 8001880:	781b      	ldrb	r3, [r3, #0]
 8001882:	2b00      	cmp	r3, #0
 8001884:	d024      	beq.n	80018d0 <disp_send_data+0x68>
 8001886:	4b14      	ldr	r3, [pc, #80]	; (80018d8 <disp_send_data+0x70>)
 8001888:	2120      	movs	r1, #32
 800188a:	0018      	movs	r0, r3
 800188c:	f7ff feeb 	bl	8001666 <LL_GPIO_SetOutputPin>
 8001890:	e015      	b.n	80018be <disp_send_data+0x56>
 8001892:	220f      	movs	r2, #15
 8001894:	18bb      	adds	r3, r7, r2
 8001896:	781b      	ldrb	r3, [r3, #0]
 8001898:	18ba      	adds	r2, r7, r2
 800189a:	1c59      	adds	r1, r3, #1
 800189c:	7011      	strb	r1, [r2, #0]
 800189e:	001a      	movs	r2, r3
 80018a0:	687b      	ldr	r3, [r7, #4]
 80018a2:	189b      	adds	r3, r3, r2
 80018a4:	781b      	ldrb	r3, [r3, #0]
 80018a6:	4a0d      	ldr	r2, [pc, #52]	; (80018dc <disp_send_data+0x74>)
 80018a8:	0019      	movs	r1, r3
 80018aa:	0010      	movs	r0, r2
 80018ac:	f7ff fe6c 	bl	8001588 <LL_SPI_TransmitData8>
 80018b0:	46c0      	nop			; (mov r8, r8)
 80018b2:	4b0a      	ldr	r3, [pc, #40]	; (80018dc <disp_send_data+0x74>)
 80018b4:	0018      	movs	r0, r3
 80018b6:	f7ff fe57 	bl	8001568 <LL_SPI_IsActiveFlag_TXE>
 80018ba:	1e03      	subs	r3, r0, #0
 80018bc:	d0f9      	beq.n	80018b2 <disp_send_data+0x4a>
 80018be:	1cfb      	adds	r3, r7, #3
 80018c0:	781b      	ldrb	r3, [r3, #0]
 80018c2:	1cfa      	adds	r2, r7, #3
 80018c4:	1e59      	subs	r1, r3, #1
 80018c6:	7011      	strb	r1, [r2, #0]
 80018c8:	2b00      	cmp	r3, #0
 80018ca:	d1e2      	bne.n	8001892 <disp_send_data+0x2a>
 80018cc:	46c0      	nop			; (mov r8, r8)
 80018ce:	e000      	b.n	80018d2 <disp_send_data+0x6a>
 80018d0:	46c0      	nop			; (mov r8, r8)
 80018d2:	46bd      	mov	sp, r7
 80018d4:	b004      	add	sp, #16
 80018d6:	bd80      	pop	{r7, pc}
 80018d8:	48000800 	.word	0x48000800
 80018dc:	40013000 	.word	0x40013000

080018e0 <disp_fill>:
 80018e0:	b580      	push	{r7, lr}
 80018e2:	b082      	sub	sp, #8
 80018e4:	af00      	add	r7, sp, #0
 80018e6:	0002      	movs	r2, r0
 80018e8:	1dfb      	adds	r3, r7, #7
 80018ea:	701a      	strb	r2, [r3, #0]
 80018ec:	1dfb      	adds	r3, r7, #7
 80018ee:	7819      	ldrb	r1, [r3, #0]
 80018f0:	2380      	movs	r3, #128	; 0x80
 80018f2:	00da      	lsls	r2, r3, #3
 80018f4:	4b03      	ldr	r3, [pc, #12]	; (8001904 <disp_fill+0x24>)
 80018f6:	0018      	movs	r0, r3
 80018f8:	f007 fe6c 	bl	80095d4 <memset>
 80018fc:	46c0      	nop			; (mov r8, r8)
 80018fe:	46bd      	mov	sp, r7
 8001900:	b002      	add	sp, #8
 8001902:	bd80      	pop	{r7, pc}
 8001904:	20000b40 	.word	0x20000b40

08001908 <disp_update>:
 8001908:	b580      	push	{r7, lr}
 800190a:	b082      	sub	sp, #8
 800190c:	af00      	add	r7, sp, #0
 800190e:	1dfb      	adds	r3, r7, #7
 8001910:	2200      	movs	r2, #0
 8001912:	701a      	strb	r2, [r3, #0]
 8001914:	2020      	movs	r0, #32
 8001916:	f7ff ff85 	bl	8001824 <disp_send_cmd>
 800191a:	2010      	movs	r0, #16
 800191c:	f7ff ff82 	bl	8001824 <disp_send_cmd>
 8001920:	20d3      	movs	r0, #211	; 0xd3
 8001922:	f7ff ff7f 	bl	8001824 <disp_send_cmd>
 8001926:	2000      	movs	r0, #0
 8001928:	f7ff ff7c 	bl	8001824 <disp_send_cmd>
 800192c:	1dfb      	adds	r3, r7, #7
 800192e:	2200      	movs	r2, #0
 8001930:	701a      	strb	r2, [r3, #0]
 8001932:	e01a      	b.n	800196a <disp_update+0x62>
 8001934:	1dfb      	adds	r3, r7, #7
 8001936:	781b      	ldrb	r3, [r3, #0]
 8001938:	3b50      	subs	r3, #80	; 0x50
 800193a:	b2db      	uxtb	r3, r3
 800193c:	0018      	movs	r0, r3
 800193e:	f7ff ff71 	bl	8001824 <disp_send_cmd>
 8001942:	2000      	movs	r0, #0
 8001944:	f7ff ff6e 	bl	8001824 <disp_send_cmd>
 8001948:	2010      	movs	r0, #16
 800194a:	f7ff ff6b 	bl	8001824 <disp_send_cmd>
 800194e:	1dfb      	adds	r3, r7, #7
 8001950:	781b      	ldrb	r3, [r3, #0]
 8001952:	01da      	lsls	r2, r3, #7
 8001954:	4b09      	ldr	r3, [pc, #36]	; (800197c <disp_update+0x74>)
 8001956:	18d3      	adds	r3, r2, r3
 8001958:	2180      	movs	r1, #128	; 0x80
 800195a:	0018      	movs	r0, r3
 800195c:	f7ff ff84 	bl	8001868 <disp_send_data>
 8001960:	1dfb      	adds	r3, r7, #7
 8001962:	781a      	ldrb	r2, [r3, #0]
 8001964:	1dfb      	adds	r3, r7, #7
 8001966:	3201      	adds	r2, #1
 8001968:	701a      	strb	r2, [r3, #0]
 800196a:	1dfb      	adds	r3, r7, #7
 800196c:	781b      	ldrb	r3, [r3, #0]
 800196e:	2b07      	cmp	r3, #7
 8001970:	d9e0      	bls.n	8001934 <disp_update+0x2c>
 8001972:	46c0      	nop			; (mov r8, r8)
 8001974:	46bd      	mov	sp, r7
 8001976:	b002      	add	sp, #8
 8001978:	bd80      	pop	{r7, pc}
 800197a:	46c0      	nop			; (mov r8, r8)
 800197c:	20000b40 	.word	0x20000b40

08001980 <disp_draw_pix>:
 8001980:	b590      	push	{r4, r7, lr}
 8001982:	b083      	sub	sp, #12
 8001984:	af00      	add	r7, sp, #0
 8001986:	0004      	movs	r4, r0
 8001988:	0008      	movs	r0, r1
 800198a:	0011      	movs	r1, r2
 800198c:	1dfb      	adds	r3, r7, #7
 800198e:	1c22      	adds	r2, r4, #0
 8001990:	701a      	strb	r2, [r3, #0]
 8001992:	1dbb      	adds	r3, r7, #6
 8001994:	1c02      	adds	r2, r0, #0
 8001996:	701a      	strb	r2, [r3, #0]
 8001998:	1d7b      	adds	r3, r7, #5
 800199a:	1c0a      	adds	r2, r1, #0
 800199c:	701a      	strb	r2, [r3, #0]
 800199e:	1dfb      	adds	r3, r7, #7
 80019a0:	781b      	ldrb	r3, [r3, #0]
 80019a2:	2b80      	cmp	r3, #128	; 0x80
 80019a4:	d848      	bhi.n	8001a38 <disp_draw_pix+0xb8>
 80019a6:	1dbb      	adds	r3, r7, #6
 80019a8:	781b      	ldrb	r3, [r3, #0]
 80019aa:	2b40      	cmp	r3, #64	; 0x40
 80019ac:	d844      	bhi.n	8001a38 <disp_draw_pix+0xb8>
 80019ae:	1d7b      	adds	r3, r7, #5
 80019b0:	781b      	ldrb	r3, [r3, #0]
 80019b2:	2b01      	cmp	r3, #1
 80019b4:	d11e      	bne.n	80019f4 <disp_draw_pix+0x74>
 80019b6:	1dfb      	adds	r3, r7, #7
 80019b8:	781a      	ldrb	r2, [r3, #0]
 80019ba:	1dbb      	adds	r3, r7, #6
 80019bc:	781b      	ldrb	r3, [r3, #0]
 80019be:	08db      	lsrs	r3, r3, #3
 80019c0:	b2d8      	uxtb	r0, r3
 80019c2:	0003      	movs	r3, r0
 80019c4:	01db      	lsls	r3, r3, #7
 80019c6:	18d3      	adds	r3, r2, r3
 80019c8:	4a1d      	ldr	r2, [pc, #116]	; (8001a40 <disp_draw_pix+0xc0>)
 80019ca:	5cd3      	ldrb	r3, [r2, r3]
 80019cc:	b25a      	sxtb	r2, r3
 80019ce:	1dbb      	adds	r3, r7, #6
 80019d0:	781b      	ldrb	r3, [r3, #0]
 80019d2:	2107      	movs	r1, #7
 80019d4:	400b      	ands	r3, r1
 80019d6:	2101      	movs	r1, #1
 80019d8:	4099      	lsls	r1, r3
 80019da:	000b      	movs	r3, r1
 80019dc:	b25b      	sxtb	r3, r3
 80019de:	4313      	orrs	r3, r2
 80019e0:	b259      	sxtb	r1, r3
 80019e2:	1dfb      	adds	r3, r7, #7
 80019e4:	781a      	ldrb	r2, [r3, #0]
 80019e6:	0003      	movs	r3, r0
 80019e8:	01db      	lsls	r3, r3, #7
 80019ea:	18d3      	adds	r3, r2, r3
 80019ec:	b2c9      	uxtb	r1, r1
 80019ee:	4a14      	ldr	r2, [pc, #80]	; (8001a40 <disp_draw_pix+0xc0>)
 80019f0:	54d1      	strb	r1, [r2, r3]
 80019f2:	e022      	b.n	8001a3a <disp_draw_pix+0xba>
 80019f4:	1dfb      	adds	r3, r7, #7
 80019f6:	781a      	ldrb	r2, [r3, #0]
 80019f8:	1dbb      	adds	r3, r7, #6
 80019fa:	781b      	ldrb	r3, [r3, #0]
 80019fc:	08db      	lsrs	r3, r3, #3
 80019fe:	b2d8      	uxtb	r0, r3
 8001a00:	0003      	movs	r3, r0
 8001a02:	01db      	lsls	r3, r3, #7
 8001a04:	18d3      	adds	r3, r2, r3
 8001a06:	4a0e      	ldr	r2, [pc, #56]	; (8001a40 <disp_draw_pix+0xc0>)
 8001a08:	5cd3      	ldrb	r3, [r2, r3]
 8001a0a:	b25b      	sxtb	r3, r3
 8001a0c:	1dba      	adds	r2, r7, #6
 8001a0e:	7812      	ldrb	r2, [r2, #0]
 8001a10:	2107      	movs	r1, #7
 8001a12:	400a      	ands	r2, r1
 8001a14:	2101      	movs	r1, #1
 8001a16:	4091      	lsls	r1, r2
 8001a18:	000a      	movs	r2, r1
 8001a1a:	b252      	sxtb	r2, r2
 8001a1c:	43d2      	mvns	r2, r2
 8001a1e:	b252      	sxtb	r2, r2
 8001a20:	4013      	ands	r3, r2
 8001a22:	b259      	sxtb	r1, r3
 8001a24:	1dfb      	adds	r3, r7, #7
 8001a26:	781a      	ldrb	r2, [r3, #0]
 8001a28:	0003      	movs	r3, r0
 8001a2a:	01db      	lsls	r3, r3, #7
 8001a2c:	18d3      	adds	r3, r2, r3
 8001a2e:	b2c9      	uxtb	r1, r1
 8001a30:	4a03      	ldr	r2, [pc, #12]	; (8001a40 <disp_draw_pix+0xc0>)
 8001a32:	54d1      	strb	r1, [r2, r3]
 8001a34:	46c0      	nop			; (mov r8, r8)
 8001a36:	e000      	b.n	8001a3a <disp_draw_pix+0xba>
 8001a38:	46c0      	nop			; (mov r8, r8)
 8001a3a:	46bd      	mov	sp, r7
 8001a3c:	b003      	add	sp, #12
 8001a3e:	bd90      	pop	{r4, r7, pc}
 8001a40:	20000b40 	.word	0x20000b40

08001a44 <disp_write_char>:
 8001a44:	b580      	push	{r7, lr}
 8001a46:	b086      	sub	sp, #24
 8001a48:	af00      	add	r7, sp, #0
 8001a4a:	0002      	movs	r2, r0
 8001a4c:	1dfb      	adds	r3, r7, #7
 8001a4e:	701a      	strb	r2, [r3, #0]
 8001a50:	4b2f      	ldr	r3, [pc, #188]	; (8001b10 <disp_write_char+0xcc>)
 8001a52:	781b      	ldrb	r3, [r3, #0]
 8001a54:	2b78      	cmp	r3, #120	; 0x78
 8001a56:	d803      	bhi.n	8001a60 <disp_write_char+0x1c>
 8001a58:	4b2d      	ldr	r3, [pc, #180]	; (8001b10 <disp_write_char+0xcc>)
 8001a5a:	785b      	ldrb	r3, [r3, #1]
 8001a5c:	2b35      	cmp	r3, #53	; 0x35
 8001a5e:	d902      	bls.n	8001a66 <disp_write_char+0x22>
 8001a60:	2301      	movs	r3, #1
 8001a62:	425b      	negs	r3, r3
 8001a64:	e04f      	b.n	8001b06 <disp_write_char+0xc2>
 8001a66:	2300      	movs	r3, #0
 8001a68:	617b      	str	r3, [r7, #20]
 8001a6a:	e042      	b.n	8001af2 <disp_write_char+0xae>
 8001a6c:	1dfb      	adds	r3, r7, #7
 8001a6e:	781b      	ldrb	r3, [r3, #0]
 8001a70:	3b20      	subs	r3, #32
 8001a72:	001a      	movs	r2, r3
 8001a74:	0013      	movs	r3, r2
 8001a76:	009b      	lsls	r3, r3, #2
 8001a78:	189b      	adds	r3, r3, r2
 8001a7a:	005b      	lsls	r3, r3, #1
 8001a7c:	001a      	movs	r2, r3
 8001a7e:	697b      	ldr	r3, [r7, #20]
 8001a80:	18d2      	adds	r2, r2, r3
 8001a82:	4b24      	ldr	r3, [pc, #144]	; (8001b14 <disp_write_char+0xd0>)
 8001a84:	0052      	lsls	r2, r2, #1
 8001a86:	5ad3      	ldrh	r3, [r2, r3]
 8001a88:	60fb      	str	r3, [r7, #12]
 8001a8a:	2300      	movs	r3, #0
 8001a8c:	613b      	str	r3, [r7, #16]
 8001a8e:	e02a      	b.n	8001ae6 <disp_write_char+0xa2>
 8001a90:	68fa      	ldr	r2, [r7, #12]
 8001a92:	693b      	ldr	r3, [r7, #16]
 8001a94:	409a      	lsls	r2, r3
 8001a96:	2380      	movs	r3, #128	; 0x80
 8001a98:	021b      	lsls	r3, r3, #8
 8001a9a:	4013      	ands	r3, r2
 8001a9c:	d010      	beq.n	8001ac0 <disp_write_char+0x7c>
 8001a9e:	4b1c      	ldr	r3, [pc, #112]	; (8001b10 <disp_write_char+0xcc>)
 8001aa0:	781a      	ldrb	r2, [r3, #0]
 8001aa2:	693b      	ldr	r3, [r7, #16]
 8001aa4:	b2db      	uxtb	r3, r3
 8001aa6:	18d3      	adds	r3, r2, r3
 8001aa8:	b2d8      	uxtb	r0, r3
 8001aaa:	4b19      	ldr	r3, [pc, #100]	; (8001b10 <disp_write_char+0xcc>)
 8001aac:	785a      	ldrb	r2, [r3, #1]
 8001aae:	697b      	ldr	r3, [r7, #20]
 8001ab0:	b2db      	uxtb	r3, r3
 8001ab2:	18d3      	adds	r3, r2, r3
 8001ab4:	b2db      	uxtb	r3, r3
 8001ab6:	2201      	movs	r2, #1
 8001ab8:	0019      	movs	r1, r3
 8001aba:	f7ff ff61 	bl	8001980 <disp_draw_pix>
 8001abe:	e00f      	b.n	8001ae0 <disp_write_char+0x9c>
 8001ac0:	4b13      	ldr	r3, [pc, #76]	; (8001b10 <disp_write_char+0xcc>)
 8001ac2:	781a      	ldrb	r2, [r3, #0]
 8001ac4:	693b      	ldr	r3, [r7, #16]
 8001ac6:	b2db      	uxtb	r3, r3
 8001ac8:	18d3      	adds	r3, r2, r3
 8001aca:	b2d8      	uxtb	r0, r3
 8001acc:	4b10      	ldr	r3, [pc, #64]	; (8001b10 <disp_write_char+0xcc>)
 8001ace:	785a      	ldrb	r2, [r3, #1]
 8001ad0:	697b      	ldr	r3, [r7, #20]
 8001ad2:	b2db      	uxtb	r3, r3
 8001ad4:	18d3      	adds	r3, r2, r3
 8001ad6:	b2db      	uxtb	r3, r3
 8001ad8:	2200      	movs	r2, #0
 8001ada:	0019      	movs	r1, r3
 8001adc:	f7ff ff50 	bl	8001980 <disp_draw_pix>
 8001ae0:	693b      	ldr	r3, [r7, #16]
 8001ae2:	3301      	adds	r3, #1
 8001ae4:	613b      	str	r3, [r7, #16]
 8001ae6:	693b      	ldr	r3, [r7, #16]
 8001ae8:	2b06      	cmp	r3, #6
 8001aea:	d9d1      	bls.n	8001a90 <disp_write_char+0x4c>
 8001aec:	697b      	ldr	r3, [r7, #20]
 8001aee:	3301      	adds	r3, #1
 8001af0:	617b      	str	r3, [r7, #20]
 8001af2:	697b      	ldr	r3, [r7, #20]
 8001af4:	2b09      	cmp	r3, #9
 8001af6:	d9b9      	bls.n	8001a6c <disp_write_char+0x28>
 8001af8:	4b05      	ldr	r3, [pc, #20]	; (8001b10 <disp_write_char+0xcc>)
 8001afa:	781b      	ldrb	r3, [r3, #0]
 8001afc:	3307      	adds	r3, #7
 8001afe:	b2da      	uxtb	r2, r3
 8001b00:	4b03      	ldr	r3, [pc, #12]	; (8001b10 <disp_write_char+0xcc>)
 8001b02:	701a      	strb	r2, [r3, #0]
 8001b04:	2300      	movs	r3, #0
 8001b06:	0018      	movs	r0, r3
 8001b08:	46bd      	mov	sp, r7
 8001b0a:	b006      	add	sp, #24
 8001b0c:	bd80      	pop	{r7, pc}
 8001b0e:	46c0      	nop			; (mov r8, r8)
 8001b10:	20000f40 	.word	0x20000f40
 8001b14:	080097cc 	.word	0x080097cc

08001b18 <disp_set_cursor>:
 8001b18:	b580      	push	{r7, lr}
 8001b1a:	b082      	sub	sp, #8
 8001b1c:	af00      	add	r7, sp, #0
 8001b1e:	0002      	movs	r2, r0
 8001b20:	1dfb      	adds	r3, r7, #7
 8001b22:	701a      	strb	r2, [r3, #0]
 8001b24:	1dbb      	adds	r3, r7, #6
 8001b26:	1c0a      	adds	r2, r1, #0
 8001b28:	701a      	strb	r2, [r3, #0]
 8001b2a:	1dfb      	adds	r3, r7, #7
 8001b2c:	781b      	ldrb	r3, [r3, #0]
 8001b2e:	2b12      	cmp	r3, #18
 8001b30:	d815      	bhi.n	8001b5e <disp_set_cursor+0x46>
 8001b32:	1dbb      	adds	r3, r7, #6
 8001b34:	781b      	ldrb	r3, [r3, #0]
 8001b36:	2b0c      	cmp	r3, #12
 8001b38:	d811      	bhi.n	8001b5e <disp_set_cursor+0x46>
 8001b3a:	1dfb      	adds	r3, r7, #7
 8001b3c:	781b      	ldrb	r3, [r3, #0]
 8001b3e:	1c1a      	adds	r2, r3, #0
 8001b40:	00d2      	lsls	r2, r2, #3
 8001b42:	1ad3      	subs	r3, r2, r3
 8001b44:	b2da      	uxtb	r2, r3
 8001b46:	4b08      	ldr	r3, [pc, #32]	; (8001b68 <disp_set_cursor+0x50>)
 8001b48:	701a      	strb	r2, [r3, #0]
 8001b4a:	1dbb      	adds	r3, r7, #6
 8001b4c:	781b      	ldrb	r3, [r3, #0]
 8001b4e:	1c1a      	adds	r2, r3, #0
 8001b50:	0092      	lsls	r2, r2, #2
 8001b52:	18d3      	adds	r3, r2, r3
 8001b54:	18db      	adds	r3, r3, r3
 8001b56:	b2da      	uxtb	r2, r3
 8001b58:	4b03      	ldr	r3, [pc, #12]	; (8001b68 <disp_set_cursor+0x50>)
 8001b5a:	705a      	strb	r2, [r3, #1]
 8001b5c:	e000      	b.n	8001b60 <disp_set_cursor+0x48>
 8001b5e:	46c0      	nop			; (mov r8, r8)
 8001b60:	46bd      	mov	sp, r7
 8001b62:	b002      	add	sp, #8
 8001b64:	bd80      	pop	{r7, pc}
 8001b66:	46c0      	nop			; (mov r8, r8)
 8001b68:	20000f40 	.word	0x20000f40

08001b6c <disp_init>:
 8001b6c:	b580      	push	{r7, lr}
 8001b6e:	af00      	add	r7, sp, #0
 8001b70:	f7ff fdbe 	bl	80016f0 <disp_hw_config>
 8001b74:	f7ff fe30 	bl	80017d8 <disp_reset>
 8001b78:	20ae      	movs	r0, #174	; 0xae
 8001b7a:	f7ff fe53 	bl	8001824 <disp_send_cmd>
 8001b7e:	2020      	movs	r0, #32
 8001b80:	f7ff fe50 	bl	8001824 <disp_send_cmd>
 8001b84:	2010      	movs	r0, #16
 8001b86:	f7ff fe4d 	bl	8001824 <disp_send_cmd>
 8001b8a:	20c8      	movs	r0, #200	; 0xc8
 8001b8c:	f7ff fe4a 	bl	8001824 <disp_send_cmd>
 8001b90:	2040      	movs	r0, #64	; 0x40
 8001b92:	f7ff fe47 	bl	8001824 <disp_send_cmd>
 8001b96:	2081      	movs	r0, #129	; 0x81
 8001b98:	f7ff fe44 	bl	8001824 <disp_send_cmd>
 8001b9c:	20ff      	movs	r0, #255	; 0xff
 8001b9e:	f7ff fe41 	bl	8001824 <disp_send_cmd>
 8001ba2:	20a1      	movs	r0, #161	; 0xa1
 8001ba4:	f7ff fe3e 	bl	8001824 <disp_send_cmd>
 8001ba8:	20a6      	movs	r0, #166	; 0xa6
 8001baa:	f7ff fe3b 	bl	8001824 <disp_send_cmd>
 8001bae:	20a8      	movs	r0, #168	; 0xa8
 8001bb0:	f7ff fe38 	bl	8001824 <disp_send_cmd>
 8001bb4:	203f      	movs	r0, #63	; 0x3f
 8001bb6:	f7ff fe35 	bl	8001824 <disp_send_cmd>
 8001bba:	20a4      	movs	r0, #164	; 0xa4
 8001bbc:	f7ff fe32 	bl	8001824 <disp_send_cmd>
 8001bc0:	20d3      	movs	r0, #211	; 0xd3
 8001bc2:	f7ff fe2f 	bl	8001824 <disp_send_cmd>
 8001bc6:	2000      	movs	r0, #0
 8001bc8:	f7ff fe2c 	bl	8001824 <disp_send_cmd>
 8001bcc:	20d5      	movs	r0, #213	; 0xd5
 8001bce:	f7ff fe29 	bl	8001824 <disp_send_cmd>
 8001bd2:	20f0      	movs	r0, #240	; 0xf0
 8001bd4:	f7ff fe26 	bl	8001824 <disp_send_cmd>
 8001bd8:	20da      	movs	r0, #218	; 0xda
 8001bda:	f7ff fe23 	bl	8001824 <disp_send_cmd>
 8001bde:	2012      	movs	r0, #18
 8001be0:	f7ff fe20 	bl	8001824 <disp_send_cmd>
 8001be4:	208d      	movs	r0, #141	; 0x8d
 8001be6:	f7ff fe1d 	bl	8001824 <disp_send_cmd>
 8001bea:	2014      	movs	r0, #20
 8001bec:	f7ff fe1a 	bl	8001824 <disp_send_cmd>
 8001bf0:	20af      	movs	r0, #175	; 0xaf
 8001bf2:	f7ff fe17 	bl	8001824 <disp_send_cmd>
 8001bf6:	2000      	movs	r0, #0
 8001bf8:	f7ff fe72 	bl	80018e0 <disp_fill>
 8001bfc:	f7ff fe84 	bl	8001908 <disp_update>
 8001c00:	4b05      	ldr	r3, [pc, #20]	; (8001c18 <disp_init+0xac>)
 8001c02:	2200      	movs	r2, #0
 8001c04:	701a      	strb	r2, [r3, #0]
 8001c06:	4b04      	ldr	r3, [pc, #16]	; (8001c18 <disp_init+0xac>)
 8001c08:	2200      	movs	r2, #0
 8001c0a:	705a      	strb	r2, [r3, #1]
 8001c0c:	4b03      	ldr	r3, [pc, #12]	; (8001c1c <disp_init+0xb0>)
 8001c0e:	4a04      	ldr	r2, [pc, #16]	; (8001c20 <disp_init+0xb4>)
 8001c10:	601a      	str	r2, [r3, #0]
 8001c12:	46c0      	nop			; (mov r8, r8)
 8001c14:	46bd      	mov	sp, r7
 8001c16:	bd80      	pop	{r7, pc}
 8001c18:	20000f40 	.word	0x20000f40
 8001c1c:	2000115c 	.word	0x2000115c
 8001c20:	08001a45 	.word	0x08001a45

08001c24 <LL_GPIO_SetPinMode>:
 8001c24:	b580      	push	{r7, lr}
 8001c26:	b084      	sub	sp, #16
 8001c28:	af00      	add	r7, sp, #0
 8001c2a:	60f8      	str	r0, [r7, #12]
 8001c2c:	60b9      	str	r1, [r7, #8]
 8001c2e:	607a      	str	r2, [r7, #4]
 8001c30:	68fb      	ldr	r3, [r7, #12]
 8001c32:	6819      	ldr	r1, [r3, #0]
 8001c34:	68bb      	ldr	r3, [r7, #8]
 8001c36:	68ba      	ldr	r2, [r7, #8]
 8001c38:	435a      	muls	r2, r3
 8001c3a:	0013      	movs	r3, r2
 8001c3c:	005b      	lsls	r3, r3, #1
 8001c3e:	189b      	adds	r3, r3, r2
 8001c40:	43db      	mvns	r3, r3
 8001c42:	400b      	ands	r3, r1
 8001c44:	001a      	movs	r2, r3
 8001c46:	68bb      	ldr	r3, [r7, #8]
 8001c48:	68b9      	ldr	r1, [r7, #8]
 8001c4a:	434b      	muls	r3, r1
 8001c4c:	6879      	ldr	r1, [r7, #4]
 8001c4e:	434b      	muls	r3, r1
 8001c50:	431a      	orrs	r2, r3
 8001c52:	68fb      	ldr	r3, [r7, #12]
 8001c54:	601a      	str	r2, [r3, #0]
 8001c56:	46c0      	nop			; (mov r8, r8)
 8001c58:	46bd      	mov	sp, r7
 8001c5a:	b004      	add	sp, #16
 8001c5c:	bd80      	pop	{r7, pc}

08001c5e <LL_GPIO_SetPinOutputType>:
 8001c5e:	b580      	push	{r7, lr}
 8001c60:	b084      	sub	sp, #16
 8001c62:	af00      	add	r7, sp, #0
 8001c64:	60f8      	str	r0, [r7, #12]
 8001c66:	60b9      	str	r1, [r7, #8]
 8001c68:	607a      	str	r2, [r7, #4]
 8001c6a:	68fb      	ldr	r3, [r7, #12]
 8001c6c:	685b      	ldr	r3, [r3, #4]
 8001c6e:	68ba      	ldr	r2, [r7, #8]
 8001c70:	43d2      	mvns	r2, r2
 8001c72:	401a      	ands	r2, r3
 8001c74:	68bb      	ldr	r3, [r7, #8]
 8001c76:	6879      	ldr	r1, [r7, #4]
 8001c78:	434b      	muls	r3, r1
 8001c7a:	431a      	orrs	r2, r3
 8001c7c:	68fb      	ldr	r3, [r7, #12]
 8001c7e:	605a      	str	r2, [r3, #4]
 8001c80:	46c0      	nop			; (mov r8, r8)
 8001c82:	46bd      	mov	sp, r7
 8001c84:	b004      	add	sp, #16
 8001c86:	bd80      	pop	{r7, pc}

08001c88 <LL_GPIO_SetPinSpeed>:
 8001c88:	b580      	push	{r7, lr}
 8001c8a:	b084      	sub	sp, #16
 8001c8c:	af00      	add	r7, sp, #0
 8001c8e:	60f8      	str	r0, [r7, #12]
 8001c90:	60b9      	str	r1, [r7, #8]
 8001c92:	607a      	str	r2, [r7, #4]
 8001c94:	68fb      	ldr	r3, [r7, #12]
 8001c96:	6899      	ldr	r1, [r3, #8]
 8001c98:	68bb      	ldr	r3, [r7, #8]
 8001c9a:	68ba      	ldr	r2, [r7, #8]
 8001c9c:	435a      	muls	r2, r3
 8001c9e:	0013      	movs	r3, r2
 8001ca0:	005b      	lsls	r3, r3, #1
 8001ca2:	189b      	adds	r3, r3, r2
 8001ca4:	43db      	mvns	r3, r3
 8001ca6:	400b      	ands	r3, r1
 8001ca8:	001a      	movs	r2, r3
 8001caa:	68bb      	ldr	r3, [r7, #8]
 8001cac:	68b9      	ldr	r1, [r7, #8]
 8001cae:	434b      	muls	r3, r1
 8001cb0:	6879      	ldr	r1, [r7, #4]
 8001cb2:	434b      	muls	r3, r1
 8001cb4:	431a      	orrs	r2, r3
 8001cb6:	68fb      	ldr	r3, [r7, #12]
 8001cb8:	609a      	str	r2, [r3, #8]
 8001cba:	46c0      	nop			; (mov r8, r8)
 8001cbc:	46bd      	mov	sp, r7
 8001cbe:	b004      	add	sp, #16
 8001cc0:	bd80      	pop	{r7, pc}

08001cc2 <LL_GPIO_SetPinPull>:
 8001cc2:	b580      	push	{r7, lr}
 8001cc4:	b084      	sub	sp, #16
 8001cc6:	af00      	add	r7, sp, #0
 8001cc8:	60f8      	str	r0, [r7, #12]
 8001cca:	60b9      	str	r1, [r7, #8]
 8001ccc:	607a      	str	r2, [r7, #4]
 8001cce:	68fb      	ldr	r3, [r7, #12]
 8001cd0:	68d9      	ldr	r1, [r3, #12]
 8001cd2:	68bb      	ldr	r3, [r7, #8]
 8001cd4:	68ba      	ldr	r2, [r7, #8]
 8001cd6:	435a      	muls	r2, r3
 8001cd8:	0013      	movs	r3, r2
 8001cda:	005b      	lsls	r3, r3, #1
 8001cdc:	189b      	adds	r3, r3, r2
 8001cde:	43db      	mvns	r3, r3
 8001ce0:	400b      	ands	r3, r1
 8001ce2:	001a      	movs	r2, r3
 8001ce4:	68bb      	ldr	r3, [r7, #8]
 8001ce6:	68b9      	ldr	r1, [r7, #8]
 8001ce8:	434b      	muls	r3, r1
 8001cea:	6879      	ldr	r1, [r7, #4]
 8001cec:	434b      	muls	r3, r1
 8001cee:	431a      	orrs	r2, r3
 8001cf0:	68fb      	ldr	r3, [r7, #12]
 8001cf2:	60da      	str	r2, [r3, #12]
 8001cf4:	46c0      	nop			; (mov r8, r8)
 8001cf6:	46bd      	mov	sp, r7
 8001cf8:	b004      	add	sp, #16
 8001cfa:	bd80      	pop	{r7, pc}

08001cfc <LL_GPIO_SetAFPin_8_15>:
 8001cfc:	b580      	push	{r7, lr}
 8001cfe:	b084      	sub	sp, #16
 8001d00:	af00      	add	r7, sp, #0
 8001d02:	60f8      	str	r0, [r7, #12]
 8001d04:	60b9      	str	r1, [r7, #8]
 8001d06:	607a      	str	r2, [r7, #4]
 8001d08:	68fb      	ldr	r3, [r7, #12]
 8001d0a:	6a59      	ldr	r1, [r3, #36]	; 0x24
 8001d0c:	68bb      	ldr	r3, [r7, #8]
 8001d0e:	0a1b      	lsrs	r3, r3, #8
 8001d10:	68ba      	ldr	r2, [r7, #8]
 8001d12:	0a12      	lsrs	r2, r2, #8
 8001d14:	4353      	muls	r3, r2
 8001d16:	68ba      	ldr	r2, [r7, #8]
 8001d18:	0a12      	lsrs	r2, r2, #8
 8001d1a:	4353      	muls	r3, r2
 8001d1c:	68ba      	ldr	r2, [r7, #8]
 8001d1e:	0a12      	lsrs	r2, r2, #8
 8001d20:	435a      	muls	r2, r3
 8001d22:	0013      	movs	r3, r2
 8001d24:	011b      	lsls	r3, r3, #4
 8001d26:	1a9b      	subs	r3, r3, r2
 8001d28:	43db      	mvns	r3, r3
 8001d2a:	400b      	ands	r3, r1
 8001d2c:	001a      	movs	r2, r3
 8001d2e:	68bb      	ldr	r3, [r7, #8]
 8001d30:	0a1b      	lsrs	r3, r3, #8
 8001d32:	68b9      	ldr	r1, [r7, #8]
 8001d34:	0a09      	lsrs	r1, r1, #8
 8001d36:	434b      	muls	r3, r1
 8001d38:	68b9      	ldr	r1, [r7, #8]
 8001d3a:	0a09      	lsrs	r1, r1, #8
 8001d3c:	434b      	muls	r3, r1
 8001d3e:	68b9      	ldr	r1, [r7, #8]
 8001d40:	0a09      	lsrs	r1, r1, #8
 8001d42:	434b      	muls	r3, r1
 8001d44:	6879      	ldr	r1, [r7, #4]
 8001d46:	434b      	muls	r3, r1
 8001d48:	431a      	orrs	r2, r3
 8001d4a:	68fb      	ldr	r3, [r7, #12]
 8001d4c:	625a      	str	r2, [r3, #36]	; 0x24
 8001d4e:	46c0      	nop			; (mov r8, r8)
 8001d50:	46bd      	mov	sp, r7
 8001d52:	b004      	add	sp, #16
 8001d54:	bd80      	pop	{r7, pc}

08001d56 <LL_GPIO_SetOutputPin>:
 8001d56:	b580      	push	{r7, lr}
 8001d58:	b082      	sub	sp, #8
 8001d5a:	af00      	add	r7, sp, #0
 8001d5c:	6078      	str	r0, [r7, #4]
 8001d5e:	6039      	str	r1, [r7, #0]
 8001d60:	687b      	ldr	r3, [r7, #4]
 8001d62:	683a      	ldr	r2, [r7, #0]
 8001d64:	619a      	str	r2, [r3, #24]
 8001d66:	46c0      	nop			; (mov r8, r8)
 8001d68:	46bd      	mov	sp, r7
 8001d6a:	b002      	add	sp, #8
 8001d6c:	bd80      	pop	{r7, pc}

08001d6e <LL_USART_Enable>:
 8001d6e:	b580      	push	{r7, lr}
 8001d70:	b082      	sub	sp, #8
 8001d72:	af00      	add	r7, sp, #0
 8001d74:	6078      	str	r0, [r7, #4]
 8001d76:	687b      	ldr	r3, [r7, #4]
 8001d78:	681b      	ldr	r3, [r3, #0]
 8001d7a:	2201      	movs	r2, #1
 8001d7c:	431a      	orrs	r2, r3
 8001d7e:	687b      	ldr	r3, [r7, #4]
 8001d80:	601a      	str	r2, [r3, #0]
 8001d82:	46c0      	nop			; (mov r8, r8)
 8001d84:	46bd      	mov	sp, r7
 8001d86:	b002      	add	sp, #8
 8001d88:	bd80      	pop	{r7, pc}

08001d8a <LL_USART_EnableDirectionRx>:
 8001d8a:	b580      	push	{r7, lr}
 8001d8c:	b082      	sub	sp, #8
 8001d8e:	af00      	add	r7, sp, #0
 8001d90:	6078      	str	r0, [r7, #4]
 8001d92:	687b      	ldr	r3, [r7, #4]
 8001d94:	681b      	ldr	r3, [r3, #0]
 8001d96:	2204      	movs	r2, #4
 8001d98:	431a      	orrs	r2, r3
 8001d9a:	687b      	ldr	r3, [r7, #4]
 8001d9c:	601a      	str	r2, [r3, #0]
 8001d9e:	46c0      	nop			; (mov r8, r8)
 8001da0:	46bd      	mov	sp, r7
 8001da2:	b002      	add	sp, #8
 8001da4:	bd80      	pop	{r7, pc}

08001da6 <LL_USART_DisableDirectionRx>:
 8001da6:	b580      	push	{r7, lr}
 8001da8:	b082      	sub	sp, #8
 8001daa:	af00      	add	r7, sp, #0
 8001dac:	6078      	str	r0, [r7, #4]
 8001dae:	687b      	ldr	r3, [r7, #4]
 8001db0:	681b      	ldr	r3, [r3, #0]
 8001db2:	2204      	movs	r2, #4
 8001db4:	4393      	bics	r3, r2
 8001db6:	001a      	movs	r2, r3
 8001db8:	687b      	ldr	r3, [r7, #4]
 8001dba:	601a      	str	r2, [r3, #0]
 8001dbc:	46c0      	nop			; (mov r8, r8)
 8001dbe:	46bd      	mov	sp, r7
 8001dc0:	b002      	add	sp, #8
 8001dc2:	bd80      	pop	{r7, pc}

08001dc4 <LL_USART_EnableDirectionTx>:
 8001dc4:	b580      	push	{r7, lr}
 8001dc6:	b082      	sub	sp, #8
 8001dc8:	af00      	add	r7, sp, #0
 8001dca:	6078      	str	r0, [r7, #4]
 8001dcc:	687b      	ldr	r3, [r7, #4]
 8001dce:	681b      	ldr	r3, [r3, #0]
 8001dd0:	2208      	movs	r2, #8
 8001dd2:	431a      	orrs	r2, r3
 8001dd4:	687b      	ldr	r3, [r7, #4]
 8001dd6:	601a      	str	r2, [r3, #0]
 8001dd8:	46c0      	nop			; (mov r8, r8)
 8001dda:	46bd      	mov	sp, r7
 8001ddc:	b002      	add	sp, #8
 8001dde:	bd80      	pop	{r7, pc}

08001de0 <LL_USART_DisableDirectionTx>:
 8001de0:	b580      	push	{r7, lr}
 8001de2:	b082      	sub	sp, #8
 8001de4:	af00      	add	r7, sp, #0
 8001de6:	6078      	str	r0, [r7, #4]
 8001de8:	687b      	ldr	r3, [r7, #4]
 8001dea:	681b      	ldr	r3, [r3, #0]
 8001dec:	2208      	movs	r2, #8
 8001dee:	4393      	bics	r3, r2
 8001df0:	001a      	movs	r2, r3
 8001df2:	687b      	ldr	r3, [r7, #4]
 8001df4:	601a      	str	r2, [r3, #0]
 8001df6:	46c0      	nop			; (mov r8, r8)
 8001df8:	46bd      	mov	sp, r7
 8001dfa:	b002      	add	sp, #8
 8001dfc:	bd80      	pop	{r7, pc}
	...

08001e00 <LL_USART_SetParity>:
 8001e00:	b580      	push	{r7, lr}
 8001e02:	b082      	sub	sp, #8
 8001e04:	af00      	add	r7, sp, #0
 8001e06:	6078      	str	r0, [r7, #4]
 8001e08:	6039      	str	r1, [r7, #0]
 8001e0a:	687b      	ldr	r3, [r7, #4]
 8001e0c:	681b      	ldr	r3, [r3, #0]
 8001e0e:	4a05      	ldr	r2, [pc, #20]	; (8001e24 <LL_USART_SetParity+0x24>)
 8001e10:	401a      	ands	r2, r3
 8001e12:	683b      	ldr	r3, [r7, #0]
 8001e14:	431a      	orrs	r2, r3
 8001e16:	687b      	ldr	r3, [r7, #4]
 8001e18:	601a      	str	r2, [r3, #0]
 8001e1a:	46c0      	nop			; (mov r8, r8)
 8001e1c:	46bd      	mov	sp, r7
 8001e1e:	b002      	add	sp, #8
 8001e20:	bd80      	pop	{r7, pc}
 8001e22:	46c0      	nop			; (mov r8, r8)
 8001e24:	fffff9ff 	.word	0xfffff9ff

08001e28 <LL_USART_SetDataWidth>:
 8001e28:	b580      	push	{r7, lr}
 8001e2a:	b082      	sub	sp, #8
 8001e2c:	af00      	add	r7, sp, #0
 8001e2e:	6078      	str	r0, [r7, #4]
 8001e30:	6039      	str	r1, [r7, #0]
 8001e32:	687b      	ldr	r3, [r7, #4]
 8001e34:	681b      	ldr	r3, [r3, #0]
 8001e36:	4a05      	ldr	r2, [pc, #20]	; (8001e4c <LL_USART_SetDataWidth+0x24>)
 8001e38:	401a      	ands	r2, r3
 8001e3a:	683b      	ldr	r3, [r7, #0]
 8001e3c:	431a      	orrs	r2, r3
 8001e3e:	687b      	ldr	r3, [r7, #4]
 8001e40:	601a      	str	r2, [r3, #0]
 8001e42:	46c0      	nop			; (mov r8, r8)
 8001e44:	46bd      	mov	sp, r7
 8001e46:	b002      	add	sp, #8
 8001e48:	bd80      	pop	{r7, pc}
 8001e4a:	46c0      	nop			; (mov r8, r8)
 8001e4c:	ffffefff 	.word	0xffffefff

08001e50 <LL_USART_SetStopBitsLength>:
 8001e50:	b580      	push	{r7, lr}
 8001e52:	b082      	sub	sp, #8
 8001e54:	af00      	add	r7, sp, #0
 8001e56:	6078      	str	r0, [r7, #4]
 8001e58:	6039      	str	r1, [r7, #0]
 8001e5a:	687b      	ldr	r3, [r7, #4]
 8001e5c:	685b      	ldr	r3, [r3, #4]
 8001e5e:	4a05      	ldr	r2, [pc, #20]	; (8001e74 <LL_USART_SetStopBitsLength+0x24>)
 8001e60:	401a      	ands	r2, r3
 8001e62:	683b      	ldr	r3, [r7, #0]
 8001e64:	431a      	orrs	r2, r3
 8001e66:	687b      	ldr	r3, [r7, #4]
 8001e68:	605a      	str	r2, [r3, #4]
 8001e6a:	46c0      	nop			; (mov r8, r8)
 8001e6c:	46bd      	mov	sp, r7
 8001e6e:	b002      	add	sp, #8
 8001e70:	bd80      	pop	{r7, pc}
 8001e72:	46c0      	nop			; (mov r8, r8)
 8001e74:	ffffcfff 	.word	0xffffcfff

08001e78 <LL_USART_SetBaudRate>:
 8001e78:	b5b0      	push	{r4, r5, r7, lr}
 8001e7a:	b084      	sub	sp, #16
 8001e7c:	af00      	add	r7, sp, #0
 8001e7e:	60f8      	str	r0, [r7, #12]
 8001e80:	60b9      	str	r1, [r7, #8]
 8001e82:	607a      	str	r2, [r7, #4]
 8001e84:	603b      	str	r3, [r7, #0]
 8001e86:	2500      	movs	r5, #0
 8001e88:	2400      	movs	r4, #0
 8001e8a:	687a      	ldr	r2, [r7, #4]
 8001e8c:	2380      	movs	r3, #128	; 0x80
 8001e8e:	021b      	lsls	r3, r3, #8
 8001e90:	429a      	cmp	r2, r3
 8001e92:	d117      	bne.n	8001ec4 <LL_USART_SetBaudRate+0x4c>
 8001e94:	68bb      	ldr	r3, [r7, #8]
 8001e96:	005a      	lsls	r2, r3, #1
 8001e98:	683b      	ldr	r3, [r7, #0]
 8001e9a:	085b      	lsrs	r3, r3, #1
 8001e9c:	18d3      	adds	r3, r2, r3
 8001e9e:	6839      	ldr	r1, [r7, #0]
 8001ea0:	0018      	movs	r0, r3
 8001ea2:	f7fe f931 	bl	8000108 <__udivsi3>
 8001ea6:	0003      	movs	r3, r0
 8001ea8:	b29b      	uxth	r3, r3
 8001eaa:	001d      	movs	r5, r3
 8001eac:	4b0e      	ldr	r3, [pc, #56]	; (8001ee8 <LL_USART_SetBaudRate+0x70>)
 8001eae:	402b      	ands	r3, r5
 8001eb0:	001c      	movs	r4, r3
 8001eb2:	086b      	lsrs	r3, r5, #1
 8001eb4:	b29b      	uxth	r3, r3
 8001eb6:	001a      	movs	r2, r3
 8001eb8:	2307      	movs	r3, #7
 8001eba:	4013      	ands	r3, r2
 8001ebc:	431c      	orrs	r4, r3
 8001ebe:	68fb      	ldr	r3, [r7, #12]
 8001ec0:	60dc      	str	r4, [r3, #12]
 8001ec2:	e00c      	b.n	8001ede <LL_USART_SetBaudRate+0x66>
 8001ec4:	683b      	ldr	r3, [r7, #0]
 8001ec6:	085a      	lsrs	r2, r3, #1
 8001ec8:	68bb      	ldr	r3, [r7, #8]
 8001eca:	18d3      	adds	r3, r2, r3
 8001ecc:	6839      	ldr	r1, [r7, #0]
 8001ece:	0018      	movs	r0, r3
 8001ed0:	f7fe f91a 	bl	8000108 <__udivsi3>
 8001ed4:	0003      	movs	r3, r0
 8001ed6:	b29b      	uxth	r3, r3
 8001ed8:	001a      	movs	r2, r3
 8001eda:	68fb      	ldr	r3, [r7, #12]
 8001edc:	60da      	str	r2, [r3, #12]
 8001ede:	46c0      	nop			; (mov r8, r8)
 8001ee0:	46bd      	mov	sp, r7
 8001ee2:	b004      	add	sp, #16
 8001ee4:	bdb0      	pop	{r4, r5, r7, pc}
 8001ee6:	46c0      	nop			; (mov r8, r8)
 8001ee8:	0000fff0 	.word	0x0000fff0

08001eec <LL_USART_EnableHalfDuplex>:
 8001eec:	b580      	push	{r7, lr}
 8001eee:	b082      	sub	sp, #8
 8001ef0:	af00      	add	r7, sp, #0
 8001ef2:	6078      	str	r0, [r7, #4]
 8001ef4:	687b      	ldr	r3, [r7, #4]
 8001ef6:	689b      	ldr	r3, [r3, #8]
 8001ef8:	2208      	movs	r2, #8
 8001efa:	431a      	orrs	r2, r3
 8001efc:	687b      	ldr	r3, [r7, #4]
 8001efe:	609a      	str	r2, [r3, #8]
 8001f00:	46c0      	nop			; (mov r8, r8)
 8001f02:	46bd      	mov	sp, r7
 8001f04:	b002      	add	sp, #8
 8001f06:	bd80      	pop	{r7, pc}

08001f08 <LL_USART_IsActiveFlag_TC>:
 8001f08:	b580      	push	{r7, lr}
 8001f0a:	b082      	sub	sp, #8
 8001f0c:	af00      	add	r7, sp, #0
 8001f0e:	6078      	str	r0, [r7, #4]
 8001f10:	687b      	ldr	r3, [r7, #4]
 8001f12:	69db      	ldr	r3, [r3, #28]
 8001f14:	2240      	movs	r2, #64	; 0x40
 8001f16:	4013      	ands	r3, r2
 8001f18:	3b40      	subs	r3, #64	; 0x40
 8001f1a:	425a      	negs	r2, r3
 8001f1c:	4153      	adcs	r3, r2
 8001f1e:	b2db      	uxtb	r3, r3
 8001f20:	0018      	movs	r0, r3
 8001f22:	46bd      	mov	sp, r7
 8001f24:	b002      	add	sp, #8
 8001f26:	bd80      	pop	{r7, pc}

08001f28 <LL_USART_IsActiveFlag_TXE>:
 8001f28:	b580      	push	{r7, lr}
 8001f2a:	b082      	sub	sp, #8
 8001f2c:	af00      	add	r7, sp, #0
 8001f2e:	6078      	str	r0, [r7, #4]
 8001f30:	687b      	ldr	r3, [r7, #4]
 8001f32:	69db      	ldr	r3, [r3, #28]
 8001f34:	2280      	movs	r2, #128	; 0x80
 8001f36:	4013      	ands	r3, r2
 8001f38:	3b80      	subs	r3, #128	; 0x80
 8001f3a:	425a      	negs	r2, r3
 8001f3c:	4153      	adcs	r3, r2
 8001f3e:	b2db      	uxtb	r3, r3
 8001f40:	0018      	movs	r0, r3
 8001f42:	46bd      	mov	sp, r7
 8001f44:	b002      	add	sp, #8
 8001f46:	bd80      	pop	{r7, pc}

08001f48 <LL_USART_ClearFlag_TC>:
 8001f48:	b580      	push	{r7, lr}
 8001f4a:	b082      	sub	sp, #8
 8001f4c:	af00      	add	r7, sp, #0
 8001f4e:	6078      	str	r0, [r7, #4]
 8001f50:	687b      	ldr	r3, [r7, #4]
 8001f52:	2240      	movs	r2, #64	; 0x40
 8001f54:	621a      	str	r2, [r3, #32]
 8001f56:	46c0      	nop			; (mov r8, r8)
 8001f58:	46bd      	mov	sp, r7
 8001f5a:	b002      	add	sp, #8
 8001f5c:	bd80      	pop	{r7, pc}

08001f5e <LL_USART_TransmitData8>:
 8001f5e:	b580      	push	{r7, lr}
 8001f60:	b082      	sub	sp, #8
 8001f62:	af00      	add	r7, sp, #0
 8001f64:	6078      	str	r0, [r7, #4]
 8001f66:	000a      	movs	r2, r1
 8001f68:	1cfb      	adds	r3, r7, #3
 8001f6a:	701a      	strb	r2, [r3, #0]
 8001f6c:	1cfb      	adds	r3, r7, #3
 8001f6e:	781b      	ldrb	r3, [r3, #0]
 8001f70:	b29a      	uxth	r2, r3
 8001f72:	687b      	ldr	r3, [r7, #4]
 8001f74:	851a      	strh	r2, [r3, #40]	; 0x28
 8001f76:	46c0      	nop			; (mov r8, r8)
 8001f78:	46bd      	mov	sp, r7
 8001f7a:	b002      	add	sp, #8
 8001f7c:	bd80      	pop	{r7, pc}
	...

08001f80 <LL_AHB1_GRP1_EnableClock>:
 8001f80:	b580      	push	{r7, lr}
 8001f82:	b084      	sub	sp, #16
 8001f84:	af00      	add	r7, sp, #0
 8001f86:	6078      	str	r0, [r7, #4]
 8001f88:	4b07      	ldr	r3, [pc, #28]	; (8001fa8 <LL_AHB1_GRP1_EnableClock+0x28>)
 8001f8a:	6959      	ldr	r1, [r3, #20]
 8001f8c:	4b06      	ldr	r3, [pc, #24]	; (8001fa8 <LL_AHB1_GRP1_EnableClock+0x28>)
 8001f8e:	687a      	ldr	r2, [r7, #4]
 8001f90:	430a      	orrs	r2, r1
 8001f92:	615a      	str	r2, [r3, #20]
 8001f94:	4b04      	ldr	r3, [pc, #16]	; (8001fa8 <LL_AHB1_GRP1_EnableClock+0x28>)
 8001f96:	695b      	ldr	r3, [r3, #20]
 8001f98:	687a      	ldr	r2, [r7, #4]
 8001f9a:	4013      	ands	r3, r2
 8001f9c:	60fb      	str	r3, [r7, #12]
 8001f9e:	68fb      	ldr	r3, [r7, #12]
 8001fa0:	46c0      	nop			; (mov r8, r8)
 8001fa2:	46bd      	mov	sp, r7
 8001fa4:	b004      	add	sp, #16
 8001fa6:	bd80      	pop	{r7, pc}
 8001fa8:	40021000 	.word	0x40021000

08001fac <LL_APB1_GRP2_EnableClock>:
 8001fac:	b580      	push	{r7, lr}
 8001fae:	b084      	sub	sp, #16
 8001fb0:	af00      	add	r7, sp, #0
 8001fb2:	6078      	str	r0, [r7, #4]
 8001fb4:	4b07      	ldr	r3, [pc, #28]	; (8001fd4 <LL_APB1_GRP2_EnableClock+0x28>)
 8001fb6:	6999      	ldr	r1, [r3, #24]
 8001fb8:	4b06      	ldr	r3, [pc, #24]	; (8001fd4 <LL_APB1_GRP2_EnableClock+0x28>)
 8001fba:	687a      	ldr	r2, [r7, #4]
 8001fbc:	430a      	orrs	r2, r1
 8001fbe:	619a      	str	r2, [r3, #24]
 8001fc0:	4b04      	ldr	r3, [pc, #16]	; (8001fd4 <LL_APB1_GRP2_EnableClock+0x28>)
 8001fc2:	699b      	ldr	r3, [r3, #24]
 8001fc4:	687a      	ldr	r2, [r7, #4]
 8001fc6:	4013      	ands	r3, r2
 8001fc8:	60fb      	str	r3, [r7, #12]
 8001fca:	68fb      	ldr	r3, [r7, #12]
 8001fcc:	46c0      	nop			; (mov r8, r8)
 8001fce:	46bd      	mov	sp, r7
 8001fd0:	b004      	add	sp, #16
 8001fd2:	bd80      	pop	{r7, pc}
 8001fd4:	40021000 	.word	0x40021000

08001fd8 <dyn_send_cmd>:
 8001fd8:	b580      	push	{r7, lr}
 8001fda:	b084      	sub	sp, #16
 8001fdc:	af00      	add	r7, sp, #0
 8001fde:	6078      	str	r0, [r7, #4]
 8001fe0:	6039      	str	r1, [r7, #0]
 8001fe2:	2300      	movs	r3, #0
 8001fe4:	60fb      	str	r3, [r7, #12]
 8001fe6:	b672      	cpsid	i
 8001fe8:	4b1c      	ldr	r3, [pc, #112]	; (800205c <dyn_send_cmd+0x84>)
 8001fea:	0018      	movs	r0, r3
 8001fec:	f7ff fedb 	bl	8001da6 <LL_USART_DisableDirectionRx>
 8001ff0:	4b1a      	ldr	r3, [pc, #104]	; (800205c <dyn_send_cmd+0x84>)
 8001ff2:	0018      	movs	r0, r3
 8001ff4:	f7ff fee6 	bl	8001dc4 <LL_USART_EnableDirectionTx>
 8001ff8:	4b18      	ldr	r3, [pc, #96]	; (800205c <dyn_send_cmd+0x84>)
 8001ffa:	0018      	movs	r0, r3
 8001ffc:	f7ff ffa4 	bl	8001f48 <LL_USART_ClearFlag_TC>
 8002000:	e012      	b.n	8002028 <dyn_send_cmd+0x50>
 8002002:	46c0      	nop			; (mov r8, r8)
 8002004:	4b15      	ldr	r3, [pc, #84]	; (800205c <dyn_send_cmd+0x84>)
 8002006:	0018      	movs	r0, r3
 8002008:	f7ff ff8e 	bl	8001f28 <LL_USART_IsActiveFlag_TXE>
 800200c:	1e03      	subs	r3, r0, #0
 800200e:	d0f9      	beq.n	8002004 <dyn_send_cmd+0x2c>
 8002010:	68fb      	ldr	r3, [r7, #12]
 8002012:	1c5a      	adds	r2, r3, #1
 8002014:	60fa      	str	r2, [r7, #12]
 8002016:	001a      	movs	r2, r3
 8002018:	687b      	ldr	r3, [r7, #4]
 800201a:	189b      	adds	r3, r3, r2
 800201c:	781b      	ldrb	r3, [r3, #0]
 800201e:	4a0f      	ldr	r2, [pc, #60]	; (800205c <dyn_send_cmd+0x84>)
 8002020:	0019      	movs	r1, r3
 8002022:	0010      	movs	r0, r2
 8002024:	f7ff ff9b 	bl	8001f5e <LL_USART_TransmitData8>
 8002028:	683b      	ldr	r3, [r7, #0]
 800202a:	1e5a      	subs	r2, r3, #1
 800202c:	603a      	str	r2, [r7, #0]
 800202e:	2b00      	cmp	r3, #0
 8002030:	d1e7      	bne.n	8002002 <dyn_send_cmd+0x2a>
 8002032:	46c0      	nop			; (mov r8, r8)
 8002034:	4b09      	ldr	r3, [pc, #36]	; (800205c <dyn_send_cmd+0x84>)
 8002036:	0018      	movs	r0, r3
 8002038:	f7ff ff66 	bl	8001f08 <LL_USART_IsActiveFlag_TC>
 800203c:	1e03      	subs	r3, r0, #0
 800203e:	d0f9      	beq.n	8002034 <dyn_send_cmd+0x5c>
 8002040:	4b06      	ldr	r3, [pc, #24]	; (800205c <dyn_send_cmd+0x84>)
 8002042:	0018      	movs	r0, r3
 8002044:	f7ff fecc 	bl	8001de0 <LL_USART_DisableDirectionTx>
 8002048:	4b04      	ldr	r3, [pc, #16]	; (800205c <dyn_send_cmd+0x84>)
 800204a:	0018      	movs	r0, r3
 800204c:	f7ff fe9d 	bl	8001d8a <LL_USART_EnableDirectionRx>
 8002050:	b662      	cpsie	i
 8002052:	46c0      	nop			; (mov r8, r8)
 8002054:	46bd      	mov	sp, r7
 8002056:	b004      	add	sp, #16
 8002058:	bd80      	pop	{r7, pc}
 800205a:	46c0      	nop			; (mov r8, r8)
 800205c:	40013800 	.word	0x40013800

08002060 <dyn_delay>:
 8002060:	b580      	push	{r7, lr}
 8002062:	b082      	sub	sp, #8
 8002064:	af00      	add	r7, sp, #0
 8002066:	6078      	str	r0, [r7, #4]
 8002068:	46c0      	nop			; (mov r8, r8)
 800206a:	687b      	ldr	r3, [r7, #4]
 800206c:	1e5a      	subs	r2, r3, #1
 800206e:	607a      	str	r2, [r7, #4]
 8002070:	2b00      	cmp	r3, #0
 8002072:	d1fa      	bne.n	800206a <dyn_delay+0xa>
 8002074:	46c0      	nop			; (mov r8, r8)
 8002076:	46bd      	mov	sp, r7
 8002078:	b002      	add	sp, #8
 800207a:	bd80      	pop	{r7, pc}

0800207c <dyn_set_paws_angle>:
 800207c:	b590      	push	{r4, r7, lr}
 800207e:	b089      	sub	sp, #36	; 0x24
 8002080:	af00      	add	r7, sp, #0
 8002082:	6078      	str	r0, [r7, #4]
 8002084:	231a      	movs	r3, #26
 8002086:	18fb      	adds	r3, r7, r3
 8002088:	2205      	movs	r2, #5
 800208a:	701a      	strb	r2, [r3, #0]
 800208c:	231f      	movs	r3, #31
 800208e:	18fb      	adds	r3, r7, r3
 8002090:	2201      	movs	r2, #1
 8002092:	701a      	strb	r2, [r3, #0]
 8002094:	231e      	movs	r3, #30
 8002096:	18fb      	adds	r3, r7, r3
 8002098:	2200      	movs	r2, #0
 800209a:	701a      	strb	r2, [r3, #0]
 800209c:	e085      	b.n	80021aa <dyn_set_paws_angle+0x12e>
 800209e:	210c      	movs	r1, #12
 80020a0:	187b      	adds	r3, r7, r1
 80020a2:	22ff      	movs	r2, #255	; 0xff
 80020a4:	701a      	strb	r2, [r3, #0]
 80020a6:	187b      	adds	r3, r7, r1
 80020a8:	22ff      	movs	r2, #255	; 0xff
 80020aa:	705a      	strb	r2, [r3, #1]
 80020ac:	187b      	adds	r3, r7, r1
 80020ae:	221a      	movs	r2, #26
 80020b0:	18ba      	adds	r2, r7, r2
 80020b2:	7812      	ldrb	r2, [r2, #0]
 80020b4:	70da      	strb	r2, [r3, #3]
 80020b6:	187b      	adds	r3, r7, r1
 80020b8:	2203      	movs	r2, #3
 80020ba:	711a      	strb	r2, [r3, #4]
 80020bc:	187b      	adds	r3, r7, r1
 80020be:	221e      	movs	r2, #30
 80020c0:	715a      	strb	r2, [r3, #5]
 80020c2:	187b      	adds	r3, r7, r1
 80020c4:	221f      	movs	r2, #31
 80020c6:	18ba      	adds	r2, r7, r2
 80020c8:	7812      	ldrb	r2, [r2, #0]
 80020ca:	709a      	strb	r2, [r3, #2]
 80020cc:	241e      	movs	r4, #30
 80020ce:	193b      	adds	r3, r7, r4
 80020d0:	781b      	ldrb	r3, [r3, #0]
 80020d2:	687a      	ldr	r2, [r7, #4]
 80020d4:	18d2      	adds	r2, r2, r3
 80020d6:	2019      	movs	r0, #25
 80020d8:	183b      	adds	r3, r7, r0
 80020da:	7812      	ldrb	r2, [r2, #0]
 80020dc:	701a      	strb	r2, [r3, #0]
 80020de:	193b      	adds	r3, r7, r4
 80020e0:	781b      	ldrb	r3, [r3, #0]
 80020e2:	3301      	adds	r3, #1
 80020e4:	687a      	ldr	r2, [r7, #4]
 80020e6:	18d2      	adds	r2, r2, r3
 80020e8:	2418      	movs	r4, #24
 80020ea:	193b      	adds	r3, r7, r4
 80020ec:	7812      	ldrb	r2, [r2, #0]
 80020ee:	701a      	strb	r2, [r3, #0]
 80020f0:	187b      	adds	r3, r7, r1
 80020f2:	193a      	adds	r2, r7, r4
 80020f4:	7812      	ldrb	r2, [r2, #0]
 80020f6:	719a      	strb	r2, [r3, #6]
 80020f8:	187b      	adds	r3, r7, r1
 80020fa:	0001      	movs	r1, r0
 80020fc:	187a      	adds	r2, r7, r1
 80020fe:	7812      	ldrb	r2, [r2, #0]
 8002100:	71da      	strb	r2, [r3, #7]
 8002102:	2016      	movs	r0, #22
 8002104:	183b      	adds	r3, r7, r0
 8002106:	187a      	adds	r2, r7, r1
 8002108:	7812      	ldrb	r2, [r2, #0]
 800210a:	801a      	strh	r2, [r3, #0]
 800210c:	183b      	adds	r3, r7, r0
 800210e:	183a      	adds	r2, r7, r0
 8002110:	8812      	ldrh	r2, [r2, #0]
 8002112:	0212      	lsls	r2, r2, #8
 8002114:	801a      	strh	r2, [r3, #0]
 8002116:	193b      	adds	r3, r7, r4
 8002118:	781b      	ldrb	r3, [r3, #0]
 800211a:	b299      	uxth	r1, r3
 800211c:	183b      	adds	r3, r7, r0
 800211e:	183a      	adds	r2, r7, r0
 8002120:	8812      	ldrh	r2, [r2, #0]
 8002122:	188a      	adds	r2, r1, r2
 8002124:	801a      	strh	r2, [r3, #0]
 8002126:	231c      	movs	r3, #28
 8002128:	18fb      	adds	r3, r7, r3
 800212a:	2200      	movs	r2, #0
 800212c:	801a      	strh	r2, [r3, #0]
 800212e:	231b      	movs	r3, #27
 8002130:	18fb      	adds	r3, r7, r3
 8002132:	2202      	movs	r2, #2
 8002134:	701a      	strb	r2, [r3, #0]
 8002136:	e011      	b.n	800215c <dyn_set_paws_angle+0xe0>
 8002138:	201b      	movs	r0, #27
 800213a:	183b      	adds	r3, r7, r0
 800213c:	781b      	ldrb	r3, [r3, #0]
 800213e:	220c      	movs	r2, #12
 8002140:	18ba      	adds	r2, r7, r2
 8002142:	5cd3      	ldrb	r3, [r2, r3]
 8002144:	b299      	uxth	r1, r3
 8002146:	221c      	movs	r2, #28
 8002148:	18bb      	adds	r3, r7, r2
 800214a:	18ba      	adds	r2, r7, r2
 800214c:	8812      	ldrh	r2, [r2, #0]
 800214e:	188a      	adds	r2, r1, r2
 8002150:	801a      	strh	r2, [r3, #0]
 8002152:	183b      	adds	r3, r7, r0
 8002154:	183a      	adds	r2, r7, r0
 8002156:	7812      	ldrb	r2, [r2, #0]
 8002158:	3201      	adds	r2, #1
 800215a:	701a      	strb	r2, [r3, #0]
 800215c:	231b      	movs	r3, #27
 800215e:	18fb      	adds	r3, r7, r3
 8002160:	781b      	ldrb	r3, [r3, #0]
 8002162:	2b07      	cmp	r3, #7
 8002164:	d9e8      	bls.n	8002138 <dyn_set_paws_angle+0xbc>
 8002166:	231c      	movs	r3, #28
 8002168:	18fb      	adds	r3, r7, r3
 800216a:	881b      	ldrh	r3, [r3, #0]
 800216c:	b2da      	uxtb	r2, r3
 800216e:	2115      	movs	r1, #21
 8002170:	187b      	adds	r3, r7, r1
 8002172:	43d2      	mvns	r2, r2
 8002174:	701a      	strb	r2, [r3, #0]
 8002176:	200c      	movs	r0, #12
 8002178:	183b      	adds	r3, r7, r0
 800217a:	187a      	adds	r2, r7, r1
 800217c:	7812      	ldrb	r2, [r2, #0]
 800217e:	721a      	strb	r2, [r3, #8]
 8002180:	183b      	adds	r3, r7, r0
 8002182:	2109      	movs	r1, #9
 8002184:	0018      	movs	r0, r3
 8002186:	f7ff ff27 	bl	8001fd8 <dyn_send_cmd>
 800218a:	4b0d      	ldr	r3, [pc, #52]	; (80021c0 <dyn_set_paws_angle+0x144>)
 800218c:	0018      	movs	r0, r3
 800218e:	f7ff ff67 	bl	8002060 <dyn_delay>
 8002192:	221f      	movs	r2, #31
 8002194:	18bb      	adds	r3, r7, r2
 8002196:	18ba      	adds	r2, r7, r2
 8002198:	7812      	ldrb	r2, [r2, #0]
 800219a:	3201      	adds	r2, #1
 800219c:	701a      	strb	r2, [r3, #0]
 800219e:	221e      	movs	r2, #30
 80021a0:	18bb      	adds	r3, r7, r2
 80021a2:	18ba      	adds	r2, r7, r2
 80021a4:	7812      	ldrb	r2, [r2, #0]
 80021a6:	3202      	adds	r2, #2
 80021a8:	701a      	strb	r2, [r3, #0]
 80021aa:	231f      	movs	r3, #31
 80021ac:	18fb      	adds	r3, r7, r3
 80021ae:	781b      	ldrb	r3, [r3, #0]
 80021b0:	2b05      	cmp	r3, #5
 80021b2:	d800      	bhi.n	80021b6 <dyn_set_paws_angle+0x13a>
 80021b4:	e773      	b.n	800209e <dyn_set_paws_angle+0x22>
 80021b6:	46c0      	nop			; (mov r8, r8)
 80021b8:	46bd      	mov	sp, r7
 80021ba:	b009      	add	sp, #36	; 0x24
 80021bc:	bd90      	pop	{r4, r7, pc}
 80021be:	46c0      	nop			; (mov r8, r8)
 80021c0:	0000bb80 	.word	0x0000bb80

080021c4 <dyn_set_init_pos>:
 80021c4:	b590      	push	{r4, r7, lr}
 80021c6:	b089      	sub	sp, #36	; 0x24
 80021c8:	af00      	add	r7, sp, #0
 80021ca:	6078      	str	r0, [r7, #4]
 80021cc:	201a      	movs	r0, #26
 80021ce:	183b      	adds	r3, r7, r0
 80021d0:	2205      	movs	r2, #5
 80021d2:	701a      	strb	r2, [r3, #0]
 80021d4:	210c      	movs	r1, #12
 80021d6:	187b      	adds	r3, r7, r1
 80021d8:	22ff      	movs	r2, #255	; 0xff
 80021da:	701a      	strb	r2, [r3, #0]
 80021dc:	187b      	adds	r3, r7, r1
 80021de:	22ff      	movs	r2, #255	; 0xff
 80021e0:	705a      	strb	r2, [r3, #1]
 80021e2:	187b      	adds	r3, r7, r1
 80021e4:	183a      	adds	r2, r7, r0
 80021e6:	7812      	ldrb	r2, [r2, #0]
 80021e8:	70da      	strb	r2, [r3, #3]
 80021ea:	187b      	adds	r3, r7, r1
 80021ec:	2203      	movs	r2, #3
 80021ee:	711a      	strb	r2, [r3, #4]
 80021f0:	187b      	adds	r3, r7, r1
 80021f2:	221e      	movs	r2, #30
 80021f4:	715a      	strb	r2, [r3, #5]
 80021f6:	231f      	movs	r3, #31
 80021f8:	18fb      	adds	r3, r7, r3
 80021fa:	2201      	movs	r2, #1
 80021fc:	701a      	strb	r2, [r3, #0]
 80021fe:	231e      	movs	r3, #30
 8002200:	18fb      	adds	r3, r7, r3
 8002202:	2200      	movs	r2, #0
 8002204:	701a      	strb	r2, [r3, #0]
 8002206:	e074      	b.n	80022f2 <dyn_set_init_pos+0x12e>
 8002208:	210c      	movs	r1, #12
 800220a:	187b      	adds	r3, r7, r1
 800220c:	221f      	movs	r2, #31
 800220e:	18ba      	adds	r2, r7, r2
 8002210:	7812      	ldrb	r2, [r2, #0]
 8002212:	709a      	strb	r2, [r3, #2]
 8002214:	241e      	movs	r4, #30
 8002216:	193b      	adds	r3, r7, r4
 8002218:	781b      	ldrb	r3, [r3, #0]
 800221a:	687a      	ldr	r2, [r7, #4]
 800221c:	18d2      	adds	r2, r2, r3
 800221e:	2019      	movs	r0, #25
 8002220:	183b      	adds	r3, r7, r0
 8002222:	7812      	ldrb	r2, [r2, #0]
 8002224:	701a      	strb	r2, [r3, #0]
 8002226:	193b      	adds	r3, r7, r4
 8002228:	781b      	ldrb	r3, [r3, #0]
 800222a:	3301      	adds	r3, #1
 800222c:	687a      	ldr	r2, [r7, #4]
 800222e:	18d2      	adds	r2, r2, r3
 8002230:	2418      	movs	r4, #24
 8002232:	193b      	adds	r3, r7, r4
 8002234:	7812      	ldrb	r2, [r2, #0]
 8002236:	701a      	strb	r2, [r3, #0]
 8002238:	187b      	adds	r3, r7, r1
 800223a:	193a      	adds	r2, r7, r4
 800223c:	7812      	ldrb	r2, [r2, #0]
 800223e:	719a      	strb	r2, [r3, #6]
 8002240:	187b      	adds	r3, r7, r1
 8002242:	0001      	movs	r1, r0
 8002244:	187a      	adds	r2, r7, r1
 8002246:	7812      	ldrb	r2, [r2, #0]
 8002248:	71da      	strb	r2, [r3, #7]
 800224a:	2016      	movs	r0, #22
 800224c:	183b      	adds	r3, r7, r0
 800224e:	187a      	adds	r2, r7, r1
 8002250:	7812      	ldrb	r2, [r2, #0]
 8002252:	801a      	strh	r2, [r3, #0]
 8002254:	183b      	adds	r3, r7, r0
 8002256:	183a      	adds	r2, r7, r0
 8002258:	8812      	ldrh	r2, [r2, #0]
 800225a:	0212      	lsls	r2, r2, #8
 800225c:	801a      	strh	r2, [r3, #0]
 800225e:	193b      	adds	r3, r7, r4
 8002260:	781b      	ldrb	r3, [r3, #0]
 8002262:	b299      	uxth	r1, r3
 8002264:	183b      	adds	r3, r7, r0
 8002266:	183a      	adds	r2, r7, r0
 8002268:	8812      	ldrh	r2, [r2, #0]
 800226a:	188a      	adds	r2, r1, r2
 800226c:	801a      	strh	r2, [r3, #0]
 800226e:	231c      	movs	r3, #28
 8002270:	18fb      	adds	r3, r7, r3
 8002272:	2200      	movs	r2, #0
 8002274:	801a      	strh	r2, [r3, #0]
 8002276:	231b      	movs	r3, #27
 8002278:	18fb      	adds	r3, r7, r3
 800227a:	2202      	movs	r2, #2
 800227c:	701a      	strb	r2, [r3, #0]
 800227e:	e011      	b.n	80022a4 <dyn_set_init_pos+0xe0>
 8002280:	201b      	movs	r0, #27
 8002282:	183b      	adds	r3, r7, r0
 8002284:	781b      	ldrb	r3, [r3, #0]
 8002286:	220c      	movs	r2, #12
 8002288:	18ba      	adds	r2, r7, r2
 800228a:	5cd3      	ldrb	r3, [r2, r3]
 800228c:	b299      	uxth	r1, r3
 800228e:	221c      	movs	r2, #28
 8002290:	18bb      	adds	r3, r7, r2
 8002292:	18ba      	adds	r2, r7, r2
 8002294:	8812      	ldrh	r2, [r2, #0]
 8002296:	188a      	adds	r2, r1, r2
 8002298:	801a      	strh	r2, [r3, #0]
 800229a:	183b      	adds	r3, r7, r0
 800229c:	183a      	adds	r2, r7, r0
 800229e:	7812      	ldrb	r2, [r2, #0]
 80022a0:	3201      	adds	r2, #1
 80022a2:	701a      	strb	r2, [r3, #0]
 80022a4:	231b      	movs	r3, #27
 80022a6:	18fb      	adds	r3, r7, r3
 80022a8:	781b      	ldrb	r3, [r3, #0]
 80022aa:	2b07      	cmp	r3, #7
 80022ac:	d9e8      	bls.n	8002280 <dyn_set_init_pos+0xbc>
 80022ae:	231c      	movs	r3, #28
 80022b0:	18fb      	adds	r3, r7, r3
 80022b2:	881b      	ldrh	r3, [r3, #0]
 80022b4:	b2da      	uxtb	r2, r3
 80022b6:	2115      	movs	r1, #21
 80022b8:	187b      	adds	r3, r7, r1
 80022ba:	43d2      	mvns	r2, r2
 80022bc:	701a      	strb	r2, [r3, #0]
 80022be:	200c      	movs	r0, #12
 80022c0:	183b      	adds	r3, r7, r0
 80022c2:	187a      	adds	r2, r7, r1
 80022c4:	7812      	ldrb	r2, [r2, #0]
 80022c6:	721a      	strb	r2, [r3, #8]
 80022c8:	183b      	adds	r3, r7, r0
 80022ca:	2109      	movs	r1, #9
 80022cc:	0018      	movs	r0, r3
 80022ce:	f7ff fe83 	bl	8001fd8 <dyn_send_cmd>
 80022d2:	4b0c      	ldr	r3, [pc, #48]	; (8002304 <dyn_set_init_pos+0x140>)
 80022d4:	0018      	movs	r0, r3
 80022d6:	f7ff fec3 	bl	8002060 <dyn_delay>
 80022da:	221f      	movs	r2, #31
 80022dc:	18bb      	adds	r3, r7, r2
 80022de:	18ba      	adds	r2, r7, r2
 80022e0:	7812      	ldrb	r2, [r2, #0]
 80022e2:	3201      	adds	r2, #1
 80022e4:	701a      	strb	r2, [r3, #0]
 80022e6:	221e      	movs	r2, #30
 80022e8:	18bb      	adds	r3, r7, r2
 80022ea:	18ba      	adds	r2, r7, r2
 80022ec:	7812      	ldrb	r2, [r2, #0]
 80022ee:	3202      	adds	r2, #2
 80022f0:	701a      	strb	r2, [r3, #0]
 80022f2:	231f      	movs	r3, #31
 80022f4:	18fb      	adds	r3, r7, r3
 80022f6:	781b      	ldrb	r3, [r3, #0]
 80022f8:	2b09      	cmp	r3, #9
 80022fa:	d985      	bls.n	8002208 <dyn_set_init_pos+0x44>
 80022fc:	46c0      	nop			; (mov r8, r8)
 80022fe:	46bd      	mov	sp, r7
 8002300:	b009      	add	sp, #36	; 0x24
 8002302:	bd90      	pop	{r4, r7, pc}
 8002304:	0000bb80 	.word	0x0000bb80

08002308 <dyn_set_doors_angle>:
 8002308:	b5b0      	push	{r4, r5, r7, lr}
 800230a:	b088      	sub	sp, #32
 800230c:	af00      	add	r7, sp, #0
 800230e:	6078      	str	r0, [r7, #4]
 8002310:	231a      	movs	r3, #26
 8002312:	18fb      	adds	r3, r7, r3
 8002314:	2205      	movs	r2, #5
 8002316:	701a      	strb	r2, [r3, #0]
 8002318:	231f      	movs	r3, #31
 800231a:	18fb      	adds	r3, r7, r3
 800231c:	2206      	movs	r2, #6
 800231e:	701a      	strb	r2, [r3, #0]
 8002320:	e080      	b.n	8002424 <dyn_set_doors_angle+0x11c>
 8002322:	210c      	movs	r1, #12
 8002324:	187b      	adds	r3, r7, r1
 8002326:	22ff      	movs	r2, #255	; 0xff
 8002328:	701a      	strb	r2, [r3, #0]
 800232a:	187b      	adds	r3, r7, r1
 800232c:	22ff      	movs	r2, #255	; 0xff
 800232e:	705a      	strb	r2, [r3, #1]
 8002330:	187b      	adds	r3, r7, r1
 8002332:	221a      	movs	r2, #26
 8002334:	18ba      	adds	r2, r7, r2
 8002336:	7812      	ldrb	r2, [r2, #0]
 8002338:	70da      	strb	r2, [r3, #3]
 800233a:	187b      	adds	r3, r7, r1
 800233c:	2203      	movs	r2, #3
 800233e:	711a      	strb	r2, [r3, #4]
 8002340:	187b      	adds	r3, r7, r1
 8002342:	221e      	movs	r2, #30
 8002344:	715a      	strb	r2, [r3, #5]
 8002346:	187b      	adds	r3, r7, r1
 8002348:	201f      	movs	r0, #31
 800234a:	183a      	adds	r2, r7, r0
 800234c:	7812      	ldrb	r2, [r2, #0]
 800234e:	709a      	strb	r2, [r3, #2]
 8002350:	183b      	adds	r3, r7, r0
 8002352:	781b      	ldrb	r3, [r3, #0]
 8002354:	005b      	lsls	r3, r3, #1
 8002356:	3b02      	subs	r3, #2
 8002358:	687a      	ldr	r2, [r7, #4]
 800235a:	18d2      	adds	r2, r2, r3
 800235c:	2419      	movs	r4, #25
 800235e:	193b      	adds	r3, r7, r4
 8002360:	7812      	ldrb	r2, [r2, #0]
 8002362:	701a      	strb	r2, [r3, #0]
 8002364:	183b      	adds	r3, r7, r0
 8002366:	781b      	ldrb	r3, [r3, #0]
 8002368:	005b      	lsls	r3, r3, #1
 800236a:	3b01      	subs	r3, #1
 800236c:	687a      	ldr	r2, [r7, #4]
 800236e:	18d2      	adds	r2, r2, r3
 8002370:	2518      	movs	r5, #24
 8002372:	197b      	adds	r3, r7, r5
 8002374:	7812      	ldrb	r2, [r2, #0]
 8002376:	701a      	strb	r2, [r3, #0]
 8002378:	187b      	adds	r3, r7, r1
 800237a:	197a      	adds	r2, r7, r5
 800237c:	7812      	ldrb	r2, [r2, #0]
 800237e:	719a      	strb	r2, [r3, #6]
 8002380:	187b      	adds	r3, r7, r1
 8002382:	193a      	adds	r2, r7, r4
 8002384:	7812      	ldrb	r2, [r2, #0]
 8002386:	71da      	strb	r2, [r3, #7]
 8002388:	2016      	movs	r0, #22
 800238a:	183b      	adds	r3, r7, r0
 800238c:	193a      	adds	r2, r7, r4
 800238e:	7812      	ldrb	r2, [r2, #0]
 8002390:	801a      	strh	r2, [r3, #0]
 8002392:	183b      	adds	r3, r7, r0
 8002394:	183a      	adds	r2, r7, r0
 8002396:	8812      	ldrh	r2, [r2, #0]
 8002398:	0212      	lsls	r2, r2, #8
 800239a:	801a      	strh	r2, [r3, #0]
 800239c:	197b      	adds	r3, r7, r5
 800239e:	781b      	ldrb	r3, [r3, #0]
 80023a0:	b299      	uxth	r1, r3
 80023a2:	183b      	adds	r3, r7, r0
 80023a4:	183a      	adds	r2, r7, r0
 80023a6:	8812      	ldrh	r2, [r2, #0]
 80023a8:	188a      	adds	r2, r1, r2
 80023aa:	801a      	strh	r2, [r3, #0]
 80023ac:	231c      	movs	r3, #28
 80023ae:	18fb      	adds	r3, r7, r3
 80023b0:	2200      	movs	r2, #0
 80023b2:	801a      	strh	r2, [r3, #0]
 80023b4:	231b      	movs	r3, #27
 80023b6:	18fb      	adds	r3, r7, r3
 80023b8:	2202      	movs	r2, #2
 80023ba:	701a      	strb	r2, [r3, #0]
 80023bc:	e011      	b.n	80023e2 <dyn_set_doors_angle+0xda>
 80023be:	201b      	movs	r0, #27
 80023c0:	183b      	adds	r3, r7, r0
 80023c2:	781b      	ldrb	r3, [r3, #0]
 80023c4:	220c      	movs	r2, #12
 80023c6:	18ba      	adds	r2, r7, r2
 80023c8:	5cd3      	ldrb	r3, [r2, r3]
 80023ca:	b299      	uxth	r1, r3
 80023cc:	221c      	movs	r2, #28
 80023ce:	18bb      	adds	r3, r7, r2
 80023d0:	18ba      	adds	r2, r7, r2
 80023d2:	8812      	ldrh	r2, [r2, #0]
 80023d4:	188a      	adds	r2, r1, r2
 80023d6:	801a      	strh	r2, [r3, #0]
 80023d8:	183b      	adds	r3, r7, r0
 80023da:	183a      	adds	r2, r7, r0
 80023dc:	7812      	ldrb	r2, [r2, #0]
 80023de:	3201      	adds	r2, #1
 80023e0:	701a      	strb	r2, [r3, #0]
 80023e2:	231b      	movs	r3, #27
 80023e4:	18fb      	adds	r3, r7, r3
 80023e6:	781b      	ldrb	r3, [r3, #0]
 80023e8:	2b07      	cmp	r3, #7
 80023ea:	d9e8      	bls.n	80023be <dyn_set_doors_angle+0xb6>
 80023ec:	231c      	movs	r3, #28
 80023ee:	18fb      	adds	r3, r7, r3
 80023f0:	881b      	ldrh	r3, [r3, #0]
 80023f2:	b2da      	uxtb	r2, r3
 80023f4:	2115      	movs	r1, #21
 80023f6:	187b      	adds	r3, r7, r1
 80023f8:	43d2      	mvns	r2, r2
 80023fa:	701a      	strb	r2, [r3, #0]
 80023fc:	200c      	movs	r0, #12
 80023fe:	183b      	adds	r3, r7, r0
 8002400:	187a      	adds	r2, r7, r1
 8002402:	7812      	ldrb	r2, [r2, #0]
 8002404:	721a      	strb	r2, [r3, #8]
 8002406:	183b      	adds	r3, r7, r0
 8002408:	2109      	movs	r1, #9
 800240a:	0018      	movs	r0, r3
 800240c:	f7ff fde4 	bl	8001fd8 <dyn_send_cmd>
 8002410:	4b09      	ldr	r3, [pc, #36]	; (8002438 <dyn_set_doors_angle+0x130>)
 8002412:	0018      	movs	r0, r3
 8002414:	f7ff fe24 	bl	8002060 <dyn_delay>
 8002418:	211f      	movs	r1, #31
 800241a:	187b      	adds	r3, r7, r1
 800241c:	781a      	ldrb	r2, [r3, #0]
 800241e:	187b      	adds	r3, r7, r1
 8002420:	3201      	adds	r2, #1
 8002422:	701a      	strb	r2, [r3, #0]
 8002424:	231f      	movs	r3, #31
 8002426:	18fb      	adds	r3, r7, r3
 8002428:	781b      	ldrb	r3, [r3, #0]
 800242a:	2b09      	cmp	r3, #9
 800242c:	d800      	bhi.n	8002430 <dyn_set_doors_angle+0x128>
 800242e:	e778      	b.n	8002322 <dyn_set_doors_angle+0x1a>
 8002430:	46c0      	nop			; (mov r8, r8)
 8002432:	46bd      	mov	sp, r7
 8002434:	b008      	add	sp, #32
 8002436:	bdb0      	pop	{r4, r5, r7, pc}
 8002438:	0000bb80 	.word	0x0000bb80

0800243c <dynamixel_init>:
 800243c:	b580      	push	{r7, lr}
 800243e:	af00      	add	r7, sp, #0
 8002440:	2380      	movs	r3, #128	; 0x80
 8002442:	029b      	lsls	r3, r3, #10
 8002444:	0018      	movs	r0, r3
 8002446:	f7ff fd9b 	bl	8001f80 <LL_AHB1_GRP1_EnableClock>
 800244a:	2380      	movs	r3, #128	; 0x80
 800244c:	0099      	lsls	r1, r3, #2
 800244e:	2390      	movs	r3, #144	; 0x90
 8002450:	05db      	lsls	r3, r3, #23
 8002452:	2202      	movs	r2, #2
 8002454:	0018      	movs	r0, r3
 8002456:	f7ff fbe5 	bl	8001c24 <LL_GPIO_SetPinMode>
 800245a:	2380      	movs	r3, #128	; 0x80
 800245c:	0099      	lsls	r1, r3, #2
 800245e:	2390      	movs	r3, #144	; 0x90
 8002460:	05db      	lsls	r3, r3, #23
 8002462:	2201      	movs	r2, #1
 8002464:	0018      	movs	r0, r3
 8002466:	f7ff fc49 	bl	8001cfc <LL_GPIO_SetAFPin_8_15>
 800246a:	2380      	movs	r3, #128	; 0x80
 800246c:	0099      	lsls	r1, r3, #2
 800246e:	2390      	movs	r3, #144	; 0x90
 8002470:	05db      	lsls	r3, r3, #23
 8002472:	2200      	movs	r2, #0
 8002474:	0018      	movs	r0, r3
 8002476:	f7ff fbf2 	bl	8001c5e <LL_GPIO_SetPinOutputType>
 800247a:	2390      	movs	r3, #144	; 0x90
 800247c:	05db      	lsls	r3, r3, #23
 800247e:	2202      	movs	r2, #2
 8002480:	2104      	movs	r1, #4
 8002482:	0018      	movs	r0, r3
 8002484:	f7ff fc1d 	bl	8001cc2 <LL_GPIO_SetPinPull>
 8002488:	2380      	movs	r3, #128	; 0x80
 800248a:	0099      	lsls	r1, r3, #2
 800248c:	2390      	movs	r3, #144	; 0x90
 800248e:	05db      	lsls	r3, r3, #23
 8002490:	2203      	movs	r2, #3
 8002492:	0018      	movs	r0, r3
 8002494:	f7ff fbf8 	bl	8001c88 <LL_GPIO_SetPinSpeed>
 8002498:	2380      	movs	r3, #128	; 0x80
 800249a:	01db      	lsls	r3, r3, #7
 800249c:	0018      	movs	r0, r3
 800249e:	f7ff fd85 	bl	8001fac <LL_APB1_GRP2_EnableClock>
 80024a2:	4b20      	ldr	r3, [pc, #128]	; (8002524 <dynamixel_init+0xe8>)
 80024a4:	2100      	movs	r1, #0
 80024a6:	0018      	movs	r0, r3
 80024a8:	f7ff fcaa 	bl	8001e00 <LL_USART_SetParity>
 80024ac:	4b1d      	ldr	r3, [pc, #116]	; (8002524 <dynamixel_init+0xe8>)
 80024ae:	2100      	movs	r1, #0
 80024b0:	0018      	movs	r0, r3
 80024b2:	f7ff fcb9 	bl	8001e28 <LL_USART_SetDataWidth>
 80024b6:	4b1b      	ldr	r3, [pc, #108]	; (8002524 <dynamixel_init+0xe8>)
 80024b8:	2100      	movs	r1, #0
 80024ba:	0018      	movs	r0, r3
 80024bc:	f7ff fcc8 	bl	8001e50 <LL_USART_SetStopBitsLength>
 80024c0:	4b19      	ldr	r3, [pc, #100]	; (8002528 <dynamixel_init+0xec>)
 80024c2:	6819      	ldr	r1, [r3, #0]
 80024c4:	23e1      	movs	r3, #225	; 0xe1
 80024c6:	025b      	lsls	r3, r3, #9
 80024c8:	4816      	ldr	r0, [pc, #88]	; (8002524 <dynamixel_init+0xe8>)
 80024ca:	2200      	movs	r2, #0
 80024cc:	f7ff fcd4 	bl	8001e78 <LL_USART_SetBaudRate>
 80024d0:	4b14      	ldr	r3, [pc, #80]	; (8002524 <dynamixel_init+0xe8>)
 80024d2:	0018      	movs	r0, r3
 80024d4:	f7ff fd0a 	bl	8001eec <LL_USART_EnableHalfDuplex>
 80024d8:	4b12      	ldr	r3, [pc, #72]	; (8002524 <dynamixel_init+0xe8>)
 80024da:	0018      	movs	r0, r3
 80024dc:	f7ff fc80 	bl	8001de0 <LL_USART_DisableDirectionTx>
 80024e0:	4b10      	ldr	r3, [pc, #64]	; (8002524 <dynamixel_init+0xe8>)
 80024e2:	0018      	movs	r0, r3
 80024e4:	f7ff fc51 	bl	8001d8a <LL_USART_EnableDirectionRx>
 80024e8:	4b0e      	ldr	r3, [pc, #56]	; (8002524 <dynamixel_init+0xe8>)
 80024ea:	0018      	movs	r0, r3
 80024ec:	f7ff fc3f 	bl	8001d6e <LL_USART_Enable>
 80024f0:	2380      	movs	r3, #128	; 0x80
 80024f2:	031b      	lsls	r3, r3, #12
 80024f4:	0018      	movs	r0, r3
 80024f6:	f7ff fd43 	bl	8001f80 <LL_AHB1_GRP1_EnableClock>
 80024fa:	4b0c      	ldr	r3, [pc, #48]	; (800252c <dynamixel_init+0xf0>)
 80024fc:	2201      	movs	r2, #1
 80024fe:	2140      	movs	r1, #64	; 0x40
 8002500:	0018      	movs	r0, r3
 8002502:	f7ff fb8f 	bl	8001c24 <LL_GPIO_SetPinMode>
 8002506:	4b09      	ldr	r3, [pc, #36]	; (800252c <dynamixel_init+0xf0>)
 8002508:	2200      	movs	r2, #0
 800250a:	2140      	movs	r1, #64	; 0x40
 800250c:	0018      	movs	r0, r3
 800250e:	f7ff fba6 	bl	8001c5e <LL_GPIO_SetPinOutputType>
 8002512:	4b06      	ldr	r3, [pc, #24]	; (800252c <dynamixel_init+0xf0>)
 8002514:	2140      	movs	r1, #64	; 0x40
 8002516:	0018      	movs	r0, r3
 8002518:	f7ff fc1d 	bl	8001d56 <LL_GPIO_SetOutputPin>
 800251c:	46c0      	nop			; (mov r8, r8)
 800251e:	46bd      	mov	sp, r7
 8002520:	bd80      	pop	{r7, pc}
 8002522:	46c0      	nop			; (mov r8, r8)
 8002524:	40013800 	.word	0x40013800
 8002528:	20000000 	.word	0x20000000
 800252c:	48000800 	.word	0x48000800

08002530 <dyn_manager>:
 8002530:	b580      	push	{r7, lr}
 8002532:	b086      	sub	sp, #24
 8002534:	af00      	add	r7, sp, #0
 8002536:	6078      	str	r0, [r7, #4]
 8002538:	687b      	ldr	r3, [r7, #4]
 800253a:	617b      	str	r3, [r7, #20]
 800253c:	697b      	ldr	r3, [r7, #20]
 800253e:	3301      	adds	r3, #1
 8002540:	613b      	str	r3, [r7, #16]
 8002542:	210f      	movs	r1, #15
 8002544:	187b      	adds	r3, r7, r1
 8002546:	697a      	ldr	r2, [r7, #20]
 8002548:	7812      	ldrb	r2, [r2, #0]
 800254a:	701a      	strb	r2, [r3, #0]
 800254c:	187b      	adds	r3, r7, r1
 800254e:	781b      	ldrb	r3, [r3, #0]
 8002550:	2b03      	cmp	r3, #3
 8002552:	d00c      	beq.n	800256e <dyn_manager+0x3e>
 8002554:	dc02      	bgt.n	800255c <dyn_manager+0x2c>
 8002556:	2b00      	cmp	r3, #0
 8002558:	db15      	blt.n	8002586 <dyn_manager+0x56>
 800255a:	e002      	b.n	8002562 <dyn_manager+0x32>
 800255c:	2b04      	cmp	r3, #4
 800255e:	d00c      	beq.n	800257a <dyn_manager+0x4a>
 8002560:	e011      	b.n	8002586 <dyn_manager+0x56>
 8002562:	697b      	ldr	r3, [r7, #20]
 8002564:	3301      	adds	r3, #1
 8002566:	0018      	movs	r0, r3
 8002568:	f7ff fe2c 	bl	80021c4 <dyn_set_init_pos>
 800256c:	e00c      	b.n	8002588 <dyn_manager+0x58>
 800256e:	697b      	ldr	r3, [r7, #20]
 8002570:	3301      	adds	r3, #1
 8002572:	0018      	movs	r0, r3
 8002574:	f7ff fec8 	bl	8002308 <dyn_set_doors_angle>
 8002578:	e006      	b.n	8002588 <dyn_manager+0x58>
 800257a:	697b      	ldr	r3, [r7, #20]
 800257c:	3301      	adds	r3, #1
 800257e:	0018      	movs	r0, r3
 8002580:	f7ff fd7c 	bl	800207c <dyn_set_paws_angle>
 8002584:	e000      	b.n	8002588 <dyn_manager+0x58>
 8002586:	46c0      	nop			; (mov r8, r8)
 8002588:	46c0      	nop			; (mov r8, r8)
 800258a:	46bd      	mov	sp, r7
 800258c:	b006      	add	sp, #24
 800258e:	bd80      	pop	{r7, pc}

08002590 <NVIC_EnableIRQ>:
 8002590:	b580      	push	{r7, lr}
 8002592:	b082      	sub	sp, #8
 8002594:	af00      	add	r7, sp, #0
 8002596:	0002      	movs	r2, r0
 8002598:	1dfb      	adds	r3, r7, #7
 800259a:	701a      	strb	r2, [r3, #0]
 800259c:	1dfb      	adds	r3, r7, #7
 800259e:	781b      	ldrb	r3, [r3, #0]
 80025a0:	001a      	movs	r2, r3
 80025a2:	231f      	movs	r3, #31
 80025a4:	401a      	ands	r2, r3
 80025a6:	4b04      	ldr	r3, [pc, #16]	; (80025b8 <NVIC_EnableIRQ+0x28>)
 80025a8:	2101      	movs	r1, #1
 80025aa:	4091      	lsls	r1, r2
 80025ac:	000a      	movs	r2, r1
 80025ae:	601a      	str	r2, [r3, #0]
 80025b0:	46c0      	nop			; (mov r8, r8)
 80025b2:	46bd      	mov	sp, r7
 80025b4:	b002      	add	sp, #8
 80025b6:	bd80      	pop	{r7, pc}
 80025b8:	e000e100 	.word	0xe000e100

080025bc <NVIC_SetPriority>:
 80025bc:	b590      	push	{r4, r7, lr}
 80025be:	b083      	sub	sp, #12
 80025c0:	af00      	add	r7, sp, #0
 80025c2:	0002      	movs	r2, r0
 80025c4:	6039      	str	r1, [r7, #0]
 80025c6:	1dfb      	adds	r3, r7, #7
 80025c8:	701a      	strb	r2, [r3, #0]
 80025ca:	1dfb      	adds	r3, r7, #7
 80025cc:	781b      	ldrb	r3, [r3, #0]
 80025ce:	2b7f      	cmp	r3, #127	; 0x7f
 80025d0:	d932      	bls.n	8002638 <NVIC_SetPriority+0x7c>
 80025d2:	4a2f      	ldr	r2, [pc, #188]	; (8002690 <NVIC_SetPriority+0xd4>)
 80025d4:	1dfb      	adds	r3, r7, #7
 80025d6:	781b      	ldrb	r3, [r3, #0]
 80025d8:	0019      	movs	r1, r3
 80025da:	230f      	movs	r3, #15
 80025dc:	400b      	ands	r3, r1
 80025de:	3b08      	subs	r3, #8
 80025e0:	089b      	lsrs	r3, r3, #2
 80025e2:	3306      	adds	r3, #6
 80025e4:	009b      	lsls	r3, r3, #2
 80025e6:	18d3      	adds	r3, r2, r3
 80025e8:	3304      	adds	r3, #4
 80025ea:	681b      	ldr	r3, [r3, #0]
 80025ec:	1dfa      	adds	r2, r7, #7
 80025ee:	7812      	ldrb	r2, [r2, #0]
 80025f0:	0011      	movs	r1, r2
 80025f2:	2203      	movs	r2, #3
 80025f4:	400a      	ands	r2, r1
 80025f6:	00d2      	lsls	r2, r2, #3
 80025f8:	21ff      	movs	r1, #255	; 0xff
 80025fa:	4091      	lsls	r1, r2
 80025fc:	000a      	movs	r2, r1
 80025fe:	43d2      	mvns	r2, r2
 8002600:	401a      	ands	r2, r3
 8002602:	0011      	movs	r1, r2
 8002604:	683b      	ldr	r3, [r7, #0]
 8002606:	019b      	lsls	r3, r3, #6
 8002608:	22ff      	movs	r2, #255	; 0xff
 800260a:	401a      	ands	r2, r3
 800260c:	1dfb      	adds	r3, r7, #7
 800260e:	781b      	ldrb	r3, [r3, #0]
 8002610:	0018      	movs	r0, r3
 8002612:	2303      	movs	r3, #3
 8002614:	4003      	ands	r3, r0
 8002616:	00db      	lsls	r3, r3, #3
 8002618:	409a      	lsls	r2, r3
 800261a:	481d      	ldr	r0, [pc, #116]	; (8002690 <NVIC_SetPriority+0xd4>)
 800261c:	1dfb      	adds	r3, r7, #7
 800261e:	781b      	ldrb	r3, [r3, #0]
 8002620:	001c      	movs	r4, r3
 8002622:	230f      	movs	r3, #15
 8002624:	4023      	ands	r3, r4
 8002626:	3b08      	subs	r3, #8
 8002628:	089b      	lsrs	r3, r3, #2
 800262a:	430a      	orrs	r2, r1
 800262c:	3306      	adds	r3, #6
 800262e:	009b      	lsls	r3, r3, #2
 8002630:	18c3      	adds	r3, r0, r3
 8002632:	3304      	adds	r3, #4
 8002634:	601a      	str	r2, [r3, #0]
 8002636:	e027      	b.n	8002688 <NVIC_SetPriority+0xcc>
 8002638:	4a16      	ldr	r2, [pc, #88]	; (8002694 <NVIC_SetPriority+0xd8>)
 800263a:	1dfb      	adds	r3, r7, #7
 800263c:	781b      	ldrb	r3, [r3, #0]
 800263e:	b25b      	sxtb	r3, r3
 8002640:	089b      	lsrs	r3, r3, #2
 8002642:	33c0      	adds	r3, #192	; 0xc0
 8002644:	009b      	lsls	r3, r3, #2
 8002646:	589b      	ldr	r3, [r3, r2]
 8002648:	1dfa      	adds	r2, r7, #7
 800264a:	7812      	ldrb	r2, [r2, #0]
 800264c:	0011      	movs	r1, r2
 800264e:	2203      	movs	r2, #3
 8002650:	400a      	ands	r2, r1
 8002652:	00d2      	lsls	r2, r2, #3
 8002654:	21ff      	movs	r1, #255	; 0xff
 8002656:	4091      	lsls	r1, r2
 8002658:	000a      	movs	r2, r1
 800265a:	43d2      	mvns	r2, r2
 800265c:	401a      	ands	r2, r3
 800265e:	0011      	movs	r1, r2
 8002660:	683b      	ldr	r3, [r7, #0]
 8002662:	019b      	lsls	r3, r3, #6
 8002664:	22ff      	movs	r2, #255	; 0xff
 8002666:	401a      	ands	r2, r3
 8002668:	1dfb      	adds	r3, r7, #7
 800266a:	781b      	ldrb	r3, [r3, #0]
 800266c:	0018      	movs	r0, r3
 800266e:	2303      	movs	r3, #3
 8002670:	4003      	ands	r3, r0
 8002672:	00db      	lsls	r3, r3, #3
 8002674:	409a      	lsls	r2, r3
 8002676:	4807      	ldr	r0, [pc, #28]	; (8002694 <NVIC_SetPriority+0xd8>)
 8002678:	1dfb      	adds	r3, r7, #7
 800267a:	781b      	ldrb	r3, [r3, #0]
 800267c:	b25b      	sxtb	r3, r3
 800267e:	089b      	lsrs	r3, r3, #2
 8002680:	430a      	orrs	r2, r1
 8002682:	33c0      	adds	r3, #192	; 0xc0
 8002684:	009b      	lsls	r3, r3, #2
 8002686:	501a      	str	r2, [r3, r0]
 8002688:	46c0      	nop			; (mov r8, r8)
 800268a:	46bd      	mov	sp, r7
 800268c:	b003      	add	sp, #12
 800268e:	bd90      	pop	{r4, r7, pc}
 8002690:	e000ed00 	.word	0xe000ed00
 8002694:	e000e100 	.word	0xe000e100

08002698 <LL_TIM_EnableCounter>:
 8002698:	b580      	push	{r7, lr}
 800269a:	b082      	sub	sp, #8
 800269c:	af00      	add	r7, sp, #0
 800269e:	6078      	str	r0, [r7, #4]
 80026a0:	687b      	ldr	r3, [r7, #4]
 80026a2:	681b      	ldr	r3, [r3, #0]
 80026a4:	2201      	movs	r2, #1
 80026a6:	431a      	orrs	r2, r3
 80026a8:	687b      	ldr	r3, [r7, #4]
 80026aa:	601a      	str	r2, [r3, #0]
 80026ac:	46c0      	nop			; (mov r8, r8)
 80026ae:	46bd      	mov	sp, r7
 80026b0:	b002      	add	sp, #8
 80026b2:	bd80      	pop	{r7, pc}

080026b4 <LL_TIM_SetCounterMode>:
 80026b4:	b580      	push	{r7, lr}
 80026b6:	b082      	sub	sp, #8
 80026b8:	af00      	add	r7, sp, #0
 80026ba:	6078      	str	r0, [r7, #4]
 80026bc:	6039      	str	r1, [r7, #0]
 80026be:	687b      	ldr	r3, [r7, #4]
 80026c0:	681b      	ldr	r3, [r3, #0]
 80026c2:	2270      	movs	r2, #112	; 0x70
 80026c4:	4393      	bics	r3, r2
 80026c6:	001a      	movs	r2, r3
 80026c8:	683b      	ldr	r3, [r7, #0]
 80026ca:	431a      	orrs	r2, r3
 80026cc:	687b      	ldr	r3, [r7, #4]
 80026ce:	601a      	str	r2, [r3, #0]
 80026d0:	46c0      	nop			; (mov r8, r8)
 80026d2:	46bd      	mov	sp, r7
 80026d4:	b002      	add	sp, #8
 80026d6:	bd80      	pop	{r7, pc}

080026d8 <LL_TIM_SetPrescaler>:
 80026d8:	b580      	push	{r7, lr}
 80026da:	b082      	sub	sp, #8
 80026dc:	af00      	add	r7, sp, #0
 80026de:	6078      	str	r0, [r7, #4]
 80026e0:	6039      	str	r1, [r7, #0]
 80026e2:	687b      	ldr	r3, [r7, #4]
 80026e4:	683a      	ldr	r2, [r7, #0]
 80026e6:	629a      	str	r2, [r3, #40]	; 0x28
 80026e8:	46c0      	nop			; (mov r8, r8)
 80026ea:	46bd      	mov	sp, r7
 80026ec:	b002      	add	sp, #8
 80026ee:	bd80      	pop	{r7, pc}

080026f0 <LL_TIM_SetAutoReload>:
 80026f0:	b580      	push	{r7, lr}
 80026f2:	b082      	sub	sp, #8
 80026f4:	af00      	add	r7, sp, #0
 80026f6:	6078      	str	r0, [r7, #4]
 80026f8:	6039      	str	r1, [r7, #0]
 80026fa:	687b      	ldr	r3, [r7, #4]
 80026fc:	683a      	ldr	r2, [r7, #0]
 80026fe:	62da      	str	r2, [r3, #44]	; 0x2c
 8002700:	46c0      	nop			; (mov r8, r8)
 8002702:	46bd      	mov	sp, r7
 8002704:	b002      	add	sp, #8
 8002706:	bd80      	pop	{r7, pc}

08002708 <LL_TIM_ClearFlag_UPDATE>:
 8002708:	b580      	push	{r7, lr}
 800270a:	b082      	sub	sp, #8
 800270c:	af00      	add	r7, sp, #0
 800270e:	6078      	str	r0, [r7, #4]
 8002710:	687b      	ldr	r3, [r7, #4]
 8002712:	2202      	movs	r2, #2
 8002714:	4252      	negs	r2, r2
 8002716:	611a      	str	r2, [r3, #16]
 8002718:	46c0      	nop			; (mov r8, r8)
 800271a:	46bd      	mov	sp, r7
 800271c:	b002      	add	sp, #8
 800271e:	bd80      	pop	{r7, pc}

08002720 <LL_TIM_EnableIT_UPDATE>:
 8002720:	b580      	push	{r7, lr}
 8002722:	b082      	sub	sp, #8
 8002724:	af00      	add	r7, sp, #0
 8002726:	6078      	str	r0, [r7, #4]
 8002728:	687b      	ldr	r3, [r7, #4]
 800272a:	68db      	ldr	r3, [r3, #12]
 800272c:	2201      	movs	r2, #1
 800272e:	431a      	orrs	r2, r3
 8002730:	687b      	ldr	r3, [r7, #4]
 8002732:	60da      	str	r2, [r3, #12]
 8002734:	46c0      	nop			; (mov r8, r8)
 8002736:	46bd      	mov	sp, r7
 8002738:	b002      	add	sp, #8
 800273a:	bd80      	pop	{r7, pc}

0800273c <LL_APB1_GRP1_EnableClock>:
 800273c:	b580      	push	{r7, lr}
 800273e:	b084      	sub	sp, #16
 8002740:	af00      	add	r7, sp, #0
 8002742:	6078      	str	r0, [r7, #4]
 8002744:	4b07      	ldr	r3, [pc, #28]	; (8002764 <LL_APB1_GRP1_EnableClock+0x28>)
 8002746:	69d9      	ldr	r1, [r3, #28]
 8002748:	4b06      	ldr	r3, [pc, #24]	; (8002764 <LL_APB1_GRP1_EnableClock+0x28>)
 800274a:	687a      	ldr	r2, [r7, #4]
 800274c:	430a      	orrs	r2, r1
 800274e:	61da      	str	r2, [r3, #28]
 8002750:	4b04      	ldr	r3, [pc, #16]	; (8002764 <LL_APB1_GRP1_EnableClock+0x28>)
 8002752:	69db      	ldr	r3, [r3, #28]
 8002754:	687a      	ldr	r2, [r7, #4]
 8002756:	4013      	ands	r3, r2
 8002758:	60fb      	str	r3, [r7, #12]
 800275a:	68fb      	ldr	r3, [r7, #12]
 800275c:	46c0      	nop			; (mov r8, r8)
 800275e:	46bd      	mov	sp, r7
 8002760:	b004      	add	sp, #16
 8002762:	bd80      	pop	{r7, pc}
 8002764:	40021000 	.word	0x40021000

08002768 <tim_init>:
 8002768:	b580      	push	{r7, lr}
 800276a:	af00      	add	r7, sp, #0
 800276c:	2002      	movs	r0, #2
 800276e:	f7ff ffe5 	bl	800273c <LL_APB1_GRP1_EnableClock>
 8002772:	4a11      	ldr	r2, [pc, #68]	; (80027b8 <tim_init+0x50>)
 8002774:	4b11      	ldr	r3, [pc, #68]	; (80027bc <tim_init+0x54>)
 8002776:	0011      	movs	r1, r2
 8002778:	0018      	movs	r0, r3
 800277a:	f7ff ffad 	bl	80026d8 <LL_TIM_SetPrescaler>
 800277e:	4b0f      	ldr	r3, [pc, #60]	; (80027bc <tim_init+0x54>)
 8002780:	2163      	movs	r1, #99	; 0x63
 8002782:	0018      	movs	r0, r3
 8002784:	f7ff ffb4 	bl	80026f0 <LL_TIM_SetAutoReload>
 8002788:	4b0c      	ldr	r3, [pc, #48]	; (80027bc <tim_init+0x54>)
 800278a:	2100      	movs	r1, #0
 800278c:	0018      	movs	r0, r3
 800278e:	f7ff ff91 	bl	80026b4 <LL_TIM_SetCounterMode>
 8002792:	4b0a      	ldr	r3, [pc, #40]	; (80027bc <tim_init+0x54>)
 8002794:	0018      	movs	r0, r3
 8002796:	f7ff ffc3 	bl	8002720 <LL_TIM_EnableIT_UPDATE>
 800279a:	4b08      	ldr	r3, [pc, #32]	; (80027bc <tim_init+0x54>)
 800279c:	0018      	movs	r0, r3
 800279e:	f7ff ff7b 	bl	8002698 <LL_TIM_EnableCounter>
 80027a2:	2010      	movs	r0, #16
 80027a4:	f7ff fef4 	bl	8002590 <NVIC_EnableIRQ>
 80027a8:	2145      	movs	r1, #69	; 0x45
 80027aa:	2010      	movs	r0, #16
 80027ac:	f7ff ff06 	bl	80025bc <NVIC_SetPriority>
 80027b0:	46c0      	nop			; (mov r8, r8)
 80027b2:	46bd      	mov	sp, r7
 80027b4:	bd80      	pop	{r7, pc}
 80027b6:	46c0      	nop			; (mov r8, r8)
 80027b8:	0000bb7f 	.word	0x0000bb7f
 80027bc:	40000400 	.word	0x40000400

080027c0 <err_man_show_err>:
 80027c0:	b580      	push	{r7, lr}
 80027c2:	b082      	sub	sp, #8
 80027c4:	af00      	add	r7, sp, #0
 80027c6:	2300      	movs	r3, #0
 80027c8:	607b      	str	r3, [r7, #4]
 80027ca:	2000      	movs	r0, #0
 80027cc:	f7ff f888 	bl	80018e0 <disp_fill>
 80027d0:	2300      	movs	r3, #0
 80027d2:	607b      	str	r3, [r7, #4]
 80027d4:	e030      	b.n	8002838 <err_man_show_err+0x78>
 80027d6:	687b      	ldr	r3, [r7, #4]
 80027d8:	4a22      	ldr	r2, [pc, #136]	; (8002864 <err_man_show_err+0xa4>)
 80027da:	4013      	ands	r3, r2
 80027dc:	d504      	bpl.n	80027e8 <err_man_show_err+0x28>
 80027de:	3b01      	subs	r3, #1
 80027e0:	2202      	movs	r2, #2
 80027e2:	4252      	negs	r2, r2
 80027e4:	4313      	orrs	r3, r2
 80027e6:	3301      	adds	r3, #1
 80027e8:	b2db      	uxtb	r3, r3
 80027ea:	1c1a      	adds	r2, r3, #0
 80027ec:	00d2      	lsls	r2, r2, #3
 80027ee:	1ad3      	subs	r3, r2, r3
 80027f0:	b2da      	uxtb	r2, r3
 80027f2:	687b      	ldr	r3, [r7, #4]
 80027f4:	105b      	asrs	r3, r3, #1
 80027f6:	b2db      	uxtb	r3, r3
 80027f8:	0019      	movs	r1, r3
 80027fa:	0010      	movs	r0, r2
 80027fc:	f7ff f98c 	bl	8001b18 <disp_set_cursor>
 8002800:	4a19      	ldr	r2, [pc, #100]	; (8002868 <err_man_show_err+0xa8>)
 8002802:	687b      	ldr	r3, [r7, #4]
 8002804:	18d3      	adds	r3, r2, r3
 8002806:	3308      	adds	r3, #8
 8002808:	781b      	ldrb	r3, [r3, #0]
 800280a:	2bff      	cmp	r3, #255	; 0xff
 800280c:	d106      	bne.n	800281c <err_man_show_err+0x5c>
 800280e:	687a      	ldr	r2, [r7, #4]
 8002810:	4b16      	ldr	r3, [pc, #88]	; (800286c <err_man_show_err+0xac>)
 8002812:	0011      	movs	r1, r2
 8002814:	0018      	movs	r0, r3
 8002816:	f001 fb13 	bl	8003e40 <xprintf>
 800281a:	e00a      	b.n	8002832 <err_man_show_err+0x72>
 800281c:	4a12      	ldr	r2, [pc, #72]	; (8002868 <err_man_show_err+0xa8>)
 800281e:	687b      	ldr	r3, [r7, #4]
 8002820:	18d3      	adds	r3, r2, r3
 8002822:	3308      	adds	r3, #8
 8002824:	781b      	ldrb	r3, [r3, #0]
 8002826:	001a      	movs	r2, r3
 8002828:	6879      	ldr	r1, [r7, #4]
 800282a:	4b11      	ldr	r3, [pc, #68]	; (8002870 <err_man_show_err+0xb0>)
 800282c:	0018      	movs	r0, r3
 800282e:	f001 fb07 	bl	8003e40 <xprintf>
 8002832:	687b      	ldr	r3, [r7, #4]
 8002834:	3301      	adds	r3, #1
 8002836:	607b      	str	r3, [r7, #4]
 8002838:	687b      	ldr	r3, [r7, #4]
 800283a:	2b02      	cmp	r3, #2
 800283c:	ddcb      	ble.n	80027d6 <err_man_show_err+0x16>
 800283e:	2105      	movs	r1, #5
 8002840:	2000      	movs	r0, #0
 8002842:	f7ff f969 	bl	8001b18 <disp_set_cursor>
 8002846:	4b08      	ldr	r3, [pc, #32]	; (8002868 <err_man_show_err+0xa8>)
 8002848:	781b      	ldrb	r3, [r3, #0]
 800284a:	001a      	movs	r2, r3
 800284c:	4b09      	ldr	r3, [pc, #36]	; (8002874 <err_man_show_err+0xb4>)
 800284e:	0011      	movs	r1, r2
 8002850:	0018      	movs	r0, r3
 8002852:	f001 faf5 	bl	8003e40 <xprintf>
 8002856:	f7ff f857 	bl	8001908 <disp_update>
 800285a:	46c0      	nop			; (mov r8, r8)
 800285c:	46bd      	mov	sp, r7
 800285e:	b002      	add	sp, #8
 8002860:	bd80      	pop	{r7, pc}
 8002862:	46c0      	nop			; (mov r8, r8)
 8002864:	80000001 	.word	0x80000001
 8002868:	20000f44 	.word	0x20000f44
 800286c:	080097ac 	.word	0x080097ac
 8002870:	080097b4 	.word	0x080097b4
 8002874:	080097bc 	.word	0x080097bc

08002878 <err_man_set_time>:
 8002878:	b580      	push	{r7, lr}
 800287a:	b082      	sub	sp, #8
 800287c:	af00      	add	r7, sp, #0
 800287e:	6078      	str	r0, [r7, #4]
 8002880:	4b03      	ldr	r3, [pc, #12]	; (8002890 <err_man_set_time+0x18>)
 8002882:	687a      	ldr	r2, [r7, #4]
 8002884:	605a      	str	r2, [r3, #4]
 8002886:	46c0      	nop			; (mov r8, r8)
 8002888:	46bd      	mov	sp, r7
 800288a:	b002      	add	sp, #8
 800288c:	bd80      	pop	{r7, pc}
 800288e:	46c0      	nop			; (mov r8, r8)
 8002890:	20000f44 	.word	0x20000f44

08002894 <err_man_set_dist>:
 8002894:	b580      	push	{r7, lr}
 8002896:	b082      	sub	sp, #8
 8002898:	af00      	add	r7, sp, #0
 800289a:	6078      	str	r0, [r7, #4]
 800289c:	000a      	movs	r2, r1
 800289e:	1cfb      	adds	r3, r7, #3
 80028a0:	701a      	strb	r2, [r3, #0]
 80028a2:	1cfb      	adds	r3, r7, #3
 80028a4:	781a      	ldrb	r2, [r3, #0]
 80028a6:	6879      	ldr	r1, [r7, #4]
 80028a8:	4b03      	ldr	r3, [pc, #12]	; (80028b8 <err_man_set_dist+0x24>)
 80028aa:	0018      	movs	r0, r3
 80028ac:	f006 fe40 	bl	8009530 <memcpy>
 80028b0:	46c0      	nop			; (mov r8, r8)
 80028b2:	46bd      	mov	sp, r7
 80028b4:	b002      	add	sp, #8
 80028b6:	bd80      	pop	{r7, pc}
 80028b8:	20000f4c 	.word	0x20000f4c

080028bc <err_man_init>:
 80028bc:	b580      	push	{r7, lr}
 80028be:	af00      	add	r7, sp, #0
 80028c0:	4b09      	ldr	r3, [pc, #36]	; (80028e8 <err_man_init+0x2c>)
 80028c2:	2200      	movs	r2, #0
 80028c4:	701a      	strb	r2, [r3, #0]
 80028c6:	4b08      	ldr	r3, [pc, #32]	; (80028e8 <err_man_init+0x2c>)
 80028c8:	2200      	movs	r2, #0
 80028ca:	739a      	strb	r2, [r3, #14]
 80028cc:	4b06      	ldr	r3, [pc, #24]	; (80028e8 <err_man_init+0x2c>)
 80028ce:	2200      	movs	r2, #0
 80028d0:	605a      	str	r2, [r3, #4]
 80028d2:	4b05      	ldr	r3, [pc, #20]	; (80028e8 <err_man_init+0x2c>)
 80028d4:	2200      	movs	r2, #0
 80028d6:	705a      	strb	r2, [r3, #1]
 80028d8:	f7ff f948 	bl	8001b6c <disp_init>
 80028dc:	f7ff ff44 	bl	8002768 <tim_init>
 80028e0:	46c0      	nop			; (mov r8, r8)
 80028e2:	46bd      	mov	sp, r7
 80028e4:	bd80      	pop	{r7, pc}
 80028e6:	46c0      	nop			; (mov r8, r8)
 80028e8:	20000f44 	.word	0x20000f44

080028ec <er_man_disp_get>:
 80028ec:	b580      	push	{r7, lr}
 80028ee:	af00      	add	r7, sp, #0
 80028f0:	4b02      	ldr	r3, [pc, #8]	; (80028fc <er_man_disp_get+0x10>)
 80028f2:	7b9b      	ldrb	r3, [r3, #14]
 80028f4:	0018      	movs	r0, r3
 80028f6:	46bd      	mov	sp, r7
 80028f8:	bd80      	pop	{r7, pc}
 80028fa:	46c0      	nop			; (mov r8, r8)
 80028fc:	20000f44 	.word	0x20000f44

08002900 <er_man_disp_set>:
 8002900:	b580      	push	{r7, lr}
 8002902:	af00      	add	r7, sp, #0
 8002904:	4b02      	ldr	r3, [pc, #8]	; (8002910 <er_man_disp_set+0x10>)
 8002906:	2201      	movs	r2, #1
 8002908:	739a      	strb	r2, [r3, #14]
 800290a:	46c0      	nop			; (mov r8, r8)
 800290c:	46bd      	mov	sp, r7
 800290e:	bd80      	pop	{r7, pc}
 8002910:	20000f44 	.word	0x20000f44

08002914 <er_man_disp_clr>:
 8002914:	b580      	push	{r7, lr}
 8002916:	af00      	add	r7, sp, #0
 8002918:	4b02      	ldr	r3, [pc, #8]	; (8002924 <er_man_disp_clr+0x10>)
 800291a:	2200      	movs	r2, #0
 800291c:	739a      	strb	r2, [r3, #14]
 800291e:	46c0      	nop			; (mov r8, r8)
 8002920:	46bd      	mov	sp, r7
 8002922:	bd80      	pop	{r7, pc}
 8002924:	20000f44 	.word	0x20000f44

08002928 <er_man_add_err>:
 8002928:	b580      	push	{r7, lr}
 800292a:	b082      	sub	sp, #8
 800292c:	af00      	add	r7, sp, #0
 800292e:	6078      	str	r0, [r7, #4]
 8002930:	000a      	movs	r2, r1
 8002932:	1cfb      	adds	r3, r7, #3
 8002934:	701a      	strb	r2, [r3, #0]
 8002936:	4b0d      	ldr	r3, [pc, #52]	; (800296c <er_man_add_err+0x44>)
 8002938:	220b      	movs	r2, #11
 800293a:	6879      	ldr	r1, [r7, #4]
 800293c:	189b      	adds	r3, r3, r2
 800293e:	2203      	movs	r2, #3
 8002940:	0018      	movs	r0, r3
 8002942:	f006 fdf5 	bl	8009530 <memcpy>
 8002946:	4b09      	ldr	r3, [pc, #36]	; (800296c <er_man_add_err+0x44>)
 8002948:	1cfa      	adds	r2, r7, #3
 800294a:	7812      	ldrb	r2, [r2, #0]
 800294c:	709a      	strb	r2, [r3, #2]
 800294e:	1cfb      	adds	r3, r7, #3
 8002950:	781b      	ldrb	r3, [r3, #0]
 8002952:	2b00      	cmp	r3, #0
 8002954:	d006      	beq.n	8002964 <er_man_add_err+0x3c>
 8002956:	4b05      	ldr	r3, [pc, #20]	; (800296c <er_man_add_err+0x44>)
 8002958:	781b      	ldrb	r3, [r3, #0]
 800295a:	3301      	adds	r3, #1
 800295c:	b2da      	uxtb	r2, r3
 800295e:	4b03      	ldr	r3, [pc, #12]	; (800296c <er_man_add_err+0x44>)
 8002960:	701a      	strb	r2, [r3, #0]
 8002962:	46c0      	nop			; (mov r8, r8)
 8002964:	46c0      	nop			; (mov r8, r8)
 8002966:	46bd      	mov	sp, r7
 8002968:	b002      	add	sp, #8
 800296a:	bd80      	pop	{r7, pc}
 800296c:	20000f44 	.word	0x20000f44

08002970 <TIM3_IRQHandler>:
 8002970:	b580      	push	{r7, lr}
 8002972:	af00      	add	r7, sp, #0
 8002974:	f7ff ffc4 	bl	8002900 <er_man_disp_set>
 8002978:	4b03      	ldr	r3, [pc, #12]	; (8002988 <TIM3_IRQHandler+0x18>)
 800297a:	0018      	movs	r0, r3
 800297c:	f7ff fec4 	bl	8002708 <LL_TIM_ClearFlag_UPDATE>
 8002980:	46c0      	nop			; (mov r8, r8)
 8002982:	46bd      	mov	sp, r7
 8002984:	bd80      	pop	{r7, pc}
 8002986:	46c0      	nop			; (mov r8, r8)
 8002988:	40000400 	.word	0x40000400

0800298c <NMI_Handler>:
 800298c:	b580      	push	{r7, lr}
 800298e:	af00      	add	r7, sp, #0
 8002990:	46c0      	nop			; (mov r8, r8)
 8002992:	46bd      	mov	sp, r7
 8002994:	bd80      	pop	{r7, pc}

08002996 <HardFault_Handler>:
 8002996:	b580      	push	{r7, lr}
 8002998:	af00      	add	r7, sp, #0
 800299a:	e7fe      	b.n	800299a <HardFault_Handler+0x4>

0800299c <SVC_Handler>:
 800299c:	b580      	push	{r7, lr}
 800299e:	af00      	add	r7, sp, #0
 80029a0:	46c0      	nop			; (mov r8, r8)
 80029a2:	46bd      	mov	sp, r7
 80029a4:	bd80      	pop	{r7, pc}

080029a6 <PendSV_Handler>:
 80029a6:	b580      	push	{r7, lr}
 80029a8:	af00      	add	r7, sp, #0
 80029aa:	46c0      	nop			; (mov r8, r8)
 80029ac:	46bd      	mov	sp, r7
 80029ae:	bd80      	pop	{r7, pc}

080029b0 <NVIC_EnableIRQ>:
 80029b0:	b580      	push	{r7, lr}
 80029b2:	b082      	sub	sp, #8
 80029b4:	af00      	add	r7, sp, #0
 80029b6:	0002      	movs	r2, r0
 80029b8:	1dfb      	adds	r3, r7, #7
 80029ba:	701a      	strb	r2, [r3, #0]
 80029bc:	1dfb      	adds	r3, r7, #7
 80029be:	781b      	ldrb	r3, [r3, #0]
 80029c0:	001a      	movs	r2, r3
 80029c2:	231f      	movs	r3, #31
 80029c4:	401a      	ands	r2, r3
 80029c6:	4b04      	ldr	r3, [pc, #16]	; (80029d8 <NVIC_EnableIRQ+0x28>)
 80029c8:	2101      	movs	r1, #1
 80029ca:	4091      	lsls	r1, r2
 80029cc:	000a      	movs	r2, r1
 80029ce:	601a      	str	r2, [r3, #0]
 80029d0:	46c0      	nop			; (mov r8, r8)
 80029d2:	46bd      	mov	sp, r7
 80029d4:	b002      	add	sp, #8
 80029d6:	bd80      	pop	{r7, pc}
 80029d8:	e000e100 	.word	0xe000e100

080029dc <NVIC_DisableIRQ>:
 80029dc:	b580      	push	{r7, lr}
 80029de:	b082      	sub	sp, #8
 80029e0:	af00      	add	r7, sp, #0
 80029e2:	0002      	movs	r2, r0
 80029e4:	1dfb      	adds	r3, r7, #7
 80029e6:	701a      	strb	r2, [r3, #0]
 80029e8:	1dfb      	adds	r3, r7, #7
 80029ea:	781b      	ldrb	r3, [r3, #0]
 80029ec:	001a      	movs	r2, r3
 80029ee:	231f      	movs	r3, #31
 80029f0:	4013      	ands	r3, r2
 80029f2:	4905      	ldr	r1, [pc, #20]	; (8002a08 <NVIC_DisableIRQ+0x2c>)
 80029f4:	2201      	movs	r2, #1
 80029f6:	409a      	lsls	r2, r3
 80029f8:	0013      	movs	r3, r2
 80029fa:	2280      	movs	r2, #128	; 0x80
 80029fc:	508b      	str	r3, [r1, r2]
 80029fe:	46c0      	nop			; (mov r8, r8)
 8002a00:	46bd      	mov	sp, r7
 8002a02:	b002      	add	sp, #8
 8002a04:	bd80      	pop	{r7, pc}
 8002a06:	46c0      	nop			; (mov r8, r8)
 8002a08:	e000e100 	.word	0xe000e100

08002a0c <NVIC_SetPriority>:
 8002a0c:	b590      	push	{r4, r7, lr}
 8002a0e:	b083      	sub	sp, #12
 8002a10:	af00      	add	r7, sp, #0
 8002a12:	0002      	movs	r2, r0
 8002a14:	6039      	str	r1, [r7, #0]
 8002a16:	1dfb      	adds	r3, r7, #7
 8002a18:	701a      	strb	r2, [r3, #0]
 8002a1a:	1dfb      	adds	r3, r7, #7
 8002a1c:	781b      	ldrb	r3, [r3, #0]
 8002a1e:	2b7f      	cmp	r3, #127	; 0x7f
 8002a20:	d932      	bls.n	8002a88 <NVIC_SetPriority+0x7c>
 8002a22:	4a2f      	ldr	r2, [pc, #188]	; (8002ae0 <NVIC_SetPriority+0xd4>)
 8002a24:	1dfb      	adds	r3, r7, #7
 8002a26:	781b      	ldrb	r3, [r3, #0]
 8002a28:	0019      	movs	r1, r3
 8002a2a:	230f      	movs	r3, #15
 8002a2c:	400b      	ands	r3, r1
 8002a2e:	3b08      	subs	r3, #8
 8002a30:	089b      	lsrs	r3, r3, #2
 8002a32:	3306      	adds	r3, #6
 8002a34:	009b      	lsls	r3, r3, #2
 8002a36:	18d3      	adds	r3, r2, r3
 8002a38:	3304      	adds	r3, #4
 8002a3a:	681b      	ldr	r3, [r3, #0]
 8002a3c:	1dfa      	adds	r2, r7, #7
 8002a3e:	7812      	ldrb	r2, [r2, #0]
 8002a40:	0011      	movs	r1, r2
 8002a42:	2203      	movs	r2, #3
 8002a44:	400a      	ands	r2, r1
 8002a46:	00d2      	lsls	r2, r2, #3
 8002a48:	21ff      	movs	r1, #255	; 0xff
 8002a4a:	4091      	lsls	r1, r2
 8002a4c:	000a      	movs	r2, r1
 8002a4e:	43d2      	mvns	r2, r2
 8002a50:	401a      	ands	r2, r3
 8002a52:	0011      	movs	r1, r2
 8002a54:	683b      	ldr	r3, [r7, #0]
 8002a56:	019b      	lsls	r3, r3, #6
 8002a58:	22ff      	movs	r2, #255	; 0xff
 8002a5a:	401a      	ands	r2, r3
 8002a5c:	1dfb      	adds	r3, r7, #7
 8002a5e:	781b      	ldrb	r3, [r3, #0]
 8002a60:	0018      	movs	r0, r3
 8002a62:	2303      	movs	r3, #3
 8002a64:	4003      	ands	r3, r0
 8002a66:	00db      	lsls	r3, r3, #3
 8002a68:	409a      	lsls	r2, r3
 8002a6a:	481d      	ldr	r0, [pc, #116]	; (8002ae0 <NVIC_SetPriority+0xd4>)
 8002a6c:	1dfb      	adds	r3, r7, #7
 8002a6e:	781b      	ldrb	r3, [r3, #0]
 8002a70:	001c      	movs	r4, r3
 8002a72:	230f      	movs	r3, #15
 8002a74:	4023      	ands	r3, r4
 8002a76:	3b08      	subs	r3, #8
 8002a78:	089b      	lsrs	r3, r3, #2
 8002a7a:	430a      	orrs	r2, r1
 8002a7c:	3306      	adds	r3, #6
 8002a7e:	009b      	lsls	r3, r3, #2
 8002a80:	18c3      	adds	r3, r0, r3
 8002a82:	3304      	adds	r3, #4
 8002a84:	601a      	str	r2, [r3, #0]
 8002a86:	e027      	b.n	8002ad8 <NVIC_SetPriority+0xcc>
 8002a88:	4a16      	ldr	r2, [pc, #88]	; (8002ae4 <NVIC_SetPriority+0xd8>)
 8002a8a:	1dfb      	adds	r3, r7, #7
 8002a8c:	781b      	ldrb	r3, [r3, #0]
 8002a8e:	b25b      	sxtb	r3, r3
 8002a90:	089b      	lsrs	r3, r3, #2
 8002a92:	33c0      	adds	r3, #192	; 0xc0
 8002a94:	009b      	lsls	r3, r3, #2
 8002a96:	589b      	ldr	r3, [r3, r2]
 8002a98:	1dfa      	adds	r2, r7, #7
 8002a9a:	7812      	ldrb	r2, [r2, #0]
 8002a9c:	0011      	movs	r1, r2
 8002a9e:	2203      	movs	r2, #3
 8002aa0:	400a      	ands	r2, r1
 8002aa2:	00d2      	lsls	r2, r2, #3
 8002aa4:	21ff      	movs	r1, #255	; 0xff
 8002aa6:	4091      	lsls	r1, r2
 8002aa8:	000a      	movs	r2, r1
 8002aaa:	43d2      	mvns	r2, r2
 8002aac:	401a      	ands	r2, r3
 8002aae:	0011      	movs	r1, r2
 8002ab0:	683b      	ldr	r3, [r7, #0]
 8002ab2:	019b      	lsls	r3, r3, #6
 8002ab4:	22ff      	movs	r2, #255	; 0xff
 8002ab6:	401a      	ands	r2, r3
 8002ab8:	1dfb      	adds	r3, r7, #7
 8002aba:	781b      	ldrb	r3, [r3, #0]
 8002abc:	0018      	movs	r0, r3
 8002abe:	2303      	movs	r3, #3
 8002ac0:	4003      	ands	r3, r0
 8002ac2:	00db      	lsls	r3, r3, #3
 8002ac4:	409a      	lsls	r2, r3
 8002ac6:	4807      	ldr	r0, [pc, #28]	; (8002ae4 <NVIC_SetPriority+0xd8>)
 8002ac8:	1dfb      	adds	r3, r7, #7
 8002aca:	781b      	ldrb	r3, [r3, #0]
 8002acc:	b25b      	sxtb	r3, r3
 8002ace:	089b      	lsrs	r3, r3, #2
 8002ad0:	430a      	orrs	r2, r1
 8002ad2:	33c0      	adds	r3, #192	; 0xc0
 8002ad4:	009b      	lsls	r3, r3, #2
 8002ad6:	501a      	str	r2, [r3, r0]
 8002ad8:	46c0      	nop			; (mov r8, r8)
 8002ada:	46bd      	mov	sp, r7
 8002adc:	b003      	add	sp, #12
 8002ade:	bd90      	pop	{r4, r7, pc}
 8002ae0:	e000ed00 	.word	0xe000ed00
 8002ae4:	e000e100 	.word	0xe000e100

08002ae8 <LL_USART_Enable>:
 8002ae8:	b580      	push	{r7, lr}
 8002aea:	b082      	sub	sp, #8
 8002aec:	af00      	add	r7, sp, #0
 8002aee:	6078      	str	r0, [r7, #4]
 8002af0:	687b      	ldr	r3, [r7, #4]
 8002af2:	681b      	ldr	r3, [r3, #0]
 8002af4:	2201      	movs	r2, #1
 8002af6:	431a      	orrs	r2, r3
 8002af8:	687b      	ldr	r3, [r7, #4]
 8002afa:	601a      	str	r2, [r3, #0]
 8002afc:	46c0      	nop			; (mov r8, r8)
 8002afe:	46bd      	mov	sp, r7
 8002b00:	b002      	add	sp, #8
 8002b02:	bd80      	pop	{r7, pc}

08002b04 <LL_USART_SetTransferDirection>:
 8002b04:	b580      	push	{r7, lr}
 8002b06:	b082      	sub	sp, #8
 8002b08:	af00      	add	r7, sp, #0
 8002b0a:	6078      	str	r0, [r7, #4]
 8002b0c:	6039      	str	r1, [r7, #0]
 8002b0e:	687b      	ldr	r3, [r7, #4]
 8002b10:	681b      	ldr	r3, [r3, #0]
 8002b12:	220c      	movs	r2, #12
 8002b14:	4393      	bics	r3, r2
 8002b16:	001a      	movs	r2, r3
 8002b18:	683b      	ldr	r3, [r7, #0]
 8002b1a:	431a      	orrs	r2, r3
 8002b1c:	687b      	ldr	r3, [r7, #4]
 8002b1e:	601a      	str	r2, [r3, #0]
 8002b20:	46c0      	nop			; (mov r8, r8)
 8002b22:	46bd      	mov	sp, r7
 8002b24:	b002      	add	sp, #8
 8002b26:	bd80      	pop	{r7, pc}

08002b28 <LL_USART_SetParity>:
 8002b28:	b580      	push	{r7, lr}
 8002b2a:	b082      	sub	sp, #8
 8002b2c:	af00      	add	r7, sp, #0
 8002b2e:	6078      	str	r0, [r7, #4]
 8002b30:	6039      	str	r1, [r7, #0]
 8002b32:	687b      	ldr	r3, [r7, #4]
 8002b34:	681b      	ldr	r3, [r3, #0]
 8002b36:	4a05      	ldr	r2, [pc, #20]	; (8002b4c <LL_USART_SetParity+0x24>)
 8002b38:	401a      	ands	r2, r3
 8002b3a:	683b      	ldr	r3, [r7, #0]
 8002b3c:	431a      	orrs	r2, r3
 8002b3e:	687b      	ldr	r3, [r7, #4]
 8002b40:	601a      	str	r2, [r3, #0]
 8002b42:	46c0      	nop			; (mov r8, r8)
 8002b44:	46bd      	mov	sp, r7
 8002b46:	b002      	add	sp, #8
 8002b48:	bd80      	pop	{r7, pc}
 8002b4a:	46c0      	nop			; (mov r8, r8)
 8002b4c:	fffff9ff 	.word	0xfffff9ff

08002b50 <LL_USART_SetDataWidth>:
 8002b50:	b580      	push	{r7, lr}
 8002b52:	b082      	sub	sp, #8
 8002b54:	af00      	add	r7, sp, #0
 8002b56:	6078      	str	r0, [r7, #4]
 8002b58:	6039      	str	r1, [r7, #0]
 8002b5a:	687b      	ldr	r3, [r7, #4]
 8002b5c:	681b      	ldr	r3, [r3, #0]
 8002b5e:	4a05      	ldr	r2, [pc, #20]	; (8002b74 <LL_USART_SetDataWidth+0x24>)
 8002b60:	401a      	ands	r2, r3
 8002b62:	683b      	ldr	r3, [r7, #0]
 8002b64:	431a      	orrs	r2, r3
 8002b66:	687b      	ldr	r3, [r7, #4]
 8002b68:	601a      	str	r2, [r3, #0]
 8002b6a:	46c0      	nop			; (mov r8, r8)
 8002b6c:	46bd      	mov	sp, r7
 8002b6e:	b002      	add	sp, #8
 8002b70:	bd80      	pop	{r7, pc}
 8002b72:	46c0      	nop			; (mov r8, r8)
 8002b74:	ffffefff 	.word	0xffffefff

08002b78 <LL_USART_SetStopBitsLength>:
 8002b78:	b580      	push	{r7, lr}
 8002b7a:	b082      	sub	sp, #8
 8002b7c:	af00      	add	r7, sp, #0
 8002b7e:	6078      	str	r0, [r7, #4]
 8002b80:	6039      	str	r1, [r7, #0]
 8002b82:	687b      	ldr	r3, [r7, #4]
 8002b84:	685b      	ldr	r3, [r3, #4]
 8002b86:	4a05      	ldr	r2, [pc, #20]	; (8002b9c <LL_USART_SetStopBitsLength+0x24>)
 8002b88:	401a      	ands	r2, r3
 8002b8a:	683b      	ldr	r3, [r7, #0]
 8002b8c:	431a      	orrs	r2, r3
 8002b8e:	687b      	ldr	r3, [r7, #4]
 8002b90:	605a      	str	r2, [r3, #4]
 8002b92:	46c0      	nop			; (mov r8, r8)
 8002b94:	46bd      	mov	sp, r7
 8002b96:	b002      	add	sp, #8
 8002b98:	bd80      	pop	{r7, pc}
 8002b9a:	46c0      	nop			; (mov r8, r8)
 8002b9c:	ffffcfff 	.word	0xffffcfff

08002ba0 <LL_USART_SetBaudRate>:
 8002ba0:	b5b0      	push	{r4, r5, r7, lr}
 8002ba2:	b084      	sub	sp, #16
 8002ba4:	af00      	add	r7, sp, #0
 8002ba6:	60f8      	str	r0, [r7, #12]
 8002ba8:	60b9      	str	r1, [r7, #8]
 8002baa:	607a      	str	r2, [r7, #4]
 8002bac:	603b      	str	r3, [r7, #0]
 8002bae:	2500      	movs	r5, #0
 8002bb0:	2400      	movs	r4, #0
 8002bb2:	687a      	ldr	r2, [r7, #4]
 8002bb4:	2380      	movs	r3, #128	; 0x80
 8002bb6:	021b      	lsls	r3, r3, #8
 8002bb8:	429a      	cmp	r2, r3
 8002bba:	d117      	bne.n	8002bec <LL_USART_SetBaudRate+0x4c>
 8002bbc:	68bb      	ldr	r3, [r7, #8]
 8002bbe:	005a      	lsls	r2, r3, #1
 8002bc0:	683b      	ldr	r3, [r7, #0]
 8002bc2:	085b      	lsrs	r3, r3, #1
 8002bc4:	18d3      	adds	r3, r2, r3
 8002bc6:	6839      	ldr	r1, [r7, #0]
 8002bc8:	0018      	movs	r0, r3
 8002bca:	f7fd fa9d 	bl	8000108 <__udivsi3>
 8002bce:	0003      	movs	r3, r0
 8002bd0:	b29b      	uxth	r3, r3
 8002bd2:	001d      	movs	r5, r3
 8002bd4:	4b0e      	ldr	r3, [pc, #56]	; (8002c10 <LL_USART_SetBaudRate+0x70>)
 8002bd6:	402b      	ands	r3, r5
 8002bd8:	001c      	movs	r4, r3
 8002bda:	086b      	lsrs	r3, r5, #1
 8002bdc:	b29b      	uxth	r3, r3
 8002bde:	001a      	movs	r2, r3
 8002be0:	2307      	movs	r3, #7
 8002be2:	4013      	ands	r3, r2
 8002be4:	431c      	orrs	r4, r3
 8002be6:	68fb      	ldr	r3, [r7, #12]
 8002be8:	60dc      	str	r4, [r3, #12]
 8002bea:	e00c      	b.n	8002c06 <LL_USART_SetBaudRate+0x66>
 8002bec:	683b      	ldr	r3, [r7, #0]
 8002bee:	085a      	lsrs	r2, r3, #1
 8002bf0:	68bb      	ldr	r3, [r7, #8]
 8002bf2:	18d3      	adds	r3, r2, r3
 8002bf4:	6839      	ldr	r1, [r7, #0]
 8002bf6:	0018      	movs	r0, r3
 8002bf8:	f7fd fa86 	bl	8000108 <__udivsi3>
 8002bfc:	0003      	movs	r3, r0
 8002bfe:	b29b      	uxth	r3, r3
 8002c00:	001a      	movs	r2, r3
 8002c02:	68fb      	ldr	r3, [r7, #12]
 8002c04:	60da      	str	r2, [r3, #12]
 8002c06:	46c0      	nop			; (mov r8, r8)
 8002c08:	46bd      	mov	sp, r7
 8002c0a:	b004      	add	sp, #16
 8002c0c:	bdb0      	pop	{r4, r5, r7, pc}
 8002c0e:	46c0      	nop			; (mov r8, r8)
 8002c10:	0000fff0 	.word	0x0000fff0

08002c14 <LL_USART_IsActiveFlag_TC>:
 8002c14:	b580      	push	{r7, lr}
 8002c16:	b082      	sub	sp, #8
 8002c18:	af00      	add	r7, sp, #0
 8002c1a:	6078      	str	r0, [r7, #4]
 8002c1c:	687b      	ldr	r3, [r7, #4]
 8002c1e:	69db      	ldr	r3, [r3, #28]
 8002c20:	2240      	movs	r2, #64	; 0x40
 8002c22:	4013      	ands	r3, r2
 8002c24:	3b40      	subs	r3, #64	; 0x40
 8002c26:	425a      	negs	r2, r3
 8002c28:	4153      	adcs	r3, r2
 8002c2a:	b2db      	uxtb	r3, r3
 8002c2c:	0018      	movs	r0, r3
 8002c2e:	46bd      	mov	sp, r7
 8002c30:	b002      	add	sp, #8
 8002c32:	bd80      	pop	{r7, pc}

08002c34 <LL_USART_IsActiveFlag_TXE>:
 8002c34:	b580      	push	{r7, lr}
 8002c36:	b082      	sub	sp, #8
 8002c38:	af00      	add	r7, sp, #0
 8002c3a:	6078      	str	r0, [r7, #4]
 8002c3c:	687b      	ldr	r3, [r7, #4]
 8002c3e:	69db      	ldr	r3, [r3, #28]
 8002c40:	2280      	movs	r2, #128	; 0x80
 8002c42:	4013      	ands	r3, r2
 8002c44:	3b80      	subs	r3, #128	; 0x80
 8002c46:	425a      	negs	r2, r3
 8002c48:	4153      	adcs	r3, r2
 8002c4a:	b2db      	uxtb	r3, r3
 8002c4c:	0018      	movs	r0, r3
 8002c4e:	46bd      	mov	sp, r7
 8002c50:	b002      	add	sp, #8
 8002c52:	bd80      	pop	{r7, pc}

08002c54 <LL_USART_ClearFlag_TC>:
 8002c54:	b580      	push	{r7, lr}
 8002c56:	b082      	sub	sp, #8
 8002c58:	af00      	add	r7, sp, #0
 8002c5a:	6078      	str	r0, [r7, #4]
 8002c5c:	687b      	ldr	r3, [r7, #4]
 8002c5e:	2240      	movs	r2, #64	; 0x40
 8002c60:	621a      	str	r2, [r3, #32]
 8002c62:	46c0      	nop			; (mov r8, r8)
 8002c64:	46bd      	mov	sp, r7
 8002c66:	b002      	add	sp, #8
 8002c68:	bd80      	pop	{r7, pc}

08002c6a <LL_USART_EnableDMAReq_RX>:
 8002c6a:	b580      	push	{r7, lr}
 8002c6c:	b082      	sub	sp, #8
 8002c6e:	af00      	add	r7, sp, #0
 8002c70:	6078      	str	r0, [r7, #4]
 8002c72:	687b      	ldr	r3, [r7, #4]
 8002c74:	689b      	ldr	r3, [r3, #8]
 8002c76:	2240      	movs	r2, #64	; 0x40
 8002c78:	431a      	orrs	r2, r3
 8002c7a:	687b      	ldr	r3, [r7, #4]
 8002c7c:	609a      	str	r2, [r3, #8]
 8002c7e:	46c0      	nop			; (mov r8, r8)
 8002c80:	46bd      	mov	sp, r7
 8002c82:	b002      	add	sp, #8
 8002c84:	bd80      	pop	{r7, pc}

08002c86 <LL_USART_TransmitData8>:
 8002c86:	b580      	push	{r7, lr}
 8002c88:	b082      	sub	sp, #8
 8002c8a:	af00      	add	r7, sp, #0
 8002c8c:	6078      	str	r0, [r7, #4]
 8002c8e:	000a      	movs	r2, r1
 8002c90:	1cfb      	adds	r3, r7, #3
 8002c92:	701a      	strb	r2, [r3, #0]
 8002c94:	1cfb      	adds	r3, r7, #3
 8002c96:	781b      	ldrb	r3, [r3, #0]
 8002c98:	b29a      	uxth	r2, r3
 8002c9a:	687b      	ldr	r3, [r7, #4]
 8002c9c:	851a      	strh	r2, [r3, #40]	; 0x28
 8002c9e:	46c0      	nop			; (mov r8, r8)
 8002ca0:	46bd      	mov	sp, r7
 8002ca2:	b002      	add	sp, #8
 8002ca4:	bd80      	pop	{r7, pc}
	...

08002ca8 <LL_DMA_EnableChannel>:
 8002ca8:	b580      	push	{r7, lr}
 8002caa:	b082      	sub	sp, #8
 8002cac:	af00      	add	r7, sp, #0
 8002cae:	6078      	str	r0, [r7, #4]
 8002cb0:	6039      	str	r1, [r7, #0]
 8002cb2:	683b      	ldr	r3, [r7, #0]
 8002cb4:	3b01      	subs	r3, #1
 8002cb6:	4a0a      	ldr	r2, [pc, #40]	; (8002ce0 <LL_DMA_EnableChannel+0x38>)
 8002cb8:	5cd3      	ldrb	r3, [r2, r3]
 8002cba:	001a      	movs	r2, r3
 8002cbc:	687b      	ldr	r3, [r7, #4]
 8002cbe:	18d3      	adds	r3, r2, r3
 8002cc0:	681a      	ldr	r2, [r3, #0]
 8002cc2:	683b      	ldr	r3, [r7, #0]
 8002cc4:	3b01      	subs	r3, #1
 8002cc6:	4906      	ldr	r1, [pc, #24]	; (8002ce0 <LL_DMA_EnableChannel+0x38>)
 8002cc8:	5ccb      	ldrb	r3, [r1, r3]
 8002cca:	0019      	movs	r1, r3
 8002ccc:	687b      	ldr	r3, [r7, #4]
 8002cce:	18cb      	adds	r3, r1, r3
 8002cd0:	2101      	movs	r1, #1
 8002cd2:	430a      	orrs	r2, r1
 8002cd4:	601a      	str	r2, [r3, #0]
 8002cd6:	46c0      	nop			; (mov r8, r8)
 8002cd8:	46bd      	mov	sp, r7
 8002cda:	b002      	add	sp, #8
 8002cdc:	bd80      	pop	{r7, pc}
 8002cde:	46c0      	nop			; (mov r8, r8)
 8002ce0:	08009f38 	.word	0x08009f38

08002ce4 <LL_DMA_SetMode>:
 8002ce4:	b580      	push	{r7, lr}
 8002ce6:	b084      	sub	sp, #16
 8002ce8:	af00      	add	r7, sp, #0
 8002cea:	60f8      	str	r0, [r7, #12]
 8002cec:	60b9      	str	r1, [r7, #8]
 8002cee:	607a      	str	r2, [r7, #4]
 8002cf0:	68bb      	ldr	r3, [r7, #8]
 8002cf2:	3b01      	subs	r3, #1
 8002cf4:	4a0b      	ldr	r2, [pc, #44]	; (8002d24 <LL_DMA_SetMode+0x40>)
 8002cf6:	5cd3      	ldrb	r3, [r2, r3]
 8002cf8:	001a      	movs	r2, r3
 8002cfa:	68fb      	ldr	r3, [r7, #12]
 8002cfc:	18d3      	adds	r3, r2, r3
 8002cfe:	681b      	ldr	r3, [r3, #0]
 8002d00:	2220      	movs	r2, #32
 8002d02:	4393      	bics	r3, r2
 8002d04:	0019      	movs	r1, r3
 8002d06:	68bb      	ldr	r3, [r7, #8]
 8002d08:	3b01      	subs	r3, #1
 8002d0a:	4a06      	ldr	r2, [pc, #24]	; (8002d24 <LL_DMA_SetMode+0x40>)
 8002d0c:	5cd3      	ldrb	r3, [r2, r3]
 8002d0e:	001a      	movs	r2, r3
 8002d10:	68fb      	ldr	r3, [r7, #12]
 8002d12:	18d3      	adds	r3, r2, r3
 8002d14:	687a      	ldr	r2, [r7, #4]
 8002d16:	430a      	orrs	r2, r1
 8002d18:	601a      	str	r2, [r3, #0]
 8002d1a:	46c0      	nop			; (mov r8, r8)
 8002d1c:	46bd      	mov	sp, r7
 8002d1e:	b004      	add	sp, #16
 8002d20:	bd80      	pop	{r7, pc}
 8002d22:	46c0      	nop			; (mov r8, r8)
 8002d24:	08009f38 	.word	0x08009f38

08002d28 <LL_DMA_SetMemoryIncMode>:
 8002d28:	b580      	push	{r7, lr}
 8002d2a:	b084      	sub	sp, #16
 8002d2c:	af00      	add	r7, sp, #0
 8002d2e:	60f8      	str	r0, [r7, #12]
 8002d30:	60b9      	str	r1, [r7, #8]
 8002d32:	607a      	str	r2, [r7, #4]
 8002d34:	68bb      	ldr	r3, [r7, #8]
 8002d36:	3b01      	subs	r3, #1
 8002d38:	4a0b      	ldr	r2, [pc, #44]	; (8002d68 <LL_DMA_SetMemoryIncMode+0x40>)
 8002d3a:	5cd3      	ldrb	r3, [r2, r3]
 8002d3c:	001a      	movs	r2, r3
 8002d3e:	68fb      	ldr	r3, [r7, #12]
 8002d40:	18d3      	adds	r3, r2, r3
 8002d42:	681b      	ldr	r3, [r3, #0]
 8002d44:	2280      	movs	r2, #128	; 0x80
 8002d46:	4393      	bics	r3, r2
 8002d48:	0019      	movs	r1, r3
 8002d4a:	68bb      	ldr	r3, [r7, #8]
 8002d4c:	3b01      	subs	r3, #1
 8002d4e:	4a06      	ldr	r2, [pc, #24]	; (8002d68 <LL_DMA_SetMemoryIncMode+0x40>)
 8002d50:	5cd3      	ldrb	r3, [r2, r3]
 8002d52:	001a      	movs	r2, r3
 8002d54:	68fb      	ldr	r3, [r7, #12]
 8002d56:	18d3      	adds	r3, r2, r3
 8002d58:	687a      	ldr	r2, [r7, #4]
 8002d5a:	430a      	orrs	r2, r1
 8002d5c:	601a      	str	r2, [r3, #0]
 8002d5e:	46c0      	nop			; (mov r8, r8)
 8002d60:	46bd      	mov	sp, r7
 8002d62:	b004      	add	sp, #16
 8002d64:	bd80      	pop	{r7, pc}
 8002d66:	46c0      	nop			; (mov r8, r8)
 8002d68:	08009f38 	.word	0x08009f38

08002d6c <LL_DMA_SetDataLength>:
 8002d6c:	b580      	push	{r7, lr}
 8002d6e:	b084      	sub	sp, #16
 8002d70:	af00      	add	r7, sp, #0
 8002d72:	60f8      	str	r0, [r7, #12]
 8002d74:	60b9      	str	r1, [r7, #8]
 8002d76:	607a      	str	r2, [r7, #4]
 8002d78:	68bb      	ldr	r3, [r7, #8]
 8002d7a:	3b01      	subs	r3, #1
 8002d7c:	4a0a      	ldr	r2, [pc, #40]	; (8002da8 <LL_DMA_SetDataLength+0x3c>)
 8002d7e:	5cd3      	ldrb	r3, [r2, r3]
 8002d80:	001a      	movs	r2, r3
 8002d82:	68fb      	ldr	r3, [r7, #12]
 8002d84:	18d3      	adds	r3, r2, r3
 8002d86:	685b      	ldr	r3, [r3, #4]
 8002d88:	0c1b      	lsrs	r3, r3, #16
 8002d8a:	0419      	lsls	r1, r3, #16
 8002d8c:	68bb      	ldr	r3, [r7, #8]
 8002d8e:	3b01      	subs	r3, #1
 8002d90:	4a05      	ldr	r2, [pc, #20]	; (8002da8 <LL_DMA_SetDataLength+0x3c>)
 8002d92:	5cd3      	ldrb	r3, [r2, r3]
 8002d94:	001a      	movs	r2, r3
 8002d96:	68fb      	ldr	r3, [r7, #12]
 8002d98:	18d3      	adds	r3, r2, r3
 8002d9a:	687a      	ldr	r2, [r7, #4]
 8002d9c:	430a      	orrs	r2, r1
 8002d9e:	605a      	str	r2, [r3, #4]
 8002da0:	46c0      	nop			; (mov r8, r8)
 8002da2:	46bd      	mov	sp, r7
 8002da4:	b004      	add	sp, #16
 8002da6:	bd80      	pop	{r7, pc}
 8002da8:	08009f38 	.word	0x08009f38

08002dac <LL_DMA_ConfigAddresses>:
 8002dac:	b580      	push	{r7, lr}
 8002dae:	b084      	sub	sp, #16
 8002db0:	af00      	add	r7, sp, #0
 8002db2:	60f8      	str	r0, [r7, #12]
 8002db4:	60b9      	str	r1, [r7, #8]
 8002db6:	607a      	str	r2, [r7, #4]
 8002db8:	603b      	str	r3, [r7, #0]
 8002dba:	69bb      	ldr	r3, [r7, #24]
 8002dbc:	2b10      	cmp	r3, #16
 8002dbe:	d112      	bne.n	8002de6 <LL_DMA_ConfigAddresses+0x3a>
 8002dc0:	68bb      	ldr	r3, [r7, #8]
 8002dc2:	3b01      	subs	r3, #1
 8002dc4:	4a13      	ldr	r2, [pc, #76]	; (8002e14 <LL_DMA_ConfigAddresses+0x68>)
 8002dc6:	5cd3      	ldrb	r3, [r2, r3]
 8002dc8:	001a      	movs	r2, r3
 8002dca:	68fb      	ldr	r3, [r7, #12]
 8002dcc:	18d3      	adds	r3, r2, r3
 8002dce:	687a      	ldr	r2, [r7, #4]
 8002dd0:	60da      	str	r2, [r3, #12]
 8002dd2:	68bb      	ldr	r3, [r7, #8]
 8002dd4:	3b01      	subs	r3, #1
 8002dd6:	4a0f      	ldr	r2, [pc, #60]	; (8002e14 <LL_DMA_ConfigAddresses+0x68>)
 8002dd8:	5cd3      	ldrb	r3, [r2, r3]
 8002dda:	001a      	movs	r2, r3
 8002ddc:	68fb      	ldr	r3, [r7, #12]
 8002dde:	18d3      	adds	r3, r2, r3
 8002de0:	683a      	ldr	r2, [r7, #0]
 8002de2:	609a      	str	r2, [r3, #8]
 8002de4:	e011      	b.n	8002e0a <LL_DMA_ConfigAddresses+0x5e>
 8002de6:	68bb      	ldr	r3, [r7, #8]
 8002de8:	3b01      	subs	r3, #1
 8002dea:	4a0a      	ldr	r2, [pc, #40]	; (8002e14 <LL_DMA_ConfigAddresses+0x68>)
 8002dec:	5cd3      	ldrb	r3, [r2, r3]
 8002dee:	001a      	movs	r2, r3
 8002df0:	68fb      	ldr	r3, [r7, #12]
 8002df2:	18d3      	adds	r3, r2, r3
 8002df4:	687a      	ldr	r2, [r7, #4]
 8002df6:	609a      	str	r2, [r3, #8]
 8002df8:	68bb      	ldr	r3, [r7, #8]
 8002dfa:	3b01      	subs	r3, #1
 8002dfc:	4a05      	ldr	r2, [pc, #20]	; (8002e14 <LL_DMA_ConfigAddresses+0x68>)
 8002dfe:	5cd3      	ldrb	r3, [r2, r3]
 8002e00:	001a      	movs	r2, r3
 8002e02:	68fb      	ldr	r3, [r7, #12]
 8002e04:	18d3      	adds	r3, r2, r3
 8002e06:	683a      	ldr	r2, [r7, #0]
 8002e08:	60da      	str	r2, [r3, #12]
 8002e0a:	46c0      	nop			; (mov r8, r8)
 8002e0c:	46bd      	mov	sp, r7
 8002e0e:	b004      	add	sp, #16
 8002e10:	bd80      	pop	{r7, pc}
 8002e12:	46c0      	nop			; (mov r8, r8)
 8002e14:	08009f38 	.word	0x08009f38

08002e18 <LL_DMA_IsActiveFlag_TC5>:
 8002e18:	b580      	push	{r7, lr}
 8002e1a:	b082      	sub	sp, #8
 8002e1c:	af00      	add	r7, sp, #0
 8002e1e:	6078      	str	r0, [r7, #4]
 8002e20:	687b      	ldr	r3, [r7, #4]
 8002e22:	681a      	ldr	r2, [r3, #0]
 8002e24:	2380      	movs	r3, #128	; 0x80
 8002e26:	029b      	lsls	r3, r3, #10
 8002e28:	4013      	ands	r3, r2
 8002e2a:	4a05      	ldr	r2, [pc, #20]	; (8002e40 <LL_DMA_IsActiveFlag_TC5+0x28>)
 8002e2c:	4694      	mov	ip, r2
 8002e2e:	4463      	add	r3, ip
 8002e30:	425a      	negs	r2, r3
 8002e32:	4153      	adcs	r3, r2
 8002e34:	b2db      	uxtb	r3, r3
 8002e36:	0018      	movs	r0, r3
 8002e38:	46bd      	mov	sp, r7
 8002e3a:	b002      	add	sp, #8
 8002e3c:	bd80      	pop	{r7, pc}
 8002e3e:	46c0      	nop			; (mov r8, r8)
 8002e40:	fffe0000 	.word	0xfffe0000

08002e44 <LL_DMA_ClearFlag_TC5>:
 8002e44:	b580      	push	{r7, lr}
 8002e46:	b082      	sub	sp, #8
 8002e48:	af00      	add	r7, sp, #0
 8002e4a:	6078      	str	r0, [r7, #4]
 8002e4c:	687b      	ldr	r3, [r7, #4]
 8002e4e:	2280      	movs	r2, #128	; 0x80
 8002e50:	0292      	lsls	r2, r2, #10
 8002e52:	605a      	str	r2, [r3, #4]
 8002e54:	46c0      	nop			; (mov r8, r8)
 8002e56:	46bd      	mov	sp, r7
 8002e58:	b002      	add	sp, #8
 8002e5a:	bd80      	pop	{r7, pc}

08002e5c <LL_DMA_EnableIT_TC>:
 8002e5c:	b580      	push	{r7, lr}
 8002e5e:	b082      	sub	sp, #8
 8002e60:	af00      	add	r7, sp, #0
 8002e62:	6078      	str	r0, [r7, #4]
 8002e64:	6039      	str	r1, [r7, #0]
 8002e66:	683b      	ldr	r3, [r7, #0]
 8002e68:	3b01      	subs	r3, #1
 8002e6a:	4a0a      	ldr	r2, [pc, #40]	; (8002e94 <LL_DMA_EnableIT_TC+0x38>)
 8002e6c:	5cd3      	ldrb	r3, [r2, r3]
 8002e6e:	001a      	movs	r2, r3
 8002e70:	687b      	ldr	r3, [r7, #4]
 8002e72:	18d3      	adds	r3, r2, r3
 8002e74:	681a      	ldr	r2, [r3, #0]
 8002e76:	683b      	ldr	r3, [r7, #0]
 8002e78:	3b01      	subs	r3, #1
 8002e7a:	4906      	ldr	r1, [pc, #24]	; (8002e94 <LL_DMA_EnableIT_TC+0x38>)
 8002e7c:	5ccb      	ldrb	r3, [r1, r3]
 8002e7e:	0019      	movs	r1, r3
 8002e80:	687b      	ldr	r3, [r7, #4]
 8002e82:	18cb      	adds	r3, r1, r3
 8002e84:	2102      	movs	r1, #2
 8002e86:	430a      	orrs	r2, r1
 8002e88:	601a      	str	r2, [r3, #0]
 8002e8a:	46c0      	nop			; (mov r8, r8)
 8002e8c:	46bd      	mov	sp, r7
 8002e8e:	b002      	add	sp, #8
 8002e90:	bd80      	pop	{r7, pc}
 8002e92:	46c0      	nop			; (mov r8, r8)
 8002e94:	08009f38 	.word	0x08009f38

08002e98 <LL_GPIO_SetPinMode>:
 8002e98:	b580      	push	{r7, lr}
 8002e9a:	b084      	sub	sp, #16
 8002e9c:	af00      	add	r7, sp, #0
 8002e9e:	60f8      	str	r0, [r7, #12]
 8002ea0:	60b9      	str	r1, [r7, #8]
 8002ea2:	607a      	str	r2, [r7, #4]
 8002ea4:	68fb      	ldr	r3, [r7, #12]
 8002ea6:	6819      	ldr	r1, [r3, #0]
 8002ea8:	68bb      	ldr	r3, [r7, #8]
 8002eaa:	68ba      	ldr	r2, [r7, #8]
 8002eac:	435a      	muls	r2, r3
 8002eae:	0013      	movs	r3, r2
 8002eb0:	005b      	lsls	r3, r3, #1
 8002eb2:	189b      	adds	r3, r3, r2
 8002eb4:	43db      	mvns	r3, r3
 8002eb6:	400b      	ands	r3, r1
 8002eb8:	001a      	movs	r2, r3
 8002eba:	68bb      	ldr	r3, [r7, #8]
 8002ebc:	68b9      	ldr	r1, [r7, #8]
 8002ebe:	434b      	muls	r3, r1
 8002ec0:	6879      	ldr	r1, [r7, #4]
 8002ec2:	434b      	muls	r3, r1
 8002ec4:	431a      	orrs	r2, r3
 8002ec6:	68fb      	ldr	r3, [r7, #12]
 8002ec8:	601a      	str	r2, [r3, #0]
 8002eca:	46c0      	nop			; (mov r8, r8)
 8002ecc:	46bd      	mov	sp, r7
 8002ece:	b004      	add	sp, #16
 8002ed0:	bd80      	pop	{r7, pc}

08002ed2 <LL_GPIO_SetPinOutputType>:
 8002ed2:	b580      	push	{r7, lr}
 8002ed4:	b084      	sub	sp, #16
 8002ed6:	af00      	add	r7, sp, #0
 8002ed8:	60f8      	str	r0, [r7, #12]
 8002eda:	60b9      	str	r1, [r7, #8]
 8002edc:	607a      	str	r2, [r7, #4]
 8002ede:	68fb      	ldr	r3, [r7, #12]
 8002ee0:	685b      	ldr	r3, [r3, #4]
 8002ee2:	68ba      	ldr	r2, [r7, #8]
 8002ee4:	43d2      	mvns	r2, r2
 8002ee6:	401a      	ands	r2, r3
 8002ee8:	68bb      	ldr	r3, [r7, #8]
 8002eea:	6879      	ldr	r1, [r7, #4]
 8002eec:	434b      	muls	r3, r1
 8002eee:	431a      	orrs	r2, r3
 8002ef0:	68fb      	ldr	r3, [r7, #12]
 8002ef2:	605a      	str	r2, [r3, #4]
 8002ef4:	46c0      	nop			; (mov r8, r8)
 8002ef6:	46bd      	mov	sp, r7
 8002ef8:	b004      	add	sp, #16
 8002efa:	bd80      	pop	{r7, pc}

08002efc <LL_GPIO_SetPinSpeed>:
 8002efc:	b580      	push	{r7, lr}
 8002efe:	b084      	sub	sp, #16
 8002f00:	af00      	add	r7, sp, #0
 8002f02:	60f8      	str	r0, [r7, #12]
 8002f04:	60b9      	str	r1, [r7, #8]
 8002f06:	607a      	str	r2, [r7, #4]
 8002f08:	68fb      	ldr	r3, [r7, #12]
 8002f0a:	6899      	ldr	r1, [r3, #8]
 8002f0c:	68bb      	ldr	r3, [r7, #8]
 8002f0e:	68ba      	ldr	r2, [r7, #8]
 8002f10:	435a      	muls	r2, r3
 8002f12:	0013      	movs	r3, r2
 8002f14:	005b      	lsls	r3, r3, #1
 8002f16:	189b      	adds	r3, r3, r2
 8002f18:	43db      	mvns	r3, r3
 8002f1a:	400b      	ands	r3, r1
 8002f1c:	001a      	movs	r2, r3
 8002f1e:	68bb      	ldr	r3, [r7, #8]
 8002f20:	68b9      	ldr	r1, [r7, #8]
 8002f22:	434b      	muls	r3, r1
 8002f24:	6879      	ldr	r1, [r7, #4]
 8002f26:	434b      	muls	r3, r1
 8002f28:	431a      	orrs	r2, r3
 8002f2a:	68fb      	ldr	r3, [r7, #12]
 8002f2c:	609a      	str	r2, [r3, #8]
 8002f2e:	46c0      	nop			; (mov r8, r8)
 8002f30:	46bd      	mov	sp, r7
 8002f32:	b004      	add	sp, #16
 8002f34:	bd80      	pop	{r7, pc}

08002f36 <LL_GPIO_SetPinPull>:
 8002f36:	b580      	push	{r7, lr}
 8002f38:	b084      	sub	sp, #16
 8002f3a:	af00      	add	r7, sp, #0
 8002f3c:	60f8      	str	r0, [r7, #12]
 8002f3e:	60b9      	str	r1, [r7, #8]
 8002f40:	607a      	str	r2, [r7, #4]
 8002f42:	68fb      	ldr	r3, [r7, #12]
 8002f44:	68d9      	ldr	r1, [r3, #12]
 8002f46:	68bb      	ldr	r3, [r7, #8]
 8002f48:	68ba      	ldr	r2, [r7, #8]
 8002f4a:	435a      	muls	r2, r3
 8002f4c:	0013      	movs	r3, r2
 8002f4e:	005b      	lsls	r3, r3, #1
 8002f50:	189b      	adds	r3, r3, r2
 8002f52:	43db      	mvns	r3, r3
 8002f54:	400b      	ands	r3, r1
 8002f56:	001a      	movs	r2, r3
 8002f58:	68bb      	ldr	r3, [r7, #8]
 8002f5a:	68b9      	ldr	r1, [r7, #8]
 8002f5c:	434b      	muls	r3, r1
 8002f5e:	6879      	ldr	r1, [r7, #4]
 8002f60:	434b      	muls	r3, r1
 8002f62:	431a      	orrs	r2, r3
 8002f64:	68fb      	ldr	r3, [r7, #12]
 8002f66:	60da      	str	r2, [r3, #12]
 8002f68:	46c0      	nop			; (mov r8, r8)
 8002f6a:	46bd      	mov	sp, r7
 8002f6c:	b004      	add	sp, #16
 8002f6e:	bd80      	pop	{r7, pc}

08002f70 <LL_GPIO_SetAFPin_0_7>:
 8002f70:	b580      	push	{r7, lr}
 8002f72:	b084      	sub	sp, #16
 8002f74:	af00      	add	r7, sp, #0
 8002f76:	60f8      	str	r0, [r7, #12]
 8002f78:	60b9      	str	r1, [r7, #8]
 8002f7a:	607a      	str	r2, [r7, #4]
 8002f7c:	68fb      	ldr	r3, [r7, #12]
 8002f7e:	6a19      	ldr	r1, [r3, #32]
 8002f80:	68bb      	ldr	r3, [r7, #8]
 8002f82:	68ba      	ldr	r2, [r7, #8]
 8002f84:	4353      	muls	r3, r2
 8002f86:	68ba      	ldr	r2, [r7, #8]
 8002f88:	4353      	muls	r3, r2
 8002f8a:	68ba      	ldr	r2, [r7, #8]
 8002f8c:	435a      	muls	r2, r3
 8002f8e:	0013      	movs	r3, r2
 8002f90:	011b      	lsls	r3, r3, #4
 8002f92:	1a9b      	subs	r3, r3, r2
 8002f94:	43db      	mvns	r3, r3
 8002f96:	400b      	ands	r3, r1
 8002f98:	001a      	movs	r2, r3
 8002f9a:	68bb      	ldr	r3, [r7, #8]
 8002f9c:	68b9      	ldr	r1, [r7, #8]
 8002f9e:	434b      	muls	r3, r1
 8002fa0:	68b9      	ldr	r1, [r7, #8]
 8002fa2:	434b      	muls	r3, r1
 8002fa4:	68b9      	ldr	r1, [r7, #8]
 8002fa6:	434b      	muls	r3, r1
 8002fa8:	6879      	ldr	r1, [r7, #4]
 8002faa:	434b      	muls	r3, r1
 8002fac:	431a      	orrs	r2, r3
 8002fae:	68fb      	ldr	r3, [r7, #12]
 8002fb0:	621a      	str	r2, [r3, #32]
 8002fb2:	46c0      	nop			; (mov r8, r8)
 8002fb4:	46bd      	mov	sp, r7
 8002fb6:	b004      	add	sp, #16
 8002fb8:	bd80      	pop	{r7, pc}
	...

08002fbc <LL_AHB1_GRP1_EnableClock>:
 8002fbc:	b580      	push	{r7, lr}
 8002fbe:	b084      	sub	sp, #16
 8002fc0:	af00      	add	r7, sp, #0
 8002fc2:	6078      	str	r0, [r7, #4]
 8002fc4:	4b07      	ldr	r3, [pc, #28]	; (8002fe4 <LL_AHB1_GRP1_EnableClock+0x28>)
 8002fc6:	6959      	ldr	r1, [r3, #20]
 8002fc8:	4b06      	ldr	r3, [pc, #24]	; (8002fe4 <LL_AHB1_GRP1_EnableClock+0x28>)
 8002fca:	687a      	ldr	r2, [r7, #4]
 8002fcc:	430a      	orrs	r2, r1
 8002fce:	615a      	str	r2, [r3, #20]
 8002fd0:	4b04      	ldr	r3, [pc, #16]	; (8002fe4 <LL_AHB1_GRP1_EnableClock+0x28>)
 8002fd2:	695b      	ldr	r3, [r3, #20]
 8002fd4:	687a      	ldr	r2, [r7, #4]
 8002fd6:	4013      	ands	r3, r2
 8002fd8:	60fb      	str	r3, [r7, #12]
 8002fda:	68fb      	ldr	r3, [r7, #12]
 8002fdc:	46c0      	nop			; (mov r8, r8)
 8002fde:	46bd      	mov	sp, r7
 8002fe0:	b004      	add	sp, #16
 8002fe2:	bd80      	pop	{r7, pc}
 8002fe4:	40021000 	.word	0x40021000

08002fe8 <LL_APB1_GRP1_EnableClock>:
 8002fe8:	b580      	push	{r7, lr}
 8002fea:	b084      	sub	sp, #16
 8002fec:	af00      	add	r7, sp, #0
 8002fee:	6078      	str	r0, [r7, #4]
 8002ff0:	4b07      	ldr	r3, [pc, #28]	; (8003010 <LL_APB1_GRP1_EnableClock+0x28>)
 8002ff2:	69d9      	ldr	r1, [r3, #28]
 8002ff4:	4b06      	ldr	r3, [pc, #24]	; (8003010 <LL_APB1_GRP1_EnableClock+0x28>)
 8002ff6:	687a      	ldr	r2, [r7, #4]
 8002ff8:	430a      	orrs	r2, r1
 8002ffa:	61da      	str	r2, [r3, #28]
 8002ffc:	4b04      	ldr	r3, [pc, #16]	; (8003010 <LL_APB1_GRP1_EnableClock+0x28>)
 8002ffe:	69db      	ldr	r3, [r3, #28]
 8003000:	687a      	ldr	r2, [r7, #4]
 8003002:	4013      	ands	r3, r2
 8003004:	60fb      	str	r3, [r7, #12]
 8003006:	68fb      	ldr	r3, [r7, #12]
 8003008:	46c0      	nop			; (mov r8, r8)
 800300a:	46bd      	mov	sp, r7
 800300c:	b004      	add	sp, #16
 800300e:	bd80      	pop	{r7, pc}
 8003010:	40021000 	.word	0x40021000

08003014 <term_init>:
 8003014:	b580      	push	{r7, lr}
 8003016:	b082      	sub	sp, #8
 8003018:	af02      	add	r7, sp, #8
 800301a:	2380      	movs	r3, #128	; 0x80
 800301c:	029b      	lsls	r3, r3, #10
 800301e:	0018      	movs	r0, r3
 8003020:	f7ff ffcc 	bl	8002fbc <LL_AHB1_GRP1_EnableClock>
 8003024:	2390      	movs	r3, #144	; 0x90
 8003026:	05db      	lsls	r3, r3, #23
 8003028:	2202      	movs	r2, #2
 800302a:	2104      	movs	r1, #4
 800302c:	0018      	movs	r0, r3
 800302e:	f7ff ff33 	bl	8002e98 <LL_GPIO_SetPinMode>
 8003032:	2390      	movs	r3, #144	; 0x90
 8003034:	05db      	lsls	r3, r3, #23
 8003036:	2201      	movs	r2, #1
 8003038:	2104      	movs	r1, #4
 800303a:	0018      	movs	r0, r3
 800303c:	f7ff ff98 	bl	8002f70 <LL_GPIO_SetAFPin_0_7>
 8003040:	2390      	movs	r3, #144	; 0x90
 8003042:	05db      	lsls	r3, r3, #23
 8003044:	2200      	movs	r2, #0
 8003046:	2104      	movs	r1, #4
 8003048:	0018      	movs	r0, r3
 800304a:	f7ff ff42 	bl	8002ed2 <LL_GPIO_SetPinOutputType>
 800304e:	2390      	movs	r3, #144	; 0x90
 8003050:	05db      	lsls	r3, r3, #23
 8003052:	2202      	movs	r2, #2
 8003054:	2104      	movs	r1, #4
 8003056:	0018      	movs	r0, r3
 8003058:	f7ff ff6d 	bl	8002f36 <LL_GPIO_SetPinPull>
 800305c:	2390      	movs	r3, #144	; 0x90
 800305e:	05db      	lsls	r3, r3, #23
 8003060:	2203      	movs	r2, #3
 8003062:	2104      	movs	r1, #4
 8003064:	0018      	movs	r0, r3
 8003066:	f7ff ff49 	bl	8002efc <LL_GPIO_SetPinSpeed>
 800306a:	2390      	movs	r3, #144	; 0x90
 800306c:	05db      	lsls	r3, r3, #23
 800306e:	2202      	movs	r2, #2
 8003070:	2108      	movs	r1, #8
 8003072:	0018      	movs	r0, r3
 8003074:	f7ff ff10 	bl	8002e98 <LL_GPIO_SetPinMode>
 8003078:	2390      	movs	r3, #144	; 0x90
 800307a:	05db      	lsls	r3, r3, #23
 800307c:	2201      	movs	r2, #1
 800307e:	2108      	movs	r1, #8
 8003080:	0018      	movs	r0, r3
 8003082:	f7ff ff75 	bl	8002f70 <LL_GPIO_SetAFPin_0_7>
 8003086:	2390      	movs	r3, #144	; 0x90
 8003088:	05db      	lsls	r3, r3, #23
 800308a:	2200      	movs	r2, #0
 800308c:	2108      	movs	r1, #8
 800308e:	0018      	movs	r0, r3
 8003090:	f7ff ff51 	bl	8002f36 <LL_GPIO_SetPinPull>
 8003094:	2390      	movs	r3, #144	; 0x90
 8003096:	05db      	lsls	r3, r3, #23
 8003098:	2203      	movs	r2, #3
 800309a:	2108      	movs	r1, #8
 800309c:	0018      	movs	r0, r3
 800309e:	f7ff ff2d 	bl	8002efc <LL_GPIO_SetPinSpeed>
 80030a2:	2380      	movs	r3, #128	; 0x80
 80030a4:	029b      	lsls	r3, r3, #10
 80030a6:	0018      	movs	r0, r3
 80030a8:	f7ff ff9e 	bl	8002fe8 <LL_APB1_GRP1_EnableClock>
 80030ac:	4b2b      	ldr	r3, [pc, #172]	; (800315c <term_init+0x148>)
 80030ae:	210c      	movs	r1, #12
 80030b0:	0018      	movs	r0, r3
 80030b2:	f7ff fd27 	bl	8002b04 <LL_USART_SetTransferDirection>
 80030b6:	4b29      	ldr	r3, [pc, #164]	; (800315c <term_init+0x148>)
 80030b8:	2100      	movs	r1, #0
 80030ba:	0018      	movs	r0, r3
 80030bc:	f7ff fd34 	bl	8002b28 <LL_USART_SetParity>
 80030c0:	4b26      	ldr	r3, [pc, #152]	; (800315c <term_init+0x148>)
 80030c2:	2100      	movs	r1, #0
 80030c4:	0018      	movs	r0, r3
 80030c6:	f7ff fd43 	bl	8002b50 <LL_USART_SetDataWidth>
 80030ca:	4b24      	ldr	r3, [pc, #144]	; (800315c <term_init+0x148>)
 80030cc:	2100      	movs	r1, #0
 80030ce:	0018      	movs	r0, r3
 80030d0:	f7ff fd52 	bl	8002b78 <LL_USART_SetStopBitsLength>
 80030d4:	4b22      	ldr	r3, [pc, #136]	; (8003160 <term_init+0x14c>)
 80030d6:	6819      	ldr	r1, [r3, #0]
 80030d8:	23e1      	movs	r3, #225	; 0xe1
 80030da:	025b      	lsls	r3, r3, #9
 80030dc:	481f      	ldr	r0, [pc, #124]	; (800315c <term_init+0x148>)
 80030de:	2200      	movs	r2, #0
 80030e0:	f7ff fd5e 	bl	8002ba0 <LL_USART_SetBaudRate>
 80030e4:	4b1d      	ldr	r3, [pc, #116]	; (800315c <term_init+0x148>)
 80030e6:	0018      	movs	r0, r3
 80030e8:	f7ff fdbf 	bl	8002c6a <LL_USART_EnableDMAReq_RX>
 80030ec:	2001      	movs	r0, #1
 80030ee:	f7ff ff65 	bl	8002fbc <LL_AHB1_GRP1_EnableClock>
 80030f2:	4a1c      	ldr	r2, [pc, #112]	; (8003164 <term_init+0x150>)
 80030f4:	491c      	ldr	r1, [pc, #112]	; (8003168 <term_init+0x154>)
 80030f6:	481d      	ldr	r0, [pc, #116]	; (800316c <term_init+0x158>)
 80030f8:	2300      	movs	r3, #0
 80030fa:	9300      	str	r3, [sp, #0]
 80030fc:	0013      	movs	r3, r2
 80030fe:	000a      	movs	r2, r1
 8003100:	2105      	movs	r1, #5
 8003102:	f7ff fe53 	bl	8002dac <LL_DMA_ConfigAddresses>
 8003106:	4b19      	ldr	r3, [pc, #100]	; (800316c <term_init+0x158>)
 8003108:	2213      	movs	r2, #19
 800310a:	2105      	movs	r1, #5
 800310c:	0018      	movs	r0, r3
 800310e:	f7ff fe2d 	bl	8002d6c <LL_DMA_SetDataLength>
 8003112:	4b16      	ldr	r3, [pc, #88]	; (800316c <term_init+0x158>)
 8003114:	2280      	movs	r2, #128	; 0x80
 8003116:	2105      	movs	r1, #5
 8003118:	0018      	movs	r0, r3
 800311a:	f7ff fe05 	bl	8002d28 <LL_DMA_SetMemoryIncMode>
 800311e:	4b13      	ldr	r3, [pc, #76]	; (800316c <term_init+0x158>)
 8003120:	2220      	movs	r2, #32
 8003122:	2105      	movs	r1, #5
 8003124:	0018      	movs	r0, r3
 8003126:	f7ff fddd 	bl	8002ce4 <LL_DMA_SetMode>
 800312a:	4b10      	ldr	r3, [pc, #64]	; (800316c <term_init+0x158>)
 800312c:	2105      	movs	r1, #5
 800312e:	0018      	movs	r0, r3
 8003130:	f7ff fe94 	bl	8002e5c <LL_DMA_EnableIT_TC>
 8003134:	2102      	movs	r1, #2
 8003136:	200b      	movs	r0, #11
 8003138:	f7ff fc68 	bl	8002a0c <NVIC_SetPriority>
 800313c:	200b      	movs	r0, #11
 800313e:	f7ff fc37 	bl	80029b0 <NVIC_EnableIRQ>
 8003142:	4b06      	ldr	r3, [pc, #24]	; (800315c <term_init+0x148>)
 8003144:	0018      	movs	r0, r3
 8003146:	f7ff fccf 	bl	8002ae8 <LL_USART_Enable>
 800314a:	4b08      	ldr	r3, [pc, #32]	; (800316c <term_init+0x158>)
 800314c:	2105      	movs	r1, #5
 800314e:	0018      	movs	r0, r3
 8003150:	f7ff fdaa 	bl	8002ca8 <LL_DMA_EnableChannel>
 8003154:	46c0      	nop			; (mov r8, r8)
 8003156:	46bd      	mov	sp, r7
 8003158:	bd80      	pop	{r7, pc}
 800315a:	46c0      	nop			; (mov r8, r8)
 800315c:	40004400 	.word	0x40004400
 8003160:	20000000 	.word	0x20000000
 8003164:	20000f54 	.word	0x20000f54
 8003168:	40004424 	.word	0x40004424
 800316c:	40020000 	.word	0x40020000

08003170 <comm_send_msg>:
 8003170:	b580      	push	{r7, lr}
 8003172:	b084      	sub	sp, #16
 8003174:	af00      	add	r7, sp, #0
 8003176:	6078      	str	r0, [r7, #4]
 8003178:	6039      	str	r1, [r7, #0]
 800317a:	2300      	movs	r3, #0
 800317c:	60fb      	str	r3, [r7, #12]
 800317e:	4b14      	ldr	r3, [pc, #80]	; (80031d0 <comm_send_msg+0x60>)
 8003180:	0018      	movs	r0, r3
 8003182:	f7ff fd67 	bl	8002c54 <LL_USART_ClearFlag_TC>
 8003186:	e012      	b.n	80031ae <comm_send_msg+0x3e>
 8003188:	46c0      	nop			; (mov r8, r8)
 800318a:	4b11      	ldr	r3, [pc, #68]	; (80031d0 <comm_send_msg+0x60>)
 800318c:	0018      	movs	r0, r3
 800318e:	f7ff fd51 	bl	8002c34 <LL_USART_IsActiveFlag_TXE>
 8003192:	1e03      	subs	r3, r0, #0
 8003194:	d0f9      	beq.n	800318a <comm_send_msg+0x1a>
 8003196:	68fb      	ldr	r3, [r7, #12]
 8003198:	1c5a      	adds	r2, r3, #1
 800319a:	60fa      	str	r2, [r7, #12]
 800319c:	001a      	movs	r2, r3
 800319e:	687b      	ldr	r3, [r7, #4]
 80031a0:	189b      	adds	r3, r3, r2
 80031a2:	781b      	ldrb	r3, [r3, #0]
 80031a4:	4a0a      	ldr	r2, [pc, #40]	; (80031d0 <comm_send_msg+0x60>)
 80031a6:	0019      	movs	r1, r3
 80031a8:	0010      	movs	r0, r2
 80031aa:	f7ff fd6c 	bl	8002c86 <LL_USART_TransmitData8>
 80031ae:	683b      	ldr	r3, [r7, #0]
 80031b0:	1e5a      	subs	r2, r3, #1
 80031b2:	603a      	str	r2, [r7, #0]
 80031b4:	2b00      	cmp	r3, #0
 80031b6:	d1e7      	bne.n	8003188 <comm_send_msg+0x18>
 80031b8:	46c0      	nop			; (mov r8, r8)
 80031ba:	4b05      	ldr	r3, [pc, #20]	; (80031d0 <comm_send_msg+0x60>)
 80031bc:	0018      	movs	r0, r3
 80031be:	f7ff fd29 	bl	8002c14 <LL_USART_IsActiveFlag_TC>
 80031c2:	1e03      	subs	r3, r0, #0
 80031c4:	d0f9      	beq.n	80031ba <comm_send_msg+0x4a>
 80031c6:	46c0      	nop			; (mov r8, r8)
 80031c8:	46bd      	mov	sp, r7
 80031ca:	b004      	add	sp, #16
 80031cc:	bd80      	pop	{r7, pc}
 80031ce:	46c0      	nop			; (mov r8, r8)
 80031d0:	40004400 	.word	0x40004400

080031d4 <DMA1_Channel4_5_IRQHandler>:
 80031d4:	b580      	push	{r7, lr}
 80031d6:	af00      	add	r7, sp, #0
 80031d8:	200f      	movs	r0, #15
 80031da:	f7ff fbff 	bl	80029dc <NVIC_DisableIRQ>
 80031de:	2010      	movs	r0, #16
 80031e0:	f7ff fbfc 	bl	80029dc <NVIC_DisableIRQ>
 80031e4:	4b0e      	ldr	r3, [pc, #56]	; (8003220 <DMA1_Channel4_5_IRQHandler+0x4c>)
 80031e6:	0018      	movs	r0, r3
 80031e8:	f7ff fe16 	bl	8002e18 <LL_DMA_IsActiveFlag_TC5>
 80031ec:	1e03      	subs	r3, r0, #0
 80031ee:	d00d      	beq.n	800320c <DMA1_Channel4_5_IRQHandler+0x38>
 80031f0:	4b0b      	ldr	r3, [pc, #44]	; (8003220 <DMA1_Channel4_5_IRQHandler+0x4c>)
 80031f2:	0018      	movs	r0, r3
 80031f4:	f7ff fe26 	bl	8002e44 <LL_DMA_ClearFlag_TC5>
 80031f8:	490a      	ldr	r1, [pc, #40]	; (8003224 <DMA1_Channel4_5_IRQHandler+0x50>)
 80031fa:	4b0b      	ldr	r3, [pc, #44]	; (8003228 <DMA1_Channel4_5_IRQHandler+0x54>)
 80031fc:	2213      	movs	r2, #19
 80031fe:	0018      	movs	r0, r3
 8003200:	f006 f996 	bl	8009530 <memcpy>
 8003204:	4b08      	ldr	r3, [pc, #32]	; (8003228 <DMA1_Channel4_5_IRQHandler+0x54>)
 8003206:	0018      	movs	r0, r3
 8003208:	f7ff f992 	bl	8002530 <dyn_manager>
 800320c:	200f      	movs	r0, #15
 800320e:	f7ff fbcf 	bl	80029b0 <NVIC_EnableIRQ>
 8003212:	2010      	movs	r0, #16
 8003214:	f7ff fbcc 	bl	80029b0 <NVIC_EnableIRQ>
 8003218:	46c0      	nop			; (mov r8, r8)
 800321a:	46bd      	mov	sp, r7
 800321c:	bd80      	pop	{r7, pc}
 800321e:	46c0      	nop			; (mov r8, r8)
 8003220:	40020000 	.word	0x40020000
 8003224:	20000f54 	.word	0x20000f54
 8003228:	20001053 	.word	0x20001053

0800322c <LL_GPIO_SetPinMode>:
 800322c:	b580      	push	{r7, lr}
 800322e:	b084      	sub	sp, #16
 8003230:	af00      	add	r7, sp, #0
 8003232:	60f8      	str	r0, [r7, #12]
 8003234:	60b9      	str	r1, [r7, #8]
 8003236:	607a      	str	r2, [r7, #4]
 8003238:	68fb      	ldr	r3, [r7, #12]
 800323a:	6819      	ldr	r1, [r3, #0]
 800323c:	68bb      	ldr	r3, [r7, #8]
 800323e:	68ba      	ldr	r2, [r7, #8]
 8003240:	435a      	muls	r2, r3
 8003242:	0013      	movs	r3, r2
 8003244:	005b      	lsls	r3, r3, #1
 8003246:	189b      	adds	r3, r3, r2
 8003248:	43db      	mvns	r3, r3
 800324a:	400b      	ands	r3, r1
 800324c:	001a      	movs	r2, r3
 800324e:	68bb      	ldr	r3, [r7, #8]
 8003250:	68b9      	ldr	r1, [r7, #8]
 8003252:	434b      	muls	r3, r1
 8003254:	6879      	ldr	r1, [r7, #4]
 8003256:	434b      	muls	r3, r1
 8003258:	431a      	orrs	r2, r3
 800325a:	68fb      	ldr	r3, [r7, #12]
 800325c:	601a      	str	r2, [r3, #0]
 800325e:	46c0      	nop			; (mov r8, r8)
 8003260:	46bd      	mov	sp, r7
 8003262:	b004      	add	sp, #16
 8003264:	bd80      	pop	{r7, pc}

08003266 <LL_GPIO_SetPinOutputType>:
 8003266:	b580      	push	{r7, lr}
 8003268:	b084      	sub	sp, #16
 800326a:	af00      	add	r7, sp, #0
 800326c:	60f8      	str	r0, [r7, #12]
 800326e:	60b9      	str	r1, [r7, #8]
 8003270:	607a      	str	r2, [r7, #4]
 8003272:	68fb      	ldr	r3, [r7, #12]
 8003274:	685b      	ldr	r3, [r3, #4]
 8003276:	68ba      	ldr	r2, [r7, #8]
 8003278:	43d2      	mvns	r2, r2
 800327a:	401a      	ands	r2, r3
 800327c:	68bb      	ldr	r3, [r7, #8]
 800327e:	6879      	ldr	r1, [r7, #4]
 8003280:	434b      	muls	r3, r1
 8003282:	431a      	orrs	r2, r3
 8003284:	68fb      	ldr	r3, [r7, #12]
 8003286:	605a      	str	r2, [r3, #4]
 8003288:	46c0      	nop			; (mov r8, r8)
 800328a:	46bd      	mov	sp, r7
 800328c:	b004      	add	sp, #16
 800328e:	bd80      	pop	{r7, pc}

08003290 <LL_GPIO_SetPinSpeed>:
 8003290:	b580      	push	{r7, lr}
 8003292:	b084      	sub	sp, #16
 8003294:	af00      	add	r7, sp, #0
 8003296:	60f8      	str	r0, [r7, #12]
 8003298:	60b9      	str	r1, [r7, #8]
 800329a:	607a      	str	r2, [r7, #4]
 800329c:	68fb      	ldr	r3, [r7, #12]
 800329e:	6899      	ldr	r1, [r3, #8]
 80032a0:	68bb      	ldr	r3, [r7, #8]
 80032a2:	68ba      	ldr	r2, [r7, #8]
 80032a4:	435a      	muls	r2, r3
 80032a6:	0013      	movs	r3, r2
 80032a8:	005b      	lsls	r3, r3, #1
 80032aa:	189b      	adds	r3, r3, r2
 80032ac:	43db      	mvns	r3, r3
 80032ae:	400b      	ands	r3, r1
 80032b0:	001a      	movs	r2, r3
 80032b2:	68bb      	ldr	r3, [r7, #8]
 80032b4:	68b9      	ldr	r1, [r7, #8]
 80032b6:	434b      	muls	r3, r1
 80032b8:	6879      	ldr	r1, [r7, #4]
 80032ba:	434b      	muls	r3, r1
 80032bc:	431a      	orrs	r2, r3
 80032be:	68fb      	ldr	r3, [r7, #12]
 80032c0:	609a      	str	r2, [r3, #8]
 80032c2:	46c0      	nop			; (mov r8, r8)
 80032c4:	46bd      	mov	sp, r7
 80032c6:	b004      	add	sp, #16
 80032c8:	bd80      	pop	{r7, pc}

080032ca <LL_GPIO_SetAFPin_0_7>:
 80032ca:	b580      	push	{r7, lr}
 80032cc:	b084      	sub	sp, #16
 80032ce:	af00      	add	r7, sp, #0
 80032d0:	60f8      	str	r0, [r7, #12]
 80032d2:	60b9      	str	r1, [r7, #8]
 80032d4:	607a      	str	r2, [r7, #4]
 80032d6:	68fb      	ldr	r3, [r7, #12]
 80032d8:	6a19      	ldr	r1, [r3, #32]
 80032da:	68bb      	ldr	r3, [r7, #8]
 80032dc:	68ba      	ldr	r2, [r7, #8]
 80032de:	4353      	muls	r3, r2
 80032e0:	68ba      	ldr	r2, [r7, #8]
 80032e2:	4353      	muls	r3, r2
 80032e4:	68ba      	ldr	r2, [r7, #8]
 80032e6:	435a      	muls	r2, r3
 80032e8:	0013      	movs	r3, r2
 80032ea:	011b      	lsls	r3, r3, #4
 80032ec:	1a9b      	subs	r3, r3, r2
 80032ee:	43db      	mvns	r3, r3
 80032f0:	400b      	ands	r3, r1
 80032f2:	001a      	movs	r2, r3
 80032f4:	68bb      	ldr	r3, [r7, #8]
 80032f6:	68b9      	ldr	r1, [r7, #8]
 80032f8:	434b      	muls	r3, r1
 80032fa:	68b9      	ldr	r1, [r7, #8]
 80032fc:	434b      	muls	r3, r1
 80032fe:	68b9      	ldr	r1, [r7, #8]
 8003300:	434b      	muls	r3, r1
 8003302:	6879      	ldr	r1, [r7, #4]
 8003304:	434b      	muls	r3, r1
 8003306:	431a      	orrs	r2, r3
 8003308:	68fb      	ldr	r3, [r7, #12]
 800330a:	621a      	str	r2, [r3, #32]
 800330c:	46c0      	nop			; (mov r8, r8)
 800330e:	46bd      	mov	sp, r7
 8003310:	b004      	add	sp, #16
 8003312:	bd80      	pop	{r7, pc}

08003314 <LL_GPIO_ResetOutputPin>:
 8003314:	b580      	push	{r7, lr}
 8003316:	b082      	sub	sp, #8
 8003318:	af00      	add	r7, sp, #0
 800331a:	6078      	str	r0, [r7, #4]
 800331c:	6039      	str	r1, [r7, #0]
 800331e:	687b      	ldr	r3, [r7, #4]
 8003320:	683a      	ldr	r2, [r7, #0]
 8003322:	629a      	str	r2, [r3, #40]	; 0x28
 8003324:	46c0      	nop			; (mov r8, r8)
 8003326:	46bd      	mov	sp, r7
 8003328:	b002      	add	sp, #8
 800332a:	bd80      	pop	{r7, pc}

0800332c <LL_I2C_Enable>:
 800332c:	b580      	push	{r7, lr}
 800332e:	b082      	sub	sp, #8
 8003330:	af00      	add	r7, sp, #0
 8003332:	6078      	str	r0, [r7, #4]
 8003334:	687b      	ldr	r3, [r7, #4]
 8003336:	681b      	ldr	r3, [r3, #0]
 8003338:	2201      	movs	r2, #1
 800333a:	431a      	orrs	r2, r3
 800333c:	687b      	ldr	r3, [r7, #4]
 800333e:	601a      	str	r2, [r3, #0]
 8003340:	46c0      	nop			; (mov r8, r8)
 8003342:	46bd      	mov	sp, r7
 8003344:	b002      	add	sp, #8
 8003346:	bd80      	pop	{r7, pc}

08003348 <LL_I2C_Disable>:
 8003348:	b580      	push	{r7, lr}
 800334a:	b082      	sub	sp, #8
 800334c:	af00      	add	r7, sp, #0
 800334e:	6078      	str	r0, [r7, #4]
 8003350:	687b      	ldr	r3, [r7, #4]
 8003352:	681b      	ldr	r3, [r3, #0]
 8003354:	2201      	movs	r2, #1
 8003356:	4393      	bics	r3, r2
 8003358:	001a      	movs	r2, r3
 800335a:	687b      	ldr	r3, [r7, #4]
 800335c:	601a      	str	r2, [r3, #0]
 800335e:	46c0      	nop			; (mov r8, r8)
 8003360:	46bd      	mov	sp, r7
 8003362:	b002      	add	sp, #8
 8003364:	bd80      	pop	{r7, pc}
	...

08003368 <LL_I2C_SetDigitalFilter>:
 8003368:	b580      	push	{r7, lr}
 800336a:	b082      	sub	sp, #8
 800336c:	af00      	add	r7, sp, #0
 800336e:	6078      	str	r0, [r7, #4]
 8003370:	6039      	str	r1, [r7, #0]
 8003372:	687b      	ldr	r3, [r7, #4]
 8003374:	681b      	ldr	r3, [r3, #0]
 8003376:	4a05      	ldr	r2, [pc, #20]	; (800338c <LL_I2C_SetDigitalFilter+0x24>)
 8003378:	401a      	ands	r2, r3
 800337a:	683b      	ldr	r3, [r7, #0]
 800337c:	021b      	lsls	r3, r3, #8
 800337e:	431a      	orrs	r2, r3
 8003380:	687b      	ldr	r3, [r7, #4]
 8003382:	601a      	str	r2, [r3, #0]
 8003384:	46c0      	nop			; (mov r8, r8)
 8003386:	46bd      	mov	sp, r7
 8003388:	b002      	add	sp, #8
 800338a:	bd80      	pop	{r7, pc}
 800338c:	fffff0ff 	.word	0xfffff0ff

08003390 <LL_I2C_DisableAnalogFilter>:
 8003390:	b580      	push	{r7, lr}
 8003392:	b082      	sub	sp, #8
 8003394:	af00      	add	r7, sp, #0
 8003396:	6078      	str	r0, [r7, #4]
 8003398:	687b      	ldr	r3, [r7, #4]
 800339a:	681b      	ldr	r3, [r3, #0]
 800339c:	2280      	movs	r2, #128	; 0x80
 800339e:	0152      	lsls	r2, r2, #5
 80033a0:	431a      	orrs	r2, r3
 80033a2:	687b      	ldr	r3, [r7, #4]
 80033a4:	601a      	str	r2, [r3, #0]
 80033a6:	46c0      	nop			; (mov r8, r8)
 80033a8:	46bd      	mov	sp, r7
 80033aa:	b002      	add	sp, #8
 80033ac:	bd80      	pop	{r7, pc}

080033ae <LL_I2C_DisableClockStretching>:
 80033ae:	b580      	push	{r7, lr}
 80033b0:	b082      	sub	sp, #8
 80033b2:	af00      	add	r7, sp, #0
 80033b4:	6078      	str	r0, [r7, #4]
 80033b6:	687b      	ldr	r3, [r7, #4]
 80033b8:	681b      	ldr	r3, [r3, #0]
 80033ba:	2280      	movs	r2, #128	; 0x80
 80033bc:	0292      	lsls	r2, r2, #10
 80033be:	431a      	orrs	r2, r3
 80033c0:	687b      	ldr	r3, [r7, #4]
 80033c2:	601a      	str	r2, [r3, #0]
 80033c4:	46c0      	nop			; (mov r8, r8)
 80033c6:	46bd      	mov	sp, r7
 80033c8:	b002      	add	sp, #8
 80033ca:	bd80      	pop	{r7, pc}

080033cc <LL_I2C_SetMasterAddressingMode>:
 80033cc:	b580      	push	{r7, lr}
 80033ce:	b082      	sub	sp, #8
 80033d0:	af00      	add	r7, sp, #0
 80033d2:	6078      	str	r0, [r7, #4]
 80033d4:	6039      	str	r1, [r7, #0]
 80033d6:	687b      	ldr	r3, [r7, #4]
 80033d8:	685b      	ldr	r3, [r3, #4]
 80033da:	4a05      	ldr	r2, [pc, #20]	; (80033f0 <LL_I2C_SetMasterAddressingMode+0x24>)
 80033dc:	401a      	ands	r2, r3
 80033de:	683b      	ldr	r3, [r7, #0]
 80033e0:	431a      	orrs	r2, r3
 80033e2:	687b      	ldr	r3, [r7, #4]
 80033e4:	605a      	str	r2, [r3, #4]
 80033e6:	46c0      	nop			; (mov r8, r8)
 80033e8:	46bd      	mov	sp, r7
 80033ea:	b002      	add	sp, #8
 80033ec:	bd80      	pop	{r7, pc}
 80033ee:	46c0      	nop			; (mov r8, r8)
 80033f0:	fffff7ff 	.word	0xfffff7ff

080033f4 <LL_I2C_SetTiming>:
 80033f4:	b580      	push	{r7, lr}
 80033f6:	b082      	sub	sp, #8
 80033f8:	af00      	add	r7, sp, #0
 80033fa:	6078      	str	r0, [r7, #4]
 80033fc:	6039      	str	r1, [r7, #0]
 80033fe:	687b      	ldr	r3, [r7, #4]
 8003400:	683a      	ldr	r2, [r7, #0]
 8003402:	611a      	str	r2, [r3, #16]
 8003404:	46c0      	nop			; (mov r8, r8)
 8003406:	46bd      	mov	sp, r7
 8003408:	b002      	add	sp, #8
 800340a:	bd80      	pop	{r7, pc}

0800340c <LL_I2C_SetMode>:
 800340c:	b580      	push	{r7, lr}
 800340e:	b082      	sub	sp, #8
 8003410:	af00      	add	r7, sp, #0
 8003412:	6078      	str	r0, [r7, #4]
 8003414:	6039      	str	r1, [r7, #0]
 8003416:	687b      	ldr	r3, [r7, #4]
 8003418:	681b      	ldr	r3, [r3, #0]
 800341a:	4a05      	ldr	r2, [pc, #20]	; (8003430 <LL_I2C_SetMode+0x24>)
 800341c:	401a      	ands	r2, r3
 800341e:	683b      	ldr	r3, [r7, #0]
 8003420:	431a      	orrs	r2, r3
 8003422:	687b      	ldr	r3, [r7, #4]
 8003424:	601a      	str	r2, [r3, #0]
 8003426:	46c0      	nop			; (mov r8, r8)
 8003428:	46bd      	mov	sp, r7
 800342a:	b002      	add	sp, #8
 800342c:	bd80      	pop	{r7, pc}
 800342e:	46c0      	nop			; (mov r8, r8)
 8003430:	ffcfffff 	.word	0xffcfffff

08003434 <LL_I2C_IsActiveFlag_TXIS>:
 8003434:	b580      	push	{r7, lr}
 8003436:	b082      	sub	sp, #8
 8003438:	af00      	add	r7, sp, #0
 800343a:	6078      	str	r0, [r7, #4]
 800343c:	687b      	ldr	r3, [r7, #4]
 800343e:	699b      	ldr	r3, [r3, #24]
 8003440:	2202      	movs	r2, #2
 8003442:	4013      	ands	r3, r2
 8003444:	3b02      	subs	r3, #2
 8003446:	425a      	negs	r2, r3
 8003448:	4153      	adcs	r3, r2
 800344a:	b2db      	uxtb	r3, r3
 800344c:	0018      	movs	r0, r3
 800344e:	46bd      	mov	sp, r7
 8003450:	b002      	add	sp, #8
 8003452:	bd80      	pop	{r7, pc}

08003454 <LL_I2C_IsActiveFlag_RXNE>:
 8003454:	b580      	push	{r7, lr}
 8003456:	b082      	sub	sp, #8
 8003458:	af00      	add	r7, sp, #0
 800345a:	6078      	str	r0, [r7, #4]
 800345c:	687b      	ldr	r3, [r7, #4]
 800345e:	699b      	ldr	r3, [r3, #24]
 8003460:	2204      	movs	r2, #4
 8003462:	4013      	ands	r3, r2
 8003464:	3b04      	subs	r3, #4
 8003466:	425a      	negs	r2, r3
 8003468:	4153      	adcs	r3, r2
 800346a:	b2db      	uxtb	r3, r3
 800346c:	0018      	movs	r0, r3
 800346e:	46bd      	mov	sp, r7
 8003470:	b002      	add	sp, #8
 8003472:	bd80      	pop	{r7, pc}

08003474 <LL_I2C_IsActiveFlag_TC>:
 8003474:	b580      	push	{r7, lr}
 8003476:	b082      	sub	sp, #8
 8003478:	af00      	add	r7, sp, #0
 800347a:	6078      	str	r0, [r7, #4]
 800347c:	687b      	ldr	r3, [r7, #4]
 800347e:	699b      	ldr	r3, [r3, #24]
 8003480:	2240      	movs	r2, #64	; 0x40
 8003482:	4013      	ands	r3, r2
 8003484:	3b40      	subs	r3, #64	; 0x40
 8003486:	425a      	negs	r2, r3
 8003488:	4153      	adcs	r3, r2
 800348a:	b2db      	uxtb	r3, r3
 800348c:	0018      	movs	r0, r3
 800348e:	46bd      	mov	sp, r7
 8003490:	b002      	add	sp, #8
 8003492:	bd80      	pop	{r7, pc}

08003494 <LL_I2C_HandleTransfer>:
 8003494:	b580      	push	{r7, lr}
 8003496:	b084      	sub	sp, #16
 8003498:	af00      	add	r7, sp, #0
 800349a:	60f8      	str	r0, [r7, #12]
 800349c:	60b9      	str	r1, [r7, #8]
 800349e:	607a      	str	r2, [r7, #4]
 80034a0:	603b      	str	r3, [r7, #0]
 80034a2:	68fb      	ldr	r3, [r7, #12]
 80034a4:	685b      	ldr	r3, [r3, #4]
 80034a6:	4a09      	ldr	r2, [pc, #36]	; (80034cc <LL_I2C_HandleTransfer+0x38>)
 80034a8:	401a      	ands	r2, r3
 80034aa:	68b9      	ldr	r1, [r7, #8]
 80034ac:	687b      	ldr	r3, [r7, #4]
 80034ae:	4319      	orrs	r1, r3
 80034b0:	683b      	ldr	r3, [r7, #0]
 80034b2:	041b      	lsls	r3, r3, #16
 80034b4:	4319      	orrs	r1, r3
 80034b6:	69bb      	ldr	r3, [r7, #24]
 80034b8:	4319      	orrs	r1, r3
 80034ba:	69fb      	ldr	r3, [r7, #28]
 80034bc:	430b      	orrs	r3, r1
 80034be:	431a      	orrs	r2, r3
 80034c0:	68fb      	ldr	r3, [r7, #12]
 80034c2:	605a      	str	r2, [r3, #4]
 80034c4:	46c0      	nop			; (mov r8, r8)
 80034c6:	46bd      	mov	sp, r7
 80034c8:	b004      	add	sp, #16
 80034ca:	bd80      	pop	{r7, pc}
 80034cc:	fc008000 	.word	0xfc008000

080034d0 <LL_I2C_ReceiveData8>:
 80034d0:	b580      	push	{r7, lr}
 80034d2:	b082      	sub	sp, #8
 80034d4:	af00      	add	r7, sp, #0
 80034d6:	6078      	str	r0, [r7, #4]
 80034d8:	687b      	ldr	r3, [r7, #4]
 80034da:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 80034dc:	b2db      	uxtb	r3, r3
 80034de:	0018      	movs	r0, r3
 80034e0:	46bd      	mov	sp, r7
 80034e2:	b002      	add	sp, #8
 80034e4:	bd80      	pop	{r7, pc}

080034e6 <LL_I2C_TransmitData8>:
 80034e6:	b580      	push	{r7, lr}
 80034e8:	b082      	sub	sp, #8
 80034ea:	af00      	add	r7, sp, #0
 80034ec:	6078      	str	r0, [r7, #4]
 80034ee:	000a      	movs	r2, r1
 80034f0:	1cfb      	adds	r3, r7, #3
 80034f2:	701a      	strb	r2, [r3, #0]
 80034f4:	1cfb      	adds	r3, r7, #3
 80034f6:	781a      	ldrb	r2, [r3, #0]
 80034f8:	687b      	ldr	r3, [r7, #4]
 80034fa:	629a      	str	r2, [r3, #40]	; 0x28
 80034fc:	46c0      	nop			; (mov r8, r8)
 80034fe:	46bd      	mov	sp, r7
 8003500:	b002      	add	sp, #8
 8003502:	bd80      	pop	{r7, pc}

08003504 <LL_AHB1_GRP1_EnableClock>:
 8003504:	b580      	push	{r7, lr}
 8003506:	b084      	sub	sp, #16
 8003508:	af00      	add	r7, sp, #0
 800350a:	6078      	str	r0, [r7, #4]
 800350c:	4b07      	ldr	r3, [pc, #28]	; (800352c <LL_AHB1_GRP1_EnableClock+0x28>)
 800350e:	6959      	ldr	r1, [r3, #20]
 8003510:	4b06      	ldr	r3, [pc, #24]	; (800352c <LL_AHB1_GRP1_EnableClock+0x28>)
 8003512:	687a      	ldr	r2, [r7, #4]
 8003514:	430a      	orrs	r2, r1
 8003516:	615a      	str	r2, [r3, #20]
 8003518:	4b04      	ldr	r3, [pc, #16]	; (800352c <LL_AHB1_GRP1_EnableClock+0x28>)
 800351a:	695b      	ldr	r3, [r3, #20]
 800351c:	687a      	ldr	r2, [r7, #4]
 800351e:	4013      	ands	r3, r2
 8003520:	60fb      	str	r3, [r7, #12]
 8003522:	68fb      	ldr	r3, [r7, #12]
 8003524:	46c0      	nop			; (mov r8, r8)
 8003526:	46bd      	mov	sp, r7
 8003528:	b004      	add	sp, #16
 800352a:	bd80      	pop	{r7, pc}
 800352c:	40021000 	.word	0x40021000

08003530 <LL_APB1_GRP1_EnableClock>:
 8003530:	b580      	push	{r7, lr}
 8003532:	b084      	sub	sp, #16
 8003534:	af00      	add	r7, sp, #0
 8003536:	6078      	str	r0, [r7, #4]
 8003538:	4b07      	ldr	r3, [pc, #28]	; (8003558 <LL_APB1_GRP1_EnableClock+0x28>)
 800353a:	69d9      	ldr	r1, [r3, #28]
 800353c:	4b06      	ldr	r3, [pc, #24]	; (8003558 <LL_APB1_GRP1_EnableClock+0x28>)
 800353e:	687a      	ldr	r2, [r7, #4]
 8003540:	430a      	orrs	r2, r1
 8003542:	61da      	str	r2, [r3, #28]
 8003544:	4b04      	ldr	r3, [pc, #16]	; (8003558 <LL_APB1_GRP1_EnableClock+0x28>)
 8003546:	69db      	ldr	r3, [r3, #28]
 8003548:	687a      	ldr	r2, [r7, #4]
 800354a:	4013      	ands	r3, r2
 800354c:	60fb      	str	r3, [r7, #12]
 800354e:	68fb      	ldr	r3, [r7, #12]
 8003550:	46c0      	nop			; (mov r8, r8)
 8003552:	46bd      	mov	sp, r7
 8003554:	b004      	add	sp, #16
 8003556:	bd80      	pop	{r7, pc}
 8003558:	40021000 	.word	0x40021000

0800355c <LL_RCC_SetI2CClockSource>:
 800355c:	b580      	push	{r7, lr}
 800355e:	b082      	sub	sp, #8
 8003560:	af00      	add	r7, sp, #0
 8003562:	6078      	str	r0, [r7, #4]
 8003564:	4b06      	ldr	r3, [pc, #24]	; (8003580 <LL_RCC_SetI2CClockSource+0x24>)
 8003566:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 8003568:	2210      	movs	r2, #16
 800356a:	4393      	bics	r3, r2
 800356c:	0019      	movs	r1, r3
 800356e:	4b04      	ldr	r3, [pc, #16]	; (8003580 <LL_RCC_SetI2CClockSource+0x24>)
 8003570:	687a      	ldr	r2, [r7, #4]
 8003572:	430a      	orrs	r2, r1
 8003574:	631a      	str	r2, [r3, #48]	; 0x30
 8003576:	46c0      	nop			; (mov r8, r8)
 8003578:	46bd      	mov	sp, r7
 800357a:	b002      	add	sp, #8
 800357c:	bd80      	pop	{r7, pc}
 800357e:	46c0      	nop			; (mov r8, r8)
 8003580:	40021000 	.word	0x40021000

08003584 <VL53L0X_hw_reset>:
 8003584:	b580      	push	{r7, lr}
 8003586:	af00      	add	r7, sp, #0
 8003588:	4b05      	ldr	r3, [pc, #20]	; (80035a0 <VL53L0X_hw_reset+0x1c>)
 800358a:	0018      	movs	r0, r3
 800358c:	f7ff fedc 	bl	8003348 <LL_I2C_Disable>
 8003590:	4b03      	ldr	r3, [pc, #12]	; (80035a0 <VL53L0X_hw_reset+0x1c>)
 8003592:	0018      	movs	r0, r3
 8003594:	f7ff feca 	bl	800332c <LL_I2C_Enable>
 8003598:	46c0      	nop			; (mov r8, r8)
 800359a:	46bd      	mov	sp, r7
 800359c:	bd80      	pop	{r7, pc}
 800359e:	46c0      	nop			; (mov r8, r8)
 80035a0:	40005400 	.word	0x40005400

080035a4 <VL53L0X_hw_config>:
 80035a4:	b590      	push	{r4, r7, lr}
 80035a6:	b085      	sub	sp, #20
 80035a8:	af00      	add	r7, sp, #0
 80035aa:	6078      	str	r0, [r7, #4]
 80035ac:	2380      	movs	r3, #128	; 0x80
 80035ae:	02db      	lsls	r3, r3, #11
 80035b0:	0018      	movs	r0, r3
 80035b2:	f7ff ffa7 	bl	8003504 <LL_AHB1_GRP1_EnableClock>
 80035b6:	4b52      	ldr	r3, [pc, #328]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035b8:	2202      	movs	r2, #2
 80035ba:	2140      	movs	r1, #64	; 0x40
 80035bc:	0018      	movs	r0, r3
 80035be:	f7ff fe35 	bl	800322c <LL_GPIO_SetPinMode>
 80035c2:	4b4f      	ldr	r3, [pc, #316]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035c4:	2201      	movs	r2, #1
 80035c6:	2140      	movs	r1, #64	; 0x40
 80035c8:	0018      	movs	r0, r3
 80035ca:	f7ff fe4c 	bl	8003266 <LL_GPIO_SetPinOutputType>
 80035ce:	4b4c      	ldr	r3, [pc, #304]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035d0:	2201      	movs	r2, #1
 80035d2:	2140      	movs	r1, #64	; 0x40
 80035d4:	0018      	movs	r0, r3
 80035d6:	f7ff fe78 	bl	80032ca <LL_GPIO_SetAFPin_0_7>
 80035da:	4b49      	ldr	r3, [pc, #292]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035dc:	2203      	movs	r2, #3
 80035de:	2140      	movs	r1, #64	; 0x40
 80035e0:	0018      	movs	r0, r3
 80035e2:	f7ff fe55 	bl	8003290 <LL_GPIO_SetPinSpeed>
 80035e6:	4b46      	ldr	r3, [pc, #280]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035e8:	2202      	movs	r2, #2
 80035ea:	2180      	movs	r1, #128	; 0x80
 80035ec:	0018      	movs	r0, r3
 80035ee:	f7ff fe1d 	bl	800322c <LL_GPIO_SetPinMode>
 80035f2:	4b43      	ldr	r3, [pc, #268]	; (8003700 <VL53L0X_hw_config+0x15c>)
 80035f4:	2201      	movs	r2, #1
 80035f6:	2180      	movs	r1, #128	; 0x80
 80035f8:	0018      	movs	r0, r3
 80035fa:	f7ff fe34 	bl	8003266 <LL_GPIO_SetPinOutputType>
 80035fe:	4b40      	ldr	r3, [pc, #256]	; (8003700 <VL53L0X_hw_config+0x15c>)
 8003600:	2201      	movs	r2, #1
 8003602:	2180      	movs	r1, #128	; 0x80
 8003604:	0018      	movs	r0, r3
 8003606:	f7ff fe60 	bl	80032ca <LL_GPIO_SetAFPin_0_7>
 800360a:	4b3d      	ldr	r3, [pc, #244]	; (8003700 <VL53L0X_hw_config+0x15c>)
 800360c:	2203      	movs	r2, #3
 800360e:	2180      	movs	r1, #128	; 0x80
 8003610:	0018      	movs	r0, r3
 8003612:	f7ff fe3d 	bl	8003290 <LL_GPIO_SetPinSpeed>
 8003616:	2380      	movs	r3, #128	; 0x80
 8003618:	031b      	lsls	r3, r3, #12
 800361a:	0018      	movs	r0, r3
 800361c:	f7ff ff72 	bl	8003504 <LL_AHB1_GRP1_EnableClock>
 8003620:	230e      	movs	r3, #14
 8003622:	18fb      	adds	r3, r7, r3
 8003624:	2200      	movs	r2, #0
 8003626:	801a      	strh	r2, [r3, #0]
 8003628:	e034      	b.n	8003694 <VL53L0X_hw_config+0xf0>
 800362a:	240e      	movs	r4, #14
 800362c:	193b      	adds	r3, r7, r4
 800362e:	881a      	ldrh	r2, [r3, #0]
 8003630:	4b34      	ldr	r3, [pc, #208]	; (8003704 <VL53L0X_hw_config+0x160>)
 8003632:	00d2      	lsls	r2, r2, #3
 8003634:	58d0      	ldr	r0, [r2, r3]
 8003636:	193b      	adds	r3, r7, r4
 8003638:	881b      	ldrh	r3, [r3, #0]
 800363a:	4a32      	ldr	r2, [pc, #200]	; (8003704 <VL53L0X_hw_config+0x160>)
 800363c:	00db      	lsls	r3, r3, #3
 800363e:	18d3      	adds	r3, r2, r3
 8003640:	3304      	adds	r3, #4
 8003642:	681b      	ldr	r3, [r3, #0]
 8003644:	2201      	movs	r2, #1
 8003646:	0019      	movs	r1, r3
 8003648:	f7ff fdf0 	bl	800322c <LL_GPIO_SetPinMode>
 800364c:	193b      	adds	r3, r7, r4
 800364e:	881a      	ldrh	r2, [r3, #0]
 8003650:	4b2c      	ldr	r3, [pc, #176]	; (8003704 <VL53L0X_hw_config+0x160>)
 8003652:	00d2      	lsls	r2, r2, #3
 8003654:	58d0      	ldr	r0, [r2, r3]
 8003656:	193b      	adds	r3, r7, r4
 8003658:	881b      	ldrh	r3, [r3, #0]
 800365a:	4a2a      	ldr	r2, [pc, #168]	; (8003704 <VL53L0X_hw_config+0x160>)
 800365c:	00db      	lsls	r3, r3, #3
 800365e:	18d3      	adds	r3, r2, r3
 8003660:	3304      	adds	r3, #4
 8003662:	681b      	ldr	r3, [r3, #0]
 8003664:	2200      	movs	r2, #0
 8003666:	0019      	movs	r1, r3
 8003668:	f7ff fdfd 	bl	8003266 <LL_GPIO_SetPinOutputType>
 800366c:	193b      	adds	r3, r7, r4
 800366e:	881a      	ldrh	r2, [r3, #0]
 8003670:	4b24      	ldr	r3, [pc, #144]	; (8003704 <VL53L0X_hw_config+0x160>)
 8003672:	00d2      	lsls	r2, r2, #3
 8003674:	58d0      	ldr	r0, [r2, r3]
 8003676:	193b      	adds	r3, r7, r4
 8003678:	881b      	ldrh	r3, [r3, #0]
 800367a:	4a22      	ldr	r2, [pc, #136]	; (8003704 <VL53L0X_hw_config+0x160>)
 800367c:	00db      	lsls	r3, r3, #3
 800367e:	18d3      	adds	r3, r2, r3
 8003680:	3304      	adds	r3, #4
 8003682:	681b      	ldr	r3, [r3, #0]
 8003684:	0019      	movs	r1, r3
 8003686:	f7ff fe45 	bl	8003314 <LL_GPIO_ResetOutputPin>
 800368a:	193b      	adds	r3, r7, r4
 800368c:	881a      	ldrh	r2, [r3, #0]
 800368e:	193b      	adds	r3, r7, r4
 8003690:	3201      	adds	r2, #1
 8003692:	801a      	strh	r2, [r3, #0]
 8003694:	230e      	movs	r3, #14
 8003696:	18fb      	adds	r3, r7, r3
 8003698:	881b      	ldrh	r3, [r3, #0]
 800369a:	2b02      	cmp	r3, #2
 800369c:	d9c5      	bls.n	800362a <VL53L0X_hw_config+0x86>
 800369e:	687b      	ldr	r3, [r7, #4]
 80036a0:	4a18      	ldr	r2, [pc, #96]	; (8003704 <VL53L0X_hw_config+0x160>)
 80036a2:	601a      	str	r2, [r3, #0]
 80036a4:	2010      	movs	r0, #16
 80036a6:	f7ff ff59 	bl	800355c <LL_RCC_SetI2CClockSource>
 80036aa:	2380      	movs	r3, #128	; 0x80
 80036ac:	039b      	lsls	r3, r3, #14
 80036ae:	0018      	movs	r0, r3
 80036b0:	f7ff ff3e 	bl	8003530 <LL_APB1_GRP1_EnableClock>
 80036b4:	4b14      	ldr	r3, [pc, #80]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036b6:	0018      	movs	r0, r3
 80036b8:	f7ff fe6a 	bl	8003390 <LL_I2C_DisableAnalogFilter>
 80036bc:	4b12      	ldr	r3, [pc, #72]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036be:	2101      	movs	r1, #1
 80036c0:	0018      	movs	r0, r3
 80036c2:	f7ff fe51 	bl	8003368 <LL_I2C_SetDigitalFilter>
 80036c6:	4a11      	ldr	r2, [pc, #68]	; (800370c <VL53L0X_hw_config+0x168>)
 80036c8:	4b0f      	ldr	r3, [pc, #60]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036ca:	0011      	movs	r1, r2
 80036cc:	0018      	movs	r0, r3
 80036ce:	f7ff fe91 	bl	80033f4 <LL_I2C_SetTiming>
 80036d2:	4b0d      	ldr	r3, [pc, #52]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036d4:	0018      	movs	r0, r3
 80036d6:	f7ff fe6a 	bl	80033ae <LL_I2C_DisableClockStretching>
 80036da:	4b0b      	ldr	r3, [pc, #44]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036dc:	2100      	movs	r1, #0
 80036de:	0018      	movs	r0, r3
 80036e0:	f7ff fe74 	bl	80033cc <LL_I2C_SetMasterAddressingMode>
 80036e4:	4b08      	ldr	r3, [pc, #32]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036e6:	2100      	movs	r1, #0
 80036e8:	0018      	movs	r0, r3
 80036ea:	f7ff fe8f 	bl	800340c <LL_I2C_SetMode>
 80036ee:	4b06      	ldr	r3, [pc, #24]	; (8003708 <VL53L0X_hw_config+0x164>)
 80036f0:	0018      	movs	r0, r3
 80036f2:	f7ff fe1b 	bl	800332c <LL_I2C_Enable>
 80036f6:	46c0      	nop			; (mov r8, r8)
 80036f8:	46bd      	mov	sp, r7
 80036fa:	b005      	add	sp, #20
 80036fc:	bd90      	pop	{r4, r7, pc}
 80036fe:	46c0      	nop			; (mov r8, r8)
 8003700:	48000400 	.word	0x48000400
 8003704:	08009f40 	.word	0x08009f40
 8003708:	40005400 	.word	0x40005400
 800370c:	50330309 	.word	0x50330309

08003710 <VL53L0X_WriteMulti>:
 8003710:	b590      	push	{r4, r7, lr}
 8003712:	b089      	sub	sp, #36	; 0x24
 8003714:	af02      	add	r7, sp, #8
 8003716:	60f8      	str	r0, [r7, #12]
 8003718:	607a      	str	r2, [r7, #4]
 800371a:	603b      	str	r3, [r7, #0]
 800371c:	230b      	movs	r3, #11
 800371e:	18fb      	adds	r3, r7, r3
 8003720:	1c0a      	adds	r2, r1, #0
 8003722:	701a      	strb	r2, [r3, #0]
 8003724:	4b38      	ldr	r3, [pc, #224]	; (8003808 <VL53L0X_WriteMulti+0xf8>)
 8003726:	613b      	str	r3, [r7, #16]
 8003728:	68fa      	ldr	r2, [r7, #12]
 800372a:	239e      	movs	r3, #158	; 0x9e
 800372c:	005b      	lsls	r3, r3, #1
 800372e:	5cd3      	ldrb	r3, [r2, r3]
 8003730:	0019      	movs	r1, r3
 8003732:	683b      	ldr	r3, [r7, #0]
 8003734:	1c5a      	adds	r2, r3, #1
 8003736:	4835      	ldr	r0, [pc, #212]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 8003738:	2380      	movs	r3, #128	; 0x80
 800373a:	019b      	lsls	r3, r3, #6
 800373c:	9301      	str	r3, [sp, #4]
 800373e:	2380      	movs	r3, #128	; 0x80
 8003740:	049b      	lsls	r3, r3, #18
 8003742:	9300      	str	r3, [sp, #0]
 8003744:	0013      	movs	r3, r2
 8003746:	2200      	movs	r2, #0
 8003748:	f7ff fea4 	bl	8003494 <LL_I2C_HandleTransfer>
 800374c:	46c0      	nop			; (mov r8, r8)
 800374e:	4b2f      	ldr	r3, [pc, #188]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 8003750:	0018      	movs	r0, r3
 8003752:	f7ff fe6f 	bl	8003434 <LL_I2C_IsActiveFlag_TXIS>
 8003756:	1e03      	subs	r3, r0, #0
 8003758:	d104      	bne.n	8003764 <VL53L0X_WriteMulti+0x54>
 800375a:	693b      	ldr	r3, [r7, #16]
 800375c:	1e5a      	subs	r2, r3, #1
 800375e:	613a      	str	r2, [r7, #16]
 8003760:	2b00      	cmp	r3, #0
 8003762:	dcf4      	bgt.n	800374e <VL53L0X_WriteMulti+0x3e>
 8003764:	693b      	ldr	r3, [r7, #16]
 8003766:	2b00      	cmp	r3, #0
 8003768:	dc02      	bgt.n	8003770 <VL53L0X_WriteMulti+0x60>
 800376a:	2307      	movs	r3, #7
 800376c:	425b      	negs	r3, r3
 800376e:	e047      	b.n	8003800 <VL53L0X_WriteMulti+0xf0>
 8003770:	230b      	movs	r3, #11
 8003772:	18fb      	adds	r3, r7, r3
 8003774:	781b      	ldrb	r3, [r3, #0]
 8003776:	4a25      	ldr	r2, [pc, #148]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 8003778:	0019      	movs	r1, r3
 800377a:	0010      	movs	r0, r2
 800377c:	f7ff feb3 	bl	80034e6 <LL_I2C_TransmitData8>
 8003780:	2317      	movs	r3, #23
 8003782:	18fb      	adds	r3, r7, r3
 8003784:	2200      	movs	r2, #0
 8003786:	701a      	strb	r2, [r3, #0]
 8003788:	e021      	b.n	80037ce <VL53L0X_WriteMulti+0xbe>
 800378a:	46c0      	nop			; (mov r8, r8)
 800378c:	4b1f      	ldr	r3, [pc, #124]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 800378e:	0018      	movs	r0, r3
 8003790:	f7ff fe50 	bl	8003434 <LL_I2C_IsActiveFlag_TXIS>
 8003794:	1e03      	subs	r3, r0, #0
 8003796:	d104      	bne.n	80037a2 <VL53L0X_WriteMulti+0x92>
 8003798:	693b      	ldr	r3, [r7, #16]
 800379a:	1e5a      	subs	r2, r3, #1
 800379c:	613a      	str	r2, [r7, #16]
 800379e:	2b00      	cmp	r3, #0
 80037a0:	dcf4      	bgt.n	800378c <VL53L0X_WriteMulti+0x7c>
 80037a2:	693b      	ldr	r3, [r7, #16]
 80037a4:	2b00      	cmp	r3, #0
 80037a6:	dc02      	bgt.n	80037ae <VL53L0X_WriteMulti+0x9e>
 80037a8:	2307      	movs	r3, #7
 80037aa:	425b      	negs	r3, r3
 80037ac:	e028      	b.n	8003800 <VL53L0X_WriteMulti+0xf0>
 80037ae:	2417      	movs	r4, #23
 80037b0:	193b      	adds	r3, r7, r4
 80037b2:	781b      	ldrb	r3, [r3, #0]
 80037b4:	687a      	ldr	r2, [r7, #4]
 80037b6:	18d3      	adds	r3, r2, r3
 80037b8:	781b      	ldrb	r3, [r3, #0]
 80037ba:	4a14      	ldr	r2, [pc, #80]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 80037bc:	0019      	movs	r1, r3
 80037be:	0010      	movs	r0, r2
 80037c0:	f7ff fe91 	bl	80034e6 <LL_I2C_TransmitData8>
 80037c4:	193b      	adds	r3, r7, r4
 80037c6:	781a      	ldrb	r2, [r3, #0]
 80037c8:	193b      	adds	r3, r7, r4
 80037ca:	3201      	adds	r2, #1
 80037cc:	701a      	strb	r2, [r3, #0]
 80037ce:	2317      	movs	r3, #23
 80037d0:	18fb      	adds	r3, r7, r3
 80037d2:	781b      	ldrb	r3, [r3, #0]
 80037d4:	683a      	ldr	r2, [r7, #0]
 80037d6:	429a      	cmp	r2, r3
 80037d8:	d8d7      	bhi.n	800378a <VL53L0X_WriteMulti+0x7a>
 80037da:	46c0      	nop			; (mov r8, r8)
 80037dc:	4b0b      	ldr	r3, [pc, #44]	; (800380c <VL53L0X_WriteMulti+0xfc>)
 80037de:	0018      	movs	r0, r3
 80037e0:	f7ff fe48 	bl	8003474 <LL_I2C_IsActiveFlag_TC>
 80037e4:	1e03      	subs	r3, r0, #0
 80037e6:	d004      	beq.n	80037f2 <VL53L0X_WriteMulti+0xe2>
 80037e8:	693b      	ldr	r3, [r7, #16]
 80037ea:	1e5a      	subs	r2, r3, #1
 80037ec:	613a      	str	r2, [r7, #16]
 80037ee:	2b00      	cmp	r3, #0
 80037f0:	dcf4      	bgt.n	80037dc <VL53L0X_WriteMulti+0xcc>
 80037f2:	693b      	ldr	r3, [r7, #16]
 80037f4:	2b00      	cmp	r3, #0
 80037f6:	dc02      	bgt.n	80037fe <VL53L0X_WriteMulti+0xee>
 80037f8:	2307      	movs	r3, #7
 80037fa:	425b      	negs	r3, r3
 80037fc:	e000      	b.n	8003800 <VL53L0X_WriteMulti+0xf0>
 80037fe:	2300      	movs	r3, #0
 8003800:	0018      	movs	r0, r3
 8003802:	46bd      	mov	sp, r7
 8003804:	b007      	add	sp, #28
 8003806:	bd90      	pop	{r4, r7, pc}
 8003808:	000005dc 	.word	0x000005dc
 800380c:	40005400 	.word	0x40005400

08003810 <VL53L0X_ReadMulti>:
 8003810:	b5b0      	push	{r4, r5, r7, lr}
 8003812:	b088      	sub	sp, #32
 8003814:	af02      	add	r7, sp, #8
 8003816:	60f8      	str	r0, [r7, #12]
 8003818:	607a      	str	r2, [r7, #4]
 800381a:	603b      	str	r3, [r7, #0]
 800381c:	200b      	movs	r0, #11
 800381e:	183b      	adds	r3, r7, r0
 8003820:	1c0a      	adds	r2, r1, #0
 8003822:	701a      	strb	r2, [r3, #0]
 8003824:	4b31      	ldr	r3, [pc, #196]	; (80038ec <VL53L0X_ReadMulti+0xdc>)
 8003826:	613b      	str	r3, [r7, #16]
 8003828:	183b      	adds	r3, r7, r0
 800382a:	7819      	ldrb	r1, [r3, #0]
 800382c:	68f8      	ldr	r0, [r7, #12]
 800382e:	2300      	movs	r3, #0
 8003830:	2200      	movs	r2, #0
 8003832:	f7ff ff6d 	bl	8003710 <VL53L0X_WriteMulti>
 8003836:	0003      	movs	r3, r0
 8003838:	3307      	adds	r3, #7
 800383a:	d102      	bne.n	8003842 <VL53L0X_ReadMulti+0x32>
 800383c:	2307      	movs	r3, #7
 800383e:	425b      	negs	r3, r3
 8003840:	e050      	b.n	80038e4 <VL53L0X_ReadMulti+0xd4>
 8003842:	68fa      	ldr	r2, [r7, #12]
 8003844:	239e      	movs	r3, #158	; 0x9e
 8003846:	005b      	lsls	r3, r3, #1
 8003848:	5cd3      	ldrb	r3, [r2, r3]
 800384a:	0019      	movs	r1, r3
 800384c:	683a      	ldr	r2, [r7, #0]
 800384e:	4828      	ldr	r0, [pc, #160]	; (80038f0 <VL53L0X_ReadMulti+0xe0>)
 8003850:	2390      	movs	r3, #144	; 0x90
 8003852:	019b      	lsls	r3, r3, #6
 8003854:	9301      	str	r3, [sp, #4]
 8003856:	2380      	movs	r3, #128	; 0x80
 8003858:	049b      	lsls	r3, r3, #18
 800385a:	9300      	str	r3, [sp, #0]
 800385c:	0013      	movs	r3, r2
 800385e:	2200      	movs	r2, #0
 8003860:	f7ff fe18 	bl	8003494 <LL_I2C_HandleTransfer>
 8003864:	2317      	movs	r3, #23
 8003866:	18fb      	adds	r3, r7, r3
 8003868:	2200      	movs	r2, #0
 800386a:	701a      	strb	r2, [r3, #0]
 800386c:	e021      	b.n	80038b2 <VL53L0X_ReadMulti+0xa2>
 800386e:	46c0      	nop			; (mov r8, r8)
 8003870:	4b1f      	ldr	r3, [pc, #124]	; (80038f0 <VL53L0X_ReadMulti+0xe0>)
 8003872:	0018      	movs	r0, r3
 8003874:	f7ff fdee 	bl	8003454 <LL_I2C_IsActiveFlag_RXNE>
 8003878:	1e03      	subs	r3, r0, #0
 800387a:	d104      	bne.n	8003886 <VL53L0X_ReadMulti+0x76>
 800387c:	693b      	ldr	r3, [r7, #16]
 800387e:	1e5a      	subs	r2, r3, #1
 8003880:	613a      	str	r2, [r7, #16]
 8003882:	2b00      	cmp	r3, #0
 8003884:	dcf4      	bgt.n	8003870 <VL53L0X_ReadMulti+0x60>
 8003886:	693b      	ldr	r3, [r7, #16]
 8003888:	2b00      	cmp	r3, #0
 800388a:	dc02      	bgt.n	8003892 <VL53L0X_ReadMulti+0x82>
 800388c:	2307      	movs	r3, #7
 800388e:	425b      	negs	r3, r3
 8003890:	e028      	b.n	80038e4 <VL53L0X_ReadMulti+0xd4>
 8003892:	2517      	movs	r5, #23
 8003894:	197b      	adds	r3, r7, r5
 8003896:	781b      	ldrb	r3, [r3, #0]
 8003898:	687a      	ldr	r2, [r7, #4]
 800389a:	18d4      	adds	r4, r2, r3
 800389c:	4b14      	ldr	r3, [pc, #80]	; (80038f0 <VL53L0X_ReadMulti+0xe0>)
 800389e:	0018      	movs	r0, r3
 80038a0:	f7ff fe16 	bl	80034d0 <LL_I2C_ReceiveData8>
 80038a4:	0003      	movs	r3, r0
 80038a6:	7023      	strb	r3, [r4, #0]
 80038a8:	197b      	adds	r3, r7, r5
 80038aa:	781a      	ldrb	r2, [r3, #0]
 80038ac:	197b      	adds	r3, r7, r5
 80038ae:	3201      	adds	r2, #1
 80038b0:	701a      	strb	r2, [r3, #0]
 80038b2:	2317      	movs	r3, #23
 80038b4:	18fb      	adds	r3, r7, r3
 80038b6:	781b      	ldrb	r3, [r3, #0]
 80038b8:	683a      	ldr	r2, [r7, #0]
 80038ba:	429a      	cmp	r2, r3
 80038bc:	d8d7      	bhi.n	800386e <VL53L0X_ReadMulti+0x5e>
 80038be:	46c0      	nop			; (mov r8, r8)
 80038c0:	4b0b      	ldr	r3, [pc, #44]	; (80038f0 <VL53L0X_ReadMulti+0xe0>)
 80038c2:	0018      	movs	r0, r3
 80038c4:	f7ff fdd6 	bl	8003474 <LL_I2C_IsActiveFlag_TC>
 80038c8:	1e03      	subs	r3, r0, #0
 80038ca:	d004      	beq.n	80038d6 <VL53L0X_ReadMulti+0xc6>
 80038cc:	693b      	ldr	r3, [r7, #16]
 80038ce:	1e5a      	subs	r2, r3, #1
 80038d0:	613a      	str	r2, [r7, #16]
 80038d2:	2b00      	cmp	r3, #0
 80038d4:	dcf4      	bgt.n	80038c0 <VL53L0X_ReadMulti+0xb0>
 80038d6:	693b      	ldr	r3, [r7, #16]
 80038d8:	2b00      	cmp	r3, #0
 80038da:	dc02      	bgt.n	80038e2 <VL53L0X_ReadMulti+0xd2>
 80038dc:	2307      	movs	r3, #7
 80038de:	425b      	negs	r3, r3
 80038e0:	e000      	b.n	80038e4 <VL53L0X_ReadMulti+0xd4>
 80038e2:	2300      	movs	r3, #0
 80038e4:	0018      	movs	r0, r3
 80038e6:	46bd      	mov	sp, r7
 80038e8:	b006      	add	sp, #24
 80038ea:	bdb0      	pop	{r4, r5, r7, pc}
 80038ec:	000005dc 	.word	0x000005dc
 80038f0:	40005400 	.word	0x40005400

080038f4 <VL53L0X_WrByte>:
 80038f4:	b580      	push	{r7, lr}
 80038f6:	b082      	sub	sp, #8
 80038f8:	af00      	add	r7, sp, #0
 80038fa:	6078      	str	r0, [r7, #4]
 80038fc:	0008      	movs	r0, r1
 80038fe:	0011      	movs	r1, r2
 8003900:	1cfb      	adds	r3, r7, #3
 8003902:	1c02      	adds	r2, r0, #0
 8003904:	701a      	strb	r2, [r3, #0]
 8003906:	1cbb      	adds	r3, r7, #2
 8003908:	1c0a      	adds	r2, r1, #0
 800390a:	701a      	strb	r2, [r3, #0]
 800390c:	1cba      	adds	r2, r7, #2
 800390e:	1cfb      	adds	r3, r7, #3
 8003910:	7819      	ldrb	r1, [r3, #0]
 8003912:	6878      	ldr	r0, [r7, #4]
 8003914:	2301      	movs	r3, #1
 8003916:	f7ff fefb 	bl	8003710 <VL53L0X_WriteMulti>
 800391a:	2300      	movs	r3, #0
 800391c:	0018      	movs	r0, r3
 800391e:	46bd      	mov	sp, r7
 8003920:	b002      	add	sp, #8
 8003922:	bd80      	pop	{r7, pc}

08003924 <VL53L0X_WrWord>:
 8003924:	b580      	push	{r7, lr}
 8003926:	b084      	sub	sp, #16
 8003928:	af00      	add	r7, sp, #0
 800392a:	6078      	str	r0, [r7, #4]
 800392c:	0008      	movs	r0, r1
 800392e:	0011      	movs	r1, r2
 8003930:	1cfb      	adds	r3, r7, #3
 8003932:	1c02      	adds	r2, r0, #0
 8003934:	701a      	strb	r2, [r3, #0]
 8003936:	003b      	movs	r3, r7
 8003938:	1c0a      	adds	r2, r1, #0
 800393a:	801a      	strh	r2, [r3, #0]
 800393c:	003b      	movs	r3, r7
 800393e:	881b      	ldrh	r3, [r3, #0]
 8003940:	0a1b      	lsrs	r3, r3, #8
 8003942:	b29b      	uxth	r3, r3
 8003944:	b2da      	uxtb	r2, r3
 8003946:	210c      	movs	r1, #12
 8003948:	187b      	adds	r3, r7, r1
 800394a:	701a      	strb	r2, [r3, #0]
 800394c:	003b      	movs	r3, r7
 800394e:	881b      	ldrh	r3, [r3, #0]
 8003950:	b2da      	uxtb	r2, r3
 8003952:	187b      	adds	r3, r7, r1
 8003954:	705a      	strb	r2, [r3, #1]
 8003956:	187a      	adds	r2, r7, r1
 8003958:	1cfb      	adds	r3, r7, #3
 800395a:	7819      	ldrb	r1, [r3, #0]
 800395c:	6878      	ldr	r0, [r7, #4]
 800395e:	2302      	movs	r3, #2
 8003960:	f7ff fed6 	bl	8003710 <VL53L0X_WriteMulti>
 8003964:	2300      	movs	r3, #0
 8003966:	0018      	movs	r0, r3
 8003968:	46bd      	mov	sp, r7
 800396a:	b004      	add	sp, #16
 800396c:	bd80      	pop	{r7, pc}

0800396e <VL53L0X_UpdateByte>:
 800396e:	b590      	push	{r4, r7, lr}
 8003970:	b085      	sub	sp, #20
 8003972:	af00      	add	r7, sp, #0
 8003974:	6078      	str	r0, [r7, #4]
 8003976:	000c      	movs	r4, r1
 8003978:	0010      	movs	r0, r2
 800397a:	0019      	movs	r1, r3
 800397c:	1cfb      	adds	r3, r7, #3
 800397e:	1c22      	adds	r2, r4, #0
 8003980:	701a      	strb	r2, [r3, #0]
 8003982:	1cbb      	adds	r3, r7, #2
 8003984:	1c02      	adds	r2, r0, #0
 8003986:	701a      	strb	r2, [r3, #0]
 8003988:	1c7b      	adds	r3, r7, #1
 800398a:	1c0a      	adds	r2, r1, #0
 800398c:	701a      	strb	r2, [r3, #0]
 800398e:	240f      	movs	r4, #15
 8003990:	193a      	adds	r2, r7, r4
 8003992:	1cfb      	adds	r3, r7, #3
 8003994:	7819      	ldrb	r1, [r3, #0]
 8003996:	6878      	ldr	r0, [r7, #4]
 8003998:	2301      	movs	r3, #1
 800399a:	f7ff ff39 	bl	8003810 <VL53L0X_ReadMulti>
 800399e:	0021      	movs	r1, r4
 80039a0:	187b      	adds	r3, r7, r1
 80039a2:	781b      	ldrb	r3, [r3, #0]
 80039a4:	1cba      	adds	r2, r7, #2
 80039a6:	7812      	ldrb	r2, [r2, #0]
 80039a8:	4013      	ands	r3, r2
 80039aa:	b2da      	uxtb	r2, r3
 80039ac:	1c7b      	adds	r3, r7, #1
 80039ae:	781b      	ldrb	r3, [r3, #0]
 80039b0:	4313      	orrs	r3, r2
 80039b2:	b2da      	uxtb	r2, r3
 80039b4:	187b      	adds	r3, r7, r1
 80039b6:	701a      	strb	r2, [r3, #0]
 80039b8:	187a      	adds	r2, r7, r1
 80039ba:	1cfb      	adds	r3, r7, #3
 80039bc:	7819      	ldrb	r1, [r3, #0]
 80039be:	6878      	ldr	r0, [r7, #4]
 80039c0:	2301      	movs	r3, #1
 80039c2:	f7ff fea5 	bl	8003710 <VL53L0X_WriteMulti>
 80039c6:	2300      	movs	r3, #0
 80039c8:	0018      	movs	r0, r3
 80039ca:	46bd      	mov	sp, r7
 80039cc:	b005      	add	sp, #20
 80039ce:	bd90      	pop	{r4, r7, pc}

080039d0 <VL53L0X_RdByte>:
 80039d0:	b580      	push	{r7, lr}
 80039d2:	b084      	sub	sp, #16
 80039d4:	af00      	add	r7, sp, #0
 80039d6:	60f8      	str	r0, [r7, #12]
 80039d8:	607a      	str	r2, [r7, #4]
 80039da:	200b      	movs	r0, #11
 80039dc:	183b      	adds	r3, r7, r0
 80039de:	1c0a      	adds	r2, r1, #0
 80039e0:	701a      	strb	r2, [r3, #0]
 80039e2:	687a      	ldr	r2, [r7, #4]
 80039e4:	183b      	adds	r3, r7, r0
 80039e6:	7819      	ldrb	r1, [r3, #0]
 80039e8:	68f8      	ldr	r0, [r7, #12]
 80039ea:	2301      	movs	r3, #1
 80039ec:	f7ff ff10 	bl	8003810 <VL53L0X_ReadMulti>
 80039f0:	2300      	movs	r3, #0
 80039f2:	0018      	movs	r0, r3
 80039f4:	46bd      	mov	sp, r7
 80039f6:	b004      	add	sp, #16
 80039f8:	bd80      	pop	{r7, pc}

080039fa <VL53L0X_RdWord>:
 80039fa:	b590      	push	{r4, r7, lr}
 80039fc:	b087      	sub	sp, #28
 80039fe:	af00      	add	r7, sp, #0
 8003a00:	60f8      	str	r0, [r7, #12]
 8003a02:	607a      	str	r2, [r7, #4]
 8003a04:	200b      	movs	r0, #11
 8003a06:	183b      	adds	r3, r7, r0
 8003a08:	1c0a      	adds	r2, r1, #0
 8003a0a:	701a      	strb	r2, [r3, #0]
 8003a0c:	2414      	movs	r4, #20
 8003a0e:	193a      	adds	r2, r7, r4
 8003a10:	183b      	adds	r3, r7, r0
 8003a12:	7819      	ldrb	r1, [r3, #0]
 8003a14:	68f8      	ldr	r0, [r7, #12]
 8003a16:	2302      	movs	r3, #2
 8003a18:	f7ff fefa 	bl	8003810 <VL53L0X_ReadMulti>
 8003a1c:	0021      	movs	r1, r4
 8003a1e:	187b      	adds	r3, r7, r1
 8003a20:	781b      	ldrb	r3, [r3, #0]
 8003a22:	b29b      	uxth	r3, r3
 8003a24:	021b      	lsls	r3, r3, #8
 8003a26:	b29a      	uxth	r2, r3
 8003a28:	187b      	adds	r3, r7, r1
 8003a2a:	785b      	ldrb	r3, [r3, #1]
 8003a2c:	b29b      	uxth	r3, r3
 8003a2e:	18d3      	adds	r3, r2, r3
 8003a30:	b29a      	uxth	r2, r3
 8003a32:	687b      	ldr	r3, [r7, #4]
 8003a34:	801a      	strh	r2, [r3, #0]
 8003a36:	2300      	movs	r3, #0
 8003a38:	0018      	movs	r0, r3
 8003a3a:	46bd      	mov	sp, r7
 8003a3c:	b007      	add	sp, #28
 8003a3e:	bd90      	pop	{r4, r7, pc}

08003a40 <VL53L0X_RdDWord>:
 8003a40:	b590      	push	{r4, r7, lr}
 8003a42:	b087      	sub	sp, #28
 8003a44:	af00      	add	r7, sp, #0
 8003a46:	60f8      	str	r0, [r7, #12]
 8003a48:	607a      	str	r2, [r7, #4]
 8003a4a:	200b      	movs	r0, #11
 8003a4c:	183b      	adds	r3, r7, r0
 8003a4e:	1c0a      	adds	r2, r1, #0
 8003a50:	701a      	strb	r2, [r3, #0]
 8003a52:	2414      	movs	r4, #20
 8003a54:	193a      	adds	r2, r7, r4
 8003a56:	183b      	adds	r3, r7, r0
 8003a58:	7819      	ldrb	r1, [r3, #0]
 8003a5a:	68f8      	ldr	r0, [r7, #12]
 8003a5c:	2304      	movs	r3, #4
 8003a5e:	f7ff fed7 	bl	8003810 <VL53L0X_ReadMulti>
 8003a62:	0021      	movs	r1, r4
 8003a64:	187b      	adds	r3, r7, r1
 8003a66:	781b      	ldrb	r3, [r3, #0]
 8003a68:	061a      	lsls	r2, r3, #24
 8003a6a:	187b      	adds	r3, r7, r1
 8003a6c:	785b      	ldrb	r3, [r3, #1]
 8003a6e:	041b      	lsls	r3, r3, #16
 8003a70:	18d2      	adds	r2, r2, r3
 8003a72:	187b      	adds	r3, r7, r1
 8003a74:	789b      	ldrb	r3, [r3, #2]
 8003a76:	021b      	lsls	r3, r3, #8
 8003a78:	18d3      	adds	r3, r2, r3
 8003a7a:	187a      	adds	r2, r7, r1
 8003a7c:	78d2      	ldrb	r2, [r2, #3]
 8003a7e:	189a      	adds	r2, r3, r2
 8003a80:	687b      	ldr	r3, [r7, #4]
 8003a82:	601a      	str	r2, [r3, #0]
 8003a84:	2300      	movs	r3, #0
 8003a86:	0018      	movs	r0, r3
 8003a88:	46bd      	mov	sp, r7
 8003a8a:	b007      	add	sp, #28
 8003a8c:	bd90      	pop	{r4, r7, pc}
	...

08003a90 <VL53L0X_PollingDelay>:
 8003a90:	b580      	push	{r7, lr}
 8003a92:	b084      	sub	sp, #16
 8003a94:	af00      	add	r7, sp, #0
 8003a96:	6078      	str	r0, [r7, #4]
 8003a98:	4b06      	ldr	r3, [pc, #24]	; (8003ab4 <VL53L0X_PollingDelay+0x24>)
 8003a9a:	60fb      	str	r3, [r7, #12]
 8003a9c:	46c0      	nop			; (mov r8, r8)
 8003a9e:	68fb      	ldr	r3, [r7, #12]
 8003aa0:	1e5a      	subs	r2, r3, #1
 8003aa2:	60fa      	str	r2, [r7, #12]
 8003aa4:	2b00      	cmp	r3, #0
 8003aa6:	d1fa      	bne.n	8003a9e <VL53L0X_PollingDelay+0xe>
 8003aa8:	2300      	movs	r3, #0
 8003aaa:	0018      	movs	r0, r3
 8003aac:	46bd      	mov	sp, r7
 8003aae:	b004      	add	sp, #16
 8003ab0:	bd80      	pop	{r7, pc}
 8003ab2:	46c0      	nop			; (mov r8, r8)
 8003ab4:	00001388 	.word	0x00001388

08003ab8 <xputc>:
 8003ab8:	b580      	push	{r7, lr}
 8003aba:	b082      	sub	sp, #8
 8003abc:	af00      	add	r7, sp, #0
 8003abe:	0002      	movs	r2, r0
 8003ac0:	1dfb      	adds	r3, r7, #7
 8003ac2:	701a      	strb	r2, [r3, #0]
 8003ac4:	1dfb      	adds	r3, r7, #7
 8003ac6:	781b      	ldrb	r3, [r3, #0]
 8003ac8:	2b0a      	cmp	r3, #10
 8003aca:	d102      	bne.n	8003ad2 <xputc+0x1a>
 8003acc:	200d      	movs	r0, #13
 8003ace:	f7ff fff3 	bl	8003ab8 <xputc>
 8003ad2:	4b0d      	ldr	r3, [pc, #52]	; (8003b08 <xputc+0x50>)
 8003ad4:	681b      	ldr	r3, [r3, #0]
 8003ad6:	2b00      	cmp	r3, #0
 8003ad8:	d008      	beq.n	8003aec <xputc+0x34>
 8003ada:	4b0b      	ldr	r3, [pc, #44]	; (8003b08 <xputc+0x50>)
 8003adc:	681b      	ldr	r3, [r3, #0]
 8003ade:	1c59      	adds	r1, r3, #1
 8003ae0:	4a09      	ldr	r2, [pc, #36]	; (8003b08 <xputc+0x50>)
 8003ae2:	6011      	str	r1, [r2, #0]
 8003ae4:	1dfa      	adds	r2, r7, #7
 8003ae6:	7812      	ldrb	r2, [r2, #0]
 8003ae8:	701a      	strb	r2, [r3, #0]
 8003aea:	e009      	b.n	8003b00 <xputc+0x48>
 8003aec:	4b07      	ldr	r3, [pc, #28]	; (8003b0c <xputc+0x54>)
 8003aee:	681b      	ldr	r3, [r3, #0]
 8003af0:	2b00      	cmp	r3, #0
 8003af2:	d005      	beq.n	8003b00 <xputc+0x48>
 8003af4:	4b05      	ldr	r3, [pc, #20]	; (8003b0c <xputc+0x54>)
 8003af6:	681b      	ldr	r3, [r3, #0]
 8003af8:	1dfa      	adds	r2, r7, #7
 8003afa:	7812      	ldrb	r2, [r2, #0]
 8003afc:	0010      	movs	r0, r2
 8003afe:	4798      	blx	r3
 8003b00:	46bd      	mov	sp, r7
 8003b02:	b002      	add	sp, #8
 8003b04:	bd80      	pop	{r7, pc}
 8003b06:	46c0      	nop			; (mov r8, r8)
 8003b08:	20001154 	.word	0x20001154
 8003b0c:	2000115c 	.word	0x2000115c

08003b10 <xputs>:
 8003b10:	b580      	push	{r7, lr}
 8003b12:	b082      	sub	sp, #8
 8003b14:	af00      	add	r7, sp, #0
 8003b16:	6078      	str	r0, [r7, #4]
 8003b18:	e006      	b.n	8003b28 <xputs+0x18>
 8003b1a:	687b      	ldr	r3, [r7, #4]
 8003b1c:	1c5a      	adds	r2, r3, #1
 8003b1e:	607a      	str	r2, [r7, #4]
 8003b20:	781b      	ldrb	r3, [r3, #0]
 8003b22:	0018      	movs	r0, r3
 8003b24:	f7ff ffc8 	bl	8003ab8 <xputc>
 8003b28:	687b      	ldr	r3, [r7, #4]
 8003b2a:	781b      	ldrb	r3, [r3, #0]
 8003b2c:	2b00      	cmp	r3, #0
 8003b2e:	d1f4      	bne.n	8003b1a <xputs+0xa>
 8003b30:	46c0      	nop			; (mov r8, r8)
 8003b32:	46bd      	mov	sp, r7
 8003b34:	b002      	add	sp, #8
 8003b36:	bd80      	pop	{r7, pc}

08003b38 <xvprintf>:
 8003b38:	b590      	push	{r4, r7, lr}
 8003b3a:	b093      	sub	sp, #76	; 0x4c
 8003b3c:	af00      	add	r7, sp, #0
 8003b3e:	6078      	str	r0, [r7, #4]
 8003b40:	6039      	str	r1, [r7, #0]
 8003b42:	687b      	ldr	r3, [r7, #4]
 8003b44:	1c5a      	adds	r2, r3, #1
 8003b46:	607a      	str	r2, [r7, #4]
 8003b48:	2133      	movs	r1, #51	; 0x33
 8003b4a:	187a      	adds	r2, r7, r1
 8003b4c:	781b      	ldrb	r3, [r3, #0]
 8003b4e:	7013      	strb	r3, [r2, #0]
 8003b50:	000a      	movs	r2, r1
 8003b52:	18bb      	adds	r3, r7, r2
 8003b54:	781b      	ldrb	r3, [r3, #0]
 8003b56:	2b00      	cmp	r3, #0
 8003b58:	d100      	bne.n	8003b5c <xvprintf+0x24>
 8003b5a:	e167      	b.n	8003e2c <xvprintf+0x2f4>
 8003b5c:	18bb      	adds	r3, r7, r2
 8003b5e:	781b      	ldrb	r3, [r3, #0]
 8003b60:	2b25      	cmp	r3, #37	; 0x25
 8003b62:	d005      	beq.n	8003b70 <xvprintf+0x38>
 8003b64:	18bb      	adds	r3, r7, r2
 8003b66:	781b      	ldrb	r3, [r3, #0]
 8003b68:	0018      	movs	r0, r3
 8003b6a:	f7ff ffa5 	bl	8003ab8 <xputc>
 8003b6e:	e15c      	b.n	8003e2a <xvprintf+0x2f2>
 8003b70:	2300      	movs	r3, #0
 8003b72:	637b      	str	r3, [r7, #52]	; 0x34
 8003b74:	687b      	ldr	r3, [r7, #4]
 8003b76:	1c5a      	adds	r2, r3, #1
 8003b78:	607a      	str	r2, [r7, #4]
 8003b7a:	2133      	movs	r1, #51	; 0x33
 8003b7c:	187a      	adds	r2, r7, r1
 8003b7e:	781b      	ldrb	r3, [r3, #0]
 8003b80:	7013      	strb	r3, [r2, #0]
 8003b82:	187b      	adds	r3, r7, r1
 8003b84:	781b      	ldrb	r3, [r3, #0]
 8003b86:	2b30      	cmp	r3, #48	; 0x30
 8003b88:	d108      	bne.n	8003b9c <xvprintf+0x64>
 8003b8a:	2301      	movs	r3, #1
 8003b8c:	637b      	str	r3, [r7, #52]	; 0x34
 8003b8e:	687b      	ldr	r3, [r7, #4]
 8003b90:	1c5a      	adds	r2, r3, #1
 8003b92:	607a      	str	r2, [r7, #4]
 8003b94:	187a      	adds	r2, r7, r1
 8003b96:	781b      	ldrb	r3, [r3, #0]
 8003b98:	7013      	strb	r3, [r2, #0]
 8003b9a:	e00c      	b.n	8003bb6 <xvprintf+0x7e>
 8003b9c:	2133      	movs	r1, #51	; 0x33
 8003b9e:	187b      	adds	r3, r7, r1
 8003ba0:	781b      	ldrb	r3, [r3, #0]
 8003ba2:	2b2d      	cmp	r3, #45	; 0x2d
 8003ba4:	d107      	bne.n	8003bb6 <xvprintf+0x7e>
 8003ba6:	2302      	movs	r3, #2
 8003ba8:	637b      	str	r3, [r7, #52]	; 0x34
 8003baa:	687b      	ldr	r3, [r7, #4]
 8003bac:	1c5a      	adds	r2, r3, #1
 8003bae:	607a      	str	r2, [r7, #4]
 8003bb0:	187a      	adds	r2, r7, r1
 8003bb2:	781b      	ldrb	r3, [r3, #0]
 8003bb4:	7013      	strb	r3, [r2, #0]
 8003bb6:	2300      	movs	r3, #0
 8003bb8:	63bb      	str	r3, [r7, #56]	; 0x38
 8003bba:	e011      	b.n	8003be0 <xvprintf+0xa8>
 8003bbc:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8003bbe:	0013      	movs	r3, r2
 8003bc0:	009b      	lsls	r3, r3, #2
 8003bc2:	189b      	adds	r3, r3, r2
 8003bc4:	005b      	lsls	r3, r3, #1
 8003bc6:	001a      	movs	r2, r3
 8003bc8:	2133      	movs	r1, #51	; 0x33
 8003bca:	187b      	adds	r3, r7, r1
 8003bcc:	781b      	ldrb	r3, [r3, #0]
 8003bce:	18d3      	adds	r3, r2, r3
 8003bd0:	3b30      	subs	r3, #48	; 0x30
 8003bd2:	63bb      	str	r3, [r7, #56]	; 0x38
 8003bd4:	687b      	ldr	r3, [r7, #4]
 8003bd6:	1c5a      	adds	r2, r3, #1
 8003bd8:	607a      	str	r2, [r7, #4]
 8003bda:	187a      	adds	r2, r7, r1
 8003bdc:	781b      	ldrb	r3, [r3, #0]
 8003bde:	7013      	strb	r3, [r2, #0]
 8003be0:	2233      	movs	r2, #51	; 0x33
 8003be2:	18bb      	adds	r3, r7, r2
 8003be4:	781b      	ldrb	r3, [r3, #0]
 8003be6:	2b2f      	cmp	r3, #47	; 0x2f
 8003be8:	d903      	bls.n	8003bf2 <xvprintf+0xba>
 8003bea:	18bb      	adds	r3, r7, r2
 8003bec:	781b      	ldrb	r3, [r3, #0]
 8003bee:	2b39      	cmp	r3, #57	; 0x39
 8003bf0:	d9e4      	bls.n	8003bbc <xvprintf+0x84>
 8003bf2:	2233      	movs	r2, #51	; 0x33
 8003bf4:	18bb      	adds	r3, r7, r2
 8003bf6:	781b      	ldrb	r3, [r3, #0]
 8003bf8:	2b6c      	cmp	r3, #108	; 0x6c
 8003bfa:	d003      	beq.n	8003c04 <xvprintf+0xcc>
 8003bfc:	18bb      	adds	r3, r7, r2
 8003bfe:	781b      	ldrb	r3, [r3, #0]
 8003c00:	2b4c      	cmp	r3, #76	; 0x4c
 8003c02:	d10a      	bne.n	8003c1a <xvprintf+0xe2>
 8003c04:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003c06:	2204      	movs	r2, #4
 8003c08:	4313      	orrs	r3, r2
 8003c0a:	637b      	str	r3, [r7, #52]	; 0x34
 8003c0c:	687b      	ldr	r3, [r7, #4]
 8003c0e:	1c5a      	adds	r2, r3, #1
 8003c10:	607a      	str	r2, [r7, #4]
 8003c12:	2233      	movs	r2, #51	; 0x33
 8003c14:	18ba      	adds	r2, r7, r2
 8003c16:	781b      	ldrb	r3, [r3, #0]
 8003c18:	7013      	strb	r3, [r2, #0]
 8003c1a:	2233      	movs	r2, #51	; 0x33
 8003c1c:	18bb      	adds	r3, r7, r2
 8003c1e:	781b      	ldrb	r3, [r3, #0]
 8003c20:	2b00      	cmp	r3, #0
 8003c22:	d100      	bne.n	8003c26 <xvprintf+0xee>
 8003c24:	e104      	b.n	8003e30 <xvprintf+0x2f8>
 8003c26:	2132      	movs	r1, #50	; 0x32
 8003c28:	187b      	adds	r3, r7, r1
 8003c2a:	18ba      	adds	r2, r7, r2
 8003c2c:	7812      	ldrb	r2, [r2, #0]
 8003c2e:	701a      	strb	r2, [r3, #0]
 8003c30:	187b      	adds	r3, r7, r1
 8003c32:	781b      	ldrb	r3, [r3, #0]
 8003c34:	2b60      	cmp	r3, #96	; 0x60
 8003c36:	d904      	bls.n	8003c42 <xvprintf+0x10a>
 8003c38:	187b      	adds	r3, r7, r1
 8003c3a:	187a      	adds	r2, r7, r1
 8003c3c:	7812      	ldrb	r2, [r2, #0]
 8003c3e:	3a20      	subs	r2, #32
 8003c40:	701a      	strb	r2, [r3, #0]
 8003c42:	2332      	movs	r3, #50	; 0x32
 8003c44:	18fb      	adds	r3, r7, r3
 8003c46:	781b      	ldrb	r3, [r3, #0]
 8003c48:	3b42      	subs	r3, #66	; 0x42
 8003c4a:	2b16      	cmp	r3, #22
 8003c4c:	d847      	bhi.n	8003cde <xvprintf+0x1a6>
 8003c4e:	009a      	lsls	r2, r3, #2
 8003c50:	4b7a      	ldr	r3, [pc, #488]	; (8003e3c <xvprintf+0x304>)
 8003c52:	18d3      	adds	r3, r2, r3
 8003c54:	681b      	ldr	r3, [r3, #0]
 8003c56:	469f      	mov	pc, r3
 8003c58:	683b      	ldr	r3, [r7, #0]
 8003c5a:	1d1a      	adds	r2, r3, #4
 8003c5c:	603a      	str	r2, [r7, #0]
 8003c5e:	681b      	ldr	r3, [r3, #0]
 8003c60:	627b      	str	r3, [r7, #36]	; 0x24
 8003c62:	2300      	movs	r3, #0
 8003c64:	63fb      	str	r3, [r7, #60]	; 0x3c
 8003c66:	e002      	b.n	8003c6e <xvprintf+0x136>
 8003c68:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003c6a:	3301      	adds	r3, #1
 8003c6c:	63fb      	str	r3, [r7, #60]	; 0x3c
 8003c6e:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8003c70:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003c72:	18d3      	adds	r3, r2, r3
 8003c74:	781b      	ldrb	r3, [r3, #0]
 8003c76:	2b00      	cmp	r3, #0
 8003c78:	d1f6      	bne.n	8003c68 <xvprintf+0x130>
 8003c7a:	e002      	b.n	8003c82 <xvprintf+0x14a>
 8003c7c:	2020      	movs	r0, #32
 8003c7e:	f7ff ff1b 	bl	8003ab8 <xputc>
 8003c82:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003c84:	2202      	movs	r2, #2
 8003c86:	4013      	ands	r3, r2
 8003c88:	d105      	bne.n	8003c96 <xvprintf+0x15e>
 8003c8a:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003c8c:	1c5a      	adds	r2, r3, #1
 8003c8e:	63fa      	str	r2, [r7, #60]	; 0x3c
 8003c90:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8003c92:	429a      	cmp	r2, r3
 8003c94:	d8f2      	bhi.n	8003c7c <xvprintf+0x144>
 8003c96:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003c98:	0018      	movs	r0, r3
 8003c9a:	f7ff ff39 	bl	8003b10 <xputs>
 8003c9e:	e002      	b.n	8003ca6 <xvprintf+0x16e>
 8003ca0:	2020      	movs	r0, #32
 8003ca2:	f7ff ff09 	bl	8003ab8 <xputc>
 8003ca6:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003ca8:	1c5a      	adds	r2, r3, #1
 8003caa:	63fa      	str	r2, [r7, #60]	; 0x3c
 8003cac:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8003cae:	429a      	cmp	r2, r3
 8003cb0:	d8f6      	bhi.n	8003ca0 <xvprintf+0x168>
 8003cb2:	e0ba      	b.n	8003e2a <xvprintf+0x2f2>
 8003cb4:	683b      	ldr	r3, [r7, #0]
 8003cb6:	1d1a      	adds	r2, r3, #4
 8003cb8:	603a      	str	r2, [r7, #0]
 8003cba:	681b      	ldr	r3, [r3, #0]
 8003cbc:	b2db      	uxtb	r3, r3
 8003cbe:	0018      	movs	r0, r3
 8003cc0:	f7ff fefa 	bl	8003ab8 <xputc>
 8003cc4:	e0b1      	b.n	8003e2a <xvprintf+0x2f2>
 8003cc6:	2302      	movs	r3, #2
 8003cc8:	647b      	str	r3, [r7, #68]	; 0x44
 8003cca:	e00f      	b.n	8003cec <xvprintf+0x1b4>
 8003ccc:	2308      	movs	r3, #8
 8003cce:	647b      	str	r3, [r7, #68]	; 0x44
 8003cd0:	e00c      	b.n	8003cec <xvprintf+0x1b4>
 8003cd2:	230a      	movs	r3, #10
 8003cd4:	647b      	str	r3, [r7, #68]	; 0x44
 8003cd6:	e009      	b.n	8003cec <xvprintf+0x1b4>
 8003cd8:	2310      	movs	r3, #16
 8003cda:	647b      	str	r3, [r7, #68]	; 0x44
 8003cdc:	e006      	b.n	8003cec <xvprintf+0x1b4>
 8003cde:	2333      	movs	r3, #51	; 0x33
 8003ce0:	18fb      	adds	r3, r7, r3
 8003ce2:	781b      	ldrb	r3, [r3, #0]
 8003ce4:	0018      	movs	r0, r3
 8003ce6:	f7ff fee7 	bl	8003ab8 <xputc>
 8003cea:	e09e      	b.n	8003e2a <xvprintf+0x2f2>
 8003cec:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003cee:	2204      	movs	r2, #4
 8003cf0:	4013      	ands	r3, r2
 8003cf2:	d005      	beq.n	8003d00 <xvprintf+0x1c8>
 8003cf4:	683b      	ldr	r3, [r7, #0]
 8003cf6:	1d1a      	adds	r2, r3, #4
 8003cf8:	603a      	str	r2, [r7, #0]
 8003cfa:	681b      	ldr	r3, [r3, #0]
 8003cfc:	62fb      	str	r3, [r7, #44]	; 0x2c
 8003cfe:	e00e      	b.n	8003d1e <xvprintf+0x1e6>
 8003d00:	2332      	movs	r3, #50	; 0x32
 8003d02:	18fb      	adds	r3, r7, r3
 8003d04:	781b      	ldrb	r3, [r3, #0]
 8003d06:	2b44      	cmp	r3, #68	; 0x44
 8003d08:	d104      	bne.n	8003d14 <xvprintf+0x1dc>
 8003d0a:	683b      	ldr	r3, [r7, #0]
 8003d0c:	1d1a      	adds	r2, r3, #4
 8003d0e:	603a      	str	r2, [r7, #0]
 8003d10:	681b      	ldr	r3, [r3, #0]
 8003d12:	e003      	b.n	8003d1c <xvprintf+0x1e4>
 8003d14:	683b      	ldr	r3, [r7, #0]
 8003d16:	1d1a      	adds	r2, r3, #4
 8003d18:	603a      	str	r2, [r7, #0]
 8003d1a:	681b      	ldr	r3, [r3, #0]
 8003d1c:	62fb      	str	r3, [r7, #44]	; 0x2c
 8003d1e:	2332      	movs	r3, #50	; 0x32
 8003d20:	18fb      	adds	r3, r7, r3
 8003d22:	781b      	ldrb	r3, [r3, #0]
 8003d24:	2b44      	cmp	r3, #68	; 0x44
 8003d26:	d109      	bne.n	8003d3c <xvprintf+0x204>
 8003d28:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8003d2a:	2b00      	cmp	r3, #0
 8003d2c:	da06      	bge.n	8003d3c <xvprintf+0x204>
 8003d2e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8003d30:	425b      	negs	r3, r3
 8003d32:	62fb      	str	r3, [r7, #44]	; 0x2c
 8003d34:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003d36:	2210      	movs	r2, #16
 8003d38:	4313      	orrs	r3, r2
 8003d3a:	637b      	str	r3, [r7, #52]	; 0x34
 8003d3c:	2300      	movs	r3, #0
 8003d3e:	643b      	str	r3, [r7, #64]	; 0x40
 8003d40:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8003d42:	62bb      	str	r3, [r7, #40]	; 0x28
 8003d44:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8003d46:	6c79      	ldr	r1, [r7, #68]	; 0x44
 8003d48:	0018      	movs	r0, r3
 8003d4a:	f7fc fa63 	bl	8000214 <__aeabi_uidivmod>
 8003d4e:	000b      	movs	r3, r1
 8003d50:	001a      	movs	r2, r3
 8003d52:	2432      	movs	r4, #50	; 0x32
 8003d54:	193b      	adds	r3, r7, r4
 8003d56:	701a      	strb	r2, [r3, #0]
 8003d58:	6c79      	ldr	r1, [r7, #68]	; 0x44
 8003d5a:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 8003d5c:	f7fc f9d4 	bl	8000108 <__udivsi3>
 8003d60:	0003      	movs	r3, r0
 8003d62:	62bb      	str	r3, [r7, #40]	; 0x28
 8003d64:	193b      	adds	r3, r7, r4
 8003d66:	781b      	ldrb	r3, [r3, #0]
 8003d68:	2b09      	cmp	r3, #9
 8003d6a:	d90d      	bls.n	8003d88 <xvprintf+0x250>
 8003d6c:	2333      	movs	r3, #51	; 0x33
 8003d6e:	18fb      	adds	r3, r7, r3
 8003d70:	781b      	ldrb	r3, [r3, #0]
 8003d72:	2b78      	cmp	r3, #120	; 0x78
 8003d74:	d101      	bne.n	8003d7a <xvprintf+0x242>
 8003d76:	2327      	movs	r3, #39	; 0x27
 8003d78:	e000      	b.n	8003d7c <xvprintf+0x244>
 8003d7a:	2307      	movs	r3, #7
 8003d7c:	2132      	movs	r1, #50	; 0x32
 8003d7e:	187a      	adds	r2, r7, r1
 8003d80:	1879      	adds	r1, r7, r1
 8003d82:	7809      	ldrb	r1, [r1, #0]
 8003d84:	185b      	adds	r3, r3, r1
 8003d86:	7013      	strb	r3, [r2, #0]
 8003d88:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003d8a:	1c5a      	adds	r2, r3, #1
 8003d8c:	643a      	str	r2, [r7, #64]	; 0x40
 8003d8e:	2232      	movs	r2, #50	; 0x32
 8003d90:	18ba      	adds	r2, r7, r2
 8003d92:	7812      	ldrb	r2, [r2, #0]
 8003d94:	3230      	adds	r2, #48	; 0x30
 8003d96:	b2d1      	uxtb	r1, r2
 8003d98:	220c      	movs	r2, #12
 8003d9a:	18ba      	adds	r2, r7, r2
 8003d9c:	54d1      	strb	r1, [r2, r3]
 8003d9e:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8003da0:	2b00      	cmp	r3, #0
 8003da2:	d002      	beq.n	8003daa <xvprintf+0x272>
 8003da4:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003da6:	2b17      	cmp	r3, #23
 8003da8:	d9cc      	bls.n	8003d44 <xvprintf+0x20c>
 8003daa:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003dac:	2210      	movs	r2, #16
 8003dae:	4013      	ands	r3, r2
 8003db0:	d006      	beq.n	8003dc0 <xvprintf+0x288>
 8003db2:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003db4:	1c5a      	adds	r2, r3, #1
 8003db6:	643a      	str	r2, [r7, #64]	; 0x40
 8003db8:	220c      	movs	r2, #12
 8003dba:	18ba      	adds	r2, r7, r2
 8003dbc:	212d      	movs	r1, #45	; 0x2d
 8003dbe:	54d1      	strb	r1, [r2, r3]
 8003dc0:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003dc2:	63fb      	str	r3, [r7, #60]	; 0x3c
 8003dc4:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003dc6:	2201      	movs	r2, #1
 8003dc8:	4013      	ands	r3, r2
 8003dca:	d001      	beq.n	8003dd0 <xvprintf+0x298>
 8003dcc:	2230      	movs	r2, #48	; 0x30
 8003dce:	e000      	b.n	8003dd2 <xvprintf+0x29a>
 8003dd0:	2220      	movs	r2, #32
 8003dd2:	2332      	movs	r3, #50	; 0x32
 8003dd4:	18fb      	adds	r3, r7, r3
 8003dd6:	701a      	strb	r2, [r3, #0]
 8003dd8:	e005      	b.n	8003de6 <xvprintf+0x2ae>
 8003dda:	2332      	movs	r3, #50	; 0x32
 8003ddc:	18fb      	adds	r3, r7, r3
 8003dde:	781b      	ldrb	r3, [r3, #0]
 8003de0:	0018      	movs	r0, r3
 8003de2:	f7ff fe69 	bl	8003ab8 <xputc>
 8003de6:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8003de8:	2202      	movs	r2, #2
 8003dea:	4013      	ands	r3, r2
 8003dec:	d105      	bne.n	8003dfa <xvprintf+0x2c2>
 8003dee:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003df0:	1c5a      	adds	r2, r3, #1
 8003df2:	63fa      	str	r2, [r7, #60]	; 0x3c
 8003df4:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8003df6:	429a      	cmp	r2, r3
 8003df8:	d8ef      	bhi.n	8003dda <xvprintf+0x2a2>
 8003dfa:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003dfc:	3b01      	subs	r3, #1
 8003dfe:	643b      	str	r3, [r7, #64]	; 0x40
 8003e00:	230c      	movs	r3, #12
 8003e02:	18fa      	adds	r2, r7, r3
 8003e04:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003e06:	18d3      	adds	r3, r2, r3
 8003e08:	781b      	ldrb	r3, [r3, #0]
 8003e0a:	0018      	movs	r0, r3
 8003e0c:	f7ff fe54 	bl	8003ab8 <xputc>
 8003e10:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8003e12:	2b00      	cmp	r3, #0
 8003e14:	d1f1      	bne.n	8003dfa <xvprintf+0x2c2>
 8003e16:	e002      	b.n	8003e1e <xvprintf+0x2e6>
 8003e18:	2020      	movs	r0, #32
 8003e1a:	f7ff fe4d 	bl	8003ab8 <xputc>
 8003e1e:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8003e20:	1c5a      	adds	r2, r3, #1
 8003e22:	63fa      	str	r2, [r7, #60]	; 0x3c
 8003e24:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8003e26:	429a      	cmp	r2, r3
 8003e28:	d8f6      	bhi.n	8003e18 <xvprintf+0x2e0>
 8003e2a:	e68a      	b.n	8003b42 <xvprintf+0xa>
 8003e2c:	46c0      	nop			; (mov r8, r8)
 8003e2e:	e000      	b.n	8003e32 <xvprintf+0x2fa>
 8003e30:	46c0      	nop			; (mov r8, r8)
 8003e32:	46c0      	nop			; (mov r8, r8)
 8003e34:	46bd      	mov	sp, r7
 8003e36:	b013      	add	sp, #76	; 0x4c
 8003e38:	bd90      	pop	{r4, r7, pc}
 8003e3a:	46c0      	nop			; (mov r8, r8)
 8003e3c:	08009f88 	.word	0x08009f88

08003e40 <xprintf>:
 8003e40:	b40f      	push	{r0, r1, r2, r3}
 8003e42:	b580      	push	{r7, lr}
 8003e44:	b082      	sub	sp, #8
 8003e46:	af00      	add	r7, sp, #0
 8003e48:	2314      	movs	r3, #20
 8003e4a:	18fb      	adds	r3, r7, r3
 8003e4c:	607b      	str	r3, [r7, #4]
 8003e4e:	687a      	ldr	r2, [r7, #4]
 8003e50:	693b      	ldr	r3, [r7, #16]
 8003e52:	0011      	movs	r1, r2
 8003e54:	0018      	movs	r0, r3
 8003e56:	f7ff fe6f 	bl	8003b38 <xvprintf>
 8003e5a:	46c0      	nop			; (mov r8, r8)
 8003e5c:	46bd      	mov	sp, r7
 8003e5e:	b002      	add	sp, #8
 8003e60:	bc80      	pop	{r7}
 8003e62:	bc08      	pop	{r3}
 8003e64:	b004      	add	sp, #16
 8003e66:	4718      	bx	r3

08003e68 <VL53L0X_GetOffsetCalibrationDataMicroMeter>:
 8003e68:	b5b0      	push	{r4, r5, r7, lr}
 8003e6a:	b084      	sub	sp, #16
 8003e6c:	af00      	add	r7, sp, #0
 8003e6e:	6078      	str	r0, [r7, #4]
 8003e70:	6039      	str	r1, [r7, #0]
 8003e72:	250f      	movs	r5, #15
 8003e74:	197b      	adds	r3, r7, r5
 8003e76:	2200      	movs	r2, #0
 8003e78:	701a      	strb	r2, [r3, #0]
 8003e7a:	197c      	adds	r4, r7, r5
 8003e7c:	683a      	ldr	r2, [r7, #0]
 8003e7e:	687b      	ldr	r3, [r7, #4]
 8003e80:	0011      	movs	r1, r2
 8003e82:	0018      	movs	r0, r3
 8003e84:	f002 f832 	bl	8005eec <VL53L0X_get_offset_calibration_data_micro_meter>
 8003e88:	0003      	movs	r3, r0
 8003e8a:	7023      	strb	r3, [r4, #0]
 8003e8c:	197b      	adds	r3, r7, r5
 8003e8e:	781b      	ldrb	r3, [r3, #0]
 8003e90:	b25b      	sxtb	r3, r3
 8003e92:	0018      	movs	r0, r3
 8003e94:	46bd      	mov	sp, r7
 8003e96:	b004      	add	sp, #16
 8003e98:	bdb0      	pop	{r4, r5, r7, pc}

08003e9a <VL53L0X_SetDeviceAddress>:
 8003e9a:	b5b0      	push	{r4, r5, r7, lr}
 8003e9c:	b084      	sub	sp, #16
 8003e9e:	af00      	add	r7, sp, #0
 8003ea0:	6078      	str	r0, [r7, #4]
 8003ea2:	000a      	movs	r2, r1
 8003ea4:	1cfb      	adds	r3, r7, #3
 8003ea6:	701a      	strb	r2, [r3, #0]
 8003ea8:	210f      	movs	r1, #15
 8003eaa:	187b      	adds	r3, r7, r1
 8003eac:	2200      	movs	r2, #0
 8003eae:	701a      	strb	r2, [r3, #0]
 8003eb0:	1cfb      	adds	r3, r7, #3
 8003eb2:	781b      	ldrb	r3, [r3, #0]
 8003eb4:	085b      	lsrs	r3, r3, #1
 8003eb6:	b2da      	uxtb	r2, r3
 8003eb8:	000d      	movs	r5, r1
 8003eba:	187c      	adds	r4, r7, r1
 8003ebc:	687b      	ldr	r3, [r7, #4]
 8003ebe:	218a      	movs	r1, #138	; 0x8a
 8003ec0:	0018      	movs	r0, r3
 8003ec2:	f7ff fd17 	bl	80038f4 <VL53L0X_WrByte>
 8003ec6:	0003      	movs	r3, r0
 8003ec8:	7023      	strb	r3, [r4, #0]
 8003eca:	197b      	adds	r3, r7, r5
 8003ecc:	781b      	ldrb	r3, [r3, #0]
 8003ece:	b25b      	sxtb	r3, r3
 8003ed0:	0018      	movs	r0, r3
 8003ed2:	46bd      	mov	sp, r7
 8003ed4:	b004      	add	sp, #16
 8003ed6:	bdb0      	pop	{r4, r5, r7, pc}

08003ed8 <VL53L0X_DataInit>:
 8003ed8:	b5f0      	push	{r4, r5, r6, r7, lr}
 8003eda:	b097      	sub	sp, #92	; 0x5c
 8003edc:	af00      	add	r7, sp, #0
 8003ede:	6078      	str	r0, [r7, #4]
 8003ee0:	2557      	movs	r5, #87	; 0x57
 8003ee2:	197b      	adds	r3, r7, r5
 8003ee4:	2200      	movs	r2, #0
 8003ee6:	701a      	strb	r2, [r3, #0]
 8003ee8:	197c      	adds	r4, r7, r5
 8003eea:	6878      	ldr	r0, [r7, #4]
 8003eec:	2301      	movs	r3, #1
 8003eee:	22fe      	movs	r2, #254	; 0xfe
 8003ef0:	2189      	movs	r1, #137	; 0x89
 8003ef2:	f7ff fd3c 	bl	800396e <VL53L0X_UpdateByte>
 8003ef6:	0003      	movs	r3, r0
 8003ef8:	7023      	strb	r3, [r4, #0]
 8003efa:	197b      	adds	r3, r7, r5
 8003efc:	781b      	ldrb	r3, [r3, #0]
 8003efe:	b25b      	sxtb	r3, r3
 8003f00:	2b00      	cmp	r3, #0
 8003f02:	d108      	bne.n	8003f16 <VL53L0X_DataInit+0x3e>
 8003f04:	197c      	adds	r4, r7, r5
 8003f06:	687b      	ldr	r3, [r7, #4]
 8003f08:	2200      	movs	r2, #0
 8003f0a:	2188      	movs	r1, #136	; 0x88
 8003f0c:	0018      	movs	r0, r3
 8003f0e:	f7ff fcf1 	bl	80038f4 <VL53L0X_WrByte>
 8003f12:	0003      	movs	r3, r0
 8003f14:	7023      	strb	r3, [r4, #0]
 8003f16:	687b      	ldr	r3, [r7, #4]
 8003f18:	22f0      	movs	r2, #240	; 0xf0
 8003f1a:	2100      	movs	r1, #0
 8003f1c:	5499      	strb	r1, [r3, r2]
 8003f1e:	687a      	ldr	r2, [r7, #4]
 8003f20:	2399      	movs	r3, #153	; 0x99
 8003f22:	005b      	lsls	r3, r3, #1
 8003f24:	21fa      	movs	r1, #250	; 0xfa
 8003f26:	0089      	lsls	r1, r1, #2
 8003f28:	52d1      	strh	r1, [r2, r3]
 8003f2a:	687a      	ldr	r2, [r7, #4]
 8003f2c:	239a      	movs	r3, #154	; 0x9a
 8003f2e:	005b      	lsls	r3, r3, #1
 8003f30:	21c8      	movs	r1, #200	; 0xc8
 8003f32:	0049      	lsls	r1, r1, #1
 8003f34:	52d1      	strh	r1, [r2, r3]
 8003f36:	687a      	ldr	r2, [r7, #4]
 8003f38:	239c      	movs	r3, #156	; 0x9c
 8003f3a:	005b      	lsls	r3, r3, #1
 8003f3c:	49b5      	ldr	r1, [pc, #724]	; (8004214 <VL53L0X_DataInit+0x33c>)
 8003f3e:	50d1      	str	r1, [r2, r3]
 8003f40:	687b      	ldr	r3, [r7, #4]
 8003f42:	22d4      	movs	r2, #212	; 0xd4
 8003f44:	49b4      	ldr	r1, [pc, #720]	; (8004218 <VL53L0X_DataInit+0x340>)
 8003f46:	5099      	str	r1, [r3, r2]
 8003f48:	687b      	ldr	r3, [r7, #4]
 8003f4a:	2200      	movs	r2, #0
 8003f4c:	621a      	str	r2, [r3, #32]
 8003f4e:	2657      	movs	r6, #87	; 0x57
 8003f50:	19bc      	adds	r4, r7, r6
 8003f52:	2510      	movs	r5, #16
 8003f54:	197a      	adds	r2, r7, r5
 8003f56:	687b      	ldr	r3, [r7, #4]
 8003f58:	0011      	movs	r1, r2
 8003f5a:	0018      	movs	r0, r3
 8003f5c:	f000 fb2a 	bl	80045b4 <VL53L0X_GetDeviceParameters>
 8003f60:	0003      	movs	r3, r0
 8003f62:	7023      	strb	r3, [r4, #0]
 8003f64:	19bb      	adds	r3, r7, r6
 8003f66:	781b      	ldrb	r3, [r3, #0]
 8003f68:	b25b      	sxtb	r3, r3
 8003f6a:	2b00      	cmp	r3, #0
 8003f6c:	d10d      	bne.n	8003f8a <VL53L0X_DataInit+0xb2>
 8003f6e:	197b      	adds	r3, r7, r5
 8003f70:	2200      	movs	r2, #0
 8003f72:	701a      	strb	r2, [r3, #0]
 8003f74:	197b      	adds	r3, r7, r5
 8003f76:	2200      	movs	r2, #0
 8003f78:	705a      	strb	r2, [r3, #1]
 8003f7a:	687b      	ldr	r3, [r7, #4]
 8003f7c:	197a      	adds	r2, r7, r5
 8003f7e:	3310      	adds	r3, #16
 8003f80:	0011      	movs	r1, r2
 8003f82:	2240      	movs	r2, #64	; 0x40
 8003f84:	0018      	movs	r0, r3
 8003f86:	f005 fad3 	bl	8009530 <memcpy>
 8003f8a:	687a      	ldr	r2, [r7, #4]
 8003f8c:	238a      	movs	r3, #138	; 0x8a
 8003f8e:	005b      	lsls	r3, r3, #1
 8003f90:	2164      	movs	r1, #100	; 0x64
 8003f92:	52d1      	strh	r1, [r2, r3]
 8003f94:	687a      	ldr	r2, [r7, #4]
 8003f96:	238b      	movs	r3, #139	; 0x8b
 8003f98:	005b      	lsls	r3, r3, #1
 8003f9a:	21e1      	movs	r1, #225	; 0xe1
 8003f9c:	0089      	lsls	r1, r1, #2
 8003f9e:	52d1      	strh	r1, [r2, r3]
 8003fa0:	687a      	ldr	r2, [r7, #4]
 8003fa2:	238c      	movs	r3, #140	; 0x8c
 8003fa4:	005b      	lsls	r3, r3, #1
 8003fa6:	21fa      	movs	r1, #250	; 0xfa
 8003fa8:	0049      	lsls	r1, r1, #1
 8003faa:	52d1      	strh	r1, [r2, r3]
 8003fac:	687a      	ldr	r2, [r7, #4]
 8003fae:	238e      	movs	r3, #142	; 0x8e
 8003fb0:	005b      	lsls	r3, r3, #1
 8003fb2:	21a0      	movs	r1, #160	; 0xa0
 8003fb4:	0109      	lsls	r1, r1, #4
 8003fb6:	52d1      	strh	r1, [r2, r3]
 8003fb8:	687a      	ldr	r2, [r7, #4]
 8003fba:	2398      	movs	r3, #152	; 0x98
 8003fbc:	005b      	lsls	r3, r3, #1
 8003fbe:	2101      	movs	r1, #1
 8003fc0:	54d1      	strb	r1, [r2, r3]
 8003fc2:	687b      	ldr	r3, [r7, #4]
 8003fc4:	2201      	movs	r2, #1
 8003fc6:	2180      	movs	r1, #128	; 0x80
 8003fc8:	0018      	movs	r0, r3
 8003fca:	f7ff fc93 	bl	80038f4 <VL53L0X_WrByte>
 8003fce:	0003      	movs	r3, r0
 8003fd0:	0019      	movs	r1, r3
 8003fd2:	2457      	movs	r4, #87	; 0x57
 8003fd4:	193b      	adds	r3, r7, r4
 8003fd6:	193a      	adds	r2, r7, r4
 8003fd8:	7812      	ldrb	r2, [r2, #0]
 8003fda:	430a      	orrs	r2, r1
 8003fdc:	701a      	strb	r2, [r3, #0]
 8003fde:	687b      	ldr	r3, [r7, #4]
 8003fe0:	2201      	movs	r2, #1
 8003fe2:	21ff      	movs	r1, #255	; 0xff
 8003fe4:	0018      	movs	r0, r3
 8003fe6:	f7ff fc85 	bl	80038f4 <VL53L0X_WrByte>
 8003fea:	0003      	movs	r3, r0
 8003fec:	0019      	movs	r1, r3
 8003fee:	193b      	adds	r3, r7, r4
 8003ff0:	193a      	adds	r2, r7, r4
 8003ff2:	7812      	ldrb	r2, [r2, #0]
 8003ff4:	430a      	orrs	r2, r1
 8003ff6:	701a      	strb	r2, [r3, #0]
 8003ff8:	687b      	ldr	r3, [r7, #4]
 8003ffa:	2200      	movs	r2, #0
 8003ffc:	2100      	movs	r1, #0
 8003ffe:	0018      	movs	r0, r3
 8004000:	f7ff fc78 	bl	80038f4 <VL53L0X_WrByte>
 8004004:	0003      	movs	r3, r0
 8004006:	0019      	movs	r1, r3
 8004008:	193b      	adds	r3, r7, r4
 800400a:	193a      	adds	r2, r7, r4
 800400c:	7812      	ldrb	r2, [r2, #0]
 800400e:	430a      	orrs	r2, r1
 8004010:	701a      	strb	r2, [r3, #0]
 8004012:	250f      	movs	r5, #15
 8004014:	197a      	adds	r2, r7, r5
 8004016:	687b      	ldr	r3, [r7, #4]
 8004018:	2191      	movs	r1, #145	; 0x91
 800401a:	0018      	movs	r0, r3
 800401c:	f7ff fcd8 	bl	80039d0 <VL53L0X_RdByte>
 8004020:	0003      	movs	r3, r0
 8004022:	0019      	movs	r1, r3
 8004024:	193b      	adds	r3, r7, r4
 8004026:	193a      	adds	r2, r7, r4
 8004028:	7812      	ldrb	r2, [r2, #0]
 800402a:	430a      	orrs	r2, r1
 800402c:	701a      	strb	r2, [r3, #0]
 800402e:	197b      	adds	r3, r7, r5
 8004030:	7819      	ldrb	r1, [r3, #0]
 8004032:	687a      	ldr	r2, [r7, #4]
 8004034:	238d      	movs	r3, #141	; 0x8d
 8004036:	005b      	lsls	r3, r3, #1
 8004038:	54d1      	strb	r1, [r2, r3]
 800403a:	687b      	ldr	r3, [r7, #4]
 800403c:	2201      	movs	r2, #1
 800403e:	2100      	movs	r1, #0
 8004040:	0018      	movs	r0, r3
 8004042:	f7ff fc57 	bl	80038f4 <VL53L0X_WrByte>
 8004046:	0003      	movs	r3, r0
 8004048:	0019      	movs	r1, r3
 800404a:	193b      	adds	r3, r7, r4
 800404c:	193a      	adds	r2, r7, r4
 800404e:	7812      	ldrb	r2, [r2, #0]
 8004050:	430a      	orrs	r2, r1
 8004052:	701a      	strb	r2, [r3, #0]
 8004054:	687b      	ldr	r3, [r7, #4]
 8004056:	2200      	movs	r2, #0
 8004058:	21ff      	movs	r1, #255	; 0xff
 800405a:	0018      	movs	r0, r3
 800405c:	f7ff fc4a 	bl	80038f4 <VL53L0X_WrByte>
 8004060:	0003      	movs	r3, r0
 8004062:	0019      	movs	r1, r3
 8004064:	193b      	adds	r3, r7, r4
 8004066:	193a      	adds	r2, r7, r4
 8004068:	7812      	ldrb	r2, [r2, #0]
 800406a:	430a      	orrs	r2, r1
 800406c:	701a      	strb	r2, [r3, #0]
 800406e:	687b      	ldr	r3, [r7, #4]
 8004070:	2200      	movs	r2, #0
 8004072:	2180      	movs	r1, #128	; 0x80
 8004074:	0018      	movs	r0, r3
 8004076:	f7ff fc3d 	bl	80038f4 <VL53L0X_WrByte>
 800407a:	0003      	movs	r3, r0
 800407c:	0019      	movs	r1, r3
 800407e:	193b      	adds	r3, r7, r4
 8004080:	193a      	adds	r2, r7, r4
 8004082:	7812      	ldrb	r2, [r2, #0]
 8004084:	430a      	orrs	r2, r1
 8004086:	701a      	strb	r2, [r3, #0]
 8004088:	2300      	movs	r3, #0
 800408a:	653b      	str	r3, [r7, #80]	; 0x50
 800408c:	e016      	b.n	80040bc <VL53L0X_DataInit+0x1e4>
 800408e:	2457      	movs	r4, #87	; 0x57
 8004090:	193b      	adds	r3, r7, r4
 8004092:	781b      	ldrb	r3, [r3, #0]
 8004094:	b25b      	sxtb	r3, r3
 8004096:	2b00      	cmp	r3, #0
 8004098:	d114      	bne.n	80040c4 <VL53L0X_DataInit+0x1ec>
 800409a:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 800409c:	b299      	uxth	r1, r3
 800409e:	687b      	ldr	r3, [r7, #4]
 80040a0:	2201      	movs	r2, #1
 80040a2:	0018      	movs	r0, r3
 80040a4:	f000 fe42 	bl	8004d2c <VL53L0X_SetLimitCheckEnable>
 80040a8:	0003      	movs	r3, r0
 80040aa:	0019      	movs	r1, r3
 80040ac:	193b      	adds	r3, r7, r4
 80040ae:	193a      	adds	r2, r7, r4
 80040b0:	7812      	ldrb	r2, [r2, #0]
 80040b2:	430a      	orrs	r2, r1
 80040b4:	701a      	strb	r2, [r3, #0]
 80040b6:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80040b8:	3301      	adds	r3, #1
 80040ba:	653b      	str	r3, [r7, #80]	; 0x50
 80040bc:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80040be:	2b05      	cmp	r3, #5
 80040c0:	dde5      	ble.n	800408e <VL53L0X_DataInit+0x1b6>
 80040c2:	e000      	b.n	80040c6 <VL53L0X_DataInit+0x1ee>
 80040c4:	46c0      	nop			; (mov r8, r8)
 80040c6:	2257      	movs	r2, #87	; 0x57
 80040c8:	18bb      	adds	r3, r7, r2
 80040ca:	781b      	ldrb	r3, [r3, #0]
 80040cc:	b25b      	sxtb	r3, r3
 80040ce:	2b00      	cmp	r3, #0
 80040d0:	d108      	bne.n	80040e4 <VL53L0X_DataInit+0x20c>
 80040d2:	18bc      	adds	r4, r7, r2
 80040d4:	687b      	ldr	r3, [r7, #4]
 80040d6:	2200      	movs	r2, #0
 80040d8:	2102      	movs	r1, #2
 80040da:	0018      	movs	r0, r3
 80040dc:	f000 fe26 	bl	8004d2c <VL53L0X_SetLimitCheckEnable>
 80040e0:	0003      	movs	r3, r0
 80040e2:	7023      	strb	r3, [r4, #0]
 80040e4:	2257      	movs	r2, #87	; 0x57
 80040e6:	18bb      	adds	r3, r7, r2
 80040e8:	781b      	ldrb	r3, [r3, #0]
 80040ea:	b25b      	sxtb	r3, r3
 80040ec:	2b00      	cmp	r3, #0
 80040ee:	d108      	bne.n	8004102 <VL53L0X_DataInit+0x22a>
 80040f0:	18bc      	adds	r4, r7, r2
 80040f2:	687b      	ldr	r3, [r7, #4]
 80040f4:	2200      	movs	r2, #0
 80040f6:	2103      	movs	r1, #3
 80040f8:	0018      	movs	r0, r3
 80040fa:	f000 fe17 	bl	8004d2c <VL53L0X_SetLimitCheckEnable>
 80040fe:	0003      	movs	r3, r0
 8004100:	7023      	strb	r3, [r4, #0]
 8004102:	2257      	movs	r2, #87	; 0x57
 8004104:	18bb      	adds	r3, r7, r2
 8004106:	781b      	ldrb	r3, [r3, #0]
 8004108:	b25b      	sxtb	r3, r3
 800410a:	2b00      	cmp	r3, #0
 800410c:	d108      	bne.n	8004120 <VL53L0X_DataInit+0x248>
 800410e:	18bc      	adds	r4, r7, r2
 8004110:	687b      	ldr	r3, [r7, #4]
 8004112:	2200      	movs	r2, #0
 8004114:	2104      	movs	r1, #4
 8004116:	0018      	movs	r0, r3
 8004118:	f000 fe08 	bl	8004d2c <VL53L0X_SetLimitCheckEnable>
 800411c:	0003      	movs	r3, r0
 800411e:	7023      	strb	r3, [r4, #0]
 8004120:	2257      	movs	r2, #87	; 0x57
 8004122:	18bb      	adds	r3, r7, r2
 8004124:	781b      	ldrb	r3, [r3, #0]
 8004126:	b25b      	sxtb	r3, r3
 8004128:	2b00      	cmp	r3, #0
 800412a:	d108      	bne.n	800413e <VL53L0X_DataInit+0x266>
 800412c:	18bc      	adds	r4, r7, r2
 800412e:	687b      	ldr	r3, [r7, #4]
 8004130:	2200      	movs	r2, #0
 8004132:	2105      	movs	r1, #5
 8004134:	0018      	movs	r0, r3
 8004136:	f000 fdf9 	bl	8004d2c <VL53L0X_SetLimitCheckEnable>
 800413a:	0003      	movs	r3, r0
 800413c:	7023      	strb	r3, [r4, #0]
 800413e:	2257      	movs	r2, #87	; 0x57
 8004140:	18bb      	adds	r3, r7, r2
 8004142:	781b      	ldrb	r3, [r3, #0]
 8004144:	b25b      	sxtb	r3, r3
 8004146:	2b00      	cmp	r3, #0
 8004148:	d109      	bne.n	800415e <VL53L0X_DataInit+0x286>
 800414a:	18bc      	adds	r4, r7, r2
 800414c:	2390      	movs	r3, #144	; 0x90
 800414e:	035a      	lsls	r2, r3, #13
 8004150:	687b      	ldr	r3, [r7, #4]
 8004152:	2100      	movs	r1, #0
 8004154:	0018      	movs	r0, r3
 8004156:	f000 fed5 	bl	8004f04 <VL53L0X_SetLimitCheckValue>
 800415a:	0003      	movs	r3, r0
 800415c:	7023      	strb	r3, [r4, #0]
 800415e:	2257      	movs	r2, #87	; 0x57
 8004160:	18bb      	adds	r3, r7, r2
 8004162:	781b      	ldrb	r3, [r3, #0]
 8004164:	b25b      	sxtb	r3, r3
 8004166:	2b00      	cmp	r3, #0
 8004168:	d109      	bne.n	800417e <VL53L0X_DataInit+0x2a6>
 800416a:	18bc      	adds	r4, r7, r2
 800416c:	2380      	movs	r3, #128	; 0x80
 800416e:	01da      	lsls	r2, r3, #7
 8004170:	687b      	ldr	r3, [r7, #4]
 8004172:	2101      	movs	r1, #1
 8004174:	0018      	movs	r0, r3
 8004176:	f000 fec5 	bl	8004f04 <VL53L0X_SetLimitCheckValue>
 800417a:	0003      	movs	r3, r0
 800417c:	7023      	strb	r3, [r4, #0]
 800417e:	2257      	movs	r2, #87	; 0x57
 8004180:	18bb      	adds	r3, r7, r2
 8004182:	781b      	ldrb	r3, [r3, #0]
 8004184:	b25b      	sxtb	r3, r3
 8004186:	2b00      	cmp	r3, #0
 8004188:	d109      	bne.n	800419e <VL53L0X_DataInit+0x2c6>
 800418a:	18bc      	adds	r4, r7, r2
 800418c:	238c      	movs	r3, #140	; 0x8c
 800418e:	039a      	lsls	r2, r3, #14
 8004190:	687b      	ldr	r3, [r7, #4]
 8004192:	2102      	movs	r1, #2
 8004194:	0018      	movs	r0, r3
 8004196:	f000 feb5 	bl	8004f04 <VL53L0X_SetLimitCheckValue>
 800419a:	0003      	movs	r3, r0
 800419c:	7023      	strb	r3, [r4, #0]
 800419e:	2257      	movs	r2, #87	; 0x57
 80041a0:	18bb      	adds	r3, r7, r2
 80041a2:	781b      	ldrb	r3, [r3, #0]
 80041a4:	b25b      	sxtb	r3, r3
 80041a6:	2b00      	cmp	r3, #0
 80041a8:	d108      	bne.n	80041bc <VL53L0X_DataInit+0x2e4>
 80041aa:	18bc      	adds	r4, r7, r2
 80041ac:	687b      	ldr	r3, [r7, #4]
 80041ae:	2200      	movs	r2, #0
 80041b0:	2103      	movs	r1, #3
 80041b2:	0018      	movs	r0, r3
 80041b4:	f000 fea6 	bl	8004f04 <VL53L0X_SetLimitCheckValue>
 80041b8:	0003      	movs	r3, r0
 80041ba:	7023      	strb	r3, [r4, #0]
 80041bc:	2057      	movs	r0, #87	; 0x57
 80041be:	183b      	adds	r3, r7, r0
 80041c0:	781b      	ldrb	r3, [r3, #0]
 80041c2:	b25b      	sxtb	r3, r3
 80041c4:	2b00      	cmp	r3, #0
 80041c6:	d112      	bne.n	80041ee <VL53L0X_DataInit+0x316>
 80041c8:	687a      	ldr	r2, [r7, #4]
 80041ca:	2388      	movs	r3, #136	; 0x88
 80041cc:	005b      	lsls	r3, r3, #1
 80041ce:	21ff      	movs	r1, #255	; 0xff
 80041d0:	54d1      	strb	r1, [r2, r3]
 80041d2:	183c      	adds	r4, r7, r0
 80041d4:	687b      	ldr	r3, [r7, #4]
 80041d6:	22ff      	movs	r2, #255	; 0xff
 80041d8:	2101      	movs	r1, #1
 80041da:	0018      	movs	r0, r3
 80041dc:	f7ff fb8a 	bl	80038f4 <VL53L0X_WrByte>
 80041e0:	0003      	movs	r3, r0
 80041e2:	7023      	strb	r3, [r4, #0]
 80041e4:	687a      	ldr	r2, [r7, #4]
 80041e6:	2389      	movs	r3, #137	; 0x89
 80041e8:	005b      	lsls	r3, r3, #1
 80041ea:	2101      	movs	r1, #1
 80041ec:	54d1      	strb	r1, [r2, r3]
 80041ee:	2357      	movs	r3, #87	; 0x57
 80041f0:	18fb      	adds	r3, r7, r3
 80041f2:	781b      	ldrb	r3, [r3, #0]
 80041f4:	b25b      	sxtb	r3, r3
 80041f6:	2b00      	cmp	r3, #0
 80041f8:	d103      	bne.n	8004202 <VL53L0X_DataInit+0x32a>
 80041fa:	687b      	ldr	r3, [r7, #4]
 80041fc:	22f5      	movs	r2, #245	; 0xf5
 80041fe:	2100      	movs	r1, #0
 8004200:	5499      	strb	r1, [r3, r2]
 8004202:	2357      	movs	r3, #87	; 0x57
 8004204:	18fb      	adds	r3, r7, r3
 8004206:	781b      	ldrb	r3, [r3, #0]
 8004208:	b25b      	sxtb	r3, r3
 800420a:	0018      	movs	r0, r3
 800420c:	46bd      	mov	sp, r7
 800420e:	b017      	add	sp, #92	; 0x5c
 8004210:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8004212:	46c0      	nop			; (mov r8, r8)
 8004214:	00016b85 	.word	0x00016b85
 8004218:	000970a4 	.word	0x000970a4

0800421c <VL53L0X_StaticInit>:
 800421c:	b5f0      	push	{r4, r5, r6, r7, lr}
 800421e:	b09f      	sub	sp, #124	; 0x7c
 8004220:	af02      	add	r7, sp, #8
 8004222:	6078      	str	r0, [r7, #4]
 8004224:	246f      	movs	r4, #111	; 0x6f
 8004226:	193b      	adds	r3, r7, r4
 8004228:	2200      	movs	r2, #0
 800422a:	701a      	strb	r2, [r3, #0]
 800422c:	231c      	movs	r3, #28
 800422e:	18fb      	adds	r3, r7, r3
 8004230:	0018      	movs	r0, r3
 8004232:	2340      	movs	r3, #64	; 0x40
 8004234:	001a      	movs	r2, r3
 8004236:	2100      	movs	r1, #0
 8004238:	f005 f9cc 	bl	80095d4 <memset>
 800423c:	231a      	movs	r3, #26
 800423e:	18fb      	adds	r3, r7, r3
 8004240:	2200      	movs	r2, #0
 8004242:	801a      	strh	r2, [r3, #0]
 8004244:	2319      	movs	r3, #25
 8004246:	18fb      	adds	r3, r7, r3
 8004248:	2200      	movs	r2, #0
 800424a:	701a      	strb	r2, [r3, #0]
 800424c:	2367      	movs	r3, #103	; 0x67
 800424e:	18fb      	adds	r3, r7, r3
 8004250:	2200      	movs	r2, #0
 8004252:	701a      	strb	r2, [r3, #0]
 8004254:	2300      	movs	r3, #0
 8004256:	663b      	str	r3, [r7, #96]	; 0x60
 8004258:	2318      	movs	r3, #24
 800425a:	18fb      	adds	r3, r7, r3
 800425c:	2200      	movs	r2, #0
 800425e:	701a      	strb	r2, [r3, #0]
 8004260:	2300      	movs	r3, #0
 8004262:	617b      	str	r3, [r7, #20]
 8004264:	255f      	movs	r5, #95	; 0x5f
 8004266:	197b      	adds	r3, r7, r5
 8004268:	2200      	movs	r2, #0
 800426a:	701a      	strb	r2, [r3, #0]
 800426c:	193c      	adds	r4, r7, r4
 800426e:	687b      	ldr	r3, [r7, #4]
 8004270:	2101      	movs	r1, #1
 8004272:	0018      	movs	r0, r3
 8004274:	f002 ff18 	bl	80070a8 <VL53L0X_get_info_from_device>
 8004278:	0003      	movs	r3, r0
 800427a:	7023      	strb	r3, [r4, #0]
 800427c:	687b      	ldr	r3, [r7, #4]
 800427e:	22f3      	movs	r2, #243	; 0xf3
 8004280:	5c9b      	ldrb	r3, [r3, r2]
 8004282:	663b      	str	r3, [r7, #96]	; 0x60
 8004284:	197b      	adds	r3, r7, r5
 8004286:	687a      	ldr	r2, [r7, #4]
 8004288:	21f4      	movs	r1, #244	; 0xf4
 800428a:	5c52      	ldrb	r2, [r2, r1]
 800428c:	701a      	strb	r2, [r3, #0]
 800428e:	197b      	adds	r3, r7, r5
 8004290:	781b      	ldrb	r3, [r3, #0]
 8004292:	2b01      	cmp	r3, #1
 8004294:	d80e      	bhi.n	80042b4 <VL53L0X_StaticInit+0x98>
 8004296:	197b      	adds	r3, r7, r5
 8004298:	781b      	ldrb	r3, [r3, #0]
 800429a:	2b01      	cmp	r3, #1
 800429c:	d102      	bne.n	80042a4 <VL53L0X_StaticInit+0x88>
 800429e:	6e3b      	ldr	r3, [r7, #96]	; 0x60
 80042a0:	2b20      	cmp	r3, #32
 80042a2:	d807      	bhi.n	80042b4 <VL53L0X_StaticInit+0x98>
 80042a4:	235f      	movs	r3, #95	; 0x5f
 80042a6:	18fb      	adds	r3, r7, r3
 80042a8:	781b      	ldrb	r3, [r3, #0]
 80042aa:	2b00      	cmp	r3, #0
 80042ac:	d10f      	bne.n	80042ce <VL53L0X_StaticInit+0xb2>
 80042ae:	6e3b      	ldr	r3, [r7, #96]	; 0x60
 80042b0:	2b0c      	cmp	r3, #12
 80042b2:	d90c      	bls.n	80042ce <VL53L0X_StaticInit+0xb2>
 80042b4:	236f      	movs	r3, #111	; 0x6f
 80042b6:	18fc      	adds	r4, r7, r3
 80042b8:	2318      	movs	r3, #24
 80042ba:	18fa      	adds	r2, r7, r3
 80042bc:	2314      	movs	r3, #20
 80042be:	18f9      	adds	r1, r7, r3
 80042c0:	687b      	ldr	r3, [r7, #4]
 80042c2:	0018      	movs	r0, r3
 80042c4:	f002 f861 	bl	800638a <VL53L0X_perform_ref_spad_management>
 80042c8:	0003      	movs	r3, r0
 80042ca:	7023      	strb	r3, [r4, #0]
 80042cc:	e00b      	b.n	80042e6 <VL53L0X_StaticInit+0xca>
 80042ce:	236f      	movs	r3, #111	; 0x6f
 80042d0:	18fc      	adds	r4, r7, r3
 80042d2:	235f      	movs	r3, #95	; 0x5f
 80042d4:	18fb      	adds	r3, r7, r3
 80042d6:	781a      	ldrb	r2, [r3, #0]
 80042d8:	6e39      	ldr	r1, [r7, #96]	; 0x60
 80042da:	687b      	ldr	r3, [r7, #4]
 80042dc:	0018      	movs	r0, r3
 80042de:	f002 fac0 	bl	8006862 <VL53L0X_set_reference_spads>
 80042e2:	0003      	movs	r3, r0
 80042e4:	7023      	strb	r3, [r4, #0]
 80042e6:	4bb2      	ldr	r3, [pc, #712]	; (80045b0 <VL53L0X_StaticInit+0x394>)
 80042e8:	66bb      	str	r3, [r7, #104]	; 0x68
 80042ea:	236f      	movs	r3, #111	; 0x6f
 80042ec:	18fb      	adds	r3, r7, r3
 80042ee:	781b      	ldrb	r3, [r3, #0]
 80042f0:	b25b      	sxtb	r3, r3
 80042f2:	2b00      	cmp	r3, #0
 80042f4:	d112      	bne.n	800431c <VL53L0X_StaticInit+0x100>
 80042f6:	2067      	movs	r0, #103	; 0x67
 80042f8:	183b      	adds	r3, r7, r0
 80042fa:	6879      	ldr	r1, [r7, #4]
 80042fc:	2298      	movs	r2, #152	; 0x98
 80042fe:	0052      	lsls	r2, r2, #1
 8004300:	5c8a      	ldrb	r2, [r1, r2]
 8004302:	701a      	strb	r2, [r3, #0]
 8004304:	183b      	adds	r3, r7, r0
 8004306:	781b      	ldrb	r3, [r3, #0]
 8004308:	2b00      	cmp	r3, #0
 800430a:	d105      	bne.n	8004318 <VL53L0X_StaticInit+0xfc>
 800430c:	687a      	ldr	r2, [r7, #4]
 800430e:	2396      	movs	r3, #150	; 0x96
 8004310:	005b      	lsls	r3, r3, #1
 8004312:	58d3      	ldr	r3, [r2, r3]
 8004314:	66bb      	str	r3, [r7, #104]	; 0x68
 8004316:	e001      	b.n	800431c <VL53L0X_StaticInit+0x100>
 8004318:	4ba5      	ldr	r3, [pc, #660]	; (80045b0 <VL53L0X_StaticInit+0x394>)
 800431a:	66bb      	str	r3, [r7, #104]	; 0x68
 800431c:	226f      	movs	r2, #111	; 0x6f
 800431e:	18bb      	adds	r3, r7, r2
 8004320:	781b      	ldrb	r3, [r3, #0]
 8004322:	b25b      	sxtb	r3, r3
 8004324:	2b00      	cmp	r3, #0
 8004326:	d108      	bne.n	800433a <VL53L0X_StaticInit+0x11e>
 8004328:	18bc      	adds	r4, r7, r2
 800432a:	6eba      	ldr	r2, [r7, #104]	; 0x68
 800432c:	687b      	ldr	r3, [r7, #4]
 800432e:	0011      	movs	r1, r2
 8004330:	0018      	movs	r0, r3
 8004332:	f004 f92b 	bl	800858c <VL53L0X_load_tuning_settings>
 8004336:	0003      	movs	r3, r0
 8004338:	7023      	strb	r3, [r4, #0]
 800433a:	226f      	movs	r2, #111	; 0x6f
 800433c:	18bb      	adds	r3, r7, r2
 800433e:	781b      	ldrb	r3, [r3, #0]
 8004340:	b25b      	sxtb	r3, r3
 8004342:	2b00      	cmp	r3, #0
 8004344:	d10a      	bne.n	800435c <VL53L0X_StaticInit+0x140>
 8004346:	18bc      	adds	r4, r7, r2
 8004348:	6878      	ldr	r0, [r7, #4]
 800434a:	2300      	movs	r3, #0
 800434c:	9300      	str	r3, [sp, #0]
 800434e:	2304      	movs	r3, #4
 8004350:	2200      	movs	r2, #0
 8004352:	2100      	movs	r1, #0
 8004354:	f001 fb60 	bl	8005a18 <VL53L0X_SetGpioConfig>
 8004358:	0003      	movs	r3, r0
 800435a:	7023      	strb	r3, [r4, #0]
 800435c:	256f      	movs	r5, #111	; 0x6f
 800435e:	197b      	adds	r3, r7, r5
 8004360:	781b      	ldrb	r3, [r3, #0]
 8004362:	b25b      	sxtb	r3, r3
 8004364:	2b00      	cmp	r3, #0
 8004366:	d123      	bne.n	80043b0 <VL53L0X_StaticInit+0x194>
 8004368:	197c      	adds	r4, r7, r5
 800436a:	687b      	ldr	r3, [r7, #4]
 800436c:	2201      	movs	r2, #1
 800436e:	21ff      	movs	r1, #255	; 0xff
 8004370:	0018      	movs	r0, r3
 8004372:	f7ff fabf 	bl	80038f4 <VL53L0X_WrByte>
 8004376:	0003      	movs	r3, r0
 8004378:	7023      	strb	r3, [r4, #0]
 800437a:	231a      	movs	r3, #26
 800437c:	18fa      	adds	r2, r7, r3
 800437e:	687b      	ldr	r3, [r7, #4]
 8004380:	2184      	movs	r1, #132	; 0x84
 8004382:	0018      	movs	r0, r3
 8004384:	f7ff fb39 	bl	80039fa <VL53L0X_RdWord>
 8004388:	0003      	movs	r3, r0
 800438a:	0019      	movs	r1, r3
 800438c:	197b      	adds	r3, r7, r5
 800438e:	197a      	adds	r2, r7, r5
 8004390:	7812      	ldrb	r2, [r2, #0]
 8004392:	430a      	orrs	r2, r1
 8004394:	701a      	strb	r2, [r3, #0]
 8004396:	687b      	ldr	r3, [r7, #4]
 8004398:	2200      	movs	r2, #0
 800439a:	21ff      	movs	r1, #255	; 0xff
 800439c:	0018      	movs	r0, r3
 800439e:	f7ff faa9 	bl	80038f4 <VL53L0X_WrByte>
 80043a2:	0003      	movs	r3, r0
 80043a4:	0019      	movs	r1, r3
 80043a6:	197b      	adds	r3, r7, r5
 80043a8:	197a      	adds	r2, r7, r5
 80043aa:	7812      	ldrb	r2, [r2, #0]
 80043ac:	430a      	orrs	r2, r1
 80043ae:	701a      	strb	r2, [r3, #0]
 80043b0:	236f      	movs	r3, #111	; 0x6f
 80043b2:	18fb      	adds	r3, r7, r3
 80043b4:	781b      	ldrb	r3, [r3, #0]
 80043b6:	b25b      	sxtb	r3, r3
 80043b8:	2b00      	cmp	r3, #0
 80043ba:	d107      	bne.n	80043cc <VL53L0X_StaticInit+0x1b0>
 80043bc:	231a      	movs	r3, #26
 80043be:	18fb      	adds	r3, r7, r3
 80043c0:	881b      	ldrh	r3, [r3, #0]
 80043c2:	011b      	lsls	r3, r3, #4
 80043c4:	0019      	movs	r1, r3
 80043c6:	687b      	ldr	r3, [r7, #4]
 80043c8:	22d4      	movs	r2, #212	; 0xd4
 80043ca:	5099      	str	r1, [r3, r2]
 80043cc:	226f      	movs	r2, #111	; 0x6f
 80043ce:	18bb      	adds	r3, r7, r2
 80043d0:	781b      	ldrb	r3, [r3, #0]
 80043d2:	b25b      	sxtb	r3, r3
 80043d4:	2b00      	cmp	r3, #0
 80043d6:	d109      	bne.n	80043ec <VL53L0X_StaticInit+0x1d0>
 80043d8:	18bc      	adds	r4, r7, r2
 80043da:	231c      	movs	r3, #28
 80043dc:	18fa      	adds	r2, r7, r3
 80043de:	687b      	ldr	r3, [r7, #4]
 80043e0:	0011      	movs	r1, r2
 80043e2:	0018      	movs	r0, r3
 80043e4:	f000 f8e6 	bl	80045b4 <VL53L0X_GetDeviceParameters>
 80043e8:	0003      	movs	r3, r0
 80043ea:	7023      	strb	r3, [r4, #0]
 80043ec:	256f      	movs	r5, #111	; 0x6f
 80043ee:	197b      	adds	r3, r7, r5
 80043f0:	781b      	ldrb	r3, [r3, #0]
 80043f2:	b25b      	sxtb	r3, r3
 80043f4:	2b00      	cmp	r3, #0
 80043f6:	d114      	bne.n	8004422 <VL53L0X_StaticInit+0x206>
 80043f8:	197c      	adds	r4, r7, r5
 80043fa:	2619      	movs	r6, #25
 80043fc:	19ba      	adds	r2, r7, r6
 80043fe:	687b      	ldr	r3, [r7, #4]
 8004400:	0011      	movs	r1, r2
 8004402:	0018      	movs	r0, r3
 8004404:	f000 f9cc 	bl	80047a0 <VL53L0X_GetFractionEnable>
 8004408:	0003      	movs	r3, r0
 800440a:	7023      	strb	r3, [r4, #0]
 800440c:	197b      	adds	r3, r7, r5
 800440e:	781b      	ldrb	r3, [r3, #0]
 8004410:	b25b      	sxtb	r3, r3
 8004412:	2b00      	cmp	r3, #0
 8004414:	d105      	bne.n	8004422 <VL53L0X_StaticInit+0x206>
 8004416:	19bb      	adds	r3, r7, r6
 8004418:	7819      	ldrb	r1, [r3, #0]
 800441a:	687a      	ldr	r2, [r7, #4]
 800441c:	2312      	movs	r3, #18
 800441e:	33ff      	adds	r3, #255	; 0xff
 8004420:	54d1      	strb	r1, [r2, r3]
 8004422:	236f      	movs	r3, #111	; 0x6f
 8004424:	18fb      	adds	r3, r7, r3
 8004426:	781b      	ldrb	r3, [r3, #0]
 8004428:	b25b      	sxtb	r3, r3
 800442a:	2b00      	cmp	r3, #0
 800442c:	d108      	bne.n	8004440 <VL53L0X_StaticInit+0x224>
 800442e:	687b      	ldr	r3, [r7, #4]
 8004430:	221c      	movs	r2, #28
 8004432:	18ba      	adds	r2, r7, r2
 8004434:	3310      	adds	r3, #16
 8004436:	0011      	movs	r1, r2
 8004438:	2240      	movs	r2, #64	; 0x40
 800443a:	0018      	movs	r0, r3
 800443c:	f005 f878 	bl	8009530 <memcpy>
 8004440:	256f      	movs	r5, #111	; 0x6f
 8004442:	197b      	adds	r3, r7, r5
 8004444:	781b      	ldrb	r3, [r3, #0]
 8004446:	b25b      	sxtb	r3, r3
 8004448:	2b00      	cmp	r3, #0
 800444a:	d114      	bne.n	8004476 <VL53L0X_StaticInit+0x25a>
 800444c:	197c      	adds	r4, r7, r5
 800444e:	2619      	movs	r6, #25
 8004450:	19ba      	adds	r2, r7, r6
 8004452:	687b      	ldr	r3, [r7, #4]
 8004454:	2101      	movs	r1, #1
 8004456:	0018      	movs	r0, r3
 8004458:	f7ff faba 	bl	80039d0 <VL53L0X_RdByte>
 800445c:	0003      	movs	r3, r0
 800445e:	7023      	strb	r3, [r4, #0]
 8004460:	197b      	adds	r3, r7, r5
 8004462:	781b      	ldrb	r3, [r3, #0]
 8004464:	b25b      	sxtb	r3, r3
 8004466:	2b00      	cmp	r3, #0
 8004468:	d105      	bne.n	8004476 <VL53L0X_StaticInit+0x25a>
 800446a:	19bb      	adds	r3, r7, r6
 800446c:	7819      	ldrb	r1, [r3, #0]
 800446e:	687a      	ldr	r2, [r7, #4]
 8004470:	2388      	movs	r3, #136	; 0x88
 8004472:	005b      	lsls	r3, r3, #1
 8004474:	54d1      	strb	r1, [r2, r3]
 8004476:	226f      	movs	r2, #111	; 0x6f
 8004478:	18bb      	adds	r3, r7, r2
 800447a:	781b      	ldrb	r3, [r3, #0]
 800447c:	b25b      	sxtb	r3, r3
 800447e:	2b00      	cmp	r3, #0
 8004480:	d108      	bne.n	8004494 <VL53L0X_StaticInit+0x278>
 8004482:	18bc      	adds	r4, r7, r2
 8004484:	687b      	ldr	r3, [r7, #4]
 8004486:	2200      	movs	r2, #0
 8004488:	2100      	movs	r1, #0
 800448a:	0018      	movs	r0, r3
 800448c:	f000 f9fe 	bl	800488c <VL53L0X_SetSequenceStepEnable>
 8004490:	0003      	movs	r3, r0
 8004492:	7023      	strb	r3, [r4, #0]
 8004494:	226f      	movs	r2, #111	; 0x6f
 8004496:	18bb      	adds	r3, r7, r2
 8004498:	781b      	ldrb	r3, [r3, #0]
 800449a:	b25b      	sxtb	r3, r3
 800449c:	2b00      	cmp	r3, #0
 800449e:	d108      	bne.n	80044b2 <VL53L0X_StaticInit+0x296>
 80044a0:	18bc      	adds	r4, r7, r2
 80044a2:	687b      	ldr	r3, [r7, #4]
 80044a4:	2200      	movs	r2, #0
 80044a6:	2102      	movs	r1, #2
 80044a8:	0018      	movs	r0, r3
 80044aa:	f000 f9ef 	bl	800488c <VL53L0X_SetSequenceStepEnable>
 80044ae:	0003      	movs	r3, r0
 80044b0:	7023      	strb	r3, [r4, #0]
 80044b2:	236f      	movs	r3, #111	; 0x6f
 80044b4:	18fb      	adds	r3, r7, r3
 80044b6:	781b      	ldrb	r3, [r3, #0]
 80044b8:	b25b      	sxtb	r3, r3
 80044ba:	2b00      	cmp	r3, #0
 80044bc:	d104      	bne.n	80044c8 <VL53L0X_StaticInit+0x2ac>
 80044be:	687a      	ldr	r2, [r7, #4]
 80044c0:	2389      	movs	r3, #137	; 0x89
 80044c2:	005b      	lsls	r3, r3, #1
 80044c4:	2103      	movs	r1, #3
 80044c6:	54d1      	strb	r1, [r2, r3]
 80044c8:	226f      	movs	r2, #111	; 0x6f
 80044ca:	18bb      	adds	r3, r7, r2
 80044cc:	781b      	ldrb	r3, [r3, #0]
 80044ce:	b25b      	sxtb	r3, r3
 80044d0:	2b00      	cmp	r3, #0
 80044d2:	d109      	bne.n	80044e8 <VL53L0X_StaticInit+0x2cc>
 80044d4:	18bc      	adds	r4, r7, r2
 80044d6:	2313      	movs	r3, #19
 80044d8:	18fa      	adds	r2, r7, r3
 80044da:	687b      	ldr	r3, [r7, #4]
 80044dc:	2100      	movs	r1, #0
 80044de:	0018      	movs	r0, r3
 80044e0:	f000 f9b6 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 80044e4:	0003      	movs	r3, r0
 80044e6:	7023      	strb	r3, [r4, #0]
 80044e8:	236f      	movs	r3, #111	; 0x6f
 80044ea:	18fb      	adds	r3, r7, r3
 80044ec:	781b      	ldrb	r3, [r3, #0]
 80044ee:	b25b      	sxtb	r3, r3
 80044f0:	2b00      	cmp	r3, #0
 80044f2:	d105      	bne.n	8004500 <VL53L0X_StaticInit+0x2e4>
 80044f4:	2313      	movs	r3, #19
 80044f6:	18fb      	adds	r3, r7, r3
 80044f8:	7819      	ldrb	r1, [r3, #0]
 80044fa:	687b      	ldr	r3, [r7, #4]
 80044fc:	22e8      	movs	r2, #232	; 0xe8
 80044fe:	5499      	strb	r1, [r3, r2]
 8004500:	226f      	movs	r2, #111	; 0x6f
 8004502:	18bb      	adds	r3, r7, r2
 8004504:	781b      	ldrb	r3, [r3, #0]
 8004506:	b25b      	sxtb	r3, r3
 8004508:	2b00      	cmp	r3, #0
 800450a:	d109      	bne.n	8004520 <VL53L0X_StaticInit+0x304>
 800450c:	18bc      	adds	r4, r7, r2
 800450e:	2313      	movs	r3, #19
 8004510:	18fa      	adds	r2, r7, r3
 8004512:	687b      	ldr	r3, [r7, #4]
 8004514:	2101      	movs	r1, #1
 8004516:	0018      	movs	r0, r3
 8004518:	f000 f99a 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 800451c:	0003      	movs	r3, r0
 800451e:	7023      	strb	r3, [r4, #0]
 8004520:	236f      	movs	r3, #111	; 0x6f
 8004522:	18fb      	adds	r3, r7, r3
 8004524:	781b      	ldrb	r3, [r3, #0]
 8004526:	b25b      	sxtb	r3, r3
 8004528:	2b00      	cmp	r3, #0
 800452a:	d105      	bne.n	8004538 <VL53L0X_StaticInit+0x31c>
 800452c:	2313      	movs	r3, #19
 800452e:	18fb      	adds	r3, r7, r3
 8004530:	7819      	ldrb	r1, [r3, #0]
 8004532:	687b      	ldr	r3, [r7, #4]
 8004534:	22e0      	movs	r2, #224	; 0xe0
 8004536:	5499      	strb	r1, [r3, r2]
 8004538:	226f      	movs	r2, #111	; 0x6f
 800453a:	18bb      	adds	r3, r7, r2
 800453c:	781b      	ldrb	r3, [r3, #0]
 800453e:	b25b      	sxtb	r3, r3
 8004540:	2b00      	cmp	r3, #0
 8004542:	d109      	bne.n	8004558 <VL53L0X_StaticInit+0x33c>
 8004544:	18bc      	adds	r4, r7, r2
 8004546:	230c      	movs	r3, #12
 8004548:	18fa      	adds	r2, r7, r3
 800454a:	687b      	ldr	r3, [r7, #4]
 800454c:	2103      	movs	r1, #3
 800454e:	0018      	movs	r0, r3
 8004550:	f003 fba3 	bl	8007c9a <get_sequence_step_timeout>
 8004554:	0003      	movs	r3, r0
 8004556:	7023      	strb	r3, [r4, #0]
 8004558:	236f      	movs	r3, #111	; 0x6f
 800455a:	18fb      	adds	r3, r7, r3
 800455c:	781b      	ldrb	r3, [r3, #0]
 800455e:	b25b      	sxtb	r3, r3
 8004560:	2b00      	cmp	r3, #0
 8004562:	d103      	bne.n	800456c <VL53L0X_StaticInit+0x350>
 8004564:	68fa      	ldr	r2, [r7, #12]
 8004566:	687b      	ldr	r3, [r7, #4]
 8004568:	21e4      	movs	r1, #228	; 0xe4
 800456a:	505a      	str	r2, [r3, r1]
 800456c:	226f      	movs	r2, #111	; 0x6f
 800456e:	18bb      	adds	r3, r7, r2
 8004570:	781b      	ldrb	r3, [r3, #0]
 8004572:	b25b      	sxtb	r3, r3
 8004574:	2b00      	cmp	r3, #0
 8004576:	d109      	bne.n	800458c <VL53L0X_StaticInit+0x370>
 8004578:	18bc      	adds	r4, r7, r2
 800457a:	230c      	movs	r3, #12
 800457c:	18fa      	adds	r2, r7, r3
 800457e:	687b      	ldr	r3, [r7, #4]
 8004580:	2104      	movs	r1, #4
 8004582:	0018      	movs	r0, r3
 8004584:	f003 fb89 	bl	8007c9a <get_sequence_step_timeout>
 8004588:	0003      	movs	r3, r0
 800458a:	7023      	strb	r3, [r4, #0]
 800458c:	236f      	movs	r3, #111	; 0x6f
 800458e:	18fb      	adds	r3, r7, r3
 8004590:	781b      	ldrb	r3, [r3, #0]
 8004592:	b25b      	sxtb	r3, r3
 8004594:	2b00      	cmp	r3, #0
 8004596:	d103      	bne.n	80045a0 <VL53L0X_StaticInit+0x384>
 8004598:	68fa      	ldr	r2, [r7, #12]
 800459a:	687b      	ldr	r3, [r7, #4]
 800459c:	21dc      	movs	r1, #220	; 0xdc
 800459e:	505a      	str	r2, [r3, r1]
 80045a0:	236f      	movs	r3, #111	; 0x6f
 80045a2:	18fb      	adds	r3, r7, r3
 80045a4:	781b      	ldrb	r3, [r3, #0]
 80045a6:	b25b      	sxtb	r3, r3
 80045a8:	0018      	movs	r0, r3
 80045aa:	46bd      	mov	sp, r7
 80045ac:	b01d      	add	sp, #116	; 0x74
 80045ae:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80045b0:	20000004 	.word	0x20000004

080045b4 <VL53L0X_GetDeviceParameters>:
 80045b4:	b5b0      	push	{r4, r5, r7, lr}
 80045b6:	b084      	sub	sp, #16
 80045b8:	af00      	add	r7, sp, #0
 80045ba:	6078      	str	r0, [r7, #4]
 80045bc:	6039      	str	r1, [r7, #0]
 80045be:	250f      	movs	r5, #15
 80045c0:	197b      	adds	r3, r7, r5
 80045c2:	2200      	movs	r2, #0
 80045c4:	701a      	strb	r2, [r3, #0]
 80045c6:	683a      	ldr	r2, [r7, #0]
 80045c8:	197c      	adds	r4, r7, r5
 80045ca:	687b      	ldr	r3, [r7, #4]
 80045cc:	0011      	movs	r1, r2
 80045ce:	0018      	movs	r0, r3
 80045d0:	f000 f8d2 	bl	8004778 <VL53L0X_GetDeviceMode>
 80045d4:	0003      	movs	r3, r0
 80045d6:	7023      	strb	r3, [r4, #0]
 80045d8:	0029      	movs	r1, r5
 80045da:	187b      	adds	r3, r7, r1
 80045dc:	781b      	ldrb	r3, [r3, #0]
 80045de:	b25b      	sxtb	r3, r3
 80045e0:	2b00      	cmp	r3, #0
 80045e2:	d10a      	bne.n	80045fa <VL53L0X_GetDeviceParameters+0x46>
 80045e4:	683b      	ldr	r3, [r7, #0]
 80045e6:	3308      	adds	r3, #8
 80045e8:	001a      	movs	r2, r3
 80045ea:	187c      	adds	r4, r7, r1
 80045ec:	687b      	ldr	r3, [r7, #4]
 80045ee:	0011      	movs	r1, r2
 80045f0:	0018      	movs	r0, r3
 80045f2:	f000 fb02 	bl	8004bfa <VL53L0X_GetInterMeasurementPeriodMilliSeconds>
 80045f6:	0003      	movs	r3, r0
 80045f8:	7023      	strb	r3, [r4, #0]
 80045fa:	230f      	movs	r3, #15
 80045fc:	18fb      	adds	r3, r7, r3
 80045fe:	781b      	ldrb	r3, [r3, #0]
 8004600:	b25b      	sxtb	r3, r3
 8004602:	2b00      	cmp	r3, #0
 8004604:	d102      	bne.n	800460c <VL53L0X_GetDeviceParameters+0x58>
 8004606:	683b      	ldr	r3, [r7, #0]
 8004608:	2200      	movs	r2, #0
 800460a:	731a      	strb	r2, [r3, #12]
 800460c:	210f      	movs	r1, #15
 800460e:	187b      	adds	r3, r7, r1
 8004610:	781b      	ldrb	r3, [r3, #0]
 8004612:	b25b      	sxtb	r3, r3
 8004614:	2b00      	cmp	r3, #0
 8004616:	d10a      	bne.n	800462e <VL53L0X_GetDeviceParameters+0x7a>
 8004618:	683b      	ldr	r3, [r7, #0]
 800461a:	3310      	adds	r3, #16
 800461c:	001a      	movs	r2, r3
 800461e:	187c      	adds	r4, r7, r1
 8004620:	687b      	ldr	r3, [r7, #4]
 8004622:	0011      	movs	r1, r2
 8004624:	0018      	movs	r0, r3
 8004626:	f000 fb45 	bl	8004cb4 <VL53L0X_GetXTalkCompensationRateMegaCps>
 800462a:	0003      	movs	r3, r0
 800462c:	7023      	strb	r3, [r4, #0]
 800462e:	210f      	movs	r1, #15
 8004630:	187b      	adds	r3, r7, r1
 8004632:	781b      	ldrb	r3, [r3, #0]
 8004634:	b25b      	sxtb	r3, r3
 8004636:	2b00      	cmp	r3, #0
 8004638:	d10a      	bne.n	8004650 <VL53L0X_GetDeviceParameters+0x9c>
 800463a:	683b      	ldr	r3, [r7, #0]
 800463c:	3314      	adds	r3, #20
 800463e:	001a      	movs	r2, r3
 8004640:	187c      	adds	r4, r7, r1
 8004642:	687b      	ldr	r3, [r7, #4]
 8004644:	0011      	movs	r1, r2
 8004646:	0018      	movs	r0, r3
 8004648:	f7ff fc0e 	bl	8003e68 <VL53L0X_GetOffsetCalibrationDataMicroMeter>
 800464c:	0003      	movs	r3, r0
 800464e:	7023      	strb	r3, [r4, #0]
 8004650:	230f      	movs	r3, #15
 8004652:	18fb      	adds	r3, r7, r3
 8004654:	781b      	ldrb	r3, [r3, #0]
 8004656:	b25b      	sxtb	r3, r3
 8004658:	2b00      	cmp	r3, #0
 800465a:	d13b      	bne.n	80046d4 <VL53L0X_GetDeviceParameters+0x120>
 800465c:	2300      	movs	r3, #0
 800465e:	60bb      	str	r3, [r7, #8]
 8004660:	e031      	b.n	80046c6 <VL53L0X_GetDeviceParameters+0x112>
 8004662:	240f      	movs	r4, #15
 8004664:	193b      	adds	r3, r7, r4
 8004666:	781b      	ldrb	r3, [r3, #0]
 8004668:	b25b      	sxtb	r3, r3
 800466a:	2b00      	cmp	r3, #0
 800466c:	d12f      	bne.n	80046ce <VL53L0X_GetDeviceParameters+0x11a>
 800466e:	68bb      	ldr	r3, [r7, #8]
 8004670:	b299      	uxth	r1, r3
 8004672:	68bb      	ldr	r3, [r7, #8]
 8004674:	3308      	adds	r3, #8
 8004676:	009b      	lsls	r3, r3, #2
 8004678:	683a      	ldr	r2, [r7, #0]
 800467a:	18d3      	adds	r3, r2, r3
 800467c:	1d1a      	adds	r2, r3, #4
 800467e:	687b      	ldr	r3, [r7, #4]
 8004680:	0018      	movs	r0, r3
 8004682:	f000 fcb3 	bl	8004fec <VL53L0X_GetLimitCheckValue>
 8004686:	0003      	movs	r3, r0
 8004688:	0019      	movs	r1, r3
 800468a:	193b      	adds	r3, r7, r4
 800468c:	193a      	adds	r2, r7, r4
 800468e:	7812      	ldrb	r2, [r2, #0]
 8004690:	430a      	orrs	r2, r1
 8004692:	701a      	strb	r2, [r3, #0]
 8004694:	193b      	adds	r3, r7, r4
 8004696:	781b      	ldrb	r3, [r3, #0]
 8004698:	b25b      	sxtb	r3, r3
 800469a:	2b00      	cmp	r3, #0
 800469c:	d119      	bne.n	80046d2 <VL53L0X_GetDeviceParameters+0x11e>
 800469e:	68bb      	ldr	r3, [r7, #8]
 80046a0:	b299      	uxth	r1, r3
 80046a2:	68bb      	ldr	r3, [r7, #8]
 80046a4:	3318      	adds	r3, #24
 80046a6:	683a      	ldr	r2, [r7, #0]
 80046a8:	18d2      	adds	r2, r2, r3
 80046aa:	687b      	ldr	r3, [r7, #4]
 80046ac:	0018      	movs	r0, r3
 80046ae:	f000 fbf9 	bl	8004ea4 <VL53L0X_GetLimitCheckEnable>
 80046b2:	0003      	movs	r3, r0
 80046b4:	0019      	movs	r1, r3
 80046b6:	193b      	adds	r3, r7, r4
 80046b8:	193a      	adds	r2, r7, r4
 80046ba:	7812      	ldrb	r2, [r2, #0]
 80046bc:	430a      	orrs	r2, r1
 80046be:	701a      	strb	r2, [r3, #0]
 80046c0:	68bb      	ldr	r3, [r7, #8]
 80046c2:	3301      	adds	r3, #1
 80046c4:	60bb      	str	r3, [r7, #8]
 80046c6:	68bb      	ldr	r3, [r7, #8]
 80046c8:	2b05      	cmp	r3, #5
 80046ca:	ddca      	ble.n	8004662 <VL53L0X_GetDeviceParameters+0xae>
 80046cc:	e002      	b.n	80046d4 <VL53L0X_GetDeviceParameters+0x120>
 80046ce:	46c0      	nop			; (mov r8, r8)
 80046d0:	e000      	b.n	80046d4 <VL53L0X_GetDeviceParameters+0x120>
 80046d2:	46c0      	nop			; (mov r8, r8)
 80046d4:	210f      	movs	r1, #15
 80046d6:	187b      	adds	r3, r7, r1
 80046d8:	781b      	ldrb	r3, [r3, #0]
 80046da:	b25b      	sxtb	r3, r3
 80046dc:	2b00      	cmp	r3, #0
 80046de:	d10a      	bne.n	80046f6 <VL53L0X_GetDeviceParameters+0x142>
 80046e0:	683b      	ldr	r3, [r7, #0]
 80046e2:	333c      	adds	r3, #60	; 0x3c
 80046e4:	001a      	movs	r2, r3
 80046e6:	187c      	adds	r4, r7, r1
 80046e8:	687b      	ldr	r3, [r7, #4]
 80046ea:	0011      	movs	r1, r2
 80046ec:	0018      	movs	r0, r3
 80046ee:	f000 fd2d 	bl	800514c <VL53L0X_GetWrapAroundCheckEnable>
 80046f2:	0003      	movs	r3, r0
 80046f4:	7023      	strb	r3, [r4, #0]
 80046f6:	210f      	movs	r1, #15
 80046f8:	187b      	adds	r3, r7, r1
 80046fa:	781b      	ldrb	r3, [r3, #0]
 80046fc:	b25b      	sxtb	r3, r3
 80046fe:	2b00      	cmp	r3, #0
 8004700:	d109      	bne.n	8004716 <VL53L0X_GetDeviceParameters+0x162>
 8004702:	683b      	ldr	r3, [r7, #0]
 8004704:	1d1a      	adds	r2, r3, #4
 8004706:	187c      	adds	r4, r7, r1
 8004708:	687b      	ldr	r3, [r7, #4]
 800470a:	0011      	movs	r1, r2
 800470c:	0018      	movs	r0, r3
 800470e:	f000 f886 	bl	800481e <VL53L0X_GetMeasurementTimingBudgetMicroSeconds>
 8004712:	0003      	movs	r3, r0
 8004714:	7023      	strb	r3, [r4, #0]
 8004716:	230f      	movs	r3, #15
 8004718:	18fb      	adds	r3, r7, r3
 800471a:	781b      	ldrb	r3, [r3, #0]
 800471c:	b25b      	sxtb	r3, r3
 800471e:	0018      	movs	r0, r3
 8004720:	46bd      	mov	sp, r7
 8004722:	b004      	add	sp, #16
 8004724:	bdb0      	pop	{r4, r5, r7, pc}
	...

08004728 <VL53L0X_SetDeviceMode>:
 8004728:	b580      	push	{r7, lr}
 800472a:	b084      	sub	sp, #16
 800472c:	af00      	add	r7, sp, #0
 800472e:	6078      	str	r0, [r7, #4]
 8004730:	000a      	movs	r2, r1
 8004732:	1cfb      	adds	r3, r7, #3
 8004734:	701a      	strb	r2, [r3, #0]
 8004736:	230f      	movs	r3, #15
 8004738:	18fb      	adds	r3, r7, r3
 800473a:	2200      	movs	r2, #0
 800473c:	701a      	strb	r2, [r3, #0]
 800473e:	1cfb      	adds	r3, r7, #3
 8004740:	781b      	ldrb	r3, [r3, #0]
 8004742:	2b15      	cmp	r3, #21
 8004744:	d809      	bhi.n	800475a <VL53L0X_SetDeviceMode+0x32>
 8004746:	009a      	lsls	r2, r3, #2
 8004748:	4b0a      	ldr	r3, [pc, #40]	; (8004774 <VL53L0X_SetDeviceMode+0x4c>)
 800474a:	18d3      	adds	r3, r2, r3
 800474c:	681b      	ldr	r3, [r3, #0]
 800474e:	469f      	mov	pc, r3
 8004750:	687b      	ldr	r3, [r7, #4]
 8004752:	1cfa      	adds	r2, r7, #3
 8004754:	7812      	ldrb	r2, [r2, #0]
 8004756:	741a      	strb	r2, [r3, #16]
 8004758:	e003      	b.n	8004762 <VL53L0X_SetDeviceMode+0x3a>
 800475a:	230f      	movs	r3, #15
 800475c:	18fb      	adds	r3, r7, r3
 800475e:	22f8      	movs	r2, #248	; 0xf8
 8004760:	701a      	strb	r2, [r3, #0]
 8004762:	230f      	movs	r3, #15
 8004764:	18fb      	adds	r3, r7, r3
 8004766:	781b      	ldrb	r3, [r3, #0]
 8004768:	b25b      	sxtb	r3, r3
 800476a:	0018      	movs	r0, r3
 800476c:	46bd      	mov	sp, r7
 800476e:	b004      	add	sp, #16
 8004770:	bd80      	pop	{r7, pc}
 8004772:	46c0      	nop			; (mov r8, r8)
 8004774:	08009fe4 	.word	0x08009fe4

08004778 <VL53L0X_GetDeviceMode>:
 8004778:	b580      	push	{r7, lr}
 800477a:	b084      	sub	sp, #16
 800477c:	af00      	add	r7, sp, #0
 800477e:	6078      	str	r0, [r7, #4]
 8004780:	6039      	str	r1, [r7, #0]
 8004782:	210f      	movs	r1, #15
 8004784:	187b      	adds	r3, r7, r1
 8004786:	2200      	movs	r2, #0
 8004788:	701a      	strb	r2, [r3, #0]
 800478a:	687b      	ldr	r3, [r7, #4]
 800478c:	7c1a      	ldrb	r2, [r3, #16]
 800478e:	683b      	ldr	r3, [r7, #0]
 8004790:	701a      	strb	r2, [r3, #0]
 8004792:	187b      	adds	r3, r7, r1
 8004794:	781b      	ldrb	r3, [r3, #0]
 8004796:	b25b      	sxtb	r3, r3
 8004798:	0018      	movs	r0, r3
 800479a:	46bd      	mov	sp, r7
 800479c:	b004      	add	sp, #16
 800479e:	bd80      	pop	{r7, pc}

080047a0 <VL53L0X_GetFractionEnable>:
 80047a0:	b5b0      	push	{r4, r5, r7, lr}
 80047a2:	b084      	sub	sp, #16
 80047a4:	af00      	add	r7, sp, #0
 80047a6:	6078      	str	r0, [r7, #4]
 80047a8:	6039      	str	r1, [r7, #0]
 80047aa:	250f      	movs	r5, #15
 80047ac:	197b      	adds	r3, r7, r5
 80047ae:	2200      	movs	r2, #0
 80047b0:	701a      	strb	r2, [r3, #0]
 80047b2:	197c      	adds	r4, r7, r5
 80047b4:	683a      	ldr	r2, [r7, #0]
 80047b6:	687b      	ldr	r3, [r7, #4]
 80047b8:	2109      	movs	r1, #9
 80047ba:	0018      	movs	r0, r3
 80047bc:	f7ff f908 	bl	80039d0 <VL53L0X_RdByte>
 80047c0:	0003      	movs	r3, r0
 80047c2:	7023      	strb	r3, [r4, #0]
 80047c4:	197b      	adds	r3, r7, r5
 80047c6:	781b      	ldrb	r3, [r3, #0]
 80047c8:	b25b      	sxtb	r3, r3
 80047ca:	2b00      	cmp	r3, #0
 80047cc:	d106      	bne.n	80047dc <VL53L0X_GetFractionEnable+0x3c>
 80047ce:	683b      	ldr	r3, [r7, #0]
 80047d0:	781b      	ldrb	r3, [r3, #0]
 80047d2:	2201      	movs	r2, #1
 80047d4:	4013      	ands	r3, r2
 80047d6:	b2da      	uxtb	r2, r3
 80047d8:	683b      	ldr	r3, [r7, #0]
 80047da:	701a      	strb	r2, [r3, #0]
 80047dc:	230f      	movs	r3, #15
 80047de:	18fb      	adds	r3, r7, r3
 80047e0:	781b      	ldrb	r3, [r3, #0]
 80047e2:	b25b      	sxtb	r3, r3
 80047e4:	0018      	movs	r0, r3
 80047e6:	46bd      	mov	sp, r7
 80047e8:	b004      	add	sp, #16
 80047ea:	bdb0      	pop	{r4, r5, r7, pc}

080047ec <VL53L0X_SetMeasurementTimingBudgetMicroSeconds>:
 80047ec:	b5b0      	push	{r4, r5, r7, lr}
 80047ee:	b084      	sub	sp, #16
 80047f0:	af00      	add	r7, sp, #0
 80047f2:	6078      	str	r0, [r7, #4]
 80047f4:	6039      	str	r1, [r7, #0]
 80047f6:	250f      	movs	r5, #15
 80047f8:	197b      	adds	r3, r7, r5
 80047fa:	2200      	movs	r2, #0
 80047fc:	701a      	strb	r2, [r3, #0]
 80047fe:	197c      	adds	r4, r7, r5
 8004800:	683a      	ldr	r2, [r7, #0]
 8004802:	687b      	ldr	r3, [r7, #4]
 8004804:	0011      	movs	r1, r2
 8004806:	0018      	movs	r0, r3
 8004808:	f003 fce2 	bl	80081d0 <VL53L0X_set_measurement_timing_budget_micro_seconds>
 800480c:	0003      	movs	r3, r0
 800480e:	7023      	strb	r3, [r4, #0]
 8004810:	197b      	adds	r3, r7, r5
 8004812:	781b      	ldrb	r3, [r3, #0]
 8004814:	b25b      	sxtb	r3, r3
 8004816:	0018      	movs	r0, r3
 8004818:	46bd      	mov	sp, r7
 800481a:	b004      	add	sp, #16
 800481c:	bdb0      	pop	{r4, r5, r7, pc}

0800481e <VL53L0X_GetMeasurementTimingBudgetMicroSeconds>:
 800481e:	b5b0      	push	{r4, r5, r7, lr}
 8004820:	b084      	sub	sp, #16
 8004822:	af00      	add	r7, sp, #0
 8004824:	6078      	str	r0, [r7, #4]
 8004826:	6039      	str	r1, [r7, #0]
 8004828:	250f      	movs	r5, #15
 800482a:	197b      	adds	r3, r7, r5
 800482c:	2200      	movs	r2, #0
 800482e:	701a      	strb	r2, [r3, #0]
 8004830:	197c      	adds	r4, r7, r5
 8004832:	683a      	ldr	r2, [r7, #0]
 8004834:	687b      	ldr	r3, [r7, #4]
 8004836:	0011      	movs	r1, r2
 8004838:	0018      	movs	r0, r3
 800483a:	f003 fdd5 	bl	80083e8 <VL53L0X_get_measurement_timing_budget_micro_seconds>
 800483e:	0003      	movs	r3, r0
 8004840:	7023      	strb	r3, [r4, #0]
 8004842:	197b      	adds	r3, r7, r5
 8004844:	781b      	ldrb	r3, [r3, #0]
 8004846:	b25b      	sxtb	r3, r3
 8004848:	0018      	movs	r0, r3
 800484a:	46bd      	mov	sp, r7
 800484c:	b004      	add	sp, #16
 800484e:	bdb0      	pop	{r4, r5, r7, pc}

08004850 <VL53L0X_GetVcselPulsePeriod>:
 8004850:	b5b0      	push	{r4, r5, r7, lr}
 8004852:	b086      	sub	sp, #24
 8004854:	af00      	add	r7, sp, #0
 8004856:	60f8      	str	r0, [r7, #12]
 8004858:	607a      	str	r2, [r7, #4]
 800485a:	200b      	movs	r0, #11
 800485c:	183b      	adds	r3, r7, r0
 800485e:	1c0a      	adds	r2, r1, #0
 8004860:	701a      	strb	r2, [r3, #0]
 8004862:	2517      	movs	r5, #23
 8004864:	197b      	adds	r3, r7, r5
 8004866:	2200      	movs	r2, #0
 8004868:	701a      	strb	r2, [r3, #0]
 800486a:	197c      	adds	r4, r7, r5
 800486c:	687a      	ldr	r2, [r7, #4]
 800486e:	183b      	adds	r3, r7, r0
 8004870:	7819      	ldrb	r1, [r3, #0]
 8004872:	68fb      	ldr	r3, [r7, #12]
 8004874:	0018      	movs	r0, r3
 8004876:	f003 fc62 	bl	800813e <VL53L0X_get_vcsel_pulse_period>
 800487a:	0003      	movs	r3, r0
 800487c:	7023      	strb	r3, [r4, #0]
 800487e:	197b      	adds	r3, r7, r5
 8004880:	781b      	ldrb	r3, [r3, #0]
 8004882:	b25b      	sxtb	r3, r3
 8004884:	0018      	movs	r0, r3
 8004886:	46bd      	mov	sp, r7
 8004888:	b006      	add	sp, #24
 800488a:	bdb0      	pop	{r4, r5, r7, pc}

0800488c <VL53L0X_SetSequenceStepEnable>:
 800488c:	b5f0      	push	{r4, r5, r6, r7, lr}
 800488e:	b087      	sub	sp, #28
 8004890:	af00      	add	r7, sp, #0
 8004892:	6078      	str	r0, [r7, #4]
 8004894:	0008      	movs	r0, r1
 8004896:	0011      	movs	r1, r2
 8004898:	1cfb      	adds	r3, r7, #3
 800489a:	1c02      	adds	r2, r0, #0
 800489c:	701a      	strb	r2, [r3, #0]
 800489e:	1cbb      	adds	r3, r7, #2
 80048a0:	1c0a      	adds	r2, r1, #0
 80048a2:	701a      	strb	r2, [r3, #0]
 80048a4:	2517      	movs	r5, #23
 80048a6:	197b      	adds	r3, r7, r5
 80048a8:	2200      	movs	r2, #0
 80048aa:	701a      	strb	r2, [r3, #0]
 80048ac:	210f      	movs	r1, #15
 80048ae:	187b      	adds	r3, r7, r1
 80048b0:	2200      	movs	r2, #0
 80048b2:	701a      	strb	r2, [r3, #0]
 80048b4:	2316      	movs	r3, #22
 80048b6:	18fb      	adds	r3, r7, r3
 80048b8:	2200      	movs	r2, #0
 80048ba:	701a      	strb	r2, [r3, #0]
 80048bc:	197c      	adds	r4, r7, r5
 80048be:	000e      	movs	r6, r1
 80048c0:	187a      	adds	r2, r7, r1
 80048c2:	687b      	ldr	r3, [r7, #4]
 80048c4:	2101      	movs	r1, #1
 80048c6:	0018      	movs	r0, r3
 80048c8:	f7ff f882 	bl	80039d0 <VL53L0X_RdByte>
 80048cc:	0003      	movs	r3, r0
 80048ce:	7023      	strb	r3, [r4, #0]
 80048d0:	2316      	movs	r3, #22
 80048d2:	18fb      	adds	r3, r7, r3
 80048d4:	19ba      	adds	r2, r7, r6
 80048d6:	7812      	ldrb	r2, [r2, #0]
 80048d8:	701a      	strb	r2, [r3, #0]
 80048da:	197b      	adds	r3, r7, r5
 80048dc:	781b      	ldrb	r3, [r3, #0]
 80048de:	b25b      	sxtb	r3, r3
 80048e0:	2b00      	cmp	r3, #0
 80048e2:	d000      	beq.n	80048e6 <VL53L0X_SetSequenceStepEnable+0x5a>
 80048e4:	e070      	b.n	80049c8 <VL53L0X_SetSequenceStepEnable+0x13c>
 80048e6:	1cbb      	adds	r3, r7, #2
 80048e8:	781b      	ldrb	r3, [r3, #0]
 80048ea:	2b01      	cmp	r3, #1
 80048ec:	d136      	bne.n	800495c <VL53L0X_SetSequenceStepEnable+0xd0>
 80048ee:	1cfb      	adds	r3, r7, #3
 80048f0:	781b      	ldrb	r3, [r3, #0]
 80048f2:	2b04      	cmp	r3, #4
 80048f4:	d82d      	bhi.n	8004952 <VL53L0X_SetSequenceStepEnable+0xc6>
 80048f6:	009a      	lsls	r2, r3, #2
 80048f8:	4b52      	ldr	r3, [pc, #328]	; (8004a44 <VL53L0X_SetSequenceStepEnable+0x1b8>)
 80048fa:	18d3      	adds	r3, r2, r3
 80048fc:	681b      	ldr	r3, [r3, #0]
 80048fe:	469f      	mov	pc, r3
 8004900:	2216      	movs	r2, #22
 8004902:	18bb      	adds	r3, r7, r2
 8004904:	18ba      	adds	r2, r7, r2
 8004906:	7812      	ldrb	r2, [r2, #0]
 8004908:	2110      	movs	r1, #16
 800490a:	430a      	orrs	r2, r1
 800490c:	701a      	strb	r2, [r3, #0]
 800490e:	e05c      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 8004910:	2216      	movs	r2, #22
 8004912:	18bb      	adds	r3, r7, r2
 8004914:	18ba      	adds	r2, r7, r2
 8004916:	7812      	ldrb	r2, [r2, #0]
 8004918:	2128      	movs	r1, #40	; 0x28
 800491a:	430a      	orrs	r2, r1
 800491c:	701a      	strb	r2, [r3, #0]
 800491e:	e054      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 8004920:	2216      	movs	r2, #22
 8004922:	18bb      	adds	r3, r7, r2
 8004924:	18ba      	adds	r2, r7, r2
 8004926:	7812      	ldrb	r2, [r2, #0]
 8004928:	2104      	movs	r1, #4
 800492a:	430a      	orrs	r2, r1
 800492c:	701a      	strb	r2, [r3, #0]
 800492e:	e04c      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 8004930:	2216      	movs	r2, #22
 8004932:	18bb      	adds	r3, r7, r2
 8004934:	18ba      	adds	r2, r7, r2
 8004936:	7812      	ldrb	r2, [r2, #0]
 8004938:	2140      	movs	r1, #64	; 0x40
 800493a:	430a      	orrs	r2, r1
 800493c:	701a      	strb	r2, [r3, #0]
 800493e:	e044      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 8004940:	2216      	movs	r2, #22
 8004942:	18bb      	adds	r3, r7, r2
 8004944:	18ba      	adds	r2, r7, r2
 8004946:	7812      	ldrb	r2, [r2, #0]
 8004948:	2180      	movs	r1, #128	; 0x80
 800494a:	4249      	negs	r1, r1
 800494c:	430a      	orrs	r2, r1
 800494e:	701a      	strb	r2, [r3, #0]
 8004950:	e03b      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 8004952:	2317      	movs	r3, #23
 8004954:	18fb      	adds	r3, r7, r3
 8004956:	22fc      	movs	r2, #252	; 0xfc
 8004958:	701a      	strb	r2, [r3, #0]
 800495a:	e036      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 800495c:	1cfb      	adds	r3, r7, #3
 800495e:	781b      	ldrb	r3, [r3, #0]
 8004960:	2b04      	cmp	r3, #4
 8004962:	d82c      	bhi.n	80049be <VL53L0X_SetSequenceStepEnable+0x132>
 8004964:	009a      	lsls	r2, r3, #2
 8004966:	4b38      	ldr	r3, [pc, #224]	; (8004a48 <VL53L0X_SetSequenceStepEnable+0x1bc>)
 8004968:	18d3      	adds	r3, r2, r3
 800496a:	681b      	ldr	r3, [r3, #0]
 800496c:	469f      	mov	pc, r3
 800496e:	2216      	movs	r2, #22
 8004970:	18bb      	adds	r3, r7, r2
 8004972:	18ba      	adds	r2, r7, r2
 8004974:	7812      	ldrb	r2, [r2, #0]
 8004976:	2110      	movs	r1, #16
 8004978:	438a      	bics	r2, r1
 800497a:	701a      	strb	r2, [r3, #0]
 800497c:	e025      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 800497e:	2216      	movs	r2, #22
 8004980:	18bb      	adds	r3, r7, r2
 8004982:	18ba      	adds	r2, r7, r2
 8004984:	7812      	ldrb	r2, [r2, #0]
 8004986:	2128      	movs	r1, #40	; 0x28
 8004988:	438a      	bics	r2, r1
 800498a:	701a      	strb	r2, [r3, #0]
 800498c:	e01d      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 800498e:	2216      	movs	r2, #22
 8004990:	18bb      	adds	r3, r7, r2
 8004992:	18ba      	adds	r2, r7, r2
 8004994:	7812      	ldrb	r2, [r2, #0]
 8004996:	2104      	movs	r1, #4
 8004998:	438a      	bics	r2, r1
 800499a:	701a      	strb	r2, [r3, #0]
 800499c:	e015      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 800499e:	2216      	movs	r2, #22
 80049a0:	18bb      	adds	r3, r7, r2
 80049a2:	18ba      	adds	r2, r7, r2
 80049a4:	7812      	ldrb	r2, [r2, #0]
 80049a6:	2140      	movs	r1, #64	; 0x40
 80049a8:	438a      	bics	r2, r1
 80049aa:	701a      	strb	r2, [r3, #0]
 80049ac:	e00d      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 80049ae:	2216      	movs	r2, #22
 80049b0:	18bb      	adds	r3, r7, r2
 80049b2:	18ba      	adds	r2, r7, r2
 80049b4:	7812      	ldrb	r2, [r2, #0]
 80049b6:	217f      	movs	r1, #127	; 0x7f
 80049b8:	400a      	ands	r2, r1
 80049ba:	701a      	strb	r2, [r3, #0]
 80049bc:	e005      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 80049be:	2317      	movs	r3, #23
 80049c0:	18fb      	adds	r3, r7, r3
 80049c2:	22fc      	movs	r2, #252	; 0xfc
 80049c4:	701a      	strb	r2, [r3, #0]
 80049c6:	e000      	b.n	80049ca <VL53L0X_SetSequenceStepEnable+0x13e>
 80049c8:	46c0      	nop			; (mov r8, r8)
 80049ca:	230f      	movs	r3, #15
 80049cc:	18fb      	adds	r3, r7, r3
 80049ce:	781b      	ldrb	r3, [r3, #0]
 80049d0:	2116      	movs	r1, #22
 80049d2:	187a      	adds	r2, r7, r1
 80049d4:	7812      	ldrb	r2, [r2, #0]
 80049d6:	429a      	cmp	r2, r3
 80049d8:	d02b      	beq.n	8004a32 <VL53L0X_SetSequenceStepEnable+0x1a6>
 80049da:	2217      	movs	r2, #23
 80049dc:	18bb      	adds	r3, r7, r2
 80049de:	781b      	ldrb	r3, [r3, #0]
 80049e0:	b25b      	sxtb	r3, r3
 80049e2:	2b00      	cmp	r3, #0
 80049e4:	d109      	bne.n	80049fa <VL53L0X_SetSequenceStepEnable+0x16e>
 80049e6:	18bc      	adds	r4, r7, r2
 80049e8:	187b      	adds	r3, r7, r1
 80049ea:	781a      	ldrb	r2, [r3, #0]
 80049ec:	687b      	ldr	r3, [r7, #4]
 80049ee:	2101      	movs	r1, #1
 80049f0:	0018      	movs	r0, r3
 80049f2:	f7fe ff7f 	bl	80038f4 <VL53L0X_WrByte>
 80049f6:	0003      	movs	r3, r0
 80049f8:	7023      	strb	r3, [r4, #0]
 80049fa:	2317      	movs	r3, #23
 80049fc:	18fb      	adds	r3, r7, r3
 80049fe:	781b      	ldrb	r3, [r3, #0]
 8004a00:	b25b      	sxtb	r3, r3
 8004a02:	2b00      	cmp	r3, #0
 8004a04:	d106      	bne.n	8004a14 <VL53L0X_SetSequenceStepEnable+0x188>
 8004a06:	687a      	ldr	r2, [r7, #4]
 8004a08:	2316      	movs	r3, #22
 8004a0a:	18f9      	adds	r1, r7, r3
 8004a0c:	2388      	movs	r3, #136	; 0x88
 8004a0e:	005b      	lsls	r3, r3, #1
 8004a10:	7809      	ldrb	r1, [r1, #0]
 8004a12:	54d1      	strb	r1, [r2, r3]
 8004a14:	2317      	movs	r3, #23
 8004a16:	18fb      	adds	r3, r7, r3
 8004a18:	781b      	ldrb	r3, [r3, #0]
 8004a1a:	b25b      	sxtb	r3, r3
 8004a1c:	2b00      	cmp	r3, #0
 8004a1e:	d108      	bne.n	8004a32 <VL53L0X_SetSequenceStepEnable+0x1a6>
 8004a20:	687b      	ldr	r3, [r7, #4]
 8004a22:	695b      	ldr	r3, [r3, #20]
 8004a24:	613b      	str	r3, [r7, #16]
 8004a26:	693a      	ldr	r2, [r7, #16]
 8004a28:	687b      	ldr	r3, [r7, #4]
 8004a2a:	0011      	movs	r1, r2
 8004a2c:	0018      	movs	r0, r3
 8004a2e:	f7ff fedd 	bl	80047ec <VL53L0X_SetMeasurementTimingBudgetMicroSeconds>
 8004a32:	2317      	movs	r3, #23
 8004a34:	18fb      	adds	r3, r7, r3
 8004a36:	781b      	ldrb	r3, [r3, #0]
 8004a38:	b25b      	sxtb	r3, r3
 8004a3a:	0018      	movs	r0, r3
 8004a3c:	46bd      	mov	sp, r7
 8004a3e:	b007      	add	sp, #28
 8004a40:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8004a42:	46c0      	nop			; (mov r8, r8)
 8004a44:	0800a03c 	.word	0x0800a03c
 8004a48:	0800a050 	.word	0x0800a050

08004a4c <sequence_step_enabled>:
 8004a4c:	b590      	push	{r4, r7, lr}
 8004a4e:	b087      	sub	sp, #28
 8004a50:	af00      	add	r7, sp, #0
 8004a52:	60f8      	str	r0, [r7, #12]
 8004a54:	0008      	movs	r0, r1
 8004a56:	0011      	movs	r1, r2
 8004a58:	607b      	str	r3, [r7, #4]
 8004a5a:	240b      	movs	r4, #11
 8004a5c:	193b      	adds	r3, r7, r4
 8004a5e:	1c02      	adds	r2, r0, #0
 8004a60:	701a      	strb	r2, [r3, #0]
 8004a62:	230a      	movs	r3, #10
 8004a64:	18fb      	adds	r3, r7, r3
 8004a66:	1c0a      	adds	r2, r1, #0
 8004a68:	701a      	strb	r2, [r3, #0]
 8004a6a:	2317      	movs	r3, #23
 8004a6c:	18fb      	adds	r3, r7, r3
 8004a6e:	2200      	movs	r2, #0
 8004a70:	701a      	strb	r2, [r3, #0]
 8004a72:	687b      	ldr	r3, [r7, #4]
 8004a74:	2200      	movs	r2, #0
 8004a76:	701a      	strb	r2, [r3, #0]
 8004a78:	193b      	adds	r3, r7, r4
 8004a7a:	781b      	ldrb	r3, [r3, #0]
 8004a7c:	2b04      	cmp	r3, #4
 8004a7e:	d838      	bhi.n	8004af2 <sequence_step_enabled+0xa6>
 8004a80:	009a      	lsls	r2, r3, #2
 8004a82:	4b22      	ldr	r3, [pc, #136]	; (8004b0c <sequence_step_enabled+0xc0>)
 8004a84:	18d3      	adds	r3, r2, r3
 8004a86:	681b      	ldr	r3, [r3, #0]
 8004a88:	469f      	mov	pc, r3
 8004a8a:	230a      	movs	r3, #10
 8004a8c:	18fb      	adds	r3, r7, r3
 8004a8e:	781b      	ldrb	r3, [r3, #0]
 8004a90:	111b      	asrs	r3, r3, #4
 8004a92:	b2db      	uxtb	r3, r3
 8004a94:	2201      	movs	r2, #1
 8004a96:	4013      	ands	r3, r2
 8004a98:	b2da      	uxtb	r2, r3
 8004a9a:	687b      	ldr	r3, [r7, #4]
 8004a9c:	701a      	strb	r2, [r3, #0]
 8004a9e:	e02c      	b.n	8004afa <sequence_step_enabled+0xae>
 8004aa0:	230a      	movs	r3, #10
 8004aa2:	18fb      	adds	r3, r7, r3
 8004aa4:	781b      	ldrb	r3, [r3, #0]
 8004aa6:	10db      	asrs	r3, r3, #3
 8004aa8:	b2db      	uxtb	r3, r3
 8004aaa:	2201      	movs	r2, #1
 8004aac:	4013      	ands	r3, r2
 8004aae:	b2da      	uxtb	r2, r3
 8004ab0:	687b      	ldr	r3, [r7, #4]
 8004ab2:	701a      	strb	r2, [r3, #0]
 8004ab4:	e021      	b.n	8004afa <sequence_step_enabled+0xae>
 8004ab6:	230a      	movs	r3, #10
 8004ab8:	18fb      	adds	r3, r7, r3
 8004aba:	781b      	ldrb	r3, [r3, #0]
 8004abc:	109b      	asrs	r3, r3, #2
 8004abe:	b2db      	uxtb	r3, r3
 8004ac0:	2201      	movs	r2, #1
 8004ac2:	4013      	ands	r3, r2
 8004ac4:	b2da      	uxtb	r2, r3
 8004ac6:	687b      	ldr	r3, [r7, #4]
 8004ac8:	701a      	strb	r2, [r3, #0]
 8004aca:	e016      	b.n	8004afa <sequence_step_enabled+0xae>
 8004acc:	230a      	movs	r3, #10
 8004ace:	18fb      	adds	r3, r7, r3
 8004ad0:	781b      	ldrb	r3, [r3, #0]
 8004ad2:	119b      	asrs	r3, r3, #6
 8004ad4:	b2db      	uxtb	r3, r3
 8004ad6:	2201      	movs	r2, #1
 8004ad8:	4013      	ands	r3, r2
 8004ada:	b2da      	uxtb	r2, r3
 8004adc:	687b      	ldr	r3, [r7, #4]
 8004ade:	701a      	strb	r2, [r3, #0]
 8004ae0:	e00b      	b.n	8004afa <sequence_step_enabled+0xae>
 8004ae2:	230a      	movs	r3, #10
 8004ae4:	18fb      	adds	r3, r7, r3
 8004ae6:	781b      	ldrb	r3, [r3, #0]
 8004ae8:	09db      	lsrs	r3, r3, #7
 8004aea:	b2da      	uxtb	r2, r3
 8004aec:	687b      	ldr	r3, [r7, #4]
 8004aee:	701a      	strb	r2, [r3, #0]
 8004af0:	e003      	b.n	8004afa <sequence_step_enabled+0xae>
 8004af2:	2317      	movs	r3, #23
 8004af4:	18fb      	adds	r3, r7, r3
 8004af6:	22fc      	movs	r2, #252	; 0xfc
 8004af8:	701a      	strb	r2, [r3, #0]
 8004afa:	2317      	movs	r3, #23
 8004afc:	18fb      	adds	r3, r7, r3
 8004afe:	781b      	ldrb	r3, [r3, #0]
 8004b00:	b25b      	sxtb	r3, r3
 8004b02:	0018      	movs	r0, r3
 8004b04:	46bd      	mov	sp, r7
 8004b06:	b007      	add	sp, #28
 8004b08:	bd90      	pop	{r4, r7, pc}
 8004b0a:	46c0      	nop			; (mov r8, r8)
 8004b0c:	0800a064 	.word	0x0800a064

08004b10 <VL53L0X_GetSequenceStepEnables>:
 8004b10:	b5f0      	push	{r4, r5, r6, r7, lr}
 8004b12:	b085      	sub	sp, #20
 8004b14:	af00      	add	r7, sp, #0
 8004b16:	6078      	str	r0, [r7, #4]
 8004b18:	6039      	str	r1, [r7, #0]
 8004b1a:	250f      	movs	r5, #15
 8004b1c:	197b      	adds	r3, r7, r5
 8004b1e:	2200      	movs	r2, #0
 8004b20:	701a      	strb	r2, [r3, #0]
 8004b22:	260e      	movs	r6, #14
 8004b24:	19bb      	adds	r3, r7, r6
 8004b26:	2200      	movs	r2, #0
 8004b28:	701a      	strb	r2, [r3, #0]
 8004b2a:	197c      	adds	r4, r7, r5
 8004b2c:	19ba      	adds	r2, r7, r6
 8004b2e:	687b      	ldr	r3, [r7, #4]
 8004b30:	2101      	movs	r1, #1
 8004b32:	0018      	movs	r0, r3
 8004b34:	f7fe ff4c 	bl	80039d0 <VL53L0X_RdByte>
 8004b38:	0003      	movs	r3, r0
 8004b3a:	7023      	strb	r3, [r4, #0]
 8004b3c:	197b      	adds	r3, r7, r5
 8004b3e:	781b      	ldrb	r3, [r3, #0]
 8004b40:	b25b      	sxtb	r3, r3
 8004b42:	2b00      	cmp	r3, #0
 8004b44:	d109      	bne.n	8004b5a <VL53L0X_GetSequenceStepEnables+0x4a>
 8004b46:	19bb      	adds	r3, r7, r6
 8004b48:	781a      	ldrb	r2, [r3, #0]
 8004b4a:	683b      	ldr	r3, [r7, #0]
 8004b4c:	197c      	adds	r4, r7, r5
 8004b4e:	6878      	ldr	r0, [r7, #4]
 8004b50:	2100      	movs	r1, #0
 8004b52:	f7ff ff7b 	bl	8004a4c <sequence_step_enabled>
 8004b56:	0003      	movs	r3, r0
 8004b58:	7023      	strb	r3, [r4, #0]
 8004b5a:	210f      	movs	r1, #15
 8004b5c:	187b      	adds	r3, r7, r1
 8004b5e:	781b      	ldrb	r3, [r3, #0]
 8004b60:	b25b      	sxtb	r3, r3
 8004b62:	2b00      	cmp	r3, #0
 8004b64:	d10b      	bne.n	8004b7e <VL53L0X_GetSequenceStepEnables+0x6e>
 8004b66:	230e      	movs	r3, #14
 8004b68:	18fb      	adds	r3, r7, r3
 8004b6a:	781a      	ldrb	r2, [r3, #0]
 8004b6c:	683b      	ldr	r3, [r7, #0]
 8004b6e:	3302      	adds	r3, #2
 8004b70:	187c      	adds	r4, r7, r1
 8004b72:	6878      	ldr	r0, [r7, #4]
 8004b74:	2101      	movs	r1, #1
 8004b76:	f7ff ff69 	bl	8004a4c <sequence_step_enabled>
 8004b7a:	0003      	movs	r3, r0
 8004b7c:	7023      	strb	r3, [r4, #0]
 8004b7e:	210f      	movs	r1, #15
 8004b80:	187b      	adds	r3, r7, r1
 8004b82:	781b      	ldrb	r3, [r3, #0]
 8004b84:	b25b      	sxtb	r3, r3
 8004b86:	2b00      	cmp	r3, #0
 8004b88:	d10b      	bne.n	8004ba2 <VL53L0X_GetSequenceStepEnables+0x92>
 8004b8a:	230e      	movs	r3, #14
 8004b8c:	18fb      	adds	r3, r7, r3
 8004b8e:	781a      	ldrb	r2, [r3, #0]
 8004b90:	683b      	ldr	r3, [r7, #0]
 8004b92:	3301      	adds	r3, #1
 8004b94:	187c      	adds	r4, r7, r1
 8004b96:	6878      	ldr	r0, [r7, #4]
 8004b98:	2102      	movs	r1, #2
 8004b9a:	f7ff ff57 	bl	8004a4c <sequence_step_enabled>
 8004b9e:	0003      	movs	r3, r0
 8004ba0:	7023      	strb	r3, [r4, #0]
 8004ba2:	210f      	movs	r1, #15
 8004ba4:	187b      	adds	r3, r7, r1
 8004ba6:	781b      	ldrb	r3, [r3, #0]
 8004ba8:	b25b      	sxtb	r3, r3
 8004baa:	2b00      	cmp	r3, #0
 8004bac:	d10b      	bne.n	8004bc6 <VL53L0X_GetSequenceStepEnables+0xb6>
 8004bae:	230e      	movs	r3, #14
 8004bb0:	18fb      	adds	r3, r7, r3
 8004bb2:	781a      	ldrb	r2, [r3, #0]
 8004bb4:	683b      	ldr	r3, [r7, #0]
 8004bb6:	3303      	adds	r3, #3
 8004bb8:	187c      	adds	r4, r7, r1
 8004bba:	6878      	ldr	r0, [r7, #4]
 8004bbc:	2103      	movs	r1, #3
 8004bbe:	f7ff ff45 	bl	8004a4c <sequence_step_enabled>
 8004bc2:	0003      	movs	r3, r0
 8004bc4:	7023      	strb	r3, [r4, #0]
 8004bc6:	210f      	movs	r1, #15
 8004bc8:	187b      	adds	r3, r7, r1
 8004bca:	781b      	ldrb	r3, [r3, #0]
 8004bcc:	b25b      	sxtb	r3, r3
 8004bce:	2b00      	cmp	r3, #0
 8004bd0:	d10b      	bne.n	8004bea <VL53L0X_GetSequenceStepEnables+0xda>
 8004bd2:	230e      	movs	r3, #14
 8004bd4:	18fb      	adds	r3, r7, r3
 8004bd6:	781a      	ldrb	r2, [r3, #0]
 8004bd8:	683b      	ldr	r3, [r7, #0]
 8004bda:	3304      	adds	r3, #4
 8004bdc:	187c      	adds	r4, r7, r1
 8004bde:	6878      	ldr	r0, [r7, #4]
 8004be0:	2104      	movs	r1, #4
 8004be2:	f7ff ff33 	bl	8004a4c <sequence_step_enabled>
 8004be6:	0003      	movs	r3, r0
 8004be8:	7023      	strb	r3, [r4, #0]
 8004bea:	230f      	movs	r3, #15
 8004bec:	18fb      	adds	r3, r7, r3
 8004bee:	781b      	ldrb	r3, [r3, #0]
 8004bf0:	b25b      	sxtb	r3, r3
 8004bf2:	0018      	movs	r0, r3
 8004bf4:	46bd      	mov	sp, r7
 8004bf6:	b005      	add	sp, #20
 8004bf8:	bdf0      	pop	{r4, r5, r6, r7, pc}

08004bfa <VL53L0X_GetInterMeasurementPeriodMilliSeconds>:
 8004bfa:	b5b0      	push	{r4, r5, r7, lr}
 8004bfc:	b084      	sub	sp, #16
 8004bfe:	af00      	add	r7, sp, #0
 8004c00:	6078      	str	r0, [r7, #4]
 8004c02:	6039      	str	r1, [r7, #0]
 8004c04:	250f      	movs	r5, #15
 8004c06:	197b      	adds	r3, r7, r5
 8004c08:	2200      	movs	r2, #0
 8004c0a:	701a      	strb	r2, [r3, #0]
 8004c0c:	197c      	adds	r4, r7, r5
 8004c0e:	230c      	movs	r3, #12
 8004c10:	18fa      	adds	r2, r7, r3
 8004c12:	687b      	ldr	r3, [r7, #4]
 8004c14:	21f8      	movs	r1, #248	; 0xf8
 8004c16:	0018      	movs	r0, r3
 8004c18:	f7fe feef 	bl	80039fa <VL53L0X_RdWord>
 8004c1c:	0003      	movs	r3, r0
 8004c1e:	7023      	strb	r3, [r4, #0]
 8004c20:	197b      	adds	r3, r7, r5
 8004c22:	781b      	ldrb	r3, [r3, #0]
 8004c24:	b25b      	sxtb	r3, r3
 8004c26:	2b00      	cmp	r3, #0
 8004c28:	d109      	bne.n	8004c3e <VL53L0X_GetInterMeasurementPeriodMilliSeconds+0x44>
 8004c2a:	197c      	adds	r4, r7, r5
 8004c2c:	2308      	movs	r3, #8
 8004c2e:	18fa      	adds	r2, r7, r3
 8004c30:	687b      	ldr	r3, [r7, #4]
 8004c32:	2104      	movs	r1, #4
 8004c34:	0018      	movs	r0, r3
 8004c36:	f7fe ff03 	bl	8003a40 <VL53L0X_RdDWord>
 8004c3a:	0003      	movs	r3, r0
 8004c3c:	7023      	strb	r3, [r4, #0]
 8004c3e:	230f      	movs	r3, #15
 8004c40:	18fb      	adds	r3, r7, r3
 8004c42:	781b      	ldrb	r3, [r3, #0]
 8004c44:	b25b      	sxtb	r3, r3
 8004c46:	2b00      	cmp	r3, #0
 8004c48:	d113      	bne.n	8004c72 <VL53L0X_GetInterMeasurementPeriodMilliSeconds+0x78>
 8004c4a:	210c      	movs	r1, #12
 8004c4c:	187b      	adds	r3, r7, r1
 8004c4e:	881b      	ldrh	r3, [r3, #0]
 8004c50:	2b00      	cmp	r3, #0
 8004c52:	d00a      	beq.n	8004c6a <VL53L0X_GetInterMeasurementPeriodMilliSeconds+0x70>
 8004c54:	68ba      	ldr	r2, [r7, #8]
 8004c56:	187b      	adds	r3, r7, r1
 8004c58:	881b      	ldrh	r3, [r3, #0]
 8004c5a:	0019      	movs	r1, r3
 8004c5c:	0010      	movs	r0, r2
 8004c5e:	f7fb fa53 	bl	8000108 <__udivsi3>
 8004c62:	0003      	movs	r3, r0
 8004c64:	001a      	movs	r2, r3
 8004c66:	683b      	ldr	r3, [r7, #0]
 8004c68:	601a      	str	r2, [r3, #0]
 8004c6a:	683b      	ldr	r3, [r7, #0]
 8004c6c:	681a      	ldr	r2, [r3, #0]
 8004c6e:	687b      	ldr	r3, [r7, #4]
 8004c70:	619a      	str	r2, [r3, #24]
 8004c72:	230f      	movs	r3, #15
 8004c74:	18fb      	adds	r3, r7, r3
 8004c76:	781b      	ldrb	r3, [r3, #0]
 8004c78:	b25b      	sxtb	r3, r3
 8004c7a:	0018      	movs	r0, r3
 8004c7c:	46bd      	mov	sp, r7
 8004c7e:	b004      	add	sp, #16
 8004c80:	bdb0      	pop	{r4, r5, r7, pc}

08004c82 <VL53L0X_GetXTalkCompensationEnable>:
 8004c82:	b580      	push	{r7, lr}
 8004c84:	b084      	sub	sp, #16
 8004c86:	af00      	add	r7, sp, #0
 8004c88:	6078      	str	r0, [r7, #4]
 8004c8a:	6039      	str	r1, [r7, #0]
 8004c8c:	210f      	movs	r1, #15
 8004c8e:	187b      	adds	r3, r7, r1
 8004c90:	2200      	movs	r2, #0
 8004c92:	701a      	strb	r2, [r3, #0]
 8004c94:	200e      	movs	r0, #14
 8004c96:	183b      	adds	r3, r7, r0
 8004c98:	687a      	ldr	r2, [r7, #4]
 8004c9a:	7f12      	ldrb	r2, [r2, #28]
 8004c9c:	701a      	strb	r2, [r3, #0]
 8004c9e:	683b      	ldr	r3, [r7, #0]
 8004ca0:	183a      	adds	r2, r7, r0
 8004ca2:	7812      	ldrb	r2, [r2, #0]
 8004ca4:	701a      	strb	r2, [r3, #0]
 8004ca6:	187b      	adds	r3, r7, r1
 8004ca8:	781b      	ldrb	r3, [r3, #0]
 8004caa:	b25b      	sxtb	r3, r3
 8004cac:	0018      	movs	r0, r3
 8004cae:	46bd      	mov	sp, r7
 8004cb0:	b004      	add	sp, #16
 8004cb2:	bd80      	pop	{r7, pc}

08004cb4 <VL53L0X_GetXTalkCompensationRateMegaCps>:
 8004cb4:	b5f0      	push	{r4, r5, r6, r7, lr}
 8004cb6:	b087      	sub	sp, #28
 8004cb8:	af00      	add	r7, sp, #0
 8004cba:	6078      	str	r0, [r7, #4]
 8004cbc:	6039      	str	r1, [r7, #0]
 8004cbe:	2517      	movs	r5, #23
 8004cc0:	197b      	adds	r3, r7, r5
 8004cc2:	2200      	movs	r2, #0
 8004cc4:	701a      	strb	r2, [r3, #0]
 8004cc6:	197c      	adds	r4, r7, r5
 8004cc8:	260e      	movs	r6, #14
 8004cca:	19ba      	adds	r2, r7, r6
 8004ccc:	687b      	ldr	r3, [r7, #4]
 8004cce:	2120      	movs	r1, #32
 8004cd0:	0018      	movs	r0, r3
 8004cd2:	f7fe fe92 	bl	80039fa <VL53L0X_RdWord>
 8004cd6:	0003      	movs	r3, r0
 8004cd8:	7023      	strb	r3, [r4, #0]
 8004cda:	197b      	adds	r3, r7, r5
 8004cdc:	781b      	ldrb	r3, [r3, #0]
 8004cde:	b25b      	sxtb	r3, r3
 8004ce0:	2b00      	cmp	r3, #0
 8004ce2:	d11b      	bne.n	8004d1c <VL53L0X_GetXTalkCompensationRateMegaCps+0x68>
 8004ce4:	19bb      	adds	r3, r7, r6
 8004ce6:	881b      	ldrh	r3, [r3, #0]
 8004ce8:	2b00      	cmp	r3, #0
 8004cea:	d109      	bne.n	8004d00 <VL53L0X_GetXTalkCompensationRateMegaCps+0x4c>
 8004cec:	687b      	ldr	r3, [r7, #4]
 8004cee:	6a1b      	ldr	r3, [r3, #32]
 8004cf0:	613b      	str	r3, [r7, #16]
 8004cf2:	683b      	ldr	r3, [r7, #0]
 8004cf4:	693a      	ldr	r2, [r7, #16]
 8004cf6:	601a      	str	r2, [r3, #0]
 8004cf8:	687b      	ldr	r3, [r7, #4]
 8004cfa:	2200      	movs	r2, #0
 8004cfc:	771a      	strb	r2, [r3, #28]
 8004cfe:	e00d      	b.n	8004d1c <VL53L0X_GetXTalkCompensationRateMegaCps+0x68>
 8004d00:	230e      	movs	r3, #14
 8004d02:	18fb      	adds	r3, r7, r3
 8004d04:	881b      	ldrh	r3, [r3, #0]
 8004d06:	00db      	lsls	r3, r3, #3
 8004d08:	613b      	str	r3, [r7, #16]
 8004d0a:	683b      	ldr	r3, [r7, #0]
 8004d0c:	693a      	ldr	r2, [r7, #16]
 8004d0e:	601a      	str	r2, [r3, #0]
 8004d10:	687b      	ldr	r3, [r7, #4]
 8004d12:	693a      	ldr	r2, [r7, #16]
 8004d14:	621a      	str	r2, [r3, #32]
 8004d16:	687b      	ldr	r3, [r7, #4]
 8004d18:	2201      	movs	r2, #1
 8004d1a:	771a      	strb	r2, [r3, #28]
 8004d1c:	2317      	movs	r3, #23
 8004d1e:	18fb      	adds	r3, r7, r3
 8004d20:	781b      	ldrb	r3, [r3, #0]
 8004d22:	b25b      	sxtb	r3, r3
 8004d24:	0018      	movs	r0, r3
 8004d26:	46bd      	mov	sp, r7
 8004d28:	b007      	add	sp, #28
 8004d2a:	bdf0      	pop	{r4, r5, r6, r7, pc}

08004d2c <VL53L0X_SetLimitCheckEnable>:
 8004d2c:	b590      	push	{r4, r7, lr}
 8004d2e:	b087      	sub	sp, #28
 8004d30:	af00      	add	r7, sp, #0
 8004d32:	6078      	str	r0, [r7, #4]
 8004d34:	0008      	movs	r0, r1
 8004d36:	0011      	movs	r1, r2
 8004d38:	1cbb      	adds	r3, r7, #2
 8004d3a:	1c02      	adds	r2, r0, #0
 8004d3c:	801a      	strh	r2, [r3, #0]
 8004d3e:	1c7b      	adds	r3, r7, #1
 8004d40:	1c0a      	adds	r2, r1, #0
 8004d42:	701a      	strb	r2, [r3, #0]
 8004d44:	2117      	movs	r1, #23
 8004d46:	187b      	adds	r3, r7, r1
 8004d48:	2200      	movs	r2, #0
 8004d4a:	701a      	strb	r2, [r3, #0]
 8004d4c:	2300      	movs	r3, #0
 8004d4e:	613b      	str	r3, [r7, #16]
 8004d50:	230f      	movs	r3, #15
 8004d52:	18fb      	adds	r3, r7, r3
 8004d54:	2200      	movs	r2, #0
 8004d56:	701a      	strb	r2, [r3, #0]
 8004d58:	230e      	movs	r3, #14
 8004d5a:	18fb      	adds	r3, r7, r3
 8004d5c:	2200      	movs	r2, #0
 8004d5e:	701a      	strb	r2, [r3, #0]
 8004d60:	1cbb      	adds	r3, r7, #2
 8004d62:	881b      	ldrh	r3, [r3, #0]
 8004d64:	2b05      	cmp	r3, #5
 8004d66:	d903      	bls.n	8004d70 <VL53L0X_SetLimitCheckEnable+0x44>
 8004d68:	187b      	adds	r3, r7, r1
 8004d6a:	22fc      	movs	r2, #252	; 0xfc
 8004d6c:	701a      	strb	r2, [r3, #0]
 8004d6e:	e074      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004d70:	1c7b      	adds	r3, r7, #1
 8004d72:	781b      	ldrb	r3, [r3, #0]
 8004d74:	2b00      	cmp	r3, #0
 8004d76:	d10a      	bne.n	8004d8e <VL53L0X_SetLimitCheckEnable+0x62>
 8004d78:	2300      	movs	r3, #0
 8004d7a:	613b      	str	r3, [r7, #16]
 8004d7c:	230f      	movs	r3, #15
 8004d7e:	18fb      	adds	r3, r7, r3
 8004d80:	2200      	movs	r2, #0
 8004d82:	701a      	strb	r2, [r3, #0]
 8004d84:	230e      	movs	r3, #14
 8004d86:	18fb      	adds	r3, r7, r3
 8004d88:	2201      	movs	r2, #1
 8004d8a:	701a      	strb	r2, [r3, #0]
 8004d8c:	e010      	b.n	8004db0 <VL53L0X_SetLimitCheckEnable+0x84>
 8004d8e:	1cbb      	adds	r3, r7, #2
 8004d90:	881b      	ldrh	r3, [r3, #0]
 8004d92:	687a      	ldr	r2, [r7, #4]
 8004d94:	330c      	adds	r3, #12
 8004d96:	009b      	lsls	r3, r3, #2
 8004d98:	18d3      	adds	r3, r2, r3
 8004d9a:	3304      	adds	r3, #4
 8004d9c:	681b      	ldr	r3, [r3, #0]
 8004d9e:	613b      	str	r3, [r7, #16]
 8004da0:	230e      	movs	r3, #14
 8004da2:	18fb      	adds	r3, r7, r3
 8004da4:	2200      	movs	r2, #0
 8004da6:	701a      	strb	r2, [r3, #0]
 8004da8:	230f      	movs	r3, #15
 8004daa:	18fb      	adds	r3, r7, r3
 8004dac:	2201      	movs	r2, #1
 8004dae:	701a      	strb	r2, [r3, #0]
 8004db0:	1cbb      	adds	r3, r7, #2
 8004db2:	881b      	ldrh	r3, [r3, #0]
 8004db4:	2b05      	cmp	r3, #5
 8004db6:	d84c      	bhi.n	8004e52 <VL53L0X_SetLimitCheckEnable+0x126>
 8004db8:	009a      	lsls	r2, r3, #2
 8004dba:	4b39      	ldr	r3, [pc, #228]	; (8004ea0 <VL53L0X_SetLimitCheckEnable+0x174>)
 8004dbc:	18d3      	adds	r3, r2, r3
 8004dbe:	681b      	ldr	r3, [r3, #0]
 8004dc0:	469f      	mov	pc, r3
 8004dc2:	687b      	ldr	r3, [r7, #4]
 8004dc4:	220f      	movs	r2, #15
 8004dc6:	18ba      	adds	r2, r7, r2
 8004dc8:	2128      	movs	r1, #40	; 0x28
 8004dca:	7812      	ldrb	r2, [r2, #0]
 8004dcc:	545a      	strb	r2, [r3, r1]
 8004dce:	e044      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004dd0:	693b      	ldr	r3, [r7, #16]
 8004dd2:	0a5b      	lsrs	r3, r3, #9
 8004dd4:	b29a      	uxth	r2, r3
 8004dd6:	2317      	movs	r3, #23
 8004dd8:	18fc      	adds	r4, r7, r3
 8004dda:	687b      	ldr	r3, [r7, #4]
 8004ddc:	2144      	movs	r1, #68	; 0x44
 8004dde:	0018      	movs	r0, r3
 8004de0:	f7fe fda0 	bl	8003924 <VL53L0X_WrWord>
 8004de4:	0003      	movs	r3, r0
 8004de6:	7023      	strb	r3, [r4, #0]
 8004de8:	e037      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004dea:	687b      	ldr	r3, [r7, #4]
 8004dec:	220f      	movs	r2, #15
 8004dee:	18ba      	adds	r2, r7, r2
 8004df0:	212a      	movs	r1, #42	; 0x2a
 8004df2:	7812      	ldrb	r2, [r2, #0]
 8004df4:	545a      	strb	r2, [r3, r1]
 8004df6:	e030      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004df8:	687b      	ldr	r3, [r7, #4]
 8004dfa:	220f      	movs	r2, #15
 8004dfc:	18ba      	adds	r2, r7, r2
 8004dfe:	212b      	movs	r1, #43	; 0x2b
 8004e00:	7812      	ldrb	r2, [r2, #0]
 8004e02:	545a      	strb	r2, [r3, r1]
 8004e04:	e029      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004e06:	210d      	movs	r1, #13
 8004e08:	187a      	adds	r2, r7, r1
 8004e0a:	230e      	movs	r3, #14
 8004e0c:	18fb      	adds	r3, r7, r3
 8004e0e:	781b      	ldrb	r3, [r3, #0]
 8004e10:	18db      	adds	r3, r3, r3
 8004e12:	7013      	strb	r3, [r2, #0]
 8004e14:	2317      	movs	r3, #23
 8004e16:	18fc      	adds	r4, r7, r3
 8004e18:	187b      	adds	r3, r7, r1
 8004e1a:	781b      	ldrb	r3, [r3, #0]
 8004e1c:	6878      	ldr	r0, [r7, #4]
 8004e1e:	22fe      	movs	r2, #254	; 0xfe
 8004e20:	2160      	movs	r1, #96	; 0x60
 8004e22:	f7fe fda4 	bl	800396e <VL53L0X_UpdateByte>
 8004e26:	0003      	movs	r3, r0
 8004e28:	7023      	strb	r3, [r4, #0]
 8004e2a:	e016      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004e2c:	210d      	movs	r1, #13
 8004e2e:	187b      	adds	r3, r7, r1
 8004e30:	220e      	movs	r2, #14
 8004e32:	18ba      	adds	r2, r7, r2
 8004e34:	7812      	ldrb	r2, [r2, #0]
 8004e36:	0112      	lsls	r2, r2, #4
 8004e38:	701a      	strb	r2, [r3, #0]
 8004e3a:	2317      	movs	r3, #23
 8004e3c:	18fc      	adds	r4, r7, r3
 8004e3e:	187b      	adds	r3, r7, r1
 8004e40:	781b      	ldrb	r3, [r3, #0]
 8004e42:	6878      	ldr	r0, [r7, #4]
 8004e44:	22ef      	movs	r2, #239	; 0xef
 8004e46:	2160      	movs	r1, #96	; 0x60
 8004e48:	f7fe fd91 	bl	800396e <VL53L0X_UpdateByte>
 8004e4c:	0003      	movs	r3, r0
 8004e4e:	7023      	strb	r3, [r4, #0]
 8004e50:	e003      	b.n	8004e5a <VL53L0X_SetLimitCheckEnable+0x12e>
 8004e52:	2317      	movs	r3, #23
 8004e54:	18fb      	adds	r3, r7, r3
 8004e56:	22fc      	movs	r2, #252	; 0xfc
 8004e58:	701a      	strb	r2, [r3, #0]
 8004e5a:	2317      	movs	r3, #23
 8004e5c:	18fb      	adds	r3, r7, r3
 8004e5e:	781b      	ldrb	r3, [r3, #0]
 8004e60:	b25b      	sxtb	r3, r3
 8004e62:	2b00      	cmp	r3, #0
 8004e64:	d114      	bne.n	8004e90 <VL53L0X_SetLimitCheckEnable+0x164>
 8004e66:	1c7b      	adds	r3, r7, #1
 8004e68:	781b      	ldrb	r3, [r3, #0]
 8004e6a:	2b00      	cmp	r3, #0
 8004e6c:	d108      	bne.n	8004e80 <VL53L0X_SetLimitCheckEnable+0x154>
 8004e6e:	1cbb      	adds	r3, r7, #2
 8004e70:	881b      	ldrh	r3, [r3, #0]
 8004e72:	687a      	ldr	r2, [r7, #4]
 8004e74:	2128      	movs	r1, #40	; 0x28
 8004e76:	18d3      	adds	r3, r2, r3
 8004e78:	185b      	adds	r3, r3, r1
 8004e7a:	2200      	movs	r2, #0
 8004e7c:	701a      	strb	r2, [r3, #0]
 8004e7e:	e007      	b.n	8004e90 <VL53L0X_SetLimitCheckEnable+0x164>
 8004e80:	1cbb      	adds	r3, r7, #2
 8004e82:	881b      	ldrh	r3, [r3, #0]
 8004e84:	687a      	ldr	r2, [r7, #4]
 8004e86:	2128      	movs	r1, #40	; 0x28
 8004e88:	18d3      	adds	r3, r2, r3
 8004e8a:	185b      	adds	r3, r3, r1
 8004e8c:	2201      	movs	r2, #1
 8004e8e:	701a      	strb	r2, [r3, #0]
 8004e90:	2317      	movs	r3, #23
 8004e92:	18fb      	adds	r3, r7, r3
 8004e94:	781b      	ldrb	r3, [r3, #0]
 8004e96:	b25b      	sxtb	r3, r3
 8004e98:	0018      	movs	r0, r3
 8004e9a:	46bd      	mov	sp, r7
 8004e9c:	b007      	add	sp, #28
 8004e9e:	bd90      	pop	{r4, r7, pc}
 8004ea0:	0800a078 	.word	0x0800a078

08004ea4 <VL53L0X_GetLimitCheckEnable>:
 8004ea4:	b590      	push	{r4, r7, lr}
 8004ea6:	b087      	sub	sp, #28
 8004ea8:	af00      	add	r7, sp, #0
 8004eaa:	60f8      	str	r0, [r7, #12]
 8004eac:	607a      	str	r2, [r7, #4]
 8004eae:	200a      	movs	r0, #10
 8004eb0:	183b      	adds	r3, r7, r0
 8004eb2:	1c0a      	adds	r2, r1, #0
 8004eb4:	801a      	strh	r2, [r3, #0]
 8004eb6:	2117      	movs	r1, #23
 8004eb8:	187b      	adds	r3, r7, r1
 8004eba:	2200      	movs	r2, #0
 8004ebc:	701a      	strb	r2, [r3, #0]
 8004ebe:	183b      	adds	r3, r7, r0
 8004ec0:	881b      	ldrh	r3, [r3, #0]
 8004ec2:	2b05      	cmp	r3, #5
 8004ec4:	d906      	bls.n	8004ed4 <VL53L0X_GetLimitCheckEnable+0x30>
 8004ec6:	187b      	adds	r3, r7, r1
 8004ec8:	22fc      	movs	r2, #252	; 0xfc
 8004eca:	701a      	strb	r2, [r3, #0]
 8004ecc:	687b      	ldr	r3, [r7, #4]
 8004ece:	2200      	movs	r2, #0
 8004ed0:	701a      	strb	r2, [r3, #0]
 8004ed2:	e00e      	b.n	8004ef2 <VL53L0X_GetLimitCheckEnable+0x4e>
 8004ed4:	230a      	movs	r3, #10
 8004ed6:	18fb      	adds	r3, r7, r3
 8004ed8:	881a      	ldrh	r2, [r3, #0]
 8004eda:	2416      	movs	r4, #22
 8004edc:	193b      	adds	r3, r7, r4
 8004ede:	68f9      	ldr	r1, [r7, #12]
 8004ee0:	2028      	movs	r0, #40	; 0x28
 8004ee2:	188a      	adds	r2, r1, r2
 8004ee4:	1812      	adds	r2, r2, r0
 8004ee6:	7812      	ldrb	r2, [r2, #0]
 8004ee8:	701a      	strb	r2, [r3, #0]
 8004eea:	687b      	ldr	r3, [r7, #4]
 8004eec:	193a      	adds	r2, r7, r4
 8004eee:	7812      	ldrb	r2, [r2, #0]
 8004ef0:	701a      	strb	r2, [r3, #0]
 8004ef2:	2317      	movs	r3, #23
 8004ef4:	18fb      	adds	r3, r7, r3
 8004ef6:	781b      	ldrb	r3, [r3, #0]
 8004ef8:	b25b      	sxtb	r3, r3
 8004efa:	0018      	movs	r0, r3
 8004efc:	46bd      	mov	sp, r7
 8004efe:	b007      	add	sp, #28
 8004f00:	bd90      	pop	{r4, r7, pc}
	...

08004f04 <VL53L0X_SetLimitCheckValue>:
 8004f04:	b5b0      	push	{r4, r5, r7, lr}
 8004f06:	b086      	sub	sp, #24
 8004f08:	af00      	add	r7, sp, #0
 8004f0a:	60f8      	str	r0, [r7, #12]
 8004f0c:	607a      	str	r2, [r7, #4]
 8004f0e:	240a      	movs	r4, #10
 8004f10:	193b      	adds	r3, r7, r4
 8004f12:	1c0a      	adds	r2, r1, #0
 8004f14:	801a      	strh	r2, [r3, #0]
 8004f16:	2317      	movs	r3, #23
 8004f18:	18fb      	adds	r3, r7, r3
 8004f1a:	2200      	movs	r2, #0
 8004f1c:	701a      	strb	r2, [r3, #0]
 8004f1e:	193b      	adds	r3, r7, r4
 8004f20:	881a      	ldrh	r2, [r3, #0]
 8004f22:	2516      	movs	r5, #22
 8004f24:	197b      	adds	r3, r7, r5
 8004f26:	68f9      	ldr	r1, [r7, #12]
 8004f28:	2028      	movs	r0, #40	; 0x28
 8004f2a:	188a      	adds	r2, r1, r2
 8004f2c:	1812      	adds	r2, r2, r0
 8004f2e:	7812      	ldrb	r2, [r2, #0]
 8004f30:	701a      	strb	r2, [r3, #0]
 8004f32:	197b      	adds	r3, r7, r5
 8004f34:	781b      	ldrb	r3, [r3, #0]
 8004f36:	2b00      	cmp	r3, #0
 8004f38:	d109      	bne.n	8004f4e <VL53L0X_SetLimitCheckValue+0x4a>
 8004f3a:	193b      	adds	r3, r7, r4
 8004f3c:	881b      	ldrh	r3, [r3, #0]
 8004f3e:	68fa      	ldr	r2, [r7, #12]
 8004f40:	330c      	adds	r3, #12
 8004f42:	009b      	lsls	r3, r3, #2
 8004f44:	18d3      	adds	r3, r2, r3
 8004f46:	3304      	adds	r3, #4
 8004f48:	687a      	ldr	r2, [r7, #4]
 8004f4a:	601a      	str	r2, [r3, #0]
 8004f4c:	e043      	b.n	8004fd6 <VL53L0X_SetLimitCheckValue+0xd2>
 8004f4e:	230a      	movs	r3, #10
 8004f50:	18fb      	adds	r3, r7, r3
 8004f52:	881b      	ldrh	r3, [r3, #0]
 8004f54:	2b05      	cmp	r3, #5
 8004f56:	d82a      	bhi.n	8004fae <VL53L0X_SetLimitCheckValue+0xaa>
 8004f58:	009a      	lsls	r2, r3, #2
 8004f5a:	4b23      	ldr	r3, [pc, #140]	; (8004fe8 <VL53L0X_SetLimitCheckValue+0xe4>)
 8004f5c:	18d3      	adds	r3, r2, r3
 8004f5e:	681b      	ldr	r3, [r3, #0]
 8004f60:	469f      	mov	pc, r3
 8004f62:	68fb      	ldr	r3, [r7, #12]
 8004f64:	687a      	ldr	r2, [r7, #4]
 8004f66:	635a      	str	r2, [r3, #52]	; 0x34
 8004f68:	e025      	b.n	8004fb6 <VL53L0X_SetLimitCheckValue+0xb2>
 8004f6a:	687b      	ldr	r3, [r7, #4]
 8004f6c:	0a5b      	lsrs	r3, r3, #9
 8004f6e:	b29a      	uxth	r2, r3
 8004f70:	2317      	movs	r3, #23
 8004f72:	18fc      	adds	r4, r7, r3
 8004f74:	68fb      	ldr	r3, [r7, #12]
 8004f76:	2144      	movs	r1, #68	; 0x44
 8004f78:	0018      	movs	r0, r3
 8004f7a:	f7fe fcd3 	bl	8003924 <VL53L0X_WrWord>
 8004f7e:	0003      	movs	r3, r0
 8004f80:	7023      	strb	r3, [r4, #0]
 8004f82:	e018      	b.n	8004fb6 <VL53L0X_SetLimitCheckValue+0xb2>
 8004f84:	68fb      	ldr	r3, [r7, #12]
 8004f86:	687a      	ldr	r2, [r7, #4]
 8004f88:	63da      	str	r2, [r3, #60]	; 0x3c
 8004f8a:	e014      	b.n	8004fb6 <VL53L0X_SetLimitCheckValue+0xb2>
 8004f8c:	68fb      	ldr	r3, [r7, #12]
 8004f8e:	687a      	ldr	r2, [r7, #4]
 8004f90:	641a      	str	r2, [r3, #64]	; 0x40
 8004f92:	e010      	b.n	8004fb6 <VL53L0X_SetLimitCheckValue+0xb2>
 8004f94:	687b      	ldr	r3, [r7, #4]
 8004f96:	0a5b      	lsrs	r3, r3, #9
 8004f98:	b29a      	uxth	r2, r3
 8004f9a:	2317      	movs	r3, #23
 8004f9c:	18fc      	adds	r4, r7, r3
 8004f9e:	68fb      	ldr	r3, [r7, #12]
 8004fa0:	2164      	movs	r1, #100	; 0x64
 8004fa2:	0018      	movs	r0, r3
 8004fa4:	f7fe fcbe 	bl	8003924 <VL53L0X_WrWord>
 8004fa8:	0003      	movs	r3, r0
 8004faa:	7023      	strb	r3, [r4, #0]
 8004fac:	e003      	b.n	8004fb6 <VL53L0X_SetLimitCheckValue+0xb2>
 8004fae:	2317      	movs	r3, #23
 8004fb0:	18fb      	adds	r3, r7, r3
 8004fb2:	22fc      	movs	r2, #252	; 0xfc
 8004fb4:	701a      	strb	r2, [r3, #0]
 8004fb6:	2317      	movs	r3, #23
 8004fb8:	18fb      	adds	r3, r7, r3
 8004fba:	781b      	ldrb	r3, [r3, #0]
 8004fbc:	b25b      	sxtb	r3, r3
 8004fbe:	2b00      	cmp	r3, #0
 8004fc0:	d109      	bne.n	8004fd6 <VL53L0X_SetLimitCheckValue+0xd2>
 8004fc2:	230a      	movs	r3, #10
 8004fc4:	18fb      	adds	r3, r7, r3
 8004fc6:	881b      	ldrh	r3, [r3, #0]
 8004fc8:	68fa      	ldr	r2, [r7, #12]
 8004fca:	330c      	adds	r3, #12
 8004fcc:	009b      	lsls	r3, r3, #2
 8004fce:	18d3      	adds	r3, r2, r3
 8004fd0:	3304      	adds	r3, #4
 8004fd2:	687a      	ldr	r2, [r7, #4]
 8004fd4:	601a      	str	r2, [r3, #0]
 8004fd6:	2317      	movs	r3, #23
 8004fd8:	18fb      	adds	r3, r7, r3
 8004fda:	781b      	ldrb	r3, [r3, #0]
 8004fdc:	b25b      	sxtb	r3, r3
 8004fde:	0018      	movs	r0, r3
 8004fe0:	46bd      	mov	sp, r7
 8004fe2:	b006      	add	sp, #24
 8004fe4:	bdb0      	pop	{r4, r5, r7, pc}
 8004fe6:	46c0      	nop			; (mov r8, r8)
 8004fe8:	0800a090 	.word	0x0800a090

08004fec <VL53L0X_GetLimitCheckValue>:
 8004fec:	b5f0      	push	{r4, r5, r6, r7, lr}
 8004fee:	b089      	sub	sp, #36	; 0x24
 8004ff0:	af00      	add	r7, sp, #0
 8004ff2:	60f8      	str	r0, [r7, #12]
 8004ff4:	607a      	str	r2, [r7, #4]
 8004ff6:	200a      	movs	r0, #10
 8004ff8:	183b      	adds	r3, r7, r0
 8004ffa:	1c0a      	adds	r2, r1, #0
 8004ffc:	801a      	strh	r2, [r3, #0]
 8004ffe:	231f      	movs	r3, #31
 8005000:	18fb      	adds	r3, r7, r3
 8005002:	2200      	movs	r2, #0
 8005004:	701a      	strb	r2, [r3, #0]
 8005006:	231e      	movs	r3, #30
 8005008:	18fb      	adds	r3, r7, r3
 800500a:	2200      	movs	r2, #0
 800500c:	701a      	strb	r2, [r3, #0]
 800500e:	183b      	adds	r3, r7, r0
 8005010:	881b      	ldrh	r3, [r3, #0]
 8005012:	2b05      	cmp	r3, #5
 8005014:	d84e      	bhi.n	80050b4 <VL53L0X_GetLimitCheckValue+0xc8>
 8005016:	009a      	lsls	r2, r3, #2
 8005018:	4b4b      	ldr	r3, [pc, #300]	; (8005148 <VL53L0X_GetLimitCheckValue+0x15c>)
 800501a:	18d3      	adds	r3, r2, r3
 800501c:	681b      	ldr	r3, [r3, #0]
 800501e:	469f      	mov	pc, r3
 8005020:	68fb      	ldr	r3, [r7, #12]
 8005022:	6b5b      	ldr	r3, [r3, #52]	; 0x34
 8005024:	61bb      	str	r3, [r7, #24]
 8005026:	231e      	movs	r3, #30
 8005028:	18fb      	adds	r3, r7, r3
 800502a:	2200      	movs	r2, #0
 800502c:	701a      	strb	r2, [r3, #0]
 800502e:	e045      	b.n	80050bc <VL53L0X_GetLimitCheckValue+0xd0>
 8005030:	251f      	movs	r5, #31
 8005032:	197c      	adds	r4, r7, r5
 8005034:	2616      	movs	r6, #22
 8005036:	19ba      	adds	r2, r7, r6
 8005038:	68fb      	ldr	r3, [r7, #12]
 800503a:	2144      	movs	r1, #68	; 0x44
 800503c:	0018      	movs	r0, r3
 800503e:	f7fe fcdc 	bl	80039fa <VL53L0X_RdWord>
 8005042:	0003      	movs	r3, r0
 8005044:	7023      	strb	r3, [r4, #0]
 8005046:	197b      	adds	r3, r7, r5
 8005048:	781b      	ldrb	r3, [r3, #0]
 800504a:	b25b      	sxtb	r3, r3
 800504c:	2b00      	cmp	r3, #0
 800504e:	d103      	bne.n	8005058 <VL53L0X_GetLimitCheckValue+0x6c>
 8005050:	19bb      	adds	r3, r7, r6
 8005052:	881b      	ldrh	r3, [r3, #0]
 8005054:	025b      	lsls	r3, r3, #9
 8005056:	61bb      	str	r3, [r7, #24]
 8005058:	231e      	movs	r3, #30
 800505a:	18fb      	adds	r3, r7, r3
 800505c:	2201      	movs	r2, #1
 800505e:	701a      	strb	r2, [r3, #0]
 8005060:	e02c      	b.n	80050bc <VL53L0X_GetLimitCheckValue+0xd0>
 8005062:	68fb      	ldr	r3, [r7, #12]
 8005064:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8005066:	61bb      	str	r3, [r7, #24]
 8005068:	231e      	movs	r3, #30
 800506a:	18fb      	adds	r3, r7, r3
 800506c:	2200      	movs	r2, #0
 800506e:	701a      	strb	r2, [r3, #0]
 8005070:	e024      	b.n	80050bc <VL53L0X_GetLimitCheckValue+0xd0>
 8005072:	68fb      	ldr	r3, [r7, #12]
 8005074:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8005076:	61bb      	str	r3, [r7, #24]
 8005078:	231e      	movs	r3, #30
 800507a:	18fb      	adds	r3, r7, r3
 800507c:	2200      	movs	r2, #0
 800507e:	701a      	strb	r2, [r3, #0]
 8005080:	e01c      	b.n	80050bc <VL53L0X_GetLimitCheckValue+0xd0>
 8005082:	251f      	movs	r5, #31
 8005084:	197c      	adds	r4, r7, r5
 8005086:	2616      	movs	r6, #22
 8005088:	19ba      	adds	r2, r7, r6
 800508a:	68fb      	ldr	r3, [r7, #12]
 800508c:	2164      	movs	r1, #100	; 0x64
 800508e:	0018      	movs	r0, r3
 8005090:	f7fe fcb3 	bl	80039fa <VL53L0X_RdWord>
 8005094:	0003      	movs	r3, r0
 8005096:	7023      	strb	r3, [r4, #0]
 8005098:	197b      	adds	r3, r7, r5
 800509a:	781b      	ldrb	r3, [r3, #0]
 800509c:	b25b      	sxtb	r3, r3
 800509e:	2b00      	cmp	r3, #0
 80050a0:	d103      	bne.n	80050aa <VL53L0X_GetLimitCheckValue+0xbe>
 80050a2:	19bb      	adds	r3, r7, r6
 80050a4:	881b      	ldrh	r3, [r3, #0]
 80050a6:	025b      	lsls	r3, r3, #9
 80050a8:	61bb      	str	r3, [r7, #24]
 80050aa:	231e      	movs	r3, #30
 80050ac:	18fb      	adds	r3, r7, r3
 80050ae:	2200      	movs	r2, #0
 80050b0:	701a      	strb	r2, [r3, #0]
 80050b2:	e003      	b.n	80050bc <VL53L0X_GetLimitCheckValue+0xd0>
 80050b4:	231f      	movs	r3, #31
 80050b6:	18fb      	adds	r3, r7, r3
 80050b8:	22fc      	movs	r2, #252	; 0xfc
 80050ba:	701a      	strb	r2, [r3, #0]
 80050bc:	231f      	movs	r3, #31
 80050be:	18fb      	adds	r3, r7, r3
 80050c0:	781b      	ldrb	r3, [r3, #0]
 80050c2:	b25b      	sxtb	r3, r3
 80050c4:	2b00      	cmp	r3, #0
 80050c6:	d136      	bne.n	8005136 <VL53L0X_GetLimitCheckValue+0x14a>
 80050c8:	231e      	movs	r3, #30
 80050ca:	18fb      	adds	r3, r7, r3
 80050cc:	781b      	ldrb	r3, [r3, #0]
 80050ce:	2b01      	cmp	r3, #1
 80050d0:	d12e      	bne.n	8005130 <VL53L0X_GetLimitCheckValue+0x144>
 80050d2:	69bb      	ldr	r3, [r7, #24]
 80050d4:	2b00      	cmp	r3, #0
 80050d6:	d115      	bne.n	8005104 <VL53L0X_GetLimitCheckValue+0x118>
 80050d8:	210a      	movs	r1, #10
 80050da:	187b      	adds	r3, r7, r1
 80050dc:	881b      	ldrh	r3, [r3, #0]
 80050de:	68fa      	ldr	r2, [r7, #12]
 80050e0:	330c      	adds	r3, #12
 80050e2:	009b      	lsls	r3, r3, #2
 80050e4:	18d3      	adds	r3, r2, r3
 80050e6:	3304      	adds	r3, #4
 80050e8:	681b      	ldr	r3, [r3, #0]
 80050ea:	61bb      	str	r3, [r7, #24]
 80050ec:	687b      	ldr	r3, [r7, #4]
 80050ee:	69ba      	ldr	r2, [r7, #24]
 80050f0:	601a      	str	r2, [r3, #0]
 80050f2:	187b      	adds	r3, r7, r1
 80050f4:	881b      	ldrh	r3, [r3, #0]
 80050f6:	68fa      	ldr	r2, [r7, #12]
 80050f8:	2128      	movs	r1, #40	; 0x28
 80050fa:	18d3      	adds	r3, r2, r3
 80050fc:	185b      	adds	r3, r3, r1
 80050fe:	2200      	movs	r2, #0
 8005100:	701a      	strb	r2, [r3, #0]
 8005102:	e018      	b.n	8005136 <VL53L0X_GetLimitCheckValue+0x14a>
 8005104:	687b      	ldr	r3, [r7, #4]
 8005106:	69ba      	ldr	r2, [r7, #24]
 8005108:	601a      	str	r2, [r3, #0]
 800510a:	210a      	movs	r1, #10
 800510c:	187b      	adds	r3, r7, r1
 800510e:	881b      	ldrh	r3, [r3, #0]
 8005110:	68fa      	ldr	r2, [r7, #12]
 8005112:	330c      	adds	r3, #12
 8005114:	009b      	lsls	r3, r3, #2
 8005116:	18d3      	adds	r3, r2, r3
 8005118:	3304      	adds	r3, #4
 800511a:	69ba      	ldr	r2, [r7, #24]
 800511c:	601a      	str	r2, [r3, #0]
 800511e:	187b      	adds	r3, r7, r1
 8005120:	881b      	ldrh	r3, [r3, #0]
 8005122:	68fa      	ldr	r2, [r7, #12]
 8005124:	2128      	movs	r1, #40	; 0x28
 8005126:	18d3      	adds	r3, r2, r3
 8005128:	185b      	adds	r3, r3, r1
 800512a:	2201      	movs	r2, #1
 800512c:	701a      	strb	r2, [r3, #0]
 800512e:	e002      	b.n	8005136 <VL53L0X_GetLimitCheckValue+0x14a>
 8005130:	687b      	ldr	r3, [r7, #4]
 8005132:	69ba      	ldr	r2, [r7, #24]
 8005134:	601a      	str	r2, [r3, #0]
 8005136:	231f      	movs	r3, #31
 8005138:	18fb      	adds	r3, r7, r3
 800513a:	781b      	ldrb	r3, [r3, #0]
 800513c:	b25b      	sxtb	r3, r3
 800513e:	0018      	movs	r0, r3
 8005140:	46bd      	mov	sp, r7
 8005142:	b009      	add	sp, #36	; 0x24
 8005144:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8005146:	46c0      	nop			; (mov r8, r8)
 8005148:	0800a0a8 	.word	0x0800a0a8

0800514c <VL53L0X_GetWrapAroundCheckEnable>:
 800514c:	b5f0      	push	{r4, r5, r6, r7, lr}
 800514e:	b085      	sub	sp, #20
 8005150:	af00      	add	r7, sp, #0
 8005152:	6078      	str	r0, [r7, #4]
 8005154:	6039      	str	r1, [r7, #0]
 8005156:	250f      	movs	r5, #15
 8005158:	197b      	adds	r3, r7, r5
 800515a:	2200      	movs	r2, #0
 800515c:	701a      	strb	r2, [r3, #0]
 800515e:	197c      	adds	r4, r7, r5
 8005160:	260e      	movs	r6, #14
 8005162:	19ba      	adds	r2, r7, r6
 8005164:	687b      	ldr	r3, [r7, #4]
 8005166:	2101      	movs	r1, #1
 8005168:	0018      	movs	r0, r3
 800516a:	f7fe fc31 	bl	80039d0 <VL53L0X_RdByte>
 800516e:	0003      	movs	r3, r0
 8005170:	7023      	strb	r3, [r4, #0]
 8005172:	197b      	adds	r3, r7, r5
 8005174:	781b      	ldrb	r3, [r3, #0]
 8005176:	b25b      	sxtb	r3, r3
 8005178:	2b00      	cmp	r3, #0
 800517a:	d111      	bne.n	80051a0 <VL53L0X_GetWrapAroundCheckEnable+0x54>
 800517c:	19bb      	adds	r3, r7, r6
 800517e:	7819      	ldrb	r1, [r3, #0]
 8005180:	687a      	ldr	r2, [r7, #4]
 8005182:	2388      	movs	r3, #136	; 0x88
 8005184:	005b      	lsls	r3, r3, #1
 8005186:	54d1      	strb	r1, [r2, r3]
 8005188:	19bb      	adds	r3, r7, r6
 800518a:	781b      	ldrb	r3, [r3, #0]
 800518c:	b25b      	sxtb	r3, r3
 800518e:	2b00      	cmp	r3, #0
 8005190:	da03      	bge.n	800519a <VL53L0X_GetWrapAroundCheckEnable+0x4e>
 8005192:	683b      	ldr	r3, [r7, #0]
 8005194:	2201      	movs	r2, #1
 8005196:	701a      	strb	r2, [r3, #0]
 8005198:	e002      	b.n	80051a0 <VL53L0X_GetWrapAroundCheckEnable+0x54>
 800519a:	683b      	ldr	r3, [r7, #0]
 800519c:	2200      	movs	r2, #0
 800519e:	701a      	strb	r2, [r3, #0]
 80051a0:	230f      	movs	r3, #15
 80051a2:	18fb      	adds	r3, r7, r3
 80051a4:	781b      	ldrb	r3, [r3, #0]
 80051a6:	b25b      	sxtb	r3, r3
 80051a8:	2b00      	cmp	r3, #0
 80051aa:	d104      	bne.n	80051b6 <VL53L0X_GetWrapAroundCheckEnable+0x6a>
 80051ac:	683b      	ldr	r3, [r7, #0]
 80051ae:	7819      	ldrb	r1, [r3, #0]
 80051b0:	687b      	ldr	r3, [r7, #4]
 80051b2:	224c      	movs	r2, #76	; 0x4c
 80051b4:	5499      	strb	r1, [r3, r2]
 80051b6:	230f      	movs	r3, #15
 80051b8:	18fb      	adds	r3, r7, r3
 80051ba:	781b      	ldrb	r3, [r3, #0]
 80051bc:	b25b      	sxtb	r3, r3
 80051be:	0018      	movs	r0, r3
 80051c0:	46bd      	mov	sp, r7
 80051c2:	b005      	add	sp, #20
 80051c4:	bdf0      	pop	{r4, r5, r6, r7, pc}

080051c6 <VL53L0X_PerformSingleMeasurement>:
 80051c6:	b5f0      	push	{r4, r5, r6, r7, lr}
 80051c8:	b085      	sub	sp, #20
 80051ca:	af00      	add	r7, sp, #0
 80051cc:	6078      	str	r0, [r7, #4]
 80051ce:	250f      	movs	r5, #15
 80051d0:	197b      	adds	r3, r7, r5
 80051d2:	2200      	movs	r2, #0
 80051d4:	701a      	strb	r2, [r3, #0]
 80051d6:	197c      	adds	r4, r7, r5
 80051d8:	260e      	movs	r6, #14
 80051da:	19ba      	adds	r2, r7, r6
 80051dc:	687b      	ldr	r3, [r7, #4]
 80051de:	0011      	movs	r1, r2
 80051e0:	0018      	movs	r0, r3
 80051e2:	f7ff fac9 	bl	8004778 <VL53L0X_GetDeviceMode>
 80051e6:	0003      	movs	r3, r0
 80051e8:	7023      	strb	r3, [r4, #0]
 80051ea:	002a      	movs	r2, r5
 80051ec:	18bb      	adds	r3, r7, r2
 80051ee:	781b      	ldrb	r3, [r3, #0]
 80051f0:	b25b      	sxtb	r3, r3
 80051f2:	2b00      	cmp	r3, #0
 80051f4:	d10a      	bne.n	800520c <VL53L0X_PerformSingleMeasurement+0x46>
 80051f6:	19bb      	adds	r3, r7, r6
 80051f8:	781b      	ldrb	r3, [r3, #0]
 80051fa:	2b00      	cmp	r3, #0
 80051fc:	d106      	bne.n	800520c <VL53L0X_PerformSingleMeasurement+0x46>
 80051fe:	18bc      	adds	r4, r7, r2
 8005200:	687b      	ldr	r3, [r7, #4]
 8005202:	0018      	movs	r0, r3
 8005204:	f000 f8ca 	bl	800539c <VL53L0X_StartMeasurement>
 8005208:	0003      	movs	r3, r0
 800520a:	7023      	strb	r3, [r4, #0]
 800520c:	220f      	movs	r2, #15
 800520e:	18bb      	adds	r3, r7, r2
 8005210:	781b      	ldrb	r3, [r3, #0]
 8005212:	b25b      	sxtb	r3, r3
 8005214:	2b00      	cmp	r3, #0
 8005216:	d106      	bne.n	8005226 <VL53L0X_PerformSingleMeasurement+0x60>
 8005218:	18bc      	adds	r4, r7, r2
 800521a:	687b      	ldr	r3, [r7, #4]
 800521c:	0018      	movs	r0, r3
 800521e:	f001 fe63 	bl	8006ee8 <VL53L0X_measurement_poll_for_completion>
 8005222:	0003      	movs	r3, r0
 8005224:	7023      	strb	r3, [r4, #0]
 8005226:	230f      	movs	r3, #15
 8005228:	18fb      	adds	r3, r7, r3
 800522a:	781b      	ldrb	r3, [r3, #0]
 800522c:	b25b      	sxtb	r3, r3
 800522e:	2b00      	cmp	r3, #0
 8005230:	d109      	bne.n	8005246 <VL53L0X_PerformSingleMeasurement+0x80>
 8005232:	230e      	movs	r3, #14
 8005234:	18fb      	adds	r3, r7, r3
 8005236:	781b      	ldrb	r3, [r3, #0]
 8005238:	2b00      	cmp	r3, #0
 800523a:	d104      	bne.n	8005246 <VL53L0X_PerformSingleMeasurement+0x80>
 800523c:	687a      	ldr	r2, [r7, #4]
 800523e:	2389      	movs	r3, #137	; 0x89
 8005240:	005b      	lsls	r3, r3, #1
 8005242:	2103      	movs	r1, #3
 8005244:	54d1      	strb	r1, [r2, r3]
 8005246:	230f      	movs	r3, #15
 8005248:	18fb      	adds	r3, r7, r3
 800524a:	781b      	ldrb	r3, [r3, #0]
 800524c:	b25b      	sxtb	r3, r3
 800524e:	0018      	movs	r0, r3
 8005250:	46bd      	mov	sp, r7
 8005252:	b005      	add	sp, #20
 8005254:	bdf0      	pop	{r4, r5, r6, r7, pc}

08005256 <VL53L0X_PerformRefCalibration>:
 8005256:	b5b0      	push	{r4, r5, r7, lr}
 8005258:	b086      	sub	sp, #24
 800525a:	af00      	add	r7, sp, #0
 800525c:	60f8      	str	r0, [r7, #12]
 800525e:	60b9      	str	r1, [r7, #8]
 8005260:	607a      	str	r2, [r7, #4]
 8005262:	2517      	movs	r5, #23
 8005264:	197b      	adds	r3, r7, r5
 8005266:	2200      	movs	r2, #0
 8005268:	701a      	strb	r2, [r3, #0]
 800526a:	197c      	adds	r4, r7, r5
 800526c:	687a      	ldr	r2, [r7, #4]
 800526e:	68b9      	ldr	r1, [r7, #8]
 8005270:	68f8      	ldr	r0, [r7, #12]
 8005272:	2301      	movs	r3, #1
 8005274:	f001 fde3 	bl	8006e3e <VL53L0X_perform_ref_calibration>
 8005278:	0003      	movs	r3, r0
 800527a:	7023      	strb	r3, [r4, #0]
 800527c:	197b      	adds	r3, r7, r5
 800527e:	781b      	ldrb	r3, [r3, #0]
 8005280:	b25b      	sxtb	r3, r3
 8005282:	0018      	movs	r0, r3
 8005284:	46bd      	mov	sp, r7
 8005286:	b006      	add	sp, #24
 8005288:	bdb0      	pop	{r4, r5, r7, pc}
	...

0800528c <VL53L0X_CheckAndLoadInterruptSettings>:
 800528c:	b590      	push	{r4, r7, lr}
 800528e:	b087      	sub	sp, #28
 8005290:	af00      	add	r7, sp, #0
 8005292:	6078      	str	r0, [r7, #4]
 8005294:	000a      	movs	r2, r1
 8005296:	1cfb      	adds	r3, r7, #3
 8005298:	701a      	strb	r2, [r3, #0]
 800529a:	2317      	movs	r3, #23
 800529c:	18fb      	adds	r3, r7, r3
 800529e:	2200      	movs	r2, #0
 80052a0:	701a      	strb	r2, [r3, #0]
 80052a2:	2016      	movs	r0, #22
 80052a4:	183b      	adds	r3, r7, r0
 80052a6:	687a      	ldr	r2, [r7, #4]
 80052a8:	21da      	movs	r1, #218	; 0xda
 80052aa:	5c52      	ldrb	r2, [r2, r1]
 80052ac:	701a      	strb	r2, [r3, #0]
 80052ae:	0002      	movs	r2, r0
 80052b0:	18bb      	adds	r3, r7, r2
 80052b2:	781b      	ldrb	r3, [r3, #0]
 80052b4:	2b01      	cmp	r3, #1
 80052b6:	d007      	beq.n	80052c8 <VL53L0X_CheckAndLoadInterruptSettings+0x3c>
 80052b8:	18bb      	adds	r3, r7, r2
 80052ba:	781b      	ldrb	r3, [r3, #0]
 80052bc:	2b02      	cmp	r3, #2
 80052be:	d003      	beq.n	80052c8 <VL53L0X_CheckAndLoadInterruptSettings+0x3c>
 80052c0:	18bb      	adds	r3, r7, r2
 80052c2:	781b      	ldrb	r3, [r3, #0]
 80052c4:	2b03      	cmp	r3, #3
 80052c6:	d15e      	bne.n	8005386 <VL53L0X_CheckAndLoadInterruptSettings+0xfa>
 80052c8:	2317      	movs	r3, #23
 80052ca:	18fc      	adds	r4, r7, r3
 80052cc:	230c      	movs	r3, #12
 80052ce:	18fb      	adds	r3, r7, r3
 80052d0:	2210      	movs	r2, #16
 80052d2:	18ba      	adds	r2, r7, r2
 80052d4:	6878      	ldr	r0, [r7, #4]
 80052d6:	2101      	movs	r1, #1
 80052d8:	f000 fd26 	bl	8005d28 <VL53L0X_GetInterruptThresholds>
 80052dc:	0003      	movs	r3, r0
 80052de:	7023      	strb	r3, [r4, #0]
 80052e0:	693a      	ldr	r2, [r7, #16]
 80052e2:	23ff      	movs	r3, #255	; 0xff
 80052e4:	041b      	lsls	r3, r3, #16
 80052e6:	429a      	cmp	r2, r3
 80052e8:	d804      	bhi.n	80052f4 <VL53L0X_CheckAndLoadInterruptSettings+0x68>
 80052ea:	68fa      	ldr	r2, [r7, #12]
 80052ec:	23ff      	movs	r3, #255	; 0xff
 80052ee:	041b      	lsls	r3, r3, #16
 80052f0:	429a      	cmp	r2, r3
 80052f2:	d948      	bls.n	8005386 <VL53L0X_CheckAndLoadInterruptSettings+0xfa>
 80052f4:	2217      	movs	r2, #23
 80052f6:	18bb      	adds	r3, r7, r2
 80052f8:	781b      	ldrb	r3, [r3, #0]
 80052fa:	b25b      	sxtb	r3, r3
 80052fc:	2b00      	cmp	r3, #0
 80052fe:	d142      	bne.n	8005386 <VL53L0X_CheckAndLoadInterruptSettings+0xfa>
 8005300:	1cfb      	adds	r3, r7, #3
 8005302:	781b      	ldrb	r3, [r3, #0]
 8005304:	2b00      	cmp	r3, #0
 8005306:	d009      	beq.n	800531c <VL53L0X_CheckAndLoadInterruptSettings+0x90>
 8005308:	18bc      	adds	r4, r7, r2
 800530a:	4a23      	ldr	r2, [pc, #140]	; (8005398 <VL53L0X_CheckAndLoadInterruptSettings+0x10c>)
 800530c:	687b      	ldr	r3, [r7, #4]
 800530e:	0011      	movs	r1, r2
 8005310:	0018      	movs	r0, r3
 8005312:	f003 f93b 	bl	800858c <VL53L0X_load_tuning_settings>
 8005316:	0003      	movs	r3, r0
 8005318:	7023      	strb	r3, [r4, #0]
 800531a:	e034      	b.n	8005386 <VL53L0X_CheckAndLoadInterruptSettings+0xfa>
 800531c:	687b      	ldr	r3, [r7, #4]
 800531e:	2204      	movs	r2, #4
 8005320:	21ff      	movs	r1, #255	; 0xff
 8005322:	0018      	movs	r0, r3
 8005324:	f7fe fae6 	bl	80038f4 <VL53L0X_WrByte>
 8005328:	0003      	movs	r3, r0
 800532a:	0019      	movs	r1, r3
 800532c:	2417      	movs	r4, #23
 800532e:	193b      	adds	r3, r7, r4
 8005330:	193a      	adds	r2, r7, r4
 8005332:	7812      	ldrb	r2, [r2, #0]
 8005334:	430a      	orrs	r2, r1
 8005336:	701a      	strb	r2, [r3, #0]
 8005338:	687b      	ldr	r3, [r7, #4]
 800533a:	2200      	movs	r2, #0
 800533c:	2170      	movs	r1, #112	; 0x70
 800533e:	0018      	movs	r0, r3
 8005340:	f7fe fad8 	bl	80038f4 <VL53L0X_WrByte>
 8005344:	0003      	movs	r3, r0
 8005346:	0019      	movs	r1, r3
 8005348:	193b      	adds	r3, r7, r4
 800534a:	193a      	adds	r2, r7, r4
 800534c:	7812      	ldrb	r2, [r2, #0]
 800534e:	430a      	orrs	r2, r1
 8005350:	701a      	strb	r2, [r3, #0]
 8005352:	687b      	ldr	r3, [r7, #4]
 8005354:	2200      	movs	r2, #0
 8005356:	21ff      	movs	r1, #255	; 0xff
 8005358:	0018      	movs	r0, r3
 800535a:	f7fe facb 	bl	80038f4 <VL53L0X_WrByte>
 800535e:	0003      	movs	r3, r0
 8005360:	0019      	movs	r1, r3
 8005362:	193b      	adds	r3, r7, r4
 8005364:	193a      	adds	r2, r7, r4
 8005366:	7812      	ldrb	r2, [r2, #0]
 8005368:	430a      	orrs	r2, r1
 800536a:	701a      	strb	r2, [r3, #0]
 800536c:	687b      	ldr	r3, [r7, #4]
 800536e:	2200      	movs	r2, #0
 8005370:	2180      	movs	r1, #128	; 0x80
 8005372:	0018      	movs	r0, r3
 8005374:	f7fe fabe 	bl	80038f4 <VL53L0X_WrByte>
 8005378:	0003      	movs	r3, r0
 800537a:	0019      	movs	r1, r3
 800537c:	193b      	adds	r3, r7, r4
 800537e:	193a      	adds	r2, r7, r4
 8005380:	7812      	ldrb	r2, [r2, #0]
 8005382:	430a      	orrs	r2, r1
 8005384:	701a      	strb	r2, [r3, #0]
 8005386:	2317      	movs	r3, #23
 8005388:	18fb      	adds	r3, r7, r3
 800538a:	781b      	ldrb	r3, [r3, #0]
 800538c:	b25b      	sxtb	r3, r3
 800538e:	0018      	movs	r0, r3
 8005390:	46bd      	mov	sp, r7
 8005392:	b007      	add	sp, #28
 8005394:	bd90      	pop	{r4, r7, pc}
 8005396:	46c0      	nop			; (mov r8, r8)
 8005398:	200000f8 	.word	0x200000f8

0800539c <VL53L0X_StartMeasurement>:
 800539c:	b5f0      	push	{r4, r5, r6, r7, lr}
 800539e:	b087      	sub	sp, #28
 80053a0:	af00      	add	r7, sp, #0
 80053a2:	6078      	str	r0, [r7, #4]
 80053a4:	2517      	movs	r5, #23
 80053a6:	197b      	adds	r3, r7, r5
 80053a8:	2200      	movs	r2, #0
 80053aa:	701a      	strb	r2, [r3, #0]
 80053ac:	260f      	movs	r6, #15
 80053ae:	19bb      	adds	r3, r7, r6
 80053b0:	2201      	movs	r2, #1
 80053b2:	701a      	strb	r2, [r3, #0]
 80053b4:	230e      	movs	r3, #14
 80053b6:	18fa      	adds	r2, r7, r3
 80053b8:	687b      	ldr	r3, [r7, #4]
 80053ba:	0011      	movs	r1, r2
 80053bc:	0018      	movs	r0, r3
 80053be:	f7ff f9db 	bl	8004778 <VL53L0X_GetDeviceMode>
 80053c2:	197c      	adds	r4, r7, r5
 80053c4:	687b      	ldr	r3, [r7, #4]
 80053c6:	2201      	movs	r2, #1
 80053c8:	2180      	movs	r1, #128	; 0x80
 80053ca:	0018      	movs	r0, r3
 80053cc:	f7fe fa92 	bl	80038f4 <VL53L0X_WrByte>
 80053d0:	0003      	movs	r3, r0
 80053d2:	7023      	strb	r3, [r4, #0]
 80053d4:	197c      	adds	r4, r7, r5
 80053d6:	687b      	ldr	r3, [r7, #4]
 80053d8:	2201      	movs	r2, #1
 80053da:	21ff      	movs	r1, #255	; 0xff
 80053dc:	0018      	movs	r0, r3
 80053de:	f7fe fa89 	bl	80038f4 <VL53L0X_WrByte>
 80053e2:	0003      	movs	r3, r0
 80053e4:	7023      	strb	r3, [r4, #0]
 80053e6:	197c      	adds	r4, r7, r5
 80053e8:	687b      	ldr	r3, [r7, #4]
 80053ea:	2200      	movs	r2, #0
 80053ec:	2100      	movs	r1, #0
 80053ee:	0018      	movs	r0, r3
 80053f0:	f7fe fa80 	bl	80038f4 <VL53L0X_WrByte>
 80053f4:	0003      	movs	r3, r0
 80053f6:	7023      	strb	r3, [r4, #0]
 80053f8:	687a      	ldr	r2, [r7, #4]
 80053fa:	238d      	movs	r3, #141	; 0x8d
 80053fc:	005b      	lsls	r3, r3, #1
 80053fe:	5cd2      	ldrb	r2, [r2, r3]
 8005400:	197c      	adds	r4, r7, r5
 8005402:	687b      	ldr	r3, [r7, #4]
 8005404:	2191      	movs	r1, #145	; 0x91
 8005406:	0018      	movs	r0, r3
 8005408:	f7fe fa74 	bl	80038f4 <VL53L0X_WrByte>
 800540c:	0003      	movs	r3, r0
 800540e:	7023      	strb	r3, [r4, #0]
 8005410:	197c      	adds	r4, r7, r5
 8005412:	687b      	ldr	r3, [r7, #4]
 8005414:	2201      	movs	r2, #1
 8005416:	2100      	movs	r1, #0
 8005418:	0018      	movs	r0, r3
 800541a:	f7fe fa6b 	bl	80038f4 <VL53L0X_WrByte>
 800541e:	0003      	movs	r3, r0
 8005420:	7023      	strb	r3, [r4, #0]
 8005422:	197c      	adds	r4, r7, r5
 8005424:	687b      	ldr	r3, [r7, #4]
 8005426:	2200      	movs	r2, #0
 8005428:	21ff      	movs	r1, #255	; 0xff
 800542a:	0018      	movs	r0, r3
 800542c:	f7fe fa62 	bl	80038f4 <VL53L0X_WrByte>
 8005430:	0003      	movs	r3, r0
 8005432:	7023      	strb	r3, [r4, #0]
 8005434:	197c      	adds	r4, r7, r5
 8005436:	687b      	ldr	r3, [r7, #4]
 8005438:	2200      	movs	r2, #0
 800543a:	2180      	movs	r1, #128	; 0x80
 800543c:	0018      	movs	r0, r3
 800543e:	f7fe fa59 	bl	80038f4 <VL53L0X_WrByte>
 8005442:	0003      	movs	r3, r0
 8005444:	7023      	strb	r3, [r4, #0]
 8005446:	230e      	movs	r3, #14
 8005448:	18fb      	adds	r3, r7, r3
 800544a:	781b      	ldrb	r3, [r3, #0]
 800544c:	2b01      	cmp	r3, #1
 800544e:	d04a      	beq.n	80054e6 <VL53L0X_StartMeasurement+0x14a>
 8005450:	2b03      	cmp	r3, #3
 8005452:	d06b      	beq.n	800552c <VL53L0X_StartMeasurement+0x190>
 8005454:	2b00      	cmp	r3, #0
 8005456:	d000      	beq.n	800545a <VL53L0X_StartMeasurement+0xbe>
 8005458:	e08b      	b.n	8005572 <VL53L0X_StartMeasurement+0x1d6>
 800545a:	197c      	adds	r4, r7, r5
 800545c:	687b      	ldr	r3, [r7, #4]
 800545e:	2201      	movs	r2, #1
 8005460:	2100      	movs	r1, #0
 8005462:	0018      	movs	r0, r3
 8005464:	f7fe fa46 	bl	80038f4 <VL53L0X_WrByte>
 8005468:	0003      	movs	r3, r0
 800546a:	7023      	strb	r3, [r4, #0]
 800546c:	230d      	movs	r3, #13
 800546e:	18fb      	adds	r3, r7, r3
 8005470:	19ba      	adds	r2, r7, r6
 8005472:	7812      	ldrb	r2, [r2, #0]
 8005474:	701a      	strb	r2, [r3, #0]
 8005476:	197b      	adds	r3, r7, r5
 8005478:	781b      	ldrb	r3, [r3, #0]
 800547a:	b25b      	sxtb	r3, r3
 800547c:	2b00      	cmp	r3, #0
 800547e:	d000      	beq.n	8005482 <VL53L0X_StartMeasurement+0xe6>
 8005480:	e07c      	b.n	800557c <VL53L0X_StartMeasurement+0x1e0>
 8005482:	2300      	movs	r3, #0
 8005484:	613b      	str	r3, [r7, #16]
 8005486:	693b      	ldr	r3, [r7, #16]
 8005488:	2b00      	cmp	r3, #0
 800548a:	d00a      	beq.n	80054a2 <VL53L0X_StartMeasurement+0x106>
 800548c:	2317      	movs	r3, #23
 800548e:	18fc      	adds	r4, r7, r3
 8005490:	230d      	movs	r3, #13
 8005492:	18fa      	adds	r2, r7, r3
 8005494:	687b      	ldr	r3, [r7, #4]
 8005496:	2100      	movs	r1, #0
 8005498:	0018      	movs	r0, r3
 800549a:	f7fe fa99 	bl	80039d0 <VL53L0X_RdByte>
 800549e:	0003      	movs	r3, r0
 80054a0:	7023      	strb	r3, [r4, #0]
 80054a2:	693b      	ldr	r3, [r7, #16]
 80054a4:	3301      	adds	r3, #1
 80054a6:	613b      	str	r3, [r7, #16]
 80054a8:	230d      	movs	r3, #13
 80054aa:	18fb      	adds	r3, r7, r3
 80054ac:	781b      	ldrb	r3, [r3, #0]
 80054ae:	210f      	movs	r1, #15
 80054b0:	187a      	adds	r2, r7, r1
 80054b2:	7812      	ldrb	r2, [r2, #0]
 80054b4:	4013      	ands	r3, r2
 80054b6:	b2db      	uxtb	r3, r3
 80054b8:	187a      	adds	r2, r7, r1
 80054ba:	7812      	ldrb	r2, [r2, #0]
 80054bc:	429a      	cmp	r2, r3
 80054be:	d109      	bne.n	80054d4 <VL53L0X_StartMeasurement+0x138>
 80054c0:	2317      	movs	r3, #23
 80054c2:	18fb      	adds	r3, r7, r3
 80054c4:	781b      	ldrb	r3, [r3, #0]
 80054c6:	b25b      	sxtb	r3, r3
 80054c8:	2b00      	cmp	r3, #0
 80054ca:	d103      	bne.n	80054d4 <VL53L0X_StartMeasurement+0x138>
 80054cc:	693b      	ldr	r3, [r7, #16]
 80054ce:	4a32      	ldr	r2, [pc, #200]	; (8005598 <VL53L0X_StartMeasurement+0x1fc>)
 80054d0:	4293      	cmp	r3, r2
 80054d2:	d9d8      	bls.n	8005486 <VL53L0X_StartMeasurement+0xea>
 80054d4:	693b      	ldr	r3, [r7, #16]
 80054d6:	4a30      	ldr	r2, [pc, #192]	; (8005598 <VL53L0X_StartMeasurement+0x1fc>)
 80054d8:	4293      	cmp	r3, r2
 80054da:	d94f      	bls.n	800557c <VL53L0X_StartMeasurement+0x1e0>
 80054dc:	2317      	movs	r3, #23
 80054de:	18fb      	adds	r3, r7, r3
 80054e0:	22f9      	movs	r2, #249	; 0xf9
 80054e2:	701a      	strb	r2, [r3, #0]
 80054e4:	e04a      	b.n	800557c <VL53L0X_StartMeasurement+0x1e0>
 80054e6:	2217      	movs	r2, #23
 80054e8:	18bb      	adds	r3, r7, r2
 80054ea:	781b      	ldrb	r3, [r3, #0]
 80054ec:	b25b      	sxtb	r3, r3
 80054ee:	2b00      	cmp	r3, #0
 80054f0:	d107      	bne.n	8005502 <VL53L0X_StartMeasurement+0x166>
 80054f2:	18bc      	adds	r4, r7, r2
 80054f4:	687b      	ldr	r3, [r7, #4]
 80054f6:	2101      	movs	r1, #1
 80054f8:	0018      	movs	r0, r3
 80054fa:	f7ff fec7 	bl	800528c <VL53L0X_CheckAndLoadInterruptSettings>
 80054fe:	0003      	movs	r3, r0
 8005500:	7023      	strb	r3, [r4, #0]
 8005502:	2517      	movs	r5, #23
 8005504:	197c      	adds	r4, r7, r5
 8005506:	687b      	ldr	r3, [r7, #4]
 8005508:	2202      	movs	r2, #2
 800550a:	2100      	movs	r1, #0
 800550c:	0018      	movs	r0, r3
 800550e:	f7fe f9f1 	bl	80038f4 <VL53L0X_WrByte>
 8005512:	0003      	movs	r3, r0
 8005514:	7023      	strb	r3, [r4, #0]
 8005516:	197b      	adds	r3, r7, r5
 8005518:	781b      	ldrb	r3, [r3, #0]
 800551a:	b25b      	sxtb	r3, r3
 800551c:	2b00      	cmp	r3, #0
 800551e:	d12f      	bne.n	8005580 <VL53L0X_StartMeasurement+0x1e4>
 8005520:	687a      	ldr	r2, [r7, #4]
 8005522:	2389      	movs	r3, #137	; 0x89
 8005524:	005b      	lsls	r3, r3, #1
 8005526:	2104      	movs	r1, #4
 8005528:	54d1      	strb	r1, [r2, r3]
 800552a:	e029      	b.n	8005580 <VL53L0X_StartMeasurement+0x1e4>
 800552c:	2217      	movs	r2, #23
 800552e:	18bb      	adds	r3, r7, r2
 8005530:	781b      	ldrb	r3, [r3, #0]
 8005532:	b25b      	sxtb	r3, r3
 8005534:	2b00      	cmp	r3, #0
 8005536:	d107      	bne.n	8005548 <VL53L0X_StartMeasurement+0x1ac>
 8005538:	18bc      	adds	r4, r7, r2
 800553a:	687b      	ldr	r3, [r7, #4]
 800553c:	2101      	movs	r1, #1
 800553e:	0018      	movs	r0, r3
 8005540:	f7ff fea4 	bl	800528c <VL53L0X_CheckAndLoadInterruptSettings>
 8005544:	0003      	movs	r3, r0
 8005546:	7023      	strb	r3, [r4, #0]
 8005548:	2517      	movs	r5, #23
 800554a:	197c      	adds	r4, r7, r5
 800554c:	687b      	ldr	r3, [r7, #4]
 800554e:	2204      	movs	r2, #4
 8005550:	2100      	movs	r1, #0
 8005552:	0018      	movs	r0, r3
 8005554:	f7fe f9ce 	bl	80038f4 <VL53L0X_WrByte>
 8005558:	0003      	movs	r3, r0
 800555a:	7023      	strb	r3, [r4, #0]
 800555c:	197b      	adds	r3, r7, r5
 800555e:	781b      	ldrb	r3, [r3, #0]
 8005560:	b25b      	sxtb	r3, r3
 8005562:	2b00      	cmp	r3, #0
 8005564:	d10e      	bne.n	8005584 <VL53L0X_StartMeasurement+0x1e8>
 8005566:	687a      	ldr	r2, [r7, #4]
 8005568:	2389      	movs	r3, #137	; 0x89
 800556a:	005b      	lsls	r3, r3, #1
 800556c:	2104      	movs	r1, #4
 800556e:	54d1      	strb	r1, [r2, r3]
 8005570:	e008      	b.n	8005584 <VL53L0X_StartMeasurement+0x1e8>
 8005572:	2317      	movs	r3, #23
 8005574:	18fb      	adds	r3, r7, r3
 8005576:	22f8      	movs	r2, #248	; 0xf8
 8005578:	701a      	strb	r2, [r3, #0]
 800557a:	e004      	b.n	8005586 <VL53L0X_StartMeasurement+0x1ea>
 800557c:	46c0      	nop			; (mov r8, r8)
 800557e:	e002      	b.n	8005586 <VL53L0X_StartMeasurement+0x1ea>
 8005580:	46c0      	nop			; (mov r8, r8)
 8005582:	e000      	b.n	8005586 <VL53L0X_StartMeasurement+0x1ea>
 8005584:	46c0      	nop			; (mov r8, r8)
 8005586:	2317      	movs	r3, #23
 8005588:	18fb      	adds	r3, r7, r3
 800558a:	781b      	ldrb	r3, [r3, #0]
 800558c:	b25b      	sxtb	r3, r3
 800558e:	0018      	movs	r0, r3
 8005590:	46bd      	mov	sp, r7
 8005592:	b007      	add	sp, #28
 8005594:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8005596:	46c0      	nop			; (mov r8, r8)
 8005598:	000007cf 	.word	0x000007cf

0800559c <VL53L0X_StopMeasurement>:
 800559c:	b5b0      	push	{r4, r5, r7, lr}
 800559e:	b084      	sub	sp, #16
 80055a0:	af00      	add	r7, sp, #0
 80055a2:	6078      	str	r0, [r7, #4]
 80055a4:	250f      	movs	r5, #15
 80055a6:	197b      	adds	r3, r7, r5
 80055a8:	2200      	movs	r2, #0
 80055aa:	701a      	strb	r2, [r3, #0]
 80055ac:	197c      	adds	r4, r7, r5
 80055ae:	687b      	ldr	r3, [r7, #4]
 80055b0:	2200      	movs	r2, #0
 80055b2:	2100      	movs	r1, #0
 80055b4:	0018      	movs	r0, r3
 80055b6:	f7fe f99d 	bl	80038f4 <VL53L0X_WrByte>
 80055ba:	0003      	movs	r3, r0
 80055bc:	7023      	strb	r3, [r4, #0]
 80055be:	197c      	adds	r4, r7, r5
 80055c0:	687b      	ldr	r3, [r7, #4]
 80055c2:	2201      	movs	r2, #1
 80055c4:	21ff      	movs	r1, #255	; 0xff
 80055c6:	0018      	movs	r0, r3
 80055c8:	f7fe f994 	bl	80038f4 <VL53L0X_WrByte>
 80055cc:	0003      	movs	r3, r0
 80055ce:	7023      	strb	r3, [r4, #0]
 80055d0:	197c      	adds	r4, r7, r5
 80055d2:	687b      	ldr	r3, [r7, #4]
 80055d4:	2200      	movs	r2, #0
 80055d6:	2100      	movs	r1, #0
 80055d8:	0018      	movs	r0, r3
 80055da:	f7fe f98b 	bl	80038f4 <VL53L0X_WrByte>
 80055de:	0003      	movs	r3, r0
 80055e0:	7023      	strb	r3, [r4, #0]
 80055e2:	197c      	adds	r4, r7, r5
 80055e4:	687b      	ldr	r3, [r7, #4]
 80055e6:	2200      	movs	r2, #0
 80055e8:	2191      	movs	r1, #145	; 0x91
 80055ea:	0018      	movs	r0, r3
 80055ec:	f7fe f982 	bl	80038f4 <VL53L0X_WrByte>
 80055f0:	0003      	movs	r3, r0
 80055f2:	7023      	strb	r3, [r4, #0]
 80055f4:	197c      	adds	r4, r7, r5
 80055f6:	687b      	ldr	r3, [r7, #4]
 80055f8:	2201      	movs	r2, #1
 80055fa:	2100      	movs	r1, #0
 80055fc:	0018      	movs	r0, r3
 80055fe:	f7fe f979 	bl	80038f4 <VL53L0X_WrByte>
 8005602:	0003      	movs	r3, r0
 8005604:	7023      	strb	r3, [r4, #0]
 8005606:	197c      	adds	r4, r7, r5
 8005608:	687b      	ldr	r3, [r7, #4]
 800560a:	2200      	movs	r2, #0
 800560c:	21ff      	movs	r1, #255	; 0xff
 800560e:	0018      	movs	r0, r3
 8005610:	f7fe f970 	bl	80038f4 <VL53L0X_WrByte>
 8005614:	0003      	movs	r3, r0
 8005616:	7023      	strb	r3, [r4, #0]
 8005618:	197b      	adds	r3, r7, r5
 800561a:	781b      	ldrb	r3, [r3, #0]
 800561c:	b25b      	sxtb	r3, r3
 800561e:	2b00      	cmp	r3, #0
 8005620:	d104      	bne.n	800562c <VL53L0X_StopMeasurement+0x90>
 8005622:	687a      	ldr	r2, [r7, #4]
 8005624:	2389      	movs	r3, #137	; 0x89
 8005626:	005b      	lsls	r3, r3, #1
 8005628:	2103      	movs	r1, #3
 800562a:	54d1      	strb	r1, [r2, r3]
 800562c:	220f      	movs	r2, #15
 800562e:	18bb      	adds	r3, r7, r2
 8005630:	781b      	ldrb	r3, [r3, #0]
 8005632:	b25b      	sxtb	r3, r3
 8005634:	2b00      	cmp	r3, #0
 8005636:	d107      	bne.n	8005648 <VL53L0X_StopMeasurement+0xac>
 8005638:	18bc      	adds	r4, r7, r2
 800563a:	687b      	ldr	r3, [r7, #4]
 800563c:	2100      	movs	r1, #0
 800563e:	0018      	movs	r0, r3
 8005640:	f7ff fe24 	bl	800528c <VL53L0X_CheckAndLoadInterruptSettings>
 8005644:	0003      	movs	r3, r0
 8005646:	7023      	strb	r3, [r4, #0]
 8005648:	230f      	movs	r3, #15
 800564a:	18fb      	adds	r3, r7, r3
 800564c:	781b      	ldrb	r3, [r3, #0]
 800564e:	b25b      	sxtb	r3, r3
 8005650:	0018      	movs	r0, r3
 8005652:	46bd      	mov	sp, r7
 8005654:	b004      	add	sp, #16
 8005656:	bdb0      	pop	{r4, r5, r7, pc}

08005658 <VL53L0X_GetMeasurementDataReady>:
 8005658:	b5f0      	push	{r4, r5, r6, r7, lr}
 800565a:	b085      	sub	sp, #20
 800565c:	af00      	add	r7, sp, #0
 800565e:	6078      	str	r0, [r7, #4]
 8005660:	6039      	str	r1, [r7, #0]
 8005662:	200f      	movs	r0, #15
 8005664:	183b      	adds	r3, r7, r0
 8005666:	2200      	movs	r2, #0
 8005668:	701a      	strb	r2, [r3, #0]
 800566a:	240e      	movs	r4, #14
 800566c:	193b      	adds	r3, r7, r4
 800566e:	687a      	ldr	r2, [r7, #4]
 8005670:	21da      	movs	r1, #218	; 0xda
 8005672:	5c52      	ldrb	r2, [r2, r1]
 8005674:	701a      	strb	r2, [r3, #0]
 8005676:	193b      	adds	r3, r7, r4
 8005678:	781b      	ldrb	r3, [r3, #0]
 800567a:	2b04      	cmp	r3, #4
 800567c:	d114      	bne.n	80056a8 <VL53L0X_GetMeasurementDataReady+0x50>
 800567e:	183c      	adds	r4, r7, r0
 8005680:	2308      	movs	r3, #8
 8005682:	18fa      	adds	r2, r7, r3
 8005684:	687b      	ldr	r3, [r7, #4]
 8005686:	0011      	movs	r1, r2
 8005688:	0018      	movs	r0, r3
 800568a:	f000 fbe9 	bl	8005e60 <VL53L0X_GetInterruptMaskStatus>
 800568e:	0003      	movs	r3, r0
 8005690:	7023      	strb	r3, [r4, #0]
 8005692:	68bb      	ldr	r3, [r7, #8]
 8005694:	2b04      	cmp	r3, #4
 8005696:	d103      	bne.n	80056a0 <VL53L0X_GetMeasurementDataReady+0x48>
 8005698:	683b      	ldr	r3, [r7, #0]
 800569a:	2201      	movs	r2, #1
 800569c:	701a      	strb	r2, [r3, #0]
 800569e:	e020      	b.n	80056e2 <VL53L0X_GetMeasurementDataReady+0x8a>
 80056a0:	683b      	ldr	r3, [r7, #0]
 80056a2:	2200      	movs	r2, #0
 80056a4:	701a      	strb	r2, [r3, #0]
 80056a6:	e01c      	b.n	80056e2 <VL53L0X_GetMeasurementDataReady+0x8a>
 80056a8:	250f      	movs	r5, #15
 80056aa:	197c      	adds	r4, r7, r5
 80056ac:	260d      	movs	r6, #13
 80056ae:	19ba      	adds	r2, r7, r6
 80056b0:	687b      	ldr	r3, [r7, #4]
 80056b2:	2114      	movs	r1, #20
 80056b4:	0018      	movs	r0, r3
 80056b6:	f7fe f98b 	bl	80039d0 <VL53L0X_RdByte>
 80056ba:	0003      	movs	r3, r0
 80056bc:	7023      	strb	r3, [r4, #0]
 80056be:	197b      	adds	r3, r7, r5
 80056c0:	781b      	ldrb	r3, [r3, #0]
 80056c2:	b25b      	sxtb	r3, r3
 80056c4:	2b00      	cmp	r3, #0
 80056c6:	d10c      	bne.n	80056e2 <VL53L0X_GetMeasurementDataReady+0x8a>
 80056c8:	19bb      	adds	r3, r7, r6
 80056ca:	781b      	ldrb	r3, [r3, #0]
 80056cc:	001a      	movs	r2, r3
 80056ce:	2301      	movs	r3, #1
 80056d0:	4013      	ands	r3, r2
 80056d2:	d003      	beq.n	80056dc <VL53L0X_GetMeasurementDataReady+0x84>
 80056d4:	683b      	ldr	r3, [r7, #0]
 80056d6:	2201      	movs	r2, #1
 80056d8:	701a      	strb	r2, [r3, #0]
 80056da:	e002      	b.n	80056e2 <VL53L0X_GetMeasurementDataReady+0x8a>
 80056dc:	683b      	ldr	r3, [r7, #0]
 80056de:	2200      	movs	r2, #0
 80056e0:	701a      	strb	r2, [r3, #0]
 80056e2:	230f      	movs	r3, #15
 80056e4:	18fb      	adds	r3, r7, r3
 80056e6:	781b      	ldrb	r3, [r3, #0]
 80056e8:	b25b      	sxtb	r3, r3
 80056ea:	0018      	movs	r0, r3
 80056ec:	46bd      	mov	sp, r7
 80056ee:	b005      	add	sp, #20
 80056f0:	bdf0      	pop	{r4, r5, r6, r7, pc}
	...

080056f4 <VL53L0X_GetRangingMeasurementData>:
 80056f4:	b5f0      	push	{r4, r5, r6, r7, lr}
 80056f6:	b097      	sub	sp, #92	; 0x5c
 80056f8:	af02      	add	r7, sp, #8
 80056fa:	6078      	str	r0, [r7, #4]
 80056fc:	6039      	str	r1, [r7, #0]
 80056fe:	264f      	movs	r6, #79	; 0x4f
 8005700:	19bb      	adds	r3, r7, r6
 8005702:	2200      	movs	r2, #0
 8005704:	701a      	strb	r2, [r3, #0]
 8005706:	19bc      	adds	r4, r7, r6
 8005708:	2528      	movs	r5, #40	; 0x28
 800570a:	197a      	adds	r2, r7, r5
 800570c:	6878      	ldr	r0, [r7, #4]
 800570e:	230c      	movs	r3, #12
 8005710:	2114      	movs	r1, #20
 8005712:	f7fe f87d 	bl	8003810 <VL53L0X_ReadMulti>
 8005716:	0003      	movs	r3, r0
 8005718:	7023      	strb	r3, [r4, #0]
 800571a:	19bb      	adds	r3, r7, r6
 800571c:	781b      	ldrb	r3, [r3, #0]
 800571e:	b25b      	sxtb	r3, r3
 8005720:	2b00      	cmp	r3, #0
 8005722:	d000      	beq.n	8005726 <VL53L0X_GetRangingMeasurementData+0x32>
 8005724:	e0f1      	b.n	800590a <VL53L0X_GetRangingMeasurementData+0x216>
 8005726:	683b      	ldr	r3, [r7, #0]
 8005728:	2200      	movs	r2, #0
 800572a:	759a      	strb	r2, [r3, #22]
 800572c:	683b      	ldr	r3, [r7, #0]
 800572e:	2200      	movs	r2, #0
 8005730:	601a      	str	r2, [r3, #0]
 8005732:	0028      	movs	r0, r5
 8005734:	183b      	adds	r3, r7, r0
 8005736:	7a9b      	ldrb	r3, [r3, #10]
 8005738:	b29b      	uxth	r3, r3
 800573a:	021b      	lsls	r3, r3, #8
 800573c:	b299      	uxth	r1, r3
 800573e:	183b      	adds	r3, r7, r0
 8005740:	7adb      	ldrb	r3, [r3, #11]
 8005742:	b29a      	uxth	r2, r3
 8005744:	244c      	movs	r4, #76	; 0x4c
 8005746:	193b      	adds	r3, r7, r4
 8005748:	188a      	adds	r2, r1, r2
 800574a:	801a      	strh	r2, [r3, #0]
 800574c:	683b      	ldr	r3, [r7, #0]
 800574e:	2200      	movs	r2, #0
 8005750:	605a      	str	r2, [r3, #4]
 8005752:	183b      	adds	r3, r7, r0
 8005754:	799b      	ldrb	r3, [r3, #6]
 8005756:	b29b      	uxth	r3, r3
 8005758:	021b      	lsls	r3, r3, #8
 800575a:	b29a      	uxth	r2, r3
 800575c:	183b      	adds	r3, r7, r0
 800575e:	79db      	ldrb	r3, [r3, #7]
 8005760:	b29b      	uxth	r3, r3
 8005762:	18d3      	adds	r3, r2, r3
 8005764:	b29b      	uxth	r3, r3
 8005766:	025b      	lsls	r3, r3, #9
 8005768:	647b      	str	r3, [r7, #68]	; 0x44
 800576a:	683b      	ldr	r3, [r7, #0]
 800576c:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 800576e:	60da      	str	r2, [r3, #12]
 8005770:	183b      	adds	r3, r7, r0
 8005772:	7a1b      	ldrb	r3, [r3, #8]
 8005774:	b29b      	uxth	r3, r3
 8005776:	021b      	lsls	r3, r3, #8
 8005778:	b299      	uxth	r1, r3
 800577a:	183b      	adds	r3, r7, r0
 800577c:	7a5b      	ldrb	r3, [r3, #9]
 800577e:	b29a      	uxth	r2, r3
 8005780:	2542      	movs	r5, #66	; 0x42
 8005782:	197b      	adds	r3, r7, r5
 8005784:	188a      	adds	r2, r1, r2
 8005786:	801a      	strh	r2, [r3, #0]
 8005788:	197b      	adds	r3, r7, r5
 800578a:	881b      	ldrh	r3, [r3, #0]
 800578c:	025b      	lsls	r3, r3, #9
 800578e:	001a      	movs	r2, r3
 8005790:	683b      	ldr	r3, [r7, #0]
 8005792:	611a      	str	r2, [r3, #16]
 8005794:	183b      	adds	r3, r7, r0
 8005796:	789b      	ldrb	r3, [r3, #2]
 8005798:	b29b      	uxth	r3, r3
 800579a:	021b      	lsls	r3, r3, #8
 800579c:	b299      	uxth	r1, r3
 800579e:	183b      	adds	r3, r7, r0
 80057a0:	78db      	ldrb	r3, [r3, #3]
 80057a2:	b29a      	uxth	r2, r3
 80057a4:	2540      	movs	r5, #64	; 0x40
 80057a6:	197b      	adds	r3, r7, r5
 80057a8:	188a      	adds	r2, r1, r2
 80057aa:	801a      	strh	r2, [r3, #0]
 80057ac:	683b      	ldr	r3, [r7, #0]
 80057ae:	197a      	adds	r2, r7, r5
 80057b0:	8812      	ldrh	r2, [r2, #0]
 80057b2:	829a      	strh	r2, [r3, #20]
 80057b4:	233f      	movs	r3, #63	; 0x3f
 80057b6:	18fb      	adds	r3, r7, r3
 80057b8:	183a      	adds	r2, r7, r0
 80057ba:	7812      	ldrb	r2, [r2, #0]
 80057bc:	701a      	strb	r2, [r3, #0]
 80057be:	203c      	movs	r0, #60	; 0x3c
 80057c0:	183b      	adds	r3, r7, r0
 80057c2:	6879      	ldr	r1, [r7, #4]
 80057c4:	2299      	movs	r2, #153	; 0x99
 80057c6:	0052      	lsls	r2, r2, #1
 80057c8:	5a8a      	ldrh	r2, [r1, r2]
 80057ca:	801a      	strh	r2, [r3, #0]
 80057cc:	263b      	movs	r6, #59	; 0x3b
 80057ce:	19bb      	adds	r3, r7, r6
 80057d0:	6879      	ldr	r1, [r7, #4]
 80057d2:	2212      	movs	r2, #18
 80057d4:	32ff      	adds	r2, #255	; 0xff
 80057d6:	5c8a      	ldrb	r2, [r1, r2]
 80057d8:	701a      	strb	r2, [r3, #0]
 80057da:	183b      	adds	r3, r7, r0
 80057dc:	881a      	ldrh	r2, [r3, #0]
 80057de:	23fa      	movs	r3, #250	; 0xfa
 80057e0:	009b      	lsls	r3, r3, #2
 80057e2:	429a      	cmp	r2, r3
 80057e4:	d054      	beq.n	8005890 <VL53L0X_GetRangingMeasurementData+0x19c>
 80057e6:	183b      	adds	r3, r7, r0
 80057e8:	881b      	ldrh	r3, [r3, #0]
 80057ea:	193a      	adds	r2, r7, r4
 80057ec:	8812      	ldrh	r2, [r2, #0]
 80057ee:	4353      	muls	r3, r2
 80057f0:	33f5      	adds	r3, #245	; 0xf5
 80057f2:	33ff      	adds	r3, #255	; 0xff
 80057f4:	001a      	movs	r2, r3
 80057f6:	23fa      	movs	r3, #250	; 0xfa
 80057f8:	0099      	lsls	r1, r3, #2
 80057fa:	0010      	movs	r0, r2
 80057fc:	f7fa fd0e 	bl	800021c <__divsi3>
 8005800:	0003      	movs	r3, r0
 8005802:	001a      	movs	r2, r3
 8005804:	193b      	adds	r3, r7, r4
 8005806:	801a      	strh	r2, [r3, #0]
 8005808:	687b      	ldr	r3, [r7, #4]
 800580a:	6a1a      	ldr	r2, [r3, #32]
 800580c:	2138      	movs	r1, #56	; 0x38
 800580e:	187b      	adds	r3, r7, r1
 8005810:	801a      	strh	r2, [r3, #0]
 8005812:	2037      	movs	r0, #55	; 0x37
 8005814:	183b      	adds	r3, r7, r0
 8005816:	687a      	ldr	r2, [r7, #4]
 8005818:	7f12      	ldrb	r2, [r2, #28]
 800581a:	701a      	strb	r2, [r3, #0]
 800581c:	183b      	adds	r3, r7, r0
 800581e:	781b      	ldrb	r3, [r3, #0]
 8005820:	2b00      	cmp	r3, #0
 8005822:	d035      	beq.n	8005890 <VL53L0X_GetRangingMeasurementData+0x19c>
 8005824:	187b      	adds	r3, r7, r1
 8005826:	881b      	ldrh	r3, [r3, #0]
 8005828:	197a      	adds	r2, r7, r5
 800582a:	8812      	ldrh	r2, [r2, #0]
 800582c:	4353      	muls	r3, r2
 800582e:	121a      	asrs	r2, r3, #8
 8005830:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 8005832:	429a      	cmp	r2, r3
 8005834:	d10d      	bne.n	8005852 <VL53L0X_GetRangingMeasurementData+0x15e>
 8005836:	19bb      	adds	r3, r7, r6
 8005838:	781b      	ldrb	r3, [r3, #0]
 800583a:	2b00      	cmp	r3, #0
 800583c:	d004      	beq.n	8005848 <VL53L0X_GetRangingMeasurementData+0x154>
 800583e:	234a      	movs	r3, #74	; 0x4a
 8005840:	18fb      	adds	r3, r7, r3
 8005842:	4a52      	ldr	r2, [pc, #328]	; (800598c <VL53L0X_GetRangingMeasurementData+0x298>)
 8005844:	801a      	strh	r2, [r3, #0]
 8005846:	e01d      	b.n	8005884 <VL53L0X_GetRangingMeasurementData+0x190>
 8005848:	234a      	movs	r3, #74	; 0x4a
 800584a:	18fb      	adds	r3, r7, r3
 800584c:	4a50      	ldr	r2, [pc, #320]	; (8005990 <VL53L0X_GetRangingMeasurementData+0x29c>)
 800584e:	801a      	strh	r2, [r3, #0]
 8005850:	e018      	b.n	8005884 <VL53L0X_GetRangingMeasurementData+0x190>
 8005852:	234c      	movs	r3, #76	; 0x4c
 8005854:	18fb      	adds	r3, r7, r3
 8005856:	881b      	ldrh	r3, [r3, #0]
 8005858:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 800585a:	435a      	muls	r2, r3
 800585c:	0010      	movs	r0, r2
 800585e:	2338      	movs	r3, #56	; 0x38
 8005860:	18fb      	adds	r3, r7, r3
 8005862:	881b      	ldrh	r3, [r3, #0]
 8005864:	2240      	movs	r2, #64	; 0x40
 8005866:	18ba      	adds	r2, r7, r2
 8005868:	8812      	ldrh	r2, [r2, #0]
 800586a:	4353      	muls	r3, r2
 800586c:	121b      	asrs	r3, r3, #8
 800586e:	001a      	movs	r2, r3
 8005870:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 8005872:	1a9b      	subs	r3, r3, r2
 8005874:	0019      	movs	r1, r3
 8005876:	f7fa fc47 	bl	8000108 <__udivsi3>
 800587a:	0003      	movs	r3, r0
 800587c:	001a      	movs	r2, r3
 800587e:	234a      	movs	r3, #74	; 0x4a
 8005880:	18fb      	adds	r3, r7, r3
 8005882:	801a      	strh	r2, [r3, #0]
 8005884:	234c      	movs	r3, #76	; 0x4c
 8005886:	18fb      	adds	r3, r7, r3
 8005888:	224a      	movs	r2, #74	; 0x4a
 800588a:	18ba      	adds	r2, r7, r2
 800588c:	8812      	ldrh	r2, [r2, #0]
 800588e:	801a      	strh	r2, [r3, #0]
 8005890:	233b      	movs	r3, #59	; 0x3b
 8005892:	18fb      	adds	r3, r7, r3
 8005894:	781b      	ldrb	r3, [r3, #0]
 8005896:	2b00      	cmp	r3, #0
 8005898:	d00e      	beq.n	80058b8 <VL53L0X_GetRangingMeasurementData+0x1c4>
 800589a:	214c      	movs	r1, #76	; 0x4c
 800589c:	187b      	adds	r3, r7, r1
 800589e:	881b      	ldrh	r3, [r3, #0]
 80058a0:	089b      	lsrs	r3, r3, #2
 80058a2:	b29a      	uxth	r2, r3
 80058a4:	683b      	ldr	r3, [r7, #0]
 80058a6:	811a      	strh	r2, [r3, #8]
 80058a8:	187b      	adds	r3, r7, r1
 80058aa:	881b      	ldrh	r3, [r3, #0]
 80058ac:	b2db      	uxtb	r3, r3
 80058ae:	019b      	lsls	r3, r3, #6
 80058b0:	b2da      	uxtb	r2, r3
 80058b2:	683b      	ldr	r3, [r7, #0]
 80058b4:	75da      	strb	r2, [r3, #23]
 80058b6:	e007      	b.n	80058c8 <VL53L0X_GetRangingMeasurementData+0x1d4>
 80058b8:	683b      	ldr	r3, [r7, #0]
 80058ba:	224c      	movs	r2, #76	; 0x4c
 80058bc:	18ba      	adds	r2, r7, r2
 80058be:	8812      	ldrh	r2, [r2, #0]
 80058c0:	811a      	strh	r2, [r3, #8]
 80058c2:	683b      	ldr	r3, [r7, #0]
 80058c4:	2200      	movs	r2, #0
 80058c6:	75da      	strb	r2, [r3, #23]
 80058c8:	2340      	movs	r3, #64	; 0x40
 80058ca:	18fb      	adds	r3, r7, r3
 80058cc:	881c      	ldrh	r4, [r3, #0]
 80058ce:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 80058d0:	233f      	movs	r3, #63	; 0x3f
 80058d2:	18fb      	adds	r3, r7, r3
 80058d4:	7819      	ldrb	r1, [r3, #0]
 80058d6:	6878      	ldr	r0, [r7, #4]
 80058d8:	2536      	movs	r5, #54	; 0x36
 80058da:	197b      	adds	r3, r7, r5
 80058dc:	9301      	str	r3, [sp, #4]
 80058de:	683b      	ldr	r3, [r7, #0]
 80058e0:	9300      	str	r3, [sp, #0]
 80058e2:	0023      	movs	r3, r4
 80058e4:	f003 fbc8 	bl	8009078 <VL53L0X_get_pal_range_status>
 80058e8:	0003      	movs	r3, r0
 80058ea:	0019      	movs	r1, r3
 80058ec:	204f      	movs	r0, #79	; 0x4f
 80058ee:	183b      	adds	r3, r7, r0
 80058f0:	183a      	adds	r2, r7, r0
 80058f2:	7812      	ldrb	r2, [r2, #0]
 80058f4:	430a      	orrs	r2, r1
 80058f6:	701a      	strb	r2, [r3, #0]
 80058f8:	183b      	adds	r3, r7, r0
 80058fa:	781b      	ldrb	r3, [r3, #0]
 80058fc:	b25b      	sxtb	r3, r3
 80058fe:	2b00      	cmp	r3, #0
 8005900:	d103      	bne.n	800590a <VL53L0X_GetRangingMeasurementData+0x216>
 8005902:	197b      	adds	r3, r7, r5
 8005904:	781a      	ldrb	r2, [r3, #0]
 8005906:	683b      	ldr	r3, [r7, #0]
 8005908:	761a      	strb	r2, [r3, #24]
 800590a:	234f      	movs	r3, #79	; 0x4f
 800590c:	18fb      	adds	r3, r7, r3
 800590e:	781b      	ldrb	r3, [r3, #0]
 8005910:	b25b      	sxtb	r3, r3
 8005912:	2b00      	cmp	r3, #0
 8005914:	d132      	bne.n	800597c <VL53L0X_GetRangingMeasurementData+0x288>
 8005916:	210c      	movs	r1, #12
 8005918:	187a      	adds	r2, r7, r1
 800591a:	687b      	ldr	r3, [r7, #4]
 800591c:	3350      	adds	r3, #80	; 0x50
 800591e:	cb31      	ldmia	r3!, {r0, r4, r5}
 8005920:	c231      	stmia	r2!, {r0, r4, r5}
 8005922:	cb31      	ldmia	r3!, {r0, r4, r5}
 8005924:	c231      	stmia	r2!, {r0, r4, r5}
 8005926:	681b      	ldr	r3, [r3, #0]
 8005928:	6013      	str	r3, [r2, #0]
 800592a:	683b      	ldr	r3, [r7, #0]
 800592c:	891a      	ldrh	r2, [r3, #8]
 800592e:	187b      	adds	r3, r7, r1
 8005930:	811a      	strh	r2, [r3, #8]
 8005932:	683b      	ldr	r3, [r7, #0]
 8005934:	7dda      	ldrb	r2, [r3, #23]
 8005936:	187b      	adds	r3, r7, r1
 8005938:	75da      	strb	r2, [r3, #23]
 800593a:	683b      	ldr	r3, [r7, #0]
 800593c:	895a      	ldrh	r2, [r3, #10]
 800593e:	187b      	adds	r3, r7, r1
 8005940:	815a      	strh	r2, [r3, #10]
 8005942:	683b      	ldr	r3, [r7, #0]
 8005944:	685a      	ldr	r2, [r3, #4]
 8005946:	187b      	adds	r3, r7, r1
 8005948:	605a      	str	r2, [r3, #4]
 800594a:	683b      	ldr	r3, [r7, #0]
 800594c:	68da      	ldr	r2, [r3, #12]
 800594e:	187b      	adds	r3, r7, r1
 8005950:	60da      	str	r2, [r3, #12]
 8005952:	683b      	ldr	r3, [r7, #0]
 8005954:	691a      	ldr	r2, [r3, #16]
 8005956:	187b      	adds	r3, r7, r1
 8005958:	611a      	str	r2, [r3, #16]
 800595a:	683b      	ldr	r3, [r7, #0]
 800595c:	8a9a      	ldrh	r2, [r3, #20]
 800595e:	187b      	adds	r3, r7, r1
 8005960:	829a      	strh	r2, [r3, #20]
 8005962:	683b      	ldr	r3, [r7, #0]
 8005964:	7e1a      	ldrb	r2, [r3, #24]
 8005966:	187b      	adds	r3, r7, r1
 8005968:	761a      	strb	r2, [r3, #24]
 800596a:	687b      	ldr	r3, [r7, #4]
 800596c:	187a      	adds	r2, r7, r1
 800596e:	3350      	adds	r3, #80	; 0x50
 8005970:	ca13      	ldmia	r2!, {r0, r1, r4}
 8005972:	c313      	stmia	r3!, {r0, r1, r4}
 8005974:	ca13      	ldmia	r2!, {r0, r1, r4}
 8005976:	c313      	stmia	r3!, {r0, r1, r4}
 8005978:	6812      	ldr	r2, [r2, #0]
 800597a:	601a      	str	r2, [r3, #0]
 800597c:	234f      	movs	r3, #79	; 0x4f
 800597e:	18fb      	adds	r3, r7, r3
 8005980:	781b      	ldrb	r3, [r3, #0]
 8005982:	b25b      	sxtb	r3, r3
 8005984:	0018      	movs	r0, r3
 8005986:	46bd      	mov	sp, r7
 8005988:	b015      	add	sp, #84	; 0x54
 800598a:	bdf0      	pop	{r4, r5, r6, r7, pc}
 800598c:	000022b8 	.word	0x000022b8
 8005990:	ffff8ae0 	.word	0xffff8ae0

08005994 <VL53L0X_PerformSingleRangingMeasurement>:
 8005994:	b5b0      	push	{r4, r5, r7, lr}
 8005996:	b084      	sub	sp, #16
 8005998:	af00      	add	r7, sp, #0
 800599a:	6078      	str	r0, [r7, #4]
 800599c:	6039      	str	r1, [r7, #0]
 800599e:	250f      	movs	r5, #15
 80059a0:	197b      	adds	r3, r7, r5
 80059a2:	2200      	movs	r2, #0
 80059a4:	701a      	strb	r2, [r3, #0]
 80059a6:	197c      	adds	r4, r7, r5
 80059a8:	687b      	ldr	r3, [r7, #4]
 80059aa:	2100      	movs	r1, #0
 80059ac:	0018      	movs	r0, r3
 80059ae:	f7fe febb 	bl	8004728 <VL53L0X_SetDeviceMode>
 80059b2:	0003      	movs	r3, r0
 80059b4:	7023      	strb	r3, [r4, #0]
 80059b6:	197b      	adds	r3, r7, r5
 80059b8:	781b      	ldrb	r3, [r3, #0]
 80059ba:	b25b      	sxtb	r3, r3
 80059bc:	2b00      	cmp	r3, #0
 80059be:	d106      	bne.n	80059ce <VL53L0X_PerformSingleRangingMeasurement+0x3a>
 80059c0:	197c      	adds	r4, r7, r5
 80059c2:	687b      	ldr	r3, [r7, #4]
 80059c4:	0018      	movs	r0, r3
 80059c6:	f7ff fbfe 	bl	80051c6 <VL53L0X_PerformSingleMeasurement>
 80059ca:	0003      	movs	r3, r0
 80059cc:	7023      	strb	r3, [r4, #0]
 80059ce:	220f      	movs	r2, #15
 80059d0:	18bb      	adds	r3, r7, r2
 80059d2:	781b      	ldrb	r3, [r3, #0]
 80059d4:	b25b      	sxtb	r3, r3
 80059d6:	2b00      	cmp	r3, #0
 80059d8:	d108      	bne.n	80059ec <VL53L0X_PerformSingleRangingMeasurement+0x58>
 80059da:	18bc      	adds	r4, r7, r2
 80059dc:	683a      	ldr	r2, [r7, #0]
 80059de:	687b      	ldr	r3, [r7, #4]
 80059e0:	0011      	movs	r1, r2
 80059e2:	0018      	movs	r0, r3
 80059e4:	f7ff fe86 	bl	80056f4 <VL53L0X_GetRangingMeasurementData>
 80059e8:	0003      	movs	r3, r0
 80059ea:	7023      	strb	r3, [r4, #0]
 80059ec:	220f      	movs	r2, #15
 80059ee:	18bb      	adds	r3, r7, r2
 80059f0:	781b      	ldrb	r3, [r3, #0]
 80059f2:	b25b      	sxtb	r3, r3
 80059f4:	2b00      	cmp	r3, #0
 80059f6:	d107      	bne.n	8005a08 <VL53L0X_PerformSingleRangingMeasurement+0x74>
 80059f8:	18bc      	adds	r4, r7, r2
 80059fa:	687b      	ldr	r3, [r7, #4]
 80059fc:	2100      	movs	r1, #0
 80059fe:	0018      	movs	r0, r3
 8005a00:	f000 f9d4 	bl	8005dac <VL53L0X_ClearInterruptMask>
 8005a04:	0003      	movs	r3, r0
 8005a06:	7023      	strb	r3, [r4, #0]
 8005a08:	230f      	movs	r3, #15
 8005a0a:	18fb      	adds	r3, r7, r3
 8005a0c:	781b      	ldrb	r3, [r3, #0]
 8005a0e:	b25b      	sxtb	r3, r3
 8005a10:	0018      	movs	r0, r3
 8005a12:	46bd      	mov	sp, r7
 8005a14:	b004      	add	sp, #16
 8005a16:	bdb0      	pop	{r4, r5, r7, pc}

08005a18 <VL53L0X_SetGpioConfig>:
 8005a18:	b590      	push	{r4, r7, lr}
 8005a1a:	b085      	sub	sp, #20
 8005a1c:	af00      	add	r7, sp, #0
 8005a1e:	6078      	str	r0, [r7, #4]
 8005a20:	000c      	movs	r4, r1
 8005a22:	0010      	movs	r0, r2
 8005a24:	0019      	movs	r1, r3
 8005a26:	1cfb      	adds	r3, r7, #3
 8005a28:	1c22      	adds	r2, r4, #0
 8005a2a:	701a      	strb	r2, [r3, #0]
 8005a2c:	1cbb      	adds	r3, r7, #2
 8005a2e:	1c02      	adds	r2, r0, #0
 8005a30:	701a      	strb	r2, [r3, #0]
 8005a32:	1c7b      	adds	r3, r7, #1
 8005a34:	1c0a      	adds	r2, r1, #0
 8005a36:	701a      	strb	r2, [r3, #0]
 8005a38:	210f      	movs	r1, #15
 8005a3a:	187b      	adds	r3, r7, r1
 8005a3c:	2200      	movs	r2, #0
 8005a3e:	701a      	strb	r2, [r3, #0]
 8005a40:	1cfb      	adds	r3, r7, #3
 8005a42:	781b      	ldrb	r3, [r3, #0]
 8005a44:	2b00      	cmp	r3, #0
 8005a46:	d003      	beq.n	8005a50 <VL53L0X_SetGpioConfig+0x38>
 8005a48:	187b      	adds	r3, r7, r1
 8005a4a:	22f6      	movs	r2, #246	; 0xf6
 8005a4c:	701a      	strb	r2, [r3, #0]
 8005a4e:	e160      	b.n	8005d12 <VL53L0X_SetGpioConfig+0x2fa>
 8005a50:	1cbb      	adds	r3, r7, #2
 8005a52:	781b      	ldrb	r3, [r3, #0]
 8005a54:	2b14      	cmp	r3, #20
 8005a56:	d11a      	bne.n	8005a8e <VL53L0X_SetGpioConfig+0x76>
 8005a58:	2320      	movs	r3, #32
 8005a5a:	18fb      	adds	r3, r7, r3
 8005a5c:	781b      	ldrb	r3, [r3, #0]
 8005a5e:	2b00      	cmp	r3, #0
 8005a60:	d104      	bne.n	8005a6c <VL53L0X_SetGpioConfig+0x54>
 8005a62:	230e      	movs	r3, #14
 8005a64:	18fb      	adds	r3, r7, r3
 8005a66:	2210      	movs	r2, #16
 8005a68:	701a      	strb	r2, [r3, #0]
 8005a6a:	e003      	b.n	8005a74 <VL53L0X_SetGpioConfig+0x5c>
 8005a6c:	230e      	movs	r3, #14
 8005a6e:	18fb      	adds	r3, r7, r3
 8005a70:	2201      	movs	r2, #1
 8005a72:	701a      	strb	r2, [r3, #0]
 8005a74:	230f      	movs	r3, #15
 8005a76:	18fc      	adds	r4, r7, r3
 8005a78:	230e      	movs	r3, #14
 8005a7a:	18fb      	adds	r3, r7, r3
 8005a7c:	781a      	ldrb	r2, [r3, #0]
 8005a7e:	687b      	ldr	r3, [r7, #4]
 8005a80:	2184      	movs	r1, #132	; 0x84
 8005a82:	0018      	movs	r0, r3
 8005a84:	f7fd ff36 	bl	80038f4 <VL53L0X_WrByte>
 8005a88:	0003      	movs	r3, r0
 8005a8a:	7023      	strb	r3, [r4, #0]
 8005a8c:	e141      	b.n	8005d12 <VL53L0X_SetGpioConfig+0x2fa>
 8005a8e:	1cbb      	adds	r3, r7, #2
 8005a90:	781b      	ldrb	r3, [r3, #0]
 8005a92:	2b15      	cmp	r3, #21
 8005a94:	d000      	beq.n	8005a98 <VL53L0X_SetGpioConfig+0x80>
 8005a96:	e0c4      	b.n	8005c22 <VL53L0X_SetGpioConfig+0x20a>
 8005a98:	687b      	ldr	r3, [r7, #4]
 8005a9a:	2201      	movs	r2, #1
 8005a9c:	21ff      	movs	r1, #255	; 0xff
 8005a9e:	0018      	movs	r0, r3
 8005aa0:	f7fd ff28 	bl	80038f4 <VL53L0X_WrByte>
 8005aa4:	0003      	movs	r3, r0
 8005aa6:	0019      	movs	r1, r3
 8005aa8:	240f      	movs	r4, #15
 8005aaa:	193b      	adds	r3, r7, r4
 8005aac:	193a      	adds	r2, r7, r4
 8005aae:	7812      	ldrb	r2, [r2, #0]
 8005ab0:	430a      	orrs	r2, r1
 8005ab2:	701a      	strb	r2, [r3, #0]
 8005ab4:	687b      	ldr	r3, [r7, #4]
 8005ab6:	2200      	movs	r2, #0
 8005ab8:	2100      	movs	r1, #0
 8005aba:	0018      	movs	r0, r3
 8005abc:	f7fd ff1a 	bl	80038f4 <VL53L0X_WrByte>
 8005ac0:	0003      	movs	r3, r0
 8005ac2:	0019      	movs	r1, r3
 8005ac4:	193b      	adds	r3, r7, r4
 8005ac6:	193a      	adds	r2, r7, r4
 8005ac8:	7812      	ldrb	r2, [r2, #0]
 8005aca:	430a      	orrs	r2, r1
 8005acc:	701a      	strb	r2, [r3, #0]
 8005ace:	687b      	ldr	r3, [r7, #4]
 8005ad0:	2200      	movs	r2, #0
 8005ad2:	21ff      	movs	r1, #255	; 0xff
 8005ad4:	0018      	movs	r0, r3
 8005ad6:	f7fd ff0d 	bl	80038f4 <VL53L0X_WrByte>
 8005ada:	0003      	movs	r3, r0
 8005adc:	0019      	movs	r1, r3
 8005ade:	193b      	adds	r3, r7, r4
 8005ae0:	193a      	adds	r2, r7, r4
 8005ae2:	7812      	ldrb	r2, [r2, #0]
 8005ae4:	430a      	orrs	r2, r1
 8005ae6:	701a      	strb	r2, [r3, #0]
 8005ae8:	687b      	ldr	r3, [r7, #4]
 8005aea:	2201      	movs	r2, #1
 8005aec:	2180      	movs	r1, #128	; 0x80
 8005aee:	0018      	movs	r0, r3
 8005af0:	f7fd ff00 	bl	80038f4 <VL53L0X_WrByte>
 8005af4:	0003      	movs	r3, r0
 8005af6:	0019      	movs	r1, r3
 8005af8:	193b      	adds	r3, r7, r4
 8005afa:	193a      	adds	r2, r7, r4
 8005afc:	7812      	ldrb	r2, [r2, #0]
 8005afe:	430a      	orrs	r2, r1
 8005b00:	701a      	strb	r2, [r3, #0]
 8005b02:	687b      	ldr	r3, [r7, #4]
 8005b04:	2202      	movs	r2, #2
 8005b06:	2185      	movs	r1, #133	; 0x85
 8005b08:	0018      	movs	r0, r3
 8005b0a:	f7fd fef3 	bl	80038f4 <VL53L0X_WrByte>
 8005b0e:	0003      	movs	r3, r0
 8005b10:	0019      	movs	r1, r3
 8005b12:	193b      	adds	r3, r7, r4
 8005b14:	193a      	adds	r2, r7, r4
 8005b16:	7812      	ldrb	r2, [r2, #0]
 8005b18:	430a      	orrs	r2, r1
 8005b1a:	701a      	strb	r2, [r3, #0]
 8005b1c:	687b      	ldr	r3, [r7, #4]
 8005b1e:	2204      	movs	r2, #4
 8005b20:	21ff      	movs	r1, #255	; 0xff
 8005b22:	0018      	movs	r0, r3
 8005b24:	f7fd fee6 	bl	80038f4 <VL53L0X_WrByte>
 8005b28:	0003      	movs	r3, r0
 8005b2a:	0019      	movs	r1, r3
 8005b2c:	193b      	adds	r3, r7, r4
 8005b2e:	193a      	adds	r2, r7, r4
 8005b30:	7812      	ldrb	r2, [r2, #0]
 8005b32:	430a      	orrs	r2, r1
 8005b34:	701a      	strb	r2, [r3, #0]
 8005b36:	687b      	ldr	r3, [r7, #4]
 8005b38:	2200      	movs	r2, #0
 8005b3a:	21cd      	movs	r1, #205	; 0xcd
 8005b3c:	0018      	movs	r0, r3
 8005b3e:	f7fd fed9 	bl	80038f4 <VL53L0X_WrByte>
 8005b42:	0003      	movs	r3, r0
 8005b44:	0019      	movs	r1, r3
 8005b46:	193b      	adds	r3, r7, r4
 8005b48:	193a      	adds	r2, r7, r4
 8005b4a:	7812      	ldrb	r2, [r2, #0]
 8005b4c:	430a      	orrs	r2, r1
 8005b4e:	701a      	strb	r2, [r3, #0]
 8005b50:	687b      	ldr	r3, [r7, #4]
 8005b52:	2211      	movs	r2, #17
 8005b54:	21cc      	movs	r1, #204	; 0xcc
 8005b56:	0018      	movs	r0, r3
 8005b58:	f7fd fecc 	bl	80038f4 <VL53L0X_WrByte>
 8005b5c:	0003      	movs	r3, r0
 8005b5e:	0019      	movs	r1, r3
 8005b60:	193b      	adds	r3, r7, r4
 8005b62:	193a      	adds	r2, r7, r4
 8005b64:	7812      	ldrb	r2, [r2, #0]
 8005b66:	430a      	orrs	r2, r1
 8005b68:	701a      	strb	r2, [r3, #0]
 8005b6a:	687b      	ldr	r3, [r7, #4]
 8005b6c:	2207      	movs	r2, #7
 8005b6e:	21ff      	movs	r1, #255	; 0xff
 8005b70:	0018      	movs	r0, r3
 8005b72:	f7fd febf 	bl	80038f4 <VL53L0X_WrByte>
 8005b76:	0003      	movs	r3, r0
 8005b78:	0019      	movs	r1, r3
 8005b7a:	193b      	adds	r3, r7, r4
 8005b7c:	193a      	adds	r2, r7, r4
 8005b7e:	7812      	ldrb	r2, [r2, #0]
 8005b80:	430a      	orrs	r2, r1
 8005b82:	701a      	strb	r2, [r3, #0]
 8005b84:	687b      	ldr	r3, [r7, #4]
 8005b86:	2200      	movs	r2, #0
 8005b88:	21be      	movs	r1, #190	; 0xbe
 8005b8a:	0018      	movs	r0, r3
 8005b8c:	f7fd feb2 	bl	80038f4 <VL53L0X_WrByte>
 8005b90:	0003      	movs	r3, r0
 8005b92:	0019      	movs	r1, r3
 8005b94:	193b      	adds	r3, r7, r4
 8005b96:	193a      	adds	r2, r7, r4
 8005b98:	7812      	ldrb	r2, [r2, #0]
 8005b9a:	430a      	orrs	r2, r1
 8005b9c:	701a      	strb	r2, [r3, #0]
 8005b9e:	687b      	ldr	r3, [r7, #4]
 8005ba0:	2206      	movs	r2, #6
 8005ba2:	21ff      	movs	r1, #255	; 0xff
 8005ba4:	0018      	movs	r0, r3
 8005ba6:	f7fd fea5 	bl	80038f4 <VL53L0X_WrByte>
 8005baa:	0003      	movs	r3, r0
 8005bac:	0019      	movs	r1, r3
 8005bae:	193b      	adds	r3, r7, r4
 8005bb0:	193a      	adds	r2, r7, r4
 8005bb2:	7812      	ldrb	r2, [r2, #0]
 8005bb4:	430a      	orrs	r2, r1
 8005bb6:	701a      	strb	r2, [r3, #0]
 8005bb8:	687b      	ldr	r3, [r7, #4]
 8005bba:	2209      	movs	r2, #9
 8005bbc:	21cc      	movs	r1, #204	; 0xcc
 8005bbe:	0018      	movs	r0, r3
 8005bc0:	f7fd fe98 	bl	80038f4 <VL53L0X_WrByte>
 8005bc4:	0003      	movs	r3, r0
 8005bc6:	0019      	movs	r1, r3
 8005bc8:	193b      	adds	r3, r7, r4
 8005bca:	193a      	adds	r2, r7, r4
 8005bcc:	7812      	ldrb	r2, [r2, #0]
 8005bce:	430a      	orrs	r2, r1
 8005bd0:	701a      	strb	r2, [r3, #0]
 8005bd2:	687b      	ldr	r3, [r7, #4]
 8005bd4:	2200      	movs	r2, #0
 8005bd6:	21ff      	movs	r1, #255	; 0xff
 8005bd8:	0018      	movs	r0, r3
 8005bda:	f7fd fe8b 	bl	80038f4 <VL53L0X_WrByte>
 8005bde:	0003      	movs	r3, r0
 8005be0:	0019      	movs	r1, r3
 8005be2:	193b      	adds	r3, r7, r4
 8005be4:	193a      	adds	r2, r7, r4
 8005be6:	7812      	ldrb	r2, [r2, #0]
 8005be8:	430a      	orrs	r2, r1
 8005bea:	701a      	strb	r2, [r3, #0]
 8005bec:	687b      	ldr	r3, [r7, #4]
 8005bee:	2201      	movs	r2, #1
 8005bf0:	21ff      	movs	r1, #255	; 0xff
 8005bf2:	0018      	movs	r0, r3
 8005bf4:	f7fd fe7e 	bl	80038f4 <VL53L0X_WrByte>
 8005bf8:	0003      	movs	r3, r0
 8005bfa:	0019      	movs	r1, r3
 8005bfc:	193b      	adds	r3, r7, r4
 8005bfe:	193a      	adds	r2, r7, r4
 8005c00:	7812      	ldrb	r2, [r2, #0]
 8005c02:	430a      	orrs	r2, r1
 8005c04:	701a      	strb	r2, [r3, #0]
 8005c06:	687b      	ldr	r3, [r7, #4]
 8005c08:	2200      	movs	r2, #0
 8005c0a:	2100      	movs	r1, #0
 8005c0c:	0018      	movs	r0, r3
 8005c0e:	f7fd fe71 	bl	80038f4 <VL53L0X_WrByte>
 8005c12:	0003      	movs	r3, r0
 8005c14:	0019      	movs	r1, r3
 8005c16:	193b      	adds	r3, r7, r4
 8005c18:	193a      	adds	r2, r7, r4
 8005c1a:	7812      	ldrb	r2, [r2, #0]
 8005c1c:	430a      	orrs	r2, r1
 8005c1e:	701a      	strb	r2, [r3, #0]
 8005c20:	e077      	b.n	8005d12 <VL53L0X_SetGpioConfig+0x2fa>
 8005c22:	230f      	movs	r3, #15
 8005c24:	18fb      	adds	r3, r7, r3
 8005c26:	781b      	ldrb	r3, [r3, #0]
 8005c28:	b25b      	sxtb	r3, r3
 8005c2a:	2b00      	cmp	r3, #0
 8005c2c:	d126      	bne.n	8005c7c <VL53L0X_SetGpioConfig+0x264>
 8005c2e:	1c7b      	adds	r3, r7, #1
 8005c30:	781b      	ldrb	r3, [r3, #0]
 8005c32:	2b04      	cmp	r3, #4
 8005c34:	d81d      	bhi.n	8005c72 <VL53L0X_SetGpioConfig+0x25a>
 8005c36:	009a      	lsls	r2, r3, #2
 8005c38:	4b3a      	ldr	r3, [pc, #232]	; (8005d24 <VL53L0X_SetGpioConfig+0x30c>)
 8005c3a:	18d3      	adds	r3, r2, r3
 8005c3c:	681b      	ldr	r3, [r3, #0]
 8005c3e:	469f      	mov	pc, r3
 8005c40:	230e      	movs	r3, #14
 8005c42:	18fb      	adds	r3, r7, r3
 8005c44:	2200      	movs	r2, #0
 8005c46:	701a      	strb	r2, [r3, #0]
 8005c48:	e019      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c4a:	230e      	movs	r3, #14
 8005c4c:	18fb      	adds	r3, r7, r3
 8005c4e:	2201      	movs	r2, #1
 8005c50:	701a      	strb	r2, [r3, #0]
 8005c52:	e014      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c54:	230e      	movs	r3, #14
 8005c56:	18fb      	adds	r3, r7, r3
 8005c58:	2202      	movs	r2, #2
 8005c5a:	701a      	strb	r2, [r3, #0]
 8005c5c:	e00f      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c5e:	230e      	movs	r3, #14
 8005c60:	18fb      	adds	r3, r7, r3
 8005c62:	2203      	movs	r2, #3
 8005c64:	701a      	strb	r2, [r3, #0]
 8005c66:	e00a      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c68:	230e      	movs	r3, #14
 8005c6a:	18fb      	adds	r3, r7, r3
 8005c6c:	2204      	movs	r2, #4
 8005c6e:	701a      	strb	r2, [r3, #0]
 8005c70:	e005      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c72:	230f      	movs	r3, #15
 8005c74:	18fb      	adds	r3, r7, r3
 8005c76:	22f5      	movs	r2, #245	; 0xf5
 8005c78:	701a      	strb	r2, [r3, #0]
 8005c7a:	e000      	b.n	8005c7e <VL53L0X_SetGpioConfig+0x266>
 8005c7c:	46c0      	nop			; (mov r8, r8)
 8005c7e:	220f      	movs	r2, #15
 8005c80:	18bb      	adds	r3, r7, r2
 8005c82:	781b      	ldrb	r3, [r3, #0]
 8005c84:	b25b      	sxtb	r3, r3
 8005c86:	2b00      	cmp	r3, #0
 8005c88:	d10a      	bne.n	8005ca0 <VL53L0X_SetGpioConfig+0x288>
 8005c8a:	18bc      	adds	r4, r7, r2
 8005c8c:	230e      	movs	r3, #14
 8005c8e:	18fb      	adds	r3, r7, r3
 8005c90:	781a      	ldrb	r2, [r3, #0]
 8005c92:	687b      	ldr	r3, [r7, #4]
 8005c94:	210a      	movs	r1, #10
 8005c96:	0018      	movs	r0, r3
 8005c98:	f7fd fe2c 	bl	80038f4 <VL53L0X_WrByte>
 8005c9c:	0003      	movs	r3, r0
 8005c9e:	7023      	strb	r3, [r4, #0]
 8005ca0:	230f      	movs	r3, #15
 8005ca2:	18fb      	adds	r3, r7, r3
 8005ca4:	781b      	ldrb	r3, [r3, #0]
 8005ca6:	b25b      	sxtb	r3, r3
 8005ca8:	2b00      	cmp	r3, #0
 8005caa:	d119      	bne.n	8005ce0 <VL53L0X_SetGpioConfig+0x2c8>
 8005cac:	2320      	movs	r3, #32
 8005cae:	18fb      	adds	r3, r7, r3
 8005cb0:	781b      	ldrb	r3, [r3, #0]
 8005cb2:	2b00      	cmp	r3, #0
 8005cb4:	d104      	bne.n	8005cc0 <VL53L0X_SetGpioConfig+0x2a8>
 8005cb6:	230e      	movs	r3, #14
 8005cb8:	18fb      	adds	r3, r7, r3
 8005cba:	2200      	movs	r2, #0
 8005cbc:	701a      	strb	r2, [r3, #0]
 8005cbe:	e003      	b.n	8005cc8 <VL53L0X_SetGpioConfig+0x2b0>
 8005cc0:	230e      	movs	r3, #14
 8005cc2:	18fb      	adds	r3, r7, r3
 8005cc4:	2210      	movs	r2, #16
 8005cc6:	701a      	strb	r2, [r3, #0]
 8005cc8:	230f      	movs	r3, #15
 8005cca:	18fc      	adds	r4, r7, r3
 8005ccc:	230e      	movs	r3, #14
 8005cce:	18fb      	adds	r3, r7, r3
 8005cd0:	781b      	ldrb	r3, [r3, #0]
 8005cd2:	6878      	ldr	r0, [r7, #4]
 8005cd4:	22ef      	movs	r2, #239	; 0xef
 8005cd6:	2184      	movs	r1, #132	; 0x84
 8005cd8:	f7fd fe49 	bl	800396e <VL53L0X_UpdateByte>
 8005cdc:	0003      	movs	r3, r0
 8005cde:	7023      	strb	r3, [r4, #0]
 8005ce0:	230f      	movs	r3, #15
 8005ce2:	18fb      	adds	r3, r7, r3
 8005ce4:	781b      	ldrb	r3, [r3, #0]
 8005ce6:	b25b      	sxtb	r3, r3
 8005ce8:	2b00      	cmp	r3, #0
 8005cea:	d104      	bne.n	8005cf6 <VL53L0X_SetGpioConfig+0x2de>
 8005cec:	687b      	ldr	r3, [r7, #4]
 8005cee:	1c7a      	adds	r2, r7, #1
 8005cf0:	21da      	movs	r1, #218	; 0xda
 8005cf2:	7812      	ldrb	r2, [r2, #0]
 8005cf4:	545a      	strb	r2, [r3, r1]
 8005cf6:	220f      	movs	r2, #15
 8005cf8:	18bb      	adds	r3, r7, r2
 8005cfa:	781b      	ldrb	r3, [r3, #0]
 8005cfc:	b25b      	sxtb	r3, r3
 8005cfe:	2b00      	cmp	r3, #0
 8005d00:	d107      	bne.n	8005d12 <VL53L0X_SetGpioConfig+0x2fa>
 8005d02:	18bc      	adds	r4, r7, r2
 8005d04:	687b      	ldr	r3, [r7, #4]
 8005d06:	2100      	movs	r1, #0
 8005d08:	0018      	movs	r0, r3
 8005d0a:	f000 f84f 	bl	8005dac <VL53L0X_ClearInterruptMask>
 8005d0e:	0003      	movs	r3, r0
 8005d10:	7023      	strb	r3, [r4, #0]
 8005d12:	230f      	movs	r3, #15
 8005d14:	18fb      	adds	r3, r7, r3
 8005d16:	781b      	ldrb	r3, [r3, #0]
 8005d18:	b25b      	sxtb	r3, r3
 8005d1a:	0018      	movs	r0, r3
 8005d1c:	46bd      	mov	sp, r7
 8005d1e:	b005      	add	sp, #20
 8005d20:	bd90      	pop	{r4, r7, pc}
 8005d22:	46c0      	nop			; (mov r8, r8)
 8005d24:	0800a0c0 	.word	0x0800a0c0

08005d28 <VL53L0X_GetInterruptThresholds>:
 8005d28:	b5f0      	push	{r4, r5, r6, r7, lr}
 8005d2a:	b087      	sub	sp, #28
 8005d2c:	af00      	add	r7, sp, #0
 8005d2e:	60f8      	str	r0, [r7, #12]
 8005d30:	607a      	str	r2, [r7, #4]
 8005d32:	603b      	str	r3, [r7, #0]
 8005d34:	230b      	movs	r3, #11
 8005d36:	18fb      	adds	r3, r7, r3
 8005d38:	1c0a      	adds	r2, r1, #0
 8005d3a:	701a      	strb	r2, [r3, #0]
 8005d3c:	2517      	movs	r5, #23
 8005d3e:	197b      	adds	r3, r7, r5
 8005d40:	2200      	movs	r2, #0
 8005d42:	701a      	strb	r2, [r3, #0]
 8005d44:	197c      	adds	r4, r7, r5
 8005d46:	2614      	movs	r6, #20
 8005d48:	19ba      	adds	r2, r7, r6
 8005d4a:	68fb      	ldr	r3, [r7, #12]
 8005d4c:	210e      	movs	r1, #14
 8005d4e:	0018      	movs	r0, r3
 8005d50:	f7fd fe53 	bl	80039fa <VL53L0X_RdWord>
 8005d54:	0003      	movs	r3, r0
 8005d56:	7023      	strb	r3, [r4, #0]
 8005d58:	0031      	movs	r1, r6
 8005d5a:	187b      	adds	r3, r7, r1
 8005d5c:	881b      	ldrh	r3, [r3, #0]
 8005d5e:	045b      	lsls	r3, r3, #17
 8005d60:	001a      	movs	r2, r3
 8005d62:	4b11      	ldr	r3, [pc, #68]	; (8005da8 <VL53L0X_GetInterruptThresholds+0x80>)
 8005d64:	401a      	ands	r2, r3
 8005d66:	687b      	ldr	r3, [r7, #4]
 8005d68:	601a      	str	r2, [r3, #0]
 8005d6a:	197b      	adds	r3, r7, r5
 8005d6c:	781b      	ldrb	r3, [r3, #0]
 8005d6e:	b25b      	sxtb	r3, r3
 8005d70:	2b00      	cmp	r3, #0
 8005d72:	d111      	bne.n	8005d98 <VL53L0X_GetInterruptThresholds+0x70>
 8005d74:	197c      	adds	r4, r7, r5
 8005d76:	000d      	movs	r5, r1
 8005d78:	187a      	adds	r2, r7, r1
 8005d7a:	68fb      	ldr	r3, [r7, #12]
 8005d7c:	210c      	movs	r1, #12
 8005d7e:	0018      	movs	r0, r3
 8005d80:	f7fd fe3b 	bl	80039fa <VL53L0X_RdWord>
 8005d84:	0003      	movs	r3, r0
 8005d86:	7023      	strb	r3, [r4, #0]
 8005d88:	197b      	adds	r3, r7, r5
 8005d8a:	881b      	ldrh	r3, [r3, #0]
 8005d8c:	045b      	lsls	r3, r3, #17
 8005d8e:	001a      	movs	r2, r3
 8005d90:	4b05      	ldr	r3, [pc, #20]	; (8005da8 <VL53L0X_GetInterruptThresholds+0x80>)
 8005d92:	401a      	ands	r2, r3
 8005d94:	683b      	ldr	r3, [r7, #0]
 8005d96:	601a      	str	r2, [r3, #0]
 8005d98:	2317      	movs	r3, #23
 8005d9a:	18fb      	adds	r3, r7, r3
 8005d9c:	781b      	ldrb	r3, [r3, #0]
 8005d9e:	b25b      	sxtb	r3, r3
 8005da0:	0018      	movs	r0, r3
 8005da2:	46bd      	mov	sp, r7
 8005da4:	b007      	add	sp, #28
 8005da6:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8005da8:	1ffe0000 	.word	0x1ffe0000

08005dac <VL53L0X_ClearInterruptMask>:
 8005dac:	b5b0      	push	{r4, r5, r7, lr}
 8005dae:	b084      	sub	sp, #16
 8005db0:	af00      	add	r7, sp, #0
 8005db2:	6078      	str	r0, [r7, #4]
 8005db4:	6039      	str	r1, [r7, #0]
 8005db6:	230f      	movs	r3, #15
 8005db8:	18fb      	adds	r3, r7, r3
 8005dba:	2200      	movs	r2, #0
 8005dbc:	701a      	strb	r2, [r3, #0]
 8005dbe:	230e      	movs	r3, #14
 8005dc0:	18fb      	adds	r3, r7, r3
 8005dc2:	2200      	movs	r2, #0
 8005dc4:	701a      	strb	r2, [r3, #0]
 8005dc6:	250f      	movs	r5, #15
 8005dc8:	197c      	adds	r4, r7, r5
 8005dca:	687b      	ldr	r3, [r7, #4]
 8005dcc:	2201      	movs	r2, #1
 8005dce:	210b      	movs	r1, #11
 8005dd0:	0018      	movs	r0, r3
 8005dd2:	f7fd fd8f 	bl	80038f4 <VL53L0X_WrByte>
 8005dd6:	0003      	movs	r3, r0
 8005dd8:	7023      	strb	r3, [r4, #0]
 8005dda:	687b      	ldr	r3, [r7, #4]
 8005ddc:	2200      	movs	r2, #0
 8005dde:	210b      	movs	r1, #11
 8005de0:	0018      	movs	r0, r3
 8005de2:	f7fd fd87 	bl	80038f4 <VL53L0X_WrByte>
 8005de6:	0003      	movs	r3, r0
 8005de8:	0019      	movs	r1, r3
 8005dea:	002c      	movs	r4, r5
 8005dec:	193b      	adds	r3, r7, r4
 8005dee:	193a      	adds	r2, r7, r4
 8005df0:	7812      	ldrb	r2, [r2, #0]
 8005df2:	430a      	orrs	r2, r1
 8005df4:	701a      	strb	r2, [r3, #0]
 8005df6:	250d      	movs	r5, #13
 8005df8:	197a      	adds	r2, r7, r5
 8005dfa:	687b      	ldr	r3, [r7, #4]
 8005dfc:	2113      	movs	r1, #19
 8005dfe:	0018      	movs	r0, r3
 8005e00:	f7fd fde6 	bl	80039d0 <VL53L0X_RdByte>
 8005e04:	0003      	movs	r3, r0
 8005e06:	0019      	movs	r1, r3
 8005e08:	0020      	movs	r0, r4
 8005e0a:	183b      	adds	r3, r7, r0
 8005e0c:	183a      	adds	r2, r7, r0
 8005e0e:	7812      	ldrb	r2, [r2, #0]
 8005e10:	430a      	orrs	r2, r1
 8005e12:	701a      	strb	r2, [r3, #0]
 8005e14:	210e      	movs	r1, #14
 8005e16:	187b      	adds	r3, r7, r1
 8005e18:	781a      	ldrb	r2, [r3, #0]
 8005e1a:	187b      	adds	r3, r7, r1
 8005e1c:	3201      	adds	r2, #1
 8005e1e:	701a      	strb	r2, [r3, #0]
 8005e20:	197b      	adds	r3, r7, r5
 8005e22:	781b      	ldrb	r3, [r3, #0]
 8005e24:	001a      	movs	r2, r3
 8005e26:	2307      	movs	r3, #7
 8005e28:	4013      	ands	r3, r2
 8005e2a:	d008      	beq.n	8005e3e <VL53L0X_ClearInterruptMask+0x92>
 8005e2c:	187b      	adds	r3, r7, r1
 8005e2e:	781b      	ldrb	r3, [r3, #0]
 8005e30:	2b02      	cmp	r3, #2
 8005e32:	d804      	bhi.n	8005e3e <VL53L0X_ClearInterruptMask+0x92>
 8005e34:	183b      	adds	r3, r7, r0
 8005e36:	781b      	ldrb	r3, [r3, #0]
 8005e38:	b25b      	sxtb	r3, r3
 8005e3a:	2b00      	cmp	r3, #0
 8005e3c:	d0c3      	beq.n	8005dc6 <VL53L0X_ClearInterruptMask+0x1a>
 8005e3e:	230e      	movs	r3, #14
 8005e40:	18fb      	adds	r3, r7, r3
 8005e42:	781b      	ldrb	r3, [r3, #0]
 8005e44:	2b02      	cmp	r3, #2
 8005e46:	d903      	bls.n	8005e50 <VL53L0X_ClearInterruptMask+0xa4>
 8005e48:	230f      	movs	r3, #15
 8005e4a:	18fb      	adds	r3, r7, r3
 8005e4c:	22f4      	movs	r2, #244	; 0xf4
 8005e4e:	701a      	strb	r2, [r3, #0]
 8005e50:	230f      	movs	r3, #15
 8005e52:	18fb      	adds	r3, r7, r3
 8005e54:	781b      	ldrb	r3, [r3, #0]
 8005e56:	b25b      	sxtb	r3, r3
 8005e58:	0018      	movs	r0, r3
 8005e5a:	46bd      	mov	sp, r7
 8005e5c:	b004      	add	sp, #16
 8005e5e:	bdb0      	pop	{r4, r5, r7, pc}

08005e60 <VL53L0X_GetInterruptMaskStatus>:
 8005e60:	b5f0      	push	{r4, r5, r6, r7, lr}
 8005e62:	b085      	sub	sp, #20
 8005e64:	af00      	add	r7, sp, #0
 8005e66:	6078      	str	r0, [r7, #4]
 8005e68:	6039      	str	r1, [r7, #0]
 8005e6a:	250f      	movs	r5, #15
 8005e6c:	197b      	adds	r3, r7, r5
 8005e6e:	2200      	movs	r2, #0
 8005e70:	701a      	strb	r2, [r3, #0]
 8005e72:	197c      	adds	r4, r7, r5
 8005e74:	260e      	movs	r6, #14
 8005e76:	19ba      	adds	r2, r7, r6
 8005e78:	687b      	ldr	r3, [r7, #4]
 8005e7a:	2113      	movs	r1, #19
 8005e7c:	0018      	movs	r0, r3
 8005e7e:	f7fd fda7 	bl	80039d0 <VL53L0X_RdByte>
 8005e82:	0003      	movs	r3, r0
 8005e84:	7023      	strb	r3, [r4, #0]
 8005e86:	0031      	movs	r1, r6
 8005e88:	187b      	adds	r3, r7, r1
 8005e8a:	781b      	ldrb	r3, [r3, #0]
 8005e8c:	001a      	movs	r2, r3
 8005e8e:	2307      	movs	r3, #7
 8005e90:	401a      	ands	r2, r3
 8005e92:	683b      	ldr	r3, [r7, #0]
 8005e94:	601a      	str	r2, [r3, #0]
 8005e96:	187b      	adds	r3, r7, r1
 8005e98:	781b      	ldrb	r3, [r3, #0]
 8005e9a:	001a      	movs	r2, r3
 8005e9c:	2318      	movs	r3, #24
 8005e9e:	4013      	ands	r3, r2
 8005ea0:	d002      	beq.n	8005ea8 <VL53L0X_GetInterruptMaskStatus+0x48>
 8005ea2:	197b      	adds	r3, r7, r5
 8005ea4:	22fa      	movs	r2, #250	; 0xfa
 8005ea6:	701a      	strb	r2, [r3, #0]
 8005ea8:	230f      	movs	r3, #15
 8005eaa:	18fb      	adds	r3, r7, r3
 8005eac:	781b      	ldrb	r3, [r3, #0]
 8005eae:	b25b      	sxtb	r3, r3
 8005eb0:	0018      	movs	r0, r3
 8005eb2:	46bd      	mov	sp, r7
 8005eb4:	b005      	add	sp, #20
 8005eb6:	bdf0      	pop	{r4, r5, r6, r7, pc}

08005eb8 <VL53L0X_PerformRefSpadManagement>:
 8005eb8:	b5b0      	push	{r4, r5, r7, lr}
 8005eba:	b086      	sub	sp, #24
 8005ebc:	af00      	add	r7, sp, #0
 8005ebe:	60f8      	str	r0, [r7, #12]
 8005ec0:	60b9      	str	r1, [r7, #8]
 8005ec2:	607a      	str	r2, [r7, #4]
 8005ec4:	2517      	movs	r5, #23
 8005ec6:	197b      	adds	r3, r7, r5
 8005ec8:	2200      	movs	r2, #0
 8005eca:	701a      	strb	r2, [r3, #0]
 8005ecc:	197c      	adds	r4, r7, r5
 8005ece:	687a      	ldr	r2, [r7, #4]
 8005ed0:	68b9      	ldr	r1, [r7, #8]
 8005ed2:	68fb      	ldr	r3, [r7, #12]
 8005ed4:	0018      	movs	r0, r3
 8005ed6:	f000 fa58 	bl	800638a <VL53L0X_perform_ref_spad_management>
 8005eda:	0003      	movs	r3, r0
 8005edc:	7023      	strb	r3, [r4, #0]
 8005ede:	197b      	adds	r3, r7, r5
 8005ee0:	781b      	ldrb	r3, [r3, #0]
 8005ee2:	b25b      	sxtb	r3, r3
 8005ee4:	0018      	movs	r0, r3
 8005ee6:	46bd      	mov	sp, r7
 8005ee8:	b006      	add	sp, #24
 8005eea:	bdb0      	pop	{r4, r5, r7, pc}

08005eec <VL53L0X_get_offset_calibration_data_micro_meter>:
 8005eec:	b5f0      	push	{r4, r5, r6, r7, lr}
 8005eee:	b085      	sub	sp, #20
 8005ef0:	af00      	add	r7, sp, #0
 8005ef2:	6078      	str	r0, [r7, #4]
 8005ef4:	6039      	str	r1, [r7, #0]
 8005ef6:	210f      	movs	r1, #15
 8005ef8:	187b      	adds	r3, r7, r1
 8005efa:	2200      	movs	r2, #0
 8005efc:	701a      	strb	r2, [r3, #0]
 8005efe:	230c      	movs	r3, #12
 8005f00:	18fb      	adds	r3, r7, r3
 8005f02:	4a23      	ldr	r2, [pc, #140]	; (8005f90 <VL53L0X_get_offset_calibration_data_micro_meter+0xa4>)
 8005f04:	801a      	strh	r2, [r3, #0]
 8005f06:	220a      	movs	r2, #10
 8005f08:	18bb      	adds	r3, r7, r2
 8005f0a:	2280      	movs	r2, #128	; 0x80
 8005f0c:	0152      	lsls	r2, r2, #5
 8005f0e:	801a      	strh	r2, [r3, #0]
 8005f10:	000e      	movs	r6, r1
 8005f12:	187c      	adds	r4, r7, r1
 8005f14:	2508      	movs	r5, #8
 8005f16:	197a      	adds	r2, r7, r5
 8005f18:	687b      	ldr	r3, [r7, #4]
 8005f1a:	2128      	movs	r1, #40	; 0x28
 8005f1c:	0018      	movs	r0, r3
 8005f1e:	f7fd fd6c 	bl	80039fa <VL53L0X_RdWord>
 8005f22:	0003      	movs	r3, r0
 8005f24:	7023      	strb	r3, [r4, #0]
 8005f26:	19bb      	adds	r3, r7, r6
 8005f28:	781b      	ldrb	r3, [r3, #0]
 8005f2a:	b25b      	sxtb	r3, r3
 8005f2c:	2b00      	cmp	r3, #0
 8005f2e:	d127      	bne.n	8005f80 <VL53L0X_get_offset_calibration_data_micro_meter+0x94>
 8005f30:	197b      	adds	r3, r7, r5
 8005f32:	881b      	ldrh	r3, [r3, #0]
 8005f34:	051b      	lsls	r3, r3, #20
 8005f36:	0d1b      	lsrs	r3, r3, #20
 8005f38:	b29a      	uxth	r2, r3
 8005f3a:	197b      	adds	r3, r7, r5
 8005f3c:	801a      	strh	r2, [r3, #0]
 8005f3e:	0029      	movs	r1, r5
 8005f40:	197b      	adds	r3, r7, r5
 8005f42:	881b      	ldrh	r3, [r3, #0]
 8005f44:	001a      	movs	r2, r3
 8005f46:	230c      	movs	r3, #12
 8005f48:	18fb      	adds	r3, r7, r3
 8005f4a:	2000      	movs	r0, #0
 8005f4c:	5e1b      	ldrsh	r3, [r3, r0]
 8005f4e:	429a      	cmp	r2, r3
 8005f50:	dd0d      	ble.n	8005f6e <VL53L0X_get_offset_calibration_data_micro_meter+0x82>
 8005f52:	187b      	adds	r3, r7, r1
 8005f54:	881a      	ldrh	r2, [r3, #0]
 8005f56:	230a      	movs	r3, #10
 8005f58:	18fb      	adds	r3, r7, r3
 8005f5a:	881b      	ldrh	r3, [r3, #0]
 8005f5c:	1ad3      	subs	r3, r2, r3
 8005f5e:	b29b      	uxth	r3, r3
 8005f60:	b21b      	sxth	r3, r3
 8005f62:	001a      	movs	r2, r3
 8005f64:	23fa      	movs	r3, #250	; 0xfa
 8005f66:	435a      	muls	r2, r3
 8005f68:	683b      	ldr	r3, [r7, #0]
 8005f6a:	601a      	str	r2, [r3, #0]
 8005f6c:	e008      	b.n	8005f80 <VL53L0X_get_offset_calibration_data_micro_meter+0x94>
 8005f6e:	2308      	movs	r3, #8
 8005f70:	18fb      	adds	r3, r7, r3
 8005f72:	881b      	ldrh	r3, [r3, #0]
 8005f74:	b21b      	sxth	r3, r3
 8005f76:	001a      	movs	r2, r3
 8005f78:	23fa      	movs	r3, #250	; 0xfa
 8005f7a:	435a      	muls	r2, r3
 8005f7c:	683b      	ldr	r3, [r7, #0]
 8005f7e:	601a      	str	r2, [r3, #0]
 8005f80:	230f      	movs	r3, #15
 8005f82:	18fb      	adds	r3, r7, r3
 8005f84:	781b      	ldrb	r3, [r3, #0]
 8005f86:	b25b      	sxtb	r3, r3
 8005f88:	0018      	movs	r0, r3
 8005f8a:	46bd      	mov	sp, r7
 8005f8c:	b005      	add	sp, #20
 8005f8e:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8005f90:	000007ff 	.word	0x000007ff

08005f94 <get_next_good_spad>:
 8005f94:	b580      	push	{r7, lr}
 8005f96:	b08a      	sub	sp, #40	; 0x28
 8005f98:	af00      	add	r7, sp, #0
 8005f9a:	60f8      	str	r0, [r7, #12]
 8005f9c:	60b9      	str	r1, [r7, #8]
 8005f9e:	607a      	str	r2, [r7, #4]
 8005fa0:	603b      	str	r3, [r7, #0]
 8005fa2:	2308      	movs	r3, #8
 8005fa4:	61bb      	str	r3, [r7, #24]
 8005fa6:	231e      	movs	r3, #30
 8005fa8:	18fb      	adds	r3, r7, r3
 8005faa:	2200      	movs	r2, #0
 8005fac:	701a      	strb	r2, [r3, #0]
 8005fae:	683b      	ldr	r3, [r7, #0]
 8005fb0:	2201      	movs	r2, #1
 8005fb2:	4252      	negs	r2, r2
 8005fb4:	601a      	str	r2, [r3, #0]
 8005fb6:	69b9      	ldr	r1, [r7, #24]
 8005fb8:	6878      	ldr	r0, [r7, #4]
 8005fba:	f7fa f8a5 	bl	8000108 <__udivsi3>
 8005fbe:	0003      	movs	r3, r0
 8005fc0:	617b      	str	r3, [r7, #20]
 8005fc2:	687b      	ldr	r3, [r7, #4]
 8005fc4:	69b9      	ldr	r1, [r7, #24]
 8005fc6:	0018      	movs	r0, r3
 8005fc8:	f7fa f924 	bl	8000214 <__aeabi_uidivmod>
 8005fcc:	000b      	movs	r3, r1
 8005fce:	613b      	str	r3, [r7, #16]
 8005fd0:	697b      	ldr	r3, [r7, #20]
 8005fd2:	627b      	str	r3, [r7, #36]	; 0x24
 8005fd4:	e038      	b.n	8006048 <get_next_good_spad+0xb4>
 8005fd6:	2300      	movs	r3, #0
 8005fd8:	623b      	str	r3, [r7, #32]
 8005fda:	68fa      	ldr	r2, [r7, #12]
 8005fdc:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8005fde:	18d2      	adds	r2, r2, r3
 8005fe0:	211f      	movs	r1, #31
 8005fe2:	187b      	adds	r3, r7, r1
 8005fe4:	7812      	ldrb	r2, [r2, #0]
 8005fe6:	701a      	strb	r2, [r3, #0]
 8005fe8:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8005fea:	697b      	ldr	r3, [r7, #20]
 8005fec:	429a      	cmp	r2, r3
 8005fee:	d124      	bne.n	800603a <get_next_good_spad+0xa6>
 8005ff0:	187b      	adds	r3, r7, r1
 8005ff2:	781a      	ldrb	r2, [r3, #0]
 8005ff4:	693b      	ldr	r3, [r7, #16]
 8005ff6:	411a      	asrs	r2, r3
 8005ff8:	187b      	adds	r3, r7, r1
 8005ffa:	701a      	strb	r2, [r3, #0]
 8005ffc:	693b      	ldr	r3, [r7, #16]
 8005ffe:	623b      	str	r3, [r7, #32]
 8006000:	e01b      	b.n	800603a <get_next_good_spad+0xa6>
 8006002:	231f      	movs	r3, #31
 8006004:	18fb      	adds	r3, r7, r3
 8006006:	781b      	ldrb	r3, [r3, #0]
 8006008:	2201      	movs	r2, #1
 800600a:	4013      	ands	r3, r2
 800600c:	d00c      	beq.n	8006028 <get_next_good_spad+0x94>
 800600e:	231e      	movs	r3, #30
 8006010:	18fb      	adds	r3, r7, r3
 8006012:	2201      	movs	r2, #1
 8006014:	701a      	strb	r2, [r3, #0]
 8006016:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8006018:	69ba      	ldr	r2, [r7, #24]
 800601a:	435a      	muls	r2, r3
 800601c:	6a3b      	ldr	r3, [r7, #32]
 800601e:	18d3      	adds	r3, r2, r3
 8006020:	001a      	movs	r2, r3
 8006022:	683b      	ldr	r3, [r7, #0]
 8006024:	601a      	str	r2, [r3, #0]
 8006026:	e00c      	b.n	8006042 <get_next_good_spad+0xae>
 8006028:	221f      	movs	r2, #31
 800602a:	18bb      	adds	r3, r7, r2
 800602c:	18ba      	adds	r2, r7, r2
 800602e:	7812      	ldrb	r2, [r2, #0]
 8006030:	0852      	lsrs	r2, r2, #1
 8006032:	701a      	strb	r2, [r3, #0]
 8006034:	6a3b      	ldr	r3, [r7, #32]
 8006036:	3301      	adds	r3, #1
 8006038:	623b      	str	r3, [r7, #32]
 800603a:	6a3a      	ldr	r2, [r7, #32]
 800603c:	69bb      	ldr	r3, [r7, #24]
 800603e:	429a      	cmp	r2, r3
 8006040:	d3df      	bcc.n	8006002 <get_next_good_spad+0x6e>
 8006042:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8006044:	3301      	adds	r3, #1
 8006046:	627b      	str	r3, [r7, #36]	; 0x24
 8006048:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 800604a:	68bb      	ldr	r3, [r7, #8]
 800604c:	429a      	cmp	r2, r3
 800604e:	d204      	bcs.n	800605a <get_next_good_spad+0xc6>
 8006050:	231e      	movs	r3, #30
 8006052:	18fb      	adds	r3, r7, r3
 8006054:	781b      	ldrb	r3, [r3, #0]
 8006056:	2b00      	cmp	r3, #0
 8006058:	d0bd      	beq.n	8005fd6 <get_next_good_spad+0x42>
 800605a:	46c0      	nop			; (mov r8, r8)
 800605c:	46bd      	mov	sp, r7
 800605e:	b00a      	add	sp, #40	; 0x28
 8006060:	bd80      	pop	{r7, pc}
	...

08006064 <is_aperture>:
 8006064:	b580      	push	{r7, lr}
 8006066:	b084      	sub	sp, #16
 8006068:	af00      	add	r7, sp, #0
 800606a:	6078      	str	r0, [r7, #4]
 800606c:	210f      	movs	r1, #15
 800606e:	187b      	adds	r3, r7, r1
 8006070:	2201      	movs	r2, #1
 8006072:	701a      	strb	r2, [r3, #0]
 8006074:	687b      	ldr	r3, [r7, #4]
 8006076:	099b      	lsrs	r3, r3, #6
 8006078:	60bb      	str	r3, [r7, #8]
 800607a:	4b08      	ldr	r3, [pc, #32]	; (800609c <is_aperture+0x38>)
 800607c:	68ba      	ldr	r2, [r7, #8]
 800607e:	0092      	lsls	r2, r2, #2
 8006080:	58d3      	ldr	r3, [r2, r3]
 8006082:	2b00      	cmp	r3, #0
 8006084:	d102      	bne.n	800608c <is_aperture+0x28>
 8006086:	187b      	adds	r3, r7, r1
 8006088:	2200      	movs	r2, #0
 800608a:	701a      	strb	r2, [r3, #0]
 800608c:	230f      	movs	r3, #15
 800608e:	18fb      	adds	r3, r7, r3
 8006090:	781b      	ldrb	r3, [r3, #0]
 8006092:	0018      	movs	r0, r3
 8006094:	46bd      	mov	sp, r7
 8006096:	b004      	add	sp, #16
 8006098:	bd80      	pop	{r7, pc}
 800609a:	46c0      	nop			; (mov r8, r8)
 800609c:	200002b0 	.word	0x200002b0

080060a0 <enable_spad_bit>:
 80060a0:	b590      	push	{r4, r7, lr}
 80060a2:	b089      	sub	sp, #36	; 0x24
 80060a4:	af00      	add	r7, sp, #0
 80060a6:	60f8      	str	r0, [r7, #12]
 80060a8:	60b9      	str	r1, [r7, #8]
 80060aa:	607a      	str	r2, [r7, #4]
 80060ac:	241f      	movs	r4, #31
 80060ae:	193b      	adds	r3, r7, r4
 80060b0:	2200      	movs	r2, #0
 80060b2:	701a      	strb	r2, [r3, #0]
 80060b4:	2308      	movs	r3, #8
 80060b6:	61bb      	str	r3, [r7, #24]
 80060b8:	69b9      	ldr	r1, [r7, #24]
 80060ba:	6878      	ldr	r0, [r7, #4]
 80060bc:	f7fa f824 	bl	8000108 <__udivsi3>
 80060c0:	0003      	movs	r3, r0
 80060c2:	617b      	str	r3, [r7, #20]
 80060c4:	687b      	ldr	r3, [r7, #4]
 80060c6:	69b9      	ldr	r1, [r7, #24]
 80060c8:	0018      	movs	r0, r3
 80060ca:	f7fa f8a3 	bl	8000214 <__aeabi_uidivmod>
 80060ce:	000b      	movs	r3, r1
 80060d0:	613b      	str	r3, [r7, #16]
 80060d2:	697a      	ldr	r2, [r7, #20]
 80060d4:	68bb      	ldr	r3, [r7, #8]
 80060d6:	429a      	cmp	r2, r3
 80060d8:	d303      	bcc.n	80060e2 <enable_spad_bit+0x42>
 80060da:	193b      	adds	r3, r7, r4
 80060dc:	22ce      	movs	r2, #206	; 0xce
 80060de:	701a      	strb	r2, [r3, #0]
 80060e0:	e010      	b.n	8006104 <enable_spad_bit+0x64>
 80060e2:	68fa      	ldr	r2, [r7, #12]
 80060e4:	697b      	ldr	r3, [r7, #20]
 80060e6:	18d3      	adds	r3, r2, r3
 80060e8:	781b      	ldrb	r3, [r3, #0]
 80060ea:	b25a      	sxtb	r2, r3
 80060ec:	2101      	movs	r1, #1
 80060ee:	693b      	ldr	r3, [r7, #16]
 80060f0:	4099      	lsls	r1, r3
 80060f2:	000b      	movs	r3, r1
 80060f4:	b25b      	sxtb	r3, r3
 80060f6:	4313      	orrs	r3, r2
 80060f8:	b259      	sxtb	r1, r3
 80060fa:	68fa      	ldr	r2, [r7, #12]
 80060fc:	697b      	ldr	r3, [r7, #20]
 80060fe:	18d3      	adds	r3, r2, r3
 8006100:	b2ca      	uxtb	r2, r1
 8006102:	701a      	strb	r2, [r3, #0]
 8006104:	231f      	movs	r3, #31
 8006106:	18fb      	adds	r3, r7, r3
 8006108:	781b      	ldrb	r3, [r3, #0]
 800610a:	b25b      	sxtb	r3, r3
 800610c:	0018      	movs	r0, r3
 800610e:	46bd      	mov	sp, r7
 8006110:	b009      	add	sp, #36	; 0x24
 8006112:	bd90      	pop	{r4, r7, pc}

08006114 <set_ref_spad_map>:
 8006114:	b5b0      	push	{r4, r5, r7, lr}
 8006116:	b084      	sub	sp, #16
 8006118:	af00      	add	r7, sp, #0
 800611a:	6078      	str	r0, [r7, #4]
 800611c:	6039      	str	r1, [r7, #0]
 800611e:	250f      	movs	r5, #15
 8006120:	197c      	adds	r4, r7, r5
 8006122:	683a      	ldr	r2, [r7, #0]
 8006124:	6878      	ldr	r0, [r7, #4]
 8006126:	2306      	movs	r3, #6
 8006128:	21b0      	movs	r1, #176	; 0xb0
 800612a:	f7fd faf1 	bl	8003710 <VL53L0X_WriteMulti>
 800612e:	0003      	movs	r3, r0
 8006130:	7023      	strb	r3, [r4, #0]
 8006132:	197b      	adds	r3, r7, r5
 8006134:	781b      	ldrb	r3, [r3, #0]
 8006136:	b25b      	sxtb	r3, r3
 8006138:	0018      	movs	r0, r3
 800613a:	46bd      	mov	sp, r7
 800613c:	b004      	add	sp, #16
 800613e:	bdb0      	pop	{r4, r5, r7, pc}

08006140 <get_ref_spad_map>:
 8006140:	b5b0      	push	{r4, r5, r7, lr}
 8006142:	b084      	sub	sp, #16
 8006144:	af00      	add	r7, sp, #0
 8006146:	6078      	str	r0, [r7, #4]
 8006148:	6039      	str	r1, [r7, #0]
 800614a:	250f      	movs	r5, #15
 800614c:	197c      	adds	r4, r7, r5
 800614e:	683a      	ldr	r2, [r7, #0]
 8006150:	6878      	ldr	r0, [r7, #4]
 8006152:	2306      	movs	r3, #6
 8006154:	21b0      	movs	r1, #176	; 0xb0
 8006156:	f7fd fb5b 	bl	8003810 <VL53L0X_ReadMulti>
 800615a:	0003      	movs	r3, r0
 800615c:	7023      	strb	r3, [r4, #0]
 800615e:	197b      	adds	r3, r7, r5
 8006160:	781b      	ldrb	r3, [r3, #0]
 8006162:	b25b      	sxtb	r3, r3
 8006164:	0018      	movs	r0, r3
 8006166:	46bd      	mov	sp, r7
 8006168:	b004      	add	sp, #16
 800616a:	bdb0      	pop	{r4, r5, r7, pc}

0800616c <enable_ref_spads>:
 800616c:	b590      	push	{r4, r7, lr}
 800616e:	b08d      	sub	sp, #52	; 0x34
 8006170:	af00      	add	r7, sp, #0
 8006172:	60f8      	str	r0, [r7, #12]
 8006174:	607a      	str	r2, [r7, #4]
 8006176:	603b      	str	r3, [r7, #0]
 8006178:	230b      	movs	r3, #11
 800617a:	18fb      	adds	r3, r7, r3
 800617c:	1c0a      	adds	r2, r1, #0
 800617e:	701a      	strb	r2, [r3, #0]
 8006180:	232f      	movs	r3, #47	; 0x2f
 8006182:	18fb      	adds	r3, r7, r3
 8006184:	2200      	movs	r2, #0
 8006186:	701a      	strb	r2, [r3, #0]
 8006188:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800618a:	61fb      	str	r3, [r7, #28]
 800618c:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800618e:	623b      	str	r3, [r7, #32]
 8006190:	2300      	movs	r3, #0
 8006192:	62bb      	str	r3, [r7, #40]	; 0x28
 8006194:	e02f      	b.n	80061f6 <enable_ref_spads+0x8a>
 8006196:	231c      	movs	r3, #28
 8006198:	18fb      	adds	r3, r7, r3
 800619a:	6a3a      	ldr	r2, [r7, #32]
 800619c:	6c39      	ldr	r1, [r7, #64]	; 0x40
 800619e:	6878      	ldr	r0, [r7, #4]
 80061a0:	f7ff fef8 	bl	8005f94 <get_next_good_spad>
 80061a4:	69fb      	ldr	r3, [r7, #28]
 80061a6:	3301      	adds	r3, #1
 80061a8:	d104      	bne.n	80061b4 <enable_ref_spads+0x48>
 80061aa:	232f      	movs	r3, #47	; 0x2f
 80061ac:	18fb      	adds	r3, r7, r3
 80061ae:	22ce      	movs	r2, #206	; 0xce
 80061b0:	701a      	strb	r2, [r3, #0]
 80061b2:	e024      	b.n	80061fe <enable_ref_spads+0x92>
 80061b4:	69fb      	ldr	r3, [r7, #28]
 80061b6:	001a      	movs	r2, r3
 80061b8:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 80061ba:	18d3      	adds	r3, r2, r3
 80061bc:	0018      	movs	r0, r3
 80061be:	f7ff ff51 	bl	8006064 <is_aperture>
 80061c2:	0003      	movs	r3, r0
 80061c4:	001a      	movs	r2, r3
 80061c6:	230b      	movs	r3, #11
 80061c8:	18fb      	adds	r3, r7, r3
 80061ca:	781b      	ldrb	r3, [r3, #0]
 80061cc:	4293      	cmp	r3, r2
 80061ce:	d004      	beq.n	80061da <enable_ref_spads+0x6e>
 80061d0:	232f      	movs	r3, #47	; 0x2f
 80061d2:	18fb      	adds	r3, r7, r3
 80061d4:	22ce      	movs	r2, #206	; 0xce
 80061d6:	701a      	strb	r2, [r3, #0]
 80061d8:	e011      	b.n	80061fe <enable_ref_spads+0x92>
 80061da:	69fb      	ldr	r3, [r7, #28]
 80061dc:	623b      	str	r3, [r7, #32]
 80061de:	6a3a      	ldr	r2, [r7, #32]
 80061e0:	6c39      	ldr	r1, [r7, #64]	; 0x40
 80061e2:	683b      	ldr	r3, [r7, #0]
 80061e4:	0018      	movs	r0, r3
 80061e6:	f7ff ff5b 	bl	80060a0 <enable_spad_bit>
 80061ea:	6a3b      	ldr	r3, [r7, #32]
 80061ec:	3301      	adds	r3, #1
 80061ee:	623b      	str	r3, [r7, #32]
 80061f0:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80061f2:	3301      	adds	r3, #1
 80061f4:	62bb      	str	r3, [r7, #40]	; 0x28
 80061f6:	6aba      	ldr	r2, [r7, #40]	; 0x28
 80061f8:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80061fa:	429a      	cmp	r2, r3
 80061fc:	d3cb      	bcc.n	8006196 <enable_ref_spads+0x2a>
 80061fe:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 8006200:	6a3a      	ldr	r2, [r7, #32]
 8006202:	601a      	str	r2, [r3, #0]
 8006204:	222f      	movs	r2, #47	; 0x2f
 8006206:	18bb      	adds	r3, r7, r2
 8006208:	781b      	ldrb	r3, [r3, #0]
 800620a:	b25b      	sxtb	r3, r3
 800620c:	2b00      	cmp	r3, #0
 800620e:	d108      	bne.n	8006222 <enable_ref_spads+0xb6>
 8006210:	18bc      	adds	r4, r7, r2
 8006212:	683a      	ldr	r2, [r7, #0]
 8006214:	68fb      	ldr	r3, [r7, #12]
 8006216:	0011      	movs	r1, r2
 8006218:	0018      	movs	r0, r3
 800621a:	f7ff ff7b 	bl	8006114 <set_ref_spad_map>
 800621e:	0003      	movs	r3, r0
 8006220:	7023      	strb	r3, [r4, #0]
 8006222:	222f      	movs	r2, #47	; 0x2f
 8006224:	18bb      	adds	r3, r7, r2
 8006226:	781b      	ldrb	r3, [r3, #0]
 8006228:	b25b      	sxtb	r3, r3
 800622a:	2b00      	cmp	r3, #0
 800622c:	d123      	bne.n	8006276 <enable_ref_spads+0x10a>
 800622e:	18bc      	adds	r4, r7, r2
 8006230:	2314      	movs	r3, #20
 8006232:	18fa      	adds	r2, r7, r3
 8006234:	68fb      	ldr	r3, [r7, #12]
 8006236:	0011      	movs	r1, r2
 8006238:	0018      	movs	r0, r3
 800623a:	f7ff ff81 	bl	8006140 <get_ref_spad_map>
 800623e:	0003      	movs	r3, r0
 8006240:	7023      	strb	r3, [r4, #0]
 8006242:	2300      	movs	r3, #0
 8006244:	627b      	str	r3, [r7, #36]	; 0x24
 8006246:	e012      	b.n	800626e <enable_ref_spads+0x102>
 8006248:	683a      	ldr	r2, [r7, #0]
 800624a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800624c:	18d3      	adds	r3, r2, r3
 800624e:	781a      	ldrb	r2, [r3, #0]
 8006250:	2314      	movs	r3, #20
 8006252:	18f9      	adds	r1, r7, r3
 8006254:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8006256:	18cb      	adds	r3, r1, r3
 8006258:	781b      	ldrb	r3, [r3, #0]
 800625a:	429a      	cmp	r2, r3
 800625c:	d004      	beq.n	8006268 <enable_ref_spads+0xfc>
 800625e:	232f      	movs	r3, #47	; 0x2f
 8006260:	18fb      	adds	r3, r7, r3
 8006262:	22ce      	movs	r2, #206	; 0xce
 8006264:	701a      	strb	r2, [r3, #0]
 8006266:	e006      	b.n	8006276 <enable_ref_spads+0x10a>
 8006268:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800626a:	3301      	adds	r3, #1
 800626c:	627b      	str	r3, [r7, #36]	; 0x24
 800626e:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8006270:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8006272:	429a      	cmp	r2, r3
 8006274:	d3e8      	bcc.n	8006248 <enable_ref_spads+0xdc>
 8006276:	232f      	movs	r3, #47	; 0x2f
 8006278:	18fb      	adds	r3, r7, r3
 800627a:	781b      	ldrb	r3, [r3, #0]
 800627c:	b25b      	sxtb	r3, r3
 800627e:	0018      	movs	r0, r3
 8006280:	46bd      	mov	sp, r7
 8006282:	b00d      	add	sp, #52	; 0x34
 8006284:	bd90      	pop	{r4, r7, pc}

08006286 <perform_ref_signal_measurement>:
 8006286:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006288:	b08b      	sub	sp, #44	; 0x2c
 800628a:	af00      	add	r7, sp, #0
 800628c:	6078      	str	r0, [r7, #4]
 800628e:	6039      	str	r1, [r7, #0]
 8006290:	2027      	movs	r0, #39	; 0x27
 8006292:	183b      	adds	r3, r7, r0
 8006294:	2200      	movs	r2, #0
 8006296:	701a      	strb	r2, [r3, #0]
 8006298:	2126      	movs	r1, #38	; 0x26
 800629a:	187b      	adds	r3, r7, r1
 800629c:	2200      	movs	r2, #0
 800629e:	701a      	strb	r2, [r3, #0]
 80062a0:	187b      	adds	r3, r7, r1
 80062a2:	6879      	ldr	r1, [r7, #4]
 80062a4:	2288      	movs	r2, #136	; 0x88
 80062a6:	0052      	lsls	r2, r2, #1
 80062a8:	5c8a      	ldrb	r2, [r1, r2]
 80062aa:	701a      	strb	r2, [r3, #0]
 80062ac:	183b      	adds	r3, r7, r0
 80062ae:	781b      	ldrb	r3, [r3, #0]
 80062b0:	b25b      	sxtb	r3, r3
 80062b2:	2b00      	cmp	r3, #0
 80062b4:	d108      	bne.n	80062c8 <perform_ref_signal_measurement+0x42>
 80062b6:	183c      	adds	r4, r7, r0
 80062b8:	687b      	ldr	r3, [r7, #4]
 80062ba:	22c0      	movs	r2, #192	; 0xc0
 80062bc:	2101      	movs	r1, #1
 80062be:	0018      	movs	r0, r3
 80062c0:	f7fd fb18 	bl	80038f4 <VL53L0X_WrByte>
 80062c4:	0003      	movs	r3, r0
 80062c6:	7023      	strb	r3, [r4, #0]
 80062c8:	2227      	movs	r2, #39	; 0x27
 80062ca:	18bb      	adds	r3, r7, r2
 80062cc:	781b      	ldrb	r3, [r3, #0]
 80062ce:	b25b      	sxtb	r3, r3
 80062d0:	2b00      	cmp	r3, #0
 80062d2:	d109      	bne.n	80062e8 <perform_ref_signal_measurement+0x62>
 80062d4:	18bc      	adds	r4, r7, r2
 80062d6:	2308      	movs	r3, #8
 80062d8:	18fa      	adds	r2, r7, r3
 80062da:	687b      	ldr	r3, [r7, #4]
 80062dc:	0011      	movs	r1, r2
 80062de:	0018      	movs	r0, r3
 80062e0:	f7ff fb58 	bl	8005994 <VL53L0X_PerformSingleRangingMeasurement>
 80062e4:	0003      	movs	r3, r0
 80062e6:	7023      	strb	r3, [r4, #0]
 80062e8:	2227      	movs	r2, #39	; 0x27
 80062ea:	18bb      	adds	r3, r7, r2
 80062ec:	781b      	ldrb	r3, [r3, #0]
 80062ee:	b25b      	sxtb	r3, r3
 80062f0:	2b00      	cmp	r3, #0
 80062f2:	d108      	bne.n	8006306 <perform_ref_signal_measurement+0x80>
 80062f4:	18bc      	adds	r4, r7, r2
 80062f6:	687b      	ldr	r3, [r7, #4]
 80062f8:	2201      	movs	r2, #1
 80062fa:	21ff      	movs	r1, #255	; 0xff
 80062fc:	0018      	movs	r0, r3
 80062fe:	f7fd faf9 	bl	80038f4 <VL53L0X_WrByte>
 8006302:	0003      	movs	r3, r0
 8006304:	7023      	strb	r3, [r4, #0]
 8006306:	2227      	movs	r2, #39	; 0x27
 8006308:	18bb      	adds	r3, r7, r2
 800630a:	781b      	ldrb	r3, [r3, #0]
 800630c:	b25b      	sxtb	r3, r3
 800630e:	2b00      	cmp	r3, #0
 8006310:	d108      	bne.n	8006324 <perform_ref_signal_measurement+0x9e>
 8006312:	18bc      	adds	r4, r7, r2
 8006314:	683a      	ldr	r2, [r7, #0]
 8006316:	687b      	ldr	r3, [r7, #4]
 8006318:	21b6      	movs	r1, #182	; 0xb6
 800631a:	0018      	movs	r0, r3
 800631c:	f7fd fb6d 	bl	80039fa <VL53L0X_RdWord>
 8006320:	0003      	movs	r3, r0
 8006322:	7023      	strb	r3, [r4, #0]
 8006324:	2227      	movs	r2, #39	; 0x27
 8006326:	18bb      	adds	r3, r7, r2
 8006328:	781b      	ldrb	r3, [r3, #0]
 800632a:	b25b      	sxtb	r3, r3
 800632c:	2b00      	cmp	r3, #0
 800632e:	d108      	bne.n	8006342 <perform_ref_signal_measurement+0xbc>
 8006330:	18bc      	adds	r4, r7, r2
 8006332:	687b      	ldr	r3, [r7, #4]
 8006334:	2200      	movs	r2, #0
 8006336:	21ff      	movs	r1, #255	; 0xff
 8006338:	0018      	movs	r0, r3
 800633a:	f7fd fadb 	bl	80038f4 <VL53L0X_WrByte>
 800633e:	0003      	movs	r3, r0
 8006340:	7023      	strb	r3, [r4, #0]
 8006342:	2527      	movs	r5, #39	; 0x27
 8006344:	197b      	adds	r3, r7, r5
 8006346:	781b      	ldrb	r3, [r3, #0]
 8006348:	b25b      	sxtb	r3, r3
 800634a:	2b00      	cmp	r3, #0
 800634c:	d115      	bne.n	800637a <perform_ref_signal_measurement+0xf4>
 800634e:	197c      	adds	r4, r7, r5
 8006350:	2626      	movs	r6, #38	; 0x26
 8006352:	19bb      	adds	r3, r7, r6
 8006354:	781a      	ldrb	r2, [r3, #0]
 8006356:	687b      	ldr	r3, [r7, #4]
 8006358:	2101      	movs	r1, #1
 800635a:	0018      	movs	r0, r3
 800635c:	f7fd faca 	bl	80038f4 <VL53L0X_WrByte>
 8006360:	0003      	movs	r3, r0
 8006362:	7023      	strb	r3, [r4, #0]
 8006364:	197b      	adds	r3, r7, r5
 8006366:	781b      	ldrb	r3, [r3, #0]
 8006368:	b25b      	sxtb	r3, r3
 800636a:	2b00      	cmp	r3, #0
 800636c:	d105      	bne.n	800637a <perform_ref_signal_measurement+0xf4>
 800636e:	687a      	ldr	r2, [r7, #4]
 8006370:	19b9      	adds	r1, r7, r6
 8006372:	2388      	movs	r3, #136	; 0x88
 8006374:	005b      	lsls	r3, r3, #1
 8006376:	7809      	ldrb	r1, [r1, #0]
 8006378:	54d1      	strb	r1, [r2, r3]
 800637a:	2327      	movs	r3, #39	; 0x27
 800637c:	18fb      	adds	r3, r7, r3
 800637e:	781b      	ldrb	r3, [r3, #0]
 8006380:	b25b      	sxtb	r3, r3
 8006382:	0018      	movs	r0, r3
 8006384:	46bd      	mov	sp, r7
 8006386:	b00b      	add	sp, #44	; 0x2c
 8006388:	bdf0      	pop	{r4, r5, r6, r7, pc}

0800638a <VL53L0X_perform_ref_spad_management>:
 800638a:	b5f0      	push	{r4, r5, r6, r7, lr}
 800638c:	b09d      	sub	sp, #116	; 0x74
 800638e:	af06      	add	r7, sp, #24
 8006390:	60f8      	str	r0, [r7, #12]
 8006392:	60b9      	str	r1, [r7, #8]
 8006394:	607a      	str	r2, [r7, #4]
 8006396:	2357      	movs	r3, #87	; 0x57
 8006398:	18fb      	adds	r3, r7, r3
 800639a:	2200      	movs	r2, #0
 800639c:	701a      	strb	r2, [r3, #0]
 800639e:	233a      	movs	r3, #58	; 0x3a
 80063a0:	18fb      	adds	r3, r7, r3
 80063a2:	22b4      	movs	r2, #180	; 0xb4
 80063a4:	701a      	strb	r2, [r3, #0]
 80063a6:	2303      	movs	r3, #3
 80063a8:	637b      	str	r3, [r7, #52]	; 0x34
 80063aa:	232c      	movs	r3, #44	; 0x2c
 80063ac:	633b      	str	r3, [r7, #48]	; 0x30
 80063ae:	2300      	movs	r3, #0
 80063b0:	653b      	str	r3, [r7, #80]	; 0x50
 80063b2:	2300      	movs	r3, #0
 80063b4:	61bb      	str	r3, [r7, #24]
 80063b6:	2300      	movs	r3, #0
 80063b8:	617b      	str	r3, [r7, #20]
 80063ba:	212e      	movs	r1, #46	; 0x2e
 80063bc:	187b      	adds	r3, r7, r1
 80063be:	22a0      	movs	r2, #160	; 0xa0
 80063c0:	0112      	lsls	r2, r2, #4
 80063c2:	801a      	strh	r2, [r3, #0]
 80063c4:	2300      	movs	r3, #0
 80063c6:	64fb      	str	r3, [r7, #76]	; 0x4c
 80063c8:	2300      	movs	r3, #0
 80063ca:	64bb      	str	r3, [r7, #72]	; 0x48
 80063cc:	2306      	movs	r3, #6
 80063ce:	62bb      	str	r3, [r7, #40]	; 0x28
 80063d0:	2300      	movs	r3, #0
 80063d2:	627b      	str	r3, [r7, #36]	; 0x24
 80063d4:	2300      	movs	r3, #0
 80063d6:	647b      	str	r3, [r7, #68]	; 0x44
 80063d8:	2343      	movs	r3, #67	; 0x43
 80063da:	18fb      	adds	r3, r7, r3
 80063dc:	2200      	movs	r2, #0
 80063de:	701a      	strb	r2, [r3, #0]
 80063e0:	2311      	movs	r3, #17
 80063e2:	18fb      	adds	r3, r7, r3
 80063e4:	2200      	movs	r2, #0
 80063e6:	701a      	strb	r2, [r3, #0]
 80063e8:	2310      	movs	r3, #16
 80063ea:	18fb      	adds	r3, r7, r3
 80063ec:	2200      	movs	r2, #0
 80063ee:	701a      	strb	r2, [r3, #0]
 80063f0:	2300      	movs	r3, #0
 80063f2:	63fb      	str	r3, [r7, #60]	; 0x3c
 80063f4:	233b      	movs	r3, #59	; 0x3b
 80063f6:	18fb      	adds	r3, r7, r3
 80063f8:	2200      	movs	r2, #0
 80063fa:	701a      	strb	r2, [r3, #0]
 80063fc:	187b      	adds	r3, r7, r1
 80063fe:	68f9      	ldr	r1, [r7, #12]
 8006400:	228e      	movs	r2, #142	; 0x8e
 8006402:	0052      	lsls	r2, r2, #1
 8006404:	5a8a      	ldrh	r2, [r1, r2]
 8006406:	801a      	strh	r2, [r3, #0]
 8006408:	2300      	movs	r3, #0
 800640a:	64bb      	str	r3, [r7, #72]	; 0x48
 800640c:	e00a      	b.n	8006424 <VL53L0X_perform_ref_spad_management+0x9a>
 800640e:	68f9      	ldr	r1, [r7, #12]
 8006410:	2382      	movs	r3, #130	; 0x82
 8006412:	005b      	lsls	r3, r3, #1
 8006414:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8006416:	188a      	adds	r2, r1, r2
 8006418:	18d3      	adds	r3, r2, r3
 800641a:	2200      	movs	r2, #0
 800641c:	701a      	strb	r2, [r3, #0]
 800641e:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8006420:	3301      	adds	r3, #1
 8006422:	64bb      	str	r3, [r7, #72]	; 0x48
 8006424:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8006426:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8006428:	429a      	cmp	r2, r3
 800642a:	d3f0      	bcc.n	800640e <VL53L0X_perform_ref_spad_management+0x84>
 800642c:	2557      	movs	r5, #87	; 0x57
 800642e:	197c      	adds	r4, r7, r5
 8006430:	68fb      	ldr	r3, [r7, #12]
 8006432:	2201      	movs	r2, #1
 8006434:	21ff      	movs	r1, #255	; 0xff
 8006436:	0018      	movs	r0, r3
 8006438:	f7fd fa5c 	bl	80038f4 <VL53L0X_WrByte>
 800643c:	0003      	movs	r3, r0
 800643e:	7023      	strb	r3, [r4, #0]
 8006440:	197b      	adds	r3, r7, r5
 8006442:	781b      	ldrb	r3, [r3, #0]
 8006444:	b25b      	sxtb	r3, r3
 8006446:	2b00      	cmp	r3, #0
 8006448:	d108      	bne.n	800645c <VL53L0X_perform_ref_spad_management+0xd2>
 800644a:	197c      	adds	r4, r7, r5
 800644c:	68fb      	ldr	r3, [r7, #12]
 800644e:	2200      	movs	r2, #0
 8006450:	214f      	movs	r1, #79	; 0x4f
 8006452:	0018      	movs	r0, r3
 8006454:	f7fd fa4e 	bl	80038f4 <VL53L0X_WrByte>
 8006458:	0003      	movs	r3, r0
 800645a:	7023      	strb	r3, [r4, #0]
 800645c:	2257      	movs	r2, #87	; 0x57
 800645e:	18bb      	adds	r3, r7, r2
 8006460:	781b      	ldrb	r3, [r3, #0]
 8006462:	b25b      	sxtb	r3, r3
 8006464:	2b00      	cmp	r3, #0
 8006466:	d108      	bne.n	800647a <VL53L0X_perform_ref_spad_management+0xf0>
 8006468:	18bc      	adds	r4, r7, r2
 800646a:	68fb      	ldr	r3, [r7, #12]
 800646c:	222c      	movs	r2, #44	; 0x2c
 800646e:	214e      	movs	r1, #78	; 0x4e
 8006470:	0018      	movs	r0, r3
 8006472:	f7fd fa3f 	bl	80038f4 <VL53L0X_WrByte>
 8006476:	0003      	movs	r3, r0
 8006478:	7023      	strb	r3, [r4, #0]
 800647a:	2257      	movs	r2, #87	; 0x57
 800647c:	18bb      	adds	r3, r7, r2
 800647e:	781b      	ldrb	r3, [r3, #0]
 8006480:	b25b      	sxtb	r3, r3
 8006482:	2b00      	cmp	r3, #0
 8006484:	d108      	bne.n	8006498 <VL53L0X_perform_ref_spad_management+0x10e>
 8006486:	18bc      	adds	r4, r7, r2
 8006488:	68fb      	ldr	r3, [r7, #12]
 800648a:	2200      	movs	r2, #0
 800648c:	21ff      	movs	r1, #255	; 0xff
 800648e:	0018      	movs	r0, r3
 8006490:	f7fd fa30 	bl	80038f4 <VL53L0X_WrByte>
 8006494:	0003      	movs	r3, r0
 8006496:	7023      	strb	r3, [r4, #0]
 8006498:	2257      	movs	r2, #87	; 0x57
 800649a:	18bb      	adds	r3, r7, r2
 800649c:	781b      	ldrb	r3, [r3, #0]
 800649e:	b25b      	sxtb	r3, r3
 80064a0:	2b00      	cmp	r3, #0
 80064a2:	d10a      	bne.n	80064ba <VL53L0X_perform_ref_spad_management+0x130>
 80064a4:	18bc      	adds	r4, r7, r2
 80064a6:	233a      	movs	r3, #58	; 0x3a
 80064a8:	18fb      	adds	r3, r7, r3
 80064aa:	781a      	ldrb	r2, [r3, #0]
 80064ac:	68fb      	ldr	r3, [r7, #12]
 80064ae:	21b6      	movs	r1, #182	; 0xb6
 80064b0:	0018      	movs	r0, r3
 80064b2:	f7fd fa1f 	bl	80038f4 <VL53L0X_WrByte>
 80064b6:	0003      	movs	r3, r0
 80064b8:	7023      	strb	r3, [r4, #0]
 80064ba:	2257      	movs	r2, #87	; 0x57
 80064bc:	18bb      	adds	r3, r7, r2
 80064be:	781b      	ldrb	r3, [r3, #0]
 80064c0:	b25b      	sxtb	r3, r3
 80064c2:	2b00      	cmp	r3, #0
 80064c4:	d108      	bne.n	80064d8 <VL53L0X_perform_ref_spad_management+0x14e>
 80064c6:	18bc      	adds	r4, r7, r2
 80064c8:	68fb      	ldr	r3, [r7, #12]
 80064ca:	2200      	movs	r2, #0
 80064cc:	2180      	movs	r1, #128	; 0x80
 80064ce:	0018      	movs	r0, r3
 80064d0:	f7fd fa10 	bl	80038f4 <VL53L0X_WrByte>
 80064d4:	0003      	movs	r3, r0
 80064d6:	7023      	strb	r3, [r4, #0]
 80064d8:	2257      	movs	r2, #87	; 0x57
 80064da:	18bb      	adds	r3, r7, r2
 80064dc:	781b      	ldrb	r3, [r3, #0]
 80064de:	b25b      	sxtb	r3, r3
 80064e0:	2b00      	cmp	r3, #0
 80064e2:	d10a      	bne.n	80064fa <VL53L0X_perform_ref_spad_management+0x170>
 80064e4:	18bc      	adds	r4, r7, r2
 80064e6:	2310      	movs	r3, #16
 80064e8:	18fa      	adds	r2, r7, r3
 80064ea:	2311      	movs	r3, #17
 80064ec:	18f9      	adds	r1, r7, r3
 80064ee:	68f8      	ldr	r0, [r7, #12]
 80064f0:	2300      	movs	r3, #0
 80064f2:	f000 fca4 	bl	8006e3e <VL53L0X_perform_ref_calibration>
 80064f6:	0003      	movs	r3, r0
 80064f8:	7023      	strb	r3, [r4, #0]
 80064fa:	2257      	movs	r2, #87	; 0x57
 80064fc:	18bb      	adds	r3, r7, r2
 80064fe:	781b      	ldrb	r3, [r3, #0]
 8006500:	b25b      	sxtb	r3, r3
 8006502:	2b00      	cmp	r3, #0
 8006504:	d123      	bne.n	800654e <VL53L0X_perform_ref_spad_management+0x1c4>
 8006506:	2300      	movs	r3, #0
 8006508:	653b      	str	r3, [r7, #80]	; 0x50
 800650a:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 800650c:	61bb      	str	r3, [r7, #24]
 800650e:	2300      	movs	r3, #0
 8006510:	64fb      	str	r3, [r7, #76]	; 0x4c
 8006512:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 8006514:	b2d9      	uxtb	r1, r3
 8006516:	68fb      	ldr	r3, [r7, #12]
 8006518:	330b      	adds	r3, #11
 800651a:	33ff      	adds	r3, #255	; 0xff
 800651c:	001d      	movs	r5, r3
 800651e:	68fb      	ldr	r3, [r7, #12]
 8006520:	1d5e      	adds	r6, r3, #5
 8006522:	36ff      	adds	r6, #255	; 0xff
 8006524:	233a      	movs	r3, #58	; 0x3a
 8006526:	18fb      	adds	r3, r7, r3
 8006528:	781b      	ldrb	r3, [r3, #0]
 800652a:	18bc      	adds	r4, r7, r2
 800652c:	68f8      	ldr	r0, [r7, #12]
 800652e:	2218      	movs	r2, #24
 8006530:	18ba      	adds	r2, r7, r2
 8006532:	9204      	str	r2, [sp, #16]
 8006534:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8006536:	9203      	str	r2, [sp, #12]
 8006538:	6d3a      	ldr	r2, [r7, #80]	; 0x50
 800653a:	9202      	str	r2, [sp, #8]
 800653c:	9301      	str	r3, [sp, #4]
 800653e:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8006540:	9300      	str	r3, [sp, #0]
 8006542:	0033      	movs	r3, r6
 8006544:	002a      	movs	r2, r5
 8006546:	f7ff fe11 	bl	800616c <enable_ref_spads>
 800654a:	0003      	movs	r3, r0
 800654c:	7023      	strb	r3, [r4, #0]
 800654e:	2557      	movs	r5, #87	; 0x57
 8006550:	197b      	adds	r3, r7, r5
 8006552:	781b      	ldrb	r3, [r3, #0]
 8006554:	b25b      	sxtb	r3, r3
 8006556:	2b00      	cmp	r3, #0
 8006558:	d000      	beq.n	800655c <VL53L0X_perform_ref_spad_management+0x1d2>
 800655a:	e086      	b.n	800666a <VL53L0X_perform_ref_spad_management+0x2e0>
 800655c:	69bb      	ldr	r3, [r7, #24]
 800655e:	653b      	str	r3, [r7, #80]	; 0x50
 8006560:	197c      	adds	r4, r7, r5
 8006562:	2612      	movs	r6, #18
 8006564:	19ba      	adds	r2, r7, r6
 8006566:	68fb      	ldr	r3, [r7, #12]
 8006568:	0011      	movs	r1, r2
 800656a:	0018      	movs	r0, r3
 800656c:	f7ff fe8b 	bl	8006286 <perform_ref_signal_measurement>
 8006570:	0003      	movs	r3, r0
 8006572:	7023      	strb	r3, [r4, #0]
 8006574:	197b      	adds	r3, r7, r5
 8006576:	781b      	ldrb	r3, [r3, #0]
 8006578:	b25b      	sxtb	r3, r3
 800657a:	2b00      	cmp	r3, #0
 800657c:	d000      	beq.n	8006580 <VL53L0X_perform_ref_spad_management+0x1f6>
 800657e:	e070      	b.n	8006662 <VL53L0X_perform_ref_spad_management+0x2d8>
 8006580:	19bb      	adds	r3, r7, r6
 8006582:	881b      	ldrh	r3, [r3, #0]
 8006584:	222e      	movs	r2, #46	; 0x2e
 8006586:	18ba      	adds	r2, r7, r2
 8006588:	8812      	ldrh	r2, [r2, #0]
 800658a:	429a      	cmp	r2, r3
 800658c:	d269      	bcs.n	8006662 <VL53L0X_perform_ref_spad_management+0x2d8>
 800658e:	2300      	movs	r3, #0
 8006590:	64bb      	str	r3, [r7, #72]	; 0x48
 8006592:	e00a      	b.n	80065aa <VL53L0X_perform_ref_spad_management+0x220>
 8006594:	68f9      	ldr	r1, [r7, #12]
 8006596:	2382      	movs	r3, #130	; 0x82
 8006598:	005b      	lsls	r3, r3, #1
 800659a:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800659c:	188a      	adds	r2, r1, r2
 800659e:	18d3      	adds	r3, r2, r3
 80065a0:	2200      	movs	r2, #0
 80065a2:	701a      	strb	r2, [r3, #0]
 80065a4:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 80065a6:	3301      	adds	r3, #1
 80065a8:	64bb      	str	r3, [r7, #72]	; 0x48
 80065aa:	6cba      	ldr	r2, [r7, #72]	; 0x48
 80065ac:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80065ae:	429a      	cmp	r2, r3
 80065b0:	d3f0      	bcc.n	8006594 <VL53L0X_perform_ref_spad_management+0x20a>
 80065b2:	e002      	b.n	80065ba <VL53L0X_perform_ref_spad_management+0x230>
 80065b4:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80065b6:	3301      	adds	r3, #1
 80065b8:	653b      	str	r3, [r7, #80]	; 0x50
 80065ba:	233a      	movs	r3, #58	; 0x3a
 80065bc:	18fb      	adds	r3, r7, r3
 80065be:	781a      	ldrb	r2, [r3, #0]
 80065c0:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80065c2:	18d3      	adds	r3, r2, r3
 80065c4:	0018      	movs	r0, r3
 80065c6:	f7ff fd4d 	bl	8006064 <is_aperture>
 80065ca:	1e03      	subs	r3, r0, #0
 80065cc:	d103      	bne.n	80065d6 <VL53L0X_perform_ref_spad_management+0x24c>
 80065ce:	6d3a      	ldr	r2, [r7, #80]	; 0x50
 80065d0:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80065d2:	429a      	cmp	r2, r3
 80065d4:	d3ee      	bcc.n	80065b4 <VL53L0X_perform_ref_spad_management+0x22a>
 80065d6:	2301      	movs	r3, #1
 80065d8:	64fb      	str	r3, [r7, #76]	; 0x4c
 80065da:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80065dc:	b2d9      	uxtb	r1, r3
 80065de:	68fb      	ldr	r3, [r7, #12]
 80065e0:	330b      	adds	r3, #11
 80065e2:	33ff      	adds	r3, #255	; 0xff
 80065e4:	001d      	movs	r5, r3
 80065e6:	68fb      	ldr	r3, [r7, #12]
 80065e8:	1d5e      	adds	r6, r3, #5
 80065ea:	36ff      	adds	r6, #255	; 0xff
 80065ec:	233a      	movs	r3, #58	; 0x3a
 80065ee:	18fb      	adds	r3, r7, r3
 80065f0:	781b      	ldrb	r3, [r3, #0]
 80065f2:	2257      	movs	r2, #87	; 0x57
 80065f4:	18bc      	adds	r4, r7, r2
 80065f6:	68f8      	ldr	r0, [r7, #12]
 80065f8:	2218      	movs	r2, #24
 80065fa:	18ba      	adds	r2, r7, r2
 80065fc:	9204      	str	r2, [sp, #16]
 80065fe:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8006600:	9203      	str	r2, [sp, #12]
 8006602:	6d3a      	ldr	r2, [r7, #80]	; 0x50
 8006604:	9202      	str	r2, [sp, #8]
 8006606:	9301      	str	r3, [sp, #4]
 8006608:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800660a:	9300      	str	r3, [sp, #0]
 800660c:	0033      	movs	r3, r6
 800660e:	002a      	movs	r2, r5
 8006610:	f7ff fdac 	bl	800616c <enable_ref_spads>
 8006614:	0003      	movs	r3, r0
 8006616:	7023      	strb	r3, [r4, #0]
 8006618:	2557      	movs	r5, #87	; 0x57
 800661a:	197b      	adds	r3, r7, r5
 800661c:	781b      	ldrb	r3, [r3, #0]
 800661e:	b25b      	sxtb	r3, r3
 8006620:	2b00      	cmp	r3, #0
 8006622:	d121      	bne.n	8006668 <VL53L0X_perform_ref_spad_management+0x2de>
 8006624:	69bb      	ldr	r3, [r7, #24]
 8006626:	653b      	str	r3, [r7, #80]	; 0x50
 8006628:	197c      	adds	r4, r7, r5
 800662a:	2612      	movs	r6, #18
 800662c:	19ba      	adds	r2, r7, r6
 800662e:	68fb      	ldr	r3, [r7, #12]
 8006630:	0011      	movs	r1, r2
 8006632:	0018      	movs	r0, r3
 8006634:	f7ff fe27 	bl	8006286 <perform_ref_signal_measurement>
 8006638:	0003      	movs	r3, r0
 800663a:	7023      	strb	r3, [r4, #0]
 800663c:	197b      	adds	r3, r7, r5
 800663e:	781b      	ldrb	r3, [r3, #0]
 8006640:	b25b      	sxtb	r3, r3
 8006642:	2b00      	cmp	r3, #0
 8006644:	d110      	bne.n	8006668 <VL53L0X_perform_ref_spad_management+0x2de>
 8006646:	19bb      	adds	r3, r7, r6
 8006648:	881b      	ldrh	r3, [r3, #0]
 800664a:	222e      	movs	r2, #46	; 0x2e
 800664c:	18ba      	adds	r2, r7, r2
 800664e:	8812      	ldrh	r2, [r2, #0]
 8006650:	429a      	cmp	r2, r3
 8006652:	d209      	bcs.n	8006668 <VL53L0X_perform_ref_spad_management+0x2de>
 8006654:	233b      	movs	r3, #59	; 0x3b
 8006656:	18fb      	adds	r3, r7, r3
 8006658:	2201      	movs	r2, #1
 800665a:	701a      	strb	r2, [r3, #0]
 800665c:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800665e:	63fb      	str	r3, [r7, #60]	; 0x3c
 8006660:	e002      	b.n	8006668 <VL53L0X_perform_ref_spad_management+0x2de>
 8006662:	2300      	movs	r3, #0
 8006664:	64fb      	str	r3, [r7, #76]	; 0x4c
 8006666:	e000      	b.n	800666a <VL53L0X_perform_ref_spad_management+0x2e0>
 8006668:	46c0      	nop			; (mov r8, r8)
 800666a:	2357      	movs	r3, #87	; 0x57
 800666c:	18fb      	adds	r3, r7, r3
 800666e:	781b      	ldrb	r3, [r3, #0]
 8006670:	b25b      	sxtb	r3, r3
 8006672:	2b00      	cmp	r3, #0
 8006674:	d000      	beq.n	8006678 <VL53L0X_perform_ref_spad_management+0x2ee>
 8006676:	e0cf      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 8006678:	2412      	movs	r4, #18
 800667a:	193b      	adds	r3, r7, r4
 800667c:	881b      	ldrh	r3, [r3, #0]
 800667e:	252e      	movs	r5, #46	; 0x2e
 8006680:	197a      	adds	r2, r7, r5
 8006682:	8812      	ldrh	r2, [r2, #0]
 8006684:	429a      	cmp	r2, r3
 8006686:	d800      	bhi.n	800668a <VL53L0X_perform_ref_spad_management+0x300>
 8006688:	e0c6      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 800668a:	233b      	movs	r3, #59	; 0x3b
 800668c:	18fb      	adds	r3, r7, r3
 800668e:	6cfa      	ldr	r2, [r7, #76]	; 0x4c
 8006690:	701a      	strb	r2, [r3, #0]
 8006692:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8006694:	63fb      	str	r3, [r7, #60]	; 0x3c
 8006696:	68fb      	ldr	r3, [r7, #12]
 8006698:	1d59      	adds	r1, r3, #5
 800669a:	31ff      	adds	r1, #255	; 0xff
 800669c:	6aba      	ldr	r2, [r7, #40]	; 0x28
 800669e:	231c      	movs	r3, #28
 80066a0:	18fb      	adds	r3, r7, r3
 80066a2:	0018      	movs	r0, r3
 80066a4:	f002 ff44 	bl	8009530 <memcpy>
 80066a8:	193b      	adds	r3, r7, r4
 80066aa:	881b      	ldrh	r3, [r3, #0]
 80066ac:	001a      	movs	r2, r3
 80066ae:	197b      	adds	r3, r7, r5
 80066b0:	881b      	ldrh	r3, [r3, #0]
 80066b2:	1ad3      	subs	r3, r2, r3
 80066b4:	17da      	asrs	r2, r3, #31
 80066b6:	189b      	adds	r3, r3, r2
 80066b8:	4053      	eors	r3, r2
 80066ba:	647b      	str	r3, [r7, #68]	; 0x44
 80066bc:	2343      	movs	r3, #67	; 0x43
 80066be:	18fb      	adds	r3, r7, r3
 80066c0:	2200      	movs	r2, #0
 80066c2:	701a      	strb	r2, [r3, #0]
 80066c4:	e09e      	b.n	8006804 <VL53L0X_perform_ref_spad_management+0x47a>
 80066c6:	68fb      	ldr	r3, [r7, #12]
 80066c8:	330b      	adds	r3, #11
 80066ca:	33ff      	adds	r3, #255	; 0xff
 80066cc:	0018      	movs	r0, r3
 80066ce:	2314      	movs	r3, #20
 80066d0:	18fb      	adds	r3, r7, r3
 80066d2:	6d3a      	ldr	r2, [r7, #80]	; 0x50
 80066d4:	6ab9      	ldr	r1, [r7, #40]	; 0x28
 80066d6:	f7ff fc5d 	bl	8005f94 <get_next_good_spad>
 80066da:	697b      	ldr	r3, [r7, #20]
 80066dc:	3301      	adds	r3, #1
 80066de:	d104      	bne.n	80066ea <VL53L0X_perform_ref_spad_management+0x360>
 80066e0:	2357      	movs	r3, #87	; 0x57
 80066e2:	18fb      	adds	r3, r7, r3
 80066e4:	22ce      	movs	r2, #206	; 0xce
 80066e6:	701a      	strb	r2, [r3, #0]
 80066e8:	e096      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 80066ea:	233a      	movs	r3, #58	; 0x3a
 80066ec:	18fb      	adds	r3, r7, r3
 80066ee:	781b      	ldrb	r3, [r3, #0]
 80066f0:	697a      	ldr	r2, [r7, #20]
 80066f2:	189b      	adds	r3, r3, r2
 80066f4:	0018      	movs	r0, r3
 80066f6:	f7ff fcb5 	bl	8006064 <is_aperture>
 80066fa:	0003      	movs	r3, r0
 80066fc:	001a      	movs	r2, r3
 80066fe:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 8006700:	4293      	cmp	r3, r2
 8006702:	d004      	beq.n	800670e <VL53L0X_perform_ref_spad_management+0x384>
 8006704:	2343      	movs	r3, #67	; 0x43
 8006706:	18fb      	adds	r3, r7, r3
 8006708:	2201      	movs	r2, #1
 800670a:	701a      	strb	r2, [r3, #0]
 800670c:	e084      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 800670e:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8006710:	3301      	adds	r3, #1
 8006712:	63fb      	str	r3, [r7, #60]	; 0x3c
 8006714:	697b      	ldr	r3, [r7, #20]
 8006716:	653b      	str	r3, [r7, #80]	; 0x50
 8006718:	68fb      	ldr	r3, [r7, #12]
 800671a:	3305      	adds	r3, #5
 800671c:	33ff      	adds	r3, #255	; 0xff
 800671e:	2557      	movs	r5, #87	; 0x57
 8006720:	197c      	adds	r4, r7, r5
 8006722:	6d3a      	ldr	r2, [r7, #80]	; 0x50
 8006724:	6ab9      	ldr	r1, [r7, #40]	; 0x28
 8006726:	0018      	movs	r0, r3
 8006728:	f7ff fcba 	bl	80060a0 <enable_spad_bit>
 800672c:	0003      	movs	r3, r0
 800672e:	7023      	strb	r3, [r4, #0]
 8006730:	0029      	movs	r1, r5
 8006732:	187b      	adds	r3, r7, r1
 8006734:	781b      	ldrb	r3, [r3, #0]
 8006736:	b25b      	sxtb	r3, r3
 8006738:	2b00      	cmp	r3, #0
 800673a:	d10d      	bne.n	8006758 <VL53L0X_perform_ref_spad_management+0x3ce>
 800673c:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 800673e:	3301      	adds	r3, #1
 8006740:	653b      	str	r3, [r7, #80]	; 0x50
 8006742:	68fb      	ldr	r3, [r7, #12]
 8006744:	1d5a      	adds	r2, r3, #5
 8006746:	32ff      	adds	r2, #255	; 0xff
 8006748:	187c      	adds	r4, r7, r1
 800674a:	68fb      	ldr	r3, [r7, #12]
 800674c:	0011      	movs	r1, r2
 800674e:	0018      	movs	r0, r3
 8006750:	f7ff fce0 	bl	8006114 <set_ref_spad_map>
 8006754:	0003      	movs	r3, r0
 8006756:	7023      	strb	r3, [r4, #0]
 8006758:	2557      	movs	r5, #87	; 0x57
 800675a:	197b      	adds	r3, r7, r5
 800675c:	781b      	ldrb	r3, [r3, #0]
 800675e:	b25b      	sxtb	r3, r3
 8006760:	2b00      	cmp	r3, #0
 8006762:	d156      	bne.n	8006812 <VL53L0X_perform_ref_spad_management+0x488>
 8006764:	197c      	adds	r4, r7, r5
 8006766:	2612      	movs	r6, #18
 8006768:	19ba      	adds	r2, r7, r6
 800676a:	68fb      	ldr	r3, [r7, #12]
 800676c:	0011      	movs	r1, r2
 800676e:	0018      	movs	r0, r3
 8006770:	f7ff fd89 	bl	8006286 <perform_ref_signal_measurement>
 8006774:	0003      	movs	r3, r0
 8006776:	7023      	strb	r3, [r4, #0]
 8006778:	0029      	movs	r1, r5
 800677a:	187b      	adds	r3, r7, r1
 800677c:	781b      	ldrb	r3, [r3, #0]
 800677e:	b25b      	sxtb	r3, r3
 8006780:	2b00      	cmp	r3, #0
 8006782:	d148      	bne.n	8006816 <VL53L0X_perform_ref_spad_management+0x48c>
 8006784:	0030      	movs	r0, r6
 8006786:	183b      	adds	r3, r7, r0
 8006788:	881b      	ldrh	r3, [r3, #0]
 800678a:	001a      	movs	r2, r3
 800678c:	242e      	movs	r4, #46	; 0x2e
 800678e:	193b      	adds	r3, r7, r4
 8006790:	881b      	ldrh	r3, [r3, #0]
 8006792:	1ad3      	subs	r3, r2, r3
 8006794:	17da      	asrs	r2, r3, #31
 8006796:	189b      	adds	r3, r3, r2
 8006798:	4053      	eors	r3, r2
 800679a:	627b      	str	r3, [r7, #36]	; 0x24
 800679c:	183b      	adds	r3, r7, r0
 800679e:	881b      	ldrh	r3, [r3, #0]
 80067a0:	193a      	adds	r2, r7, r4
 80067a2:	8812      	ldrh	r2, [r2, #0]
 80067a4:	429a      	cmp	r2, r3
 80067a6:	d21d      	bcs.n	80067e4 <VL53L0X_perform_ref_spad_management+0x45a>
 80067a8:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80067aa:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 80067ac:	429a      	cmp	r2, r3
 80067ae:	d914      	bls.n	80067da <VL53L0X_perform_ref_spad_management+0x450>
 80067b0:	187c      	adds	r4, r7, r1
 80067b2:	251c      	movs	r5, #28
 80067b4:	197a      	adds	r2, r7, r5
 80067b6:	68fb      	ldr	r3, [r7, #12]
 80067b8:	0011      	movs	r1, r2
 80067ba:	0018      	movs	r0, r3
 80067bc:	f7ff fcaa 	bl	8006114 <set_ref_spad_map>
 80067c0:	0003      	movs	r3, r0
 80067c2:	7023      	strb	r3, [r4, #0]
 80067c4:	68fb      	ldr	r3, [r7, #12]
 80067c6:	3305      	adds	r3, #5
 80067c8:	33ff      	adds	r3, #255	; 0xff
 80067ca:	6aba      	ldr	r2, [r7, #40]	; 0x28
 80067cc:	1979      	adds	r1, r7, r5
 80067ce:	0018      	movs	r0, r3
 80067d0:	f002 feae 	bl	8009530 <memcpy>
 80067d4:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 80067d6:	3b01      	subs	r3, #1
 80067d8:	63fb      	str	r3, [r7, #60]	; 0x3c
 80067da:	2343      	movs	r3, #67	; 0x43
 80067dc:	18fb      	adds	r3, r7, r3
 80067de:	2201      	movs	r2, #1
 80067e0:	701a      	strb	r2, [r3, #0]
 80067e2:	e00f      	b.n	8006804 <VL53L0X_perform_ref_spad_management+0x47a>
 80067e4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80067e6:	647b      	str	r3, [r7, #68]	; 0x44
 80067e8:	68fb      	ldr	r3, [r7, #12]
 80067ea:	1d59      	adds	r1, r3, #5
 80067ec:	31ff      	adds	r1, #255	; 0xff
 80067ee:	6aba      	ldr	r2, [r7, #40]	; 0x28
 80067f0:	231c      	movs	r3, #28
 80067f2:	18fb      	adds	r3, r7, r3
 80067f4:	0018      	movs	r0, r3
 80067f6:	f002 fe9b 	bl	8009530 <memcpy>
 80067fa:	2243      	movs	r2, #67	; 0x43
 80067fc:	18bb      	adds	r3, r7, r2
 80067fe:	18ba      	adds	r2, r7, r2
 8006800:	7812      	ldrb	r2, [r2, #0]
 8006802:	701a      	strb	r2, [r3, #0]
 8006804:	2343      	movs	r3, #67	; 0x43
 8006806:	18fb      	adds	r3, r7, r3
 8006808:	781b      	ldrb	r3, [r3, #0]
 800680a:	2b00      	cmp	r3, #0
 800680c:	d100      	bne.n	8006810 <VL53L0X_perform_ref_spad_management+0x486>
 800680e:	e75a      	b.n	80066c6 <VL53L0X_perform_ref_spad_management+0x33c>
 8006810:	e002      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 8006812:	46c0      	nop			; (mov r8, r8)
 8006814:	e000      	b.n	8006818 <VL53L0X_perform_ref_spad_management+0x48e>
 8006816:	46c0      	nop			; (mov r8, r8)
 8006818:	2357      	movs	r3, #87	; 0x57
 800681a:	18fb      	adds	r3, r7, r3
 800681c:	781b      	ldrb	r3, [r3, #0]
 800681e:	b25b      	sxtb	r3, r3
 8006820:	2b00      	cmp	r3, #0
 8006822:	d116      	bne.n	8006852 <VL53L0X_perform_ref_spad_management+0x4c8>
 8006824:	68bb      	ldr	r3, [r7, #8]
 8006826:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 8006828:	601a      	str	r2, [r3, #0]
 800682a:	687b      	ldr	r3, [r7, #4]
 800682c:	223b      	movs	r2, #59	; 0x3b
 800682e:	18ba      	adds	r2, r7, r2
 8006830:	7812      	ldrb	r2, [r2, #0]
 8006832:	701a      	strb	r2, [r3, #0]
 8006834:	68fb      	ldr	r3, [r7, #12]
 8006836:	22f5      	movs	r2, #245	; 0xf5
 8006838:	2101      	movs	r1, #1
 800683a:	5499      	strb	r1, [r3, r2]
 800683c:	68bb      	ldr	r3, [r7, #8]
 800683e:	681b      	ldr	r3, [r3, #0]
 8006840:	b2d9      	uxtb	r1, r3
 8006842:	68fb      	ldr	r3, [r7, #12]
 8006844:	22f3      	movs	r2, #243	; 0xf3
 8006846:	5499      	strb	r1, [r3, r2]
 8006848:	687b      	ldr	r3, [r7, #4]
 800684a:	7819      	ldrb	r1, [r3, #0]
 800684c:	68fb      	ldr	r3, [r7, #12]
 800684e:	22f4      	movs	r2, #244	; 0xf4
 8006850:	5499      	strb	r1, [r3, r2]
 8006852:	2357      	movs	r3, #87	; 0x57
 8006854:	18fb      	adds	r3, r7, r3
 8006856:	781b      	ldrb	r3, [r3, #0]
 8006858:	b25b      	sxtb	r3, r3
 800685a:	0018      	movs	r0, r3
 800685c:	46bd      	mov	sp, r7
 800685e:	b017      	add	sp, #92	; 0x5c
 8006860:	bdf0      	pop	{r4, r5, r6, r7, pc}

08006862 <VL53L0X_set_reference_spads>:
 8006862:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006864:	b093      	sub	sp, #76	; 0x4c
 8006866:	af06      	add	r7, sp, #24
 8006868:	60f8      	str	r0, [r7, #12]
 800686a:	60b9      	str	r1, [r7, #8]
 800686c:	1dfb      	adds	r3, r7, #7
 800686e:	701a      	strb	r2, [r3, #0]
 8006870:	212f      	movs	r1, #47	; 0x2f
 8006872:	187b      	adds	r3, r7, r1
 8006874:	2200      	movs	r2, #0
 8006876:	701a      	strb	r2, [r3, #0]
 8006878:	2300      	movs	r3, #0
 800687a:	62bb      	str	r3, [r7, #40]	; 0x28
 800687c:	2323      	movs	r3, #35	; 0x23
 800687e:	18fb      	adds	r3, r7, r3
 8006880:	22b4      	movs	r2, #180	; 0xb4
 8006882:	701a      	strb	r2, [r3, #0]
 8006884:	2306      	movs	r3, #6
 8006886:	61fb      	str	r3, [r7, #28]
 8006888:	232c      	movs	r3, #44	; 0x2c
 800688a:	61bb      	str	r3, [r7, #24]
 800688c:	000d      	movs	r5, r1
 800688e:	187c      	adds	r4, r7, r1
 8006890:	68fb      	ldr	r3, [r7, #12]
 8006892:	2201      	movs	r2, #1
 8006894:	21ff      	movs	r1, #255	; 0xff
 8006896:	0018      	movs	r0, r3
 8006898:	f7fd f82c 	bl	80038f4 <VL53L0X_WrByte>
 800689c:	0003      	movs	r3, r0
 800689e:	7023      	strb	r3, [r4, #0]
 80068a0:	197b      	adds	r3, r7, r5
 80068a2:	781b      	ldrb	r3, [r3, #0]
 80068a4:	b25b      	sxtb	r3, r3
 80068a6:	2b00      	cmp	r3, #0
 80068a8:	d108      	bne.n	80068bc <VL53L0X_set_reference_spads+0x5a>
 80068aa:	197c      	adds	r4, r7, r5
 80068ac:	68fb      	ldr	r3, [r7, #12]
 80068ae:	2200      	movs	r2, #0
 80068b0:	214f      	movs	r1, #79	; 0x4f
 80068b2:	0018      	movs	r0, r3
 80068b4:	f7fd f81e 	bl	80038f4 <VL53L0X_WrByte>
 80068b8:	0003      	movs	r3, r0
 80068ba:	7023      	strb	r3, [r4, #0]
 80068bc:	222f      	movs	r2, #47	; 0x2f
 80068be:	18bb      	adds	r3, r7, r2
 80068c0:	781b      	ldrb	r3, [r3, #0]
 80068c2:	b25b      	sxtb	r3, r3
 80068c4:	2b00      	cmp	r3, #0
 80068c6:	d108      	bne.n	80068da <VL53L0X_set_reference_spads+0x78>
 80068c8:	18bc      	adds	r4, r7, r2
 80068ca:	68fb      	ldr	r3, [r7, #12]
 80068cc:	222c      	movs	r2, #44	; 0x2c
 80068ce:	214e      	movs	r1, #78	; 0x4e
 80068d0:	0018      	movs	r0, r3
 80068d2:	f7fd f80f 	bl	80038f4 <VL53L0X_WrByte>
 80068d6:	0003      	movs	r3, r0
 80068d8:	7023      	strb	r3, [r4, #0]
 80068da:	222f      	movs	r2, #47	; 0x2f
 80068dc:	18bb      	adds	r3, r7, r2
 80068de:	781b      	ldrb	r3, [r3, #0]
 80068e0:	b25b      	sxtb	r3, r3
 80068e2:	2b00      	cmp	r3, #0
 80068e4:	d108      	bne.n	80068f8 <VL53L0X_set_reference_spads+0x96>
 80068e6:	18bc      	adds	r4, r7, r2
 80068e8:	68fb      	ldr	r3, [r7, #12]
 80068ea:	2200      	movs	r2, #0
 80068ec:	21ff      	movs	r1, #255	; 0xff
 80068ee:	0018      	movs	r0, r3
 80068f0:	f7fd f800 	bl	80038f4 <VL53L0X_WrByte>
 80068f4:	0003      	movs	r3, r0
 80068f6:	7023      	strb	r3, [r4, #0]
 80068f8:	222f      	movs	r2, #47	; 0x2f
 80068fa:	18bb      	adds	r3, r7, r2
 80068fc:	781b      	ldrb	r3, [r3, #0]
 80068fe:	b25b      	sxtb	r3, r3
 8006900:	2b00      	cmp	r3, #0
 8006902:	d10a      	bne.n	800691a <VL53L0X_set_reference_spads+0xb8>
 8006904:	18bc      	adds	r4, r7, r2
 8006906:	2323      	movs	r3, #35	; 0x23
 8006908:	18fb      	adds	r3, r7, r3
 800690a:	781a      	ldrb	r2, [r3, #0]
 800690c:	68fb      	ldr	r3, [r7, #12]
 800690e:	21b6      	movs	r1, #182	; 0xb6
 8006910:	0018      	movs	r0, r3
 8006912:	f7fc ffef 	bl	80038f4 <VL53L0X_WrByte>
 8006916:	0003      	movs	r3, r0
 8006918:	7023      	strb	r3, [r4, #0]
 800691a:	2300      	movs	r3, #0
 800691c:	627b      	str	r3, [r7, #36]	; 0x24
 800691e:	e00a      	b.n	8006936 <VL53L0X_set_reference_spads+0xd4>
 8006920:	68f9      	ldr	r1, [r7, #12]
 8006922:	2382      	movs	r3, #130	; 0x82
 8006924:	005b      	lsls	r3, r3, #1
 8006926:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8006928:	188a      	adds	r2, r1, r2
 800692a:	18d3      	adds	r3, r2, r3
 800692c:	2200      	movs	r2, #0
 800692e:	701a      	strb	r2, [r3, #0]
 8006930:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8006932:	3301      	adds	r3, #1
 8006934:	627b      	str	r3, [r7, #36]	; 0x24
 8006936:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8006938:	69fb      	ldr	r3, [r7, #28]
 800693a:	429a      	cmp	r2, r3
 800693c:	d3f0      	bcc.n	8006920 <VL53L0X_set_reference_spads+0xbe>
 800693e:	1dfb      	adds	r3, r7, #7
 8006940:	781b      	ldrb	r3, [r3, #0]
 8006942:	2b00      	cmp	r3, #0
 8006944:	d011      	beq.n	800696a <VL53L0X_set_reference_spads+0x108>
 8006946:	e002      	b.n	800694e <VL53L0X_set_reference_spads+0xec>
 8006948:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800694a:	3301      	adds	r3, #1
 800694c:	62bb      	str	r3, [r7, #40]	; 0x28
 800694e:	2323      	movs	r3, #35	; 0x23
 8006950:	18fb      	adds	r3, r7, r3
 8006952:	781a      	ldrb	r2, [r3, #0]
 8006954:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8006956:	18d3      	adds	r3, r2, r3
 8006958:	0018      	movs	r0, r3
 800695a:	f7ff fb83 	bl	8006064 <is_aperture>
 800695e:	1e03      	subs	r3, r0, #0
 8006960:	d103      	bne.n	800696a <VL53L0X_set_reference_spads+0x108>
 8006962:	6aba      	ldr	r2, [r7, #40]	; 0x28
 8006964:	69bb      	ldr	r3, [r7, #24]
 8006966:	429a      	cmp	r2, r3
 8006968:	d3ee      	bcc.n	8006948 <VL53L0X_set_reference_spads+0xe6>
 800696a:	68fb      	ldr	r3, [r7, #12]
 800696c:	330b      	adds	r3, #11
 800696e:	33ff      	adds	r3, #255	; 0xff
 8006970:	001d      	movs	r5, r3
 8006972:	68fb      	ldr	r3, [r7, #12]
 8006974:	1d5e      	adds	r6, r3, #5
 8006976:	36ff      	adds	r6, #255	; 0xff
 8006978:	2323      	movs	r3, #35	; 0x23
 800697a:	18fb      	adds	r3, r7, r3
 800697c:	781b      	ldrb	r3, [r3, #0]
 800697e:	222f      	movs	r2, #47	; 0x2f
 8006980:	18bc      	adds	r4, r7, r2
 8006982:	1dfa      	adds	r2, r7, #7
 8006984:	7811      	ldrb	r1, [r2, #0]
 8006986:	68f8      	ldr	r0, [r7, #12]
 8006988:	2214      	movs	r2, #20
 800698a:	18ba      	adds	r2, r7, r2
 800698c:	9204      	str	r2, [sp, #16]
 800698e:	68ba      	ldr	r2, [r7, #8]
 8006990:	9203      	str	r2, [sp, #12]
 8006992:	6aba      	ldr	r2, [r7, #40]	; 0x28
 8006994:	9202      	str	r2, [sp, #8]
 8006996:	9301      	str	r3, [sp, #4]
 8006998:	69fb      	ldr	r3, [r7, #28]
 800699a:	9300      	str	r3, [sp, #0]
 800699c:	0033      	movs	r3, r6
 800699e:	002a      	movs	r2, r5
 80069a0:	f7ff fbe4 	bl	800616c <enable_ref_spads>
 80069a4:	0003      	movs	r3, r0
 80069a6:	7023      	strb	r3, [r4, #0]
 80069a8:	222f      	movs	r2, #47	; 0x2f
 80069aa:	18bb      	adds	r3, r7, r2
 80069ac:	781b      	ldrb	r3, [r3, #0]
 80069ae:	b25b      	sxtb	r3, r3
 80069b0:	2b00      	cmp	r3, #0
 80069b2:	d10d      	bne.n	80069d0 <VL53L0X_set_reference_spads+0x16e>
 80069b4:	68fb      	ldr	r3, [r7, #12]
 80069b6:	22f5      	movs	r2, #245	; 0xf5
 80069b8:	2101      	movs	r1, #1
 80069ba:	5499      	strb	r1, [r3, r2]
 80069bc:	68bb      	ldr	r3, [r7, #8]
 80069be:	b2d9      	uxtb	r1, r3
 80069c0:	68fb      	ldr	r3, [r7, #12]
 80069c2:	22f3      	movs	r2, #243	; 0xf3
 80069c4:	5499      	strb	r1, [r3, r2]
 80069c6:	68fb      	ldr	r3, [r7, #12]
 80069c8:	1dfa      	adds	r2, r7, #7
 80069ca:	21f4      	movs	r1, #244	; 0xf4
 80069cc:	7812      	ldrb	r2, [r2, #0]
 80069ce:	545a      	strb	r2, [r3, r1]
 80069d0:	232f      	movs	r3, #47	; 0x2f
 80069d2:	18fb      	adds	r3, r7, r3
 80069d4:	781b      	ldrb	r3, [r3, #0]
 80069d6:	b25b      	sxtb	r3, r3
 80069d8:	0018      	movs	r0, r3
 80069da:	46bd      	mov	sp, r7
 80069dc:	b00d      	add	sp, #52	; 0x34
 80069de:	bdf0      	pop	{r4, r5, r6, r7, pc}

080069e0 <VL53L0X_perform_single_ref_calibration>:
 80069e0:	b590      	push	{r4, r7, lr}
 80069e2:	b085      	sub	sp, #20
 80069e4:	af00      	add	r7, sp, #0
 80069e6:	6078      	str	r0, [r7, #4]
 80069e8:	000a      	movs	r2, r1
 80069ea:	1cfb      	adds	r3, r7, #3
 80069ec:	701a      	strb	r2, [r3, #0]
 80069ee:	210f      	movs	r1, #15
 80069f0:	187b      	adds	r3, r7, r1
 80069f2:	2200      	movs	r2, #0
 80069f4:	701a      	strb	r2, [r3, #0]
 80069f6:	187b      	adds	r3, r7, r1
 80069f8:	781b      	ldrb	r3, [r3, #0]
 80069fa:	b25b      	sxtb	r3, r3
 80069fc:	2b00      	cmp	r3, #0
 80069fe:	d10c      	bne.n	8006a1a <VL53L0X_perform_single_ref_calibration+0x3a>
 8006a00:	1cfb      	adds	r3, r7, #3
 8006a02:	781b      	ldrb	r3, [r3, #0]
 8006a04:	2201      	movs	r2, #1
 8006a06:	4313      	orrs	r3, r2
 8006a08:	b2da      	uxtb	r2, r3
 8006a0a:	187c      	adds	r4, r7, r1
 8006a0c:	687b      	ldr	r3, [r7, #4]
 8006a0e:	2100      	movs	r1, #0
 8006a10:	0018      	movs	r0, r3
 8006a12:	f7fc ff6f 	bl	80038f4 <VL53L0X_WrByte>
 8006a16:	0003      	movs	r3, r0
 8006a18:	7023      	strb	r3, [r4, #0]
 8006a1a:	220f      	movs	r2, #15
 8006a1c:	18bb      	adds	r3, r7, r2
 8006a1e:	781b      	ldrb	r3, [r3, #0]
 8006a20:	b25b      	sxtb	r3, r3
 8006a22:	2b00      	cmp	r3, #0
 8006a24:	d106      	bne.n	8006a34 <VL53L0X_perform_single_ref_calibration+0x54>
 8006a26:	18bc      	adds	r4, r7, r2
 8006a28:	687b      	ldr	r3, [r7, #4]
 8006a2a:	0018      	movs	r0, r3
 8006a2c:	f000 fa5c 	bl	8006ee8 <VL53L0X_measurement_poll_for_completion>
 8006a30:	0003      	movs	r3, r0
 8006a32:	7023      	strb	r3, [r4, #0]
 8006a34:	220f      	movs	r2, #15
 8006a36:	18bb      	adds	r3, r7, r2
 8006a38:	781b      	ldrb	r3, [r3, #0]
 8006a3a:	b25b      	sxtb	r3, r3
 8006a3c:	2b00      	cmp	r3, #0
 8006a3e:	d107      	bne.n	8006a50 <VL53L0X_perform_single_ref_calibration+0x70>
 8006a40:	18bc      	adds	r4, r7, r2
 8006a42:	687b      	ldr	r3, [r7, #4]
 8006a44:	2100      	movs	r1, #0
 8006a46:	0018      	movs	r0, r3
 8006a48:	f7ff f9b0 	bl	8005dac <VL53L0X_ClearInterruptMask>
 8006a4c:	0003      	movs	r3, r0
 8006a4e:	7023      	strb	r3, [r4, #0]
 8006a50:	220f      	movs	r2, #15
 8006a52:	18bb      	adds	r3, r7, r2
 8006a54:	781b      	ldrb	r3, [r3, #0]
 8006a56:	b25b      	sxtb	r3, r3
 8006a58:	2b00      	cmp	r3, #0
 8006a5a:	d108      	bne.n	8006a6e <VL53L0X_perform_single_ref_calibration+0x8e>
 8006a5c:	18bc      	adds	r4, r7, r2
 8006a5e:	687b      	ldr	r3, [r7, #4]
 8006a60:	2200      	movs	r2, #0
 8006a62:	2100      	movs	r1, #0
 8006a64:	0018      	movs	r0, r3
 8006a66:	f7fc ff45 	bl	80038f4 <VL53L0X_WrByte>
 8006a6a:	0003      	movs	r3, r0
 8006a6c:	7023      	strb	r3, [r4, #0]
 8006a6e:	230f      	movs	r3, #15
 8006a70:	18fb      	adds	r3, r7, r3
 8006a72:	781b      	ldrb	r3, [r3, #0]
 8006a74:	b25b      	sxtb	r3, r3
 8006a76:	0018      	movs	r0, r3
 8006a78:	46bd      	mov	sp, r7
 8006a7a:	b005      	add	sp, #20
 8006a7c:	bd90      	pop	{r4, r7, pc}

08006a7e <VL53L0X_ref_calibration_io>:
 8006a7e:	b590      	push	{r4, r7, lr}
 8006a80:	b085      	sub	sp, #20
 8006a82:	af00      	add	r7, sp, #0
 8006a84:	6078      	str	r0, [r7, #4]
 8006a86:	000c      	movs	r4, r1
 8006a88:	0010      	movs	r0, r2
 8006a8a:	0019      	movs	r1, r3
 8006a8c:	1cfb      	adds	r3, r7, #3
 8006a8e:	1c22      	adds	r2, r4, #0
 8006a90:	701a      	strb	r2, [r3, #0]
 8006a92:	1cbb      	adds	r3, r7, #2
 8006a94:	1c02      	adds	r2, r0, #0
 8006a96:	701a      	strb	r2, [r3, #0]
 8006a98:	1c7b      	adds	r3, r7, #1
 8006a9a:	1c0a      	adds	r2, r1, #0
 8006a9c:	701a      	strb	r2, [r3, #0]
 8006a9e:	240f      	movs	r4, #15
 8006aa0:	193b      	adds	r3, r7, r4
 8006aa2:	2200      	movs	r2, #0
 8006aa4:	701a      	strb	r2, [r3, #0]
 8006aa6:	230e      	movs	r3, #14
 8006aa8:	18fb      	adds	r3, r7, r3
 8006aaa:	2200      	movs	r2, #0
 8006aac:	701a      	strb	r2, [r3, #0]
 8006aae:	687b      	ldr	r3, [r7, #4]
 8006ab0:	2201      	movs	r2, #1
 8006ab2:	21ff      	movs	r1, #255	; 0xff
 8006ab4:	0018      	movs	r0, r3
 8006ab6:	f7fc ff1d 	bl	80038f4 <VL53L0X_WrByte>
 8006aba:	0003      	movs	r3, r0
 8006abc:	0019      	movs	r1, r3
 8006abe:	193b      	adds	r3, r7, r4
 8006ac0:	193a      	adds	r2, r7, r4
 8006ac2:	7812      	ldrb	r2, [r2, #0]
 8006ac4:	430a      	orrs	r2, r1
 8006ac6:	701a      	strb	r2, [r3, #0]
 8006ac8:	687b      	ldr	r3, [r7, #4]
 8006aca:	2200      	movs	r2, #0
 8006acc:	2100      	movs	r1, #0
 8006ace:	0018      	movs	r0, r3
 8006ad0:	f7fc ff10 	bl	80038f4 <VL53L0X_WrByte>
 8006ad4:	0003      	movs	r3, r0
 8006ad6:	0019      	movs	r1, r3
 8006ad8:	193b      	adds	r3, r7, r4
 8006ada:	193a      	adds	r2, r7, r4
 8006adc:	7812      	ldrb	r2, [r2, #0]
 8006ade:	430a      	orrs	r2, r1
 8006ae0:	701a      	strb	r2, [r3, #0]
 8006ae2:	687b      	ldr	r3, [r7, #4]
 8006ae4:	2200      	movs	r2, #0
 8006ae6:	21ff      	movs	r1, #255	; 0xff
 8006ae8:	0018      	movs	r0, r3
 8006aea:	f7fc ff03 	bl	80038f4 <VL53L0X_WrByte>
 8006aee:	0003      	movs	r3, r0
 8006af0:	0019      	movs	r1, r3
 8006af2:	193b      	adds	r3, r7, r4
 8006af4:	193a      	adds	r2, r7, r4
 8006af6:	7812      	ldrb	r2, [r2, #0]
 8006af8:	430a      	orrs	r2, r1
 8006afa:	701a      	strb	r2, [r3, #0]
 8006afc:	1cfb      	adds	r3, r7, #3
 8006afe:	781b      	ldrb	r3, [r3, #0]
 8006b00:	2b00      	cmp	r3, #0
 8006b02:	d026      	beq.n	8006b52 <VL53L0X_ref_calibration_io+0xd4>
 8006b04:	2328      	movs	r3, #40	; 0x28
 8006b06:	18fb      	adds	r3, r7, r3
 8006b08:	781b      	ldrb	r3, [r3, #0]
 8006b0a:	2b00      	cmp	r3, #0
 8006b0c:	d00c      	beq.n	8006b28 <VL53L0X_ref_calibration_io+0xaa>
 8006b0e:	6a3a      	ldr	r2, [r7, #32]
 8006b10:	687b      	ldr	r3, [r7, #4]
 8006b12:	21cb      	movs	r1, #203	; 0xcb
 8006b14:	0018      	movs	r0, r3
 8006b16:	f7fc ff5b 	bl	80039d0 <VL53L0X_RdByte>
 8006b1a:	0003      	movs	r3, r0
 8006b1c:	0019      	movs	r1, r3
 8006b1e:	193b      	adds	r3, r7, r4
 8006b20:	193a      	adds	r2, r7, r4
 8006b22:	7812      	ldrb	r2, [r2, #0]
 8006b24:	430a      	orrs	r2, r1
 8006b26:	701a      	strb	r2, [r3, #0]
 8006b28:	232c      	movs	r3, #44	; 0x2c
 8006b2a:	18fb      	adds	r3, r7, r3
 8006b2c:	781b      	ldrb	r3, [r3, #0]
 8006b2e:	2b00      	cmp	r3, #0
 8006b30:	d037      	beq.n	8006ba2 <VL53L0X_ref_calibration_io+0x124>
 8006b32:	230e      	movs	r3, #14
 8006b34:	18fa      	adds	r2, r7, r3
 8006b36:	687b      	ldr	r3, [r7, #4]
 8006b38:	21ee      	movs	r1, #238	; 0xee
 8006b3a:	0018      	movs	r0, r3
 8006b3c:	f7fc ff48 	bl	80039d0 <VL53L0X_RdByte>
 8006b40:	0003      	movs	r3, r0
 8006b42:	0019      	movs	r1, r3
 8006b44:	220f      	movs	r2, #15
 8006b46:	18bb      	adds	r3, r7, r2
 8006b48:	18ba      	adds	r2, r7, r2
 8006b4a:	7812      	ldrb	r2, [r2, #0]
 8006b4c:	430a      	orrs	r2, r1
 8006b4e:	701a      	strb	r2, [r3, #0]
 8006b50:	e027      	b.n	8006ba2 <VL53L0X_ref_calibration_io+0x124>
 8006b52:	2328      	movs	r3, #40	; 0x28
 8006b54:	18fb      	adds	r3, r7, r3
 8006b56:	781b      	ldrb	r3, [r3, #0]
 8006b58:	2b00      	cmp	r3, #0
 8006b5a:	d00e      	beq.n	8006b7a <VL53L0X_ref_calibration_io+0xfc>
 8006b5c:	1cbb      	adds	r3, r7, #2
 8006b5e:	781a      	ldrb	r2, [r3, #0]
 8006b60:	687b      	ldr	r3, [r7, #4]
 8006b62:	21cb      	movs	r1, #203	; 0xcb
 8006b64:	0018      	movs	r0, r3
 8006b66:	f7fc fec5 	bl	80038f4 <VL53L0X_WrByte>
 8006b6a:	0003      	movs	r3, r0
 8006b6c:	0019      	movs	r1, r3
 8006b6e:	220f      	movs	r2, #15
 8006b70:	18bb      	adds	r3, r7, r2
 8006b72:	18ba      	adds	r2, r7, r2
 8006b74:	7812      	ldrb	r2, [r2, #0]
 8006b76:	430a      	orrs	r2, r1
 8006b78:	701a      	strb	r2, [r3, #0]
 8006b7a:	232c      	movs	r3, #44	; 0x2c
 8006b7c:	18fb      	adds	r3, r7, r3
 8006b7e:	781b      	ldrb	r3, [r3, #0]
 8006b80:	2b00      	cmp	r3, #0
 8006b82:	d00e      	beq.n	8006ba2 <VL53L0X_ref_calibration_io+0x124>
 8006b84:	1c7b      	adds	r3, r7, #1
 8006b86:	781b      	ldrb	r3, [r3, #0]
 8006b88:	6878      	ldr	r0, [r7, #4]
 8006b8a:	2280      	movs	r2, #128	; 0x80
 8006b8c:	21ee      	movs	r1, #238	; 0xee
 8006b8e:	f7fc feee 	bl	800396e <VL53L0X_UpdateByte>
 8006b92:	0003      	movs	r3, r0
 8006b94:	0019      	movs	r1, r3
 8006b96:	220f      	movs	r2, #15
 8006b98:	18bb      	adds	r3, r7, r2
 8006b9a:	18ba      	adds	r2, r7, r2
 8006b9c:	7812      	ldrb	r2, [r2, #0]
 8006b9e:	430a      	orrs	r2, r1
 8006ba0:	701a      	strb	r2, [r3, #0]
 8006ba2:	687b      	ldr	r3, [r7, #4]
 8006ba4:	2201      	movs	r2, #1
 8006ba6:	21ff      	movs	r1, #255	; 0xff
 8006ba8:	0018      	movs	r0, r3
 8006baa:	f7fc fea3 	bl	80038f4 <VL53L0X_WrByte>
 8006bae:	0003      	movs	r3, r0
 8006bb0:	0019      	movs	r1, r3
 8006bb2:	240f      	movs	r4, #15
 8006bb4:	193b      	adds	r3, r7, r4
 8006bb6:	193a      	adds	r2, r7, r4
 8006bb8:	7812      	ldrb	r2, [r2, #0]
 8006bba:	430a      	orrs	r2, r1
 8006bbc:	701a      	strb	r2, [r3, #0]
 8006bbe:	687b      	ldr	r3, [r7, #4]
 8006bc0:	2201      	movs	r2, #1
 8006bc2:	2100      	movs	r1, #0
 8006bc4:	0018      	movs	r0, r3
 8006bc6:	f7fc fe95 	bl	80038f4 <VL53L0X_WrByte>
 8006bca:	0003      	movs	r3, r0
 8006bcc:	0019      	movs	r1, r3
 8006bce:	193b      	adds	r3, r7, r4
 8006bd0:	193a      	adds	r2, r7, r4
 8006bd2:	7812      	ldrb	r2, [r2, #0]
 8006bd4:	430a      	orrs	r2, r1
 8006bd6:	701a      	strb	r2, [r3, #0]
 8006bd8:	687b      	ldr	r3, [r7, #4]
 8006bda:	2200      	movs	r2, #0
 8006bdc:	21ff      	movs	r1, #255	; 0xff
 8006bde:	0018      	movs	r0, r3
 8006be0:	f7fc fe88 	bl	80038f4 <VL53L0X_WrByte>
 8006be4:	0003      	movs	r3, r0
 8006be6:	0019      	movs	r1, r3
 8006be8:	0020      	movs	r0, r4
 8006bea:	183b      	adds	r3, r7, r0
 8006bec:	183a      	adds	r2, r7, r0
 8006bee:	7812      	ldrb	r2, [r2, #0]
 8006bf0:	430a      	orrs	r2, r1
 8006bf2:	701a      	strb	r2, [r3, #0]
 8006bf4:	230e      	movs	r3, #14
 8006bf6:	18fb      	adds	r3, r7, r3
 8006bf8:	781b      	ldrb	r3, [r3, #0]
 8006bfa:	2210      	movs	r2, #16
 8006bfc:	4393      	bics	r3, r2
 8006bfe:	b2da      	uxtb	r2, r3
 8006c00:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8006c02:	701a      	strb	r2, [r3, #0]
 8006c04:	183b      	adds	r3, r7, r0
 8006c06:	781b      	ldrb	r3, [r3, #0]
 8006c08:	b25b      	sxtb	r3, r3
 8006c0a:	0018      	movs	r0, r3
 8006c0c:	46bd      	mov	sp, r7
 8006c0e:	b005      	add	sp, #20
 8006c10:	bd90      	pop	{r4, r7, pc}

08006c12 <VL53L0X_perform_vhv_calibration>:
 8006c12:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006c14:	b08b      	sub	sp, #44	; 0x2c
 8006c16:	af04      	add	r7, sp, #16
 8006c18:	60f8      	str	r0, [r7, #12]
 8006c1a:	60b9      	str	r1, [r7, #8]
 8006c1c:	0019      	movs	r1, r3
 8006c1e:	1dfb      	adds	r3, r7, #7
 8006c20:	701a      	strb	r2, [r3, #0]
 8006c22:	1dbb      	adds	r3, r7, #6
 8006c24:	1c0a      	adds	r2, r1, #0
 8006c26:	701a      	strb	r2, [r3, #0]
 8006c28:	2317      	movs	r3, #23
 8006c2a:	18fb      	adds	r3, r7, r3
 8006c2c:	2200      	movs	r2, #0
 8006c2e:	701a      	strb	r2, [r3, #0]
 8006c30:	2116      	movs	r1, #22
 8006c32:	187b      	adds	r3, r7, r1
 8006c34:	2200      	movs	r2, #0
 8006c36:	701a      	strb	r2, [r3, #0]
 8006c38:	2315      	movs	r3, #21
 8006c3a:	18fb      	adds	r3, r7, r3
 8006c3c:	2200      	movs	r2, #0
 8006c3e:	701a      	strb	r2, [r3, #0]
 8006c40:	2314      	movs	r3, #20
 8006c42:	18fb      	adds	r3, r7, r3
 8006c44:	2200      	movs	r2, #0
 8006c46:	701a      	strb	r2, [r3, #0]
 8006c48:	2313      	movs	r3, #19
 8006c4a:	18fb      	adds	r3, r7, r3
 8006c4c:	2200      	movs	r2, #0
 8006c4e:	701a      	strb	r2, [r3, #0]
 8006c50:	1dbb      	adds	r3, r7, #6
 8006c52:	781b      	ldrb	r3, [r3, #0]
 8006c54:	2b00      	cmp	r3, #0
 8006c56:	d005      	beq.n	8006c64 <VL53L0X_perform_vhv_calibration+0x52>
 8006c58:	187b      	adds	r3, r7, r1
 8006c5a:	68f9      	ldr	r1, [r7, #12]
 8006c5c:	2288      	movs	r2, #136	; 0x88
 8006c5e:	0052      	lsls	r2, r2, #1
 8006c60:	5c8a      	ldrb	r2, [r1, r2]
 8006c62:	701a      	strb	r2, [r3, #0]
 8006c64:	2517      	movs	r5, #23
 8006c66:	197c      	adds	r4, r7, r5
 8006c68:	68fb      	ldr	r3, [r7, #12]
 8006c6a:	2201      	movs	r2, #1
 8006c6c:	2101      	movs	r1, #1
 8006c6e:	0018      	movs	r0, r3
 8006c70:	f7fc fe40 	bl	80038f4 <VL53L0X_WrByte>
 8006c74:	0003      	movs	r3, r0
 8006c76:	7023      	strb	r3, [r4, #0]
 8006c78:	197b      	adds	r3, r7, r5
 8006c7a:	781b      	ldrb	r3, [r3, #0]
 8006c7c:	b25b      	sxtb	r3, r3
 8006c7e:	2b00      	cmp	r3, #0
 8006c80:	d107      	bne.n	8006c92 <VL53L0X_perform_vhv_calibration+0x80>
 8006c82:	197c      	adds	r4, r7, r5
 8006c84:	68fb      	ldr	r3, [r7, #12]
 8006c86:	2140      	movs	r1, #64	; 0x40
 8006c88:	0018      	movs	r0, r3
 8006c8a:	f7ff fea9 	bl	80069e0 <VL53L0X_perform_single_ref_calibration>
 8006c8e:	0003      	movs	r3, r0
 8006c90:	7023      	strb	r3, [r4, #0]
 8006c92:	2217      	movs	r2, #23
 8006c94:	18bb      	adds	r3, r7, r2
 8006c96:	781b      	ldrb	r3, [r3, #0]
 8006c98:	b25b      	sxtb	r3, r3
 8006c9a:	2b00      	cmp	r3, #0
 8006c9c:	d11b      	bne.n	8006cd6 <VL53L0X_perform_vhv_calibration+0xc4>
 8006c9e:	1dfb      	adds	r3, r7, #7
 8006ca0:	781b      	ldrb	r3, [r3, #0]
 8006ca2:	2b01      	cmp	r3, #1
 8006ca4:	d117      	bne.n	8006cd6 <VL53L0X_perform_vhv_calibration+0xc4>
 8006ca6:	18bc      	adds	r4, r7, r2
 8006ca8:	2314      	movs	r3, #20
 8006caa:	18fb      	adds	r3, r7, r3
 8006cac:	7819      	ldrb	r1, [r3, #0]
 8006cae:	2315      	movs	r3, #21
 8006cb0:	18fb      	adds	r3, r7, r3
 8006cb2:	781a      	ldrb	r2, [r3, #0]
 8006cb4:	68f8      	ldr	r0, [r7, #12]
 8006cb6:	2300      	movs	r3, #0
 8006cb8:	9303      	str	r3, [sp, #12]
 8006cba:	2301      	movs	r3, #1
 8006cbc:	9302      	str	r3, [sp, #8]
 8006cbe:	2313      	movs	r3, #19
 8006cc0:	18fb      	adds	r3, r7, r3
 8006cc2:	9301      	str	r3, [sp, #4]
 8006cc4:	68bb      	ldr	r3, [r7, #8]
 8006cc6:	9300      	str	r3, [sp, #0]
 8006cc8:	000b      	movs	r3, r1
 8006cca:	2101      	movs	r1, #1
 8006ccc:	f7ff fed7 	bl	8006a7e <VL53L0X_ref_calibration_io>
 8006cd0:	0003      	movs	r3, r0
 8006cd2:	7023      	strb	r3, [r4, #0]
 8006cd4:	e002      	b.n	8006cdc <VL53L0X_perform_vhv_calibration+0xca>
 8006cd6:	68bb      	ldr	r3, [r7, #8]
 8006cd8:	2200      	movs	r2, #0
 8006cda:	701a      	strb	r2, [r3, #0]
 8006cdc:	2517      	movs	r5, #23
 8006cde:	197b      	adds	r3, r7, r5
 8006ce0:	781b      	ldrb	r3, [r3, #0]
 8006ce2:	b25b      	sxtb	r3, r3
 8006ce4:	2b00      	cmp	r3, #0
 8006ce6:	d119      	bne.n	8006d1c <VL53L0X_perform_vhv_calibration+0x10a>
 8006ce8:	1dbb      	adds	r3, r7, #6
 8006cea:	781b      	ldrb	r3, [r3, #0]
 8006cec:	2b00      	cmp	r3, #0
 8006cee:	d015      	beq.n	8006d1c <VL53L0X_perform_vhv_calibration+0x10a>
 8006cf0:	197c      	adds	r4, r7, r5
 8006cf2:	2616      	movs	r6, #22
 8006cf4:	19bb      	adds	r3, r7, r6
 8006cf6:	781a      	ldrb	r2, [r3, #0]
 8006cf8:	68fb      	ldr	r3, [r7, #12]
 8006cfa:	2101      	movs	r1, #1
 8006cfc:	0018      	movs	r0, r3
 8006cfe:	f7fc fdf9 	bl	80038f4 <VL53L0X_WrByte>
 8006d02:	0003      	movs	r3, r0
 8006d04:	7023      	strb	r3, [r4, #0]
 8006d06:	197b      	adds	r3, r7, r5
 8006d08:	781b      	ldrb	r3, [r3, #0]
 8006d0a:	b25b      	sxtb	r3, r3
 8006d0c:	2b00      	cmp	r3, #0
 8006d0e:	d105      	bne.n	8006d1c <VL53L0X_perform_vhv_calibration+0x10a>
 8006d10:	68fa      	ldr	r2, [r7, #12]
 8006d12:	19b9      	adds	r1, r7, r6
 8006d14:	2388      	movs	r3, #136	; 0x88
 8006d16:	005b      	lsls	r3, r3, #1
 8006d18:	7809      	ldrb	r1, [r1, #0]
 8006d1a:	54d1      	strb	r1, [r2, r3]
 8006d1c:	2317      	movs	r3, #23
 8006d1e:	18fb      	adds	r3, r7, r3
 8006d20:	781b      	ldrb	r3, [r3, #0]
 8006d22:	b25b      	sxtb	r3, r3
 8006d24:	0018      	movs	r0, r3
 8006d26:	46bd      	mov	sp, r7
 8006d28:	b007      	add	sp, #28
 8006d2a:	bdf0      	pop	{r4, r5, r6, r7, pc}

08006d2c <VL53L0X_perform_phase_calibration>:
 8006d2c:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006d2e:	b08b      	sub	sp, #44	; 0x2c
 8006d30:	af04      	add	r7, sp, #16
 8006d32:	60f8      	str	r0, [r7, #12]
 8006d34:	60b9      	str	r1, [r7, #8]
 8006d36:	0019      	movs	r1, r3
 8006d38:	1dfb      	adds	r3, r7, #7
 8006d3a:	701a      	strb	r2, [r3, #0]
 8006d3c:	1dbb      	adds	r3, r7, #6
 8006d3e:	1c0a      	adds	r2, r1, #0
 8006d40:	701a      	strb	r2, [r3, #0]
 8006d42:	2317      	movs	r3, #23
 8006d44:	18fb      	adds	r3, r7, r3
 8006d46:	2200      	movs	r2, #0
 8006d48:	701a      	strb	r2, [r3, #0]
 8006d4a:	2116      	movs	r1, #22
 8006d4c:	187b      	adds	r3, r7, r1
 8006d4e:	2200      	movs	r2, #0
 8006d50:	701a      	strb	r2, [r3, #0]
 8006d52:	2315      	movs	r3, #21
 8006d54:	18fb      	adds	r3, r7, r3
 8006d56:	2200      	movs	r2, #0
 8006d58:	701a      	strb	r2, [r3, #0]
 8006d5a:	2314      	movs	r3, #20
 8006d5c:	18fb      	adds	r3, r7, r3
 8006d5e:	2200      	movs	r2, #0
 8006d60:	701a      	strb	r2, [r3, #0]
 8006d62:	1dbb      	adds	r3, r7, #6
 8006d64:	781b      	ldrb	r3, [r3, #0]
 8006d66:	2b00      	cmp	r3, #0
 8006d68:	d005      	beq.n	8006d76 <VL53L0X_perform_phase_calibration+0x4a>
 8006d6a:	187b      	adds	r3, r7, r1
 8006d6c:	68f9      	ldr	r1, [r7, #12]
 8006d6e:	2288      	movs	r2, #136	; 0x88
 8006d70:	0052      	lsls	r2, r2, #1
 8006d72:	5c8a      	ldrb	r2, [r1, r2]
 8006d74:	701a      	strb	r2, [r3, #0]
 8006d76:	2517      	movs	r5, #23
 8006d78:	197c      	adds	r4, r7, r5
 8006d7a:	68fb      	ldr	r3, [r7, #12]
 8006d7c:	2202      	movs	r2, #2
 8006d7e:	2101      	movs	r1, #1
 8006d80:	0018      	movs	r0, r3
 8006d82:	f7fc fdb7 	bl	80038f4 <VL53L0X_WrByte>
 8006d86:	0003      	movs	r3, r0
 8006d88:	7023      	strb	r3, [r4, #0]
 8006d8a:	197b      	adds	r3, r7, r5
 8006d8c:	781b      	ldrb	r3, [r3, #0]
 8006d8e:	b25b      	sxtb	r3, r3
 8006d90:	2b00      	cmp	r3, #0
 8006d92:	d107      	bne.n	8006da4 <VL53L0X_perform_phase_calibration+0x78>
 8006d94:	197c      	adds	r4, r7, r5
 8006d96:	68fb      	ldr	r3, [r7, #12]
 8006d98:	2100      	movs	r1, #0
 8006d9a:	0018      	movs	r0, r3
 8006d9c:	f7ff fe20 	bl	80069e0 <VL53L0X_perform_single_ref_calibration>
 8006da0:	0003      	movs	r3, r0
 8006da2:	7023      	strb	r3, [r4, #0]
 8006da4:	2217      	movs	r2, #23
 8006da6:	18bb      	adds	r3, r7, r2
 8006da8:	781b      	ldrb	r3, [r3, #0]
 8006daa:	b25b      	sxtb	r3, r3
 8006dac:	2b00      	cmp	r3, #0
 8006dae:	d11b      	bne.n	8006de8 <VL53L0X_perform_phase_calibration+0xbc>
 8006db0:	1dfb      	adds	r3, r7, #7
 8006db2:	781b      	ldrb	r3, [r3, #0]
 8006db4:	2b01      	cmp	r3, #1
 8006db6:	d117      	bne.n	8006de8 <VL53L0X_perform_phase_calibration+0xbc>
 8006db8:	18bc      	adds	r4, r7, r2
 8006dba:	2314      	movs	r3, #20
 8006dbc:	18fb      	adds	r3, r7, r3
 8006dbe:	7819      	ldrb	r1, [r3, #0]
 8006dc0:	2315      	movs	r3, #21
 8006dc2:	18fb      	adds	r3, r7, r3
 8006dc4:	781a      	ldrb	r2, [r3, #0]
 8006dc6:	68f8      	ldr	r0, [r7, #12]
 8006dc8:	2301      	movs	r3, #1
 8006dca:	9303      	str	r3, [sp, #12]
 8006dcc:	2300      	movs	r3, #0
 8006dce:	9302      	str	r3, [sp, #8]
 8006dd0:	68bb      	ldr	r3, [r7, #8]
 8006dd2:	9301      	str	r3, [sp, #4]
 8006dd4:	2313      	movs	r3, #19
 8006dd6:	18fb      	adds	r3, r7, r3
 8006dd8:	9300      	str	r3, [sp, #0]
 8006dda:	000b      	movs	r3, r1
 8006ddc:	2101      	movs	r1, #1
 8006dde:	f7ff fe4e 	bl	8006a7e <VL53L0X_ref_calibration_io>
 8006de2:	0003      	movs	r3, r0
 8006de4:	7023      	strb	r3, [r4, #0]
 8006de6:	e002      	b.n	8006dee <VL53L0X_perform_phase_calibration+0xc2>
 8006de8:	68bb      	ldr	r3, [r7, #8]
 8006dea:	2200      	movs	r2, #0
 8006dec:	701a      	strb	r2, [r3, #0]
 8006dee:	2517      	movs	r5, #23
 8006df0:	197b      	adds	r3, r7, r5
 8006df2:	781b      	ldrb	r3, [r3, #0]
 8006df4:	b25b      	sxtb	r3, r3
 8006df6:	2b00      	cmp	r3, #0
 8006df8:	d119      	bne.n	8006e2e <VL53L0X_perform_phase_calibration+0x102>
 8006dfa:	1dbb      	adds	r3, r7, #6
 8006dfc:	781b      	ldrb	r3, [r3, #0]
 8006dfe:	2b00      	cmp	r3, #0
 8006e00:	d015      	beq.n	8006e2e <VL53L0X_perform_phase_calibration+0x102>
 8006e02:	197c      	adds	r4, r7, r5
 8006e04:	2616      	movs	r6, #22
 8006e06:	19bb      	adds	r3, r7, r6
 8006e08:	781a      	ldrb	r2, [r3, #0]
 8006e0a:	68fb      	ldr	r3, [r7, #12]
 8006e0c:	2101      	movs	r1, #1
 8006e0e:	0018      	movs	r0, r3
 8006e10:	f7fc fd70 	bl	80038f4 <VL53L0X_WrByte>
 8006e14:	0003      	movs	r3, r0
 8006e16:	7023      	strb	r3, [r4, #0]
 8006e18:	197b      	adds	r3, r7, r5
 8006e1a:	781b      	ldrb	r3, [r3, #0]
 8006e1c:	b25b      	sxtb	r3, r3
 8006e1e:	2b00      	cmp	r3, #0
 8006e20:	d105      	bne.n	8006e2e <VL53L0X_perform_phase_calibration+0x102>
 8006e22:	68fa      	ldr	r2, [r7, #12]
 8006e24:	19b9      	adds	r1, r7, r6
 8006e26:	2388      	movs	r3, #136	; 0x88
 8006e28:	005b      	lsls	r3, r3, #1
 8006e2a:	7809      	ldrb	r1, [r1, #0]
 8006e2c:	54d1      	strb	r1, [r2, r3]
 8006e2e:	2317      	movs	r3, #23
 8006e30:	18fb      	adds	r3, r7, r3
 8006e32:	781b      	ldrb	r3, [r3, #0]
 8006e34:	b25b      	sxtb	r3, r3
 8006e36:	0018      	movs	r0, r3
 8006e38:	46bd      	mov	sp, r7
 8006e3a:	b007      	add	sp, #28
 8006e3c:	bdf0      	pop	{r4, r5, r6, r7, pc}

08006e3e <VL53L0X_perform_ref_calibration>:
 8006e3e:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006e40:	b087      	sub	sp, #28
 8006e42:	af00      	add	r7, sp, #0
 8006e44:	60f8      	str	r0, [r7, #12]
 8006e46:	60b9      	str	r1, [r7, #8]
 8006e48:	607a      	str	r2, [r7, #4]
 8006e4a:	001a      	movs	r2, r3
 8006e4c:	1cfb      	adds	r3, r7, #3
 8006e4e:	701a      	strb	r2, [r3, #0]
 8006e50:	2017      	movs	r0, #23
 8006e52:	183b      	adds	r3, r7, r0
 8006e54:	2200      	movs	r2, #0
 8006e56:	701a      	strb	r2, [r3, #0]
 8006e58:	2116      	movs	r1, #22
 8006e5a:	187b      	adds	r3, r7, r1
 8006e5c:	2200      	movs	r2, #0
 8006e5e:	701a      	strb	r2, [r3, #0]
 8006e60:	187b      	adds	r3, r7, r1
 8006e62:	68f9      	ldr	r1, [r7, #12]
 8006e64:	2288      	movs	r2, #136	; 0x88
 8006e66:	0052      	lsls	r2, r2, #1
 8006e68:	5c8a      	ldrb	r2, [r1, r2]
 8006e6a:	701a      	strb	r2, [r3, #0]
 8006e6c:	0005      	movs	r5, r0
 8006e6e:	183c      	adds	r4, r7, r0
 8006e70:	1cfb      	adds	r3, r7, #3
 8006e72:	781a      	ldrb	r2, [r3, #0]
 8006e74:	68b9      	ldr	r1, [r7, #8]
 8006e76:	68f8      	ldr	r0, [r7, #12]
 8006e78:	2300      	movs	r3, #0
 8006e7a:	f7ff feca 	bl	8006c12 <VL53L0X_perform_vhv_calibration>
 8006e7e:	0003      	movs	r3, r0
 8006e80:	7023      	strb	r3, [r4, #0]
 8006e82:	197b      	adds	r3, r7, r5
 8006e84:	781b      	ldrb	r3, [r3, #0]
 8006e86:	b25b      	sxtb	r3, r3
 8006e88:	2b00      	cmp	r3, #0
 8006e8a:	d109      	bne.n	8006ea0 <VL53L0X_perform_ref_calibration+0x62>
 8006e8c:	197c      	adds	r4, r7, r5
 8006e8e:	1cfb      	adds	r3, r7, #3
 8006e90:	781a      	ldrb	r2, [r3, #0]
 8006e92:	6879      	ldr	r1, [r7, #4]
 8006e94:	68f8      	ldr	r0, [r7, #12]
 8006e96:	2300      	movs	r3, #0
 8006e98:	f7ff ff48 	bl	8006d2c <VL53L0X_perform_phase_calibration>
 8006e9c:	0003      	movs	r3, r0
 8006e9e:	7023      	strb	r3, [r4, #0]
 8006ea0:	2517      	movs	r5, #23
 8006ea2:	197b      	adds	r3, r7, r5
 8006ea4:	781b      	ldrb	r3, [r3, #0]
 8006ea6:	b25b      	sxtb	r3, r3
 8006ea8:	2b00      	cmp	r3, #0
 8006eaa:	d115      	bne.n	8006ed8 <VL53L0X_perform_ref_calibration+0x9a>
 8006eac:	197c      	adds	r4, r7, r5
 8006eae:	2616      	movs	r6, #22
 8006eb0:	19bb      	adds	r3, r7, r6
 8006eb2:	781a      	ldrb	r2, [r3, #0]
 8006eb4:	68fb      	ldr	r3, [r7, #12]
 8006eb6:	2101      	movs	r1, #1
 8006eb8:	0018      	movs	r0, r3
 8006eba:	f7fc fd1b 	bl	80038f4 <VL53L0X_WrByte>
 8006ebe:	0003      	movs	r3, r0
 8006ec0:	7023      	strb	r3, [r4, #0]
 8006ec2:	197b      	adds	r3, r7, r5
 8006ec4:	781b      	ldrb	r3, [r3, #0]
 8006ec6:	b25b      	sxtb	r3, r3
 8006ec8:	2b00      	cmp	r3, #0
 8006eca:	d105      	bne.n	8006ed8 <VL53L0X_perform_ref_calibration+0x9a>
 8006ecc:	68fa      	ldr	r2, [r7, #12]
 8006ece:	19b9      	adds	r1, r7, r6
 8006ed0:	2388      	movs	r3, #136	; 0x88
 8006ed2:	005b      	lsls	r3, r3, #1
 8006ed4:	7809      	ldrb	r1, [r1, #0]
 8006ed6:	54d1      	strb	r1, [r2, r3]
 8006ed8:	2317      	movs	r3, #23
 8006eda:	18fb      	adds	r3, r7, r3
 8006edc:	781b      	ldrb	r3, [r3, #0]
 8006ede:	b25b      	sxtb	r3, r3
 8006ee0:	0018      	movs	r0, r3
 8006ee2:	46bd      	mov	sp, r7
 8006ee4:	b007      	add	sp, #28
 8006ee6:	bdf0      	pop	{r4, r5, r6, r7, pc}

08006ee8 <VL53L0X_measurement_poll_for_completion>:
 8006ee8:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006eea:	b087      	sub	sp, #28
 8006eec:	af00      	add	r7, sp, #0
 8006eee:	6078      	str	r0, [r7, #4]
 8006ef0:	2317      	movs	r3, #23
 8006ef2:	18fb      	adds	r3, r7, r3
 8006ef4:	2200      	movs	r2, #0
 8006ef6:	701a      	strb	r2, [r3, #0]
 8006ef8:	230f      	movs	r3, #15
 8006efa:	18fb      	adds	r3, r7, r3
 8006efc:	2200      	movs	r2, #0
 8006efe:	701a      	strb	r2, [r3, #0]
 8006f00:	2300      	movs	r3, #0
 8006f02:	613b      	str	r3, [r7, #16]
 8006f04:	2517      	movs	r5, #23
 8006f06:	197c      	adds	r4, r7, r5
 8006f08:	260f      	movs	r6, #15
 8006f0a:	19ba      	adds	r2, r7, r6
 8006f0c:	687b      	ldr	r3, [r7, #4]
 8006f0e:	0011      	movs	r1, r2
 8006f10:	0018      	movs	r0, r3
 8006f12:	f7fe fba1 	bl	8005658 <VL53L0X_GetMeasurementDataReady>
 8006f16:	0003      	movs	r3, r0
 8006f18:	7023      	strb	r3, [r4, #0]
 8006f1a:	0029      	movs	r1, r5
 8006f1c:	187b      	adds	r3, r7, r1
 8006f1e:	781b      	ldrb	r3, [r3, #0]
 8006f20:	b25b      	sxtb	r3, r3
 8006f22:	2b00      	cmp	r3, #0
 8006f24:	d113      	bne.n	8006f4e <VL53L0X_measurement_poll_for_completion+0x66>
 8006f26:	19bb      	adds	r3, r7, r6
 8006f28:	781b      	ldrb	r3, [r3, #0]
 8006f2a:	2b01      	cmp	r3, #1
 8006f2c:	d011      	beq.n	8006f52 <VL53L0X_measurement_poll_for_completion+0x6a>
 8006f2e:	693b      	ldr	r3, [r7, #16]
 8006f30:	3301      	adds	r3, #1
 8006f32:	613b      	str	r3, [r7, #16]
 8006f34:	693b      	ldr	r3, [r7, #16]
 8006f36:	4a0b      	ldr	r2, [pc, #44]	; (8006f64 <VL53L0X_measurement_poll_for_completion+0x7c>)
 8006f38:	4293      	cmp	r3, r2
 8006f3a:	d903      	bls.n	8006f44 <VL53L0X_measurement_poll_for_completion+0x5c>
 8006f3c:	187b      	adds	r3, r7, r1
 8006f3e:	22f9      	movs	r2, #249	; 0xf9
 8006f40:	701a      	strb	r2, [r3, #0]
 8006f42:	e007      	b.n	8006f54 <VL53L0X_measurement_poll_for_completion+0x6c>
 8006f44:	687b      	ldr	r3, [r7, #4]
 8006f46:	0018      	movs	r0, r3
 8006f48:	f7fc fda2 	bl	8003a90 <VL53L0X_PollingDelay>
 8006f4c:	e7da      	b.n	8006f04 <VL53L0X_measurement_poll_for_completion+0x1c>
 8006f4e:	46c0      	nop			; (mov r8, r8)
 8006f50:	e000      	b.n	8006f54 <VL53L0X_measurement_poll_for_completion+0x6c>
 8006f52:	46c0      	nop			; (mov r8, r8)
 8006f54:	2317      	movs	r3, #23
 8006f56:	18fb      	adds	r3, r7, r3
 8006f58:	781b      	ldrb	r3, [r3, #0]
 8006f5a:	b25b      	sxtb	r3, r3
 8006f5c:	0018      	movs	r0, r3
 8006f5e:	46bd      	mov	sp, r7
 8006f60:	b007      	add	sp, #28
 8006f62:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8006f64:	000007cf 	.word	0x000007cf

08006f68 <VL53L0X_decode_vcsel_period>:
 8006f68:	b580      	push	{r7, lr}
 8006f6a:	b084      	sub	sp, #16
 8006f6c:	af00      	add	r7, sp, #0
 8006f6e:	0002      	movs	r2, r0
 8006f70:	1dfb      	adds	r3, r7, #7
 8006f72:	701a      	strb	r2, [r3, #0]
 8006f74:	210f      	movs	r1, #15
 8006f76:	187b      	adds	r3, r7, r1
 8006f78:	2200      	movs	r2, #0
 8006f7a:	701a      	strb	r2, [r3, #0]
 8006f7c:	1dfb      	adds	r3, r7, #7
 8006f7e:	781b      	ldrb	r3, [r3, #0]
 8006f80:	3301      	adds	r3, #1
 8006f82:	b2db      	uxtb	r3, r3
 8006f84:	187a      	adds	r2, r7, r1
 8006f86:	18db      	adds	r3, r3, r3
 8006f88:	7013      	strb	r3, [r2, #0]
 8006f8a:	187b      	adds	r3, r7, r1
 8006f8c:	781b      	ldrb	r3, [r3, #0]
 8006f8e:	0018      	movs	r0, r3
 8006f90:	46bd      	mov	sp, r7
 8006f92:	b004      	add	sp, #16
 8006f94:	bd80      	pop	{r7, pc}

08006f96 <VL53L0X_isqrt>:
 8006f96:	b580      	push	{r7, lr}
 8006f98:	b084      	sub	sp, #16
 8006f9a:	af00      	add	r7, sp, #0
 8006f9c:	6078      	str	r0, [r7, #4]
 8006f9e:	2300      	movs	r3, #0
 8006fa0:	60fb      	str	r3, [r7, #12]
 8006fa2:	2380      	movs	r3, #128	; 0x80
 8006fa4:	05db      	lsls	r3, r3, #23
 8006fa6:	60bb      	str	r3, [r7, #8]
 8006fa8:	e002      	b.n	8006fb0 <VL53L0X_isqrt+0x1a>
 8006faa:	68bb      	ldr	r3, [r7, #8]
 8006fac:	089b      	lsrs	r3, r3, #2
 8006fae:	60bb      	str	r3, [r7, #8]
 8006fb0:	68ba      	ldr	r2, [r7, #8]
 8006fb2:	687b      	ldr	r3, [r7, #4]
 8006fb4:	429a      	cmp	r2, r3
 8006fb6:	d8f8      	bhi.n	8006faa <VL53L0X_isqrt+0x14>
 8006fb8:	e017      	b.n	8006fea <VL53L0X_isqrt+0x54>
 8006fba:	68fa      	ldr	r2, [r7, #12]
 8006fbc:	68bb      	ldr	r3, [r7, #8]
 8006fbe:	18d3      	adds	r3, r2, r3
 8006fc0:	687a      	ldr	r2, [r7, #4]
 8006fc2:	429a      	cmp	r2, r3
 8006fc4:	d30b      	bcc.n	8006fde <VL53L0X_isqrt+0x48>
 8006fc6:	68fa      	ldr	r2, [r7, #12]
 8006fc8:	68bb      	ldr	r3, [r7, #8]
 8006fca:	18d3      	adds	r3, r2, r3
 8006fcc:	687a      	ldr	r2, [r7, #4]
 8006fce:	1ad3      	subs	r3, r2, r3
 8006fd0:	607b      	str	r3, [r7, #4]
 8006fd2:	68fb      	ldr	r3, [r7, #12]
 8006fd4:	085b      	lsrs	r3, r3, #1
 8006fd6:	68ba      	ldr	r2, [r7, #8]
 8006fd8:	18d3      	adds	r3, r2, r3
 8006fda:	60fb      	str	r3, [r7, #12]
 8006fdc:	e002      	b.n	8006fe4 <VL53L0X_isqrt+0x4e>
 8006fde:	68fb      	ldr	r3, [r7, #12]
 8006fe0:	085b      	lsrs	r3, r3, #1
 8006fe2:	60fb      	str	r3, [r7, #12]
 8006fe4:	68bb      	ldr	r3, [r7, #8]
 8006fe6:	089b      	lsrs	r3, r3, #2
 8006fe8:	60bb      	str	r3, [r7, #8]
 8006fea:	68bb      	ldr	r3, [r7, #8]
 8006fec:	2b00      	cmp	r3, #0
 8006fee:	d1e4      	bne.n	8006fba <VL53L0X_isqrt+0x24>
 8006ff0:	68fb      	ldr	r3, [r7, #12]
 8006ff2:	0018      	movs	r0, r3
 8006ff4:	46bd      	mov	sp, r7
 8006ff6:	b004      	add	sp, #16
 8006ff8:	bd80      	pop	{r7, pc}
	...

08006ffc <VL53L0X_device_read_strobe>:
 8006ffc:	b5f0      	push	{r4, r5, r6, r7, lr}
 8006ffe:	b087      	sub	sp, #28
 8007000:	af00      	add	r7, sp, #0
 8007002:	6078      	str	r0, [r7, #4]
 8007004:	2417      	movs	r4, #23
 8007006:	193b      	adds	r3, r7, r4
 8007008:	2200      	movs	r2, #0
 800700a:	701a      	strb	r2, [r3, #0]
 800700c:	687b      	ldr	r3, [r7, #4]
 800700e:	2200      	movs	r2, #0
 8007010:	2183      	movs	r1, #131	; 0x83
 8007012:	0018      	movs	r0, r3
 8007014:	f7fc fc6e 	bl	80038f4 <VL53L0X_WrByte>
 8007018:	0003      	movs	r3, r0
 800701a:	0019      	movs	r1, r3
 800701c:	193b      	adds	r3, r7, r4
 800701e:	193a      	adds	r2, r7, r4
 8007020:	7812      	ldrb	r2, [r2, #0]
 8007022:	430a      	orrs	r2, r1
 8007024:	701a      	strb	r2, [r3, #0]
 8007026:	193b      	adds	r3, r7, r4
 8007028:	781b      	ldrb	r3, [r3, #0]
 800702a:	b25b      	sxtb	r3, r3
 800702c:	2b00      	cmp	r3, #0
 800702e:	d124      	bne.n	800707a <VL53L0X_device_read_strobe+0x7e>
 8007030:	2300      	movs	r3, #0
 8007032:	613b      	str	r3, [r7, #16]
 8007034:	2617      	movs	r6, #23
 8007036:	19bc      	adds	r4, r7, r6
 8007038:	250f      	movs	r5, #15
 800703a:	197a      	adds	r2, r7, r5
 800703c:	687b      	ldr	r3, [r7, #4]
 800703e:	2183      	movs	r1, #131	; 0x83
 8007040:	0018      	movs	r0, r3
 8007042:	f7fc fcc5 	bl	80039d0 <VL53L0X_RdByte>
 8007046:	0003      	movs	r3, r0
 8007048:	7023      	strb	r3, [r4, #0]
 800704a:	197b      	adds	r3, r7, r5
 800704c:	781b      	ldrb	r3, [r3, #0]
 800704e:	2b00      	cmp	r3, #0
 8007050:	d10b      	bne.n	800706a <VL53L0X_device_read_strobe+0x6e>
 8007052:	19bb      	adds	r3, r7, r6
 8007054:	781b      	ldrb	r3, [r3, #0]
 8007056:	b25b      	sxtb	r3, r3
 8007058:	2b00      	cmp	r3, #0
 800705a:	d106      	bne.n	800706a <VL53L0X_device_read_strobe+0x6e>
 800705c:	693b      	ldr	r3, [r7, #16]
 800705e:	3301      	adds	r3, #1
 8007060:	613b      	str	r3, [r7, #16]
 8007062:	693b      	ldr	r3, [r7, #16]
 8007064:	4a0f      	ldr	r2, [pc, #60]	; (80070a4 <VL53L0X_device_read_strobe+0xa8>)
 8007066:	4293      	cmp	r3, r2
 8007068:	d9e4      	bls.n	8007034 <VL53L0X_device_read_strobe+0x38>
 800706a:	693b      	ldr	r3, [r7, #16]
 800706c:	4a0d      	ldr	r2, [pc, #52]	; (80070a4 <VL53L0X_device_read_strobe+0xa8>)
 800706e:	4293      	cmp	r3, r2
 8007070:	d903      	bls.n	800707a <VL53L0X_device_read_strobe+0x7e>
 8007072:	2317      	movs	r3, #23
 8007074:	18fb      	adds	r3, r7, r3
 8007076:	22f9      	movs	r2, #249	; 0xf9
 8007078:	701a      	strb	r2, [r3, #0]
 800707a:	687b      	ldr	r3, [r7, #4]
 800707c:	2201      	movs	r2, #1
 800707e:	2183      	movs	r1, #131	; 0x83
 8007080:	0018      	movs	r0, r3
 8007082:	f7fc fc37 	bl	80038f4 <VL53L0X_WrByte>
 8007086:	0003      	movs	r3, r0
 8007088:	0019      	movs	r1, r3
 800708a:	2017      	movs	r0, #23
 800708c:	183b      	adds	r3, r7, r0
 800708e:	183a      	adds	r2, r7, r0
 8007090:	7812      	ldrb	r2, [r2, #0]
 8007092:	430a      	orrs	r2, r1
 8007094:	701a      	strb	r2, [r3, #0]
 8007096:	183b      	adds	r3, r7, r0
 8007098:	781b      	ldrb	r3, [r3, #0]
 800709a:	b25b      	sxtb	r3, r3
 800709c:	0018      	movs	r0, r3
 800709e:	46bd      	mov	sp, r7
 80070a0:	b007      	add	sp, #28
 80070a2:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80070a4:	000007cf 	.word	0x000007cf

080070a8 <VL53L0X_get_info_from_device>:
 80070a8:	b5f0      	push	{r4, r5, r6, r7, lr}
 80070aa:	b097      	sub	sp, #92	; 0x5c
 80070ac:	af00      	add	r7, sp, #0
 80070ae:	6078      	str	r0, [r7, #4]
 80070b0:	000a      	movs	r2, r1
 80070b2:	1cfb      	adds	r3, r7, #3
 80070b4:	701a      	strb	r2, [r3, #0]
 80070b6:	2457      	movs	r4, #87	; 0x57
 80070b8:	193b      	adds	r3, r7, r4
 80070ba:	2200      	movs	r2, #0
 80070bc:	701a      	strb	r2, [r3, #0]
 80070be:	2356      	movs	r3, #86	; 0x56
 80070c0:	18fb      	adds	r3, r7, r3
 80070c2:	2200      	movs	r2, #0
 80070c4:	701a      	strb	r2, [r3, #0]
 80070c6:	2255      	movs	r2, #85	; 0x55
 80070c8:	18bb      	adds	r3, r7, r2
 80070ca:	2200      	movs	r2, #0
 80070cc:	701a      	strb	r2, [r3, #0]
 80070ce:	2300      	movs	r3, #0
 80070d0:	62bb      	str	r3, [r7, #40]	; 0x28
 80070d2:	2300      	movs	r3, #0
 80070d4:	627b      	str	r3, [r7, #36]	; 0x24
 80070d6:	2300      	movs	r3, #0
 80070d8:	643b      	str	r3, [r7, #64]	; 0x40
 80070da:	2152      	movs	r1, #82	; 0x52
 80070dc:	187b      	adds	r3, r7, r1
 80070de:	2200      	movs	r2, #0
 80070e0:	801a      	strh	r2, [r3, #0]
 80070e2:	23c8      	movs	r3, #200	; 0xc8
 80070e4:	015b      	lsls	r3, r3, #5
 80070e6:	63fb      	str	r3, [r7, #60]	; 0x3c
 80070e8:	2300      	movs	r3, #0
 80070ea:	64fb      	str	r3, [r7, #76]	; 0x4c
 80070ec:	2300      	movs	r3, #0
 80070ee:	64bb      	str	r3, [r7, #72]	; 0x48
 80070f0:	2300      	movs	r3, #0
 80070f2:	63bb      	str	r3, [r7, #56]	; 0x38
 80070f4:	2537      	movs	r5, #55	; 0x37
 80070f6:	197b      	adds	r3, r7, r5
 80070f8:	687a      	ldr	r2, [r7, #4]
 80070fa:	21f0      	movs	r1, #240	; 0xf0
 80070fc:	5c52      	ldrb	r2, [r2, r1]
 80070fe:	701a      	strb	r2, [r3, #0]
 8007100:	197b      	adds	r3, r7, r5
 8007102:	781b      	ldrb	r3, [r3, #0]
 8007104:	2b07      	cmp	r3, #7
 8007106:	d101      	bne.n	800710c <VL53L0X_get_info_from_device+0x64>
 8007108:	f000 fc51 	bl	80079ae <VL53L0X_get_info_from_device+0x906>
 800710c:	687b      	ldr	r3, [r7, #4]
 800710e:	2201      	movs	r2, #1
 8007110:	2180      	movs	r1, #128	; 0x80
 8007112:	0018      	movs	r0, r3
 8007114:	f7fc fbee 	bl	80038f4 <VL53L0X_WrByte>
 8007118:	0003      	movs	r3, r0
 800711a:	0019      	movs	r1, r3
 800711c:	193b      	adds	r3, r7, r4
 800711e:	193a      	adds	r2, r7, r4
 8007120:	7812      	ldrb	r2, [r2, #0]
 8007122:	430a      	orrs	r2, r1
 8007124:	701a      	strb	r2, [r3, #0]
 8007126:	687b      	ldr	r3, [r7, #4]
 8007128:	2201      	movs	r2, #1
 800712a:	21ff      	movs	r1, #255	; 0xff
 800712c:	0018      	movs	r0, r3
 800712e:	f7fc fbe1 	bl	80038f4 <VL53L0X_WrByte>
 8007132:	0003      	movs	r3, r0
 8007134:	0019      	movs	r1, r3
 8007136:	193b      	adds	r3, r7, r4
 8007138:	193a      	adds	r2, r7, r4
 800713a:	7812      	ldrb	r2, [r2, #0]
 800713c:	430a      	orrs	r2, r1
 800713e:	701a      	strb	r2, [r3, #0]
 8007140:	687b      	ldr	r3, [r7, #4]
 8007142:	2200      	movs	r2, #0
 8007144:	2100      	movs	r1, #0
 8007146:	0018      	movs	r0, r3
 8007148:	f7fc fbd4 	bl	80038f4 <VL53L0X_WrByte>
 800714c:	0003      	movs	r3, r0
 800714e:	0019      	movs	r1, r3
 8007150:	193b      	adds	r3, r7, r4
 8007152:	193a      	adds	r2, r7, r4
 8007154:	7812      	ldrb	r2, [r2, #0]
 8007156:	430a      	orrs	r2, r1
 8007158:	701a      	strb	r2, [r3, #0]
 800715a:	687b      	ldr	r3, [r7, #4]
 800715c:	2206      	movs	r2, #6
 800715e:	21ff      	movs	r1, #255	; 0xff
 8007160:	0018      	movs	r0, r3
 8007162:	f7fc fbc7 	bl	80038f4 <VL53L0X_WrByte>
 8007166:	0003      	movs	r3, r0
 8007168:	0019      	movs	r1, r3
 800716a:	193b      	adds	r3, r7, r4
 800716c:	193a      	adds	r2, r7, r4
 800716e:	7812      	ldrb	r2, [r2, #0]
 8007170:	430a      	orrs	r2, r1
 8007172:	701a      	strb	r2, [r3, #0]
 8007174:	2636      	movs	r6, #54	; 0x36
 8007176:	19ba      	adds	r2, r7, r6
 8007178:	687b      	ldr	r3, [r7, #4]
 800717a:	2183      	movs	r1, #131	; 0x83
 800717c:	0018      	movs	r0, r3
 800717e:	f7fc fc27 	bl	80039d0 <VL53L0X_RdByte>
 8007182:	0003      	movs	r3, r0
 8007184:	0019      	movs	r1, r3
 8007186:	193b      	adds	r3, r7, r4
 8007188:	193a      	adds	r2, r7, r4
 800718a:	7812      	ldrb	r2, [r2, #0]
 800718c:	430a      	orrs	r2, r1
 800718e:	701a      	strb	r2, [r3, #0]
 8007190:	19bb      	adds	r3, r7, r6
 8007192:	781b      	ldrb	r3, [r3, #0]
 8007194:	2204      	movs	r2, #4
 8007196:	4313      	orrs	r3, r2
 8007198:	b2da      	uxtb	r2, r3
 800719a:	687b      	ldr	r3, [r7, #4]
 800719c:	2183      	movs	r1, #131	; 0x83
 800719e:	0018      	movs	r0, r3
 80071a0:	f7fc fba8 	bl	80038f4 <VL53L0X_WrByte>
 80071a4:	0003      	movs	r3, r0
 80071a6:	0019      	movs	r1, r3
 80071a8:	193b      	adds	r3, r7, r4
 80071aa:	193a      	adds	r2, r7, r4
 80071ac:	7812      	ldrb	r2, [r2, #0]
 80071ae:	430a      	orrs	r2, r1
 80071b0:	701a      	strb	r2, [r3, #0]
 80071b2:	687b      	ldr	r3, [r7, #4]
 80071b4:	2207      	movs	r2, #7
 80071b6:	21ff      	movs	r1, #255	; 0xff
 80071b8:	0018      	movs	r0, r3
 80071ba:	f7fc fb9b 	bl	80038f4 <VL53L0X_WrByte>
 80071be:	0003      	movs	r3, r0
 80071c0:	0019      	movs	r1, r3
 80071c2:	193b      	adds	r3, r7, r4
 80071c4:	193a      	adds	r2, r7, r4
 80071c6:	7812      	ldrb	r2, [r2, #0]
 80071c8:	430a      	orrs	r2, r1
 80071ca:	701a      	strb	r2, [r3, #0]
 80071cc:	687b      	ldr	r3, [r7, #4]
 80071ce:	2201      	movs	r2, #1
 80071d0:	2181      	movs	r1, #129	; 0x81
 80071d2:	0018      	movs	r0, r3
 80071d4:	f7fc fb8e 	bl	80038f4 <VL53L0X_WrByte>
 80071d8:	0003      	movs	r3, r0
 80071da:	0019      	movs	r1, r3
 80071dc:	193b      	adds	r3, r7, r4
 80071de:	193a      	adds	r2, r7, r4
 80071e0:	7812      	ldrb	r2, [r2, #0]
 80071e2:	430a      	orrs	r2, r1
 80071e4:	701a      	strb	r2, [r3, #0]
 80071e6:	687b      	ldr	r3, [r7, #4]
 80071e8:	0018      	movs	r0, r3
 80071ea:	f7fc fc51 	bl	8003a90 <VL53L0X_PollingDelay>
 80071ee:	0003      	movs	r3, r0
 80071f0:	0019      	movs	r1, r3
 80071f2:	193b      	adds	r3, r7, r4
 80071f4:	193a      	adds	r2, r7, r4
 80071f6:	7812      	ldrb	r2, [r2, #0]
 80071f8:	430a      	orrs	r2, r1
 80071fa:	701a      	strb	r2, [r3, #0]
 80071fc:	687b      	ldr	r3, [r7, #4]
 80071fe:	2201      	movs	r2, #1
 8007200:	2180      	movs	r1, #128	; 0x80
 8007202:	0018      	movs	r0, r3
 8007204:	f7fc fb76 	bl	80038f4 <VL53L0X_WrByte>
 8007208:	0003      	movs	r3, r0
 800720a:	0019      	movs	r1, r3
 800720c:	193b      	adds	r3, r7, r4
 800720e:	193a      	adds	r2, r7, r4
 8007210:	7812      	ldrb	r2, [r2, #0]
 8007212:	430a      	orrs	r2, r1
 8007214:	701a      	strb	r2, [r3, #0]
 8007216:	1cfb      	adds	r3, r7, #3
 8007218:	781b      	ldrb	r3, [r3, #0]
 800721a:	2201      	movs	r2, #1
 800721c:	4013      	ands	r3, r2
 800721e:	d100      	bne.n	8007222 <VL53L0X_get_info_from_device+0x17a>
 8007220:	e0a3      	b.n	800736a <VL53L0X_get_info_from_device+0x2c2>
 8007222:	197b      	adds	r3, r7, r5
 8007224:	781b      	ldrb	r3, [r3, #0]
 8007226:	2201      	movs	r2, #1
 8007228:	4013      	ands	r3, r2
 800722a:	d000      	beq.n	800722e <VL53L0X_get_info_from_device+0x186>
 800722c:	e09d      	b.n	800736a <VL53L0X_get_info_from_device+0x2c2>
 800722e:	687b      	ldr	r3, [r7, #4]
 8007230:	226b      	movs	r2, #107	; 0x6b
 8007232:	2194      	movs	r1, #148	; 0x94
 8007234:	0018      	movs	r0, r3
 8007236:	f7fc fb5d 	bl	80038f4 <VL53L0X_WrByte>
 800723a:	0003      	movs	r3, r0
 800723c:	0019      	movs	r1, r3
 800723e:	193b      	adds	r3, r7, r4
 8007240:	193a      	adds	r2, r7, r4
 8007242:	7812      	ldrb	r2, [r2, #0]
 8007244:	430a      	orrs	r2, r1
 8007246:	701a      	strb	r2, [r3, #0]
 8007248:	687b      	ldr	r3, [r7, #4]
 800724a:	0018      	movs	r0, r3
 800724c:	f7ff fed6 	bl	8006ffc <VL53L0X_device_read_strobe>
 8007250:	0003      	movs	r3, r0
 8007252:	0019      	movs	r1, r3
 8007254:	193b      	adds	r3, r7, r4
 8007256:	193a      	adds	r2, r7, r4
 8007258:	7812      	ldrb	r2, [r2, #0]
 800725a:	430a      	orrs	r2, r1
 800725c:	701a      	strb	r2, [r3, #0]
 800725e:	2630      	movs	r6, #48	; 0x30
 8007260:	19ba      	adds	r2, r7, r6
 8007262:	687b      	ldr	r3, [r7, #4]
 8007264:	2190      	movs	r1, #144	; 0x90
 8007266:	0018      	movs	r0, r3
 8007268:	f7fc fbea 	bl	8003a40 <VL53L0X_RdDWord>
 800726c:	0003      	movs	r3, r0
 800726e:	0019      	movs	r1, r3
 8007270:	193b      	adds	r3, r7, r4
 8007272:	193a      	adds	r2, r7, r4
 8007274:	7812      	ldrb	r2, [r2, #0]
 8007276:	430a      	orrs	r2, r1
 8007278:	701a      	strb	r2, [r3, #0]
 800727a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800727c:	0a1b      	lsrs	r3, r3, #8
 800727e:	b2da      	uxtb	r2, r3
 8007280:	2356      	movs	r3, #86	; 0x56
 8007282:	18fb      	adds	r3, r7, r3
 8007284:	217f      	movs	r1, #127	; 0x7f
 8007286:	400a      	ands	r2, r1
 8007288:	701a      	strb	r2, [r3, #0]
 800728a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800728c:	0bdb      	lsrs	r3, r3, #15
 800728e:	b2da      	uxtb	r2, r3
 8007290:	2355      	movs	r3, #85	; 0x55
 8007292:	18fb      	adds	r3, r7, r3
 8007294:	2101      	movs	r1, #1
 8007296:	400a      	ands	r2, r1
 8007298:	701a      	strb	r2, [r3, #0]
 800729a:	687b      	ldr	r3, [r7, #4]
 800729c:	2224      	movs	r2, #36	; 0x24
 800729e:	2194      	movs	r1, #148	; 0x94
 80072a0:	0018      	movs	r0, r3
 80072a2:	f7fc fb27 	bl	80038f4 <VL53L0X_WrByte>
 80072a6:	0003      	movs	r3, r0
 80072a8:	0019      	movs	r1, r3
 80072aa:	193b      	adds	r3, r7, r4
 80072ac:	193a      	adds	r2, r7, r4
 80072ae:	7812      	ldrb	r2, [r2, #0]
 80072b0:	430a      	orrs	r2, r1
 80072b2:	701a      	strb	r2, [r3, #0]
 80072b4:	687b      	ldr	r3, [r7, #4]
 80072b6:	0018      	movs	r0, r3
 80072b8:	f7ff fea0 	bl	8006ffc <VL53L0X_device_read_strobe>
 80072bc:	0003      	movs	r3, r0
 80072be:	0019      	movs	r1, r3
 80072c0:	193b      	adds	r3, r7, r4
 80072c2:	193a      	adds	r2, r7, r4
 80072c4:	7812      	ldrb	r2, [r2, #0]
 80072c6:	430a      	orrs	r2, r1
 80072c8:	701a      	strb	r2, [r3, #0]
 80072ca:	19ba      	adds	r2, r7, r6
 80072cc:	687b      	ldr	r3, [r7, #4]
 80072ce:	2190      	movs	r1, #144	; 0x90
 80072d0:	0018      	movs	r0, r3
 80072d2:	f7fc fbb5 	bl	8003a40 <VL53L0X_RdDWord>
 80072d6:	0003      	movs	r3, r0
 80072d8:	0019      	movs	r1, r3
 80072da:	193b      	adds	r3, r7, r4
 80072dc:	193a      	adds	r2, r7, r4
 80072de:	7812      	ldrb	r2, [r2, #0]
 80072e0:	430a      	orrs	r2, r1
 80072e2:	701a      	strb	r2, [r3, #0]
 80072e4:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80072e6:	0e1b      	lsrs	r3, r3, #24
 80072e8:	b2da      	uxtb	r2, r3
 80072ea:	2508      	movs	r5, #8
 80072ec:	197b      	adds	r3, r7, r5
 80072ee:	701a      	strb	r2, [r3, #0]
 80072f0:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80072f2:	0c1b      	lsrs	r3, r3, #16
 80072f4:	b2da      	uxtb	r2, r3
 80072f6:	197b      	adds	r3, r7, r5
 80072f8:	705a      	strb	r2, [r3, #1]
 80072fa:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80072fc:	0a1b      	lsrs	r3, r3, #8
 80072fe:	b2da      	uxtb	r2, r3
 8007300:	197b      	adds	r3, r7, r5
 8007302:	709a      	strb	r2, [r3, #2]
 8007304:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007306:	b2da      	uxtb	r2, r3
 8007308:	197b      	adds	r3, r7, r5
 800730a:	70da      	strb	r2, [r3, #3]
 800730c:	687b      	ldr	r3, [r7, #4]
 800730e:	2225      	movs	r2, #37	; 0x25
 8007310:	2194      	movs	r1, #148	; 0x94
 8007312:	0018      	movs	r0, r3
 8007314:	f7fc faee 	bl	80038f4 <VL53L0X_WrByte>
 8007318:	0003      	movs	r3, r0
 800731a:	0019      	movs	r1, r3
 800731c:	193b      	adds	r3, r7, r4
 800731e:	193a      	adds	r2, r7, r4
 8007320:	7812      	ldrb	r2, [r2, #0]
 8007322:	430a      	orrs	r2, r1
 8007324:	701a      	strb	r2, [r3, #0]
 8007326:	687b      	ldr	r3, [r7, #4]
 8007328:	0018      	movs	r0, r3
 800732a:	f7ff fe67 	bl	8006ffc <VL53L0X_device_read_strobe>
 800732e:	0003      	movs	r3, r0
 8007330:	0019      	movs	r1, r3
 8007332:	193b      	adds	r3, r7, r4
 8007334:	193a      	adds	r2, r7, r4
 8007336:	7812      	ldrb	r2, [r2, #0]
 8007338:	430a      	orrs	r2, r1
 800733a:	701a      	strb	r2, [r3, #0]
 800733c:	19ba      	adds	r2, r7, r6
 800733e:	687b      	ldr	r3, [r7, #4]
 8007340:	2190      	movs	r1, #144	; 0x90
 8007342:	0018      	movs	r0, r3
 8007344:	f7fc fb7c 	bl	8003a40 <VL53L0X_RdDWord>
 8007348:	0003      	movs	r3, r0
 800734a:	0019      	movs	r1, r3
 800734c:	193b      	adds	r3, r7, r4
 800734e:	193a      	adds	r2, r7, r4
 8007350:	7812      	ldrb	r2, [r2, #0]
 8007352:	430a      	orrs	r2, r1
 8007354:	701a      	strb	r2, [r3, #0]
 8007356:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007358:	0e1b      	lsrs	r3, r3, #24
 800735a:	b2da      	uxtb	r2, r3
 800735c:	197b      	adds	r3, r7, r5
 800735e:	711a      	strb	r2, [r3, #4]
 8007360:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007362:	0c1b      	lsrs	r3, r3, #16
 8007364:	b2da      	uxtb	r2, r3
 8007366:	197b      	adds	r3, r7, r5
 8007368:	715a      	strb	r2, [r3, #5]
 800736a:	1cfb      	adds	r3, r7, #3
 800736c:	781b      	ldrb	r3, [r3, #0]
 800736e:	2202      	movs	r2, #2
 8007370:	4013      	ands	r3, r2
 8007372:	d100      	bne.n	8007376 <VL53L0X_get_info_from_device+0x2ce>
 8007374:	e1aa      	b.n	80076cc <VL53L0X_get_info_from_device+0x624>
 8007376:	2337      	movs	r3, #55	; 0x37
 8007378:	18fb      	adds	r3, r7, r3
 800737a:	781b      	ldrb	r3, [r3, #0]
 800737c:	2202      	movs	r2, #2
 800737e:	4013      	ands	r3, r2
 8007380:	d000      	beq.n	8007384 <VL53L0X_get_info_from_device+0x2dc>
 8007382:	e1a3      	b.n	80076cc <VL53L0X_get_info_from_device+0x624>
 8007384:	687b      	ldr	r3, [r7, #4]
 8007386:	2202      	movs	r2, #2
 8007388:	2194      	movs	r1, #148	; 0x94
 800738a:	0018      	movs	r0, r3
 800738c:	f7fc fab2 	bl	80038f4 <VL53L0X_WrByte>
 8007390:	0003      	movs	r3, r0
 8007392:	0019      	movs	r1, r3
 8007394:	2457      	movs	r4, #87	; 0x57
 8007396:	193b      	adds	r3, r7, r4
 8007398:	193a      	adds	r2, r7, r4
 800739a:	7812      	ldrb	r2, [r2, #0]
 800739c:	430a      	orrs	r2, r1
 800739e:	701a      	strb	r2, [r3, #0]
 80073a0:	687b      	ldr	r3, [r7, #4]
 80073a2:	0018      	movs	r0, r3
 80073a4:	f7ff fe2a 	bl	8006ffc <VL53L0X_device_read_strobe>
 80073a8:	0003      	movs	r3, r0
 80073aa:	0019      	movs	r1, r3
 80073ac:	193b      	adds	r3, r7, r4
 80073ae:	193a      	adds	r2, r7, r4
 80073b0:	7812      	ldrb	r2, [r2, #0]
 80073b2:	430a      	orrs	r2, r1
 80073b4:	701a      	strb	r2, [r3, #0]
 80073b6:	232f      	movs	r3, #47	; 0x2f
 80073b8:	18fa      	adds	r2, r7, r3
 80073ba:	687b      	ldr	r3, [r7, #4]
 80073bc:	2190      	movs	r1, #144	; 0x90
 80073be:	0018      	movs	r0, r3
 80073c0:	f7fc fb06 	bl	80039d0 <VL53L0X_RdByte>
 80073c4:	0003      	movs	r3, r0
 80073c6:	0019      	movs	r1, r3
 80073c8:	193b      	adds	r3, r7, r4
 80073ca:	193a      	adds	r2, r7, r4
 80073cc:	7812      	ldrb	r2, [r2, #0]
 80073ce:	430a      	orrs	r2, r1
 80073d0:	701a      	strb	r2, [r3, #0]
 80073d2:	687b      	ldr	r3, [r7, #4]
 80073d4:	227b      	movs	r2, #123	; 0x7b
 80073d6:	2194      	movs	r1, #148	; 0x94
 80073d8:	0018      	movs	r0, r3
 80073da:	f7fc fa8b 	bl	80038f4 <VL53L0X_WrByte>
 80073de:	0003      	movs	r3, r0
 80073e0:	0019      	movs	r1, r3
 80073e2:	193b      	adds	r3, r7, r4
 80073e4:	193a      	adds	r2, r7, r4
 80073e6:	7812      	ldrb	r2, [r2, #0]
 80073e8:	430a      	orrs	r2, r1
 80073ea:	701a      	strb	r2, [r3, #0]
 80073ec:	687b      	ldr	r3, [r7, #4]
 80073ee:	0018      	movs	r0, r3
 80073f0:	f7ff fe04 	bl	8006ffc <VL53L0X_device_read_strobe>
 80073f4:	0003      	movs	r3, r0
 80073f6:	0019      	movs	r1, r3
 80073f8:	193b      	adds	r3, r7, r4
 80073fa:	193a      	adds	r2, r7, r4
 80073fc:	7812      	ldrb	r2, [r2, #0]
 80073fe:	430a      	orrs	r2, r1
 8007400:	701a      	strb	r2, [r3, #0]
 8007402:	232e      	movs	r3, #46	; 0x2e
 8007404:	18fa      	adds	r2, r7, r3
 8007406:	687b      	ldr	r3, [r7, #4]
 8007408:	2190      	movs	r1, #144	; 0x90
 800740a:	0018      	movs	r0, r3
 800740c:	f7fc fae0 	bl	80039d0 <VL53L0X_RdByte>
 8007410:	0003      	movs	r3, r0
 8007412:	0019      	movs	r1, r3
 8007414:	193b      	adds	r3, r7, r4
 8007416:	193a      	adds	r2, r7, r4
 8007418:	7812      	ldrb	r2, [r2, #0]
 800741a:	430a      	orrs	r2, r1
 800741c:	701a      	strb	r2, [r3, #0]
 800741e:	687b      	ldr	r3, [r7, #4]
 8007420:	2277      	movs	r2, #119	; 0x77
 8007422:	2194      	movs	r1, #148	; 0x94
 8007424:	0018      	movs	r0, r3
 8007426:	f7fc fa65 	bl	80038f4 <VL53L0X_WrByte>
 800742a:	0003      	movs	r3, r0
 800742c:	0019      	movs	r1, r3
 800742e:	193b      	adds	r3, r7, r4
 8007430:	193a      	adds	r2, r7, r4
 8007432:	7812      	ldrb	r2, [r2, #0]
 8007434:	430a      	orrs	r2, r1
 8007436:	701a      	strb	r2, [r3, #0]
 8007438:	687b      	ldr	r3, [r7, #4]
 800743a:	0018      	movs	r0, r3
 800743c:	f7ff fdde 	bl	8006ffc <VL53L0X_device_read_strobe>
 8007440:	0003      	movs	r3, r0
 8007442:	0019      	movs	r1, r3
 8007444:	193b      	adds	r3, r7, r4
 8007446:	193a      	adds	r2, r7, r4
 8007448:	7812      	ldrb	r2, [r2, #0]
 800744a:	430a      	orrs	r2, r1
 800744c:	701a      	strb	r2, [r3, #0]
 800744e:	2330      	movs	r3, #48	; 0x30
 8007450:	18fa      	adds	r2, r7, r3
 8007452:	687b      	ldr	r3, [r7, #4]
 8007454:	2190      	movs	r1, #144	; 0x90
 8007456:	0018      	movs	r0, r3
 8007458:	f7fc faf2 	bl	8003a40 <VL53L0X_RdDWord>
 800745c:	0003      	movs	r3, r0
 800745e:	0019      	movs	r1, r3
 8007460:	193b      	adds	r3, r7, r4
 8007462:	193a      	adds	r2, r7, r4
 8007464:	7812      	ldrb	r2, [r2, #0]
 8007466:	430a      	orrs	r2, r1
 8007468:	701a      	strb	r2, [r3, #0]
 800746a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800746c:	0e5b      	lsrs	r3, r3, #25
 800746e:	b2db      	uxtb	r3, r3
 8007470:	227f      	movs	r2, #127	; 0x7f
 8007472:	4013      	ands	r3, r2
 8007474:	b2da      	uxtb	r2, r3
 8007476:	2510      	movs	r5, #16
 8007478:	197b      	adds	r3, r7, r5
 800747a:	701a      	strb	r2, [r3, #0]
 800747c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800747e:	0c9b      	lsrs	r3, r3, #18
 8007480:	b2db      	uxtb	r3, r3
 8007482:	227f      	movs	r2, #127	; 0x7f
 8007484:	4013      	ands	r3, r2
 8007486:	b2da      	uxtb	r2, r3
 8007488:	197b      	adds	r3, r7, r5
 800748a:	705a      	strb	r2, [r3, #1]
 800748c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800748e:	0adb      	lsrs	r3, r3, #11
 8007490:	b2db      	uxtb	r3, r3
 8007492:	227f      	movs	r2, #127	; 0x7f
 8007494:	4013      	ands	r3, r2
 8007496:	b2da      	uxtb	r2, r3
 8007498:	197b      	adds	r3, r7, r5
 800749a:	709a      	strb	r2, [r3, #2]
 800749c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800749e:	091b      	lsrs	r3, r3, #4
 80074a0:	b2db      	uxtb	r3, r3
 80074a2:	227f      	movs	r2, #127	; 0x7f
 80074a4:	4013      	ands	r3, r2
 80074a6:	b2da      	uxtb	r2, r3
 80074a8:	197b      	adds	r3, r7, r5
 80074aa:	70da      	strb	r2, [r3, #3]
 80074ac:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80074ae:	b2db      	uxtb	r3, r3
 80074b0:	00db      	lsls	r3, r3, #3
 80074b2:	b2db      	uxtb	r3, r3
 80074b4:	2278      	movs	r2, #120	; 0x78
 80074b6:	4013      	ands	r3, r2
 80074b8:	b2da      	uxtb	r2, r3
 80074ba:	2636      	movs	r6, #54	; 0x36
 80074bc:	19bb      	adds	r3, r7, r6
 80074be:	701a      	strb	r2, [r3, #0]
 80074c0:	687b      	ldr	r3, [r7, #4]
 80074c2:	2278      	movs	r2, #120	; 0x78
 80074c4:	2194      	movs	r1, #148	; 0x94
 80074c6:	0018      	movs	r0, r3
 80074c8:	f7fc fa14 	bl	80038f4 <VL53L0X_WrByte>
 80074cc:	0003      	movs	r3, r0
 80074ce:	0019      	movs	r1, r3
 80074d0:	193b      	adds	r3, r7, r4
 80074d2:	193a      	adds	r2, r7, r4
 80074d4:	7812      	ldrb	r2, [r2, #0]
 80074d6:	430a      	orrs	r2, r1
 80074d8:	701a      	strb	r2, [r3, #0]
 80074da:	687b      	ldr	r3, [r7, #4]
 80074dc:	0018      	movs	r0, r3
 80074de:	f7ff fd8d 	bl	8006ffc <VL53L0X_device_read_strobe>
 80074e2:	0003      	movs	r3, r0
 80074e4:	0019      	movs	r1, r3
 80074e6:	193b      	adds	r3, r7, r4
 80074e8:	193a      	adds	r2, r7, r4
 80074ea:	7812      	ldrb	r2, [r2, #0]
 80074ec:	430a      	orrs	r2, r1
 80074ee:	701a      	strb	r2, [r3, #0]
 80074f0:	2330      	movs	r3, #48	; 0x30
 80074f2:	18fa      	adds	r2, r7, r3
 80074f4:	687b      	ldr	r3, [r7, #4]
 80074f6:	2190      	movs	r1, #144	; 0x90
 80074f8:	0018      	movs	r0, r3
 80074fa:	f7fc faa1 	bl	8003a40 <VL53L0X_RdDWord>
 80074fe:	0003      	movs	r3, r0
 8007500:	0019      	movs	r1, r3
 8007502:	193b      	adds	r3, r7, r4
 8007504:	193a      	adds	r2, r7, r4
 8007506:	7812      	ldrb	r2, [r2, #0]
 8007508:	430a      	orrs	r2, r1
 800750a:	701a      	strb	r2, [r3, #0]
 800750c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800750e:	0f5b      	lsrs	r3, r3, #29
 8007510:	b2db      	uxtb	r3, r3
 8007512:	227f      	movs	r2, #127	; 0x7f
 8007514:	4013      	ands	r3, r2
 8007516:	b2da      	uxtb	r2, r3
 8007518:	0031      	movs	r1, r6
 800751a:	187b      	adds	r3, r7, r1
 800751c:	781b      	ldrb	r3, [r3, #0]
 800751e:	18d3      	adds	r3, r2, r3
 8007520:	b2da      	uxtb	r2, r3
 8007522:	197b      	adds	r3, r7, r5
 8007524:	711a      	strb	r2, [r3, #4]
 8007526:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007528:	0d9b      	lsrs	r3, r3, #22
 800752a:	b2db      	uxtb	r3, r3
 800752c:	227f      	movs	r2, #127	; 0x7f
 800752e:	4013      	ands	r3, r2
 8007530:	b2da      	uxtb	r2, r3
 8007532:	197b      	adds	r3, r7, r5
 8007534:	715a      	strb	r2, [r3, #5]
 8007536:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007538:	0bdb      	lsrs	r3, r3, #15
 800753a:	b2db      	uxtb	r3, r3
 800753c:	227f      	movs	r2, #127	; 0x7f
 800753e:	4013      	ands	r3, r2
 8007540:	b2da      	uxtb	r2, r3
 8007542:	197b      	adds	r3, r7, r5
 8007544:	719a      	strb	r2, [r3, #6]
 8007546:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007548:	0a1b      	lsrs	r3, r3, #8
 800754a:	b2db      	uxtb	r3, r3
 800754c:	227f      	movs	r2, #127	; 0x7f
 800754e:	4013      	ands	r3, r2
 8007550:	b2da      	uxtb	r2, r3
 8007552:	197b      	adds	r3, r7, r5
 8007554:	71da      	strb	r2, [r3, #7]
 8007556:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007558:	085b      	lsrs	r3, r3, #1
 800755a:	b2db      	uxtb	r3, r3
 800755c:	227f      	movs	r2, #127	; 0x7f
 800755e:	4013      	ands	r3, r2
 8007560:	b2da      	uxtb	r2, r3
 8007562:	197b      	adds	r3, r7, r5
 8007564:	721a      	strb	r2, [r3, #8]
 8007566:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007568:	b2db      	uxtb	r3, r3
 800756a:	019b      	lsls	r3, r3, #6
 800756c:	b2db      	uxtb	r3, r3
 800756e:	2240      	movs	r2, #64	; 0x40
 8007570:	4013      	ands	r3, r2
 8007572:	b2da      	uxtb	r2, r3
 8007574:	000e      	movs	r6, r1
 8007576:	19bb      	adds	r3, r7, r6
 8007578:	701a      	strb	r2, [r3, #0]
 800757a:	687b      	ldr	r3, [r7, #4]
 800757c:	2279      	movs	r2, #121	; 0x79
 800757e:	2194      	movs	r1, #148	; 0x94
 8007580:	0018      	movs	r0, r3
 8007582:	f7fc f9b7 	bl	80038f4 <VL53L0X_WrByte>
 8007586:	0003      	movs	r3, r0
 8007588:	0019      	movs	r1, r3
 800758a:	193b      	adds	r3, r7, r4
 800758c:	193a      	adds	r2, r7, r4
 800758e:	7812      	ldrb	r2, [r2, #0]
 8007590:	430a      	orrs	r2, r1
 8007592:	701a      	strb	r2, [r3, #0]
 8007594:	687b      	ldr	r3, [r7, #4]
 8007596:	0018      	movs	r0, r3
 8007598:	f7ff fd30 	bl	8006ffc <VL53L0X_device_read_strobe>
 800759c:	0003      	movs	r3, r0
 800759e:	0019      	movs	r1, r3
 80075a0:	193b      	adds	r3, r7, r4
 80075a2:	193a      	adds	r2, r7, r4
 80075a4:	7812      	ldrb	r2, [r2, #0]
 80075a6:	430a      	orrs	r2, r1
 80075a8:	701a      	strb	r2, [r3, #0]
 80075aa:	2330      	movs	r3, #48	; 0x30
 80075ac:	18fa      	adds	r2, r7, r3
 80075ae:	687b      	ldr	r3, [r7, #4]
 80075b0:	2190      	movs	r1, #144	; 0x90
 80075b2:	0018      	movs	r0, r3
 80075b4:	f7fc fa44 	bl	8003a40 <VL53L0X_RdDWord>
 80075b8:	0003      	movs	r3, r0
 80075ba:	0019      	movs	r1, r3
 80075bc:	193b      	adds	r3, r7, r4
 80075be:	193a      	adds	r2, r7, r4
 80075c0:	7812      	ldrb	r2, [r2, #0]
 80075c2:	430a      	orrs	r2, r1
 80075c4:	701a      	strb	r2, [r3, #0]
 80075c6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075c8:	0e9b      	lsrs	r3, r3, #26
 80075ca:	b2db      	uxtb	r3, r3
 80075cc:	227f      	movs	r2, #127	; 0x7f
 80075ce:	4013      	ands	r3, r2
 80075d0:	b2da      	uxtb	r2, r3
 80075d2:	19bb      	adds	r3, r7, r6
 80075d4:	781b      	ldrb	r3, [r3, #0]
 80075d6:	18d3      	adds	r3, r2, r3
 80075d8:	b2da      	uxtb	r2, r3
 80075da:	197b      	adds	r3, r7, r5
 80075dc:	725a      	strb	r2, [r3, #9]
 80075de:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075e0:	0cdb      	lsrs	r3, r3, #19
 80075e2:	b2db      	uxtb	r3, r3
 80075e4:	227f      	movs	r2, #127	; 0x7f
 80075e6:	4013      	ands	r3, r2
 80075e8:	b2da      	uxtb	r2, r3
 80075ea:	197b      	adds	r3, r7, r5
 80075ec:	729a      	strb	r2, [r3, #10]
 80075ee:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80075f0:	0b1b      	lsrs	r3, r3, #12
 80075f2:	b2db      	uxtb	r3, r3
 80075f4:	227f      	movs	r2, #127	; 0x7f
 80075f6:	4013      	ands	r3, r2
 80075f8:	b2da      	uxtb	r2, r3
 80075fa:	197b      	adds	r3, r7, r5
 80075fc:	72da      	strb	r2, [r3, #11]
 80075fe:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007600:	095b      	lsrs	r3, r3, #5
 8007602:	b2db      	uxtb	r3, r3
 8007604:	227f      	movs	r2, #127	; 0x7f
 8007606:	4013      	ands	r3, r2
 8007608:	b2da      	uxtb	r2, r3
 800760a:	197b      	adds	r3, r7, r5
 800760c:	731a      	strb	r2, [r3, #12]
 800760e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007610:	b2db      	uxtb	r3, r3
 8007612:	009b      	lsls	r3, r3, #2
 8007614:	b2db      	uxtb	r3, r3
 8007616:	227c      	movs	r2, #124	; 0x7c
 8007618:	4013      	ands	r3, r2
 800761a:	b2da      	uxtb	r2, r3
 800761c:	19bb      	adds	r3, r7, r6
 800761e:	701a      	strb	r2, [r3, #0]
 8007620:	687b      	ldr	r3, [r7, #4]
 8007622:	227a      	movs	r2, #122	; 0x7a
 8007624:	2194      	movs	r1, #148	; 0x94
 8007626:	0018      	movs	r0, r3
 8007628:	f7fc f964 	bl	80038f4 <VL53L0X_WrByte>
 800762c:	0003      	movs	r3, r0
 800762e:	0019      	movs	r1, r3
 8007630:	193b      	adds	r3, r7, r4
 8007632:	193a      	adds	r2, r7, r4
 8007634:	7812      	ldrb	r2, [r2, #0]
 8007636:	430a      	orrs	r2, r1
 8007638:	701a      	strb	r2, [r3, #0]
 800763a:	687b      	ldr	r3, [r7, #4]
 800763c:	0018      	movs	r0, r3
 800763e:	f7ff fcdd 	bl	8006ffc <VL53L0X_device_read_strobe>
 8007642:	0003      	movs	r3, r0
 8007644:	0019      	movs	r1, r3
 8007646:	193b      	adds	r3, r7, r4
 8007648:	193a      	adds	r2, r7, r4
 800764a:	7812      	ldrb	r2, [r2, #0]
 800764c:	430a      	orrs	r2, r1
 800764e:	701a      	strb	r2, [r3, #0]
 8007650:	2330      	movs	r3, #48	; 0x30
 8007652:	18fa      	adds	r2, r7, r3
 8007654:	687b      	ldr	r3, [r7, #4]
 8007656:	2190      	movs	r1, #144	; 0x90
 8007658:	0018      	movs	r0, r3
 800765a:	f7fc f9f1 	bl	8003a40 <VL53L0X_RdDWord>
 800765e:	0003      	movs	r3, r0
 8007660:	0019      	movs	r1, r3
 8007662:	193b      	adds	r3, r7, r4
 8007664:	193a      	adds	r2, r7, r4
 8007666:	7812      	ldrb	r2, [r2, #0]
 8007668:	430a      	orrs	r2, r1
 800766a:	701a      	strb	r2, [r3, #0]
 800766c:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800766e:	0f9b      	lsrs	r3, r3, #30
 8007670:	b2db      	uxtb	r3, r3
 8007672:	227f      	movs	r2, #127	; 0x7f
 8007674:	4013      	ands	r3, r2
 8007676:	b2da      	uxtb	r2, r3
 8007678:	19bb      	adds	r3, r7, r6
 800767a:	781b      	ldrb	r3, [r3, #0]
 800767c:	18d3      	adds	r3, r2, r3
 800767e:	b2da      	uxtb	r2, r3
 8007680:	0029      	movs	r1, r5
 8007682:	187b      	adds	r3, r7, r1
 8007684:	735a      	strb	r2, [r3, #13]
 8007686:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007688:	0ddb      	lsrs	r3, r3, #23
 800768a:	b2db      	uxtb	r3, r3
 800768c:	227f      	movs	r2, #127	; 0x7f
 800768e:	4013      	ands	r3, r2
 8007690:	b2da      	uxtb	r2, r3
 8007692:	187b      	adds	r3, r7, r1
 8007694:	739a      	strb	r2, [r3, #14]
 8007696:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007698:	0c1b      	lsrs	r3, r3, #16
 800769a:	b2db      	uxtb	r3, r3
 800769c:	227f      	movs	r2, #127	; 0x7f
 800769e:	4013      	ands	r3, r2
 80076a0:	b2da      	uxtb	r2, r3
 80076a2:	187b      	adds	r3, r7, r1
 80076a4:	73da      	strb	r2, [r3, #15]
 80076a6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80076a8:	0a5b      	lsrs	r3, r3, #9
 80076aa:	b2db      	uxtb	r3, r3
 80076ac:	227f      	movs	r2, #127	; 0x7f
 80076ae:	4013      	ands	r3, r2
 80076b0:	b2da      	uxtb	r2, r3
 80076b2:	187b      	adds	r3, r7, r1
 80076b4:	741a      	strb	r2, [r3, #16]
 80076b6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80076b8:	089b      	lsrs	r3, r3, #2
 80076ba:	b2db      	uxtb	r3, r3
 80076bc:	227f      	movs	r2, #127	; 0x7f
 80076be:	4013      	ands	r3, r2
 80076c0:	b2da      	uxtb	r2, r3
 80076c2:	187b      	adds	r3, r7, r1
 80076c4:	745a      	strb	r2, [r3, #17]
 80076c6:	187b      	adds	r3, r7, r1
 80076c8:	2200      	movs	r2, #0
 80076ca:	749a      	strb	r2, [r3, #18]
 80076cc:	1cfb      	adds	r3, r7, #3
 80076ce:	781b      	ldrb	r3, [r3, #0]
 80076d0:	2204      	movs	r2, #4
 80076d2:	4013      	ands	r3, r2
 80076d4:	d100      	bne.n	80076d8 <VL53L0X_get_info_from_device+0x630>
 80076d6:	e0fc      	b.n	80078d2 <VL53L0X_get_info_from_device+0x82a>
 80076d8:	2337      	movs	r3, #55	; 0x37
 80076da:	18fb      	adds	r3, r7, r3
 80076dc:	781b      	ldrb	r3, [r3, #0]
 80076de:	2204      	movs	r2, #4
 80076e0:	4013      	ands	r3, r2
 80076e2:	d000      	beq.n	80076e6 <VL53L0X_get_info_from_device+0x63e>
 80076e4:	e0f5      	b.n	80078d2 <VL53L0X_get_info_from_device+0x82a>
 80076e6:	687b      	ldr	r3, [r7, #4]
 80076e8:	227b      	movs	r2, #123	; 0x7b
 80076ea:	2194      	movs	r1, #148	; 0x94
 80076ec:	0018      	movs	r0, r3
 80076ee:	f7fc f901 	bl	80038f4 <VL53L0X_WrByte>
 80076f2:	0003      	movs	r3, r0
 80076f4:	0019      	movs	r1, r3
 80076f6:	2457      	movs	r4, #87	; 0x57
 80076f8:	193b      	adds	r3, r7, r4
 80076fa:	193a      	adds	r2, r7, r4
 80076fc:	7812      	ldrb	r2, [r2, #0]
 80076fe:	430a      	orrs	r2, r1
 8007700:	701a      	strb	r2, [r3, #0]
 8007702:	687b      	ldr	r3, [r7, #4]
 8007704:	0018      	movs	r0, r3
 8007706:	f7ff fc79 	bl	8006ffc <VL53L0X_device_read_strobe>
 800770a:	0003      	movs	r3, r0
 800770c:	0019      	movs	r1, r3
 800770e:	193b      	adds	r3, r7, r4
 8007710:	193a      	adds	r2, r7, r4
 8007712:	7812      	ldrb	r2, [r2, #0]
 8007714:	430a      	orrs	r2, r1
 8007716:	701a      	strb	r2, [r3, #0]
 8007718:	2328      	movs	r3, #40	; 0x28
 800771a:	18fa      	adds	r2, r7, r3
 800771c:	687b      	ldr	r3, [r7, #4]
 800771e:	2190      	movs	r1, #144	; 0x90
 8007720:	0018      	movs	r0, r3
 8007722:	f7fc f98d 	bl	8003a40 <VL53L0X_RdDWord>
 8007726:	0003      	movs	r3, r0
 8007728:	0019      	movs	r1, r3
 800772a:	193b      	adds	r3, r7, r4
 800772c:	193a      	adds	r2, r7, r4
 800772e:	7812      	ldrb	r2, [r2, #0]
 8007730:	430a      	orrs	r2, r1
 8007732:	701a      	strb	r2, [r3, #0]
 8007734:	687b      	ldr	r3, [r7, #4]
 8007736:	227c      	movs	r2, #124	; 0x7c
 8007738:	2194      	movs	r1, #148	; 0x94
 800773a:	0018      	movs	r0, r3
 800773c:	f7fc f8da 	bl	80038f4 <VL53L0X_WrByte>
 8007740:	0003      	movs	r3, r0
 8007742:	0019      	movs	r1, r3
 8007744:	193b      	adds	r3, r7, r4
 8007746:	193a      	adds	r2, r7, r4
 8007748:	7812      	ldrb	r2, [r2, #0]
 800774a:	430a      	orrs	r2, r1
 800774c:	701a      	strb	r2, [r3, #0]
 800774e:	687b      	ldr	r3, [r7, #4]
 8007750:	0018      	movs	r0, r3
 8007752:	f7ff fc53 	bl	8006ffc <VL53L0X_device_read_strobe>
 8007756:	0003      	movs	r3, r0
 8007758:	0019      	movs	r1, r3
 800775a:	193b      	adds	r3, r7, r4
 800775c:	193a      	adds	r2, r7, r4
 800775e:	7812      	ldrb	r2, [r2, #0]
 8007760:	430a      	orrs	r2, r1
 8007762:	701a      	strb	r2, [r3, #0]
 8007764:	2324      	movs	r3, #36	; 0x24
 8007766:	18fa      	adds	r2, r7, r3
 8007768:	687b      	ldr	r3, [r7, #4]
 800776a:	2190      	movs	r1, #144	; 0x90
 800776c:	0018      	movs	r0, r3
 800776e:	f7fc f967 	bl	8003a40 <VL53L0X_RdDWord>
 8007772:	0003      	movs	r3, r0
 8007774:	0019      	movs	r1, r3
 8007776:	193b      	adds	r3, r7, r4
 8007778:	193a      	adds	r2, r7, r4
 800777a:	7812      	ldrb	r2, [r2, #0]
 800777c:	430a      	orrs	r2, r1
 800777e:	701a      	strb	r2, [r3, #0]
 8007780:	687b      	ldr	r3, [r7, #4]
 8007782:	2273      	movs	r2, #115	; 0x73
 8007784:	2194      	movs	r1, #148	; 0x94
 8007786:	0018      	movs	r0, r3
 8007788:	f7fc f8b4 	bl	80038f4 <VL53L0X_WrByte>
 800778c:	0003      	movs	r3, r0
 800778e:	0019      	movs	r1, r3
 8007790:	193b      	adds	r3, r7, r4
 8007792:	193a      	adds	r2, r7, r4
 8007794:	7812      	ldrb	r2, [r2, #0]
 8007796:	430a      	orrs	r2, r1
 8007798:	701a      	strb	r2, [r3, #0]
 800779a:	687b      	ldr	r3, [r7, #4]
 800779c:	0018      	movs	r0, r3
 800779e:	f7ff fc2d 	bl	8006ffc <VL53L0X_device_read_strobe>
 80077a2:	0003      	movs	r3, r0
 80077a4:	0019      	movs	r1, r3
 80077a6:	193b      	adds	r3, r7, r4
 80077a8:	193a      	adds	r2, r7, r4
 80077aa:	7812      	ldrb	r2, [r2, #0]
 80077ac:	430a      	orrs	r2, r1
 80077ae:	701a      	strb	r2, [r3, #0]
 80077b0:	2530      	movs	r5, #48	; 0x30
 80077b2:	197a      	adds	r2, r7, r5
 80077b4:	687b      	ldr	r3, [r7, #4]
 80077b6:	2190      	movs	r1, #144	; 0x90
 80077b8:	0018      	movs	r0, r3
 80077ba:	f7fc f941 	bl	8003a40 <VL53L0X_RdDWord>
 80077be:	0003      	movs	r3, r0
 80077c0:	0019      	movs	r1, r3
 80077c2:	193b      	adds	r3, r7, r4
 80077c4:	193a      	adds	r2, r7, r4
 80077c6:	7812      	ldrb	r2, [r2, #0]
 80077c8:	430a      	orrs	r2, r1
 80077ca:	701a      	strb	r2, [r3, #0]
 80077cc:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80077ce:	021b      	lsls	r3, r3, #8
 80077d0:	041b      	lsls	r3, r3, #16
 80077d2:	0c1b      	lsrs	r3, r3, #16
 80077d4:	64bb      	str	r3, [r7, #72]	; 0x48
 80077d6:	687b      	ldr	r3, [r7, #4]
 80077d8:	2274      	movs	r2, #116	; 0x74
 80077da:	2194      	movs	r1, #148	; 0x94
 80077dc:	0018      	movs	r0, r3
 80077de:	f7fc f889 	bl	80038f4 <VL53L0X_WrByte>
 80077e2:	0003      	movs	r3, r0
 80077e4:	0019      	movs	r1, r3
 80077e6:	193b      	adds	r3, r7, r4
 80077e8:	193a      	adds	r2, r7, r4
 80077ea:	7812      	ldrb	r2, [r2, #0]
 80077ec:	430a      	orrs	r2, r1
 80077ee:	701a      	strb	r2, [r3, #0]
 80077f0:	687b      	ldr	r3, [r7, #4]
 80077f2:	0018      	movs	r0, r3
 80077f4:	f7ff fc02 	bl	8006ffc <VL53L0X_device_read_strobe>
 80077f8:	0003      	movs	r3, r0
 80077fa:	0019      	movs	r1, r3
 80077fc:	193b      	adds	r3, r7, r4
 80077fe:	193a      	adds	r2, r7, r4
 8007800:	7812      	ldrb	r2, [r2, #0]
 8007802:	430a      	orrs	r2, r1
 8007804:	701a      	strb	r2, [r3, #0]
 8007806:	197a      	adds	r2, r7, r5
 8007808:	687b      	ldr	r3, [r7, #4]
 800780a:	2190      	movs	r1, #144	; 0x90
 800780c:	0018      	movs	r0, r3
 800780e:	f7fc f917 	bl	8003a40 <VL53L0X_RdDWord>
 8007812:	0003      	movs	r3, r0
 8007814:	0019      	movs	r1, r3
 8007816:	193b      	adds	r3, r7, r4
 8007818:	193a      	adds	r2, r7, r4
 800781a:	7812      	ldrb	r2, [r2, #0]
 800781c:	430a      	orrs	r2, r1
 800781e:	701a      	strb	r2, [r3, #0]
 8007820:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007822:	0e1b      	lsrs	r3, r3, #24
 8007824:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8007826:	4313      	orrs	r3, r2
 8007828:	64bb      	str	r3, [r7, #72]	; 0x48
 800782a:	687b      	ldr	r3, [r7, #4]
 800782c:	2275      	movs	r2, #117	; 0x75
 800782e:	2194      	movs	r1, #148	; 0x94
 8007830:	0018      	movs	r0, r3
 8007832:	f7fc f85f 	bl	80038f4 <VL53L0X_WrByte>
 8007836:	0003      	movs	r3, r0
 8007838:	0019      	movs	r1, r3
 800783a:	193b      	adds	r3, r7, r4
 800783c:	193a      	adds	r2, r7, r4
 800783e:	7812      	ldrb	r2, [r2, #0]
 8007840:	430a      	orrs	r2, r1
 8007842:	701a      	strb	r2, [r3, #0]
 8007844:	687b      	ldr	r3, [r7, #4]
 8007846:	0018      	movs	r0, r3
 8007848:	f7ff fbd8 	bl	8006ffc <VL53L0X_device_read_strobe>
 800784c:	0003      	movs	r3, r0
 800784e:	0019      	movs	r1, r3
 8007850:	193b      	adds	r3, r7, r4
 8007852:	193a      	adds	r2, r7, r4
 8007854:	7812      	ldrb	r2, [r2, #0]
 8007856:	430a      	orrs	r2, r1
 8007858:	701a      	strb	r2, [r3, #0]
 800785a:	197a      	adds	r2, r7, r5
 800785c:	687b      	ldr	r3, [r7, #4]
 800785e:	2190      	movs	r1, #144	; 0x90
 8007860:	0018      	movs	r0, r3
 8007862:	f7fc f8ed 	bl	8003a40 <VL53L0X_RdDWord>
 8007866:	0003      	movs	r3, r0
 8007868:	0019      	movs	r1, r3
 800786a:	193b      	adds	r3, r7, r4
 800786c:	193a      	adds	r2, r7, r4
 800786e:	7812      	ldrb	r2, [r2, #0]
 8007870:	430a      	orrs	r2, r1
 8007872:	701a      	strb	r2, [r3, #0]
 8007874:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8007876:	021b      	lsls	r3, r3, #8
 8007878:	041b      	lsls	r3, r3, #16
 800787a:	0c1b      	lsrs	r3, r3, #16
 800787c:	64fb      	str	r3, [r7, #76]	; 0x4c
 800787e:	687b      	ldr	r3, [r7, #4]
 8007880:	2276      	movs	r2, #118	; 0x76
 8007882:	2194      	movs	r1, #148	; 0x94
 8007884:	0018      	movs	r0, r3
 8007886:	f7fc f835 	bl	80038f4 <VL53L0X_WrByte>
 800788a:	0003      	movs	r3, r0
 800788c:	0019      	movs	r1, r3
 800788e:	193b      	adds	r3, r7, r4
 8007890:	193a      	adds	r2, r7, r4
 8007892:	7812      	ldrb	r2, [r2, #0]
 8007894:	430a      	orrs	r2, r1
 8007896:	701a      	strb	r2, [r3, #0]
 8007898:	687b      	ldr	r3, [r7, #4]
 800789a:	0018      	movs	r0, r3
 800789c:	f7ff fbae 	bl	8006ffc <VL53L0X_device_read_strobe>
 80078a0:	0003      	movs	r3, r0
 80078a2:	0019      	movs	r1, r3
 80078a4:	193b      	adds	r3, r7, r4
 80078a6:	193a      	adds	r2, r7, r4
 80078a8:	7812      	ldrb	r2, [r2, #0]
 80078aa:	430a      	orrs	r2, r1
 80078ac:	701a      	strb	r2, [r3, #0]
 80078ae:	197a      	adds	r2, r7, r5
 80078b0:	687b      	ldr	r3, [r7, #4]
 80078b2:	2190      	movs	r1, #144	; 0x90
 80078b4:	0018      	movs	r0, r3
 80078b6:	f7fc f8c3 	bl	8003a40 <VL53L0X_RdDWord>
 80078ba:	0003      	movs	r3, r0
 80078bc:	0019      	movs	r1, r3
 80078be:	193b      	adds	r3, r7, r4
 80078c0:	193a      	adds	r2, r7, r4
 80078c2:	7812      	ldrb	r2, [r2, #0]
 80078c4:	430a      	orrs	r2, r1
 80078c6:	701a      	strb	r2, [r3, #0]
 80078c8:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80078ca:	0e1b      	lsrs	r3, r3, #24
 80078cc:	6cfa      	ldr	r2, [r7, #76]	; 0x4c
 80078ce:	4313      	orrs	r3, r2
 80078d0:	64fb      	str	r3, [r7, #76]	; 0x4c
 80078d2:	687b      	ldr	r3, [r7, #4]
 80078d4:	2200      	movs	r2, #0
 80078d6:	2181      	movs	r1, #129	; 0x81
 80078d8:	0018      	movs	r0, r3
 80078da:	f7fc f80b 	bl	80038f4 <VL53L0X_WrByte>
 80078de:	0003      	movs	r3, r0
 80078e0:	0019      	movs	r1, r3
 80078e2:	2457      	movs	r4, #87	; 0x57
 80078e4:	193b      	adds	r3, r7, r4
 80078e6:	193a      	adds	r2, r7, r4
 80078e8:	7812      	ldrb	r2, [r2, #0]
 80078ea:	430a      	orrs	r2, r1
 80078ec:	701a      	strb	r2, [r3, #0]
 80078ee:	687b      	ldr	r3, [r7, #4]
 80078f0:	2206      	movs	r2, #6
 80078f2:	21ff      	movs	r1, #255	; 0xff
 80078f4:	0018      	movs	r0, r3
 80078f6:	f7fb fffd 	bl	80038f4 <VL53L0X_WrByte>
 80078fa:	0003      	movs	r3, r0
 80078fc:	0019      	movs	r1, r3
 80078fe:	193b      	adds	r3, r7, r4
 8007900:	193a      	adds	r2, r7, r4
 8007902:	7812      	ldrb	r2, [r2, #0]
 8007904:	430a      	orrs	r2, r1
 8007906:	701a      	strb	r2, [r3, #0]
 8007908:	2536      	movs	r5, #54	; 0x36
 800790a:	197a      	adds	r2, r7, r5
 800790c:	687b      	ldr	r3, [r7, #4]
 800790e:	2183      	movs	r1, #131	; 0x83
 8007910:	0018      	movs	r0, r3
 8007912:	f7fc f85d 	bl	80039d0 <VL53L0X_RdByte>
 8007916:	0003      	movs	r3, r0
 8007918:	0019      	movs	r1, r3
 800791a:	193b      	adds	r3, r7, r4
 800791c:	193a      	adds	r2, r7, r4
 800791e:	7812      	ldrb	r2, [r2, #0]
 8007920:	430a      	orrs	r2, r1
 8007922:	701a      	strb	r2, [r3, #0]
 8007924:	197b      	adds	r3, r7, r5
 8007926:	781b      	ldrb	r3, [r3, #0]
 8007928:	2204      	movs	r2, #4
 800792a:	4393      	bics	r3, r2
 800792c:	b2da      	uxtb	r2, r3
 800792e:	687b      	ldr	r3, [r7, #4]
 8007930:	2183      	movs	r1, #131	; 0x83
 8007932:	0018      	movs	r0, r3
 8007934:	f7fb ffde 	bl	80038f4 <VL53L0X_WrByte>
 8007938:	0003      	movs	r3, r0
 800793a:	0019      	movs	r1, r3
 800793c:	193b      	adds	r3, r7, r4
 800793e:	193a      	adds	r2, r7, r4
 8007940:	7812      	ldrb	r2, [r2, #0]
 8007942:	430a      	orrs	r2, r1
 8007944:	701a      	strb	r2, [r3, #0]
 8007946:	687b      	ldr	r3, [r7, #4]
 8007948:	2201      	movs	r2, #1
 800794a:	21ff      	movs	r1, #255	; 0xff
 800794c:	0018      	movs	r0, r3
 800794e:	f7fb ffd1 	bl	80038f4 <VL53L0X_WrByte>
 8007952:	0003      	movs	r3, r0
 8007954:	0019      	movs	r1, r3
 8007956:	193b      	adds	r3, r7, r4
 8007958:	193a      	adds	r2, r7, r4
 800795a:	7812      	ldrb	r2, [r2, #0]
 800795c:	430a      	orrs	r2, r1
 800795e:	701a      	strb	r2, [r3, #0]
 8007960:	687b      	ldr	r3, [r7, #4]
 8007962:	2201      	movs	r2, #1
 8007964:	2100      	movs	r1, #0
 8007966:	0018      	movs	r0, r3
 8007968:	f7fb ffc4 	bl	80038f4 <VL53L0X_WrByte>
 800796c:	0003      	movs	r3, r0
 800796e:	0019      	movs	r1, r3
 8007970:	193b      	adds	r3, r7, r4
 8007972:	193a      	adds	r2, r7, r4
 8007974:	7812      	ldrb	r2, [r2, #0]
 8007976:	430a      	orrs	r2, r1
 8007978:	701a      	strb	r2, [r3, #0]
 800797a:	687b      	ldr	r3, [r7, #4]
 800797c:	2200      	movs	r2, #0
 800797e:	21ff      	movs	r1, #255	; 0xff
 8007980:	0018      	movs	r0, r3
 8007982:	f7fb ffb7 	bl	80038f4 <VL53L0X_WrByte>
 8007986:	0003      	movs	r3, r0
 8007988:	0019      	movs	r1, r3
 800798a:	193b      	adds	r3, r7, r4
 800798c:	193a      	adds	r2, r7, r4
 800798e:	7812      	ldrb	r2, [r2, #0]
 8007990:	430a      	orrs	r2, r1
 8007992:	701a      	strb	r2, [r3, #0]
 8007994:	687b      	ldr	r3, [r7, #4]
 8007996:	2200      	movs	r2, #0
 8007998:	2180      	movs	r1, #128	; 0x80
 800799a:	0018      	movs	r0, r3
 800799c:	f7fb ffaa 	bl	80038f4 <VL53L0X_WrByte>
 80079a0:	0003      	movs	r3, r0
 80079a2:	0019      	movs	r1, r3
 80079a4:	193b      	adds	r3, r7, r4
 80079a6:	193a      	adds	r2, r7, r4
 80079a8:	7812      	ldrb	r2, [r2, #0]
 80079aa:	430a      	orrs	r2, r1
 80079ac:	701a      	strb	r2, [r3, #0]
 80079ae:	2357      	movs	r3, #87	; 0x57
 80079b0:	18fb      	adds	r3, r7, r3
 80079b2:	781b      	ldrb	r3, [r3, #0]
 80079b4:	b25b      	sxtb	r3, r3
 80079b6:	2b00      	cmp	r3, #0
 80079b8:	d000      	beq.n	80079bc <VL53L0X_get_info_from_device+0x914>
 80079ba:	e093      	b.n	8007ae4 <VL53L0X_get_info_from_device+0xa3c>
 80079bc:	2137      	movs	r1, #55	; 0x37
 80079be:	187b      	adds	r3, r7, r1
 80079c0:	781b      	ldrb	r3, [r3, #0]
 80079c2:	2b07      	cmp	r3, #7
 80079c4:	d100      	bne.n	80079c8 <VL53L0X_get_info_from_device+0x920>
 80079c6:	e08d      	b.n	8007ae4 <VL53L0X_get_info_from_device+0xa3c>
 80079c8:	1cfb      	adds	r3, r7, #3
 80079ca:	781b      	ldrb	r3, [r3, #0]
 80079cc:	2201      	movs	r2, #1
 80079ce:	4013      	ands	r3, r2
 80079d0:	d026      	beq.n	8007a20 <VL53L0X_get_info_from_device+0x978>
 80079d2:	187b      	adds	r3, r7, r1
 80079d4:	781b      	ldrb	r3, [r3, #0]
 80079d6:	2201      	movs	r2, #1
 80079d8:	4013      	ands	r3, r2
 80079da:	d121      	bne.n	8007a20 <VL53L0X_get_info_from_device+0x978>
 80079dc:	687b      	ldr	r3, [r7, #4]
 80079de:	2256      	movs	r2, #86	; 0x56
 80079e0:	18ba      	adds	r2, r7, r2
 80079e2:	21f3      	movs	r1, #243	; 0xf3
 80079e4:	7812      	ldrb	r2, [r2, #0]
 80079e6:	545a      	strb	r2, [r3, r1]
 80079e8:	687b      	ldr	r3, [r7, #4]
 80079ea:	2255      	movs	r2, #85	; 0x55
 80079ec:	18ba      	adds	r2, r7, r2
 80079ee:	21f4      	movs	r1, #244	; 0xf4
 80079f0:	7812      	ldrb	r2, [r2, #0]
 80079f2:	545a      	strb	r2, [r3, r1]
 80079f4:	2300      	movs	r3, #0
 80079f6:	647b      	str	r3, [r7, #68]	; 0x44
 80079f8:	e00f      	b.n	8007a1a <VL53L0X_get_info_from_device+0x972>
 80079fa:	2308      	movs	r3, #8
 80079fc:	18fa      	adds	r2, r7, r3
 80079fe:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 8007a00:	18d3      	adds	r3, r2, r3
 8007a02:	7818      	ldrb	r0, [r3, #0]
 8007a04:	6879      	ldr	r1, [r7, #4]
 8007a06:	2385      	movs	r3, #133	; 0x85
 8007a08:	005b      	lsls	r3, r3, #1
 8007a0a:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 8007a0c:	188a      	adds	r2, r1, r2
 8007a0e:	18d3      	adds	r3, r2, r3
 8007a10:	1c02      	adds	r2, r0, #0
 8007a12:	701a      	strb	r2, [r3, #0]
 8007a14:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 8007a16:	3301      	adds	r3, #1
 8007a18:	647b      	str	r3, [r7, #68]	; 0x44
 8007a1a:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 8007a1c:	2b05      	cmp	r3, #5
 8007a1e:	ddec      	ble.n	80079fa <VL53L0X_get_info_from_device+0x952>
 8007a20:	1cfb      	adds	r3, r7, #3
 8007a22:	781b      	ldrb	r3, [r3, #0]
 8007a24:	2202      	movs	r2, #2
 8007a26:	4013      	ands	r3, r2
 8007a28:	d011      	beq.n	8007a4e <VL53L0X_get_info_from_device+0x9a6>
 8007a2a:	2337      	movs	r3, #55	; 0x37
 8007a2c:	18fb      	adds	r3, r7, r3
 8007a2e:	781b      	ldrb	r3, [r3, #0]
 8007a30:	2202      	movs	r2, #2
 8007a32:	4013      	ands	r3, r2
 8007a34:	d10b      	bne.n	8007a4e <VL53L0X_get_info_from_device+0x9a6>
 8007a36:	232f      	movs	r3, #47	; 0x2f
 8007a38:	18fb      	adds	r3, r7, r3
 8007a3a:	7819      	ldrb	r1, [r3, #0]
 8007a3c:	687b      	ldr	r3, [r7, #4]
 8007a3e:	22f1      	movs	r2, #241	; 0xf1
 8007a40:	5499      	strb	r1, [r3, r2]
 8007a42:	232e      	movs	r3, #46	; 0x2e
 8007a44:	18fb      	adds	r3, r7, r3
 8007a46:	7819      	ldrb	r1, [r3, #0]
 8007a48:	687b      	ldr	r3, [r7, #4]
 8007a4a:	22f2      	movs	r2, #242	; 0xf2
 8007a4c:	5499      	strb	r1, [r3, r2]
 8007a4e:	1cfb      	adds	r3, r7, #3
 8007a50:	781b      	ldrb	r3, [r3, #0]
 8007a52:	2204      	movs	r2, #4
 8007a54:	4013      	ands	r3, r2
 8007a56:	d036      	beq.n	8007ac6 <VL53L0X_get_info_from_device+0xa1e>
 8007a58:	2337      	movs	r3, #55	; 0x37
 8007a5a:	18fb      	adds	r3, r7, r3
 8007a5c:	781b      	ldrb	r3, [r3, #0]
 8007a5e:	2204      	movs	r2, #4
 8007a60:	4013      	ands	r3, r2
 8007a62:	d130      	bne.n	8007ac6 <VL53L0X_get_info_from_device+0xa1e>
 8007a64:	6aba      	ldr	r2, [r7, #40]	; 0x28
 8007a66:	687b      	ldr	r3, [r7, #4]
 8007a68:	21f8      	movs	r1, #248	; 0xf8
 8007a6a:	505a      	str	r2, [r3, r1]
 8007a6c:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8007a6e:	687b      	ldr	r3, [r7, #4]
 8007a70:	21fc      	movs	r1, #252	; 0xfc
 8007a72:	505a      	str	r2, [r3, r1]
 8007a74:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8007a76:	025b      	lsls	r3, r3, #9
 8007a78:	63bb      	str	r3, [r7, #56]	; 0x38
 8007a7a:	687a      	ldr	r2, [r7, #4]
 8007a7c:	2380      	movs	r3, #128	; 0x80
 8007a7e:	005b      	lsls	r3, r3, #1
 8007a80:	6bb9      	ldr	r1, [r7, #56]	; 0x38
 8007a82:	50d1      	str	r1, [r2, r3]
 8007a84:	2152      	movs	r1, #82	; 0x52
 8007a86:	187b      	adds	r3, r7, r1
 8007a88:	2200      	movs	r2, #0
 8007a8a:	801a      	strh	r2, [r3, #0]
 8007a8c:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 8007a8e:	2b00      	cmp	r3, #0
 8007a90:	d013      	beq.n	8007aba <VL53L0X_get_info_from_device+0xa12>
 8007a92:	6cfa      	ldr	r2, [r7, #76]	; 0x4c
 8007a94:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8007a96:	1ad3      	subs	r3, r2, r3
 8007a98:	643b      	str	r3, [r7, #64]	; 0x40
 8007a9a:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 8007a9c:	0013      	movs	r3, r2
 8007a9e:	015b      	lsls	r3, r3, #5
 8007aa0:	1a9b      	subs	r3, r3, r2
 8007aa2:	009b      	lsls	r3, r3, #2
 8007aa4:	189b      	adds	r3, r3, r2
 8007aa6:	00db      	lsls	r3, r3, #3
 8007aa8:	091a      	lsrs	r2, r3, #4
 8007aaa:	187b      	adds	r3, r7, r1
 8007aac:	801a      	strh	r2, [r3, #0]
 8007aae:	187b      	adds	r3, r7, r1
 8007ab0:	881b      	ldrh	r3, [r3, #0]
 8007ab2:	425b      	negs	r3, r3
 8007ab4:	b29a      	uxth	r2, r3
 8007ab6:	187b      	adds	r3, r7, r1
 8007ab8:	801a      	strh	r2, [r3, #0]
 8007aba:	2352      	movs	r3, #82	; 0x52
 8007abc:	18fb      	adds	r3, r7, r3
 8007abe:	2200      	movs	r2, #0
 8007ac0:	5e9a      	ldrsh	r2, [r3, r2]
 8007ac2:	687b      	ldr	r3, [r7, #4]
 8007ac4:	60da      	str	r2, [r3, #12]
 8007ac6:	2337      	movs	r3, #55	; 0x37
 8007ac8:	18fa      	adds	r2, r7, r3
 8007aca:	1cfb      	adds	r3, r7, #3
 8007acc:	7812      	ldrb	r2, [r2, #0]
 8007ace:	781b      	ldrb	r3, [r3, #0]
 8007ad0:	4313      	orrs	r3, r2
 8007ad2:	b2da      	uxtb	r2, r3
 8007ad4:	2136      	movs	r1, #54	; 0x36
 8007ad6:	187b      	adds	r3, r7, r1
 8007ad8:	701a      	strb	r2, [r3, #0]
 8007ada:	187b      	adds	r3, r7, r1
 8007adc:	7819      	ldrb	r1, [r3, #0]
 8007ade:	687b      	ldr	r3, [r7, #4]
 8007ae0:	22f0      	movs	r2, #240	; 0xf0
 8007ae2:	5499      	strb	r1, [r3, r2]
 8007ae4:	2357      	movs	r3, #87	; 0x57
 8007ae6:	18fb      	adds	r3, r7, r3
 8007ae8:	781b      	ldrb	r3, [r3, #0]
 8007aea:	b25b      	sxtb	r3, r3
 8007aec:	0018      	movs	r0, r3
 8007aee:	46bd      	mov	sp, r7
 8007af0:	b017      	add	sp, #92	; 0x5c
 8007af2:	bdf0      	pop	{r4, r5, r6, r7, pc}

08007af4 <VL53L0X_calc_macro_period_ps>:
 8007af4:	b590      	push	{r4, r7, lr}
 8007af6:	b087      	sub	sp, #28
 8007af8:	af00      	add	r7, sp, #0
 8007afa:	6078      	str	r0, [r7, #4]
 8007afc:	000a      	movs	r2, r1
 8007afe:	1cfb      	adds	r3, r7, #3
 8007b00:	701a      	strb	r2, [r3, #0]
 8007b02:	4b09      	ldr	r3, [pc, #36]	; (8007b28 <VL53L0X_calc_macro_period_ps+0x34>)
 8007b04:	2400      	movs	r4, #0
 8007b06:	613b      	str	r3, [r7, #16]
 8007b08:	617c      	str	r4, [r7, #20]
 8007b0a:	2390      	movs	r3, #144	; 0x90
 8007b0c:	011b      	lsls	r3, r3, #4
 8007b0e:	60fb      	str	r3, [r7, #12]
 8007b10:	1cfb      	adds	r3, r7, #3
 8007b12:	781b      	ldrb	r3, [r3, #0]
 8007b14:	68fa      	ldr	r2, [r7, #12]
 8007b16:	4353      	muls	r3, r2
 8007b18:	693a      	ldr	r2, [r7, #16]
 8007b1a:	4353      	muls	r3, r2
 8007b1c:	60bb      	str	r3, [r7, #8]
 8007b1e:	68bb      	ldr	r3, [r7, #8]
 8007b20:	0018      	movs	r0, r3
 8007b22:	46bd      	mov	sp, r7
 8007b24:	b007      	add	sp, #28
 8007b26:	bd90      	pop	{r4, r7, pc}
 8007b28:	00000677 	.word	0x00000677

08007b2c <VL53L0X_encode_timeout>:
 8007b2c:	b580      	push	{r7, lr}
 8007b2e:	b086      	sub	sp, #24
 8007b30:	af00      	add	r7, sp, #0
 8007b32:	6078      	str	r0, [r7, #4]
 8007b34:	2316      	movs	r3, #22
 8007b36:	18fb      	adds	r3, r7, r3
 8007b38:	2200      	movs	r2, #0
 8007b3a:	801a      	strh	r2, [r3, #0]
 8007b3c:	2300      	movs	r3, #0
 8007b3e:	613b      	str	r3, [r7, #16]
 8007b40:	230e      	movs	r3, #14
 8007b42:	18fb      	adds	r3, r7, r3
 8007b44:	2200      	movs	r2, #0
 8007b46:	801a      	strh	r2, [r3, #0]
 8007b48:	687b      	ldr	r3, [r7, #4]
 8007b4a:	2b00      	cmp	r3, #0
 8007b4c:	d01e      	beq.n	8007b8c <VL53L0X_encode_timeout+0x60>
 8007b4e:	687b      	ldr	r3, [r7, #4]
 8007b50:	3b01      	subs	r3, #1
 8007b52:	613b      	str	r3, [r7, #16]
 8007b54:	e008      	b.n	8007b68 <VL53L0X_encode_timeout+0x3c>
 8007b56:	693b      	ldr	r3, [r7, #16]
 8007b58:	085b      	lsrs	r3, r3, #1
 8007b5a:	613b      	str	r3, [r7, #16]
 8007b5c:	210e      	movs	r1, #14
 8007b5e:	187b      	adds	r3, r7, r1
 8007b60:	881a      	ldrh	r2, [r3, #0]
 8007b62:	187b      	adds	r3, r7, r1
 8007b64:	3201      	adds	r2, #1
 8007b66:	801a      	strh	r2, [r3, #0]
 8007b68:	693b      	ldr	r3, [r7, #16]
 8007b6a:	22ff      	movs	r2, #255	; 0xff
 8007b6c:	4393      	bics	r3, r2
 8007b6e:	d1f2      	bne.n	8007b56 <VL53L0X_encode_timeout+0x2a>
 8007b70:	230e      	movs	r3, #14
 8007b72:	18fb      	adds	r3, r7, r3
 8007b74:	881b      	ldrh	r3, [r3, #0]
 8007b76:	021b      	lsls	r3, r3, #8
 8007b78:	b299      	uxth	r1, r3
 8007b7a:	693b      	ldr	r3, [r7, #16]
 8007b7c:	b29b      	uxth	r3, r3
 8007b7e:	22ff      	movs	r2, #255	; 0xff
 8007b80:	4013      	ands	r3, r2
 8007b82:	b29a      	uxth	r2, r3
 8007b84:	2316      	movs	r3, #22
 8007b86:	18fb      	adds	r3, r7, r3
 8007b88:	188a      	adds	r2, r1, r2
 8007b8a:	801a      	strh	r2, [r3, #0]
 8007b8c:	2316      	movs	r3, #22
 8007b8e:	18fb      	adds	r3, r7, r3
 8007b90:	881b      	ldrh	r3, [r3, #0]
 8007b92:	0018      	movs	r0, r3
 8007b94:	46bd      	mov	sp, r7
 8007b96:	b006      	add	sp, #24
 8007b98:	bd80      	pop	{r7, pc}

08007b9a <VL53L0X_decode_timeout>:
 8007b9a:	b580      	push	{r7, lr}
 8007b9c:	b084      	sub	sp, #16
 8007b9e:	af00      	add	r7, sp, #0
 8007ba0:	0002      	movs	r2, r0
 8007ba2:	1dbb      	adds	r3, r7, #6
 8007ba4:	801a      	strh	r2, [r3, #0]
 8007ba6:	2300      	movs	r3, #0
 8007ba8:	60fb      	str	r3, [r7, #12]
 8007baa:	1dbb      	adds	r3, r7, #6
 8007bac:	881b      	ldrh	r3, [r3, #0]
 8007bae:	22ff      	movs	r2, #255	; 0xff
 8007bb0:	4013      	ands	r3, r2
 8007bb2:	1dba      	adds	r2, r7, #6
 8007bb4:	8812      	ldrh	r2, [r2, #0]
 8007bb6:	0a12      	lsrs	r2, r2, #8
 8007bb8:	b292      	uxth	r2, r2
 8007bba:	4093      	lsls	r3, r2
 8007bbc:	3301      	adds	r3, #1
 8007bbe:	60fb      	str	r3, [r7, #12]
 8007bc0:	68fb      	ldr	r3, [r7, #12]
 8007bc2:	0018      	movs	r0, r3
 8007bc4:	46bd      	mov	sp, r7
 8007bc6:	b004      	add	sp, #16
 8007bc8:	bd80      	pop	{r7, pc}

08007bca <VL53L0X_calc_timeout_mclks>:
 8007bca:	b580      	push	{r7, lr}
 8007bcc:	b088      	sub	sp, #32
 8007bce:	af00      	add	r7, sp, #0
 8007bd0:	60f8      	str	r0, [r7, #12]
 8007bd2:	60b9      	str	r1, [r7, #8]
 8007bd4:	1dfb      	adds	r3, r7, #7
 8007bd6:	701a      	strb	r2, [r3, #0]
 8007bd8:	2300      	movs	r3, #0
 8007bda:	61fb      	str	r3, [r7, #28]
 8007bdc:	1dfb      	adds	r3, r7, #7
 8007bde:	781a      	ldrb	r2, [r3, #0]
 8007be0:	68fb      	ldr	r3, [r7, #12]
 8007be2:	0011      	movs	r1, r2
 8007be4:	0018      	movs	r0, r3
 8007be6:	f7ff ff85 	bl	8007af4 <VL53L0X_calc_macro_period_ps>
 8007bea:	0003      	movs	r3, r0
 8007bec:	61bb      	str	r3, [r7, #24]
 8007bee:	69bb      	ldr	r3, [r7, #24]
 8007bf0:	33f5      	adds	r3, #245	; 0xf5
 8007bf2:	33ff      	adds	r3, #255	; 0xff
 8007bf4:	001a      	movs	r2, r3
 8007bf6:	23fa      	movs	r3, #250	; 0xfa
 8007bf8:	0099      	lsls	r1, r3, #2
 8007bfa:	0010      	movs	r0, r2
 8007bfc:	f7f8 fa84 	bl	8000108 <__udivsi3>
 8007c00:	0003      	movs	r3, r0
 8007c02:	617b      	str	r3, [r7, #20]
 8007c04:	68ba      	ldr	r2, [r7, #8]
 8007c06:	0013      	movs	r3, r2
 8007c08:	015b      	lsls	r3, r3, #5
 8007c0a:	1a9b      	subs	r3, r3, r2
 8007c0c:	009b      	lsls	r3, r3, #2
 8007c0e:	189b      	adds	r3, r3, r2
 8007c10:	00db      	lsls	r3, r3, #3
 8007c12:	001a      	movs	r2, r3
 8007c14:	697b      	ldr	r3, [r7, #20]
 8007c16:	085b      	lsrs	r3, r3, #1
 8007c18:	18d3      	adds	r3, r2, r3
 8007c1a:	6979      	ldr	r1, [r7, #20]
 8007c1c:	0018      	movs	r0, r3
 8007c1e:	f7f8 fa73 	bl	8000108 <__udivsi3>
 8007c22:	0003      	movs	r3, r0
 8007c24:	61fb      	str	r3, [r7, #28]
 8007c26:	69fb      	ldr	r3, [r7, #28]
 8007c28:	0018      	movs	r0, r3
 8007c2a:	46bd      	mov	sp, r7
 8007c2c:	b008      	add	sp, #32
 8007c2e:	bd80      	pop	{r7, pc}

08007c30 <VL53L0X_calc_timeout_us>:
 8007c30:	b580      	push	{r7, lr}
 8007c32:	b086      	sub	sp, #24
 8007c34:	af00      	add	r7, sp, #0
 8007c36:	6078      	str	r0, [r7, #4]
 8007c38:	0008      	movs	r0, r1
 8007c3a:	0011      	movs	r1, r2
 8007c3c:	1cbb      	adds	r3, r7, #2
 8007c3e:	1c02      	adds	r2, r0, #0
 8007c40:	801a      	strh	r2, [r3, #0]
 8007c42:	1c7b      	adds	r3, r7, #1
 8007c44:	1c0a      	adds	r2, r1, #0
 8007c46:	701a      	strb	r2, [r3, #0]
 8007c48:	2300      	movs	r3, #0
 8007c4a:	617b      	str	r3, [r7, #20]
 8007c4c:	1c7b      	adds	r3, r7, #1
 8007c4e:	781a      	ldrb	r2, [r3, #0]
 8007c50:	687b      	ldr	r3, [r7, #4]
 8007c52:	0011      	movs	r1, r2
 8007c54:	0018      	movs	r0, r3
 8007c56:	f7ff ff4d 	bl	8007af4 <VL53L0X_calc_macro_period_ps>
 8007c5a:	0003      	movs	r3, r0
 8007c5c:	613b      	str	r3, [r7, #16]
 8007c5e:	693b      	ldr	r3, [r7, #16]
 8007c60:	33f5      	adds	r3, #245	; 0xf5
 8007c62:	33ff      	adds	r3, #255	; 0xff
 8007c64:	001a      	movs	r2, r3
 8007c66:	23fa      	movs	r3, #250	; 0xfa
 8007c68:	0099      	lsls	r1, r3, #2
 8007c6a:	0010      	movs	r0, r2
 8007c6c:	f7f8 fa4c 	bl	8000108 <__udivsi3>
 8007c70:	0003      	movs	r3, r0
 8007c72:	60fb      	str	r3, [r7, #12]
 8007c74:	1cbb      	adds	r3, r7, #2
 8007c76:	881b      	ldrh	r3, [r3, #0]
 8007c78:	68fa      	ldr	r2, [r7, #12]
 8007c7a:	4353      	muls	r3, r2
 8007c7c:	33f5      	adds	r3, #245	; 0xf5
 8007c7e:	33ff      	adds	r3, #255	; 0xff
 8007c80:	001a      	movs	r2, r3
 8007c82:	23fa      	movs	r3, #250	; 0xfa
 8007c84:	0099      	lsls	r1, r3, #2
 8007c86:	0010      	movs	r0, r2
 8007c88:	f7f8 fa3e 	bl	8000108 <__udivsi3>
 8007c8c:	0003      	movs	r3, r0
 8007c8e:	617b      	str	r3, [r7, #20]
 8007c90:	697b      	ldr	r3, [r7, #20]
 8007c92:	0018      	movs	r0, r3
 8007c94:	46bd      	mov	sp, r7
 8007c96:	b006      	add	sp, #24
 8007c98:	bd80      	pop	{r7, pc}

08007c9a <get_sequence_step_timeout>:
 8007c9a:	b5f0      	push	{r4, r5, r6, r7, lr}
 8007c9c:	b08d      	sub	sp, #52	; 0x34
 8007c9e:	af00      	add	r7, sp, #0
 8007ca0:	60f8      	str	r0, [r7, #12]
 8007ca2:	607a      	str	r2, [r7, #4]
 8007ca4:	200b      	movs	r0, #11
 8007ca6:	183b      	adds	r3, r7, r0
 8007ca8:	1c0a      	adds	r2, r1, #0
 8007caa:	701a      	strb	r2, [r3, #0]
 8007cac:	232f      	movs	r3, #47	; 0x2f
 8007cae:	18fb      	adds	r3, r7, r3
 8007cb0:	2200      	movs	r2, #0
 8007cb2:	701a      	strb	r2, [r3, #0]
 8007cb4:	2320      	movs	r3, #32
 8007cb6:	18fb      	adds	r3, r7, r3
 8007cb8:	2200      	movs	r2, #0
 8007cba:	701a      	strb	r2, [r3, #0]
 8007cbc:	2300      	movs	r3, #0
 8007cbe:	62bb      	str	r3, [r7, #40]	; 0x28
 8007cc0:	231e      	movs	r3, #30
 8007cc2:	18fb      	adds	r3, r7, r3
 8007cc4:	2200      	movs	r2, #0
 8007cc6:	801a      	strh	r2, [r3, #0]
 8007cc8:	2324      	movs	r3, #36	; 0x24
 8007cca:	18fb      	adds	r3, r7, r3
 8007ccc:	2200      	movs	r2, #0
 8007cce:	801a      	strh	r2, [r3, #0]
 8007cd0:	183b      	adds	r3, r7, r0
 8007cd2:	781b      	ldrb	r3, [r3, #0]
 8007cd4:	2b00      	cmp	r3, #0
 8007cd6:	d007      	beq.n	8007ce8 <get_sequence_step_timeout+0x4e>
 8007cd8:	183b      	adds	r3, r7, r0
 8007cda:	781b      	ldrb	r3, [r3, #0]
 8007cdc:	2b01      	cmp	r3, #1
 8007cde:	d003      	beq.n	8007ce8 <get_sequence_step_timeout+0x4e>
 8007ce0:	183b      	adds	r3, r7, r0
 8007ce2:	781b      	ldrb	r3, [r3, #0]
 8007ce4:	2b02      	cmp	r3, #2
 8007ce6:	d130      	bne.n	8007d4a <get_sequence_step_timeout+0xb0>
 8007ce8:	252f      	movs	r5, #47	; 0x2f
 8007cea:	197c      	adds	r4, r7, r5
 8007cec:	2321      	movs	r3, #33	; 0x21
 8007cee:	18fa      	adds	r2, r7, r3
 8007cf0:	68fb      	ldr	r3, [r7, #12]
 8007cf2:	2100      	movs	r1, #0
 8007cf4:	0018      	movs	r0, r3
 8007cf6:	f7fc fdab 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007cfa:	0003      	movs	r3, r0
 8007cfc:	7023      	strb	r3, [r4, #0]
 8007cfe:	197b      	adds	r3, r7, r5
 8007d00:	781b      	ldrb	r3, [r3, #0]
 8007d02:	b25b      	sxtb	r3, r3
 8007d04:	2b00      	cmp	r3, #0
 8007d06:	d109      	bne.n	8007d1c <get_sequence_step_timeout+0x82>
 8007d08:	197c      	adds	r4, r7, r5
 8007d0a:	2320      	movs	r3, #32
 8007d0c:	18fa      	adds	r2, r7, r3
 8007d0e:	68fb      	ldr	r3, [r7, #12]
 8007d10:	2146      	movs	r1, #70	; 0x46
 8007d12:	0018      	movs	r0, r3
 8007d14:	f7fb fe5c 	bl	80039d0 <VL53L0X_RdByte>
 8007d18:	0003      	movs	r3, r0
 8007d1a:	7023      	strb	r3, [r4, #0]
 8007d1c:	2320      	movs	r3, #32
 8007d1e:	18fb      	adds	r3, r7, r3
 8007d20:	781b      	ldrb	r3, [r3, #0]
 8007d22:	b29b      	uxth	r3, r3
 8007d24:	0018      	movs	r0, r3
 8007d26:	f7ff ff38 	bl	8007b9a <VL53L0X_decode_timeout>
 8007d2a:	0002      	movs	r2, r0
 8007d2c:	2122      	movs	r1, #34	; 0x22
 8007d2e:	187b      	adds	r3, r7, r1
 8007d30:	801a      	strh	r2, [r3, #0]
 8007d32:	2321      	movs	r3, #33	; 0x21
 8007d34:	18fb      	adds	r3, r7, r3
 8007d36:	781a      	ldrb	r2, [r3, #0]
 8007d38:	187b      	adds	r3, r7, r1
 8007d3a:	8819      	ldrh	r1, [r3, #0]
 8007d3c:	68fb      	ldr	r3, [r7, #12]
 8007d3e:	0018      	movs	r0, r3
 8007d40:	f7ff ff76 	bl	8007c30 <VL53L0X_calc_timeout_us>
 8007d44:	0003      	movs	r3, r0
 8007d46:	62bb      	str	r3, [r7, #40]	; 0x28
 8007d48:	e0b6      	b.n	8007eb8 <get_sequence_step_timeout+0x21e>
 8007d4a:	230b      	movs	r3, #11
 8007d4c:	18fb      	adds	r3, r7, r3
 8007d4e:	781b      	ldrb	r3, [r3, #0]
 8007d50:	2b03      	cmp	r3, #3
 8007d52:	d13e      	bne.n	8007dd2 <get_sequence_step_timeout+0x138>
 8007d54:	252f      	movs	r5, #47	; 0x2f
 8007d56:	197c      	adds	r4, r7, r5
 8007d58:	2621      	movs	r6, #33	; 0x21
 8007d5a:	19ba      	adds	r2, r7, r6
 8007d5c:	68fb      	ldr	r3, [r7, #12]
 8007d5e:	2100      	movs	r1, #0
 8007d60:	0018      	movs	r0, r3
 8007d62:	f7fc fd75 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007d66:	0003      	movs	r3, r0
 8007d68:	7023      	strb	r3, [r4, #0]
 8007d6a:	197b      	adds	r3, r7, r5
 8007d6c:	781b      	ldrb	r3, [r3, #0]
 8007d6e:	b25b      	sxtb	r3, r3
 8007d70:	2b00      	cmp	r3, #0
 8007d72:	d000      	beq.n	8007d76 <get_sequence_step_timeout+0xdc>
 8007d74:	e0a0      	b.n	8007eb8 <get_sequence_step_timeout+0x21e>
 8007d76:	197c      	adds	r4, r7, r5
 8007d78:	19ba      	adds	r2, r7, r6
 8007d7a:	68fb      	ldr	r3, [r7, #12]
 8007d7c:	2100      	movs	r1, #0
 8007d7e:	0018      	movs	r0, r3
 8007d80:	f7fc fd66 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007d84:	0003      	movs	r3, r0
 8007d86:	7023      	strb	r3, [r4, #0]
 8007d88:	197b      	adds	r3, r7, r5
 8007d8a:	781b      	ldrb	r3, [r3, #0]
 8007d8c:	b25b      	sxtb	r3, r3
 8007d8e:	2b00      	cmp	r3, #0
 8007d90:	d109      	bne.n	8007da6 <get_sequence_step_timeout+0x10c>
 8007d92:	197c      	adds	r4, r7, r5
 8007d94:	231e      	movs	r3, #30
 8007d96:	18fa      	adds	r2, r7, r3
 8007d98:	68fb      	ldr	r3, [r7, #12]
 8007d9a:	2151      	movs	r1, #81	; 0x51
 8007d9c:	0018      	movs	r0, r3
 8007d9e:	f7fb fe2c 	bl	80039fa <VL53L0X_RdWord>
 8007da2:	0003      	movs	r3, r0
 8007da4:	7023      	strb	r3, [r4, #0]
 8007da6:	231e      	movs	r3, #30
 8007da8:	18fb      	adds	r3, r7, r3
 8007daa:	881b      	ldrh	r3, [r3, #0]
 8007dac:	0018      	movs	r0, r3
 8007dae:	f7ff fef4 	bl	8007b9a <VL53L0X_decode_timeout>
 8007db2:	0002      	movs	r2, r0
 8007db4:	2126      	movs	r1, #38	; 0x26
 8007db6:	187b      	adds	r3, r7, r1
 8007db8:	801a      	strh	r2, [r3, #0]
 8007dba:	2321      	movs	r3, #33	; 0x21
 8007dbc:	18fb      	adds	r3, r7, r3
 8007dbe:	781a      	ldrb	r2, [r3, #0]
 8007dc0:	187b      	adds	r3, r7, r1
 8007dc2:	8819      	ldrh	r1, [r3, #0]
 8007dc4:	68fb      	ldr	r3, [r7, #12]
 8007dc6:	0018      	movs	r0, r3
 8007dc8:	f7ff ff32 	bl	8007c30 <VL53L0X_calc_timeout_us>
 8007dcc:	0003      	movs	r3, r0
 8007dce:	62bb      	str	r3, [r7, #40]	; 0x28
 8007dd0:	e072      	b.n	8007eb8 <get_sequence_step_timeout+0x21e>
 8007dd2:	230b      	movs	r3, #11
 8007dd4:	18fb      	adds	r3, r7, r3
 8007dd6:	781b      	ldrb	r3, [r3, #0]
 8007dd8:	2b04      	cmp	r3, #4
 8007dda:	d16d      	bne.n	8007eb8 <get_sequence_step_timeout+0x21e>
 8007ddc:	2414      	movs	r4, #20
 8007dde:	193a      	adds	r2, r7, r4
 8007de0:	68fb      	ldr	r3, [r7, #12]
 8007de2:	0011      	movs	r1, r2
 8007de4:	0018      	movs	r0, r3
 8007de6:	f7fc fe93 	bl	8004b10 <VL53L0X_GetSequenceStepEnables>
 8007dea:	2626      	movs	r6, #38	; 0x26
 8007dec:	19bb      	adds	r3, r7, r6
 8007dee:	2200      	movs	r2, #0
 8007df0:	801a      	strh	r2, [r3, #0]
 8007df2:	193b      	adds	r3, r7, r4
 8007df4:	78db      	ldrb	r3, [r3, #3]
 8007df6:	2b00      	cmp	r3, #0
 8007df8:	d021      	beq.n	8007e3e <get_sequence_step_timeout+0x1a4>
 8007dfa:	252f      	movs	r5, #47	; 0x2f
 8007dfc:	197c      	adds	r4, r7, r5
 8007dfe:	2321      	movs	r3, #33	; 0x21
 8007e00:	18fa      	adds	r2, r7, r3
 8007e02:	68fb      	ldr	r3, [r7, #12]
 8007e04:	2100      	movs	r1, #0
 8007e06:	0018      	movs	r0, r3
 8007e08:	f7fc fd22 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007e0c:	0003      	movs	r3, r0
 8007e0e:	7023      	strb	r3, [r4, #0]
 8007e10:	197b      	adds	r3, r7, r5
 8007e12:	781b      	ldrb	r3, [r3, #0]
 8007e14:	b25b      	sxtb	r3, r3
 8007e16:	2b00      	cmp	r3, #0
 8007e18:	d111      	bne.n	8007e3e <get_sequence_step_timeout+0x1a4>
 8007e1a:	197c      	adds	r4, r7, r5
 8007e1c:	251e      	movs	r5, #30
 8007e1e:	197a      	adds	r2, r7, r5
 8007e20:	68fb      	ldr	r3, [r7, #12]
 8007e22:	2151      	movs	r1, #81	; 0x51
 8007e24:	0018      	movs	r0, r3
 8007e26:	f7fb fde8 	bl	80039fa <VL53L0X_RdWord>
 8007e2a:	0003      	movs	r3, r0
 8007e2c:	7023      	strb	r3, [r4, #0]
 8007e2e:	197b      	adds	r3, r7, r5
 8007e30:	881b      	ldrh	r3, [r3, #0]
 8007e32:	0018      	movs	r0, r3
 8007e34:	f7ff feb1 	bl	8007b9a <VL53L0X_decode_timeout>
 8007e38:	0002      	movs	r2, r0
 8007e3a:	19bb      	adds	r3, r7, r6
 8007e3c:	801a      	strh	r2, [r3, #0]
 8007e3e:	222f      	movs	r2, #47	; 0x2f
 8007e40:	18bb      	adds	r3, r7, r2
 8007e42:	781b      	ldrb	r3, [r3, #0]
 8007e44:	b25b      	sxtb	r3, r3
 8007e46:	2b00      	cmp	r3, #0
 8007e48:	d109      	bne.n	8007e5e <get_sequence_step_timeout+0x1c4>
 8007e4a:	18bc      	adds	r4, r7, r2
 8007e4c:	2321      	movs	r3, #33	; 0x21
 8007e4e:	18fa      	adds	r2, r7, r3
 8007e50:	68fb      	ldr	r3, [r7, #12]
 8007e52:	2101      	movs	r1, #1
 8007e54:	0018      	movs	r0, r3
 8007e56:	f7fc fcfb 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007e5a:	0003      	movs	r3, r0
 8007e5c:	7023      	strb	r3, [r4, #0]
 8007e5e:	222f      	movs	r2, #47	; 0x2f
 8007e60:	18bb      	adds	r3, r7, r2
 8007e62:	781b      	ldrb	r3, [r3, #0]
 8007e64:	b25b      	sxtb	r3, r3
 8007e66:	2b00      	cmp	r3, #0
 8007e68:	d112      	bne.n	8007e90 <get_sequence_step_timeout+0x1f6>
 8007e6a:	18bc      	adds	r4, r7, r2
 8007e6c:	251c      	movs	r5, #28
 8007e6e:	197a      	adds	r2, r7, r5
 8007e70:	68fb      	ldr	r3, [r7, #12]
 8007e72:	2171      	movs	r1, #113	; 0x71
 8007e74:	0018      	movs	r0, r3
 8007e76:	f7fb fdc0 	bl	80039fa <VL53L0X_RdWord>
 8007e7a:	0003      	movs	r3, r0
 8007e7c:	7023      	strb	r3, [r4, #0]
 8007e7e:	197b      	adds	r3, r7, r5
 8007e80:	881b      	ldrh	r3, [r3, #0]
 8007e82:	0018      	movs	r0, r3
 8007e84:	f7ff fe89 	bl	8007b9a <VL53L0X_decode_timeout>
 8007e88:	0002      	movs	r2, r0
 8007e8a:	2324      	movs	r3, #36	; 0x24
 8007e8c:	18fb      	adds	r3, r7, r3
 8007e8e:	801a      	strh	r2, [r3, #0]
 8007e90:	2024      	movs	r0, #36	; 0x24
 8007e92:	183b      	adds	r3, r7, r0
 8007e94:	1839      	adds	r1, r7, r0
 8007e96:	2226      	movs	r2, #38	; 0x26
 8007e98:	18ba      	adds	r2, r7, r2
 8007e9a:	8809      	ldrh	r1, [r1, #0]
 8007e9c:	8812      	ldrh	r2, [r2, #0]
 8007e9e:	1a8a      	subs	r2, r1, r2
 8007ea0:	801a      	strh	r2, [r3, #0]
 8007ea2:	2321      	movs	r3, #33	; 0x21
 8007ea4:	18fb      	adds	r3, r7, r3
 8007ea6:	781a      	ldrb	r2, [r3, #0]
 8007ea8:	183b      	adds	r3, r7, r0
 8007eaa:	8819      	ldrh	r1, [r3, #0]
 8007eac:	68fb      	ldr	r3, [r7, #12]
 8007eae:	0018      	movs	r0, r3
 8007eb0:	f7ff febe 	bl	8007c30 <VL53L0X_calc_timeout_us>
 8007eb4:	0003      	movs	r3, r0
 8007eb6:	62bb      	str	r3, [r7, #40]	; 0x28
 8007eb8:	687b      	ldr	r3, [r7, #4]
 8007eba:	6aba      	ldr	r2, [r7, #40]	; 0x28
 8007ebc:	601a      	str	r2, [r3, #0]
 8007ebe:	232f      	movs	r3, #47	; 0x2f
 8007ec0:	18fb      	adds	r3, r7, r3
 8007ec2:	781b      	ldrb	r3, [r3, #0]
 8007ec4:	b25b      	sxtb	r3, r3
 8007ec6:	0018      	movs	r0, r3
 8007ec8:	46bd      	mov	sp, r7
 8007eca:	b00d      	add	sp, #52	; 0x34
 8007ecc:	bdf0      	pop	{r4, r5, r6, r7, pc}

08007ece <set_sequence_step_timeout>:
 8007ece:	b5f0      	push	{r4, r5, r6, r7, lr}
 8007ed0:	b08b      	sub	sp, #44	; 0x2c
 8007ed2:	af00      	add	r7, sp, #0
 8007ed4:	60f8      	str	r0, [r7, #12]
 8007ed6:	607a      	str	r2, [r7, #4]
 8007ed8:	200b      	movs	r0, #11
 8007eda:	183b      	adds	r3, r7, r0
 8007edc:	1c0a      	adds	r2, r1, #0
 8007ede:	701a      	strb	r2, [r3, #0]
 8007ee0:	2327      	movs	r3, #39	; 0x27
 8007ee2:	18fb      	adds	r3, r7, r3
 8007ee4:	2200      	movs	r2, #0
 8007ee6:	701a      	strb	r2, [r3, #0]
 8007ee8:	183b      	adds	r3, r7, r0
 8007eea:	781b      	ldrb	r3, [r3, #0]
 8007eec:	2b00      	cmp	r3, #0
 8007eee:	d007      	beq.n	8007f00 <set_sequence_step_timeout+0x32>
 8007ef0:	183b      	adds	r3, r7, r0
 8007ef2:	781b      	ldrb	r3, [r3, #0]
 8007ef4:	2b01      	cmp	r3, #1
 8007ef6:	d003      	beq.n	8007f00 <set_sequence_step_timeout+0x32>
 8007ef8:	183b      	adds	r3, r7, r0
 8007efa:	781b      	ldrb	r3, [r3, #0]
 8007efc:	2b02      	cmp	r3, #2
 8007efe:	d147      	bne.n	8007f90 <set_sequence_step_timeout+0xc2>
 8007f00:	2527      	movs	r5, #39	; 0x27
 8007f02:	197c      	adds	r4, r7, r5
 8007f04:	261b      	movs	r6, #27
 8007f06:	19ba      	adds	r2, r7, r6
 8007f08:	68fb      	ldr	r3, [r7, #12]
 8007f0a:	2100      	movs	r1, #0
 8007f0c:	0018      	movs	r0, r3
 8007f0e:	f7fc fc9f 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007f12:	0003      	movs	r3, r0
 8007f14:	7023      	strb	r3, [r4, #0]
 8007f16:	197b      	adds	r3, r7, r5
 8007f18:	781b      	ldrb	r3, [r3, #0]
 8007f1a:	b25b      	sxtb	r3, r3
 8007f1c:	2b00      	cmp	r3, #0
 8007f1e:	d124      	bne.n	8007f6a <set_sequence_step_timeout+0x9c>
 8007f20:	19bb      	adds	r3, r7, r6
 8007f22:	781a      	ldrb	r2, [r3, #0]
 8007f24:	6879      	ldr	r1, [r7, #4]
 8007f26:	68fb      	ldr	r3, [r7, #12]
 8007f28:	0018      	movs	r0, r3
 8007f2a:	f7ff fe4e 	bl	8007bca <VL53L0X_calc_timeout_mclks>
 8007f2e:	0002      	movs	r2, r0
 8007f30:	211c      	movs	r1, #28
 8007f32:	187b      	adds	r3, r7, r1
 8007f34:	801a      	strh	r2, [r3, #0]
 8007f36:	187b      	adds	r3, r7, r1
 8007f38:	881a      	ldrh	r2, [r3, #0]
 8007f3a:	2380      	movs	r3, #128	; 0x80
 8007f3c:	005b      	lsls	r3, r3, #1
 8007f3e:	429a      	cmp	r2, r3
 8007f40:	d904      	bls.n	8007f4c <set_sequence_step_timeout+0x7e>
 8007f42:	2326      	movs	r3, #38	; 0x26
 8007f44:	18fb      	adds	r3, r7, r3
 8007f46:	22ff      	movs	r2, #255	; 0xff
 8007f48:	701a      	strb	r2, [r3, #0]
 8007f4a:	e007      	b.n	8007f5c <set_sequence_step_timeout+0x8e>
 8007f4c:	231c      	movs	r3, #28
 8007f4e:	18fb      	adds	r3, r7, r3
 8007f50:	881b      	ldrh	r3, [r3, #0]
 8007f52:	b2da      	uxtb	r2, r3
 8007f54:	2326      	movs	r3, #38	; 0x26
 8007f56:	18fb      	adds	r3, r7, r3
 8007f58:	3a01      	subs	r2, #1
 8007f5a:	701a      	strb	r2, [r3, #0]
 8007f5c:	2326      	movs	r3, #38	; 0x26
 8007f5e:	18fb      	adds	r3, r7, r3
 8007f60:	781b      	ldrb	r3, [r3, #0]
 8007f62:	b299      	uxth	r1, r3
 8007f64:	68fb      	ldr	r3, [r7, #12]
 8007f66:	22d8      	movs	r2, #216	; 0xd8
 8007f68:	5299      	strh	r1, [r3, r2]
 8007f6a:	2227      	movs	r2, #39	; 0x27
 8007f6c:	18bb      	adds	r3, r7, r2
 8007f6e:	781b      	ldrb	r3, [r3, #0]
 8007f70:	b25b      	sxtb	r3, r3
 8007f72:	2b00      	cmp	r3, #0
 8007f74:	d000      	beq.n	8007f78 <set_sequence_step_timeout+0xaa>
 8007f76:	e0da      	b.n	800812e <set_sequence_step_timeout+0x260>
 8007f78:	18bc      	adds	r4, r7, r2
 8007f7a:	2326      	movs	r3, #38	; 0x26
 8007f7c:	18fb      	adds	r3, r7, r3
 8007f7e:	781a      	ldrb	r2, [r3, #0]
 8007f80:	68fb      	ldr	r3, [r7, #12]
 8007f82:	2146      	movs	r1, #70	; 0x46
 8007f84:	0018      	movs	r0, r3
 8007f86:	f7fb fcb5 	bl	80038f4 <VL53L0X_WrByte>
 8007f8a:	0003      	movs	r3, r0
 8007f8c:	7023      	strb	r3, [r4, #0]
 8007f8e:	e0ce      	b.n	800812e <set_sequence_step_timeout+0x260>
 8007f90:	230b      	movs	r3, #11
 8007f92:	18fb      	adds	r3, r7, r3
 8007f94:	781b      	ldrb	r3, [r3, #0]
 8007f96:	2b03      	cmp	r3, #3
 8007f98:	d146      	bne.n	8008028 <set_sequence_step_timeout+0x15a>
 8007f9a:	2227      	movs	r2, #39	; 0x27
 8007f9c:	18bb      	adds	r3, r7, r2
 8007f9e:	781b      	ldrb	r3, [r3, #0]
 8007fa0:	b25b      	sxtb	r3, r3
 8007fa2:	2b00      	cmp	r3, #0
 8007fa4:	d123      	bne.n	8007fee <set_sequence_step_timeout+0x120>
 8007fa6:	18bc      	adds	r4, r7, r2
 8007fa8:	251b      	movs	r5, #27
 8007faa:	197a      	adds	r2, r7, r5
 8007fac:	68fb      	ldr	r3, [r7, #12]
 8007fae:	2100      	movs	r1, #0
 8007fb0:	0018      	movs	r0, r3
 8007fb2:	f7fc fc4d 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8007fb6:	0003      	movs	r3, r0
 8007fb8:	7023      	strb	r3, [r4, #0]
 8007fba:	197b      	adds	r3, r7, r5
 8007fbc:	781a      	ldrb	r2, [r3, #0]
 8007fbe:	6879      	ldr	r1, [r7, #4]
 8007fc0:	68fb      	ldr	r3, [r7, #12]
 8007fc2:	0018      	movs	r0, r3
 8007fc4:	f7ff fe01 	bl	8007bca <VL53L0X_calc_timeout_mclks>
 8007fc8:	0002      	movs	r2, r0
 8007fca:	2124      	movs	r1, #36	; 0x24
 8007fcc:	187b      	adds	r3, r7, r1
 8007fce:	801a      	strh	r2, [r3, #0]
 8007fd0:	187b      	adds	r3, r7, r1
 8007fd2:	881b      	ldrh	r3, [r3, #0]
 8007fd4:	0018      	movs	r0, r3
 8007fd6:	f7ff fda9 	bl	8007b2c <VL53L0X_encode_timeout>
 8007fda:	0003      	movs	r3, r0
 8007fdc:	001a      	movs	r2, r3
 8007fde:	2118      	movs	r1, #24
 8007fe0:	187b      	adds	r3, r7, r1
 8007fe2:	801a      	strh	r2, [r3, #0]
 8007fe4:	187b      	adds	r3, r7, r1
 8007fe6:	8819      	ldrh	r1, [r3, #0]
 8007fe8:	68fb      	ldr	r3, [r7, #12]
 8007fea:	22d8      	movs	r2, #216	; 0xd8
 8007fec:	5299      	strh	r1, [r3, r2]
 8007fee:	2127      	movs	r1, #39	; 0x27
 8007ff0:	187b      	adds	r3, r7, r1
 8007ff2:	781b      	ldrb	r3, [r3, #0]
 8007ff4:	b25b      	sxtb	r3, r3
 8007ff6:	2b00      	cmp	r3, #0
 8007ff8:	d10a      	bne.n	8008010 <set_sequence_step_timeout+0x142>
 8007ffa:	2318      	movs	r3, #24
 8007ffc:	18fb      	adds	r3, r7, r3
 8007ffe:	881a      	ldrh	r2, [r3, #0]
 8008000:	187c      	adds	r4, r7, r1
 8008002:	68fb      	ldr	r3, [r7, #12]
 8008004:	2151      	movs	r1, #81	; 0x51
 8008006:	0018      	movs	r0, r3
 8008008:	f7fb fc8c 	bl	8003924 <VL53L0X_WrWord>
 800800c:	0003      	movs	r3, r0
 800800e:	7023      	strb	r3, [r4, #0]
 8008010:	2327      	movs	r3, #39	; 0x27
 8008012:	18fb      	adds	r3, r7, r3
 8008014:	781b      	ldrb	r3, [r3, #0]
 8008016:	b25b      	sxtb	r3, r3
 8008018:	2b00      	cmp	r3, #0
 800801a:	d000      	beq.n	800801e <set_sequence_step_timeout+0x150>
 800801c:	e087      	b.n	800812e <set_sequence_step_timeout+0x260>
 800801e:	68fb      	ldr	r3, [r7, #12]
 8008020:	21e4      	movs	r1, #228	; 0xe4
 8008022:	687a      	ldr	r2, [r7, #4]
 8008024:	505a      	str	r2, [r3, r1]
 8008026:	e082      	b.n	800812e <set_sequence_step_timeout+0x260>
 8008028:	230b      	movs	r3, #11
 800802a:	18fb      	adds	r3, r7, r3
 800802c:	781b      	ldrb	r3, [r3, #0]
 800802e:	2b04      	cmp	r3, #4
 8008030:	d000      	beq.n	8008034 <set_sequence_step_timeout+0x166>
 8008032:	e078      	b.n	8008126 <set_sequence_step_timeout+0x258>
 8008034:	2410      	movs	r4, #16
 8008036:	193a      	adds	r2, r7, r4
 8008038:	68fb      	ldr	r3, [r7, #12]
 800803a:	0011      	movs	r1, r2
 800803c:	0018      	movs	r0, r3
 800803e:	f7fc fd67 	bl	8004b10 <VL53L0X_GetSequenceStepEnables>
 8008042:	2624      	movs	r6, #36	; 0x24
 8008044:	19bb      	adds	r3, r7, r6
 8008046:	2200      	movs	r2, #0
 8008048:	801a      	strh	r2, [r3, #0]
 800804a:	193b      	adds	r3, r7, r4
 800804c:	78db      	ldrb	r3, [r3, #3]
 800804e:	2b00      	cmp	r3, #0
 8008050:	d021      	beq.n	8008096 <set_sequence_step_timeout+0x1c8>
 8008052:	2527      	movs	r5, #39	; 0x27
 8008054:	197c      	adds	r4, r7, r5
 8008056:	231b      	movs	r3, #27
 8008058:	18fa      	adds	r2, r7, r3
 800805a:	68fb      	ldr	r3, [r7, #12]
 800805c:	2100      	movs	r1, #0
 800805e:	0018      	movs	r0, r3
 8008060:	f7fc fbf6 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 8008064:	0003      	movs	r3, r0
 8008066:	7023      	strb	r3, [r4, #0]
 8008068:	197b      	adds	r3, r7, r5
 800806a:	781b      	ldrb	r3, [r3, #0]
 800806c:	b25b      	sxtb	r3, r3
 800806e:	2b00      	cmp	r3, #0
 8008070:	d111      	bne.n	8008096 <set_sequence_step_timeout+0x1c8>
 8008072:	197c      	adds	r4, r7, r5
 8008074:	2518      	movs	r5, #24
 8008076:	197a      	adds	r2, r7, r5
 8008078:	68fb      	ldr	r3, [r7, #12]
 800807a:	2151      	movs	r1, #81	; 0x51
 800807c:	0018      	movs	r0, r3
 800807e:	f7fb fcbc 	bl	80039fa <VL53L0X_RdWord>
 8008082:	0003      	movs	r3, r0
 8008084:	7023      	strb	r3, [r4, #0]
 8008086:	197b      	adds	r3, r7, r5
 8008088:	881b      	ldrh	r3, [r3, #0]
 800808a:	0018      	movs	r0, r3
 800808c:	f7ff fd85 	bl	8007b9a <VL53L0X_decode_timeout>
 8008090:	0002      	movs	r2, r0
 8008092:	19bb      	adds	r3, r7, r6
 8008094:	801a      	strh	r2, [r3, #0]
 8008096:	2227      	movs	r2, #39	; 0x27
 8008098:	18bb      	adds	r3, r7, r2
 800809a:	781b      	ldrb	r3, [r3, #0]
 800809c:	b25b      	sxtb	r3, r3
 800809e:	2b00      	cmp	r3, #0
 80080a0:	d109      	bne.n	80080b6 <set_sequence_step_timeout+0x1e8>
 80080a2:	18bc      	adds	r4, r7, r2
 80080a4:	231b      	movs	r3, #27
 80080a6:	18fa      	adds	r2, r7, r3
 80080a8:	68fb      	ldr	r3, [r7, #12]
 80080aa:	2101      	movs	r1, #1
 80080ac:	0018      	movs	r0, r3
 80080ae:	f7fc fbcf 	bl	8004850 <VL53L0X_GetVcselPulsePeriod>
 80080b2:	0003      	movs	r3, r0
 80080b4:	7023      	strb	r3, [r4, #0]
 80080b6:	2527      	movs	r5, #39	; 0x27
 80080b8:	197b      	adds	r3, r7, r5
 80080ba:	781b      	ldrb	r3, [r3, #0]
 80080bc:	b25b      	sxtb	r3, r3
 80080be:	2b00      	cmp	r3, #0
 80080c0:	d135      	bne.n	800812e <set_sequence_step_timeout+0x260>
 80080c2:	231b      	movs	r3, #27
 80080c4:	18fb      	adds	r3, r7, r3
 80080c6:	781a      	ldrb	r2, [r3, #0]
 80080c8:	6879      	ldr	r1, [r7, #4]
 80080ca:	68fb      	ldr	r3, [r7, #12]
 80080cc:	0018      	movs	r0, r3
 80080ce:	f7ff fd7c 	bl	8007bca <VL53L0X_calc_timeout_mclks>
 80080d2:	0003      	movs	r3, r0
 80080d4:	623b      	str	r3, [r7, #32]
 80080d6:	2324      	movs	r3, #36	; 0x24
 80080d8:	18fb      	adds	r3, r7, r3
 80080da:	881b      	ldrh	r3, [r3, #0]
 80080dc:	6a3a      	ldr	r2, [r7, #32]
 80080de:	18d3      	adds	r3, r2, r3
 80080e0:	623b      	str	r3, [r7, #32]
 80080e2:	261e      	movs	r6, #30
 80080e4:	19bc      	adds	r4, r7, r6
 80080e6:	6a3b      	ldr	r3, [r7, #32]
 80080e8:	0018      	movs	r0, r3
 80080ea:	f7ff fd1f 	bl	8007b2c <VL53L0X_encode_timeout>
 80080ee:	0003      	movs	r3, r0
 80080f0:	8023      	strh	r3, [r4, #0]
 80080f2:	197b      	adds	r3, r7, r5
 80080f4:	781b      	ldrb	r3, [r3, #0]
 80080f6:	b25b      	sxtb	r3, r3
 80080f8:	2b00      	cmp	r3, #0
 80080fa:	d109      	bne.n	8008110 <set_sequence_step_timeout+0x242>
 80080fc:	197c      	adds	r4, r7, r5
 80080fe:	19bb      	adds	r3, r7, r6
 8008100:	881a      	ldrh	r2, [r3, #0]
 8008102:	68fb      	ldr	r3, [r7, #12]
 8008104:	2171      	movs	r1, #113	; 0x71
 8008106:	0018      	movs	r0, r3
 8008108:	f7fb fc0c 	bl	8003924 <VL53L0X_WrWord>
 800810c:	0003      	movs	r3, r0
 800810e:	7023      	strb	r3, [r4, #0]
 8008110:	2327      	movs	r3, #39	; 0x27
 8008112:	18fb      	adds	r3, r7, r3
 8008114:	781b      	ldrb	r3, [r3, #0]
 8008116:	b25b      	sxtb	r3, r3
 8008118:	2b00      	cmp	r3, #0
 800811a:	d108      	bne.n	800812e <set_sequence_step_timeout+0x260>
 800811c:	68fb      	ldr	r3, [r7, #12]
 800811e:	21dc      	movs	r1, #220	; 0xdc
 8008120:	687a      	ldr	r2, [r7, #4]
 8008122:	505a      	str	r2, [r3, r1]
 8008124:	e003      	b.n	800812e <set_sequence_step_timeout+0x260>
 8008126:	2327      	movs	r3, #39	; 0x27
 8008128:	18fb      	adds	r3, r7, r3
 800812a:	22fc      	movs	r2, #252	; 0xfc
 800812c:	701a      	strb	r2, [r3, #0]
 800812e:	2327      	movs	r3, #39	; 0x27
 8008130:	18fb      	adds	r3, r7, r3
 8008132:	781b      	ldrb	r3, [r3, #0]
 8008134:	b25b      	sxtb	r3, r3
 8008136:	0018      	movs	r0, r3
 8008138:	46bd      	mov	sp, r7
 800813a:	b00b      	add	sp, #44	; 0x2c
 800813c:	bdf0      	pop	{r4, r5, r6, r7, pc}

0800813e <VL53L0X_get_vcsel_pulse_period>:
 800813e:	b590      	push	{r4, r7, lr}
 8008140:	b087      	sub	sp, #28
 8008142:	af00      	add	r7, sp, #0
 8008144:	60f8      	str	r0, [r7, #12]
 8008146:	607a      	str	r2, [r7, #4]
 8008148:	200b      	movs	r0, #11
 800814a:	183b      	adds	r3, r7, r0
 800814c:	1c0a      	adds	r2, r1, #0
 800814e:	701a      	strb	r2, [r3, #0]
 8008150:	2317      	movs	r3, #23
 8008152:	18fb      	adds	r3, r7, r3
 8008154:	2200      	movs	r2, #0
 8008156:	701a      	strb	r2, [r3, #0]
 8008158:	183b      	adds	r3, r7, r0
 800815a:	781b      	ldrb	r3, [r3, #0]
 800815c:	2b00      	cmp	r3, #0
 800815e:	d002      	beq.n	8008166 <VL53L0X_get_vcsel_pulse_period+0x28>
 8008160:	2b01      	cmp	r3, #1
 8008162:	d00c      	beq.n	800817e <VL53L0X_get_vcsel_pulse_period+0x40>
 8008164:	e017      	b.n	8008196 <VL53L0X_get_vcsel_pulse_period+0x58>
 8008166:	2317      	movs	r3, #23
 8008168:	18fc      	adds	r4, r7, r3
 800816a:	2316      	movs	r3, #22
 800816c:	18fa      	adds	r2, r7, r3
 800816e:	68fb      	ldr	r3, [r7, #12]
 8008170:	2150      	movs	r1, #80	; 0x50
 8008172:	0018      	movs	r0, r3
 8008174:	f7fb fc2c 	bl	80039d0 <VL53L0X_RdByte>
 8008178:	0003      	movs	r3, r0
 800817a:	7023      	strb	r3, [r4, #0]
 800817c:	e00f      	b.n	800819e <VL53L0X_get_vcsel_pulse_period+0x60>
 800817e:	2317      	movs	r3, #23
 8008180:	18fc      	adds	r4, r7, r3
 8008182:	2316      	movs	r3, #22
 8008184:	18fa      	adds	r2, r7, r3
 8008186:	68fb      	ldr	r3, [r7, #12]
 8008188:	2170      	movs	r1, #112	; 0x70
 800818a:	0018      	movs	r0, r3
 800818c:	f7fb fc20 	bl	80039d0 <VL53L0X_RdByte>
 8008190:	0003      	movs	r3, r0
 8008192:	7023      	strb	r3, [r4, #0]
 8008194:	e003      	b.n	800819e <VL53L0X_get_vcsel_pulse_period+0x60>
 8008196:	2317      	movs	r3, #23
 8008198:	18fb      	adds	r3, r7, r3
 800819a:	22fc      	movs	r2, #252	; 0xfc
 800819c:	701a      	strb	r2, [r3, #0]
 800819e:	2317      	movs	r3, #23
 80081a0:	18fb      	adds	r3, r7, r3
 80081a2:	781b      	ldrb	r3, [r3, #0]
 80081a4:	b25b      	sxtb	r3, r3
 80081a6:	2b00      	cmp	r3, #0
 80081a8:	d109      	bne.n	80081be <VL53L0X_get_vcsel_pulse_period+0x80>
 80081aa:	2316      	movs	r3, #22
 80081ac:	18fb      	adds	r3, r7, r3
 80081ae:	781b      	ldrb	r3, [r3, #0]
 80081b0:	0018      	movs	r0, r3
 80081b2:	f7fe fed9 	bl	8006f68 <VL53L0X_decode_vcsel_period>
 80081b6:	0003      	movs	r3, r0
 80081b8:	001a      	movs	r2, r3
 80081ba:	687b      	ldr	r3, [r7, #4]
 80081bc:	701a      	strb	r2, [r3, #0]
 80081be:	2317      	movs	r3, #23
 80081c0:	18fb      	adds	r3, r7, r3
 80081c2:	781b      	ldrb	r3, [r3, #0]
 80081c4:	b25b      	sxtb	r3, r3
 80081c6:	0018      	movs	r0, r3
 80081c8:	46bd      	mov	sp, r7
 80081ca:	b007      	add	sp, #28
 80081cc:	bd90      	pop	{r4, r7, pc}
	...

080081d0 <VL53L0X_set_measurement_timing_budget_micro_seconds>:
 80081d0:	b5f0      	push	{r4, r5, r6, r7, lr}
 80081d2:	b093      	sub	sp, #76	; 0x4c
 80081d4:	af00      	add	r7, sp, #0
 80081d6:	6078      	str	r0, [r7, #4]
 80081d8:	6039      	str	r1, [r7, #0]
 80081da:	2147      	movs	r1, #71	; 0x47
 80081dc:	187b      	adds	r3, r7, r1
 80081de:	2200      	movs	r2, #0
 80081e0:	701a      	strb	r2, [r3, #0]
 80081e2:	23fa      	movs	r3, #250	; 0xfa
 80081e4:	00db      	lsls	r3, r3, #3
 80081e6:	613b      	str	r3, [r7, #16]
 80081e8:	4b7a      	ldr	r3, [pc, #488]	; (80083d4 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x204>)
 80081ea:	63fb      	str	r3, [r7, #60]	; 0x3c
 80081ec:	23f0      	movs	r3, #240	; 0xf0
 80081ee:	009b      	lsls	r3, r3, #2
 80081f0:	63bb      	str	r3, [r7, #56]	; 0x38
 80081f2:	23a5      	movs	r3, #165	; 0xa5
 80081f4:	009b      	lsls	r3, r3, #2
 80081f6:	637b      	str	r3, [r7, #52]	; 0x34
 80081f8:	4b77      	ldr	r3, [pc, #476]	; (80083d8 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x208>)
 80081fa:	633b      	str	r3, [r7, #48]	; 0x30
 80081fc:	4b77      	ldr	r3, [pc, #476]	; (80083dc <VL53L0X_set_measurement_timing_budget_micro_seconds+0x20c>)
 80081fe:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008200:	23a5      	movs	r3, #165	; 0xa5
 8008202:	009b      	lsls	r3, r3, #2
 8008204:	62bb      	str	r3, [r7, #40]	; 0x28
 8008206:	4b76      	ldr	r3, [pc, #472]	; (80083e0 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x210>)
 8008208:	627b      	str	r3, [r7, #36]	; 0x24
 800820a:	2300      	movs	r3, #0
 800820c:	60fb      	str	r3, [r7, #12]
 800820e:	4b75      	ldr	r3, [pc, #468]	; (80083e4 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x214>)
 8008210:	623b      	str	r3, [r7, #32]
 8008212:	2300      	movs	r3, #0
 8008214:	61fb      	str	r3, [r7, #28]
 8008216:	683a      	ldr	r2, [r7, #0]
 8008218:	6a3b      	ldr	r3, [r7, #32]
 800821a:	429a      	cmp	r2, r3
 800821c:	d206      	bcs.n	800822c <VL53L0X_set_measurement_timing_budget_micro_seconds+0x5c>
 800821e:	187b      	adds	r3, r7, r1
 8008220:	22fc      	movs	r2, #252	; 0xfc
 8008222:	701a      	strb	r2, [r3, #0]
 8008224:	187b      	adds	r3, r7, r1
 8008226:	781b      	ldrb	r3, [r3, #0]
 8008228:	b25b      	sxtb	r3, r3
 800822a:	e0ce      	b.n	80083ca <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1fa>
 800822c:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 800822e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8008230:	18d3      	adds	r3, r2, r3
 8008232:	683a      	ldr	r2, [r7, #0]
 8008234:	1ad3      	subs	r3, r2, r3
 8008236:	643b      	str	r3, [r7, #64]	; 0x40
 8008238:	2647      	movs	r6, #71	; 0x47
 800823a:	19bc      	adds	r4, r7, r6
 800823c:	2514      	movs	r5, #20
 800823e:	197a      	adds	r2, r7, r5
 8008240:	687b      	ldr	r3, [r7, #4]
 8008242:	0011      	movs	r1, r2
 8008244:	0018      	movs	r0, r3
 8008246:	f7fc fc63 	bl	8004b10 <VL53L0X_GetSequenceStepEnables>
 800824a:	0003      	movs	r3, r0
 800824c:	7023      	strb	r3, [r4, #0]
 800824e:	19bb      	adds	r3, r7, r6
 8008250:	781b      	ldrb	r3, [r3, #0]
 8008252:	b25b      	sxtb	r3, r3
 8008254:	2b00      	cmp	r3, #0
 8008256:	d000      	beq.n	800825a <VL53L0X_set_measurement_timing_budget_micro_seconds+0x8a>
 8008258:	e06d      	b.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 800825a:	197b      	adds	r3, r7, r5
 800825c:	781b      	ldrb	r3, [r3, #0]
 800825e:	2b00      	cmp	r3, #0
 8008260:	d107      	bne.n	8008272 <VL53L0X_set_measurement_timing_budget_micro_seconds+0xa2>
 8008262:	197b      	adds	r3, r7, r5
 8008264:	785b      	ldrb	r3, [r3, #1]
 8008266:	2b00      	cmp	r3, #0
 8008268:	d103      	bne.n	8008272 <VL53L0X_set_measurement_timing_budget_micro_seconds+0xa2>
 800826a:	197b      	adds	r3, r7, r5
 800826c:	789b      	ldrb	r3, [r3, #2]
 800826e:	2b00      	cmp	r3, #0
 8008270:	d061      	beq.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 8008272:	2547      	movs	r5, #71	; 0x47
 8008274:	197c      	adds	r4, r7, r5
 8008276:	2310      	movs	r3, #16
 8008278:	18fa      	adds	r2, r7, r3
 800827a:	687b      	ldr	r3, [r7, #4]
 800827c:	2102      	movs	r1, #2
 800827e:	0018      	movs	r0, r3
 8008280:	f7ff fd0b 	bl	8007c9a <get_sequence_step_timeout>
 8008284:	0003      	movs	r3, r0
 8008286:	7023      	strb	r3, [r4, #0]
 8008288:	197b      	adds	r3, r7, r5
 800828a:	781b      	ldrb	r3, [r3, #0]
 800828c:	b25b      	sxtb	r3, r3
 800828e:	2b00      	cmp	r3, #0
 8008290:	d003      	beq.n	800829a <VL53L0X_set_measurement_timing_budget_micro_seconds+0xca>
 8008292:	197b      	adds	r3, r7, r5
 8008294:	781b      	ldrb	r3, [r3, #0]
 8008296:	b25b      	sxtb	r3, r3
 8008298:	e097      	b.n	80083ca <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1fa>
 800829a:	2314      	movs	r3, #20
 800829c:	18fb      	adds	r3, r7, r3
 800829e:	781b      	ldrb	r3, [r3, #0]
 80082a0:	2b00      	cmp	r3, #0
 80082a2:	d010      	beq.n	80082c6 <VL53L0X_set_measurement_timing_budget_micro_seconds+0xf6>
 80082a4:	693b      	ldr	r3, [r7, #16]
 80082a6:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 80082a8:	18d3      	adds	r3, r2, r3
 80082aa:	61fb      	str	r3, [r7, #28]
 80082ac:	69fa      	ldr	r2, [r7, #28]
 80082ae:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 80082b0:	429a      	cmp	r2, r3
 80082b2:	d204      	bcs.n	80082be <VL53L0X_set_measurement_timing_budget_micro_seconds+0xee>
 80082b4:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 80082b6:	69fb      	ldr	r3, [r7, #28]
 80082b8:	1ad3      	subs	r3, r2, r3
 80082ba:	643b      	str	r3, [r7, #64]	; 0x40
 80082bc:	e003      	b.n	80082c6 <VL53L0X_set_measurement_timing_budget_micro_seconds+0xf6>
 80082be:	2347      	movs	r3, #71	; 0x47
 80082c0:	18fb      	adds	r3, r7, r3
 80082c2:	22fc      	movs	r2, #252	; 0xfc
 80082c4:	701a      	strb	r2, [r3, #0]
 80082c6:	2247      	movs	r2, #71	; 0x47
 80082c8:	18bb      	adds	r3, r7, r2
 80082ca:	781b      	ldrb	r3, [r3, #0]
 80082cc:	b25b      	sxtb	r3, r3
 80082ce:	2b00      	cmp	r3, #0
 80082d0:	d003      	beq.n	80082da <VL53L0X_set_measurement_timing_budget_micro_seconds+0x10a>
 80082d2:	18bb      	adds	r3, r7, r2
 80082d4:	781b      	ldrb	r3, [r3, #0]
 80082d6:	b25b      	sxtb	r3, r3
 80082d8:	e077      	b.n	80083ca <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1fa>
 80082da:	2314      	movs	r3, #20
 80082dc:	18fb      	adds	r3, r7, r3
 80082de:	789b      	ldrb	r3, [r3, #2]
 80082e0:	2b00      	cmp	r3, #0
 80082e2:	d012      	beq.n	800830a <VL53L0X_set_measurement_timing_budget_micro_seconds+0x13a>
 80082e4:	693a      	ldr	r2, [r7, #16]
 80082e6:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80082e8:	18d3      	adds	r3, r2, r3
 80082ea:	005b      	lsls	r3, r3, #1
 80082ec:	61fb      	str	r3, [r7, #28]
 80082ee:	69fa      	ldr	r2, [r7, #28]
 80082f0:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 80082f2:	429a      	cmp	r2, r3
 80082f4:	d204      	bcs.n	8008300 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x130>
 80082f6:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 80082f8:	69fb      	ldr	r3, [r7, #28]
 80082fa:	1ad3      	subs	r3, r2, r3
 80082fc:	643b      	str	r3, [r7, #64]	; 0x40
 80082fe:	e01a      	b.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 8008300:	2347      	movs	r3, #71	; 0x47
 8008302:	18fb      	adds	r3, r7, r3
 8008304:	22fc      	movs	r2, #252	; 0xfc
 8008306:	701a      	strb	r2, [r3, #0]
 8008308:	e015      	b.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 800830a:	2314      	movs	r3, #20
 800830c:	18fb      	adds	r3, r7, r3
 800830e:	785b      	ldrb	r3, [r3, #1]
 8008310:	2b00      	cmp	r3, #0
 8008312:	d010      	beq.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 8008314:	693b      	ldr	r3, [r7, #16]
 8008316:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8008318:	18d3      	adds	r3, r2, r3
 800831a:	61fb      	str	r3, [r7, #28]
 800831c:	69fa      	ldr	r2, [r7, #28]
 800831e:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8008320:	429a      	cmp	r2, r3
 8008322:	d204      	bcs.n	800832e <VL53L0X_set_measurement_timing_budget_micro_seconds+0x15e>
 8008324:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 8008326:	69fb      	ldr	r3, [r7, #28]
 8008328:	1ad3      	subs	r3, r2, r3
 800832a:	643b      	str	r3, [r7, #64]	; 0x40
 800832c:	e003      	b.n	8008336 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x166>
 800832e:	2347      	movs	r3, #71	; 0x47
 8008330:	18fb      	adds	r3, r7, r3
 8008332:	22fc      	movs	r2, #252	; 0xfc
 8008334:	701a      	strb	r2, [r3, #0]
 8008336:	2247      	movs	r2, #71	; 0x47
 8008338:	18bb      	adds	r3, r7, r2
 800833a:	781b      	ldrb	r3, [r3, #0]
 800833c:	b25b      	sxtb	r3, r3
 800833e:	2b00      	cmp	r3, #0
 8008340:	d003      	beq.n	800834a <VL53L0X_set_measurement_timing_budget_micro_seconds+0x17a>
 8008342:	18bb      	adds	r3, r7, r2
 8008344:	781b      	ldrb	r3, [r3, #0]
 8008346:	b25b      	sxtb	r3, r3
 8008348:	e03f      	b.n	80083ca <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1fa>
 800834a:	2314      	movs	r3, #20
 800834c:	18fb      	adds	r3, r7, r3
 800834e:	78db      	ldrb	r3, [r3, #3]
 8008350:	2b00      	cmp	r3, #0
 8008352:	d01b      	beq.n	800838c <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1bc>
 8008354:	2347      	movs	r3, #71	; 0x47
 8008356:	18fc      	adds	r4, r7, r3
 8008358:	230c      	movs	r3, #12
 800835a:	18fa      	adds	r2, r7, r3
 800835c:	687b      	ldr	r3, [r7, #4]
 800835e:	2103      	movs	r1, #3
 8008360:	0018      	movs	r0, r3
 8008362:	f7ff fc9a 	bl	8007c9a <get_sequence_step_timeout>
 8008366:	0003      	movs	r3, r0
 8008368:	7023      	strb	r3, [r4, #0]
 800836a:	68fb      	ldr	r3, [r7, #12]
 800836c:	6aba      	ldr	r2, [r7, #40]	; 0x28
 800836e:	18d3      	adds	r3, r2, r3
 8008370:	61fb      	str	r3, [r7, #28]
 8008372:	69fa      	ldr	r2, [r7, #28]
 8008374:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 8008376:	429a      	cmp	r2, r3
 8008378:	d204      	bcs.n	8008384 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1b4>
 800837a:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 800837c:	69fb      	ldr	r3, [r7, #28]
 800837e:	1ad3      	subs	r3, r2, r3
 8008380:	643b      	str	r3, [r7, #64]	; 0x40
 8008382:	e003      	b.n	800838c <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1bc>
 8008384:	2347      	movs	r3, #71	; 0x47
 8008386:	18fb      	adds	r3, r7, r3
 8008388:	22fc      	movs	r2, #252	; 0xfc
 800838a:	701a      	strb	r2, [r3, #0]
 800838c:	2147      	movs	r1, #71	; 0x47
 800838e:	187b      	adds	r3, r7, r1
 8008390:	781b      	ldrb	r3, [r3, #0]
 8008392:	b25b      	sxtb	r3, r3
 8008394:	2b00      	cmp	r3, #0
 8008396:	d114      	bne.n	80083c2 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1f2>
 8008398:	2314      	movs	r3, #20
 800839a:	18fb      	adds	r3, r7, r3
 800839c:	791b      	ldrb	r3, [r3, #4]
 800839e:	2b00      	cmp	r3, #0
 80083a0:	d00f      	beq.n	80083c2 <VL53L0X_set_measurement_timing_budget_micro_seconds+0x1f2>
 80083a2:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 80083a4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80083a6:	1ad3      	subs	r3, r2, r3
 80083a8:	643b      	str	r3, [r7, #64]	; 0x40
 80083aa:	187c      	adds	r4, r7, r1
 80083ac:	6c3a      	ldr	r2, [r7, #64]	; 0x40
 80083ae:	687b      	ldr	r3, [r7, #4]
 80083b0:	2104      	movs	r1, #4
 80083b2:	0018      	movs	r0, r3
 80083b4:	f7ff fd8b 	bl	8007ece <set_sequence_step_timeout>
 80083b8:	0003      	movs	r3, r0
 80083ba:	7023      	strb	r3, [r4, #0]
 80083bc:	687b      	ldr	r3, [r7, #4]
 80083be:	683a      	ldr	r2, [r7, #0]
 80083c0:	615a      	str	r2, [r3, #20]
 80083c2:	2347      	movs	r3, #71	; 0x47
 80083c4:	18fb      	adds	r3, r7, r3
 80083c6:	781b      	ldrb	r3, [r3, #0]
 80083c8:	b25b      	sxtb	r3, r3
 80083ca:	0018      	movs	r0, r3
 80083cc:	46bd      	mov	sp, r7
 80083ce:	b013      	add	sp, #76	; 0x4c
 80083d0:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80083d2:	46c0      	nop			; (mov r8, r8)
 80083d4:	00000776 	.word	0x00000776
 80083d8:	0000024e 	.word	0x0000024e
 80083dc:	000002b2 	.word	0x000002b2
 80083e0:	00000226 	.word	0x00000226
 80083e4:	00004e20 	.word	0x00004e20

080083e8 <VL53L0X_get_measurement_timing_budget_micro_seconds>:
 80083e8:	b5b0      	push	{r4, r5, r7, lr}
 80083ea:	b090      	sub	sp, #64	; 0x40
 80083ec:	af00      	add	r7, sp, #0
 80083ee:	6078      	str	r0, [r7, #4]
 80083f0:	6039      	str	r1, [r7, #0]
 80083f2:	213f      	movs	r1, #63	; 0x3f
 80083f4:	187b      	adds	r3, r7, r1
 80083f6:	2200      	movs	r2, #0
 80083f8:	701a      	strb	r2, [r3, #0]
 80083fa:	23fa      	movs	r3, #250	; 0xfa
 80083fc:	00db      	lsls	r3, r3, #3
 80083fe:	613b      	str	r3, [r7, #16]
 8008400:	4b5e      	ldr	r3, [pc, #376]	; (800857c <VL53L0X_get_measurement_timing_budget_micro_seconds+0x194>)
 8008402:	63bb      	str	r3, [r7, #56]	; 0x38
 8008404:	23f0      	movs	r3, #240	; 0xf0
 8008406:	009b      	lsls	r3, r3, #2
 8008408:	637b      	str	r3, [r7, #52]	; 0x34
 800840a:	23a5      	movs	r3, #165	; 0xa5
 800840c:	009b      	lsls	r3, r3, #2
 800840e:	633b      	str	r3, [r7, #48]	; 0x30
 8008410:	4b5b      	ldr	r3, [pc, #364]	; (8008580 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x198>)
 8008412:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008414:	4b5b      	ldr	r3, [pc, #364]	; (8008584 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x19c>)
 8008416:	62bb      	str	r3, [r7, #40]	; 0x28
 8008418:	23a5      	movs	r3, #165	; 0xa5
 800841a:	009b      	lsls	r3, r3, #2
 800841c:	627b      	str	r3, [r7, #36]	; 0x24
 800841e:	4b5a      	ldr	r3, [pc, #360]	; (8008588 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x1a0>)
 8008420:	623b      	str	r3, [r7, #32]
 8008422:	2300      	movs	r3, #0
 8008424:	60fb      	str	r3, [r7, #12]
 8008426:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8008428:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 800842a:	18d2      	adds	r2, r2, r3
 800842c:	683b      	ldr	r3, [r7, #0]
 800842e:	601a      	str	r2, [r3, #0]
 8008430:	000d      	movs	r5, r1
 8008432:	187c      	adds	r4, r7, r1
 8008434:	2318      	movs	r3, #24
 8008436:	18fa      	adds	r2, r7, r3
 8008438:	687b      	ldr	r3, [r7, #4]
 800843a:	0011      	movs	r1, r2
 800843c:	0018      	movs	r0, r3
 800843e:	f7fc fb67 	bl	8004b10 <VL53L0X_GetSequenceStepEnables>
 8008442:	0003      	movs	r3, r0
 8008444:	7023      	strb	r3, [r4, #0]
 8008446:	197b      	adds	r3, r7, r5
 8008448:	781b      	ldrb	r3, [r3, #0]
 800844a:	b25b      	sxtb	r3, r3
 800844c:	2b00      	cmp	r3, #0
 800844e:	d003      	beq.n	8008458 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x70>
 8008450:	197b      	adds	r3, r7, r5
 8008452:	781b      	ldrb	r3, [r3, #0]
 8008454:	b25b      	sxtb	r3, r3
 8008456:	e08d      	b.n	8008574 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x18c>
 8008458:	2218      	movs	r2, #24
 800845a:	18bb      	adds	r3, r7, r2
 800845c:	781b      	ldrb	r3, [r3, #0]
 800845e:	2b00      	cmp	r3, #0
 8008460:	d107      	bne.n	8008472 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x8a>
 8008462:	18bb      	adds	r3, r7, r2
 8008464:	785b      	ldrb	r3, [r3, #1]
 8008466:	2b00      	cmp	r3, #0
 8008468:	d103      	bne.n	8008472 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x8a>
 800846a:	18bb      	adds	r3, r7, r2
 800846c:	789b      	ldrb	r3, [r3, #2]
 800846e:	2b00      	cmp	r3, #0
 8008470:	d038      	beq.n	80084e4 <VL53L0X_get_measurement_timing_budget_micro_seconds+0xfc>
 8008472:	253f      	movs	r5, #63	; 0x3f
 8008474:	197c      	adds	r4, r7, r5
 8008476:	2310      	movs	r3, #16
 8008478:	18fa      	adds	r2, r7, r3
 800847a:	687b      	ldr	r3, [r7, #4]
 800847c:	2102      	movs	r1, #2
 800847e:	0018      	movs	r0, r3
 8008480:	f7ff fc0b 	bl	8007c9a <get_sequence_step_timeout>
 8008484:	0003      	movs	r3, r0
 8008486:	7023      	strb	r3, [r4, #0]
 8008488:	197b      	adds	r3, r7, r5
 800848a:	781b      	ldrb	r3, [r3, #0]
 800848c:	b25b      	sxtb	r3, r3
 800848e:	2b00      	cmp	r3, #0
 8008490:	d128      	bne.n	80084e4 <VL53L0X_get_measurement_timing_budget_micro_seconds+0xfc>
 8008492:	2318      	movs	r3, #24
 8008494:	18fb      	adds	r3, r7, r3
 8008496:	781b      	ldrb	r3, [r3, #0]
 8008498:	2b00      	cmp	r3, #0
 800849a:	d007      	beq.n	80084ac <VL53L0X_get_measurement_timing_budget_micro_seconds+0xc4>
 800849c:	683b      	ldr	r3, [r7, #0]
 800849e:	681a      	ldr	r2, [r3, #0]
 80084a0:	6939      	ldr	r1, [r7, #16]
 80084a2:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 80084a4:	18cb      	adds	r3, r1, r3
 80084a6:	18d2      	adds	r2, r2, r3
 80084a8:	683b      	ldr	r3, [r7, #0]
 80084aa:	601a      	str	r2, [r3, #0]
 80084ac:	2318      	movs	r3, #24
 80084ae:	18fb      	adds	r3, r7, r3
 80084b0:	789b      	ldrb	r3, [r3, #2]
 80084b2:	2b00      	cmp	r3, #0
 80084b4:	d009      	beq.n	80084ca <VL53L0X_get_measurement_timing_budget_micro_seconds+0xe2>
 80084b6:	683b      	ldr	r3, [r7, #0]
 80084b8:	681a      	ldr	r2, [r3, #0]
 80084ba:	6939      	ldr	r1, [r7, #16]
 80084bc:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80084be:	18cb      	adds	r3, r1, r3
 80084c0:	005b      	lsls	r3, r3, #1
 80084c2:	18d2      	adds	r2, r2, r3
 80084c4:	683b      	ldr	r3, [r7, #0]
 80084c6:	601a      	str	r2, [r3, #0]
 80084c8:	e00c      	b.n	80084e4 <VL53L0X_get_measurement_timing_budget_micro_seconds+0xfc>
 80084ca:	2318      	movs	r3, #24
 80084cc:	18fb      	adds	r3, r7, r3
 80084ce:	785b      	ldrb	r3, [r3, #1]
 80084d0:	2b00      	cmp	r3, #0
 80084d2:	d007      	beq.n	80084e4 <VL53L0X_get_measurement_timing_budget_micro_seconds+0xfc>
 80084d4:	683b      	ldr	r3, [r7, #0]
 80084d6:	681a      	ldr	r2, [r3, #0]
 80084d8:	6939      	ldr	r1, [r7, #16]
 80084da:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80084dc:	18cb      	adds	r3, r1, r3
 80084de:	18d2      	adds	r2, r2, r3
 80084e0:	683b      	ldr	r3, [r7, #0]
 80084e2:	601a      	str	r2, [r3, #0]
 80084e4:	223f      	movs	r2, #63	; 0x3f
 80084e6:	18bb      	adds	r3, r7, r2
 80084e8:	781b      	ldrb	r3, [r3, #0]
 80084ea:	b25b      	sxtb	r3, r3
 80084ec:	2b00      	cmp	r3, #0
 80084ee:	d116      	bne.n	800851e <VL53L0X_get_measurement_timing_budget_micro_seconds+0x136>
 80084f0:	2318      	movs	r3, #24
 80084f2:	18fb      	adds	r3, r7, r3
 80084f4:	78db      	ldrb	r3, [r3, #3]
 80084f6:	2b00      	cmp	r3, #0
 80084f8:	d011      	beq.n	800851e <VL53L0X_get_measurement_timing_budget_micro_seconds+0x136>
 80084fa:	18bc      	adds	r4, r7, r2
 80084fc:	230c      	movs	r3, #12
 80084fe:	18fa      	adds	r2, r7, r3
 8008500:	687b      	ldr	r3, [r7, #4]
 8008502:	2103      	movs	r1, #3
 8008504:	0018      	movs	r0, r3
 8008506:	f7ff fbc8 	bl	8007c9a <get_sequence_step_timeout>
 800850a:	0003      	movs	r3, r0
 800850c:	7023      	strb	r3, [r4, #0]
 800850e:	683b      	ldr	r3, [r7, #0]
 8008510:	681a      	ldr	r2, [r3, #0]
 8008512:	68f9      	ldr	r1, [r7, #12]
 8008514:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008516:	18cb      	adds	r3, r1, r3
 8008518:	18d2      	adds	r2, r2, r3
 800851a:	683b      	ldr	r3, [r7, #0]
 800851c:	601a      	str	r2, [r3, #0]
 800851e:	223f      	movs	r2, #63	; 0x3f
 8008520:	18bb      	adds	r3, r7, r2
 8008522:	781b      	ldrb	r3, [r3, #0]
 8008524:	b25b      	sxtb	r3, r3
 8008526:	2b00      	cmp	r3, #0
 8008528:	d116      	bne.n	8008558 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x170>
 800852a:	2318      	movs	r3, #24
 800852c:	18fb      	adds	r3, r7, r3
 800852e:	791b      	ldrb	r3, [r3, #4]
 8008530:	2b00      	cmp	r3, #0
 8008532:	d011      	beq.n	8008558 <VL53L0X_get_measurement_timing_budget_micro_seconds+0x170>
 8008534:	18bc      	adds	r4, r7, r2
 8008536:	2314      	movs	r3, #20
 8008538:	18fa      	adds	r2, r7, r3
 800853a:	687b      	ldr	r3, [r7, #4]
 800853c:	2104      	movs	r1, #4
 800853e:	0018      	movs	r0, r3
 8008540:	f7ff fbab 	bl	8007c9a <get_sequence_step_timeout>
 8008544:	0003      	movs	r3, r0
 8008546:	7023      	strb	r3, [r4, #0]
 8008548:	683b      	ldr	r3, [r7, #0]
 800854a:	681a      	ldr	r2, [r3, #0]
 800854c:	6979      	ldr	r1, [r7, #20]
 800854e:	6a3b      	ldr	r3, [r7, #32]
 8008550:	18cb      	adds	r3, r1, r3
 8008552:	18d2      	adds	r2, r2, r3
 8008554:	683b      	ldr	r3, [r7, #0]
 8008556:	601a      	str	r2, [r3, #0]
 8008558:	233f      	movs	r3, #63	; 0x3f
 800855a:	18fb      	adds	r3, r7, r3
 800855c:	781b      	ldrb	r3, [r3, #0]
 800855e:	b25b      	sxtb	r3, r3
 8008560:	2b00      	cmp	r3, #0
 8008562:	d103      	bne.n	800856c <VL53L0X_get_measurement_timing_budget_micro_seconds+0x184>
 8008564:	683b      	ldr	r3, [r7, #0]
 8008566:	681a      	ldr	r2, [r3, #0]
 8008568:	687b      	ldr	r3, [r7, #4]
 800856a:	615a      	str	r2, [r3, #20]
 800856c:	233f      	movs	r3, #63	; 0x3f
 800856e:	18fb      	adds	r3, r7, r3
 8008570:	781b      	ldrb	r3, [r3, #0]
 8008572:	b25b      	sxtb	r3, r3
 8008574:	0018      	movs	r0, r3
 8008576:	46bd      	mov	sp, r7
 8008578:	b010      	add	sp, #64	; 0x40
 800857a:	bdb0      	pop	{r4, r5, r7, pc}
 800857c:	00000776 	.word	0x00000776
 8008580:	0000024e 	.word	0x0000024e
 8008584:	000002b2 	.word	0x000002b2
 8008588:	00000226 	.word	0x00000226

0800858c <VL53L0X_load_tuning_settings>:
 800858c:	b5b0      	push	{r4, r5, r7, lr}
 800858e:	b088      	sub	sp, #32
 8008590:	af00      	add	r7, sp, #0
 8008592:	6078      	str	r0, [r7, #4]
 8008594:	6039      	str	r1, [r7, #0]
 8008596:	231f      	movs	r3, #31
 8008598:	18fb      	adds	r3, r7, r3
 800859a:	2200      	movs	r2, #0
 800859c:	701a      	strb	r2, [r3, #0]
 800859e:	2300      	movs	r3, #0
 80085a0:	617b      	str	r3, [r7, #20]
 80085a2:	e102      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 80085a4:	697b      	ldr	r3, [r7, #20]
 80085a6:	683a      	ldr	r2, [r7, #0]
 80085a8:	18d2      	adds	r2, r2, r3
 80085aa:	2113      	movs	r1, #19
 80085ac:	187b      	adds	r3, r7, r1
 80085ae:	7812      	ldrb	r2, [r2, #0]
 80085b0:	701a      	strb	r2, [r3, #0]
 80085b2:	697b      	ldr	r3, [r7, #20]
 80085b4:	3301      	adds	r3, #1
 80085b6:	617b      	str	r3, [r7, #20]
 80085b8:	187b      	adds	r3, r7, r1
 80085ba:	781b      	ldrb	r3, [r3, #0]
 80085bc:	2bff      	cmp	r3, #255	; 0xff
 80085be:	d000      	beq.n	80085c2 <VL53L0X_load_tuning_settings+0x36>
 80085c0:	e0b7      	b.n	8008732 <VL53L0X_load_tuning_settings+0x1a6>
 80085c2:	697b      	ldr	r3, [r7, #20]
 80085c4:	683a      	ldr	r2, [r7, #0]
 80085c6:	18d2      	adds	r2, r2, r3
 80085c8:	2112      	movs	r1, #18
 80085ca:	187b      	adds	r3, r7, r1
 80085cc:	7812      	ldrb	r2, [r2, #0]
 80085ce:	701a      	strb	r2, [r3, #0]
 80085d0:	697b      	ldr	r3, [r7, #20]
 80085d2:	3301      	adds	r3, #1
 80085d4:	617b      	str	r3, [r7, #20]
 80085d6:	187b      	adds	r3, r7, r1
 80085d8:	781b      	ldrb	r3, [r3, #0]
 80085da:	2b01      	cmp	r3, #1
 80085dc:	d02f      	beq.n	800863e <VL53L0X_load_tuning_settings+0xb2>
 80085de:	dc02      	bgt.n	80085e6 <VL53L0X_load_tuning_settings+0x5a>
 80085e0:	2b00      	cmp	r3, #0
 80085e2:	d005      	beq.n	80085f0 <VL53L0X_load_tuning_settings+0x64>
 80085e4:	e0a0      	b.n	8008728 <VL53L0X_load_tuning_settings+0x19c>
 80085e6:	2b02      	cmp	r3, #2
 80085e8:	d050      	beq.n	800868c <VL53L0X_load_tuning_settings+0x100>
 80085ea:	2b03      	cmp	r3, #3
 80085ec:	d075      	beq.n	80086da <VL53L0X_load_tuning_settings+0x14e>
 80085ee:	e09b      	b.n	8008728 <VL53L0X_load_tuning_settings+0x19c>
 80085f0:	697b      	ldr	r3, [r7, #20]
 80085f2:	683a      	ldr	r2, [r7, #0]
 80085f4:	18d2      	adds	r2, r2, r3
 80085f6:	2111      	movs	r1, #17
 80085f8:	187b      	adds	r3, r7, r1
 80085fa:	7812      	ldrb	r2, [r2, #0]
 80085fc:	701a      	strb	r2, [r3, #0]
 80085fe:	697b      	ldr	r3, [r7, #20]
 8008600:	3301      	adds	r3, #1
 8008602:	617b      	str	r3, [r7, #20]
 8008604:	697b      	ldr	r3, [r7, #20]
 8008606:	683a      	ldr	r2, [r7, #0]
 8008608:	18d2      	adds	r2, r2, r3
 800860a:	2010      	movs	r0, #16
 800860c:	183b      	adds	r3, r7, r0
 800860e:	7812      	ldrb	r2, [r2, #0]
 8008610:	701a      	strb	r2, [r3, #0]
 8008612:	697b      	ldr	r3, [r7, #20]
 8008614:	3301      	adds	r3, #1
 8008616:	617b      	str	r3, [r7, #20]
 8008618:	187b      	adds	r3, r7, r1
 800861a:	781b      	ldrb	r3, [r3, #0]
 800861c:	b29b      	uxth	r3, r3
 800861e:	021b      	lsls	r3, r3, #8
 8008620:	b299      	uxth	r1, r3
 8008622:	183b      	adds	r3, r7, r0
 8008624:	781b      	ldrb	r3, [r3, #0]
 8008626:	b29a      	uxth	r2, r3
 8008628:	200e      	movs	r0, #14
 800862a:	183b      	adds	r3, r7, r0
 800862c:	188a      	adds	r2, r1, r2
 800862e:	801a      	strh	r2, [r3, #0]
 8008630:	687a      	ldr	r2, [r7, #4]
 8008632:	1839      	adds	r1, r7, r0
 8008634:	238a      	movs	r3, #138	; 0x8a
 8008636:	005b      	lsls	r3, r3, #1
 8008638:	8809      	ldrh	r1, [r1, #0]
 800863a:	52d1      	strh	r1, [r2, r3]
 800863c:	e0b5      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 800863e:	697b      	ldr	r3, [r7, #20]
 8008640:	683a      	ldr	r2, [r7, #0]
 8008642:	18d2      	adds	r2, r2, r3
 8008644:	2111      	movs	r1, #17
 8008646:	187b      	adds	r3, r7, r1
 8008648:	7812      	ldrb	r2, [r2, #0]
 800864a:	701a      	strb	r2, [r3, #0]
 800864c:	697b      	ldr	r3, [r7, #20]
 800864e:	3301      	adds	r3, #1
 8008650:	617b      	str	r3, [r7, #20]
 8008652:	697b      	ldr	r3, [r7, #20]
 8008654:	683a      	ldr	r2, [r7, #0]
 8008656:	18d2      	adds	r2, r2, r3
 8008658:	2010      	movs	r0, #16
 800865a:	183b      	adds	r3, r7, r0
 800865c:	7812      	ldrb	r2, [r2, #0]
 800865e:	701a      	strb	r2, [r3, #0]
 8008660:	697b      	ldr	r3, [r7, #20]
 8008662:	3301      	adds	r3, #1
 8008664:	617b      	str	r3, [r7, #20]
 8008666:	187b      	adds	r3, r7, r1
 8008668:	781b      	ldrb	r3, [r3, #0]
 800866a:	b29b      	uxth	r3, r3
 800866c:	021b      	lsls	r3, r3, #8
 800866e:	b299      	uxth	r1, r3
 8008670:	183b      	adds	r3, r7, r0
 8008672:	781b      	ldrb	r3, [r3, #0]
 8008674:	b29a      	uxth	r2, r3
 8008676:	200e      	movs	r0, #14
 8008678:	183b      	adds	r3, r7, r0
 800867a:	188a      	adds	r2, r1, r2
 800867c:	801a      	strh	r2, [r3, #0]
 800867e:	687a      	ldr	r2, [r7, #4]
 8008680:	1839      	adds	r1, r7, r0
 8008682:	238b      	movs	r3, #139	; 0x8b
 8008684:	005b      	lsls	r3, r3, #1
 8008686:	8809      	ldrh	r1, [r1, #0]
 8008688:	52d1      	strh	r1, [r2, r3]
 800868a:	e08e      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 800868c:	697b      	ldr	r3, [r7, #20]
 800868e:	683a      	ldr	r2, [r7, #0]
 8008690:	18d2      	adds	r2, r2, r3
 8008692:	2111      	movs	r1, #17
 8008694:	187b      	adds	r3, r7, r1
 8008696:	7812      	ldrb	r2, [r2, #0]
 8008698:	701a      	strb	r2, [r3, #0]
 800869a:	697b      	ldr	r3, [r7, #20]
 800869c:	3301      	adds	r3, #1
 800869e:	617b      	str	r3, [r7, #20]
 80086a0:	697b      	ldr	r3, [r7, #20]
 80086a2:	683a      	ldr	r2, [r7, #0]
 80086a4:	18d2      	adds	r2, r2, r3
 80086a6:	2010      	movs	r0, #16
 80086a8:	183b      	adds	r3, r7, r0
 80086aa:	7812      	ldrb	r2, [r2, #0]
 80086ac:	701a      	strb	r2, [r3, #0]
 80086ae:	697b      	ldr	r3, [r7, #20]
 80086b0:	3301      	adds	r3, #1
 80086b2:	617b      	str	r3, [r7, #20]
 80086b4:	187b      	adds	r3, r7, r1
 80086b6:	781b      	ldrb	r3, [r3, #0]
 80086b8:	b29b      	uxth	r3, r3
 80086ba:	021b      	lsls	r3, r3, #8
 80086bc:	b299      	uxth	r1, r3
 80086be:	183b      	adds	r3, r7, r0
 80086c0:	781b      	ldrb	r3, [r3, #0]
 80086c2:	b29a      	uxth	r2, r3
 80086c4:	200e      	movs	r0, #14
 80086c6:	183b      	adds	r3, r7, r0
 80086c8:	188a      	adds	r2, r1, r2
 80086ca:	801a      	strh	r2, [r3, #0]
 80086cc:	687a      	ldr	r2, [r7, #4]
 80086ce:	1839      	adds	r1, r7, r0
 80086d0:	238c      	movs	r3, #140	; 0x8c
 80086d2:	005b      	lsls	r3, r3, #1
 80086d4:	8809      	ldrh	r1, [r1, #0]
 80086d6:	52d1      	strh	r1, [r2, r3]
 80086d8:	e067      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 80086da:	697b      	ldr	r3, [r7, #20]
 80086dc:	683a      	ldr	r2, [r7, #0]
 80086de:	18d2      	adds	r2, r2, r3
 80086e0:	2111      	movs	r1, #17
 80086e2:	187b      	adds	r3, r7, r1
 80086e4:	7812      	ldrb	r2, [r2, #0]
 80086e6:	701a      	strb	r2, [r3, #0]
 80086e8:	697b      	ldr	r3, [r7, #20]
 80086ea:	3301      	adds	r3, #1
 80086ec:	617b      	str	r3, [r7, #20]
 80086ee:	697b      	ldr	r3, [r7, #20]
 80086f0:	683a      	ldr	r2, [r7, #0]
 80086f2:	18d2      	adds	r2, r2, r3
 80086f4:	2010      	movs	r0, #16
 80086f6:	183b      	adds	r3, r7, r0
 80086f8:	7812      	ldrb	r2, [r2, #0]
 80086fa:	701a      	strb	r2, [r3, #0]
 80086fc:	697b      	ldr	r3, [r7, #20]
 80086fe:	3301      	adds	r3, #1
 8008700:	617b      	str	r3, [r7, #20]
 8008702:	187b      	adds	r3, r7, r1
 8008704:	781b      	ldrb	r3, [r3, #0]
 8008706:	b29b      	uxth	r3, r3
 8008708:	021b      	lsls	r3, r3, #8
 800870a:	b299      	uxth	r1, r3
 800870c:	183b      	adds	r3, r7, r0
 800870e:	781b      	ldrb	r3, [r3, #0]
 8008710:	b29a      	uxth	r2, r3
 8008712:	200e      	movs	r0, #14
 8008714:	183b      	adds	r3, r7, r0
 8008716:	188a      	adds	r2, r1, r2
 8008718:	801a      	strh	r2, [r3, #0]
 800871a:	687a      	ldr	r2, [r7, #4]
 800871c:	1839      	adds	r1, r7, r0
 800871e:	238e      	movs	r3, #142	; 0x8e
 8008720:	005b      	lsls	r3, r3, #1
 8008722:	8809      	ldrh	r1, [r1, #0]
 8008724:	52d1      	strh	r1, [r2, r3]
 8008726:	e040      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 8008728:	231f      	movs	r3, #31
 800872a:	18fb      	adds	r3, r7, r3
 800872c:	22fc      	movs	r2, #252	; 0xfc
 800872e:	701a      	strb	r2, [r3, #0]
 8008730:	e03b      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 8008732:	2313      	movs	r3, #19
 8008734:	18fb      	adds	r3, r7, r3
 8008736:	781b      	ldrb	r3, [r3, #0]
 8008738:	2b04      	cmp	r3, #4
 800873a:	d832      	bhi.n	80087a2 <VL53L0X_load_tuning_settings+0x216>
 800873c:	697b      	ldr	r3, [r7, #20]
 800873e:	683a      	ldr	r2, [r7, #0]
 8008740:	18d2      	adds	r2, r2, r3
 8008742:	230d      	movs	r3, #13
 8008744:	18fb      	adds	r3, r7, r3
 8008746:	7812      	ldrb	r2, [r2, #0]
 8008748:	701a      	strb	r2, [r3, #0]
 800874a:	697b      	ldr	r3, [r7, #20]
 800874c:	3301      	adds	r3, #1
 800874e:	617b      	str	r3, [r7, #20]
 8008750:	2300      	movs	r3, #0
 8008752:	61bb      	str	r3, [r7, #24]
 8008754:	e00f      	b.n	8008776 <VL53L0X_load_tuning_settings+0x1ea>
 8008756:	697b      	ldr	r3, [r7, #20]
 8008758:	683a      	ldr	r2, [r7, #0]
 800875a:	18d3      	adds	r3, r2, r3
 800875c:	7819      	ldrb	r1, [r3, #0]
 800875e:	2308      	movs	r3, #8
 8008760:	18fa      	adds	r2, r7, r3
 8008762:	69bb      	ldr	r3, [r7, #24]
 8008764:	18d3      	adds	r3, r2, r3
 8008766:	1c0a      	adds	r2, r1, #0
 8008768:	701a      	strb	r2, [r3, #0]
 800876a:	697b      	ldr	r3, [r7, #20]
 800876c:	3301      	adds	r3, #1
 800876e:	617b      	str	r3, [r7, #20]
 8008770:	69bb      	ldr	r3, [r7, #24]
 8008772:	3301      	adds	r3, #1
 8008774:	61bb      	str	r3, [r7, #24]
 8008776:	2113      	movs	r1, #19
 8008778:	187b      	adds	r3, r7, r1
 800877a:	781b      	ldrb	r3, [r3, #0]
 800877c:	69ba      	ldr	r2, [r7, #24]
 800877e:	429a      	cmp	r2, r3
 8008780:	dbe9      	blt.n	8008756 <VL53L0X_load_tuning_settings+0x1ca>
 8008782:	187b      	adds	r3, r7, r1
 8008784:	781d      	ldrb	r5, [r3, #0]
 8008786:	231f      	movs	r3, #31
 8008788:	18fc      	adds	r4, r7, r3
 800878a:	2308      	movs	r3, #8
 800878c:	18fa      	adds	r2, r7, r3
 800878e:	230d      	movs	r3, #13
 8008790:	18fb      	adds	r3, r7, r3
 8008792:	7819      	ldrb	r1, [r3, #0]
 8008794:	6878      	ldr	r0, [r7, #4]
 8008796:	002b      	movs	r3, r5
 8008798:	f7fa ffba 	bl	8003710 <VL53L0X_WriteMulti>
 800879c:	0003      	movs	r3, r0
 800879e:	7023      	strb	r3, [r4, #0]
 80087a0:	e003      	b.n	80087aa <VL53L0X_load_tuning_settings+0x21e>
 80087a2:	231f      	movs	r3, #31
 80087a4:	18fb      	adds	r3, r7, r3
 80087a6:	22fc      	movs	r2, #252	; 0xfc
 80087a8:	701a      	strb	r2, [r3, #0]
 80087aa:	697b      	ldr	r3, [r7, #20]
 80087ac:	683a      	ldr	r2, [r7, #0]
 80087ae:	18d3      	adds	r3, r2, r3
 80087b0:	781b      	ldrb	r3, [r3, #0]
 80087b2:	2b00      	cmp	r3, #0
 80087b4:	d006      	beq.n	80087c4 <VL53L0X_load_tuning_settings+0x238>
 80087b6:	231f      	movs	r3, #31
 80087b8:	18fb      	adds	r3, r7, r3
 80087ba:	781b      	ldrb	r3, [r3, #0]
 80087bc:	b25b      	sxtb	r3, r3
 80087be:	2b00      	cmp	r3, #0
 80087c0:	d100      	bne.n	80087c4 <VL53L0X_load_tuning_settings+0x238>
 80087c2:	e6ef      	b.n	80085a4 <VL53L0X_load_tuning_settings+0x18>
 80087c4:	231f      	movs	r3, #31
 80087c6:	18fb      	adds	r3, r7, r3
 80087c8:	781b      	ldrb	r3, [r3, #0]
 80087ca:	b25b      	sxtb	r3, r3
 80087cc:	0018      	movs	r0, r3
 80087ce:	46bd      	mov	sp, r7
 80087d0:	b008      	add	sp, #32
 80087d2:	bdb0      	pop	{r4, r5, r7, pc}

080087d4 <VL53L0X_get_total_xtalk_rate>:
 80087d4:	b5f0      	push	{r4, r5, r6, r7, lr}
 80087d6:	b089      	sub	sp, #36	; 0x24
 80087d8:	af00      	add	r7, sp, #0
 80087da:	60f8      	str	r0, [r7, #12]
 80087dc:	60b9      	str	r1, [r7, #8]
 80087de:	607a      	str	r2, [r7, #4]
 80087e0:	251f      	movs	r5, #31
 80087e2:	197b      	adds	r3, r7, r5
 80087e4:	2200      	movs	r2, #0
 80087e6:	701a      	strb	r2, [r3, #0]
 80087e8:	687b      	ldr	r3, [r7, #4]
 80087ea:	2200      	movs	r2, #0
 80087ec:	601a      	str	r2, [r3, #0]
 80087ee:	197c      	adds	r4, r7, r5
 80087f0:	2613      	movs	r6, #19
 80087f2:	19ba      	adds	r2, r7, r6
 80087f4:	68fb      	ldr	r3, [r7, #12]
 80087f6:	0011      	movs	r1, r2
 80087f8:	0018      	movs	r0, r3
 80087fa:	f7fc fa42 	bl	8004c82 <VL53L0X_GetXTalkCompensationEnable>
 80087fe:	0003      	movs	r3, r0
 8008800:	7023      	strb	r3, [r4, #0]
 8008802:	197b      	adds	r3, r7, r5
 8008804:	781b      	ldrb	r3, [r3, #0]
 8008806:	b25b      	sxtb	r3, r3
 8008808:	2b00      	cmp	r3, #0
 800880a:	d111      	bne.n	8008830 <VL53L0X_get_total_xtalk_rate+0x5c>
 800880c:	19bb      	adds	r3, r7, r6
 800880e:	781b      	ldrb	r3, [r3, #0]
 8008810:	2b00      	cmp	r3, #0
 8008812:	d00d      	beq.n	8008830 <VL53L0X_get_total_xtalk_rate+0x5c>
 8008814:	68fb      	ldr	r3, [r7, #12]
 8008816:	6a1b      	ldr	r3, [r3, #32]
 8008818:	61bb      	str	r3, [r7, #24]
 800881a:	68bb      	ldr	r3, [r7, #8]
 800881c:	8a9b      	ldrh	r3, [r3, #20]
 800881e:	001a      	movs	r2, r3
 8008820:	69bb      	ldr	r3, [r7, #24]
 8008822:	4353      	muls	r3, r2
 8008824:	617b      	str	r3, [r7, #20]
 8008826:	697b      	ldr	r3, [r7, #20]
 8008828:	3380      	adds	r3, #128	; 0x80
 800882a:	0a1a      	lsrs	r2, r3, #8
 800882c:	687b      	ldr	r3, [r7, #4]
 800882e:	601a      	str	r2, [r3, #0]
 8008830:	231f      	movs	r3, #31
 8008832:	18fb      	adds	r3, r7, r3
 8008834:	781b      	ldrb	r3, [r3, #0]
 8008836:	b25b      	sxtb	r3, r3
 8008838:	0018      	movs	r0, r3
 800883a:	46bd      	mov	sp, r7
 800883c:	b009      	add	sp, #36	; 0x24
 800883e:	bdf0      	pop	{r4, r5, r6, r7, pc}

08008840 <VL53L0X_get_total_signal_rate>:
 8008840:	b5b0      	push	{r4, r5, r7, lr}
 8008842:	b086      	sub	sp, #24
 8008844:	af00      	add	r7, sp, #0
 8008846:	60f8      	str	r0, [r7, #12]
 8008848:	60b9      	str	r1, [r7, #8]
 800884a:	607a      	str	r2, [r7, #4]
 800884c:	2517      	movs	r5, #23
 800884e:	197b      	adds	r3, r7, r5
 8008850:	2200      	movs	r2, #0
 8008852:	701a      	strb	r2, [r3, #0]
 8008854:	68bb      	ldr	r3, [r7, #8]
 8008856:	68da      	ldr	r2, [r3, #12]
 8008858:	687b      	ldr	r3, [r7, #4]
 800885a:	601a      	str	r2, [r3, #0]
 800885c:	197c      	adds	r4, r7, r5
 800885e:	2310      	movs	r3, #16
 8008860:	18fa      	adds	r2, r7, r3
 8008862:	68b9      	ldr	r1, [r7, #8]
 8008864:	68fb      	ldr	r3, [r7, #12]
 8008866:	0018      	movs	r0, r3
 8008868:	f7ff ffb4 	bl	80087d4 <VL53L0X_get_total_xtalk_rate>
 800886c:	0003      	movs	r3, r0
 800886e:	7023      	strb	r3, [r4, #0]
 8008870:	197b      	adds	r3, r7, r5
 8008872:	781b      	ldrb	r3, [r3, #0]
 8008874:	b25b      	sxtb	r3, r3
 8008876:	2b00      	cmp	r3, #0
 8008878:	d105      	bne.n	8008886 <VL53L0X_get_total_signal_rate+0x46>
 800887a:	687b      	ldr	r3, [r7, #4]
 800887c:	681a      	ldr	r2, [r3, #0]
 800887e:	693b      	ldr	r3, [r7, #16]
 8008880:	18d2      	adds	r2, r2, r3
 8008882:	687b      	ldr	r3, [r7, #4]
 8008884:	601a      	str	r2, [r3, #0]
 8008886:	2317      	movs	r3, #23
 8008888:	18fb      	adds	r3, r7, r3
 800888a:	781b      	ldrb	r3, [r3, #0]
 800888c:	b25b      	sxtb	r3, r3
 800888e:	0018      	movs	r0, r3
 8008890:	46bd      	mov	sp, r7
 8008892:	b006      	add	sp, #24
 8008894:	bdb0      	pop	{r4, r5, r7, pc}
	...

08008898 <VL53L0X_calc_dmax>:
 8008898:	b580      	push	{r7, lr}
 800889a:	b09a      	sub	sp, #104	; 0x68
 800889c:	af00      	add	r7, sp, #0
 800889e:	60f8      	str	r0, [r7, #12]
 80088a0:	60b9      	str	r1, [r7, #8]
 80088a2:	607a      	str	r2, [r7, #4]
 80088a4:	603b      	str	r3, [r7, #0]
 80088a6:	2312      	movs	r3, #18
 80088a8:	657b      	str	r3, [r7, #84]	; 0x54
 80088aa:	2380      	movs	r3, #128	; 0x80
 80088ac:	01db      	lsls	r3, r3, #7
 80088ae:	653b      	str	r3, [r7, #80]	; 0x50
 80088b0:	2342      	movs	r3, #66	; 0x42
 80088b2:	64fb      	str	r3, [r7, #76]	; 0x4c
 80088b4:	2306      	movs	r3, #6
 80088b6:	64bb      	str	r3, [r7, #72]	; 0x48
 80088b8:	2307      	movs	r3, #7
 80088ba:	647b      	str	r3, [r7, #68]	; 0x44
 80088bc:	2343      	movs	r3, #67	; 0x43
 80088be:	18fb      	adds	r3, r7, r3
 80088c0:	2200      	movs	r2, #0
 80088c2:	701a      	strb	r2, [r3, #0]
 80088c4:	68fa      	ldr	r2, [r7, #12]
 80088c6:	239a      	movs	r3, #154	; 0x9a
 80088c8:	005b      	lsls	r3, r3, #1
 80088ca:	5ad3      	ldrh	r3, [r2, r3]
 80088cc:	63fb      	str	r3, [r7, #60]	; 0x3c
 80088ce:	68fa      	ldr	r2, [r7, #12]
 80088d0:	239c      	movs	r3, #156	; 0x9c
 80088d2:	005b      	lsls	r3, r3, #1
 80088d4:	58d3      	ldr	r3, [r2, r3]
 80088d6:	63bb      	str	r3, [r7, #56]	; 0x38
 80088d8:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 80088da:	6bba      	ldr	r2, [r7, #56]	; 0x38
 80088dc:	4353      	muls	r3, r2
 80088de:	637b      	str	r3, [r7, #52]	; 0x34
 80088e0:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80088e2:	3380      	adds	r3, #128	; 0x80
 80088e4:	0a1b      	lsrs	r3, r3, #8
 80088e6:	637b      	str	r3, [r7, #52]	; 0x34
 80088e8:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80088ea:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 80088ec:	4353      	muls	r3, r2
 80088ee:	637b      	str	r3, [r7, #52]	; 0x34
 80088f0:	2300      	movs	r3, #0
 80088f2:	667b      	str	r3, [r7, #100]	; 0x64
 80088f4:	687b      	ldr	r3, [r7, #4]
 80088f6:	2b00      	cmp	r3, #0
 80088f8:	d01d      	beq.n	8008936 <VL53L0X_calc_dmax+0x9e>
 80088fa:	68bb      	ldr	r3, [r7, #8]
 80088fc:	029b      	lsls	r3, r3, #10
 80088fe:	633b      	str	r3, [r7, #48]	; 0x30
 8008900:	687b      	ldr	r3, [r7, #4]
 8008902:	085b      	lsrs	r3, r3, #1
 8008904:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8008906:	18d3      	adds	r3, r2, r3
 8008908:	667b      	str	r3, [r7, #100]	; 0x64
 800890a:	6879      	ldr	r1, [r7, #4]
 800890c:	6e78      	ldr	r0, [r7, #100]	; 0x64
 800890e:	f7f7 fbfb 	bl	8000108 <__udivsi3>
 8008912:	0003      	movs	r3, r0
 8008914:	667b      	str	r3, [r7, #100]	; 0x64
 8008916:	6e7a      	ldr	r2, [r7, #100]	; 0x64
 8008918:	0013      	movs	r3, r2
 800891a:	005b      	lsls	r3, r3, #1
 800891c:	189b      	adds	r3, r3, r2
 800891e:	667b      	str	r3, [r7, #100]	; 0x64
 8008920:	6e7b      	ldr	r3, [r7, #100]	; 0x64
 8008922:	6e7a      	ldr	r2, [r7, #100]	; 0x64
 8008924:	4353      	muls	r3, r2
 8008926:	667b      	str	r3, [r7, #100]	; 0x64
 8008928:	6e7b      	ldr	r3, [r7, #100]	; 0x64
 800892a:	2280      	movs	r2, #128	; 0x80
 800892c:	0212      	lsls	r2, r2, #8
 800892e:	4694      	mov	ip, r2
 8008930:	4463      	add	r3, ip
 8008932:	0c1b      	lsrs	r3, r3, #16
 8008934:	667b      	str	r3, [r7, #100]	; 0x64
 8008936:	683b      	ldr	r3, [r7, #0]
 8008938:	6f3a      	ldr	r2, [r7, #112]	; 0x70
 800893a:	4353      	muls	r3, r2
 800893c:	62fb      	str	r3, [r7, #44]	; 0x2c
 800893e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8008940:	2280      	movs	r2, #128	; 0x80
 8008942:	0212      	lsls	r2, r2, #8
 8008944:	4694      	mov	ip, r2
 8008946:	4463      	add	r3, ip
 8008948:	0c1b      	lsrs	r3, r3, #16
 800894a:	62fb      	str	r3, [r7, #44]	; 0x2c
 800894c:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 800894e:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 8008950:	4353      	muls	r3, r2
 8008952:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008954:	6f7b      	ldr	r3, [r7, #116]	; 0x74
 8008956:	2280      	movs	r2, #128	; 0x80
 8008958:	0212      	lsls	r2, r2, #8
 800895a:	4694      	mov	ip, r2
 800895c:	4463      	add	r3, ip
 800895e:	0c1b      	lsrs	r3, r3, #16
 8008960:	62bb      	str	r3, [r7, #40]	; 0x28
 8008962:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8008964:	085a      	lsrs	r2, r3, #1
 8008966:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8008968:	18d3      	adds	r3, r2, r3
 800896a:	6cb9      	ldr	r1, [r7, #72]	; 0x48
 800896c:	0018      	movs	r0, r3
 800896e:	f7f7 fbcb 	bl	8000108 <__udivsi3>
 8008972:	0003      	movs	r3, r0
 8008974:	62bb      	str	r3, [r7, #40]	; 0x28
 8008976:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8008978:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 800897a:	4353      	muls	r3, r2
 800897c:	62bb      	str	r3, [r7, #40]	; 0x28
 800897e:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8008980:	4a61      	ldr	r2, [pc, #388]	; (8008b08 <VL53L0X_calc_dmax+0x270>)
 8008982:	4293      	cmp	r3, r2
 8008984:	d902      	bls.n	800898c <VL53L0X_calc_dmax+0xf4>
 8008986:	4b61      	ldr	r3, [pc, #388]	; (8008b0c <VL53L0X_calc_dmax+0x274>)
 8008988:	663b      	str	r3, [r7, #96]	; 0x60
 800898a:	e018      	b.n	80089be <VL53L0X_calc_dmax+0x126>
 800898c:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800898e:	085a      	lsrs	r2, r3, #1
 8008990:	6f7b      	ldr	r3, [r7, #116]	; 0x74
 8008992:	18d3      	adds	r3, r2, r3
 8008994:	6cb9      	ldr	r1, [r7, #72]	; 0x48
 8008996:	0018      	movs	r0, r3
 8008998:	f7f7 fbb6 	bl	8000108 <__udivsi3>
 800899c:	0003      	movs	r3, r0
 800899e:	677b      	str	r3, [r7, #116]	; 0x74
 80089a0:	6f7b      	ldr	r3, [r7, #116]	; 0x74
 80089a2:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 80089a4:	4353      	muls	r3, r2
 80089a6:	677b      	str	r3, [r7, #116]	; 0x74
 80089a8:	6f7b      	ldr	r3, [r7, #116]	; 0x74
 80089aa:	2280      	movs	r2, #128	; 0x80
 80089ac:	0212      	lsls	r2, r2, #8
 80089ae:	4694      	mov	ip, r2
 80089b0:	4463      	add	r3, ip
 80089b2:	0c1b      	lsrs	r3, r3, #16
 80089b4:	663b      	str	r3, [r7, #96]	; 0x60
 80089b6:	6e3b      	ldr	r3, [r7, #96]	; 0x60
 80089b8:	6e3a      	ldr	r2, [r7, #96]	; 0x60
 80089ba:	4353      	muls	r3, r2
 80089bc:	663b      	str	r3, [r7, #96]	; 0x60
 80089be:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80089c0:	039b      	lsls	r3, r3, #14
 80089c2:	33f5      	adds	r3, #245	; 0xf5
 80089c4:	33ff      	adds	r3, #255	; 0xff
 80089c6:	001a      	movs	r2, r3
 80089c8:	23fa      	movs	r3, #250	; 0xfa
 80089ca:	0099      	lsls	r1, r3, #2
 80089cc:	0010      	movs	r0, r2
 80089ce:	f7f7 fb9b 	bl	8000108 <__udivsi3>
 80089d2:	0003      	movs	r3, r0
 80089d4:	627b      	str	r3, [r7, #36]	; 0x24
 80089d6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80089d8:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80089da:	4353      	muls	r3, r2
 80089dc:	627b      	str	r3, [r7, #36]	; 0x24
 80089de:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80089e0:	6cfa      	ldr	r2, [r7, #76]	; 0x4c
 80089e2:	4353      	muls	r3, r2
 80089e4:	623b      	str	r3, [r7, #32]
 80089e6:	6a3b      	ldr	r3, [r7, #32]
 80089e8:	3308      	adds	r3, #8
 80089ea:	091b      	lsrs	r3, r3, #4
 80089ec:	623b      	str	r3, [r7, #32]
 80089ee:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80089f0:	6a3b      	ldr	r3, [r7, #32]
 80089f2:	1ad3      	subs	r3, r2, r3
 80089f4:	627b      	str	r3, [r7, #36]	; 0x24
 80089f6:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80089f8:	0013      	movs	r3, r2
 80089fa:	005b      	lsls	r3, r3, #1
 80089fc:	189b      	adds	r3, r3, r2
 80089fe:	011b      	lsls	r3, r3, #4
 8008a00:	61fb      	str	r3, [r7, #28]
 8008a02:	69fb      	ldr	r3, [r7, #28]
 8008a04:	2280      	movs	r2, #128	; 0x80
 8008a06:	0192      	lsls	r2, r2, #6
 8008a08:	4694      	mov	ip, r2
 8008a0a:	4463      	add	r3, ip
 8008a0c:	0b9b      	lsrs	r3, r3, #14
 8008a0e:	61fb      	str	r3, [r7, #28]
 8008a10:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 8008a12:	6e3b      	ldr	r3, [r7, #96]	; 0x60
 8008a14:	18d3      	adds	r3, r2, r3
 8008a16:	61bb      	str	r3, [r7, #24]
 8008a18:	6fbb      	ldr	r3, [r7, #120]	; 0x78
 8008a1a:	085b      	lsrs	r3, r3, #1
 8008a1c:	69ba      	ldr	r2, [r7, #24]
 8008a1e:	18d3      	adds	r3, r2, r3
 8008a20:	61bb      	str	r3, [r7, #24]
 8008a22:	6fb9      	ldr	r1, [r7, #120]	; 0x78
 8008a24:	69b8      	ldr	r0, [r7, #24]
 8008a26:	f7f7 fb6f 	bl	8000108 <__udivsi3>
 8008a2a:	0003      	movs	r3, r0
 8008a2c:	61bb      	str	r3, [r7, #24]
 8008a2e:	69bb      	ldr	r3, [r7, #24]
 8008a30:	039b      	lsls	r3, r3, #14
 8008a32:	61bb      	str	r3, [r7, #24]
 8008a34:	69fb      	ldr	r3, [r7, #28]
 8008a36:	085b      	lsrs	r3, r3, #1
 8008a38:	69ba      	ldr	r2, [r7, #24]
 8008a3a:	18d3      	adds	r3, r2, r3
 8008a3c:	61bb      	str	r3, [r7, #24]
 8008a3e:	69f9      	ldr	r1, [r7, #28]
 8008a40:	69b8      	ldr	r0, [r7, #24]
 8008a42:	f7f7 fb61 	bl	8000108 <__udivsi3>
 8008a46:	0003      	movs	r3, r0
 8008a48:	61bb      	str	r3, [r7, #24]
 8008a4a:	69bb      	ldr	r3, [r7, #24]
 8008a4c:	6e7a      	ldr	r2, [r7, #100]	; 0x64
 8008a4e:	4353      	muls	r3, r2
 8008a50:	61bb      	str	r3, [r7, #24]
 8008a52:	69bb      	ldr	r3, [r7, #24]
 8008a54:	33f5      	adds	r3, #245	; 0xf5
 8008a56:	33ff      	adds	r3, #255	; 0xff
 8008a58:	001a      	movs	r2, r3
 8008a5a:	23fa      	movs	r3, #250	; 0xfa
 8008a5c:	0099      	lsls	r1, r3, #2
 8008a5e:	0010      	movs	r0, r2
 8008a60:	f7f7 fb52 	bl	8000108 <__udivsi3>
 8008a64:	0003      	movs	r3, r0
 8008a66:	61bb      	str	r3, [r7, #24]
 8008a68:	69bb      	ldr	r3, [r7, #24]
 8008a6a:	011b      	lsls	r3, r3, #4
 8008a6c:	61bb      	str	r3, [r7, #24]
 8008a6e:	69bb      	ldr	r3, [r7, #24]
 8008a70:	33f5      	adds	r3, #245	; 0xf5
 8008a72:	33ff      	adds	r3, #255	; 0xff
 8008a74:	001a      	movs	r2, r3
 8008a76:	23fa      	movs	r3, #250	; 0xfa
 8008a78:	0099      	lsls	r1, r3, #2
 8008a7a:	0010      	movs	r0, r2
 8008a7c:	f7f7 fb44 	bl	8000108 <__udivsi3>
 8008a80:	0003      	movs	r3, r0
 8008a82:	61bb      	str	r3, [r7, #24]
 8008a84:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 8008a86:	3380      	adds	r3, #128	; 0x80
 8008a88:	0a1b      	lsrs	r3, r3, #8
 8008a8a:	617b      	str	r3, [r7, #20]
 8008a8c:	697b      	ldr	r3, [r7, #20]
 8008a8e:	2b00      	cmp	r3, #0
 8008a90:	d00a      	beq.n	8008aa8 <VL53L0X_calc_dmax+0x210>
 8008a92:	697b      	ldr	r3, [r7, #20]
 8008a94:	085a      	lsrs	r2, r3, #1
 8008a96:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008a98:	18d3      	adds	r3, r2, r3
 8008a9a:	6979      	ldr	r1, [r7, #20]
 8008a9c:	0018      	movs	r0, r3
 8008a9e:	f7f7 fb33 	bl	8000108 <__udivsi3>
 8008aa2:	0003      	movs	r3, r0
 8008aa4:	65bb      	str	r3, [r7, #88]	; 0x58
 8008aa6:	e001      	b.n	8008aac <VL53L0X_calc_dmax+0x214>
 8008aa8:	2300      	movs	r3, #0
 8008aaa:	65bb      	str	r3, [r7, #88]	; 0x58
 8008aac:	6dbb      	ldr	r3, [r7, #88]	; 0x58
 8008aae:	0018      	movs	r0, r3
 8008ab0:	f7fe fa71 	bl	8006f96 <VL53L0X_isqrt>
 8008ab4:	0003      	movs	r3, r0
 8008ab6:	613b      	str	r3, [r7, #16]
 8008ab8:	69bb      	ldr	r3, [r7, #24]
 8008aba:	2b00      	cmp	r3, #0
 8008abc:	d00a      	beq.n	8008ad4 <VL53L0X_calc_dmax+0x23c>
 8008abe:	69bb      	ldr	r3, [r7, #24]
 8008ac0:	085a      	lsrs	r2, r3, #1
 8008ac2:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008ac4:	18d3      	adds	r3, r2, r3
 8008ac6:	69b9      	ldr	r1, [r7, #24]
 8008ac8:	0018      	movs	r0, r3
 8008aca:	f7f7 fb1d 	bl	8000108 <__udivsi3>
 8008ace:	0003      	movs	r3, r0
 8008ad0:	65fb      	str	r3, [r7, #92]	; 0x5c
 8008ad2:	e001      	b.n	8008ad8 <VL53L0X_calc_dmax+0x240>
 8008ad4:	2300      	movs	r3, #0
 8008ad6:	65fb      	str	r3, [r7, #92]	; 0x5c
 8008ad8:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008ada:	0018      	movs	r0, r3
 8008adc:	f7fe fa5b 	bl	8006f96 <VL53L0X_isqrt>
 8008ae0:	0003      	movs	r3, r0
 8008ae2:	65fb      	str	r3, [r7, #92]	; 0x5c
 8008ae4:	6ffb      	ldr	r3, [r7, #124]	; 0x7c
 8008ae6:	693a      	ldr	r2, [r7, #16]
 8008ae8:	601a      	str	r2, [r3, #0]
 8008aea:	693a      	ldr	r2, [r7, #16]
 8008aec:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008aee:	429a      	cmp	r2, r3
 8008af0:	d902      	bls.n	8008af8 <VL53L0X_calc_dmax+0x260>
 8008af2:	6ffb      	ldr	r3, [r7, #124]	; 0x7c
 8008af4:	6dfa      	ldr	r2, [r7, #92]	; 0x5c
 8008af6:	601a      	str	r2, [r3, #0]
 8008af8:	2343      	movs	r3, #67	; 0x43
 8008afa:	18fb      	adds	r3, r7, r3
 8008afc:	781b      	ldrb	r3, [r3, #0]
 8008afe:	b25b      	sxtb	r3, r3
 8008b00:	0018      	movs	r0, r3
 8008b02:	46bd      	mov	sp, r7
 8008b04:	b01a      	add	sp, #104	; 0x68
 8008b06:	bd80      	pop	{r7, pc}
 8008b08:	0000ffff 	.word	0x0000ffff
 8008b0c:	fff00000 	.word	0xfff00000

08008b10 <VL53L0X_calc_sigma_estimate>:
 8008b10:	b5b0      	push	{r4, r5, r7, lr}
 8008b12:	b0b4      	sub	sp, #208	; 0xd0
 8008b14:	af04      	add	r7, sp, #16
 8008b16:	60f8      	str	r0, [r7, #12]
 8008b18:	60b9      	str	r1, [r7, #8]
 8008b1a:	607a      	str	r2, [r7, #4]
 8008b1c:	603b      	str	r3, [r7, #0]
 8008b1e:	23c8      	movs	r3, #200	; 0xc8
 8008b20:	009b      	lsls	r3, r3, #2
 8008b22:	2290      	movs	r2, #144	; 0x90
 8008b24:	18ba      	adds	r2, r7, r2
 8008b26:	6013      	str	r3, [r2, #0]
 8008b28:	2396      	movs	r3, #150	; 0x96
 8008b2a:	009b      	lsls	r3, r3, #2
 8008b2c:	228c      	movs	r2, #140	; 0x8c
 8008b2e:	18b9      	adds	r1, r7, r2
 8008b30:	600b      	str	r3, [r1, #0]
 8008b32:	23c8      	movs	r3, #200	; 0xc8
 8008b34:	035b      	lsls	r3, r3, #13
 8008b36:	2188      	movs	r1, #136	; 0x88
 8008b38:	1879      	adds	r1, r7, r1
 8008b3a:	600b      	str	r3, [r1, #0]
 8008b3c:	4bb7      	ldr	r3, [pc, #732]	; (8008e1c <VL53L0X_calc_sigma_estimate+0x30c>)
 8008b3e:	2184      	movs	r1, #132	; 0x84
 8008b40:	1879      	adds	r1, r7, r1
 8008b42:	600b      	str	r3, [r1, #0]
 8008b44:	4bb6      	ldr	r3, [pc, #728]	; (8008e20 <VL53L0X_calc_sigma_estimate+0x310>)
 8008b46:	2180      	movs	r1, #128	; 0x80
 8008b48:	1879      	adds	r1, r7, r1
 8008b4a:	600b      	str	r3, [r1, #0]
 8008b4c:	23f0      	movs	r3, #240	; 0xf0
 8008b4e:	021b      	lsls	r3, r3, #8
 8008b50:	67fb      	str	r3, [r7, #124]	; 0x7c
 8008b52:	18bb      	adds	r3, r7, r2
 8008b54:	6819      	ldr	r1, [r3, #0]
 8008b56:	23f0      	movs	r3, #240	; 0xf0
 8008b58:	0618      	lsls	r0, r3, #24
 8008b5a:	f7f7 fad5 	bl	8000108 <__udivsi3>
 8008b5e:	0003      	movs	r3, r0
 8008b60:	67bb      	str	r3, [r7, #120]	; 0x78
 8008b62:	4bb0      	ldr	r3, [pc, #704]	; (8008e24 <VL53L0X_calc_sigma_estimate+0x314>)
 8008b64:	677b      	str	r3, [r7, #116]	; 0x74
 8008b66:	2380      	movs	r3, #128	; 0x80
 8008b68:	021b      	lsls	r3, r3, #8
 8008b6a:	673b      	str	r3, [r7, #112]	; 0x70
 8008b6c:	23c8      	movs	r3, #200	; 0xc8
 8008b6e:	039b      	lsls	r3, r3, #14
 8008b70:	66fb      	str	r3, [r7, #108]	; 0x6c
 8008b72:	4bad      	ldr	r3, [pc, #692]	; (8008e28 <VL53L0X_calc_sigma_estimate+0x318>)
 8008b74:	66bb      	str	r3, [r7, #104]	; 0x68
 8008b76:	219f      	movs	r1, #159	; 0x9f
 8008b78:	187b      	adds	r3, r7, r1
 8008b7a:	2200      	movs	r2, #0
 8008b7c:	701a      	strb	r2, [r3, #0]
 8008b7e:	68fb      	ldr	r3, [r7, #12]
 8008b80:	6a1b      	ldr	r3, [r3, #32]
 8008b82:	617b      	str	r3, [r7, #20]
 8008b84:	68bb      	ldr	r3, [r7, #8]
 8008b86:	691a      	ldr	r2, [r3, #16]
 8008b88:	0013      	movs	r3, r2
 8008b8a:	015b      	lsls	r3, r3, #5
 8008b8c:	1a9b      	subs	r3, r3, r2
 8008b8e:	009b      	lsls	r3, r3, #2
 8008b90:	189b      	adds	r3, r3, r2
 8008b92:	00db      	lsls	r3, r3, #3
 8008b94:	0c1b      	lsrs	r3, r3, #16
 8008b96:	667b      	str	r3, [r7, #100]	; 0x64
 8008b98:	68bb      	ldr	r3, [r7, #8]
 8008b9a:	68db      	ldr	r3, [r3, #12]
 8008b9c:	663b      	str	r3, [r7, #96]	; 0x60
 8008b9e:	000d      	movs	r5, r1
 8008ba0:	187c      	adds	r4, r7, r1
 8008ba2:	2310      	movs	r3, #16
 8008ba4:	18fa      	adds	r2, r7, r3
 8008ba6:	68b9      	ldr	r1, [r7, #8]
 8008ba8:	68fb      	ldr	r3, [r7, #12]
 8008baa:	0018      	movs	r0, r3
 8008bac:	f7ff fe48 	bl	8008840 <VL53L0X_get_total_signal_rate>
 8008bb0:	0003      	movs	r3, r0
 8008bb2:	7023      	strb	r3, [r4, #0]
 8008bb4:	197c      	adds	r4, r7, r5
 8008bb6:	2314      	movs	r3, #20
 8008bb8:	18fa      	adds	r2, r7, r3
 8008bba:	68b9      	ldr	r1, [r7, #8]
 8008bbc:	68fb      	ldr	r3, [r7, #12]
 8008bbe:	0018      	movs	r0, r3
 8008bc0:	f7ff fe08 	bl	80087d4 <VL53L0X_get_total_xtalk_rate>
 8008bc4:	0003      	movs	r3, r0
 8008bc6:	7023      	strb	r3, [r4, #0]
 8008bc8:	693a      	ldr	r2, [r7, #16]
 8008bca:	0013      	movs	r3, r2
 8008bcc:	015b      	lsls	r3, r3, #5
 8008bce:	1a9b      	subs	r3, r3, r2
 8008bd0:	009b      	lsls	r3, r3, #2
 8008bd2:	189b      	adds	r3, r3, r2
 8008bd4:	00db      	lsls	r3, r3, #3
 8008bd6:	65fb      	str	r3, [r7, #92]	; 0x5c
 8008bd8:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008bda:	2280      	movs	r2, #128	; 0x80
 8008bdc:	0212      	lsls	r2, r2, #8
 8008bde:	4694      	mov	ip, r2
 8008be0:	4463      	add	r3, ip
 8008be2:	0c1b      	lsrs	r3, r3, #16
 8008be4:	65fb      	str	r3, [r7, #92]	; 0x5c
 8008be6:	697a      	ldr	r2, [r7, #20]
 8008be8:	0013      	movs	r3, r2
 8008bea:	015b      	lsls	r3, r3, #5
 8008bec:	1a9b      	subs	r3, r3, r2
 8008bee:	009b      	lsls	r3, r3, #2
 8008bf0:	189b      	adds	r3, r3, r2
 8008bf2:	00db      	lsls	r3, r3, #3
 8008bf4:	21a0      	movs	r1, #160	; 0xa0
 8008bf6:	187a      	adds	r2, r7, r1
 8008bf8:	6013      	str	r3, [r2, #0]
 8008bfa:	187b      	adds	r3, r7, r1
 8008bfc:	681a      	ldr	r2, [r3, #0]
 8008bfe:	6efb      	ldr	r3, [r7, #108]	; 0x6c
 8008c00:	429a      	cmp	r2, r3
 8008c02:	d902      	bls.n	8008c0a <VL53L0X_calc_sigma_estimate+0xfa>
 8008c04:	6efb      	ldr	r3, [r7, #108]	; 0x6c
 8008c06:	187a      	adds	r2, r7, r1
 8008c08:	6013      	str	r3, [r2, #0]
 8008c0a:	239f      	movs	r3, #159	; 0x9f
 8008c0c:	18fb      	adds	r3, r7, r3
 8008c0e:	781b      	ldrb	r3, [r3, #0]
 8008c10:	b25b      	sxtb	r3, r3
 8008c12:	2b00      	cmp	r3, #0
 8008c14:	d176      	bne.n	8008d04 <VL53L0X_calc_sigma_estimate+0x1f4>
 8008c16:	68fb      	ldr	r3, [r7, #12]
 8008c18:	22dc      	movs	r2, #220	; 0xdc
 8008c1a:	589b      	ldr	r3, [r3, r2]
 8008c1c:	20b8      	movs	r0, #184	; 0xb8
 8008c1e:	183a      	adds	r2, r7, r0
 8008c20:	6013      	str	r3, [r2, #0]
 8008c22:	245b      	movs	r4, #91	; 0x5b
 8008c24:	193b      	adds	r3, r7, r4
 8008c26:	68fa      	ldr	r2, [r7, #12]
 8008c28:	21e0      	movs	r1, #224	; 0xe0
 8008c2a:	5c52      	ldrb	r2, [r2, r1]
 8008c2c:	701a      	strb	r2, [r3, #0]
 8008c2e:	193b      	adds	r3, r7, r4
 8008c30:	781a      	ldrb	r2, [r3, #0]
 8008c32:	183b      	adds	r3, r7, r0
 8008c34:	6819      	ldr	r1, [r3, #0]
 8008c36:	68fb      	ldr	r3, [r7, #12]
 8008c38:	0018      	movs	r0, r3
 8008c3a:	f7fe ffc6 	bl	8007bca <VL53L0X_calc_timeout_mclks>
 8008c3e:	0003      	movs	r3, r0
 8008c40:	657b      	str	r3, [r7, #84]	; 0x54
 8008c42:	68fb      	ldr	r3, [r7, #12]
 8008c44:	22e4      	movs	r2, #228	; 0xe4
 8008c46:	589b      	ldr	r3, [r3, r2]
 8008c48:	20b4      	movs	r0, #180	; 0xb4
 8008c4a:	183a      	adds	r2, r7, r0
 8008c4c:	6013      	str	r3, [r2, #0]
 8008c4e:	2553      	movs	r5, #83	; 0x53
 8008c50:	197b      	adds	r3, r7, r5
 8008c52:	68fa      	ldr	r2, [r7, #12]
 8008c54:	21e8      	movs	r1, #232	; 0xe8
 8008c56:	5c52      	ldrb	r2, [r2, r1]
 8008c58:	701a      	strb	r2, [r3, #0]
 8008c5a:	197b      	adds	r3, r7, r5
 8008c5c:	781a      	ldrb	r2, [r3, #0]
 8008c5e:	183b      	adds	r3, r7, r0
 8008c60:	6819      	ldr	r1, [r3, #0]
 8008c62:	68fb      	ldr	r3, [r7, #12]
 8008c64:	0018      	movs	r0, r3
 8008c66:	f7fe ffb0 	bl	8007bca <VL53L0X_calc_timeout_mclks>
 8008c6a:	0003      	movs	r3, r0
 8008c6c:	64fb      	str	r3, [r7, #76]	; 0x4c
 8008c6e:	2303      	movs	r3, #3
 8008c70:	2298      	movs	r2, #152	; 0x98
 8008c72:	18b9      	adds	r1, r7, r2
 8008c74:	600b      	str	r3, [r1, #0]
 8008c76:	193b      	adds	r3, r7, r4
 8008c78:	781b      	ldrb	r3, [r3, #0]
 8008c7a:	2b08      	cmp	r3, #8
 8008c7c:	d102      	bne.n	8008c84 <VL53L0X_calc_sigma_estimate+0x174>
 8008c7e:	2302      	movs	r3, #2
 8008c80:	18ba      	adds	r2, r7, r2
 8008c82:	6013      	str	r3, [r2, #0]
 8008c84:	6cfa      	ldr	r2, [r7, #76]	; 0x4c
 8008c86:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 8008c88:	18d3      	adds	r3, r2, r3
 8008c8a:	2298      	movs	r2, #152	; 0x98
 8008c8c:	18ba      	adds	r2, r7, r2
 8008c8e:	6812      	ldr	r2, [r2, #0]
 8008c90:	4353      	muls	r3, r2
 8008c92:	02db      	lsls	r3, r3, #11
 8008c94:	2494      	movs	r4, #148	; 0x94
 8008c96:	193a      	adds	r2, r7, r4
 8008c98:	6013      	str	r3, [r2, #0]
 8008c9a:	193b      	adds	r3, r7, r4
 8008c9c:	681b      	ldr	r3, [r3, #0]
 8008c9e:	33f5      	adds	r3, #245	; 0xf5
 8008ca0:	33ff      	adds	r3, #255	; 0xff
 8008ca2:	001a      	movs	r2, r3
 8008ca4:	23fa      	movs	r3, #250	; 0xfa
 8008ca6:	0099      	lsls	r1, r3, #2
 8008ca8:	0010      	movs	r0, r2
 8008caa:	f7f7 fa2d 	bl	8000108 <__udivsi3>
 8008cae:	0003      	movs	r3, r0
 8008cb0:	193a      	adds	r2, r7, r4
 8008cb2:	6013      	str	r3, [r2, #0]
 8008cb4:	193b      	adds	r3, r7, r4
 8008cb6:	681b      	ldr	r3, [r3, #0]
 8008cb8:	6eba      	ldr	r2, [r7, #104]	; 0x68
 8008cba:	4353      	muls	r3, r2
 8008cbc:	193a      	adds	r2, r7, r4
 8008cbe:	6013      	str	r3, [r2, #0]
 8008cc0:	193b      	adds	r3, r7, r4
 8008cc2:	681b      	ldr	r3, [r3, #0]
 8008cc4:	33f5      	adds	r3, #245	; 0xf5
 8008cc6:	33ff      	adds	r3, #255	; 0xff
 8008cc8:	001a      	movs	r2, r3
 8008cca:	23fa      	movs	r3, #250	; 0xfa
 8008ccc:	0099      	lsls	r1, r3, #2
 8008cce:	0010      	movs	r0, r2
 8008cd0:	f7f7 fa1a 	bl	8000108 <__udivsi3>
 8008cd4:	0003      	movs	r3, r0
 8008cd6:	0021      	movs	r1, r4
 8008cd8:	187a      	adds	r2, r7, r1
 8008cda:	6013      	str	r3, [r2, #0]
 8008cdc:	693b      	ldr	r3, [r7, #16]
 8008cde:	3380      	adds	r3, #128	; 0x80
 8008ce0:	0a1b      	lsrs	r3, r3, #8
 8008ce2:	613b      	str	r3, [r7, #16]
 8008ce4:	693a      	ldr	r2, [r7, #16]
 8008ce6:	187b      	adds	r3, r7, r1
 8008ce8:	681b      	ldr	r3, [r3, #0]
 8008cea:	4353      	muls	r3, r2
 8008cec:	22bc      	movs	r2, #188	; 0xbc
 8008cee:	18b9      	adds	r1, r7, r2
 8008cf0:	600b      	str	r3, [r1, #0]
 8008cf2:	18bb      	adds	r3, r7, r2
 8008cf4:	681b      	ldr	r3, [r3, #0]
 8008cf6:	3380      	adds	r3, #128	; 0x80
 8008cf8:	0a1b      	lsrs	r3, r3, #8
 8008cfa:	18ba      	adds	r2, r7, r2
 8008cfc:	6013      	str	r3, [r2, #0]
 8008cfe:	693b      	ldr	r3, [r7, #16]
 8008d00:	021b      	lsls	r3, r3, #8
 8008d02:	613b      	str	r3, [r7, #16]
 8008d04:	229f      	movs	r2, #159	; 0x9f
 8008d06:	18bb      	adds	r3, r7, r2
 8008d08:	781b      	ldrb	r3, [r3, #0]
 8008d0a:	b25b      	sxtb	r3, r3
 8008d0c:	2b00      	cmp	r3, #0
 8008d0e:	d003      	beq.n	8008d18 <VL53L0X_calc_sigma_estimate+0x208>
 8008d10:	18bb      	adds	r3, r7, r2
 8008d12:	781b      	ldrb	r3, [r3, #0]
 8008d14:	b25b      	sxtb	r3, r3
 8008d16:	e1a5      	b.n	8009064 <VL53L0X_calc_sigma_estimate+0x554>
 8008d18:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008d1a:	2b00      	cmp	r3, #0
 8008d1c:	d10e      	bne.n	8008d3c <VL53L0X_calc_sigma_estimate+0x22c>
 8008d1e:	687b      	ldr	r3, [r7, #4]
 8008d20:	2180      	movs	r1, #128	; 0x80
 8008d22:	187a      	adds	r2, r7, r1
 8008d24:	6812      	ldr	r2, [r2, #0]
 8008d26:	601a      	str	r2, [r3, #0]
 8008d28:	68fa      	ldr	r2, [r7, #12]
 8008d2a:	2390      	movs	r3, #144	; 0x90
 8008d2c:	005b      	lsls	r3, r3, #1
 8008d2e:	1879      	adds	r1, r7, r1
 8008d30:	6809      	ldr	r1, [r1, #0]
 8008d32:	50d1      	str	r1, [r2, r3]
 8008d34:	683b      	ldr	r3, [r7, #0]
 8008d36:	2200      	movs	r2, #0
 8008d38:	601a      	str	r2, [r3, #0]
 8008d3a:	e18f      	b.n	800905c <VL53L0X_calc_sigma_estimate+0x54c>
 8008d3c:	22bc      	movs	r2, #188	; 0xbc
 8008d3e:	18bb      	adds	r3, r7, r2
 8008d40:	681b      	ldr	r3, [r3, #0]
 8008d42:	2b00      	cmp	r3, #0
 8008d44:	d102      	bne.n	8008d4c <VL53L0X_calc_sigma_estimate+0x23c>
 8008d46:	2301      	movs	r3, #1
 8008d48:	18ba      	adds	r2, r7, r2
 8008d4a:	6013      	str	r3, [r2, #0]
 8008d4c:	2390      	movs	r3, #144	; 0x90
 8008d4e:	18fb      	adds	r3, r7, r3
 8008d50:	681b      	ldr	r3, [r3, #0]
 8008d52:	64bb      	str	r3, [r7, #72]	; 0x48
 8008d54:	6e7b      	ldr	r3, [r7, #100]	; 0x64
 8008d56:	041b      	lsls	r3, r3, #16
 8008d58:	6df9      	ldr	r1, [r7, #92]	; 0x5c
 8008d5a:	0018      	movs	r0, r3
 8008d5c:	f7f7 f9d4 	bl	8000108 <__udivsi3>
 8008d60:	0003      	movs	r3, r0
 8008d62:	21b0      	movs	r1, #176	; 0xb0
 8008d64:	187a      	adds	r2, r7, r1
 8008d66:	6013      	str	r3, [r2, #0]
 8008d68:	187b      	adds	r3, r7, r1
 8008d6a:	681a      	ldr	r2, [r3, #0]
 8008d6c:	6fbb      	ldr	r3, [r7, #120]	; 0x78
 8008d6e:	429a      	cmp	r2, r3
 8008d70:	d902      	bls.n	8008d78 <VL53L0X_calc_sigma_estimate+0x268>
 8008d72:	6fbb      	ldr	r3, [r7, #120]	; 0x78
 8008d74:	187a      	adds	r2, r7, r1
 8008d76:	6013      	str	r3, [r2, #0]
 8008d78:	21b0      	movs	r1, #176	; 0xb0
 8008d7a:	187b      	adds	r3, r7, r1
 8008d7c:	681b      	ldr	r3, [r3, #0]
 8008d7e:	228c      	movs	r2, #140	; 0x8c
 8008d80:	18ba      	adds	r2, r7, r2
 8008d82:	6812      	ldr	r2, [r2, #0]
 8008d84:	4353      	muls	r3, r2
 8008d86:	187a      	adds	r2, r7, r1
 8008d88:	6013      	str	r3, [r2, #0]
 8008d8a:	23bc      	movs	r3, #188	; 0xbc
 8008d8c:	18fb      	adds	r3, r7, r3
 8008d8e:	681a      	ldr	r2, [r3, #0]
 8008d90:	0013      	movs	r3, r2
 8008d92:	005b      	lsls	r3, r3, #1
 8008d94:	189b      	adds	r3, r3, r2
 8008d96:	009b      	lsls	r3, r3, #2
 8008d98:	0018      	movs	r0, r3
 8008d9a:	f7fe f8fc 	bl	8006f96 <VL53L0X_isqrt>
 8008d9e:	0003      	movs	r3, r0
 8008da0:	005b      	lsls	r3, r3, #1
 8008da2:	647b      	str	r3, [r7, #68]	; 0x44
 8008da4:	68bb      	ldr	r3, [r7, #8]
 8008da6:	891b      	ldrh	r3, [r3, #8]
 8008da8:	001a      	movs	r2, r3
 8008daa:	6f7b      	ldr	r3, [r7, #116]	; 0x74
 8008dac:	4353      	muls	r3, r2
 8008dae:	643b      	str	r3, [r7, #64]	; 0x40
 8008db0:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008db2:	041a      	lsls	r2, r3, #16
 8008db4:	23a0      	movs	r3, #160	; 0xa0
 8008db6:	18fb      	adds	r3, r7, r3
 8008db8:	681b      	ldr	r3, [r3, #0]
 8008dba:	005b      	lsls	r3, r3, #1
 8008dbc:	1ad3      	subs	r3, r2, r3
 8008dbe:	33f5      	adds	r3, #245	; 0xf5
 8008dc0:	33ff      	adds	r3, #255	; 0xff
 8008dc2:	001a      	movs	r2, r3
 8008dc4:	23fa      	movs	r3, #250	; 0xfa
 8008dc6:	0099      	lsls	r1, r3, #2
 8008dc8:	0010      	movs	r0, r2
 8008dca:	f7f7 f99d 	bl	8000108 <__udivsi3>
 8008dce:	0003      	movs	r3, r0
 8008dd0:	63fb      	str	r3, [r7, #60]	; 0x3c
 8008dd2:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008dd4:	041b      	lsls	r3, r3, #16
 8008dd6:	33f5      	adds	r3, #245	; 0xf5
 8008dd8:	33ff      	adds	r3, #255	; 0xff
 8008dda:	001a      	movs	r2, r3
 8008ddc:	23fa      	movs	r3, #250	; 0xfa
 8008dde:	0099      	lsls	r1, r3, #2
 8008de0:	0010      	movs	r0, r2
 8008de2:	f7f7 f991 	bl	8000108 <__udivsi3>
 8008de6:	0003      	movs	r3, r0
 8008de8:	63bb      	str	r3, [r7, #56]	; 0x38
 8008dea:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8008dec:	021b      	lsls	r3, r3, #8
 8008dee:	63fb      	str	r3, [r7, #60]	; 0x3c
 8008df0:	6bb9      	ldr	r1, [r7, #56]	; 0x38
 8008df2:	6bf8      	ldr	r0, [r7, #60]	; 0x3c
 8008df4:	f7f7 f988 	bl	8000108 <__udivsi3>
 8008df8:	0003      	movs	r3, r0
 8008dfa:	17da      	asrs	r2, r3, #31
 8008dfc:	189b      	adds	r3, r3, r2
 8008dfe:	4053      	eors	r3, r2
 8008e00:	637b      	str	r3, [r7, #52]	; 0x34
 8008e02:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008e04:	021b      	lsls	r3, r3, #8
 8008e06:	637b      	str	r3, [r7, #52]	; 0x34
 8008e08:	68bb      	ldr	r3, [r7, #8]
 8008e0a:	7e1b      	ldrb	r3, [r3, #24]
 8008e0c:	2b00      	cmp	r3, #0
 8008e0e:	d00d      	beq.n	8008e2c <VL53L0X_calc_sigma_estimate+0x31c>
 8008e10:	2380      	movs	r3, #128	; 0x80
 8008e12:	025b      	lsls	r3, r3, #9
 8008e14:	22ac      	movs	r2, #172	; 0xac
 8008e16:	18ba      	adds	r2, r7, r2
 8008e18:	6013      	str	r3, [r2, #0]
 8008e1a:	e03d      	b.n	8008e98 <VL53L0X_calc_sigma_estimate+0x388>
 8008e1c:	0000125c 	.word	0x0000125c
 8008e20:	028f87ae 	.word	0x028f87ae
 8008e24:	0006999a 	.word	0x0006999a
 8008e28:	00000677 	.word	0x00000677
 8008e2c:	2384      	movs	r3, #132	; 0x84
 8008e2e:	18fb      	adds	r3, r7, r3
 8008e30:	6819      	ldr	r1, [r3, #0]
 8008e32:	6c38      	ldr	r0, [r7, #64]	; 0x40
 8008e34:	f7f7 f968 	bl	8000108 <__udivsi3>
 8008e38:	0003      	movs	r3, r0
 8008e3a:	21ac      	movs	r1, #172	; 0xac
 8008e3c:	187a      	adds	r2, r7, r1
 8008e3e:	6013      	str	r3, [r2, #0]
 8008e40:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008e42:	2280      	movs	r2, #128	; 0x80
 8008e44:	0252      	lsls	r2, r2, #9
 8008e46:	1ad2      	subs	r2, r2, r3
 8008e48:	187b      	adds	r3, r7, r1
 8008e4a:	681b      	ldr	r3, [r3, #0]
 8008e4c:	4353      	muls	r3, r2
 8008e4e:	187a      	adds	r2, r7, r1
 8008e50:	6013      	str	r3, [r2, #0]
 8008e52:	187b      	adds	r3, r7, r1
 8008e54:	681a      	ldr	r2, [r3, #0]
 8008e56:	6f3b      	ldr	r3, [r7, #112]	; 0x70
 8008e58:	18d3      	adds	r3, r2, r3
 8008e5a:	0c1b      	lsrs	r3, r3, #16
 8008e5c:	000a      	movs	r2, r1
 8008e5e:	18b9      	adds	r1, r7, r2
 8008e60:	600b      	str	r3, [r1, #0]
 8008e62:	18bb      	adds	r3, r7, r2
 8008e64:	681b      	ldr	r3, [r3, #0]
 8008e66:	2180      	movs	r1, #128	; 0x80
 8008e68:	0249      	lsls	r1, r1, #9
 8008e6a:	468c      	mov	ip, r1
 8008e6c:	4463      	add	r3, ip
 8008e6e:	18b9      	adds	r1, r7, r2
 8008e70:	600b      	str	r3, [r1, #0]
 8008e72:	18bb      	adds	r3, r7, r2
 8008e74:	681b      	ldr	r3, [r3, #0]
 8008e76:	085b      	lsrs	r3, r3, #1
 8008e78:	0011      	movs	r1, r2
 8008e7a:	187a      	adds	r2, r7, r1
 8008e7c:	6013      	str	r3, [r2, #0]
 8008e7e:	187b      	adds	r3, r7, r1
 8008e80:	681b      	ldr	r3, [r3, #0]
 8008e82:	187a      	adds	r2, r7, r1
 8008e84:	6812      	ldr	r2, [r2, #0]
 8008e86:	4353      	muls	r3, r2
 8008e88:	000a      	movs	r2, r1
 8008e8a:	18b9      	adds	r1, r7, r2
 8008e8c:	600b      	str	r3, [r1, #0]
 8008e8e:	18bb      	adds	r3, r7, r2
 8008e90:	681b      	ldr	r3, [r3, #0]
 8008e92:	0b9b      	lsrs	r3, r3, #14
 8008e94:	18ba      	adds	r2, r7, r2
 8008e96:	6013      	str	r3, [r2, #0]
 8008e98:	23ac      	movs	r3, #172	; 0xac
 8008e9a:	18fb      	adds	r3, r7, r3
 8008e9c:	681b      	ldr	r3, [r3, #0]
 8008e9e:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8008ea0:	4353      	muls	r3, r2
 8008ea2:	633b      	str	r3, [r7, #48]	; 0x30
 8008ea4:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8008ea6:	2280      	movs	r2, #128	; 0x80
 8008ea8:	0212      	lsls	r2, r2, #8
 8008eaa:	4694      	mov	ip, r2
 8008eac:	4463      	add	r3, ip
 8008eae:	0c1b      	lsrs	r3, r3, #16
 8008eb0:	633b      	str	r3, [r7, #48]	; 0x30
 8008eb2:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8008eb4:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8008eb6:	4353      	muls	r3, r2
 8008eb8:	633b      	str	r3, [r7, #48]	; 0x30
 8008eba:	23b0      	movs	r3, #176	; 0xb0
 8008ebc:	18fb      	adds	r3, r7, r3
 8008ebe:	681b      	ldr	r3, [r3, #0]
 8008ec0:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008ec2:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8008ec4:	2280      	movs	r2, #128	; 0x80
 8008ec6:	0212      	lsls	r2, r2, #8
 8008ec8:	4694      	mov	ip, r2
 8008eca:	4463      	add	r3, ip
 8008ecc:	0c1b      	lsrs	r3, r3, #16
 8008ece:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008ed0:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8008ed2:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 8008ed4:	4353      	muls	r3, r2
 8008ed6:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008ed8:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8008eda:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8008edc:	18d3      	adds	r3, r2, r3
 8008ede:	62bb      	str	r3, [r7, #40]	; 0x28
 8008ee0:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8008ee2:	0018      	movs	r0, r3
 8008ee4:	f7fe f857 	bl	8006f96 <VL53L0X_isqrt>
 8008ee8:	0003      	movs	r3, r0
 8008eea:	627b      	str	r3, [r7, #36]	; 0x24
 8008eec:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008eee:	041b      	lsls	r3, r3, #16
 8008ef0:	627b      	str	r3, [r7, #36]	; 0x24
 8008ef2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008ef4:	3332      	adds	r3, #50	; 0x32
 8008ef6:	2164      	movs	r1, #100	; 0x64
 8008ef8:	0018      	movs	r0, r3
 8008efa:	f7f7 f905 	bl	8000108 <__udivsi3>
 8008efe:	0003      	movs	r3, r0
 8008f00:	6c79      	ldr	r1, [r7, #68]	; 0x44
 8008f02:	0018      	movs	r0, r3
 8008f04:	f7f7 f900 	bl	8000108 <__udivsi3>
 8008f08:	0003      	movs	r3, r0
 8008f0a:	21a8      	movs	r1, #168	; 0xa8
 8008f0c:	187a      	adds	r2, r7, r1
 8008f0e:	6013      	str	r3, [r2, #0]
 8008f10:	187b      	adds	r3, r7, r1
 8008f12:	681b      	ldr	r3, [r3, #0]
 8008f14:	4a55      	ldr	r2, [pc, #340]	; (800906c <VL53L0X_calc_sigma_estimate+0x55c>)
 8008f16:	4353      	muls	r3, r2
 8008f18:	000a      	movs	r2, r1
 8008f1a:	18b9      	adds	r1, r7, r2
 8008f1c:	600b      	str	r3, [r1, #0]
 8008f1e:	18bb      	adds	r3, r7, r2
 8008f20:	681b      	ldr	r3, [r3, #0]
 8008f22:	4953      	ldr	r1, [pc, #332]	; (8009070 <VL53L0X_calc_sigma_estimate+0x560>)
 8008f24:	468c      	mov	ip, r1
 8008f26:	4463      	add	r3, ip
 8008f28:	18b9      	adds	r1, r7, r2
 8008f2a:	600b      	str	r3, [r1, #0]
 8008f2c:	0014      	movs	r4, r2
 8008f2e:	18bb      	adds	r3, r7, r2
 8008f30:	681b      	ldr	r3, [r3, #0]
 8008f32:	4950      	ldr	r1, [pc, #320]	; (8009074 <VL53L0X_calc_sigma_estimate+0x564>)
 8008f34:	0018      	movs	r0, r3
 8008f36:	f7f7 f8e7 	bl	8000108 <__udivsi3>
 8008f3a:	0003      	movs	r3, r0
 8008f3c:	0021      	movs	r1, r4
 8008f3e:	187a      	adds	r2, r7, r1
 8008f40:	6013      	str	r3, [r2, #0]
 8008f42:	187b      	adds	r3, r7, r1
 8008f44:	681a      	ldr	r2, [r3, #0]
 8008f46:	6ffb      	ldr	r3, [r7, #124]	; 0x7c
 8008f48:	429a      	cmp	r2, r3
 8008f4a:	d902      	bls.n	8008f52 <VL53L0X_calc_sigma_estimate+0x442>
 8008f4c:	6ffb      	ldr	r3, [r7, #124]	; 0x7c
 8008f4e:	187a      	adds	r2, r7, r1
 8008f50:	6013      	str	r3, [r2, #0]
 8008f52:	23b8      	movs	r3, #184	; 0xb8
 8008f54:	18fb      	adds	r3, r7, r3
 8008f56:	681a      	ldr	r2, [r3, #0]
 8008f58:	23b4      	movs	r3, #180	; 0xb4
 8008f5a:	18fb      	adds	r3, r7, r3
 8008f5c:	681b      	ldr	r3, [r3, #0]
 8008f5e:	18d3      	adds	r3, r2, r3
 8008f60:	33f5      	adds	r3, #245	; 0xf5
 8008f62:	33ff      	adds	r3, #255	; 0xff
 8008f64:	001a      	movs	r2, r3
 8008f66:	23fa      	movs	r3, #250	; 0xfa
 8008f68:	0099      	lsls	r1, r3, #2
 8008f6a:	0010      	movs	r0, r2
 8008f6c:	f7f7 f8cc 	bl	8000108 <__udivsi3>
 8008f70:	0003      	movs	r3, r0
 8008f72:	623b      	str	r3, [r7, #32]
 8008f74:	6a3b      	ldr	r3, [r7, #32]
 8008f76:	085a      	lsrs	r2, r3, #1
 8008f78:	2388      	movs	r3, #136	; 0x88
 8008f7a:	18fb      	adds	r3, r7, r3
 8008f7c:	681b      	ldr	r3, [r3, #0]
 8008f7e:	18d3      	adds	r3, r2, r3
 8008f80:	6a39      	ldr	r1, [r7, #32]
 8008f82:	0018      	movs	r0, r3
 8008f84:	f7f7 f8c0 	bl	8000108 <__udivsi3>
 8008f88:	0003      	movs	r3, r0
 8008f8a:	0018      	movs	r0, r3
 8008f8c:	f7fe f803 	bl	8006f96 <VL53L0X_isqrt>
 8008f90:	0003      	movs	r3, r0
 8008f92:	61fb      	str	r3, [r7, #28]
 8008f94:	69fb      	ldr	r3, [r7, #28]
 8008f96:	021b      	lsls	r3, r3, #8
 8008f98:	61fb      	str	r3, [r7, #28]
 8008f9a:	69fb      	ldr	r3, [r7, #28]
 8008f9c:	33f5      	adds	r3, #245	; 0xf5
 8008f9e:	33ff      	adds	r3, #255	; 0xff
 8008fa0:	001a      	movs	r2, r3
 8008fa2:	23fa      	movs	r3, #250	; 0xfa
 8008fa4:	0099      	lsls	r1, r3, #2
 8008fa6:	0010      	movs	r0, r2
 8008fa8:	f7f7 f8ae 	bl	8000108 <__udivsi3>
 8008fac:	0003      	movs	r3, r0
 8008fae:	61fb      	str	r3, [r7, #28]
 8008fb0:	22a8      	movs	r2, #168	; 0xa8
 8008fb2:	18bb      	adds	r3, r7, r2
 8008fb4:	681b      	ldr	r3, [r3, #0]
 8008fb6:	18ba      	adds	r2, r7, r2
 8008fb8:	6812      	ldr	r2, [r2, #0]
 8008fba:	4353      	muls	r3, r2
 8008fbc:	633b      	str	r3, [r7, #48]	; 0x30
 8008fbe:	69fb      	ldr	r3, [r7, #28]
 8008fc0:	69fa      	ldr	r2, [r7, #28]
 8008fc2:	4353      	muls	r3, r2
 8008fc4:	62fb      	str	r3, [r7, #44]	; 0x2c
 8008fc6:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8008fc8:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8008fca:	18d3      	adds	r3, r2, r3
 8008fcc:	0018      	movs	r0, r3
 8008fce:	f7fd ffe2 	bl	8006f96 <VL53L0X_isqrt>
 8008fd2:	0003      	movs	r3, r0
 8008fd4:	61bb      	str	r3, [r7, #24]
 8008fd6:	69ba      	ldr	r2, [r7, #24]
 8008fd8:	0013      	movs	r3, r2
 8008fda:	015b      	lsls	r3, r3, #5
 8008fdc:	1a9b      	subs	r3, r3, r2
 8008fde:	009b      	lsls	r3, r3, #2
 8008fe0:	189b      	adds	r3, r3, r2
 8008fe2:	00db      	lsls	r3, r3, #3
 8008fe4:	22a4      	movs	r2, #164	; 0xa4
 8008fe6:	18b9      	adds	r1, r7, r2
 8008fe8:	600b      	str	r3, [r1, #0]
 8008fea:	6dfb      	ldr	r3, [r7, #92]	; 0x5c
 8008fec:	2b00      	cmp	r3, #0
 8008fee:	d00b      	beq.n	8009008 <VL53L0X_calc_sigma_estimate+0x4f8>
 8008ff0:	23bc      	movs	r3, #188	; 0xbc
 8008ff2:	18fb      	adds	r3, r7, r3
 8008ff4:	681b      	ldr	r3, [r3, #0]
 8008ff6:	2b00      	cmp	r3, #0
 8008ff8:	d006      	beq.n	8009008 <VL53L0X_calc_sigma_estimate+0x4f8>
 8008ffa:	18bb      	adds	r3, r7, r2
 8008ffc:	681a      	ldr	r2, [r3, #0]
 8008ffe:	2380      	movs	r3, #128	; 0x80
 8009000:	18fb      	adds	r3, r7, r3
 8009002:	681b      	ldr	r3, [r3, #0]
 8009004:	429a      	cmp	r2, r3
 8009006:	d905      	bls.n	8009014 <VL53L0X_calc_sigma_estimate+0x504>
 8009008:	2380      	movs	r3, #128	; 0x80
 800900a:	18fb      	adds	r3, r7, r3
 800900c:	681b      	ldr	r3, [r3, #0]
 800900e:	22a4      	movs	r2, #164	; 0xa4
 8009010:	18ba      	adds	r2, r7, r2
 8009012:	6013      	str	r3, [r2, #0]
 8009014:	687b      	ldr	r3, [r7, #4]
 8009016:	22a4      	movs	r2, #164	; 0xa4
 8009018:	18ba      	adds	r2, r7, r2
 800901a:	6812      	ldr	r2, [r2, #0]
 800901c:	601a      	str	r2, [r3, #0]
 800901e:	687b      	ldr	r3, [r7, #4]
 8009020:	6819      	ldr	r1, [r3, #0]
 8009022:	68fa      	ldr	r2, [r7, #12]
 8009024:	2390      	movs	r3, #144	; 0x90
 8009026:	005b      	lsls	r3, r3, #1
 8009028:	50d1      	str	r1, [r2, r3]
 800902a:	6939      	ldr	r1, [r7, #16]
 800902c:	239f      	movs	r3, #159	; 0x9f
 800902e:	18fc      	adds	r4, r7, r3
 8009030:	23ac      	movs	r3, #172	; 0xac
 8009032:	18fb      	adds	r3, r7, r3
 8009034:	681d      	ldr	r5, [r3, #0]
 8009036:	6e3a      	ldr	r2, [r7, #96]	; 0x60
 8009038:	68f8      	ldr	r0, [r7, #12]
 800903a:	683b      	ldr	r3, [r7, #0]
 800903c:	9303      	str	r3, [sp, #12]
 800903e:	2394      	movs	r3, #148	; 0x94
 8009040:	18fb      	adds	r3, r7, r3
 8009042:	681b      	ldr	r3, [r3, #0]
 8009044:	9302      	str	r3, [sp, #8]
 8009046:	23b0      	movs	r3, #176	; 0xb0
 8009048:	18fb      	adds	r3, r7, r3
 800904a:	681b      	ldr	r3, [r3, #0]
 800904c:	9301      	str	r3, [sp, #4]
 800904e:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8009050:	9300      	str	r3, [sp, #0]
 8009052:	002b      	movs	r3, r5
 8009054:	f7ff fc20 	bl	8008898 <VL53L0X_calc_dmax>
 8009058:	0003      	movs	r3, r0
 800905a:	7023      	strb	r3, [r4, #0]
 800905c:	239f      	movs	r3, #159	; 0x9f
 800905e:	18fb      	adds	r3, r7, r3
 8009060:	781b      	ldrb	r3, [r3, #0]
 8009062:	b25b      	sxtb	r3, r3
 8009064:	0018      	movs	r0, r3
 8009066:	46bd      	mov	sp, r7
 8009068:	b030      	add	sp, #192	; 0xc0
 800906a:	bdb0      	pop	{r4, r5, r7, pc}
 800906c:	00000bb5 	.word	0x00000bb5
 8009070:	00001388 	.word	0x00001388
 8009074:	00002710 	.word	0x00002710

08009078 <VL53L0X_get_pal_range_status>:
 8009078:	b5b0      	push	{r4, r5, r7, lr}
 800907a:	b090      	sub	sp, #64	; 0x40
 800907c:	af00      	add	r7, sp, #0
 800907e:	60f8      	str	r0, [r7, #12]
 8009080:	0008      	movs	r0, r1
 8009082:	607a      	str	r2, [r7, #4]
 8009084:	0019      	movs	r1, r3
 8009086:	240b      	movs	r4, #11
 8009088:	193b      	adds	r3, r7, r4
 800908a:	1c02      	adds	r2, r0, #0
 800908c:	701a      	strb	r2, [r3, #0]
 800908e:	2308      	movs	r3, #8
 8009090:	18fb      	adds	r3, r7, r3
 8009092:	1c0a      	adds	r2, r1, #0
 8009094:	801a      	strh	r2, [r3, #0]
 8009096:	233f      	movs	r3, #63	; 0x3f
 8009098:	18fb      	adds	r3, r7, r3
 800909a:	2200      	movs	r2, #0
 800909c:	701a      	strb	r2, [r3, #0]
 800909e:	233d      	movs	r3, #61	; 0x3d
 80090a0:	18fb      	adds	r3, r7, r3
 80090a2:	2200      	movs	r2, #0
 80090a4:	701a      	strb	r2, [r3, #0]
 80090a6:	233c      	movs	r3, #60	; 0x3c
 80090a8:	18fb      	adds	r3, r7, r3
 80090aa:	2200      	movs	r2, #0
 80090ac:	701a      	strb	r2, [r3, #0]
 80090ae:	233b      	movs	r3, #59	; 0x3b
 80090b0:	18fb      	adds	r3, r7, r3
 80090b2:	2200      	movs	r2, #0
 80090b4:	701a      	strb	r2, [r3, #0]
 80090b6:	232b      	movs	r3, #43	; 0x2b
 80090b8:	18fb      	adds	r3, r7, r3
 80090ba:	2200      	movs	r2, #0
 80090bc:	701a      	strb	r2, [r3, #0]
 80090be:	232a      	movs	r3, #42	; 0x2a
 80090c0:	18fb      	adds	r3, r7, r3
 80090c2:	2200      	movs	r2, #0
 80090c4:	701a      	strb	r2, [r3, #0]
 80090c6:	2329      	movs	r3, #41	; 0x29
 80090c8:	18fb      	adds	r3, r7, r3
 80090ca:	2200      	movs	r2, #0
 80090cc:	701a      	strb	r2, [r3, #0]
 80090ce:	2328      	movs	r3, #40	; 0x28
 80090d0:	18fb      	adds	r3, r7, r3
 80090d2:	2200      	movs	r2, #0
 80090d4:	701a      	strb	r2, [r3, #0]
 80090d6:	2132      	movs	r1, #50	; 0x32
 80090d8:	187b      	adds	r3, r7, r1
 80090da:	2200      	movs	r2, #0
 80090dc:	701a      	strb	r2, [r3, #0]
 80090de:	2316      	movs	r3, #22
 80090e0:	18fb      	adds	r3, r7, r3
 80090e2:	2200      	movs	r2, #0
 80090e4:	801a      	strh	r2, [r3, #0]
 80090e6:	2300      	movs	r3, #0
 80090e8:	613b      	str	r3, [r7, #16]
 80090ea:	193b      	adds	r3, r7, r4
 80090ec:	781b      	ldrb	r3, [r3, #0]
 80090ee:	10db      	asrs	r3, r3, #3
 80090f0:	b2da      	uxtb	r2, r3
 80090f2:	0008      	movs	r0, r1
 80090f4:	187b      	adds	r3, r7, r1
 80090f6:	210f      	movs	r1, #15
 80090f8:	400a      	ands	r2, r1
 80090fa:	701a      	strb	r2, [r3, #0]
 80090fc:	0002      	movs	r2, r0
 80090fe:	18bb      	adds	r3, r7, r2
 8009100:	781b      	ldrb	r3, [r3, #0]
 8009102:	2b00      	cmp	r3, #0
 8009104:	d017      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 8009106:	18bb      	adds	r3, r7, r2
 8009108:	781b      	ldrb	r3, [r3, #0]
 800910a:	2b05      	cmp	r3, #5
 800910c:	d013      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 800910e:	18bb      	adds	r3, r7, r2
 8009110:	781b      	ldrb	r3, [r3, #0]
 8009112:	2b07      	cmp	r3, #7
 8009114:	d00f      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 8009116:	18bb      	adds	r3, r7, r2
 8009118:	781b      	ldrb	r3, [r3, #0]
 800911a:	2b0c      	cmp	r3, #12
 800911c:	d00b      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 800911e:	18bb      	adds	r3, r7, r2
 8009120:	781b      	ldrb	r3, [r3, #0]
 8009122:	2b0d      	cmp	r3, #13
 8009124:	d007      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 8009126:	18bb      	adds	r3, r7, r2
 8009128:	781b      	ldrb	r3, [r3, #0]
 800912a:	2b0e      	cmp	r3, #14
 800912c:	d003      	beq.n	8009136 <VL53L0X_get_pal_range_status+0xbe>
 800912e:	18bb      	adds	r3, r7, r2
 8009130:	781b      	ldrb	r3, [r3, #0]
 8009132:	2b0f      	cmp	r3, #15
 8009134:	d104      	bne.n	8009140 <VL53L0X_get_pal_range_status+0xc8>
 8009136:	233e      	movs	r3, #62	; 0x3e
 8009138:	18fb      	adds	r3, r7, r3
 800913a:	2201      	movs	r2, #1
 800913c:	701a      	strb	r2, [r3, #0]
 800913e:	e003      	b.n	8009148 <VL53L0X_get_pal_range_status+0xd0>
 8009140:	233e      	movs	r3, #62	; 0x3e
 8009142:	18fb      	adds	r3, r7, r3
 8009144:	2200      	movs	r2, #0
 8009146:	701a      	strb	r2, [r3, #0]
 8009148:	223f      	movs	r2, #63	; 0x3f
 800914a:	18bb      	adds	r3, r7, r2
 800914c:	781b      	ldrb	r3, [r3, #0]
 800914e:	b25b      	sxtb	r3, r3
 8009150:	2b00      	cmp	r3, #0
 8009152:	d109      	bne.n	8009168 <VL53L0X_get_pal_range_status+0xf0>
 8009154:	18bc      	adds	r4, r7, r2
 8009156:	232b      	movs	r3, #43	; 0x2b
 8009158:	18fa      	adds	r2, r7, r3
 800915a:	68fb      	ldr	r3, [r7, #12]
 800915c:	2100      	movs	r1, #0
 800915e:	0018      	movs	r0, r3
 8009160:	f7fb fea0 	bl	8004ea4 <VL53L0X_GetLimitCheckEnable>
 8009164:	0003      	movs	r3, r0
 8009166:	7023      	strb	r3, [r4, #0]
 8009168:	232b      	movs	r3, #43	; 0x2b
 800916a:	18fb      	adds	r3, r7, r3
 800916c:	781b      	ldrb	r3, [r3, #0]
 800916e:	2b00      	cmp	r3, #0
 8009170:	d034      	beq.n	80091dc <VL53L0X_get_pal_range_status+0x164>
 8009172:	253f      	movs	r5, #63	; 0x3f
 8009174:	197b      	adds	r3, r7, r5
 8009176:	781b      	ldrb	r3, [r3, #0]
 8009178:	b25b      	sxtb	r3, r3
 800917a:	2b00      	cmp	r3, #0
 800917c:	d12e      	bne.n	80091dc <VL53L0X_get_pal_range_status+0x164>
 800917e:	197c      	adds	r4, r7, r5
 8009180:	2310      	movs	r3, #16
 8009182:	18fb      	adds	r3, r7, r3
 8009184:	2224      	movs	r2, #36	; 0x24
 8009186:	18ba      	adds	r2, r7, r2
 8009188:	6d39      	ldr	r1, [r7, #80]	; 0x50
 800918a:	68f8      	ldr	r0, [r7, #12]
 800918c:	f7ff fcc0 	bl	8008b10 <VL53L0X_calc_sigma_estimate>
 8009190:	0003      	movs	r3, r0
 8009192:	7023      	strb	r3, [r4, #0]
 8009194:	197b      	adds	r3, r7, r5
 8009196:	781b      	ldrb	r3, [r3, #0]
 8009198:	b25b      	sxtb	r3, r3
 800919a:	2b00      	cmp	r3, #0
 800919c:	d103      	bne.n	80091a6 <VL53L0X_get_pal_range_status+0x12e>
 800919e:	693b      	ldr	r3, [r7, #16]
 80091a0:	b29a      	uxth	r2, r3
 80091a2:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80091a4:	815a      	strh	r2, [r3, #10]
 80091a6:	223f      	movs	r2, #63	; 0x3f
 80091a8:	18bb      	adds	r3, r7, r2
 80091aa:	781b      	ldrb	r3, [r3, #0]
 80091ac:	b25b      	sxtb	r3, r3
 80091ae:	2b00      	cmp	r3, #0
 80091b0:	d114      	bne.n	80091dc <VL53L0X_get_pal_range_status+0x164>
 80091b2:	18bc      	adds	r4, r7, r2
 80091b4:	2320      	movs	r3, #32
 80091b6:	18fa      	adds	r2, r7, r3
 80091b8:	68fb      	ldr	r3, [r7, #12]
 80091ba:	2100      	movs	r1, #0
 80091bc:	0018      	movs	r0, r3
 80091be:	f7fb ff15 	bl	8004fec <VL53L0X_GetLimitCheckValue>
 80091c2:	0003      	movs	r3, r0
 80091c4:	7023      	strb	r3, [r4, #0]
 80091c6:	6a3b      	ldr	r3, [r7, #32]
 80091c8:	2b00      	cmp	r3, #0
 80091ca:	d007      	beq.n	80091dc <VL53L0X_get_pal_range_status+0x164>
 80091cc:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80091ce:	6a3b      	ldr	r3, [r7, #32]
 80091d0:	429a      	cmp	r2, r3
 80091d2:	d903      	bls.n	80091dc <VL53L0X_get_pal_range_status+0x164>
 80091d4:	233d      	movs	r3, #61	; 0x3d
 80091d6:	18fb      	adds	r3, r7, r3
 80091d8:	2201      	movs	r2, #1
 80091da:	701a      	strb	r2, [r3, #0]
 80091dc:	223f      	movs	r2, #63	; 0x3f
 80091de:	18bb      	adds	r3, r7, r2
 80091e0:	781b      	ldrb	r3, [r3, #0]
 80091e2:	b25b      	sxtb	r3, r3
 80091e4:	2b00      	cmp	r3, #0
 80091e6:	d109      	bne.n	80091fc <VL53L0X_get_pal_range_status+0x184>
 80091e8:	18bc      	adds	r4, r7, r2
 80091ea:	2329      	movs	r3, #41	; 0x29
 80091ec:	18fa      	adds	r2, r7, r3
 80091ee:	68fb      	ldr	r3, [r7, #12]
 80091f0:	2102      	movs	r1, #2
 80091f2:	0018      	movs	r0, r3
 80091f4:	f7fb fe56 	bl	8004ea4 <VL53L0X_GetLimitCheckEnable>
 80091f8:	0003      	movs	r3, r0
 80091fa:	7023      	strb	r3, [r4, #0]
 80091fc:	2329      	movs	r3, #41	; 0x29
 80091fe:	18fb      	adds	r3, r7, r3
 8009200:	781b      	ldrb	r3, [r3, #0]
 8009202:	2b00      	cmp	r3, #0
 8009204:	d051      	beq.n	80092aa <VL53L0X_get_pal_range_status+0x232>
 8009206:	253f      	movs	r5, #63	; 0x3f
 8009208:	197b      	adds	r3, r7, r5
 800920a:	781b      	ldrb	r3, [r3, #0]
 800920c:	b25b      	sxtb	r3, r3
 800920e:	2b00      	cmp	r3, #0
 8009210:	d14b      	bne.n	80092aa <VL53L0X_get_pal_range_status+0x232>
 8009212:	197c      	adds	r4, r7, r5
 8009214:	231c      	movs	r3, #28
 8009216:	18fa      	adds	r2, r7, r3
 8009218:	68fb      	ldr	r3, [r7, #12]
 800921a:	2102      	movs	r1, #2
 800921c:	0018      	movs	r0, r3
 800921e:	f7fb fee5 	bl	8004fec <VL53L0X_GetLimitCheckValue>
 8009222:	0003      	movs	r3, r0
 8009224:	7023      	strb	r3, [r4, #0]
 8009226:	197b      	adds	r3, r7, r5
 8009228:	781b      	ldrb	r3, [r3, #0]
 800922a:	b25b      	sxtb	r3, r3
 800922c:	2b00      	cmp	r3, #0
 800922e:	d108      	bne.n	8009242 <VL53L0X_get_pal_range_status+0x1ca>
 8009230:	197c      	adds	r4, r7, r5
 8009232:	68fb      	ldr	r3, [r7, #12]
 8009234:	2201      	movs	r2, #1
 8009236:	21ff      	movs	r1, #255	; 0xff
 8009238:	0018      	movs	r0, r3
 800923a:	f7fa fb5b 	bl	80038f4 <VL53L0X_WrByte>
 800923e:	0003      	movs	r3, r0
 8009240:	7023      	strb	r3, [r4, #0]
 8009242:	223f      	movs	r2, #63	; 0x3f
 8009244:	18bb      	adds	r3, r7, r2
 8009246:	781b      	ldrb	r3, [r3, #0]
 8009248:	b25b      	sxtb	r3, r3
 800924a:	2b00      	cmp	r3, #0
 800924c:	d109      	bne.n	8009262 <VL53L0X_get_pal_range_status+0x1ea>
 800924e:	18bc      	adds	r4, r7, r2
 8009250:	2316      	movs	r3, #22
 8009252:	18fa      	adds	r2, r7, r3
 8009254:	68fb      	ldr	r3, [r7, #12]
 8009256:	21b6      	movs	r1, #182	; 0xb6
 8009258:	0018      	movs	r0, r3
 800925a:	f7fa fbce 	bl	80039fa <VL53L0X_RdWord>
 800925e:	0003      	movs	r3, r0
 8009260:	7023      	strb	r3, [r4, #0]
 8009262:	223f      	movs	r2, #63	; 0x3f
 8009264:	18bb      	adds	r3, r7, r2
 8009266:	781b      	ldrb	r3, [r3, #0]
 8009268:	b25b      	sxtb	r3, r3
 800926a:	2b00      	cmp	r3, #0
 800926c:	d108      	bne.n	8009280 <VL53L0X_get_pal_range_status+0x208>
 800926e:	18bc      	adds	r4, r7, r2
 8009270:	68fb      	ldr	r3, [r7, #12]
 8009272:	2200      	movs	r2, #0
 8009274:	21ff      	movs	r1, #255	; 0xff
 8009276:	0018      	movs	r0, r3
 8009278:	f7fa fb3c 	bl	80038f4 <VL53L0X_WrByte>
 800927c:	0003      	movs	r3, r0
 800927e:	7023      	strb	r3, [r4, #0]
 8009280:	2316      	movs	r3, #22
 8009282:	18fb      	adds	r3, r7, r3
 8009284:	881b      	ldrh	r3, [r3, #0]
 8009286:	025b      	lsls	r3, r3, #9
 8009288:	62fb      	str	r3, [r7, #44]	; 0x2c
 800928a:	68fa      	ldr	r2, [r7, #12]
 800928c:	2394      	movs	r3, #148	; 0x94
 800928e:	005b      	lsls	r3, r3, #1
 8009290:	6af9      	ldr	r1, [r7, #44]	; 0x2c
 8009292:	50d1      	str	r1, [r2, r3]
 8009294:	69fb      	ldr	r3, [r7, #28]
 8009296:	2b00      	cmp	r3, #0
 8009298:	d007      	beq.n	80092aa <VL53L0X_get_pal_range_status+0x232>
 800929a:	69fb      	ldr	r3, [r7, #28]
 800929c:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 800929e:	429a      	cmp	r2, r3
 80092a0:	d903      	bls.n	80092aa <VL53L0X_get_pal_range_status+0x232>
 80092a2:	233c      	movs	r3, #60	; 0x3c
 80092a4:	18fb      	adds	r3, r7, r3
 80092a6:	2201      	movs	r2, #1
 80092a8:	701a      	strb	r2, [r3, #0]
 80092aa:	223f      	movs	r2, #63	; 0x3f
 80092ac:	18bb      	adds	r3, r7, r2
 80092ae:	781b      	ldrb	r3, [r3, #0]
 80092b0:	b25b      	sxtb	r3, r3
 80092b2:	2b00      	cmp	r3, #0
 80092b4:	d109      	bne.n	80092ca <VL53L0X_get_pal_range_status+0x252>
 80092b6:	18bc      	adds	r4, r7, r2
 80092b8:	2328      	movs	r3, #40	; 0x28
 80092ba:	18fa      	adds	r2, r7, r3
 80092bc:	68fb      	ldr	r3, [r7, #12]
 80092be:	2103      	movs	r1, #3
 80092c0:	0018      	movs	r0, r3
 80092c2:	f7fb fdef 	bl	8004ea4 <VL53L0X_GetLimitCheckEnable>
 80092c6:	0003      	movs	r3, r0
 80092c8:	7023      	strb	r3, [r4, #0]
 80092ca:	2328      	movs	r3, #40	; 0x28
 80092cc:	18fb      	adds	r3, r7, r3
 80092ce:	781b      	ldrb	r3, [r3, #0]
 80092d0:	2b00      	cmp	r3, #0
 80092d2:	d02e      	beq.n	8009332 <VL53L0X_get_pal_range_status+0x2ba>
 80092d4:	233f      	movs	r3, #63	; 0x3f
 80092d6:	18fb      	adds	r3, r7, r3
 80092d8:	781b      	ldrb	r3, [r3, #0]
 80092da:	b25b      	sxtb	r3, r3
 80092dc:	2b00      	cmp	r3, #0
 80092de:	d128      	bne.n	8009332 <VL53L0X_get_pal_range_status+0x2ba>
 80092e0:	2308      	movs	r3, #8
 80092e2:	18fb      	adds	r3, r7, r3
 80092e4:	881b      	ldrh	r3, [r3, #0]
 80092e6:	2b00      	cmp	r3, #0
 80092e8:	d102      	bne.n	80092f0 <VL53L0X_get_pal_range_status+0x278>
 80092ea:	2300      	movs	r3, #0
 80092ec:	637b      	str	r3, [r7, #52]	; 0x34
 80092ee:	e00a      	b.n	8009306 <VL53L0X_get_pal_range_status+0x28e>
 80092f0:	687b      	ldr	r3, [r7, #4]
 80092f2:	021a      	lsls	r2, r3, #8
 80092f4:	2308      	movs	r3, #8
 80092f6:	18fb      	adds	r3, r7, r3
 80092f8:	881b      	ldrh	r3, [r3, #0]
 80092fa:	0019      	movs	r1, r3
 80092fc:	0010      	movs	r0, r2
 80092fe:	f7f6 ff03 	bl	8000108 <__udivsi3>
 8009302:	0003      	movs	r3, r0
 8009304:	637b      	str	r3, [r7, #52]	; 0x34
 8009306:	233f      	movs	r3, #63	; 0x3f
 8009308:	18fc      	adds	r4, r7, r3
 800930a:	2318      	movs	r3, #24
 800930c:	18fa      	adds	r2, r7, r3
 800930e:	68fb      	ldr	r3, [r7, #12]
 8009310:	2103      	movs	r1, #3
 8009312:	0018      	movs	r0, r3
 8009314:	f7fb fe6a 	bl	8004fec <VL53L0X_GetLimitCheckValue>
 8009318:	0003      	movs	r3, r0
 800931a:	7023      	strb	r3, [r4, #0]
 800931c:	69bb      	ldr	r3, [r7, #24]
 800931e:	2b00      	cmp	r3, #0
 8009320:	d007      	beq.n	8009332 <VL53L0X_get_pal_range_status+0x2ba>
 8009322:	69bb      	ldr	r3, [r7, #24]
 8009324:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8009326:	429a      	cmp	r2, r3
 8009328:	d203      	bcs.n	8009332 <VL53L0X_get_pal_range_status+0x2ba>
 800932a:	233b      	movs	r3, #59	; 0x3b
 800932c:	18fb      	adds	r3, r7, r3
 800932e:	2201      	movs	r2, #1
 8009330:	701a      	strb	r2, [r3, #0]
 8009332:	233f      	movs	r3, #63	; 0x3f
 8009334:	18fb      	adds	r3, r7, r3
 8009336:	781b      	ldrb	r3, [r3, #0]
 8009338:	b25b      	sxtb	r3, r3
 800933a:	2b00      	cmp	r3, #0
 800933c:	d152      	bne.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 800933e:	233e      	movs	r3, #62	; 0x3e
 8009340:	18fb      	adds	r3, r7, r3
 8009342:	781b      	ldrb	r3, [r3, #0]
 8009344:	2b01      	cmp	r3, #1
 8009346:	d103      	bne.n	8009350 <VL53L0X_get_pal_range_status+0x2d8>
 8009348:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 800934a:	22ff      	movs	r2, #255	; 0xff
 800934c:	701a      	strb	r2, [r3, #0]
 800934e:	e049      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 8009350:	2232      	movs	r2, #50	; 0x32
 8009352:	18bb      	adds	r3, r7, r2
 8009354:	781b      	ldrb	r3, [r3, #0]
 8009356:	2b01      	cmp	r3, #1
 8009358:	d007      	beq.n	800936a <VL53L0X_get_pal_range_status+0x2f2>
 800935a:	18bb      	adds	r3, r7, r2
 800935c:	781b      	ldrb	r3, [r3, #0]
 800935e:	2b02      	cmp	r3, #2
 8009360:	d003      	beq.n	800936a <VL53L0X_get_pal_range_status+0x2f2>
 8009362:	18bb      	adds	r3, r7, r2
 8009364:	781b      	ldrb	r3, [r3, #0]
 8009366:	2b03      	cmp	r3, #3
 8009368:	d103      	bne.n	8009372 <VL53L0X_get_pal_range_status+0x2fa>
 800936a:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 800936c:	2205      	movs	r2, #5
 800936e:	701a      	strb	r2, [r3, #0]
 8009370:	e038      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 8009372:	2232      	movs	r2, #50	; 0x32
 8009374:	18bb      	adds	r3, r7, r2
 8009376:	781b      	ldrb	r3, [r3, #0]
 8009378:	2b06      	cmp	r3, #6
 800937a:	d003      	beq.n	8009384 <VL53L0X_get_pal_range_status+0x30c>
 800937c:	18bb      	adds	r3, r7, r2
 800937e:	781b      	ldrb	r3, [r3, #0]
 8009380:	2b09      	cmp	r3, #9
 8009382:	d103      	bne.n	800938c <VL53L0X_get_pal_range_status+0x314>
 8009384:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 8009386:	2204      	movs	r2, #4
 8009388:	701a      	strb	r2, [r3, #0]
 800938a:	e02b      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 800938c:	2232      	movs	r2, #50	; 0x32
 800938e:	18bb      	adds	r3, r7, r2
 8009390:	781b      	ldrb	r3, [r3, #0]
 8009392:	2b08      	cmp	r3, #8
 8009394:	d008      	beq.n	80093a8 <VL53L0X_get_pal_range_status+0x330>
 8009396:	18bb      	adds	r3, r7, r2
 8009398:	781b      	ldrb	r3, [r3, #0]
 800939a:	2b0a      	cmp	r3, #10
 800939c:	d004      	beq.n	80093a8 <VL53L0X_get_pal_range_status+0x330>
 800939e:	233c      	movs	r3, #60	; 0x3c
 80093a0:	18fb      	adds	r3, r7, r3
 80093a2:	781b      	ldrb	r3, [r3, #0]
 80093a4:	2b01      	cmp	r3, #1
 80093a6:	d103      	bne.n	80093b0 <VL53L0X_get_pal_range_status+0x338>
 80093a8:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80093aa:	2203      	movs	r2, #3
 80093ac:	701a      	strb	r2, [r3, #0]
 80093ae:	e019      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 80093b0:	2332      	movs	r3, #50	; 0x32
 80093b2:	18fb      	adds	r3, r7, r3
 80093b4:	781b      	ldrb	r3, [r3, #0]
 80093b6:	2b04      	cmp	r3, #4
 80093b8:	d004      	beq.n	80093c4 <VL53L0X_get_pal_range_status+0x34c>
 80093ba:	233b      	movs	r3, #59	; 0x3b
 80093bc:	18fb      	adds	r3, r7, r3
 80093be:	781b      	ldrb	r3, [r3, #0]
 80093c0:	2b01      	cmp	r3, #1
 80093c2:	d103      	bne.n	80093cc <VL53L0X_get_pal_range_status+0x354>
 80093c4:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80093c6:	2202      	movs	r2, #2
 80093c8:	701a      	strb	r2, [r3, #0]
 80093ca:	e00b      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 80093cc:	233d      	movs	r3, #61	; 0x3d
 80093ce:	18fb      	adds	r3, r7, r3
 80093d0:	781b      	ldrb	r3, [r3, #0]
 80093d2:	2b01      	cmp	r3, #1
 80093d4:	d103      	bne.n	80093de <VL53L0X_get_pal_range_status+0x366>
 80093d6:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80093d8:	2201      	movs	r2, #1
 80093da:	701a      	strb	r2, [r3, #0]
 80093dc:	e002      	b.n	80093e4 <VL53L0X_get_pal_range_status+0x36c>
 80093de:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80093e0:	2200      	movs	r2, #0
 80093e2:	701a      	strb	r2, [r3, #0]
 80093e4:	6d7b      	ldr	r3, [r7, #84]	; 0x54
 80093e6:	781b      	ldrb	r3, [r3, #0]
 80093e8:	2b00      	cmp	r3, #0
 80093ea:	d102      	bne.n	80093f2 <VL53L0X_get_pal_range_status+0x37a>
 80093ec:	6d3b      	ldr	r3, [r7, #80]	; 0x50
 80093ee:	2200      	movs	r2, #0
 80093f0:	815a      	strh	r2, [r3, #10]
 80093f2:	253f      	movs	r5, #63	; 0x3f
 80093f4:	197c      	adds	r4, r7, r5
 80093f6:	232a      	movs	r3, #42	; 0x2a
 80093f8:	18fa      	adds	r2, r7, r3
 80093fa:	68fb      	ldr	r3, [r7, #12]
 80093fc:	2101      	movs	r1, #1
 80093fe:	0018      	movs	r0, r3
 8009400:	f7fb fd50 	bl	8004ea4 <VL53L0X_GetLimitCheckEnable>
 8009404:	0003      	movs	r3, r0
 8009406:	7023      	strb	r3, [r4, #0]
 8009408:	197b      	adds	r3, r7, r5
 800940a:	781b      	ldrb	r3, [r3, #0]
 800940c:	b25b      	sxtb	r3, r3
 800940e:	2b00      	cmp	r3, #0
 8009410:	d163      	bne.n	80094da <VL53L0X_get_pal_range_status+0x462>
 8009412:	232b      	movs	r3, #43	; 0x2b
 8009414:	18fb      	adds	r3, r7, r3
 8009416:	781b      	ldrb	r3, [r3, #0]
 8009418:	2b00      	cmp	r3, #0
 800941a:	d004      	beq.n	8009426 <VL53L0X_get_pal_range_status+0x3ae>
 800941c:	233d      	movs	r3, #61	; 0x3d
 800941e:	18fb      	adds	r3, r7, r3
 8009420:	781b      	ldrb	r3, [r3, #0]
 8009422:	2b01      	cmp	r3, #1
 8009424:	d104      	bne.n	8009430 <VL53L0X_get_pal_range_status+0x3b8>
 8009426:	2333      	movs	r3, #51	; 0x33
 8009428:	18fb      	adds	r3, r7, r3
 800942a:	2201      	movs	r2, #1
 800942c:	701a      	strb	r2, [r3, #0]
 800942e:	e003      	b.n	8009438 <VL53L0X_get_pal_range_status+0x3c0>
 8009430:	2333      	movs	r3, #51	; 0x33
 8009432:	18fb      	adds	r3, r7, r3
 8009434:	2200      	movs	r2, #0
 8009436:	701a      	strb	r2, [r3, #0]
 8009438:	68fb      	ldr	r3, [r7, #12]
 800943a:	2233      	movs	r2, #51	; 0x33
 800943c:	18ba      	adds	r2, r7, r2
 800943e:	212e      	movs	r1, #46	; 0x2e
 8009440:	7812      	ldrb	r2, [r2, #0]
 8009442:	545a      	strb	r2, [r3, r1]
 8009444:	2332      	movs	r3, #50	; 0x32
 8009446:	18fb      	adds	r3, r7, r3
 8009448:	781b      	ldrb	r3, [r3, #0]
 800944a:	2b04      	cmp	r3, #4
 800944c:	d004      	beq.n	8009458 <VL53L0X_get_pal_range_status+0x3e0>
 800944e:	232a      	movs	r3, #42	; 0x2a
 8009450:	18fb      	adds	r3, r7, r3
 8009452:	781b      	ldrb	r3, [r3, #0]
 8009454:	2b00      	cmp	r3, #0
 8009456:	d104      	bne.n	8009462 <VL53L0X_get_pal_range_status+0x3ea>
 8009458:	2333      	movs	r3, #51	; 0x33
 800945a:	18fb      	adds	r3, r7, r3
 800945c:	2201      	movs	r2, #1
 800945e:	701a      	strb	r2, [r3, #0]
 8009460:	e003      	b.n	800946a <VL53L0X_get_pal_range_status+0x3f2>
 8009462:	2333      	movs	r3, #51	; 0x33
 8009464:	18fb      	adds	r3, r7, r3
 8009466:	2200      	movs	r2, #0
 8009468:	701a      	strb	r2, [r3, #0]
 800946a:	68fb      	ldr	r3, [r7, #12]
 800946c:	2233      	movs	r2, #51	; 0x33
 800946e:	18ba      	adds	r2, r7, r2
 8009470:	212f      	movs	r1, #47	; 0x2f
 8009472:	7812      	ldrb	r2, [r2, #0]
 8009474:	545a      	strb	r2, [r3, r1]
 8009476:	2329      	movs	r3, #41	; 0x29
 8009478:	18fb      	adds	r3, r7, r3
 800947a:	781b      	ldrb	r3, [r3, #0]
 800947c:	2b00      	cmp	r3, #0
 800947e:	d004      	beq.n	800948a <VL53L0X_get_pal_range_status+0x412>
 8009480:	233c      	movs	r3, #60	; 0x3c
 8009482:	18fb      	adds	r3, r7, r3
 8009484:	781b      	ldrb	r3, [r3, #0]
 8009486:	2b01      	cmp	r3, #1
 8009488:	d104      	bne.n	8009494 <VL53L0X_get_pal_range_status+0x41c>
 800948a:	2333      	movs	r3, #51	; 0x33
 800948c:	18fb      	adds	r3, r7, r3
 800948e:	2201      	movs	r2, #1
 8009490:	701a      	strb	r2, [r3, #0]
 8009492:	e003      	b.n	800949c <VL53L0X_get_pal_range_status+0x424>
 8009494:	2333      	movs	r3, #51	; 0x33
 8009496:	18fb      	adds	r3, r7, r3
 8009498:	2200      	movs	r2, #0
 800949a:	701a      	strb	r2, [r3, #0]
 800949c:	68fb      	ldr	r3, [r7, #12]
 800949e:	2233      	movs	r2, #51	; 0x33
 80094a0:	18ba      	adds	r2, r7, r2
 80094a2:	2130      	movs	r1, #48	; 0x30
 80094a4:	7812      	ldrb	r2, [r2, #0]
 80094a6:	545a      	strb	r2, [r3, r1]
 80094a8:	2328      	movs	r3, #40	; 0x28
 80094aa:	18fb      	adds	r3, r7, r3
 80094ac:	781b      	ldrb	r3, [r3, #0]
 80094ae:	2b00      	cmp	r3, #0
 80094b0:	d004      	beq.n	80094bc <VL53L0X_get_pal_range_status+0x444>
 80094b2:	233b      	movs	r3, #59	; 0x3b
 80094b4:	18fb      	adds	r3, r7, r3
 80094b6:	781b      	ldrb	r3, [r3, #0]
 80094b8:	2b01      	cmp	r3, #1
 80094ba:	d104      	bne.n	80094c6 <VL53L0X_get_pal_range_status+0x44e>
 80094bc:	2333      	movs	r3, #51	; 0x33
 80094be:	18fb      	adds	r3, r7, r3
 80094c0:	2201      	movs	r2, #1
 80094c2:	701a      	strb	r2, [r3, #0]
 80094c4:	e003      	b.n	80094ce <VL53L0X_get_pal_range_status+0x456>
 80094c6:	2333      	movs	r3, #51	; 0x33
 80094c8:	18fb      	adds	r3, r7, r3
 80094ca:	2200      	movs	r2, #0
 80094cc:	701a      	strb	r2, [r3, #0]
 80094ce:	68fb      	ldr	r3, [r7, #12]
 80094d0:	2233      	movs	r2, #51	; 0x33
 80094d2:	18ba      	adds	r2, r7, r2
 80094d4:	2131      	movs	r1, #49	; 0x31
 80094d6:	7812      	ldrb	r2, [r2, #0]
 80094d8:	545a      	strb	r2, [r3, r1]
 80094da:	233f      	movs	r3, #63	; 0x3f
 80094dc:	18fb      	adds	r3, r7, r3
 80094de:	781b      	ldrb	r3, [r3, #0]
 80094e0:	b25b      	sxtb	r3, r3
 80094e2:	0018      	movs	r0, r3
 80094e4:	46bd      	mov	sp, r7
 80094e6:	b010      	add	sp, #64	; 0x40
 80094e8:	bdb0      	pop	{r4, r5, r7, pc}
	...

080094ec <__libc_init_array>:
 80094ec:	b570      	push	{r4, r5, r6, lr}
 80094ee:	4d0c      	ldr	r5, [pc, #48]	; (8009520 <__libc_init_array+0x34>)
 80094f0:	4e0c      	ldr	r6, [pc, #48]	; (8009524 <__libc_init_array+0x38>)
 80094f2:	1b76      	subs	r6, r6, r5
 80094f4:	10b6      	asrs	r6, r6, #2
 80094f6:	d005      	beq.n	8009504 <__libc_init_array+0x18>
 80094f8:	2400      	movs	r4, #0
 80094fa:	cd08      	ldmia	r5!, {r3}
 80094fc:	3401      	adds	r4, #1
 80094fe:	4798      	blx	r3
 8009500:	42a6      	cmp	r6, r4
 8009502:	d1fa      	bne.n	80094fa <__libc_init_array+0xe>
 8009504:	f000 f946 	bl	8009794 <_init>
 8009508:	4d07      	ldr	r5, [pc, #28]	; (8009528 <__libc_init_array+0x3c>)
 800950a:	4e08      	ldr	r6, [pc, #32]	; (800952c <__libc_init_array+0x40>)
 800950c:	1b76      	subs	r6, r6, r5
 800950e:	10b6      	asrs	r6, r6, #2
 8009510:	d005      	beq.n	800951e <__libc_init_array+0x32>
 8009512:	2400      	movs	r4, #0
 8009514:	cd08      	ldmia	r5!, {r3}
 8009516:	3401      	adds	r4, #1
 8009518:	4798      	blx	r3
 800951a:	42a6      	cmp	r6, r4
 800951c:	d1fa      	bne.n	8009514 <__libc_init_array+0x28>
 800951e:	bd70      	pop	{r4, r5, r6, pc}
 8009520:	0800a0d8 	.word	0x0800a0d8
 8009524:	0800a0d8 	.word	0x0800a0d8
 8009528:	0800a0d8 	.word	0x0800a0d8
 800952c:	0800a0e0 	.word	0x0800a0e0

08009530 <memcpy>:
 8009530:	b5f0      	push	{r4, r5, r6, r7, lr}
 8009532:	46c6      	mov	lr, r8
 8009534:	b500      	push	{lr}
 8009536:	2a0f      	cmp	r2, #15
 8009538:	d942      	bls.n	80095c0 <memcpy+0x90>
 800953a:	000d      	movs	r5, r1
 800953c:	4305      	orrs	r5, r0
 800953e:	000c      	movs	r4, r1
 8009540:	0003      	movs	r3, r0
 8009542:	07ad      	lsls	r5, r5, #30
 8009544:	d141      	bne.n	80095ca <memcpy+0x9a>
 8009546:	0015      	movs	r5, r2
 8009548:	3d10      	subs	r5, #16
 800954a:	092d      	lsrs	r5, r5, #4
 800954c:	46a8      	mov	r8, r5
 800954e:	012d      	lsls	r5, r5, #4
 8009550:	46ac      	mov	ip, r5
 8009552:	4484      	add	ip, r0
 8009554:	e000      	b.n	8009558 <memcpy+0x28>
 8009556:	0033      	movs	r3, r6
 8009558:	6826      	ldr	r6, [r4, #0]
 800955a:	6865      	ldr	r5, [r4, #4]
 800955c:	601e      	str	r6, [r3, #0]
 800955e:	68e6      	ldr	r6, [r4, #12]
 8009560:	68a7      	ldr	r7, [r4, #8]
 8009562:	60de      	str	r6, [r3, #12]
 8009564:	001e      	movs	r6, r3
 8009566:	605d      	str	r5, [r3, #4]
 8009568:	609f      	str	r7, [r3, #8]
 800956a:	3410      	adds	r4, #16
 800956c:	3610      	adds	r6, #16
 800956e:	4563      	cmp	r3, ip
 8009570:	d1f1      	bne.n	8009556 <memcpy+0x26>
 8009572:	4645      	mov	r5, r8
 8009574:	240f      	movs	r4, #15
 8009576:	230c      	movs	r3, #12
 8009578:	3501      	adds	r5, #1
 800957a:	012d      	lsls	r5, r5, #4
 800957c:	1949      	adds	r1, r1, r5
 800957e:	4014      	ands	r4, r2
 8009580:	1945      	adds	r5, r0, r5
 8009582:	4213      	tst	r3, r2
 8009584:	d024      	beq.n	80095d0 <memcpy+0xa0>
 8009586:	2300      	movs	r3, #0
 8009588:	58ce      	ldr	r6, [r1, r3]
 800958a:	50ee      	str	r6, [r5, r3]
 800958c:	3304      	adds	r3, #4
 800958e:	1ae6      	subs	r6, r4, r3
 8009590:	2e03      	cmp	r6, #3
 8009592:	d8f9      	bhi.n	8009588 <memcpy+0x58>
 8009594:	2303      	movs	r3, #3
 8009596:	3c04      	subs	r4, #4
 8009598:	08a4      	lsrs	r4, r4, #2
 800959a:	3401      	adds	r4, #1
 800959c:	00a4      	lsls	r4, r4, #2
 800959e:	401a      	ands	r2, r3
 80095a0:	192d      	adds	r5, r5, r4
 80095a2:	1909      	adds	r1, r1, r4
 80095a4:	1e56      	subs	r6, r2, #1
 80095a6:	2a00      	cmp	r2, #0
 80095a8:	d007      	beq.n	80095ba <memcpy+0x8a>
 80095aa:	2300      	movs	r3, #0
 80095ac:	e000      	b.n	80095b0 <memcpy+0x80>
 80095ae:	0023      	movs	r3, r4
 80095b0:	5cca      	ldrb	r2, [r1, r3]
 80095b2:	1c5c      	adds	r4, r3, #1
 80095b4:	54ea      	strb	r2, [r5, r3]
 80095b6:	429e      	cmp	r6, r3
 80095b8:	d1f9      	bne.n	80095ae <memcpy+0x7e>
 80095ba:	bc04      	pop	{r2}
 80095bc:	4690      	mov	r8, r2
 80095be:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80095c0:	0005      	movs	r5, r0
 80095c2:	1e56      	subs	r6, r2, #1
 80095c4:	2a00      	cmp	r2, #0
 80095c6:	d1f0      	bne.n	80095aa <memcpy+0x7a>
 80095c8:	e7f7      	b.n	80095ba <memcpy+0x8a>
 80095ca:	1e56      	subs	r6, r2, #1
 80095cc:	0005      	movs	r5, r0
 80095ce:	e7ec      	b.n	80095aa <memcpy+0x7a>
 80095d0:	0022      	movs	r2, r4
 80095d2:	e7f6      	b.n	80095c2 <memcpy+0x92>

080095d4 <memset>:
 80095d4:	b5f0      	push	{r4, r5, r6, r7, lr}
 80095d6:	0005      	movs	r5, r0
 80095d8:	0783      	lsls	r3, r0, #30
 80095da:	d04a      	beq.n	8009672 <memset+0x9e>
 80095dc:	1e54      	subs	r4, r2, #1
 80095de:	2a00      	cmp	r2, #0
 80095e0:	d044      	beq.n	800966c <memset+0x98>
 80095e2:	b2ce      	uxtb	r6, r1
 80095e4:	0003      	movs	r3, r0
 80095e6:	2203      	movs	r2, #3
 80095e8:	e002      	b.n	80095f0 <memset+0x1c>
 80095ea:	3501      	adds	r5, #1
 80095ec:	3c01      	subs	r4, #1
 80095ee:	d33d      	bcc.n	800966c <memset+0x98>
 80095f0:	3301      	adds	r3, #1
 80095f2:	702e      	strb	r6, [r5, #0]
 80095f4:	4213      	tst	r3, r2
 80095f6:	d1f8      	bne.n	80095ea <memset+0x16>
 80095f8:	2c03      	cmp	r4, #3
 80095fa:	d92f      	bls.n	800965c <memset+0x88>
 80095fc:	22ff      	movs	r2, #255	; 0xff
 80095fe:	400a      	ands	r2, r1
 8009600:	0215      	lsls	r5, r2, #8
 8009602:	4315      	orrs	r5, r2
 8009604:	042a      	lsls	r2, r5, #16
 8009606:	4315      	orrs	r5, r2
 8009608:	2c0f      	cmp	r4, #15
 800960a:	d935      	bls.n	8009678 <memset+0xa4>
 800960c:	0027      	movs	r7, r4
 800960e:	3f10      	subs	r7, #16
 8009610:	093f      	lsrs	r7, r7, #4
 8009612:	013e      	lsls	r6, r7, #4
 8009614:	46b4      	mov	ip, r6
 8009616:	001e      	movs	r6, r3
 8009618:	001a      	movs	r2, r3
 800961a:	3610      	adds	r6, #16
 800961c:	4466      	add	r6, ip
 800961e:	6015      	str	r5, [r2, #0]
 8009620:	6055      	str	r5, [r2, #4]
 8009622:	6095      	str	r5, [r2, #8]
 8009624:	60d5      	str	r5, [r2, #12]
 8009626:	3210      	adds	r2, #16
 8009628:	42b2      	cmp	r2, r6
 800962a:	d1f8      	bne.n	800961e <memset+0x4a>
 800962c:	260f      	movs	r6, #15
 800962e:	220c      	movs	r2, #12
 8009630:	3701      	adds	r7, #1
 8009632:	013f      	lsls	r7, r7, #4
 8009634:	4026      	ands	r6, r4
 8009636:	19db      	adds	r3, r3, r7
 8009638:	0037      	movs	r7, r6
 800963a:	4222      	tst	r2, r4
 800963c:	d017      	beq.n	800966e <memset+0x9a>
 800963e:	1f3e      	subs	r6, r7, #4
 8009640:	08b6      	lsrs	r6, r6, #2
 8009642:	00b4      	lsls	r4, r6, #2
 8009644:	46a4      	mov	ip, r4
 8009646:	001a      	movs	r2, r3
 8009648:	1d1c      	adds	r4, r3, #4
 800964a:	4464      	add	r4, ip
 800964c:	c220      	stmia	r2!, {r5}
 800964e:	42a2      	cmp	r2, r4
 8009650:	d1fc      	bne.n	800964c <memset+0x78>
 8009652:	2403      	movs	r4, #3
 8009654:	3601      	adds	r6, #1
 8009656:	00b6      	lsls	r6, r6, #2
 8009658:	199b      	adds	r3, r3, r6
 800965a:	403c      	ands	r4, r7
 800965c:	2c00      	cmp	r4, #0
 800965e:	d005      	beq.n	800966c <memset+0x98>
 8009660:	b2c9      	uxtb	r1, r1
 8009662:	191c      	adds	r4, r3, r4
 8009664:	7019      	strb	r1, [r3, #0]
 8009666:	3301      	adds	r3, #1
 8009668:	429c      	cmp	r4, r3
 800966a:	d1fb      	bne.n	8009664 <memset+0x90>
 800966c:	bdf0      	pop	{r4, r5, r6, r7, pc}
 800966e:	0034      	movs	r4, r6
 8009670:	e7f4      	b.n	800965c <memset+0x88>
 8009672:	0014      	movs	r4, r2
 8009674:	0003      	movs	r3, r0
 8009676:	e7bf      	b.n	80095f8 <memset+0x24>
 8009678:	0027      	movs	r7, r4
 800967a:	e7e0      	b.n	800963e <memset+0x6a>

0800967c <register_fini>:
 800967c:	4b03      	ldr	r3, [pc, #12]	; (800968c <register_fini+0x10>)
 800967e:	b510      	push	{r4, lr}
 8009680:	2b00      	cmp	r3, #0
 8009682:	d002      	beq.n	800968a <register_fini+0xe>
 8009684:	4802      	ldr	r0, [pc, #8]	; (8009690 <register_fini+0x14>)
 8009686:	f000 f805 	bl	8009694 <atexit>
 800968a:	bd10      	pop	{r4, pc}
 800968c:	00000000 	.word	0x00000000
 8009690:	080096a5 	.word	0x080096a5

08009694 <atexit>:
 8009694:	b510      	push	{r4, lr}
 8009696:	0001      	movs	r1, r0
 8009698:	2300      	movs	r3, #0
 800969a:	2200      	movs	r2, #0
 800969c:	2000      	movs	r0, #0
 800969e:	f000 f819 	bl	80096d4 <__register_exitproc>
 80096a2:	bd10      	pop	{r4, pc}

080096a4 <__libc_fini_array>:
 80096a4:	b570      	push	{r4, r5, r6, lr}
 80096a6:	4d07      	ldr	r5, [pc, #28]	; (80096c4 <__libc_fini_array+0x20>)
 80096a8:	4c07      	ldr	r4, [pc, #28]	; (80096c8 <__libc_fini_array+0x24>)
 80096aa:	1b64      	subs	r4, r4, r5
 80096ac:	10a4      	asrs	r4, r4, #2
 80096ae:	d005      	beq.n	80096bc <__libc_fini_array+0x18>
 80096b0:	3c01      	subs	r4, #1
 80096b2:	00a3      	lsls	r3, r4, #2
 80096b4:	58eb      	ldr	r3, [r5, r3]
 80096b6:	4798      	blx	r3
 80096b8:	2c00      	cmp	r4, #0
 80096ba:	d1f9      	bne.n	80096b0 <__libc_fini_array+0xc>
 80096bc:	f000 f870 	bl	80097a0 <_fini>
 80096c0:	bd70      	pop	{r4, r5, r6, pc}
 80096c2:	46c0      	nop			; (mov r8, r8)
 80096c4:	0800a0e0 	.word	0x0800a0e0
 80096c8:	0800a0e4 	.word	0x0800a0e4

080096cc <__retarget_lock_acquire_recursive>:
 80096cc:	4770      	bx	lr
 80096ce:	46c0      	nop			; (mov r8, r8)

080096d0 <__retarget_lock_release_recursive>:
 80096d0:	4770      	bx	lr
 80096d2:	46c0      	nop			; (mov r8, r8)

080096d4 <__register_exitproc>:
 80096d4:	b5f0      	push	{r4, r5, r6, r7, lr}
 80096d6:	46de      	mov	lr, fp
 80096d8:	4645      	mov	r5, r8
 80096da:	464e      	mov	r6, r9
 80096dc:	4657      	mov	r7, sl
 80096de:	b5e0      	push	{r5, r6, r7, lr}
 80096e0:	4c2a      	ldr	r4, [pc, #168]	; (800978c <__register_exitproc+0xb8>)
 80096e2:	b083      	sub	sp, #12
 80096e4:	0005      	movs	r5, r0
 80096e6:	6820      	ldr	r0, [r4, #0]
 80096e8:	4690      	mov	r8, r2
 80096ea:	469b      	mov	fp, r3
 80096ec:	000e      	movs	r6, r1
 80096ee:	f7ff ffed 	bl	80096cc <__retarget_lock_acquire_recursive>
 80096f2:	4b27      	ldr	r3, [pc, #156]	; (8009790 <__register_exitproc+0xbc>)
 80096f4:	681b      	ldr	r3, [r3, #0]
 80096f6:	9301      	str	r3, [sp, #4]
 80096f8:	23a4      	movs	r3, #164	; 0xa4
 80096fa:	9a01      	ldr	r2, [sp, #4]
 80096fc:	005b      	lsls	r3, r3, #1
 80096fe:	58d2      	ldr	r2, [r2, r3]
 8009700:	2a00      	cmp	r2, #0
 8009702:	d038      	beq.n	8009776 <__register_exitproc+0xa2>
 8009704:	6853      	ldr	r3, [r2, #4]
 8009706:	6820      	ldr	r0, [r4, #0]
 8009708:	2b1f      	cmp	r3, #31
 800970a:	dc3a      	bgt.n	8009782 <__register_exitproc+0xae>
 800970c:	2d00      	cmp	r5, #0
 800970e:	d10e      	bne.n	800972e <__register_exitproc+0x5a>
 8009710:	1c59      	adds	r1, r3, #1
 8009712:	3302      	adds	r3, #2
 8009714:	009b      	lsls	r3, r3, #2
 8009716:	6051      	str	r1, [r2, #4]
 8009718:	509e      	str	r6, [r3, r2]
 800971a:	f7ff ffd9 	bl	80096d0 <__retarget_lock_release_recursive>
 800971e:	2000      	movs	r0, #0
 8009720:	b003      	add	sp, #12
 8009722:	bc3c      	pop	{r2, r3, r4, r5}
 8009724:	4690      	mov	r8, r2
 8009726:	4699      	mov	r9, r3
 8009728:	46a2      	mov	sl, r4
 800972a:	46ab      	mov	fp, r5
 800972c:	bdf0      	pop	{r4, r5, r6, r7, pc}
 800972e:	0099      	lsls	r1, r3, #2
 8009730:	4689      	mov	r9, r1
 8009732:	4491      	add	r9, r2
 8009734:	4641      	mov	r1, r8
 8009736:	2488      	movs	r4, #136	; 0x88
 8009738:	464f      	mov	r7, r9
 800973a:	5139      	str	r1, [r7, r4]
 800973c:	21c4      	movs	r1, #196	; 0xc4
 800973e:	0049      	lsls	r1, r1, #1
 8009740:	4688      	mov	r8, r1
 8009742:	4490      	add	r8, r2
 8009744:	4641      	mov	r1, r8
 8009746:	3c87      	subs	r4, #135	; 0x87
 8009748:	409c      	lsls	r4, r3
 800974a:	6809      	ldr	r1, [r1, #0]
 800974c:	46a2      	mov	sl, r4
 800974e:	4321      	orrs	r1, r4
 8009750:	468c      	mov	ip, r1
 8009752:	4641      	mov	r1, r8
 8009754:	4664      	mov	r4, ip
 8009756:	600c      	str	r4, [r1, #0]
 8009758:	2184      	movs	r1, #132	; 0x84
 800975a:	464c      	mov	r4, r9
 800975c:	465f      	mov	r7, fp
 800975e:	0049      	lsls	r1, r1, #1
 8009760:	5067      	str	r7, [r4, r1]
 8009762:	2d02      	cmp	r5, #2
 8009764:	d1d4      	bne.n	8009710 <__register_exitproc+0x3c>
 8009766:	0011      	movs	r1, r2
 8009768:	4655      	mov	r5, sl
 800976a:	318d      	adds	r1, #141	; 0x8d
 800976c:	31ff      	adds	r1, #255	; 0xff
 800976e:	680c      	ldr	r4, [r1, #0]
 8009770:	4325      	orrs	r5, r4
 8009772:	600d      	str	r5, [r1, #0]
 8009774:	e7cc      	b.n	8009710 <__register_exitproc+0x3c>
 8009776:	9a01      	ldr	r2, [sp, #4]
 8009778:	9901      	ldr	r1, [sp, #4]
 800977a:	324d      	adds	r2, #77	; 0x4d
 800977c:	32ff      	adds	r2, #255	; 0xff
 800977e:	50ca      	str	r2, [r1, r3]
 8009780:	e7c0      	b.n	8009704 <__register_exitproc+0x30>
 8009782:	f7ff ffa5 	bl	80096d0 <__retarget_lock_release_recursive>
 8009786:	2001      	movs	r0, #1
 8009788:	4240      	negs	r0, r0
 800978a:	e7c9      	b.n	8009720 <__register_exitproc+0x4c>
 800978c:	200006e8 	.word	0x200006e8
 8009790:	0800a0d4 	.word	0x0800a0d4

08009794 <_init>:
 8009794:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8009796:	46c0      	nop			; (mov r8, r8)
 8009798:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800979a:	bc08      	pop	{r3}
 800979c:	469e      	mov	lr, r3
 800979e:	4770      	bx	lr

080097a0 <_fini>:
 80097a0:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 80097a2:	46c0      	nop			; (mov r8, r8)
 80097a4:	bcf8      	pop	{r3, r4, r5, r6, r7}
 80097a6:	bc08      	pop	{r3}
 80097a8:	469e      	mov	lr, r3
 80097aa:	4770      	bx	lr
